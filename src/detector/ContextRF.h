//
//  ContextRF.h
//  FISH
//
//  Created by Yaozong Gao on 9/26/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __ContextDR_h__
#define __ContextDR_h__

#include "common/Random.h"
#include "common/mxImageUtils.h"
#include "forest/ForestTrainer.h"
#include "forest/context/ExhaustSearchTrainingContext.h"
#include "forest/DataCollection.h"
#include "forest/wl/MemoryAxisAlignedWeakLearner.h"
#include "forest/stat/MultiDimensionStatisticsAggregator.h"
#include "forest/tester/MultiDimensionRegressor.h"
#include "extern/ImageHelper.h"
#include "detector/DetectorIO.h"
#include "feature/CompositeFeatureSpace.h"
#include "feature/haar3d/Haar3DFeatureSpace.h"
#include <boost/timer/timer.hpp>

namespace BRIC { namespace IDEA { namespace FISH {

// context-sensitive regression forest
template <typename T, int Dimension>
class ContextRF
{
public:
	// type definitions
	typedef MemoryAxisAlignedWeakLearner W;
	typedef MultiDimensionStatisticsAggregator<Dimension> S;
	typedef Tree<W,S> TreeType;
	typedef Forest<W,S> ForestType;
	typedef boost::array<double,Dimension> VectorType;
	typedef mxImage<VectorType> VectorImageType;

	// interfaces to different regression tasks
	// argument image is a re-sampled image with respect to argument scale

	// distance regression on local images 
	static void localRF(const mxImage<T>& image, std::string rootFolder, std::string detectorName, int level, int scale, const vect3<int>& sp, const vect3<int>& ep, VectorImageType& vectorImage);

	// distance regression on entire image
	static void globalRF(const mxImage<T>& image, std::string rootFolder, std::string detectorName, int level, int scale, VectorImageType& vectorImage);

private:

	// general helper
	struct ContextDetectorInfo {
		std::string name;
		std::string diffOption;
		int level;
	};

	struct DetectorInfo {
		// Info section
		vect3<double> spacing;
		NormalizationType normalizationType; 
		// Context section
		int level;
		std::vector<ContextDetectorInfo> contextDetectors;
	};

	static void ReadInfo(const char* path, DetectorInfo& info);
	static std::string GetDetectorPath(std::string root, std::string name, int level);
	static std::string GetDetectorInfoPath(std::string detectorFolder, int scale);
	static void GetHyrbidFunctorTypes(DetectorInfo& info, std::vector<FunctorType>& functors, int& patchFunctorNum);
	static std::auto_ptr<ForestType> ReadDetectorWithErrorCheck(std::string rootFolder, std::string detectorName, int level, int scale, FeatureSpaceArray<IFeatureSpace>& fss, DetectorInfo& info);

	// helper for localDR
	static void ComputeEffectiveBox(const mxImage<T>& image, const vect3<int>& sp, const vect3<int>& ep, vect3<int>& new_sp, vect3<int>& new_ep);
	static void InitializeVectorImage(VectorImageType& image);
	static void localRF_L0(const mxImage<T>& image, FeatureSpaceArray<IFeatureSpace>& fss, DetectorInfo& info, ForestType* forest, const vect3<int>& sp, const vect3<int>& ep, VectorImageType& vectorImage);
	static void PrepareInputImages(const mxImage<T>& image, const vect3<int>& eff_sp, const vect3<int>& eff_ep, std::string rootFolder, const DetectorInfo& info, int scale, const std::vector<CompositeFeatureSpace*>& cfss, std::vector< mxImage<double> >& inputImages);
};

//////////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////////

template <typename T, int Dimension>
void ContextRF<T,Dimension>::ReadInfo(const char* path, DetectorInfo& info)
{
	boost::property_tree::ptree pt;
	try {

		// load parameters in info section
		boost::property_tree::ini_parser::read_ini(path, pt);
		std::string stringSpacing = pt.get<std::string>("Info.Resolution");
		info.spacing = stringUtils::ParseVect3<double>(stringSpacing);

		std::string strNormalization = pt.get<std::string>("Info.IntensityNormalization");
		if (strNormalization == "NONE") {
			info.normalizationType = NONE;
		}else if(strNormalization == "UNIT_NORM") {
			info.normalizationType = UNIT_NORM;
		}else if(strNormalization == "MAX_ONE") {
			info.normalizationType = MAX_ONE;
		}else if(strNormalization == "MIN_MAX") {
			info.normalizationType = MIN_MAX;
		}else if(strNormalization == "ZERO_MEAN_ONE_STD") {
			info.normalizationType = ZERO_MEAN_ONE_STD;
		}else {
			std::cerr << "normalization type unsupported" << std::endl;
			std::cerr << "Use: NONE, UNIT_NORM, MAX_ONE, MIN_MAX, ZERO_MEAN_ONE_STD" << std::endl;
			exit(-1);
		}

		// load parameters in context section
		info.level = pt.get<int>("Context.Level");
		if (info.level > 0) {
			std::string strNames = pt.get<std::string>("Context.Detectors");

			std::vector<std::string> contextDetectorNames;
			stringUtils::Split(strNames, ' ', contextDetectorNames);

			info.contextDetectors.resize(contextDetectorNames.size());
			for (size_t i = 0; i < contextDetectorNames.size(); ++i) {
				info.contextDetectors[i].name = contextDetectorNames[i];
				std::string entry_name = std::string("Context") + contextDetectorNames[i] + ".Level";
				info.contextDetectors[i].level = pt.get<int>(entry_name);
				entry_name = std::string("Context") + contextDetectorNames[i] + ".Diff";
				info.contextDetectors[i].diffOption = pt.get<std::string>(entry_name);
			}
		}

	} catch ( std::runtime_error& error ) {
		std::cerr << error.what() << std::endl;
		exit(-1);
	}
}

template <typename T, int Dimension>
std::string ContextRF<T,Dimension>::GetDetectorPath(std::string folder, std::string name, int level)
{
	if (level == 0)
		return folder + stringUtils::Slash() + name;
	else
		return folder + stringUtils::Slash() + name + "_C" + stringUtils::num2str(level);
}

template <typename T, int Dimension>
std::string ContextRF<T,Dimension>::GetDetectorInfoPath(std::string detectorFolder, int scale)
{
	return detectorFolder + stringUtils::Slash() + "info_R" + stringUtils::num2str(scale) + ".ini";
}

template <typename T, int Dimension>
void ContextRF<T,Dimension>::GetHyrbidFunctorTypes(DetectorInfo& info, std::vector<FunctorType>& functors, int& patchFunctorNum)
{
	functors.resize(info.contextDetectors.size() + 1);

	if (info.normalizationType == NONE) {
		functors[0] = ImageFunctorType;
		patchFunctorNum = 0;
	} else {
		functors[0] = PatchFunctorType;
		patchFunctorNum = 1;
	}

	for (unsigned int i = 1; i < functors.size(); ++i) 
		functors[i] = ImageFunctorType;
}


template <typename T, int Dimension>
std::auto_ptr<typename ContextRF<T,Dimension>::ForestType> ContextRF<T,Dimension>::ReadDetectorWithErrorCheck(std::string rootFolder, std::string detectorName, int level, int scale, FeatureSpaceArray<IFeatureSpace>& fss, DetectorInfo& info)
{
	// read detector
	std::string detectorFolder = GetDetectorPath(rootFolder, detectorName, level);
	std::string detectorInfoPath = GetDetectorInfoPath(detectorFolder, scale);
	std::auto_ptr<ForestType> forest = DetectorIO::ReadDetector<ForestType>(detectorFolder.c_str(), scale, fss);
	ReadInfo(detectorInfoPath.c_str(), info);

	// check whether the detector is successfully loaded
	if (fss.size() == 0 || forest->GetTreeNumber() == 0) {
		std::cerr << "empty forest or feature space" << std::endl; 
		exit(-1);
	}
	if ( (info.contextDetectors.size()+1) != fss[0].GetNumberFeatureSpaces() ) {
		std::cerr << "detector # + 1 != feature space #" << std::endl;
		exit(-1);
	}
	if (info.level != level) {
		std::cerr << "loaded detector is of level " << info.level << ", not level " << level << std::endl;
		exit(-1);
	}
	return forest;
}

//////////////////////////////////////////////////////////////////////////

template <typename T, int Dimension>
void ContextRF<T,Dimension>::ComputeEffectiveBox(const mxImage<T>& image, const vect3<int>& sp, const vect3<int>& ep, vect3<int>& new_sp, vect3<int>& new_ep)
{
	vect3<unsigned int> imageSize = image.GetImageSize();
	for (unsigned int i = 0; i < 3; ++i)
	{
		if (sp[i] < 0)
			new_sp[i] = 0;
		else
			new_sp[i] = sp[i];

		if (ep[i] >= static_cast<int>(imageSize[i]))
			new_ep[i] = static_cast<int>(imageSize[i])-1;
		else
			new_ep[i] = ep[i];
	}
}

template <typename T, int Dimension>
void ContextRF<T,Dimension>::InitializeVectorImage(VectorImageType& image)
{
	VectorType* internalData = image.GetData();
	vect3<unsigned int> imageSize = image.GetImageSize();
	int totalVoxelSize = imageSize[0] * imageSize[1] * imageSize[2];

	#pragma omp parallel for
	for (int i = 0; i < totalVoxelSize; ++i)
		internalData[i].fill(0);
}

template <typename T, int Dimension>
void ContextRF<T,Dimension>::globalRF(const mxImage<T>& image, std::string rootFolder, std::string detectorName, int level, int scale, VectorImageType& vectorImage)
{
	vect3<unsigned int> imageSize = image.GetImageSize();
	vect3<int> sp(0,0,0);
	vect3<int> ep(imageSize[0]-1, imageSize[1]-1, imageSize[2]-1);

	localRF(image, rootFolder, detectorName, level, scale, sp, ep, vectorImage);
}

template <typename T, int Dimension>
void ContextRF<T,Dimension>::localRF_L0(const mxImage<T>& image, FeatureSpaceArray<IFeatureSpace>& fss, DetectorInfo& info, ForestType* forest, const vect3<int>& sp, const vect3<int>&ep, VectorImageType& vectorImage)
{
	vect3<unsigned int> patchSize = fss[0].GetPatchSize(0);
	vect3<int> eff_sp, eff_ep;
	ComputeEffectiveBox(image, sp, ep, eff_sp, eff_ep);

	// req_sp and req_ep specify the size for input image
	vect3<int> req_sp = eff_sp, req_ep = eff_ep;
	mxImageUtils::PadBox(req_sp, req_ep, patchSize);

	mxImage<double> localImage;
	mxImageUtils::Crop2(image, req_sp, req_ep, localImage, static_cast<double>(0));

	vect3<unsigned int> outputImageSize;
	for (unsigned int i = 0; i < 3; ++i)
		outputImageSize[i] = static_cast<unsigned int>(ep[i] - sp[i] + 1);

	// allocate buffer for offset image
	vectorImage.CopyImageInfo(image);
	vectorImage.SetImageSize(outputImageSize);
	vect3<double> origin_new;
	mxImageUtils::Voxel2World(image, sp, origin_new);
	vectorImage.SetOrigin(origin_new);

	// initialize offset image
	InitializeVectorImage(vectorImage);

	vect3<unsigned int> eff_imageSize, eff_offset;
	for (unsigned int i = 0; i < 3; ++i) {
		if (eff_ep[i] <= eff_sp[i])
			return;
		eff_imageSize[i] = eff_ep[i] - eff_sp[i] + 1;
		eff_offset[i] = eff_sp[i] - sp[i];
	}

	if (info.normalizationType == NONE)
	{
		std::auto_ptr< ImageFeatureFunctorArray< IImageFeatureFunctor > > tmpFunctors = fss.CreateImageFeatureFunctor();
		(*tmpFunctors)[0].PreprocessInput(localImage, 0);

		#pragma omp parallel for schedule(dynamic)
		for (int z = 0; z < static_cast<int>(eff_imageSize[2]); ++z)
		{
			std::auto_ptr< ImageFeatureFunctorArray<IImageFeatureFunctor> > functors = fss.CreateImageFeatureFunctor();
			functors->SetImage(localImage,0);

			for(int y = 0; y < static_cast<int>(eff_imageSize[1]); ++y)
			{
				for (int x = 0; x < static_cast<int>(eff_imageSize[0]); ++x)
				{
					vect3<unsigned int> padvoxel;
					padvoxel[0] = x + patchSize[0]/2;
					padvoxel[1] = y + patchSize[1]/2;
					padvoxel[2] = z + patchSize[2]/2;

					functors->SetVoxel(padvoxel,0);

					vect3<unsigned int> dstvoxel;
					dstvoxel[0] = x + eff_offset[0];
					dstvoxel[1] = y + eff_offset[1];
					dstvoxel[2] = z + eff_offset[2];

					MultiDimensionRegressor<Dimension>::Regress2_weighted(forest, *functors, vectorImage(dstvoxel));
				}
			}
		}

	}else
	{

		#pragma omp parallel for schedule(dynamic)
		for (int z = 0; z < static_cast<int>(eff_imageSize[2]); ++z)
		{
			std::auto_ptr< PatchFeatureFunctorArray<IPatchFeatureFunctor> > functors = fss.CreatePatchFeatureFunctor();
			mxImage<double> patch;

			for(int y = 0; y < static_cast<int>(eff_imageSize[1]); ++y)
			{
				for (int x = 0; x < static_cast<int>(eff_imageSize[0]); ++x)
				{
					vect3<unsigned int> padvoxel;
					padvoxel[0] = x + patchSize[0]/2; 
					padvoxel[1] = y + patchSize[1]/2;
					padvoxel[2] = z + patchSize[2]/2;

					mxImageUtils::Crop3(localImage, padvoxel, patchSize, patch, static_cast<double>(0));
					functors->PreprocessInput(patch,0);
					functors->SetPatch(patch,0);

					vect3<unsigned int> dstvoxel;
					dstvoxel[0] = x + eff_offset[0];
					dstvoxel[1] = y + eff_offset[1];
					dstvoxel[2] = z + eff_offset[2];

					MultiDimensionRegressor<Dimension>::Regress2_weighted(forest, *functors, vectorImage(dstvoxel));
				}
			}
		}

	} // end if-else
}

template <typename T, int Dimension>
void ContextRF<T, Dimension>::PrepareInputImages(const mxImage<T>& image, const vect3<int>& eff_sp, const vect3<int>& eff_ep, std::string rootFolder, const DetectorInfo& info,
	int scale, const std::vector<CompositeFeatureSpace*>& cfss, std::vector< mxImage<double> >& inputImages)
{
	inputImages.resize(cfss[0]->GetNumberFeatureSpaces());

	if (cfss[0]->GetNumberFeatureSpaces() != info.contextDetectors.size() + 1) {
		std::cerr << "feature space and config file mismatch on source number" << std::endl; exit(-1);
	}

	std::vector< vect3<unsigned int> > patchSizes;
	patchSizes.resize(cfss[0]->GetNumberFeatureSpaces());
	for (unsigned int i = 0; i < patchSizes.size(); ++i)
		patchSizes[i] = cfss[0]->GetPatchSize(i);

	{ // prepare intensity image source
		vect3<int> req_sp = eff_sp, req_ep = eff_ep;
		mxImageUtils::PadBox(req_sp, req_ep, patchSizes[0]);
		mxImageUtils::Crop2(image, req_sp, req_ep, inputImages[0], static_cast<double>(0));
	}

	if (inputImages.size() <= 1)
		return;

	// prepare displacement images
	std::vector< VectorImageType > displacementImages;
	displacementImages.resize( inputImages.size() - 1 );

	for (size_t i = 0; i < displacementImages.size(); ++i)
	{
		vect3<int> req_sp = eff_sp, req_ep = eff_ep;
		mxImageUtils::PadBox(req_sp, req_ep, patchSizes[i+1]);
		localRF(image, rootFolder, info.contextDetectors[i].name, info.contextDetectors[i].level, scale, req_sp, req_ep, displacementImages[i]);
	}

	// compute L2-norm distance or difference image
	for (size_t i = 0; i < displacementImages.size(); ++i)
	{
		if (info.contextDetectors[i].diffOption != "NONE")
		{
			int target_source_idx = -1;
			for (size_t j = 0; j < displacementImages.size(); ++j) {
				if (info.contextDetectors[j].name == info.contextDetectors[i].diffOption) {
					target_source_idx = static_cast<int>(j); break;
				}
			}

			if (target_source_idx == -1) {
				std::cerr << "fail to find the detector " << info.contextDetectors[i].diffOption << " for subtraction" << std::endl; exit(-1);
			}
		
			// compute L2-norm distance map of displacement[i] - displacement[j]
			vect3<unsigned int> image_size = displacementImages[i].GetImageSize();
			inputImages[i+1].CopyImageInfo(displacementImages[i]);
			inputImages[i+1].SetImageSize(image_size);

			#pragma omp parallel for
			for (int z = 0; z < static_cast<int>(image_size[2]); ++z) {
				for (int y = 0; y < static_cast<int>(image_size[1]); ++y) {
					for (int x = 0; x < static_cast<int>(image_size[0]); ++x) {

						const VectorType& p1 = displacementImages[i](x, y, z);
						const VectorType& p2 = displacementImages[target_source_idx](x, y, z);

						if (Dimension == 1)
						{
							double v1 = p1[0], v2 = p2[0];
							inputImages[i + 1](x, y, z) = v1 - v2;
						}
						else {
							double v = 0;
							for (int k = 0; k < 3; ++k)
								v += (p1[k] - p2[k]) * (p1[k] - p2[k]);
							inputImages[i + 1](x, y, z) = sqrt(v);

							//double v1 = sqrt(p1[0] * p1[0] + p1[1] * p1[1] + p1[2] * p1[2]);
							//double v2 = sqrt(p2[0] * p2[0] + p2[1] * p2[1] + p2[2] * p2[2]);
							//inputImages[i + 1](x, y, z) = v1 - v2;
						}
					}
				}
			}
		}
		else {
			if (Dimension == 1) {
				// extract first component since dimension = 1
				mxImageUtils::ExtractFromVectorImage<double, double, Dimension>(displacementImages[i], 0, inputImages[i+1]);
			} else {
				// compute L2-norm distance map
				mxImageUtils::Vector2Norm<double, Dimension>(displacementImages[i], inputImages[i+1]);
			}
		}
	}
}

template <typename T, int Dimension>
void ContextRF<T,Dimension>::localRF(const mxImage<T>& image, std::string rootFolder, std::string detectorName, int level, int scale, const vect3<int>& sp, const vect3<int>& ep, VectorImageType& vectorImage)
{
	FeatureSpaceArray<IFeatureSpace> fss;
	DetectorInfo info;

	// read detector
	std::auto_ptr<ForestType> forest = ReadDetectorWithErrorCheck(rootFolder, detectorName, level, scale, fss, info);

	// compute effective box
	vect3<int> eff_sp, eff_ep;
	ComputeEffectiveBox(image, sp, ep, eff_sp, eff_ep);

	// setup output image
	vect3<double> origin_new;
	mxImageUtils::Voxel2World(image, sp, origin_new);
	vectorImage.CopyImageInfo(image);
	vectorImage.SetOrigin(origin_new);
	vect3<unsigned int> outputImageSize(ep[0]-sp[0]+1, ep[1]-sp[1]+1, ep[2]-sp[2]+1);
	vectorImage.SetImageSize(outputImageSize);

	// initialize offset image
	InitializeVectorImage(vectorImage);

	// compute offset relative to the box [sp,ep]
	vect3<unsigned int> eff_imageSize, eff_offset;
	for (unsigned int i = 0; i < 3; ++i) {
		if (eff_ep[i] <= eff_sp[i])
			return;
		eff_imageSize[i] = eff_ep[i] - eff_sp[i] + 1;
		eff_offset[i] = eff_sp[i] - sp[i];
	}

	if (level == 0) {
		// do regression on intensity image
		VectorImageType subImage;
		localRF_L0(image, fss, info, forest.get(), eff_sp, eff_ep, subImage);

		vect3<unsigned int> subImageSize = subImage.GetImageSize();
		for (int z = 0; z < static_cast<int>(subImageSize[2]); ++z) {
			for (int y = 0; y < static_cast<int>(subImageSize[1]); ++y) {
				for (int x = 0; x < static_cast<int>(subImageSize[0]); ++x) {
					vectorImage(x+eff_offset[0], y+eff_offset[1], z+eff_offset[2]) = subImage(x,y,z);
				}
			}
		}

	} else {
		// convert IFeatureSpace pointers to CompositeFeatureSpace pointers
		std::vector<CompositeFeatureSpace*> cfss;
		cfss.resize(fss.size());
		for (unsigned int i = 0; i < cfss.size(); ++i)
			cfss[i] = static_cast<CompositeFeatureSpace*>(&fss[i]);

		// get patch sizes for different input images
		std::vector< vect3<unsigned int> > patchSizes;
		patchSizes.resize( cfss[0]->GetNumberFeatureSpaces() );
		for (unsigned int i = 0; i < patchSizes.size(); ++i) 
			patchSizes[i] = cfss[0]->GetPatchSize(i);

		// get a temporary functor for preprocessing
		int patchFunctorNum = 0;
		std::vector<FunctorType> functorTypes;
		GetHyrbidFunctorTypes(info, functorTypes, patchFunctorNum);
		std::auto_ptr<HybridImageFeatureFunctor> tmpFunctor( cfss[0]->CreateHybridFeatureFunctor(functorTypes) );

		// prepare input images
		std::vector< mxImage<double> > inputImages;
		PrepareInputImages(image, eff_sp, eff_ep, rootFolder, info, scale, cfss, inputImages);

		// preprocess input images
		for (size_t i = 0; i < inputImages.size(); ++i) {
			if (functorTypes[i] == ImageFunctorType)
				tmpFunctor->PreprocessInput(inputImages[i], i);
		}

		#pragma omp parallel for schedule(dynamic)
		for (int z = 0; z < static_cast<int>(eff_imageSize[2]); ++z)
		{
			// setup the functors
			std::vector<HybridImageFeatureFunctor> functors;
			functors.resize(cfss.size());
			for (unsigned int i = 0; i < functors.size(); ++i) 
				new (&functors[i]) HybridImageFeatureFunctor(*cfss[i], functorTypes);
			
			for (unsigned int j = 0; j < functorTypes.size(); ++j) {
				if (functorTypes[j] == ImageFunctorType) {
					for (unsigned int i = 0; i < functors.size(); ++i)
						functors[i].SetImage(inputImages[j], j);
				}
			}

			for (int y = 0; y < static_cast<int>(eff_imageSize[1]); ++y)
			{
				for (int x = 0; x < static_cast<int>(eff_imageSize[0]); ++x)
				{
					// setup functors 
					//  - patch functors: set patch and preprocess
					//  - image functors: set voxel position
					for (unsigned int j = 0; j < functorTypes.size(); ++j)
					{
						vect3<unsigned int> patchSize = patchSizes[j];
						mxImage<double> patchBuffer; // only used if the normalization is set

						vect3<unsigned int> padvoxel(x,y,z);
						padvoxel[0] = x + patchSize[0]/2;
						padvoxel[1] = y + patchSize[1]/2;
						padvoxel[2] = z + patchSize[2]/2;

						if (functorTypes[j] == ImageFunctorType) {

							for (unsigned int i = 0; i < functors.size(); ++i)
								functors[i].SetVoxel(padvoxel, j);

						}else if (functorTypes[j] == PatchFunctorType) {

							if (j != 0) {
								std::cerr << "Normalization set for non-intensity images??" << std::endl; exit(-1);
							}

							mxImageUtils::Crop3(inputImages[j], padvoxel, patchSize, patchBuffer, static_cast<double>(0));
							mxImageUtils::Normalize(patchBuffer, info.normalizationType);
							tmpFunctor->PreprocessInput(patchBuffer, j);
							for (unsigned int i = 0; i < functors.size(); ++i)
								functors[i].SetImage(patchBuffer,j);

						}else {
							std::cerr << "unrecognized functor type" << std::endl;
							exit(-1);
						}
					}

					vect3<unsigned int> dstvoxel;
					dstvoxel[0] = x + eff_offset[0];
					dstvoxel[1] = y + eff_offset[1];
					dstvoxel[2] = z + eff_offset[2];

					// multi-variate regression
					MultiDimensionRegressor<Dimension>::Regress2_weighted(forest.get(), functors, vectorImage(dstvoxel));

				} // end x
			} // end y
		} // end z
	}
}


} } }

#endif


