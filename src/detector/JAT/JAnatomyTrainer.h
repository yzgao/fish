//
//  JAnatomyTrainer.h
//  FISH
//
//  Created by Yaozong Gao on 07/09/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __JAnatomyTrainer_h__
#define __JAnatomyTrainer_h__

#include "forest/RandomForest.h"
#include "detector/JAT/JAT_annot_struct.h"
#include "detector/JAT/JAT_ini_struct.h"
#include "extern/NearestPointIndexer.h"
#include "common/aux_func.h"
#include "feature/CompositeFeatureSpace.h"
#include "feature/haar3d/Haar3DFeatureSpace.h"
#include "detector/JAT/JAnatomyMap.h"
#include "common/mxImageUtils.h"
#include "detector/JAT/JAT_feature_space.h"
#include "common/SmartCast.h"
#include "common/mathUtils.h"
#include "detector/JAT/JAT_detect.h"



namespace BRIC { namespace IDEA { namespace FISH {


// training module for joint regression-based anatomy detection

template <typename T>
class JAnatomyTrainer
{
private:

	JAT_annot_struct m_annot;
	JAT_ini_struct m_ini;

public:

	typedef MemoryAxisAlignedWeakLearner W;
	typedef SimpleRegressionStatisticsAggregator S;
	typedef Tree<W, S> TreeType;
	typedef Tree<W, S> ForestType;

	bool LoadINI(const char* ini_path);
	std::auto_ptr<IFeatureSpace> CreateFeatureSpace(Random& random);
	std::auto_ptr<TreeType> TrainTree(Random& random, IFeatureSpace* fs);

	std::string GetDetectorFolder() const;
	void CreateOutputFolder() const;
	bool SaveTree(const Tree<W, S>* tree, const IFeatureSpace* fs) const;
	bool SaveDetectionInfo() const;
	int GetTreeNumber() const;

	const JAT_ini_struct& GetINI();
	const JAT_annot_struct& GetAnnot();

private:

	// main functions
	void fill_train_data(MemoryDataCollection& train_data, Random& random, IFeatureSpace* fs);

	// sample voxels and compute offsets
	void sample_training_voxels(int img_idx, const mxImage<T>& intensity_image, int imgIdx, Random& random, std::vector< vect3<int> >& voxels, std::vector< double >& targets);

	// create feature spaces
	Haar3DFeatureSpace* create_haar3d_feature_space(const std::vector< vect3<unsigned int> >& filter_sizes, const vect3<unsigned int>& patch_size);
	JAT_feature_space* create_jat_feature_space();

	// voxel feature extraction
	void extract_voxel_features_L0(const mxImage<double>& crop_image, const vect3<int>& voxel, IFeatureSpace* fs, double* data_vector);
	void extract_voxel_features_L1(const mxImage<double>& crop_image, const vect3<int>& voxel, const std::map<int, vect3<double> >& constraint_lanmdarks, IFeatureSpace* fs, double* data_vector);

	// compute input images
	void crop_intensity_image(const mxImage<T>& image, const vect3<int>& sp, const vect3<int>& ep, mxImage<double>& cropped_image);
	void compute_constraint_landmarks(int imgIdx, std::map<int, vect3<double> >& constraintLandmarks);

	// utility functions
	bool read_and_resample_image(int img_idx, mxImage<T>& image);
	void bounding_box(const std::vector< vect3<int> >& voxels, vect3<int>& sp, vect3<int>& ep);

	// _debug functions
	void _save_sample_map(const mxImage<T>& resampled_image, int img_idx, const std::vector< vect3<int> >& sample_voxels, const char* outmap_path);
};

//////////////////////////////////////////////////////////////////////////


/************************************************************************/
/* feature space creation                                               */
/************************************************************************/

template <typename T>
Haar3DFeatureSpace* JAnatomyTrainer<T>::create_haar3d_feature_space(const std::vector< vect3<unsigned int> >& filter_sizes, const vect3<unsigned int>& patch_size)
{
	Haar3DFeatureSpace* fs = new Haar3DFeatureSpace;

	for (size_t i = 0; i < filter_sizes.size(); ++i) 
		fs->AddFilterSize(filter_sizes[i]);
	fs->SetPatchSize(patch_size);

	return fs;
}

template <typename T>
JAT_feature_space* JAnatomyTrainer<T>::create_jat_feature_space()
{
	JAT_feature_space* fs = new JAT_feature_space;

	fs->SetIntensityPatchSize(m_ini.intensity_patchsize.to<int>());
	fs->SetGeometricRules(m_ini.geometric_rules);

	for (size_t i = 0; i < m_ini.intensity_filtersizes.size(); ++i)
		fs->AddIntensityFilterSize(m_ini.intensity_filtersizes[i]);

	return fs;
}

template <typename T>
std::auto_ptr<IFeatureSpace> JAnatomyTrainer<T>::CreateFeatureSpace(Random& random)
{
	// level = 0 with only appearance features
	if (m_ini.level == 0)
	{
		Haar3DFeatureSpace* fs = create_haar3d_feature_space(m_ini.intensity_filtersizes, m_ini.intensity_patchsize);
		fs->RandomizeFeatureSpace_twoblocks(random, m_ini.forest_params.treeParameters.numOfRandomWeakLearners);
		return std::auto_ptr<IFeatureSpace>(fs);
	}
	else
	{
		JAT_feature_space* fs = create_jat_feature_space();
		fs->RandomizeFeatureSpace(random, m_ini.forest_params.treeParameters.numOfRandomWeakLearners);
		return std::auto_ptr<IFeatureSpace>(fs);
	}
}


/************************************************************************/
/* feature extraction                                                   */
/************************************************************************/

template <typename T>
void JAnatomyTrainer<T>::extract_voxel_features_L0(const mxImage<double>& integral_image, const vect3<int>& voxel, IFeatureSpace* fs, double* data_vector)
{
	int feature_num = fs->GetFeatureNumber();

	std::auto_ptr<IImageFeatureFunctor> thread_functor(fs->CreateImageFeatureFunctor());
	thread_functor->SetImage(integral_image, 0);
	thread_functor->SetVoxel(voxel.to<unsigned int>(), 0);

	for (int featIdx = 0; featIdx < feature_num; ++featIdx)
		data_vector[featIdx] = (*thread_functor)(featIdx);
}

template <typename T>
void JAnatomyTrainer<T>::extract_voxel_features_L1(const mxImage<double>& integral_image, const vect3<int>& voxel, const std::map<int,vect3<double> >& constraint_landmarks, IFeatureSpace* fs, double* data_vector)
{
	int feature_num = fs->GetFeatureNumber();

	std::auto_ptr<JAT_image_feature_functor> thread_functor( static_cast<JAT_image_feature_functor*>(fs->CreateImageFeatureFunctor()) );
	thread_functor->SetImage(integral_image, 0);
	thread_functor->SetVoxel(voxel.to<unsigned int>(), 0);
	thread_functor->SetConstraintLandmarks(&constraint_landmarks);

	for (int featIdx = 0; featIdx < feature_num; ++featIdx)
		data_vector[featIdx] = (*thread_functor)(featIdx);
}

template <typename T>
bool JAnatomyTrainer<T>::read_and_resample_image(int img_idx, mxImage<T>& image)
{
	mxImage<T> orig_image;
	if (!ImageHelper::ReadImage<T,T>(m_annot.image_paths[img_idx].c_str(), orig_image)) 
	{
		std::cerr << "fail to load image from " << m_annot.image_paths[img_idx] << std::endl;
		return false;
	}

	mxImageUtils::Resample(orig_image, m_ini.spacing, image, IT_LINEAR_INTERPOLATION);
	return true;
}

template <typename T>
void JAnatomyTrainer<T>::bounding_box(const std::vector< vect3<int> >& voxels, vect3<int>& sp, vect3<int>& ep)
{
	sp[0] = sp[1] = sp[2] = std::numeric_limits<int>::max();
	ep[0] = ep[1] = ep[2] = -std::numeric_limits<int>::max();

	for (size_t i = 0; i < voxels.size(); ++i)
	{
		const vect3<int>& voxel = voxels[i];
		for (int j = 0; j < 3; ++j)
		{
			if (voxel[j] < sp[j])
				sp[j] = voxel[j];

			if (voxel[j] > ep[j])
				ep[j] = voxel[j];
		}
	}
}

template <typename T>
void JAnatomyTrainer<T>::crop_intensity_image(const mxImage<T>& image, const vect3<int>& sp, const vect3<int>& ep, mxImage<double>& cropped_image)
{
	vect3<int> req_sp = sp, req_ep = ep;
	vect3<unsigned int> patch_size = m_ini.intensity_patchsize;
	mxImageUtils::PadBox(req_sp, req_ep, patch_size);
	mxImageUtils::Crop2(image, req_sp, req_ep, cropped_image);
}

template <typename T>
void JAnatomyTrainer<T>::sample_training_voxels(int img_idx, const mxImage<T>& intensity_image, int imgIdx, Random& random, std::vector< vect3<int> >& voxels, std::vector<double>& targets)
{
	// sample voxels
	if (m_ini.body_mask)
	{
		mxImage<unsigned char> body_mask;
		mxImageUtils::ExtractBodyFromCT<T,T>(intensity_image, body_mask, &intensity_image);

		if (m_ini.sample_strategy == JAT_SAMPLE_UNIFORM)
			m_annot.get_random_samples(img_idx, intensity_image, m_ini.sample_radius, random, m_ini.sample_number, voxels, &body_mask);
		else if (m_ini.sample_strategy == JAT_SAMPLE_SPHERICAL)
			m_annot.get_spherical_random_samples(img_idx, intensity_image, m_ini.sample_radius, random, m_ini.sample_number, voxels, &body_mask);
		else
			err_message("unrecognized sample strategy type");
	}
	else
	{
		if (m_ini.sample_strategy == JAT_SAMPLE_UNIFORM)
			m_annot.get_random_samples(img_idx, intensity_image, m_ini.sample_radius, random, m_ini.sample_number, voxels, NULL);
		else if (m_ini.sample_strategy == JAT_SAMPLE_SPHERICAL)
			m_annot.get_spherical_random_samples(img_idx, intensity_image, m_ini.sample_radius, random, m_ini.sample_number, voxels, NULL);
		else
			err_message("unrecognized sample strategy type");
	}
	
	// compute offsets
	size_t current_target_size = targets.size();
	int target_dim = m_annot.get_offset_dimension();
	targets.resize(current_target_size + target_dim * voxels.size());

	for (size_t i = 0; i < voxels.size(); ++i)
		m_annot.get_voxel_offsets(img_idx, intensity_image, voxels[i], &targets[current_target_size + target_dim * i]);
}


template <typename T>
void JAnatomyTrainer<T>::compute_constraint_landmarks(int imgIdx, std::map<int, vect3<double> >& constraintLandmarks)
{
	JAT_detect<T> detector;

	const std::string& image_path = m_annot.image_paths[imgIdx];
	if (!detector.LoadImage(image_path.c_str()))
		err_message("fail to load input image");

	detector.SetTreeFusionType(SimpleRegressor::InverseWeighted);

	std::set<int> landmark_set;
	for (size_t i = 0; i < m_ini.geometric_rules.size(); ++i)
	{
		int id1 = m_ini.geometric_rules[i].id1;
		int id2 = m_ini.geometric_rules[i].id2;

		if (id1 > 0)
			landmark_set.insert(id1);

		if (id2 > 0)
			landmark_set.insert(id2);
	}

	std::set<int>::const_iterator it = landmark_set.begin();
	while (it != landmark_set.end())
	{
		std::cout << "[DOING] Detecting landmark " << *it << std::endl;
		std::string config_path = m_ini.root_folder + stringUtils::Slash() + "config" + stringUtils::num2str(*it) + ".ini";

		JAT_detect_config config;
		if (!config.read(config_path.c_str()))
			err_message("fail to load JAT config file");

		JAT_anatomy_positions tmp_positions;
		detector.DetectAnatomy(m_ini.root_folder.c_str(), config, tmp_positions);

		vect3<double> detect_lm = tmp_positions.get_lm_world(0, 0);
		std::cout << "[DONE] landmark " << *it << ": " << detect_lm << std::endl;

		constraintLandmarks[*it] = detect_lm;
		++it;
	}
}

template <typename T>
void JAnatomyTrainer<T>::fill_train_data(MemoryDataCollection& train_data, Random& random, IFeatureSpace* fs)
{
	std::vector<double>& data_vector = train_data.GetDataVector();
	std::vector<double>& target_vector = train_data.GetTargetVector();

	// get basic information about the training data
	int feature_num = fs->GetFeatureNumber();
	int train_image_num = static_cast<int>( m_annot.image_paths.size() );

	// total number of training samples
	size_t total_sample_num = 0;

	// extract features from each image
	for (int imgIdx = 0; imgIdx < train_image_num; ++imgIdx)
	{
		// get intensity image
		std::cout << "Loading intensity image ( " << imgIdx+1 << " / " << train_image_num << " ): " << m_annot.image_paths[imgIdx] << std::endl;
		mxImage<T> intensity_image;
		if (!read_and_resample_image(imgIdx, intensity_image))
			err_message( (std::string("fail to read image ") + stringUtils::num2str(imgIdx) + std::string("\n")).c_str() );

		std::vector< vect3<int> > sample_voxels;
		sample_training_voxels(imgIdx, intensity_image, imgIdx, random, sample_voxels, target_vector);
		//_save_sample_map(intensity_image, imgIdx, sample_voxels, "C:\\Users\\Yaozong\\Desktop\\test\\sample.mha");
	
		if (m_ini.level == 0)
		{
			// bounding box
			vect3<int> sp, ep;
			bounding_box(sample_voxels, sp, ep);

			// crop a local region with patch size padding
			mxImage<double> cropped_image;
			crop_intensity_image(intensity_image, sp, ep, cropped_image);
			std::cout << "[DONE] Cropping the intensity image by bounding box" << std::endl;

			// Haar Preprocessing
			std::auto_ptr<IImageFeatureFunctor> functor(fs->CreateImageFeatureFunctor());
			functor->PreprocessInput(cropped_image, 0);

			// feature extraction
			size_t old_data_size = data_vector.size();
			try {
				data_vector.resize( old_data_size + feature_num * sample_voxels.size() );
			}
			catch (std::exception&) {
				err_message("memory allocation fails");
			}

			// voxel-wise feature extraction in parallel
			#pragma omp parallel for
			for (int voxelIdx = 0; voxelIdx < static_cast<int>(sample_voxels.size()); ++voxelIdx)
			{
				vect3<int> voxel = sample_voxels[voxelIdx];
				vect3<int> adjusted_voxel;
				for (int k = 0; k < 3; ++k)
					adjusted_voxel[k] = voxel[k] - sp[k] + m_ini.intensity_patchsize[k] / 2;

				size_t mem_p = old_data_size + voxelIdx * feature_num;
				extract_voxel_features_L0(cropped_image, adjusted_voxel, fs, &(data_vector[mem_p]));
			}
			std::cout << "[DONE] feature extraction of training samples" << std::endl;
		}
		else
		{
			// bounding box
			vect3<int> sp, ep;
			bounding_box(sample_voxels, sp, ep);

			// 1) crop an intensity image with patch size padding
			mxImage<double> cropped_image;
			crop_intensity_image(intensity_image, sp, ep, cropped_image);
			std::cout << "[DONE] Cropping the intensity image by bounding box" << std::endl;

			// 2) compute constraint landmarks
			std::map< int, vect3<double> > constraint_landmarks;
			if (m_ini.geometric_rules.size() > 0) {
				compute_constraint_landmarks(imgIdx, constraint_landmarks);
				std::cout << "[DONE] Computing the constraint landmarks" << std::endl;
			}
			else {
				std::cout << "[SKIP] Computing the constraint landmarks" << std::endl;
			}

			// 3) Haar Preprocessing
			std::auto_ptr<IImageFeatureFunctor> functor(fs->CreateImageFeatureFunctor());
			functor->PreprocessInput(cropped_image, 0);

			// feature extraction
			size_t old_data_size = data_vector.size();
			try {
				data_vector.resize(old_data_size + feature_num * sample_voxels.size());
			}
			catch (std::exception&) {
				err_message("memory allocation fails");
			}

			// voxel-wise feature extraction in parallel
			#pragma omp parallel for
			for (int voxelIdx = 0; voxelIdx < static_cast<int>(sample_voxels.size()); ++voxelIdx)
			{
				vect3<int> voxel = sample_voxels[voxelIdx];
				vect3<int> adjusted_voxel;
				for (int k = 0; k < 3; ++k)
					adjusted_voxel[k] = voxel[k] - sp[k] + m_ini.intensity_patchsize[k] / 2;

				size_t mem_p = old_data_size + voxelIdx * feature_num;
				extract_voxel_features_L1(cropped_image, adjusted_voxel, constraint_landmarks, fs, &(data_vector[mem_p]));
			}
			std::cout << "[DONE] feature extraction of training samples" << std::endl;
		}

		// increment number of total training sample
		total_sample_num += sample_voxels.size();
		std::cout << std::endl;
	}

	train_data.SetSampleNumber(static_cast<unsigned int>(total_sample_num));
	train_data.SetFeatureNumber(feature_num);
	train_data.SetTargetDim(m_annot.get_offset_dimension());
}


/************************************************************************/
/* public interfaces                                                    */
/************************************************************************/

template <typename T>
std::auto_ptr< Tree<MemoryAxisAlignedWeakLearner, SimpleRegressionStatisticsAggregator> >
JAnatomyTrainer<T>::TrainTree(Random& random, IFeatureSpace* fs)
{
	MemoryDataCollection train_data;
	fill_train_data(train_data, random, fs);

	const TreeTrainingParameters& tree_params = m_ini.forest_params.treeParameters;

	unsigned int feature_num = fs->GetFeatureNumber();
	unsigned int target_dim = train_data.GetTargetDimension();

	assert_message(feature_num != 0 && target_dim != 0, "feature number and target dimension must be > 0");

	SimpleRegressionExTrainingContext train_context(feature_num, target_dim);
	return TreeTrainer<W, S>::TrainTree(train_context, tree_params, &train_data, random);
}

template <typename T>
std::string JAnatomyTrainer<T>::GetDetectorFolder() const
{
	std::string detectorFolder;
	if (m_ini.level == 0)
		detectorFolder = m_ini.root_folder + stringUtils::Slash() + m_ini.detector_name;
	else
		detectorFolder = m_ini.root_folder + stringUtils::Slash() + m_ini.detector_name + "_C" + stringUtils::num2str(m_ini.level);

	return detectorFolder;
}

template <typename T>
const JAT_ini_struct& JAnatomyTrainer<T>::GetINI()
{
	return m_ini;
}

template <typename T>
const JAT_annot_struct& JAnatomyTrainer<T>::GetAnnot()
{
	return m_annot;
}

template <typename T>
bool JAnatomyTrainer<T>::LoadINI(const char* ini_path)
{
	if (!m_ini.parse(ini_path))
	{
		std::cerr << "fail to parse INI file at " << ini_path << std::endl;
		return false;
	}

	if (!m_annot.parse(m_ini.annot_path.c_str()))
	{
		std::cerr << "fail to parse annotation file at " << m_ini.annot_path << std::endl;
		return false;
	}

	return true;
}

template <typename T>
void JAnatomyTrainer<T>::CreateOutputFolder() const
{
	std::string detector_folder = GetDetectorFolder();
	if (!boost::filesystem::is_directory(detector_folder))
		boost::filesystem::create_directories(detector_folder);
}

template <typename T>
bool JAnatomyTrainer<T>::SaveTree(const Tree<W, S>* tree, const IFeatureSpace* fs) const
{
	std::string detector_folder = GetDetectorFolder();

	char tree_file_path[2048] = { 0 };
	char feature_space_path[2048] = { 0 };

	// tree name & feature space name
	sprintf(tree_file_path, "%s%sTree_R%d_%s.bin", detector_folder.c_str(), stringUtils::Slash().c_str(), m_ini.scale, stringUtils::DoubleToHexString(tree->GetUniqueID()).c_str());
	sprintf(feature_space_path, "%s%sTree_R%d_%s.fd", detector_folder.c_str(), stringUtils::Slash().c_str(), m_ini.scale, stringUtils::DoubleToHexString(tree->GetUniqueID()).c_str());

	// tree output stream
	std::ofstream tree_out(tree_file_path, std::ios::binary);
	if (!tree_out) {
		std::cerr << "fails to open " << tree_file_path << " for write" << std::endl;
		return false;
	}
	tree->Serialize(tree_out);
	tree_out.close();

	// feature space output stream
	std::ofstream featurespace_out(feature_space_path, std::ios::binary);
	if (!featurespace_out) {
		std::cerr << "fails to open " << feature_space_path << " for write" << std::endl;
		return false;
	}
	fs->Serialize(featurespace_out);
	featurespace_out.close();

	return true;
}

template <typename T>
bool JAnatomyTrainer<T>::SaveDetectionInfo() const
{
	boost::property_tree::ptree pt;

	char buffer[100] = { 0 };
	sprintf(buffer, "%.3f %.3f %.3f", m_ini.spacing[0], m_ini.spacing[1], m_ini.spacing[2]);
	pt.put("Info.Resolution", buffer);

	sprintf(buffer, "%.3f %.3f %.3f", m_ini.sample_radius[0], m_ini.sample_radius[1], m_ini.sample_radius[2]);
	pt.put("Info.Radius", buffer);

	pt.put("Info.BodyMask", static_cast<int>(m_ini.body_mask == true));

	// write out constraint landmarks
	if (m_ini.geometric_rules.size() > 0)
	{
		std::set<int> landmark_set;
		for (size_t i = 0; i < m_ini.geometric_rules.size(); ++i)
		{
			int id1 = m_ini.geometric_rules[i].id1;
			int id2 = m_ini.geometric_rules[i].id2;

			if (id1 > 0)
				landmark_set.insert(id1);

			if (id2 > 0)
				landmark_set.insert(id2);
		}

		std::string tmp_str;
		std::set<int>::const_iterator it = landmark_set.begin();
		while (it != landmark_set.end())
		{
			if (it != landmark_set.begin())
				tmp_str += " ";
			tmp_str += stringUtils::num2str(*it);
			++it;
		}

		pt.put("Info.ConstraintLandmarks", tmp_str);
	}

	// write out the anatomy types and ids
	std::string anatomy_ids;
	for (size_t i = 0; i < m_annot.positions.ids.size(); ++i)
	{
		if (i != 0)
			anatomy_ids += " ";

		anatomy_ids += stringUtils::num2str( m_annot.positions.ids[i] );
	}
	pt.put("Info.IDs", anatomy_ids);

	// write out the INI file
	std::string detect_folder = GetDetectorFolder();
	char path[2048] = { 0 };
	sprintf(path, "%s%sinfo_R%d.ini", detect_folder.c_str(), stringUtils::Slash().c_str(), m_ini.scale);

	try{
		boost::property_tree::ini_parser::write_ini(path, pt);
	}
	catch (boost::property_tree::ptree_error& /*error*/) {
		// commented to avoid error message when multiple trees are trained in parallel
		// std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}

template <typename T>
int JAnatomyTrainer<T>::GetTreeNumber() const
{
	return m_ini.forest_params.numTrees;
}

template <typename T>
void JAnatomyTrainer<T>::_save_sample_map(const mxImage<T>& resampled_image, int img_idx, const std::vector< vect3<int> >& sample_voxels, const char* outmap_path)
{
	mxImage < unsigned char > sample_image;
	sample_image.SetImageSize(resampled_image.GetImageSize());
	sample_image.CopyImageInfo(resampled_image);
	sample_image.Fill(0);

	for (size_t i = 0; i < sample_voxels.size(); ++i)
	{
		int x = static_cast<int>(sample_voxels[i][0] + 0.5);
		int y = static_cast<int>(sample_voxels[i][1] + 0.5);
		int z = static_cast<int>(sample_voxels[i][2] + 0.5);
		if (!sample_image.PtInImage(x, y, z))
		{
			std::cerr << "one sample voxel (" << x << "," << y << "," << z << ") is out of image domain" << std::endl;
			exit(-1);
		}

		sample_image(x, y, z) += 1;
		//sample_image(x, y, z) = 1;
	}

	if (!ImageHelper::WriteImage<T, unsigned char>(sample_image, outmap_path))
	{
		std::cerr << "fail to write sample image to " << outmap_path << std::endl;
		exit(-1);
	}
}


} } }

#endif