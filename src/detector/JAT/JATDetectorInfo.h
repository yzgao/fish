
#ifndef __JATDetectorInfo_h__
#define __JATDetectorInfo_h__

namespace BRIC { namespace IDEA { namespace FISH {

struct JATDetectorInfo
{
public:
	vect3<double> spacing;
	vect3<double> sample_radius;
	bool body_mask;
	std::vector<std::string> constraint_landmarks;
	std::vector<int> anatomy_ids;

	void clear()
	{
		spacing = vect3<double>(0, 0, 0);
		sample_radius = vect3<double>(0,0,0);
		body_mask = false;
		anatomy_ids.clear();
	}
};


} } }

#endif