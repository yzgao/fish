//
//  JAT_annot_struct.h
//  FISH
//
//  Created by Yaozong Gao on 06/16/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __JAT_annot_struct_h__
#define __JAT_annot_struct_h__

#include "common/aux_func.h"
#include "common/stringUtils.h"
#include "common/mxImageUtils.h"
#include "common/Random.h"
#include "common/SmartCast.h"

#include "extern/ImageHelper.h"
#include "mesh/PDM.h"


namespace BRIC { namespace IDEA { namespace FISH {


/************************************************************************/
/* JAT annotation positions for single or multiple image(s)             */
/************************************************************************/

typedef vect3<double> JAT_lm_type;

class JAT_anatomy_positions
{
public:

	std::vector< int > ids;
	std::vector< std::vector<JAT_lm_type> > lm_worlds;
	int image_num;

public:

	// get regression target dimension
	int get_offset_dimension() const;

	// helper functions
	JAT_lm_type& get_lm_world(int imgIdx, int lm_idx);
	const JAT_lm_type& get_lm_world(int imgIdx, int lm_idx) const;
	void set_lm_world(int imgIdx, int lm_idx, const JAT_lm_type& lm_world);

	// get offsets, sample mask, random samples
	template <typename T> void get_voxel_offsets(int imgIdx, const mxImage<T>& image, const vect3<int>& voxel, double* offsets) const;
	template <typename T> void get_sample_map(int imgIdx, const mxImage<T>& image, vect3<double> radius, MaskImage& sample_map, const MaskImage* mask = NULL) const;
	template <typename T> void get_sample_map(int imgIdx, const mxImage<T>& image, int lm_idx, vect3<double> radius, MaskImage& sample_map, const MaskImage* mask = NULL) const;
	template <typename T> void get_random_samples(int imgIdx, const mxImage<T>& image, vect3<double> radius, Random& random, int sample_num, std::vector< vect3<int> >& voxels, const MaskImage* mask = NULL) const;

	template <typename T> void get_spherical_random_samples(int imgIdx, const mxImage<T>& image, vect3<double> radius, Random& random, int sample_num, std::vector< vect3<int> >& voxels, const MaskImage* mask = NULL) const;

	template <typename T> void get_bounding_box(int imgIdx, const mxImage<T>& image, vect3<double> radius, vect3<int>& sp, vect3<int>& ep) const;
	template <typename T> void get_bounding_box(int imgIdx, const mxImage<T>& image, int lm_idx, vect3<double> radius, vect3<int>& sp, vect3<int>& ep) const;

	// combine detections results on different images
	void aggregate(const JAT_anatomy_positions& positions);

	// combine detection results on the same image
	void fuse(const JAT_anatomy_positions& positions);

	// from PDM and to PDM
	static JAT_anatomy_positions create_from_landmarks(const std::vector<float>& verts, std::vector<int> arg_ids = std::vector<int>());
	PDM create_pdm(int imgIdx);
	
	// io
	bool save_to_file(const std::vector<std::string>& image_paths, const char* output_file) const;

public:

	// get offset from one voxel to the target anatomy
	template <typename T> void get_lm_offset(int imgIdx, const mxImage<T>& image, int lmIdx, const vect3<int>& voxel, double* offsets) const;

	// find voxel candidates for random sampling
	template <typename T> void mark_lm_samples(int imgIdx, const mxImage<T>& image, int lmIdx, vect3<double> radius, mxImage<unsigned char>& sample_map) const;
	
	// find bounding box for each anatomy
	template <typename T> void get_lm_bounding_box(int imgIdx, const mxImage<T>& image, int lmIdx, vect3<double> radius, vect3<int>& sp, vect3<int>& ep) const;

	// get a random spherical sample around landmark
	template <typename T> vect3<int> get_a_spherical_sample_from_lm(const mxImage<T>& image, const vect3<double>& lm, double max_radius, Random& random) const;
};


/************************************************************************/
/* JAT annotation structure (the information of annotation file)		*/
/************************************************************************/

class JAT_annot_struct
{
public:

	// attributes
	std::vector< std::string > image_paths;			// image paths
	JAT_anatomy_positions positions;				// anatomy positions

public:

	// parse JAT_annot_struct from annotation file 
	bool parse(const char* path);
	bool save_to_file(const char* path) const;

	// adapted functions	
	int get_offset_dimension() const;
	template <typename T> void get_voxel_offsets(int img_idx, const mxImage<T>& image, const vect3<int>& voxel, double* offsets) const;

	// get uniformly random samples
	template <typename T> 
	void get_random_samples(int img_idx, const mxImage<T>& image, vect3<double> radius, Random& random, int sample_num, std::vector< vect3<int> >& voxels, const MaskImage* mask = NULL) const;

	// get spherical random samples
	template <typename T>
	void get_spherical_random_samples(int img_idx, const mxImage<T>& image, vect3<double> radius, Random& radnom, int sample_num, std::vector< vect3<int> >& voxels, const MaskImage* mask = NULL) const;

	// get sample mask image
	template <typename T> void get_sample_map(int img_idx, const mxImage<T>& image, vect3<double> radius, MaskImage& sample_map, const MaskImage* mask = NULL) const;

	// combine detection results of different images
	void aggregate(const JAT_annot_struct& annot);

	// print out basic info
	void print_out_basic(std::ostream& os) const;

private:

	vect3<double> parse_lm(const std::vector<std::string>& tokens);
};



/************************************************************************/
/* JAT_anatomy_positions implementations                                */
/************************************************************************/

JAT_lm_type& JAT_anatomy_positions::get_lm_world(int img_idx, int lm_idx)
{
	assert_message(img_idx < image_num && img_idx >= 0, "image index out of bound");
	assert_message(lm_idx < ids.size() && lm_idx >= 0, "landmark index out of bound");

	return lm_worlds[img_idx][lm_idx];
}

const JAT_lm_type& JAT_anatomy_positions::get_lm_world(int img_idx, int lm_idx) const
{
	assert_message(img_idx < image_num && img_idx >= 0, "image index out of bound");
	assert_message(lm_idx < ids.size() && lm_idx >= 0, "landmark index out of bound");

	return lm_worlds[img_idx][lm_idx];
}

void JAT_anatomy_positions::set_lm_world(int img_idx, int lm_idx, const JAT_lm_type& lm_world)
{
	assert_message(img_idx < image_num && img_idx >= 0, "image index out of bound");
	assert_message(lm_idx < ids.size() && lm_idx >= 0, "landmark index out of bound");

	lm_worlds[img_idx][lm_idx] = lm_world;
}

int JAT_anatomy_positions::get_offset_dimension() const
{
	return ids.size() * 3;
}

template <typename T> 
void JAT_anatomy_positions::get_lm_offset(int img_idx, const mxImage<T>& image, int lm_idx, const vect3<int>& voxel, double* offsets) const
{
	vect3<double> lm = get_lm_world(img_idx, lm_idx);
	mxImageUtils::World2Voxel(image, lm);

	for (int i = 0; i < 3; ++i)
		offsets[i] = lm[i] - voxel[i];
}

template <typename T> 
void JAT_anatomy_positions::get_voxel_offsets(int img_idx, const mxImage<T>& image, const vect3<int>& voxel, double* offsets) const
{
	assert_message((img_idx >= 0 && img_idx < image_num), "invalid image index");

	for (size_t i = 0; i < ids.size(); ++i)
	{
		int index = static_cast<int>(i);
		get_lm_offset(img_idx, image, index, voxel, offsets);
		offsets += 3;
	}
}

template <typename T>
void JAT_anatomy_positions::mark_lm_samples(int img_idx, const mxImage<T>& image, int lm_idx, vect3<double> radius, MaskImage& sample_map) const
{
	vect3<unsigned int> size = image.GetImageSize();
	vect3<double> spacing = image.GetSpacing();

	vect3<double> lm_world = get_lm_world(img_idx, lm_idx);
	vect3<int> lm_voxel;
	mxImageUtils::World2Voxel(image, lm_world, lm_voxel);

	vect3<double> voxel_radius;
	for (int i = 0; i < 3; ++i)
		voxel_radius[i] = radius[i] / spacing[i];
	
	vect3<int> sp, ep;
	for (int i = 0; i < 3; ++i)
	{
		sp[i] = static_cast<int>(lm_voxel[i] - voxel_radius[i] + 0.5);
		ep[i] = static_cast<int>(lm_voxel[i] + voxel_radius[i] + 0.5);

		sp[i] = truncate<int>(sp[i], 0, size[i] - 1);
		ep[i] = truncate<int>(ep[i], 0, size[i] - 1);

		if (ep[i] < sp[i])
			err_message("invalid sample region");
	}

	for (int z = sp[2]; z <= ep[2]; ++z)
	{
		for (int y = sp[1]; y <= ep[1]; ++y)
		{
			for (int x = sp[0]; x <= ep[0]; ++x)
			{
				sample_map(x, y, z) = 255;
			}
		}
	}
}

template <typename T>
void JAT_anatomy_positions::get_sample_map(int img_idx, const mxImage<T>& image, vect3<double> radius, MaskImage& sample_map, const MaskImage* mask) const
{
	vect3<unsigned int> image_size = image.GetImageSize();
	sample_map.CopyImageInfo(image);
	sample_map.SetImageSize(image_size);
	sample_map.Fill(0);

	for (size_t i = 0; i < ids.size(); ++i)
	{
		int index = static_cast<int>(i);
		mark_lm_samples(img_idx, image, index, radius, sample_map);
	}

	if (mask != NULL)
	{
		#pragma omp parallel for schedule(dynamic)
		for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
		{
			for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
			{
				for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
				{
					if ((*mask)(x, y, z) == 0)
						sample_map(x, y, z) = 0;
				}
			}
		}
	}
}

template <typename T>
void JAT_anatomy_positions::get_sample_map(int img_idx, const mxImage<T>& image, int lm_idx, vect3<double> radius, MaskImage& sample_map, const MaskImage* mask) const
{
	vect3<unsigned int> image_size = image.GetImageSize();
	sample_map.CopyImageInfo(image);
	sample_map.SetImageSize(image_size);
	sample_map.Fill(0);

	mark_lm_samples(img_idx, image, lm_idx, radius, sample_map);

	if (mask != NULL)
	{
		#pragma omp parallel for schedule(dynamic)
		for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
		{
			for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
			{
				for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
				{
					if ((*mask)(x, y, z) == 0)
						sample_map(x, y, z) = 0;
				}
			}
		}
	}
}

template <typename T>
void JAT_anatomy_positions::get_random_samples(int img_idx, const mxImage<T>& image, vect3<double> radius, Random& random, int sample_num, std::vector< vect3<int> >& voxels, const MaskImage* mask) const
{
	std::vector < int > sample_indexs;

	MaskImage sample_map;
	get_sample_map(img_idx, image, radius, sample_map, mask);

	unsigned char* raw_data = sample_map.GetData();

	vect3<unsigned int> size = sample_map.GetImageSize();
	sample_indexs.reserve(size[0] * size[1] * size[2]);

	for (int z = 0; z < static_cast<int>(size[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(size[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(size[0]); ++x)
			{
				int index = z * size[0] * size[1] + y * size[0] + x;
				if (raw_data[index])
					sample_indexs.push_back(index);
			}
		}
	}

	for (int i = 0; i < sample_num; ++i)
	{
		size_t index = random.Next<size_t>(0, sample_indexs.size() - 1);
		vect3<int> voxel_index;
		voxel_index[2] = sample_indexs[index] / (size[0] * size[1]);
		voxel_index[1] = sample_indexs[index] % (size[0] * size[1]) / size[0];
		voxel_index[0] = sample_indexs[index] % (size[0] * size[1]) % size[0];

		voxels.push_back(voxel_index);
	}
}

template <typename T> 
vect3<int> JAT_anatomy_positions::get_a_spherical_sample_from_lm(const mxImage<T>& image, const vect3<double>& lm, double max_radius, Random& random) const
{
	double radius = random.NextDouble(0, max_radius);

	vect3<double> ptWorld;

	double z = random.NextDouble(-1, 1);
	double t = random.NextDouble(0, 2 * M_PI);

	double r = sqrt(1 - z*z);
	double x = r * cos(t);
	double y = r * sin(t);

	ptWorld[0] = lm[0] + x * radius;
	ptWorld[1] = lm[1] + y * radius;
	ptWorld[2] = lm[2] + z * radius;

	vect3<int> ptVoxel;
	mxImageUtils::World2Voxel(image, ptWorld, ptVoxel);

	return ptVoxel;
}

template <typename T> 
void JAT_anatomy_positions::get_spherical_random_samples(int img_idx, const mxImage<T>& image, vect3<double> radius, Random& random, int sample_num, std::vector< vect3<int> >& voxels, const MaskImage* mask) const
{
	double max_radius = std::max(radius[0], std::max(radius[1], radius[2]));

	for (int i = 0; i < sample_num; ++i)
	{
		size_t lm_idx = random.Next<size_t>(0, ids.size()-1);

		vect3<double> lm_world = get_lm_world(img_idx, lm_idx);
		vect3<int> bound_sp, bound_ep;
		get_lm_bounding_box(img_idx, image, lm_idx, radius, bound_sp, bound_ep);

		while (true)
		{
			vect3<int> sample_voxel = get_a_spherical_sample_from_lm(image, lm_world, max_radius, random);

			// ensure within the bounding box
			bool isValid = true;
			for (int j = 0; j < 3; ++j)
			{
				if (sample_voxel[j] < bound_sp[j] || sample_voxel[j] > bound_ep[j])
				{
					isValid = false;
					break;
				}
			}

			if (!isValid)
				continue;

			if (!image.PtInImage(sample_voxel[0], sample_voxel[1], sample_voxel[2]))
				continue;

			if (mask != NULL && (*mask)(sample_voxel[0], sample_voxel[1], sample_voxel[2]) == 0)
				continue;

			voxels.push_back(sample_voxel);
			break;
		}
	}
}

template <typename T>
void JAT_anatomy_positions::get_lm_bounding_box(int img_idx, const mxImage<T>& image, int lm_idx, vect3<double> radius, vect3<int>& sp, vect3<int>& ep) const
{
	vect3<double> lm_world = get_lm_world(img_idx, lm_idx);

	vect3<int> lm_voxel;
	mxImageUtils::World2Voxel(image, lm_world, lm_voxel);

	vect3<double> spacing = image.GetSpacing();
	for (int i = 0; i < 3; ++i)
	{
		double voxel_radius = radius[i] / spacing[i];
		sp[i] = SmartCast<int>::Cast(lm_voxel[i] - voxel_radius);
		ep[i] = SmartCast<int>::Cast(lm_voxel[i] + voxel_radius);
	}
}


template <typename T> 
void JAT_anatomy_positions::get_bounding_box(int img_idx, const mxImage<T>& image, vect3<double> radius, vect3<int>& sp, vect3<int>& ep) const
{
	sp[0] = sp[1] = sp[2] = std::numeric_limits<int>::max();
	ep[0] = ep[1] = ep[2] = -std::numeric_limits<int>::max();

	vect3<unsigned int> image_size = image.GetImageSize();

	for (size_t i = 0; i < ids.size(); ++i)
	{
		int lm_idx = static_cast<int>(i);

		vect3<int> temp_sp, temp_ep;
		get_lm_bounding_box(img_idx, image, lm_idx, radius, temp_sp, temp_ep);

		for (int j = 0; j < 3; ++j)
		{
			if (temp_sp[j] < sp[j])
				sp[j] = temp_sp[j];
			if (temp_ep[j] > ep[j])
				ep[j] = temp_ep[j];

			truncate( sp[j], 0, static_cast<int>(image_size[j]-1) );
			truncate( ep[j], 0, static_cast<int>(image_size[j]-1) );
		}
	}
}

template <typename T> 
void JAT_anatomy_positions::get_bounding_box(int img_idx, const mxImage<T>& image, int lm_idx, vect3<double> radius, vect3<int>& sp, vect3<int>& ep) const
{
	get_lm_bounding_box(img_idx, image, lm_idx, radius, sp, ep);
}

void JAT_anatomy_positions::aggregate(const JAT_anatomy_positions& positions)
{
	bool empty = (ids.size() == 0) || (image_num == 0);
	if ( empty )
	{
		ids = positions.ids;
		lm_worlds = positions.lm_worlds;
		image_num = positions.image_num;
	}
	else
	{
		assert_message( positions.ids.size() == ids.size(), "id number mismatch");
		image_num += positions.image_num;

		size_t old_size = lm_worlds.size();
		lm_worlds.resize(old_size + positions.lm_worlds.size());
		for (size_t i = 0; i < positions.lm_worlds.size(); ++i)
			lm_worlds[i + old_size] = positions.lm_worlds[i];
	}
}

bool JAT_anatomy_positions::save_to_file(const std::vector<std::string>& image_paths, const char* output_file) const
{
	assert_message(static_cast<int>(image_paths.size()) >= image_num, "not sufficient image paths provided");

	FILE* fp = fopen(output_file, "w");
	if (fp == NULL)
	{
		std::cerr << "fail to open file for write" << std::endl;
		return false;
	}

	int lm_num = static_cast<int>(ids.size());
	fprintf(fp, "%d\t%d\n", image_num, lm_num);

	for (int i = 0; i < image_num; ++i)
	{
		fprintf(fp, "%s\n", image_paths[i].c_str());

		for (int j = 0; j < lm_num; ++j)
		{
			vect3<double> lm_world = get_lm_world(i, j);
			fprintf(fp, "LM\t%d\t0\t0\t0\t%lf\t%lf\t%lf\n", ids[j], lm_world[0], lm_world[1], lm_world[2]);
		}
	}

	fclose(fp);
	return true;
}

JAT_anatomy_positions JAT_anatomy_positions::create_from_landmarks(const std::vector<float>& verts, std::vector<int> arg_ids)
{
	JAT_anatomy_positions pos;

	int lm_num = verts.size() / 3;

	if (arg_ids.size() > 0)
		assert_message(lm_num == arg_ids.size(), "landmark number mismatch");

	if (arg_ids.size() == 0)
		pos.ids.resize(lm_num, 0);
	else
		pos.ids = arg_ids;
	
	pos.image_num = 1;
	pos.lm_worlds.resize(1);
	pos.lm_worlds[0].resize(lm_num);
	for (int i = 0; i < lm_num; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			pos.lm_worlds[0][i][j] = verts[i * 3 + j];
		}
	}

	return pos;
}

PDM JAT_anatomy_positions::create_pdm(int img_idx)
{
	assert_message(img_idx >= 0 && img_idx < image_num, "image index out of bound");

	PDM lm_pdm;
	std::vector<float>& verts = lm_pdm.GetPoints();
	verts.resize( ids.size() * 3, 0 );

	for (int i = 0; i < static_cast<int>(ids.size()); ++i)
	{
		vect3<double> lm_pos = get_lm_world(img_idx, i);
		for (int j = 0; j < 3; ++j)
			verts[i * 3 + j] = lm_pos[j];
	}

	return lm_pdm;
}

void JAT_anatomy_positions::fuse(const JAT_anatomy_positions& positions)
{
	assert_message(image_num == positions.image_num, "image number mismatch during fusion");

	int old_lm_size = static_cast<int>(ids.size());
	int new_lm_size = static_cast<int>(ids.size() + positions.ids.size());

	for (size_t i = 0; i < positions.ids.size(); ++i)
		ids.push_back(positions.ids[i]);

	for (int i = 0; i < image_num; ++i)
	{
		lm_worlds[i].resize(new_lm_size);

		for (size_t j = 0; j < positions.ids.size(); ++j)
			lm_worlds[i][j + old_lm_size] = positions.lm_worlds[i][j];
	}
}


/************************************************************************/
/* JAT_annot_struct parse implementations                               */
/************************************************************************/

void JAT_annot_struct::print_out_basic(std::ostream& os) const
{
	os << std::endl << "[Annotation]" << std::endl;
	os << "NumImages:     " << image_paths.size() << std::endl;
	os << "NumLandmarks:  " << positions.ids.size() << std::endl;
	os << "TargetDims:    " << positions.get_offset_dimension() << std::endl;
}

vect3<double> JAT_annot_struct::parse_lm(const std::vector<std::string>& tokens)
{
	assert_message(tokens.size() >= 8, "fails to parse landmark");

	vect3<double> world;
	for (int i = 0; i < 3; ++i)
		world[i] = stringUtils::str2num<double>(tokens[i + 5]);

	return world;
}

bool JAT_annot_struct::save_to_file(const char* path) const
{
	if (!positions.save_to_file(image_paths, path))
	{
		std::cerr << "fail to save annot structure" << std::endl;
		return false;
	}

	return true;
}

bool JAT_annot_struct::parse(const char* path)
{
	std::vector<std::string> lines;
	if (!stringUtils::ReadLines(path, lines)) 
	{
		std::cerr << "fails to read lines from file " << path << std::endl;
		return false;
	}

	assert_message(lines.size() != 0, "empty file");

	int image_num, lm_num;
	sscanf(lines[0].c_str(), "%d\t%d", &image_num, &lm_num);

	image_paths.resize( image_num );
	positions.ids.resize( lm_num );
	positions.image_num = image_num;
	positions.lm_worlds.resize(image_num);

	for (int i = 0; i < image_num; ++i)
		positions.lm_worlds[i].resize(lm_num);

	for (int image_index = 0; image_index < image_num; ++image_index)
	{
		int sp = image_index * (1 + lm_num) + 1;
		image_paths[image_index] = lines[sp];

		for (int lm_idx = 0; lm_idx < lm_num; ++lm_idx)
		{
			std::vector<std::string> tokens = stringUtils::Split(lines[sp + 1 + lm_idx], '\t');
			assert_message(tokens.size() >= 2, "line corrupted");
			positions.lm_worlds[image_index][lm_idx] = parse_lm(tokens);

			if (image_index == 0)
				positions.ids[lm_idx] = stringUtils::str2num<int>(tokens[1]);
		}
	}

	return true;
}

int JAT_annot_struct::get_offset_dimension() const
{
	return positions.get_offset_dimension();
}

template <typename T> void JAT_annot_struct::get_voxel_offsets(int img_idx, const mxImage<T>& image, const vect3<int>& voxel, double* offsets) const
{
	positions.get_voxel_offsets(img_idx, image, voxel, offsets);
}

template <typename T> void JAT_annot_struct::get_random_samples(int img_idx, const mxImage<T>& image, vect3<double> radius, Random& random, int sample_num, std::vector< vect3<int> >& voxels, const MaskImage* mask) const
{
	positions.get_random_samples(img_idx, image, radius, random, sample_num, voxels, mask);
}

template <typename T> void JAT_annot_struct::get_spherical_random_samples(int img_idx, const mxImage<T>& image, vect3<double> radius, Random& random, int sample_num, std::vector< vect3<int> >& voxels, const MaskImage* mask) const
{
	positions.get_spherical_random_samples(img_idx, image, radius, random, sample_num, voxels, mask);
}

template <typename T> void JAT_annot_struct::get_sample_map(int img_idx, const mxImage<T>& image, vect3<double> radius, MaskImage& sample_map, const MaskImage* mask) const
{
	positions.get_sample_map(img_idx, image, radius, sample_map, mask);
}

void JAT_annot_struct::aggregate(const JAT_annot_struct& annot)
{
	positions.aggregate(annot.positions);

	for (size_t i = 0; i < annot.image_paths.size(); ++i)
		image_paths.push_back(annot.image_paths[i]);
}


} } }

#endif