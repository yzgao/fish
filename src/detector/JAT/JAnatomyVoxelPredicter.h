//
//  JAnatomyVoxelPredictor.h
//  FISH
//
//  Created by Yaozong Gao on 09/25/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __JAnatomyVoxelPredicter_h__
#define __JAnatomyVoxelPredicter_h__

#include "forest/RandomForest.h"
#include "common/mxImage.h"
#include "feature/FeatureSpaceArray.h"
#include "detector/DetectorIO.h"
#include "feature/haar3d/Haar3DFeatureSpace.h"
#include "detector/JAT/JATDetectorInfo.h"
#include "limits.h"


namespace BRIC { namespace IDEA { namespace FISH {


class JAnatomyVoxelPredicter
{
public:
	typedef MemoryAxisAlignedWeakLearner W;
	typedef SimpleRegressionStatisticsAggregator S;

public:
	void SetDetector(const char* root_folder, const char* anatomy_name, int level, int scale);
	bool ReadDetectorInfo(JATDetectorInfo& info) const;

	template <typename T>
	void ComputeDisplacements(const mxImage<T>& image, SimpleRegressor::FusionType type, const std::vector< vect3<int> >& voxels, 
		std::vector< double >& displacements, int& target_dim) const;

	template <typename T>
	void ComputeDisplacementMap(const mxImage<T>& image, SimpleRegressor::FusionType type, std::vector< mxImage<double> >& displacementMaps);

private:

	std::string get_detector_folder() const;
	void bounding_box(const std::vector< vect3<int> >& voxels, vect3<int>& sp, vect3<int>& ep) const;

	// detector info
	const char* m_root_folder;
	const char* m_anatomy_name;
	int m_level;
	int m_scale;
};


//////////////////////////////////////////////////////////////////////////
// implementations


void JAnatomyVoxelPredicter::SetDetector(const char* root_folder, const char* anatomy_name, int level, int scale)
{
	if (level != 0)
		err_message("JAnatomyVoxelPredicter only supports 0-level predictions");

	// detector info
	m_root_folder = root_folder;
	m_anatomy_name = anatomy_name;
	m_level = level;
	m_scale = scale;
}


std::string JAnatomyVoxelPredicter::get_detector_folder() const
{
	std::string detectorFolder;
	if (m_level == 0)
		detectorFolder = std::string(m_root_folder) + stringUtils::Slash() + m_anatomy_name;
	else
		detectorFolder = std::string(m_root_folder) + stringUtils::Slash() + m_anatomy_name + "_C" + stringUtils::num2str(m_level);

	return detectorFolder;
}


bool JAnatomyVoxelPredicter::ReadDetectorInfo(JATDetectorInfo& info) const
{
	info.clear();

	std::string detector_folder = get_detector_folder();
	std::string info_path = detector_folder + stringUtils::Slash() + "info_R" + stringUtils::num2str(m_scale) + ".ini";

	boost::property_tree::ptree pt;
	try {
		boost::property_tree::ini_parser::read_ini(info_path.c_str(), pt);
		std::string stringSpacing = pt.get<std::string>("Info.Resolution");
		info.spacing = stringUtils::ParseVect3<double>(stringSpacing);
		info.sample_radius = pt.get<double>("Info.Radius");
		info.body_mask = pt.get<int>("Info.BodyMask") != 0;

		boost::optional<int> v = pt.get_optional<int>("Info.UseDistance");
		if (v)
			info.use_distance_map = (v.get() != 0);
		else
			info.use_distance_map = false;

		info.context_level = pt.get<int>("Info.ContextLevel");

		std::string type_str = pt.get<std::string>("Info.AnatomyType");
		std::vector<std::string> tokens = stringUtils::Split(type_str, ' ');
		for (size_t i = 0; i < tokens.size(); ++i)
		{
			if (tokens[i] == "LM")
				info.anatomy_types.push_back(JAT_LM);
			else if (tokens[i] == "BOX")
				info.anatomy_types.push_back(JAT_BOX);
			else if (tokens[i] == "BD")
				info.anatomy_types.push_back(JAT_BD);
			else {
				std::cerr << "skip unknown anatomy type: " << tokens[i] << std::endl;
			}
		}

		std::string id_str = pt.get<std::string>("Info.IDs");
		tokens = stringUtils::Split(id_str, ' ');
		for (size_t i = 0; i < tokens.size(); ++i)
			info.anatomy_ids.push_back(stringUtils::str2num<int>(tokens[i]));

	}
	catch (std::runtime_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}


template <typename T>
void JAnatomyVoxelPredicter::ComputeDisplacements(const mxImage<T>& image, SimpleRegressor::FusionType type, const std::vector< vect3<int> >& voxels,
	std::vector< double >& displacements, int& target_dim) const
{
	/************************************************************************/
	/* Load Detector                                                        */
	/************************************************************************/

	std::string detector_folder = get_detector_folder();

	FeatureSpaceArray<IFeatureSpace> fss;
	std::auto_ptr< Forest<W, S> > forest = DetectorIO::ReadDetector< Forest<W, S>, IFeatureSpace >(detector_folder.c_str(), m_scale, fss);

	if (fss.GetFeatureSpaces().size() == 0 || forest->GetTreeNumber() == 0)
	{
		std::cerr << "empty tree and feature space" << std::endl;
		exit(-1);
	}

	const S& s = forest->GetTree(0)->GetAnyLeafStatisticsAggregator();
	target_dim = s.GetTargetDimension();


	/************************************************************************/
	/* preparing the modality images                                        */
	/************************************************************************/

	vect3<int> sp, ep;
	bounding_box(voxels, sp, ep);

	std::vector< mxImage<double> > modality_images;
	modality_images.resize(1);

	vect3<int> pad_sp = sp, pad_ep = ep;
	mxImageUtils::PadBox(pad_sp, pad_ep, fss[0].GetPatchSize(0));
	mxImageUtils::Crop2<T, double>(image, pad_sp, pad_ep, modality_images[0], 0.0);

	if (m_level > 0)
	{
		err_message("JAnatomyVoxelPredicter only supports 0-level predictions");
	}

	// preprocess images
	std::auto_ptr<IImageFeatureFunctor> functor(fss[0].CreateImageFeatureFunctor());
	for (size_t i = 0; i < modality_images.size(); ++i)
		functor->PreprocessInput(modality_images[i], i);

	assert_message(modality_images.size() == fss[0].GetNumberFeatureSpaces(), "modality image number mismatch");


	/************************************************************************/
	/* compute displacement vectors                                         */
	/************************************************************************/

	displacements.resize( voxels.size() * target_dim );

	#pragma omp parallel for schedule(dynamic)
	for (int i = 0; i < static_cast<int>(voxels.size()); ++i)
	{
		std::auto_ptr< ImageFeatureFunctorArray<IImageFeatureFunctor> > thread_functors = fss.CreateImageFeatureFunctor();

		// setup the image functor
		for (size_t k = 0; k < modality_images.size(); ++k)
		{
			vect3<unsigned int> patch_size = fss[0].GetPatchSize(k);

			vect3<unsigned int> modality_voxel;
			for (int j = 0; j < 3; ++j)
				modality_voxel[j] = voxels[i][j] - sp[j] + patch_size[j] / 2;

			thread_functors->SetImage(modality_images[k], k);
			thread_functors->SetVoxel(modality_voxel, k);
		}

		// output displacement 
		SimpleRegressor::Regress2(forest.get(), *thread_functors, type, &(displacements[i * target_dim]));
	}
}


template <typename T>
void JAnatomyVoxelPredicter::ComputeDisplacementMap(const mxImage<T>& image, SimpleRegressor::FusionType type, std::vector< mxImage<double> >& displacementMaps)
{
	vect3<unsigned int> image_size = image.GetImageSize();

	// prepare the testing voxels
	std::vector< vect3<int> > voxels;
	voxels.resize( image_size[0] * image_size[1] * image_size[2] );

	for (unsigned int z = 0; z < image_size[2]; ++z)
	{
		for (unsigned int y = 0; y < image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < image_size[0]; ++x)
			{
				unsigned int index = z * image_size[0] * image_size[1] + y * image_size[0] + x;
				voxels[index] = vect3<int>(x, y, z);
			}
		}
	}


	// compute displacements for testing voxels
	std::vector<double> displacements;
	int target_dim = 0;

	ComputeDisplacements(image, type, voxels, displacements, target_dim);


	// put results into a 3D map
	displacementMaps.resize( target_dim );
	for (size_t i = 0; i < displacementMaps.size(); ++i)
	{
		displacementMaps[i].CopyImageInfo(image);
		displacementMaps[i].SetImageSize(image_size);
		displacementMaps[i].Fill(0);
	}

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
			{
				size_t index = z * image_size[0] * image_size[1] + y * image_size[0] + x;

				for (int i = 0; i < target_dim; ++i)
					displacementMaps[i](x, y, z) = displacements[index * target_dim + i];
			}
		}
	}
}


void JAnatomyVoxelPredicter::bounding_box(const std::vector< vect3<int> >& voxels, vect3<int>& sp, vect3<int>& ep) const
{
	sp[0] = sp[1] = sp[2] = std::numeric_limits<int>::max();
	ep[0] = ep[1] = ep[2] = -std::numeric_limits<int>::max();

	for (size_t i = 0; i < voxels.size(); ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			if (voxels[i][j] < sp[j])
				sp[j] = voxels[i][j];

			if (voxels[i][j] > ep[j])
				ep[j] = voxels[i][j];
		}
	}
}



} } }



#endif
