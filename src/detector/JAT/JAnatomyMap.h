//
//  JAnatomyMap.h
//  FISH
//
//  Created by Yaozong Gao on 07/13/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __JAnatomyMap_h__
#define __JAnatomyMap_h__

#include "forest/RandomForest.h"
#include "common/mxImageUtils.h"
#include "feature/FeatureSpaceArray.h"
#include "detector/DetectorIO.h"
#include "feature/haar3d/Haar3DFeatureSpace.h"
#include "feature/CompositeFeatureSpace.h"
#include "detector/JAT/JAT_annot_struct.h"
#include "detector/JAT/JATDetectorInfo.h"
#include "detector/JAT/JAT_feature_space.h"
#include "detector/JAT/JATDetectorInfo.h"
#include "feature/MultiFeatureFunctors.h"


namespace BRIC { namespace IDEA { namespace FISH {

class JAnatomyMap
{
public:

	typedef MemoryAxisAlignedWeakLearner W;
	typedef SimpleRegressionStatisticsAggregator S;

	JAnatomyMap();

	// input parameters
	void SetBoundingBox(const vect3<int>& sp, const vect3<int>& ep);
	void SetMask(const mxImage<unsigned char>* mask);
	void SetStride(const vect3<int>& stride);
	void SetDetector(const char* root_folder, const char* anatomy_name, int level, int scale);
	void SetConstraintLandmarks(const std::map<int, vect3<double> >* constraint_landmarks);

	// major interfaces to public
	bool ReadDetectorInfo(JATDetectorInfo& info);
	template <typename T> void ComputeDisplacementMap(const mxImage<T>& image, SimpleRegressor::FusionType type, std::vector< mxImage<double> >& displacementMaps);

private:

	std::string get_detector_folder() const;
	template <typename T> void compute_effective_sp_ep(const mxImage<T>& image, const vect3<int>& sp, const vect3<int>& ep, vect3<int>& eff_sp, vect3<int>& eff_ep);
	template <typename T> void setup_displacement_maps(const mxImage<T>& image, int dim, std::vector< mxImage<double> >& displacementMaps);
	template <typename T> void voxelwise_displacement_regression_L0(const mxImage<T>& image, SimpleRegressor::FusionType type, Forest<W, S>* forest, FeatureSpaceArray<IFeatureSpace>& fss, std::vector< mxImage<double> >& displacementMaps, int old_size);
	template <typename T> void voxelwise_displacement_regression_L1(const mxImage<T>& image, SimpleRegressor::FusionType type, Forest<W, S>* forest, FeatureSpaceArray<IFeatureSpace>& fss, const JATDetectorInfo& info, std::vector< mxImage<double> >& displacementMaps, int old_size);

	// detector info
	const char* m_root_folder;
	const char* m_anatomy_name;
	int m_level;
	int m_scale;

	const std::map<int, vect3<double> >* m_constraintLandmarks;

	vect3<int> m_stride;

	// box & mask
	vect3<int> m_sp;
	vect3<int> m_ep;

	const mxImage<unsigned char>* m_mask;
};


//////////////////////////////////////////////////////////////////////////


JAnatomyMap::JAnatomyMap()
{
	m_root_folder = NULL;
	m_anatomy_name = NULL;
	m_level = 0;
	m_scale = 0;

	m_stride[0] = m_stride[1] = m_stride[2] = 1;
	for (int i = 0; i < 3; ++i)
		m_sp[i] = m_ep[i] = 0;
	
	m_mask = NULL;
	m_constraintLandmarks = NULL;
}

void JAnatomyMap::SetBoundingBox(const vect3<int>& sp, const vect3<int>& ep)
{
	m_sp = sp;
	m_ep = ep;
}

void JAnatomyMap::SetMask(const mxImage<unsigned char>* mask)
{
	m_mask = mask;
}

void JAnatomyMap::SetDetector(const char* root_folder, const char* anatomy_name, int level, int scale)
{
	m_root_folder = root_folder;
	m_anatomy_name = anatomy_name;
	m_level = level;
	m_scale = scale;
}

void JAnatomyMap::SetStride(const vect3<int>& stride)
{
	m_stride = stride;
}

void JAnatomyMap::SetConstraintLandmarks(const std::map<int, vect3<double> >* constraintLandmarks)
{
	m_constraintLandmarks = constraintLandmarks;
}

bool JAnatomyMap::ReadDetectorInfo(JATDetectorInfo& info)
{
	info.clear();

	std::string detector_folder = get_detector_folder();
	std::string info_path = detector_folder + stringUtils::Slash() + "info_R" + stringUtils::num2str(m_scale) + ".ini";

	boost::property_tree::ptree pt;
	try {
		boost::property_tree::ini_parser::read_ini(info_path.c_str(), pt);
		std::string stringSpacing = pt.get<std::string>("Info.Resolution");
		info.spacing = stringUtils::ParseVect3<double>(stringSpacing);

		std::string stringRadius = pt.get<std::string>("Info.Radius");
		info.sample_radius = stringUtils::ParseVect3<double>(stringRadius);
		
		info.body_mask = pt.get<int>("Info.BodyMask") != 0;

		boost::optional<std::string> opt_constraint_names = pt.get_optional<std::string>("Info.ConstraintLandmarks");
		if (opt_constraint_names)
			info.constraint_landmarks = stringUtils::Split(opt_constraint_names.get(), ' ');
		else
			info.constraint_landmarks.clear();

		std::string id_str = pt.get<std::string>("Info.IDs");
		std::vector<std::string> tokens = stringUtils::Split(id_str, ' ');
		for (size_t i = 0; i < tokens.size(); ++i)
			info.anatomy_ids.push_back( stringUtils::str2num<int>(tokens[i]) );
	}
	catch (std::runtime_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}

std::string JAnatomyMap::get_detector_folder() const
{
	std::string detectorFolder;
	if (m_level == 0)
		detectorFolder = std::string(m_root_folder) + stringUtils::Slash() + m_anatomy_name;
	else
		detectorFolder = std::string(m_root_folder) + stringUtils::Slash() + m_anatomy_name + "_C" + stringUtils::num2str(m_level);

	return detectorFolder;
}

template <typename T>
void JAnatomyMap::compute_effective_sp_ep(const mxImage<T>& image, const vect3<int>& sp, const vect3<int>& ep, vect3<int>& eff_sp, vect3<int>& eff_ep)
{
	vect3<unsigned int> image_size = image.GetImageSize();

	for (int i = 0; i < 3; ++i)
	{
		eff_sp[i] = sp[i];
		eff_ep[i] = ep[i];

		if (eff_sp[i] < 0)
			eff_sp[i] = 0;

		if (eff_sp[i] >= static_cast<int>(image_size[i]))
			eff_sp[i] = static_cast<int>(image_size[i]) - 1;

		if (eff_ep[i] < 0)
			eff_ep[i] = 0;

		if (eff_ep[i] >= static_cast<int>(image_size[i]))
			eff_ep[i] = static_cast<int>(image_size[i]) - 1;
	}
}

template <typename T>
void JAnatomyMap::setup_displacement_maps(const mxImage<T>& image, int dim, std::vector< mxImage<double> >& displacementMaps)
{
	vect3<unsigned int> image_size;
	for (int i = 0; i < 3; ++i)
		image_size[i] = m_ep[i] - m_sp[i] + 1;

	vect3<double> image_origin;
	mxImageUtils::Voxel2World(image, m_sp, image_origin);

	// keep the existing maps in displacementMaps for recursive invoking
	size_t old_size = displacementMaps.size();
	displacementMaps.resize(old_size + dim);

	for (int i = old_size; i < displacementMaps.size(); ++i)
	{
		displacementMaps[i].CopyImageInfo(image);
		displacementMaps[i].SetOrigin(image_origin);
		displacementMaps[i].SetImageSize(image_size);
		displacementMaps[i].Fill(0);
	}
}

template <typename T>
void JAnatomyMap::voxelwise_displacement_regression_L0(const mxImage<T>& image, SimpleRegressor::FusionType type, Forest<W,S>* forest, FeatureSpaceArray<IFeatureSpace>& fss, std::vector< mxImage<double> >& displacementMaps, int old_size)
{
	const S& s = forest->GetTree(0)->GetAnyLeafStatisticsAggregator();
	int target_dim = s.GetTargetDimension();

	vect3<int> eff_sp, eff_ep;
	compute_effective_sp_ep(image, m_sp, m_ep, eff_sp, eff_ep);

	// crop intensity image
	mxImage<double> crop_image;
	vect3<int> pad_sp = eff_sp, pad_ep = eff_ep;
	mxImageUtils::PadBox(pad_sp, pad_ep, fss[0].GetPatchSize(0));
	mxImageUtils::Crop2<T, double>(image, pad_sp, pad_ep, crop_image, 0.0);

	// Haar preprocessing
	std::auto_ptr<IImageFeatureFunctor> functor(fss[0].CreateImageFeatureFunctor());
	functor->PreprocessInput(crop_image, 0);

	assert_message(fss[0].GetNumberFeatureSpaces() == 1, "the number of input images should be 1 in level 0");

	// compute displacement maps
	#pragma omp parallel for schedule(dynamic)
	for (int z = m_sp[2]; z <= m_ep[2]; z += m_stride[2])
	{
		std::auto_ptr< ImageFeatureFunctorArray<IImageFeatureFunctor> > thread_functors = fss.CreateImageFeatureFunctor();

		std::vector<double> displacements;
		displacements.resize(target_dim);

		for (int y = m_sp[1]; y <= m_ep[1]; y += m_stride[1])
		{
			for (int x = m_sp[0]; x <= m_ep[0]; x += m_stride[0])
			{
				if (x < eff_sp[0] || x > eff_ep[0] || y < eff_sp[1] || y > eff_ep[1] || z < eff_sp[2] || z > eff_ep[2])
					continue;

				if (m_mask != NULL && (*m_mask)(x, y, z) == 0)
					continue;

				vect3<unsigned int> global_voxel(x, y, z), adjusted_voxel;
				vect3<unsigned int> patch_size = fss[0].GetPatchSize(0);
				for (int j = 0; j < 3; ++j)
					adjusted_voxel[j] = global_voxel[j] - static_cast<unsigned int>(eff_sp[j]) + patch_size[j] / 2;

				thread_functors->SetImage(crop_image, 0);
				thread_functors->SetVoxel(adjusted_voxel, 0);

				// output displacement 
				SimpleRegressor::Regress2(forest, *thread_functors, type, &displacements[0]);

				vect3<unsigned int> out_voxel;
				for (int j = 0; j < 3; ++j)
					out_voxel[j] = global_voxel[j] - m_sp[j];

				for (int i = 0; i < target_dim; ++i)
					displacementMaps[old_size + i](out_voxel) = displacements[i];
			}
		}
	}
}


template <typename T>
void JAnatomyMap::voxelwise_displacement_regression_L1(const mxImage<T>& image, SimpleRegressor::FusionType type, Forest<W,S>* forest, FeatureSpaceArray<IFeatureSpace>& fss, const JATDetectorInfo& info, std::vector< mxImage<double> >& displacementMaps, int old_size)
{
	assert_message(m_level > 0, "level must be >= 1");

	// convert general IFeatureSpace to JAT_feature_space
	FeatureSpaceArray<JAT_feature_space> jat_fss(false);
	const std::vector<IFeatureSpace*>& raw_fs_pointers = fss.GetFeatureSpaces();
	for (size_t i = 0; i < raw_fs_pointers.size(); ++i)
		jat_fss.AddFeatureSpace( static_cast<JAT_feature_space*>(raw_fs_pointers[i]) );

	// compute effective region for regression
	vect3<int> eff_sp, eff_ep;
	compute_effective_sp_ep(image, m_sp, m_ep, eff_sp, eff_ep);

	// get target dimension
	const S& s = forest->GetTree(0)->GetAnyLeafStatisticsAggregator();

	int target_dim = s.GetTargetDimension();
	vect3<unsigned int> intensity_patchsize = jat_fss[0].GetIntensityPatchSize().to<unsigned int>();

	// 1) compute local intensity image
	vect3<int> intensity_sp = eff_sp, intensity_ep = eff_ep;
	mxImageUtils::PadBox(intensity_sp, intensity_ep, intensity_patchsize);

	mxImage < double > cropped_image;
	mxImageUtils::Crop2(image, intensity_sp, intensity_ep, cropped_image, 0.0);

	// 2) preprocess all images
	std::auto_ptr<JAT_image_feature_functor> functor(jat_fss[0].CreateImageFeatureFunctor());
	functor->PreprocessInput(cropped_image, 0);

	// 3) voxel-wise displacement regression
	#pragma omp parallel for schedule(dynamic)
	for (int z = m_sp[2]; z <= m_ep[2]; z += m_stride[2])
	{
		std::auto_ptr< ImageFeatureFunctorArray<JAT_image_feature_functor> > thread_functors = jat_fss.CreateImageFeatureFunctor();
		for (size_t i = 0; i < thread_functors->size(); ++i)
			(*thread_functors)[i].SetConstraintLandmarks(m_constraintLandmarks);

		std::vector<double> displacements(target_dim, 0);

		for (int y = m_sp[1]; y <= m_ep[1]; y += m_stride[1])
		{
			for (int x = m_sp[0]; x <= m_ep[0]; x += m_stride[0])
			{
				if (x < eff_sp[0] || x > eff_ep[0] || y < eff_sp[1] || y > eff_ep[1] || z < eff_sp[2] || z > eff_ep[2])
					continue;

				if (m_mask != NULL && (*m_mask)(x, y, z) == 0)
					continue;

				vect3<unsigned int> global_voxel(x, y, z), adjusted_voxel;
				for (int j = 0; j < 3; ++j)
					adjusted_voxel[j] = global_voxel[j] - static_cast<unsigned int>(eff_sp[j]) + intensity_patchsize[j] / 2;

				// setup thread functors
				thread_functors->SetImage(cropped_image, 0);
				thread_functors->SetVoxel(adjusted_voxel, 0);

				// output displacement 
				SimpleRegressor::Regress2(forest, *thread_functors, type, &displacements[0]);

				vect3<unsigned int> out_voxel;
				for (int j = 0; j < 3; ++j)
					out_voxel[j] = global_voxel[j] - m_sp[j];

				for (int i = 0; i < target_dim; ++i)
					displacementMaps[old_size + i](out_voxel) = displacements[i];
			}
		}
	}
}

template <typename T>
void JAnatomyMap::ComputeDisplacementMap(const mxImage<T>& image, SimpleRegressor::FusionType type, std::vector< mxImage<double> >& displacementMaps)
{
	assert_message(m_ep[0] >= m_sp[0] && m_ep[1] >= m_sp[1] && m_ep[2] >= m_sp[2], "ep must be >= sp");

	// load detector & info
	std::string detector_folder = get_detector_folder();

	JATDetectorInfo info;
	if (!ReadDetectorInfo(info))
		err_message("fail to read detector info");

	// read forest and feature space
	std::auto_ptr< Forest<W, S> > forest;
	FeatureSpaceArray<IFeatureSpace> fss;

	forest = DetectorIO::ReadDetector< Forest<W, S>, IFeatureSpace >(detector_folder.c_str(), m_scale, fss);

	if (fss.GetFeatureSpaces().size() == 0 || forest->GetTreeNumber() == 0)
		err_message("empty tree and feature space");

	// setup the displacement maps
	const S& s = forest->GetTree(0)->GetAnyLeafStatisticsAggregator();
	int target_dim = s.GetTargetDimension();

	size_t old_size = displacementMaps.size();
	setup_displacement_maps(image, target_dim, displacementMaps);

	// two-level branch
	if (m_level == 0)
		voxelwise_displacement_regression_L0(image, type, forest.get(), fss, displacementMaps, old_size);
	else
	{
		assert_message(m_constraintLandmarks != NULL, "empty constraint landmarks");
		assert_message(info.constraint_landmarks.size() == m_constraintLandmarks->size(), "mismatch on the number of required constraint landmarks");
		voxelwise_displacement_regression_L1(image, type, forest.get(), fss, info, displacementMaps, old_size);
	}
}

} } }


#endif