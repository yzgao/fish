//
//  JAT_ini_struct.h
//  FISH
//
//  Created by Yaozong Gao on 06/16/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __JAT_ini_struct_h__
#define __JAT_ini_struct_h__

#include "forest/ForestTrainer.h"
#include <iomanip>


namespace BRIC { namespace IDEA { namespace FISH {


enum JAT_SAMPLE_STRATEGY_TYPE
{
	JAT_SAMPLE_UNIFORM,
	JAT_SAMPLE_SPHERICAL
};

/************************************************************************/
/*  annotation file structure                                           */
/************************************************************************/

//
// geometric rules:
// LM1,LM2,CODE  e.g., 201,-1,0 means use the x coordinate of landmark 201
//
// list of rules:
// LM1,-1,[0-2]:			use the coordinate of LM1
// LM1,LM2,[0-2]-:			use the coordinate difference between LM1 and LM2
// LM1,LM2,[0-2]m:			use the mean of coordinate between LM1 and LM2

enum GeometricCode
{
	COORD_X = 0,
	COORD_Y,
	COORD_Z,
	MEAN_COORD_X,
	MEAN_COORD_Y,
	MEAN_COORD_Z,
	NORM,
	NORM_DIFF
};

struct GeometricRule
{
	int id1;
	int id2;
	GeometricCode code;
};


struct JAT_ini_struct
{
	std::string detector_name;										// detector name
	std::string annot_path;											// annotation path
	std::string root_folder;										// output top folder
	int level;														// current context iteration
	int scale;														// image resolution index
	vect3<double> spacing;											// image resolution

	vect3<double> sample_radius;									// controls the sampling area near the anatomy (world)
	int sample_number;												// sample num per image
	bool body_mask;													// whether use body mask to restrict the sampling region
	JAT_SAMPLE_STRATEGY_TYPE sample_strategy;						// uniform sampling or spherical sampling

	ForestTrainingParameters forest_params;							// forest parameters

	std::vector< vect3<unsigned int> > intensity_filtersizes;		// intensity filter sizes of 3D Haar features
	vect3<unsigned int> intensity_patchsize;						// intensity patch size'

	std::vector<GeometricRule> geometric_rules;						// geometric detector names

	/************************************************************************/
	/*  member functions                                                    */
	/************************************************************************/

	// parse structure from path
	bool parse(const char* filepath);							// parse JAT_annot_struct from INI file

	// produce default INI files
	static bool write_default(const char* filepath);			// write default JAT_annot_struct to INI file

	// print out 
	void print_out(std::ostream& os) const;						// print out the ini 

private:

	GeometricCode parse_code(const std::string& str);
};



/************************************************************************/
/* implementation of JAT_annot_struct                                   */
/************************************************************************/

GeometricCode JAT_ini_struct::parse_code(const std::string& str)
{
	if (str.size() >= 3)
		err_message("unrecognized geometric code");

	if (str.size() == 1)
	{
		if (str[0] == 'n')
		{
			return NORM;
		}
		else
		{
			int idx = stringUtils::str2num<int>(str);
			switch (idx)
			{
			case 0: return COORD_X;
			case 1: return COORD_Y;
			case 2: return COORD_Z;
			default: err_message("geometric idx out of bound"); break;
			}
		}
	}
	else if (str.size() == 2)
	{
		if (str == "nd")
		{
			return NORM_DIFF;
		}
		else
		{
			int idx = stringUtils::str2num<int>(str.substr(0, 1));
			if (str[1] == 'm')
			{
				switch (idx)
				{
				case 0: return MEAN_COORD_X;
				case 1: return MEAN_COORD_Y;
				case 2: return MEAN_COORD_Z;
				default: err_message("geometric idx out of bound"); break;
				}
			}
			else
				err_message("unrecognized geometric code");
		}
	}
	else
		err_message("unrecognized geometric code");

	return COORD_X;
}


bool JAT_ini_struct::parse(const char* file)
{
	using boost::property_tree::ptree;

	ptree pt;
	try{
		boost::property_tree::ini_parser::read_ini(file, pt);
	}
	catch (boost::property_tree::ini_parser_error& error) {

		std::cerr << error.what() << std::endl;
		return false;
	}

	try {

		// General section
		detector_name = pt.get<std::string>("General.Name");
		annot_path = pt.get<std::string>("General.Annotation");
		root_folder = pt.get<std::string>("General.RootFolder");
		level = pt.get<int>("General.Level");
		scale = pt.get<int>("General.Scale");
		spacing = stringUtils::ParseVect3<double>(pt.get<std::string>("General.Spacing"));

		// sampling section
		sample_radius = stringUtils::ParseVect3<double>(pt.get<std::string>("Sampling.SampleRadius"));
		sample_number = pt.get<int>("Sampling.SampleNum");
		body_mask = pt.get<int>("Sampling.BodyMask") != 0;
		std::string str_sample_strategy = pt.get<std::string>("Sampling.SampleStrategy");
		if (str_sample_strategy == "UNIFORM")
			sample_strategy = JAT_SAMPLE_UNIFORM;
		else if (str_sample_strategy == "SPHERICAL")
			sample_strategy = JAT_SAMPLE_SPHERICAL;
		else
			err_message("unrecognized sample strategy type");

		// Forest section	
		forest_params.numTrees = pt.get<unsigned int>("Forest.NumTrees");
		forest_params.treeParameters.maxTreeDepth = pt.get<unsigned int>("Forest.MaxTreeDepth");
		forest_params.treeParameters.numOfCandidateThresholdsPerWeakLearner = pt.get<unsigned int>("Forest.NumThresholds");
		forest_params.treeParameters.numOfRandomWeakLearners = pt.get<unsigned int>("Forest.NumWeakLearners");
		forest_params.treeParameters.minElementsOfLeafNode = pt.get<unsigned int>("Forest.MinLeafNum");
		forest_params.treeParameters.minInformationGain = pt.get<double>("Forest.MinInfoGain");

		std::vector<unsigned int> filtersizes;

		// Intensity section
		stringUtils::str2vec<unsigned int>(pt.get<std::string>("Intensity.FilterSize"), filtersizes, ' ');
		intensity_patchsize = stringUtils::ParseVect3<unsigned int>(pt.get<std::string>("Intensity.PatchSize"));

		intensity_filtersizes.resize(filtersizes.size());
		for (size_t i = 0; i < filtersizes.size(); ++i)
			intensity_filtersizes[i] = vect3<unsigned int>(filtersizes[i], filtersizes[i], filtersizes[i]);
		filtersizes.clear();

		// Geometric section
		boost::optional<std::string> opt_geometric_rules = pt.get_optional<std::string>("Geometric.Rules");
		if (opt_geometric_rules)
		{
			std::string str_rules = opt_geometric_rules.get();
			std::vector<std::string> tokens = stringUtils::Split(str_rules, ';');

			geometric_rules.resize(tokens.size());
			for (size_t i = 0; i < tokens.size(); ++i)
			{
				std::vector<std::string> tmp_tokens = stringUtils::Split(tokens[i], ',');
				assert_message(tmp_tokens.size() == 3, "geometric token size must be 3");
				geometric_rules[i].id1 = stringUtils::str2num<int>(tmp_tokens[0]);
				geometric_rules[i].id2 = stringUtils::str2num<int>(tmp_tokens[1]);
				geometric_rules[i].code = parse_code(tmp_tokens[2]);
			}
		}
		else
			geometric_rules.clear();

	}
	catch (boost::property_tree::ptree_error& error) {

		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}


void JAT_ini_struct::print_out(std::ostream& os) const
{
	os << std::endl << "[General]" << std::endl;
	os << "Name:          " << detector_name << std::endl;
	os << "Annot:         " << annot_path << std::endl;
	os << "Root:          " << root_folder << std::endl;
	os << "Level:         " << level << std::endl;
	os << "Scale:         " << scale << std::endl;
	os << "Spacing:       " << spacing << std::endl;

	os << std::endl << "[Sampling]" << std::endl;
	os << "Radius:        " << sample_radius << std::endl;
	os << "Number:        " << sample_number << std::endl;
	os << "BodyMask:      " << body_mask << std::endl;

	if (sample_strategy == JAT_SAMPLE_UNIFORM)
		os << "Strategy:      " << "Uniform" << std::endl;
	else if (sample_strategy == JAT_SAMPLE_SPHERICAL)
		os << "Strategy:      " << "Spherical" << std::endl;
	else
		err_message("unknown sampling strategy");

	os << std::endl << "[Forest]" << std::endl;
	os << "Trees:         " << forest_params.numTrees << std::endl;
	os << "Depth:         " << forest_params.treeParameters.maxTreeDepth << std::endl;
	os << "Features:      " << forest_params.treeParameters.numOfRandomWeakLearners << std::endl;
	os << "Thresholds:    " << forest_params.treeParameters.numOfCandidateThresholdsPerWeakLearner << std::endl;
	os << "Leaf:          " << forest_params.treeParameters.minElementsOfLeafNode << std::endl;

	os << std::endl << "[Intensity Haar]" << std::endl;
	os << "FilterSize:    ";
	for (size_t i = 0; i < intensity_filtersizes.size(); ++i)
		os << intensity_filtersizes[i] << " ";
	os << std::endl;
	os << "PatchSize:     " << intensity_patchsize << std::endl;

	if (geometric_rules.size() > 0)
	{
		os << std::endl << "[Geometric]" << std::endl;
		os << "Rules:         " << std::endl;
		for (size_t i = 0; i < geometric_rules.size(); ++i) {
			os << geometric_rules[i].id1 << ", " << geometric_rules[i].id2 << ": ";
			switch (geometric_rules[i].code)
			{
			case COORD_X: os << "X Coordinate" << std::endl; break;
			case COORD_Y: os << "Y Coordinate" << std::endl; break;
			case COORD_Z: os << "Z Coordinate" << std::endl; break;
			case MEAN_COORD_X: os << "Mean X Coordinate" << std::endl; break;
			case MEAN_COORD_Y: os << "Mean Y Coordinate" << std::endl; break;
			case MEAN_COORD_Z: os << "Mean Z Coordinate" << std::endl; break;
			case NORM: os << "Use Landmark Distance" << std::endl; break;
			case NORM_DIFF: os << "Use Landmark Distance Difference" << std::endl; break;
			default: err_message("unrecognized geometric code"); break;
			}
		}
		os << std::endl;
	}
}



bool JAT_ini_struct::write_default(const char* file)
{
	std::ofstream out(file);

	if (!out)
	{
		std::cerr << "fail to open file for write: " << file << std::endl;
		return false;
	}

	const char* lines[] = {
		"; A sample INI file for JAnatomyTrain",
		"",
		"[General]",
		"Name=DetectorName",
		"Annotation=/Users/Test/annot.txt",
		"RootFolder=/Users/Test/OutFolder",
		"Level=1",
		"Scale=4",
		"Spacing=4.0 4.0 4.0",
		"",
		"[Sampling]",
		"SampleRadius=300 300 300",
		"SampleNum=10000",
		"BodyMask=1",
		"SampleStrategy=UNIFORM",
		"",
		"[Forest]",
		"NumTrees=1",
		"MaxTreeDepth=20",
		"NumThresholds=100",
		"NumWeakLearners=1000",
		"MinLeafNum=8",
		"MinInfoGain=0",
		"",
		"[Intensity]",
		"FilterSize=3 5",
		"PatchSize=15 15 15",
		"",
		"[Geometric]",
		"Rules=201,-1,0;201,-1,1;201,-1,2;201,202,0m;201,-1,n;201,202,nd"
	};

	int num_lines = sizeof(lines) / sizeof(lines[0]);
	for (int i = 0; i < num_lines; ++i)
		out << lines[i] << std::endl;

	out.close();
	return true;
}

} } }

#endif