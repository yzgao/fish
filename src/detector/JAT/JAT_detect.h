//
//  JAT_detect.h
//  FISH
//
//  Created by Yaozong Gao on 07/26/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __JAT_detect_h__
#define __JAT_detect_h__

#include "forest/RandomForest.h"
#include "common/mxImageUtils.h"
#include "extern/ImageHelper.h"
#include "detector/JAT/JAT_annot_struct.h"
#include "detector/JAT/JAT_detect_config.h"
#include "detector/JAT/JAnatomyMap.h"

namespace BRIC { namespace IDEA { namespace FISH {


// JAT displacement maps
class JAT_DMapType
{
public:
	std::vector < mxImage<double> > displacement_maps;
	mxImage< unsigned char > mask;
	vect3<int> stride;

public:
	JAT_DMapType();
};


// detection-related functions
template <typename T>
class JAT_detect
{
public:
	typedef SimpleRegressionStatisticsAggregator S;
	typedef MemoryAxisAlignedWeakLearner W;

	typedef mxImage<double> DoubleImageType;
	typedef mxImage<unsigned char> MaskImageType;
	typedef std::vector<DoubleImageType> DoubleImageArrayType;

	// constructors
	JAT_detect();

	// basic setup
	bool LoadImage(const char* image_path);
	void SetDetector(std::string root_folder, std::string anatomy_name, int level, int scale);
	const JATDetectorInfo& GetDetectorInfo() const;
	void SetTreeFusionType(SimpleRegressor::FusionType type);

	// detection intermediate steps

	// global-local displacement maps
	void ComputeGlobalDisplacementMaps(std::pair< vect3<double>, int > init_pos[3], const vect3<int>& stride, JAT_DMapType& dmaps);
	void ComputeLocalDisplacementMaps(const JAT_anatomy_positions& init_positions, vect3<double>& radius, const vect3<int>& stride, JAT_DMapType& dmaps);

	// global-local landmark voting map
	void LandmarkGlobalVote(const JAT_DMapType& dmaps, int lm_idx, mxImage<double>& vote_map, bool allocateOutput = true);
	void LandmarkLocalVote(const JAT_DMapType& dmaps, int lm_idx, mxImage<double>& vote_map, bool allocateOutput = true);
	void LandmarkLocalVote(const JAT_DMapType& dmaps, int lm_idx, const JAT_anatomy_positions& init_positions, vect3<double> radius, mxImage<double>& vote_map, bool allocateOutput = true);

	// general global voting map
	void GlobalVoteMap(const JAT_DMapType& dmaps, const std::vector<int>& lm_idxs, mxImage<double>& vote_map);
	void LocalVoteMap(const JAT_DMapType& dmaps, int lm_idx, const JAT_anatomy_positions& init_positions, vect3<double> radius, mxImage<double>& vote_map);

	// different landmark detection strategies
	JAT_lm_type DetectLandmarkByMaxVote(const mxImage<double>& vote_map);
	JAT_lm_type DetectLandmarkByAdaptiveThreshold(const mxImage<double>& vote_map, double sigma, double threshold_ratio);
	JAT_lm_type DetectLandmarkByMultMaxVote(const mxImage<double>& vote_map, double sigma, double threshold_ratio, const vect3<double>& mask_radius, int candidate_num, JAT_select_type select_type);

	// detection easy-to-use interface
	void GlobalDetectAnatomy( const char* root_folder, const JAT_detect_config_unit& step, std::pair< vect3<double>, int > init_pos[3], JAT_anatomy_positions& positions );
	void LocalDetectAnatomy( const char* root_folder, const JAT_detect_config_unit& step, JAT_anatomy_positions& positions );
	void DetectAnatomy( const char* root_folder, const JAT_detect_config& steps, JAT_anatomy_positions& positions );

	// prepare required landmarks
	void ComputeRequiredLandmarks(const char* root_folder, const JAT_detect_config& config, std::map<int, vect3<double> >& all_landmarks);
	void SetConstraintLandmarks( const std::map<int, vect3<double> >& landmarks );
	void GetGlobalInitialPosition( const std::map<int, vect3<double> >& all_landmarks, const JAT_detect_config& config, std::pair< vect3<double>, int > init_pos[3] );
	void ComputeGlobalInitialBox(const mxImage<T>& image, vect3<double> radius, std::pair< vect3<double>, int > init_pos[3], vect3<int>& sp, vect3<int>& ep);

public:

	void trilinear_vote(const vect3<double>& pos, mxImage<double>& vote_map);
	void nearest_vote(const vect3<double>& pos, mxImage<double>& vote_map);

	std::pair<JAT_lm_type, double> find_pos_by_max_vote(const mxImage<double>& vote_map);
	void mask_pos(mxImage<double>& vote_map, const vect3<double>& pos, const vect3<double>& radius);

	mxImage<T> m_orig_image;				// original image

	std::string m_root_folder;
	std::string m_anatomy_name;
	int m_level;
	int m_scale;

	// detector info
	JATDetectorInfo m_detector_info;

	// fusion strategy
	SimpleRegressor::FusionType m_fusion_type;

	// constraint landmarks
	std::map<int, vect3<double> > m_constraint_landmarks;
};


//////////////////////////////////////////////////////////////////////////

JAT_DMapType::JAT_DMapType() : stride(1, 1, 1) {}


template <typename T>
JAT_detect<T>::JAT_detect()
{
	m_level = m_scale = 0;
	m_fusion_type = SimpleRegressor::InverseWeighted;
}


template <typename T>
const JATDetectorInfo& JAT_detect<T>::GetDetectorInfo() const
{
	return m_detector_info;
}


template <typename T>
bool JAT_detect<T>::LoadImage(const char* image_path)
{
	if (!ImageHelper::ReadImage<T, T>(image_path, m_orig_image))
	{
		std::cerr << "fail to load image from " << image_path << std::endl;
		return false;
	}

	return true;
}


template <typename T>
void JAT_detect<T>::SetDetector(std::string root_folder, std::string anatomy_name, int level, int scale)
{
	m_root_folder = root_folder;
	m_anatomy_name = anatomy_name;
	m_level = level;
	m_scale = scale;
}


template <typename T>
void JAT_detect<T>::SetTreeFusionType(SimpleRegressor::FusionType type)
{
	m_fusion_type = type;
}


template <typename T>
void JAT_detect<T>::SetConstraintLandmarks(const std::map<int, vect3<double> >& landmarks)
{
	m_constraint_landmarks = landmarks;
}


template <typename T>
void JAT_detect<T>::ComputeGlobalInitialBox(const mxImage<T>& image, vect3<double> radius, std::pair< vect3<double>, int > init_pos[3], vect3<int>& sp, vect3<int>& ep)
{
	// compute voxel radius
	vect3<int> voxel_radius;
	for (int i = 0; i < 3; ++i)
		voxel_radius[i] = static_cast<int>(radius[i] / image.GetSpacing()[i] + 0.5);

	// compute voxel initial pos
	vect3<int> voxel_init_pos;
	for (int i = 0; i < 3; ++i)
	{
		if (init_pos[i].second == -1)
			voxel_init_pos[i] = -1;
		else
		{
			assert(init_pos[i].second >= 0 && init_pos[i].second <= 2);

			vect3<int> tmp_pos;
			mxImageUtils::World2Voxel(image, init_pos[i].first, tmp_pos);
			voxel_init_pos[i] = tmp_pos[init_pos[i].second];
		}
	}

	// compute bounding box
	vect3<unsigned int> image_size = image.GetImageSize();
	for (int i = 0; i < 3; ++i)
	{
		if (voxel_init_pos[i] < 0)
		{
			sp[i] = 0;
			ep[i] = image_size[i] - 1;
		}
		else
		{
			sp[i] = voxel_init_pos[i] - voxel_radius[i];
			ep[i] = voxel_init_pos[i] + voxel_radius[i];

			if (sp[i] < 0)
				sp[i] = 0;

			if (ep[i] > image_size[i] - 1)
				ep[i] = image_size[i] - 1;
		}
	}
}


template <typename T>
void JAT_detect<T>::ComputeGlobalDisplacementMaps(std::pair< vect3<double>, int > init_pos[3], const vect3<int>& stride, JAT_DMapType& dmaps)
{
	JAnatomyMap map_solver;

	map_solver.SetDetector(m_root_folder.c_str(), m_anatomy_name.c_str(), m_level, m_scale);
	if (!map_solver.ReadDetectorInfo(m_detector_info))
		err_message("fail to read detector info");

	mxImage<T> resampled_image;
	mxImageUtils::Resample(m_orig_image, m_detector_info.spacing, resampled_image, IT_LINEAR_INTERPOLATION);

	// compute bounding box
	vect3<int> sp, ep;
	ComputeGlobalInitialBox(resampled_image, m_detector_info.sample_radius, init_pos, sp, ep);
	map_solver.SetBoundingBox(sp, ep);

	if (m_detector_info.body_mask)
	{
		mxImageUtils::ExtractBodyFromCT<T,T>(resampled_image, dmaps.mask, &resampled_image);
		map_solver.SetMask(&dmaps.mask);
	}
	else
	{
		dmaps.mask.CopyImageInfo(resampled_image);
		dmaps.mask.SetImageSize(resampled_image.GetImageSize());
		dmaps.mask.Fill(255);
	}

	map_solver.SetStride(stride);
	map_solver.SetConstraintLandmarks(&m_constraint_landmarks);
	map_solver.ComputeDisplacementMap(resampled_image, m_fusion_type, dmaps.displacement_maps);

	// crop the mask to be the same size of displacementMaps
	mxImage<unsigned char> mask_copy;
	mxImageUtils::Copy(dmaps.mask, mask_copy);
	mxImageUtils::Crop2<unsigned char>(mask_copy, sp, ep, dmaps.mask, static_cast<unsigned char>(0));

	dmaps.stride = stride;
}


template <typename T>
void JAT_detect<T>::ComputeLocalDisplacementMaps(const JAT_anatomy_positions& init_positions, vect3<double>& radius, const vect3<int>& stride, JAT_DMapType& dmaps)
{
	JAnatomyMap map_solver;

	map_solver.SetDetector(m_root_folder.c_str(), m_anatomy_name.c_str(), m_level, m_scale);
	if (!map_solver.ReadDetectorInfo(m_detector_info))
		err_message("fail to read detector info");

	if (radius[0] < 0 || radius[1] < 0 || radius[2] < 0)
		radius = m_detector_info.sample_radius;

	mxImage<T> resampled_image;
	mxImageUtils::Resample(m_orig_image, m_detector_info.spacing, resampled_image, IT_LINEAR_INTERPOLATION);

	mxImage<unsigned char> sample_mask;
	init_positions.get_sample_map(0, resampled_image, radius, sample_mask, NULL);
	map_solver.SetMask(&sample_mask);

	vect3<int> sp, ep;
	init_positions.get_bounding_box(0, resampled_image, radius, sp, ep);
	map_solver.SetBoundingBox(sp, ep);

	map_solver.SetStride(stride);
	map_solver.SetConstraintLandmarks(&m_constraint_landmarks);
	map_solver.ComputeDisplacementMap(resampled_image, m_fusion_type, dmaps.displacement_maps);

	// crop the mask to be the same size of displacementMaps
	mxImageUtils::Crop2<unsigned char>(sample_mask, sp, ep, dmaps.mask, static_cast<unsigned char>(0));

	dmaps.stride = stride;
}


template <typename T>
void JAT_detect<T>::trilinear_vote(const vect3<double>& pos, mxImage<double>& vote_map)
{
	vect3<unsigned int> image_size = vote_map.GetImageSize();
	
	vect3<int> sp, ep;
	vect3<double> weight;
	for (int i = 0; i < 3; ++i)
	{
		sp[i] = static_cast<int>(pos[i]);
		ep[i] = sp[i] + 1;
		weight[i] = pos[i] - sp[i];
	}

	for (int z = sp[2]; z <= ep[2]; ++z)
	{
		for (int y = sp[1]; y <= ep[1]; ++y)
		{
			for (int x = sp[0]; x <= ep[0]; ++x)
			{
				if (!vote_map.PtInImage(x, y, z))
					continue;

				double vote = 1;

				if (z == sp[2])
					vote *= (1 - weight[2]);
				else
					vote *= weight[2];

				if (y == sp[1])
					vote *= (1 - weight[1]);
				else
					vote *= weight[1];

				if (z == sp[0])
					vote *= (1 - weight[0]);
				else
					vote *= weight[0];

				vote_map(x, y, z) += vote;
			}
		}
	}
}


template <typename T>
void JAT_detect<T>::nearest_vote(const vect3<double>& pos, mxImage<double>& vote_map)
{
	vect3<int> voxel;
	for (int i = 0; i < 3; ++i)
		voxel[i] = static_cast<int>(pos[i] + 0.5);

	if (vote_map.PtInImage(voxel[0], voxel[1], voxel[2]))
		vote_map(voxel[0], voxel[1], voxel[2]) += 1;
}


template <typename T>
void JAT_detect<T>::LandmarkGlobalVote(const JAT_DMapType& dmaps, int lm_idx, mxImage<double>& vote_map, bool allocateOutput)
{
	const std::vector< mxImage<double> >& displacementMaps = dmaps.displacement_maps;
	const mxImage<unsigned char>& mask = dmaps.mask;
	const vect3<int>& stride = dmaps.stride;

	assert_message( displacementMaps.size() > 0, "empty displacement maps" );
	assert_message( displacementMaps[0].GetImageSize() == mask.GetImageSize(), "image size mismatch between displacement map and mask" );
	
	int displacement_offset = lm_idx * 3;
	assert_message(displacement_offset >= 0 && displacement_offset + 2 < displacementMaps.size(), "displacement offset out of bound");

	vect3<unsigned int> image_size = displacementMaps[0].GetImageSize();
	if (allocateOutput)
	{
		vote_map.CopyImageInfo(displacementMaps[0]);
		vote_map.SetImageSize(image_size);
		vote_map.Fill(0);
	}

	// iterate all voxels
	for (unsigned int z = 0; z < image_size[2]; z += stride[2])
	{
		for (unsigned int y = 0; y < image_size[1]; y += stride[1])
		{
			for (unsigned int x = 0; x < image_size[0]; x += stride[0])
			{
				if (!mask(x, y, z))
					continue;

				double displacement[3] = { 0 };
				for (int j = 0; j < 3; ++j)
					displacement[j] = displacementMaps[displacement_offset + j](x, y, z);

				// vote
				vect3<double> voxel(x, y, z);
				for (int j = 0; j < 3; ++j)
					voxel[j] = voxel[j] + displacement[j];

				nearest_vote(voxel, vote_map);
				//trilinear_vote(voxel, vote_map);
			}
		}
	}
}


template <typename T>
void JAT_detect<T>::LandmarkLocalVote(const JAT_DMapType& dmaps, int lm_idx, const JAT_anatomy_positions& init_positions, vect3<double> radius, mxImage<double>& vote_map, bool allocateOutput)
{
	const std::vector< mxImage<double> >& displacementMaps = dmaps.displacement_maps;
	const mxImage<unsigned char>& mask = dmaps.mask;
	const vect3<int>& stride = dmaps.stride;

	assert_message( displacementMaps.size() > 0, "empty displacement maps" );
	assert_message( displacementMaps[0].GetImageSize() == mask.GetImageSize(), "image size mismatch between displacement map and mask" );

	int displacement_offset = lm_idx * 3;
	assert_message(displacement_offset >= 0 && displacement_offset + 2 < displacementMaps.size(), "displacement offset out of bound");

	vect3<int> box_sp, box_ep;
	init_positions.get_lm_bounding_box(0, displacementMaps[0], lm_idx, radius, box_sp, box_ep);

	vect3<unsigned int> displacement_image_size = displacementMaps[0].GetImageSize();

	if (allocateOutput)
	{
		vect3<double> origin;
		mxImageUtils::Voxel2World(displacementMaps[0], box_sp, origin);
		
		vect3<unsigned int> vote_image_size;
		for (int i = 0; i < 3; ++i)
			vote_image_size[i] = static_cast<unsigned int>(box_ep[i] - box_sp[i] + 1);

		vote_map.CopyImageInfo(displacementMaps[0]);
		vote_map.SetOrigin(origin);
		vote_map.SetImageSize(vote_image_size);
		vote_map.Fill(0);
	}

	// iterate all voxels
	for (unsigned int z = 0; z < displacement_image_size[2]; z += stride[2])
	{
		for (unsigned int y = 0; y < displacement_image_size[1]; y += stride[1])
		{
			for (unsigned int x = 0; x < displacement_image_size[0]; x += stride[0])
			{
				vect3<int> voxel(x, y, z);

				// check whether a voxel is within this bounding box
				if( voxel[0] < box_sp[0] || voxel[0] > box_ep[0] || voxel[1] < box_sp[1] || voxel[1] > box_ep[1] || voxel[2] < box_sp[2] || voxel[2] > box_ep[2] )
					continue;

				// check whether a voxel is in the mask
				if (!mask(x, y, z))
					continue;

				double displacement[3] = { 0 };
				for (int j = 0; j < 3; ++j)
					displacement[j] = displacementMaps[displacement_offset + j](x, y, z);

				// cast vote
				vect3<double> pos;
				for (int j = 0; j < 3; ++j)
					pos[j] = voxel[j] + displacement[j] - box_sp[j];

				nearest_vote(pos, vote_map);
			}
		}
	}	
}


template <typename T>
void JAT_detect<T>::LandmarkLocalVote(const JAT_DMapType& dmaps, int lm_idx, mxImage<double>& vote_map, bool allocateOutput)
{
	const std::vector< mxImage<double> >& displacementMaps = dmaps.displacement_maps;
	const mxImage<unsigned char>& mask = dmaps.mask;
	const vect3<int>& stride = dmaps.stride;

	assert_message(displacementMaps.size() > 0, "empty displacement maps");
	assert_message(displacementMaps[0].GetImageSize() == mask.GetImageSize(), "image size mismatch between displacement map and mask");

	int displacement_offset = lm_idx * 3;

	vect3<unsigned int> displacement_image_size = displacementMaps[0].GetImageSize();
	if (allocateOutput)
	{
		vote_map.CopyImageInfo(displacementMaps[0]);
		vote_map.SetImageSize(displacement_image_size);
		vote_map.Fill(0);
	}

	// iterate all voxels
	for (unsigned int z = 0; z < displacement_image_size[2]; z += stride[2])
	{
		for (unsigned int y = 0; y < displacement_image_size[1]; y += stride[1])
		{
			for (unsigned int x = 0; x < displacement_image_size[0]; x += stride[0])
			{
				vect3<int> voxel(x, y, z);

				// check whether a voxel is in the mask
				if (!mask(x, y, z))
					continue;

				double displacement[3] = { 0 };
				for (int j = 0; j < 3; ++j)
					displacement[j] = displacementMaps[displacement_offset + j](x, y, z);

				// cast vote
				vect3<double> pos;
				for (int j = 0; j < 3; ++j)
					pos[j] = voxel[j] + displacement[j];

				nearest_vote(pos, vote_map);
				//trilinear_vote(pos, vote_map);
			}
		}
	}
}


template <typename T>
void JAT_detect<T>::GlobalVoteMap(const JAT_DMapType& dmaps, const std::vector<int>& lm_idxs, mxImage<double>& vote_map)
{
	const std::vector< mxImage<double> >& displacementMaps = dmaps.displacement_maps;
	const mxImage<unsigned char>& mask = dmaps.mask;

	assert_message(displacementMaps.size() > 0, "empty displacement maps");
	assert_message(displacementMaps[0].GetImageSize() == mask.GetImageSize(), "image size mismatch between displacement map and mask");

	vect3<unsigned int> image_size = displacementMaps[0].GetImageSize();
	vote_map.CopyImageInfo(displacementMaps[0]);
	vote_map.SetImageSize(image_size);
	vote_map.Fill(0);

	for (size_t i = 0; i < lm_idxs.size(); ++i)
	{
		assert_message(lm_idxs[i] >= 0 && lm_idxs[i] < m_detector_info.anatomy_ids.size(), "landmark index out of bound");
		LandmarkGlobalVote(dmaps, lm_idxs[i], vote_map, false);
	}
}


template <typename T>
void JAT_detect<T>::LocalVoteMap(const JAT_DMapType& dmaps, int lm_idx, const JAT_anatomy_positions& init_positions, vect3<double> radius, mxImage<double>& vote_map)
{
	const std::vector< mxImage<double> >& displacementMaps = dmaps.displacement_maps;
	const mxImage<unsigned char>& mask = dmaps.mask;
	const vect3<int>& stride = dmaps.stride;

	assert_message(displacementMaps.size() > 0, "empty displacement maps");
	assert_message(displacementMaps[0].GetImageSize() == mask.GetImageSize(), "image size mismatch between displacement map and mask");

	LandmarkLocalVote(dmaps, lm_idx, init_positions, radius, vote_map, true);
}


// detect utility functions
template <typename T>
std::pair<JAT_lm_type, double> JAT_detect<T>::find_pos_by_max_vote(const mxImage<double>& vote_map)
{
	double max_vote = -std::numeric_limits<double>::max();
	vect3<unsigned int> lanmdark_voxel;

	vect3<unsigned int> image_size = vote_map.GetImageSize();

	for (unsigned int z = 0; z < image_size[2]; ++z)
	{
		for (unsigned int y = 0; y < image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < image_size[0]; ++x)
			{
				if (vote_map(x, y, z) > max_vote)
				{
					max_vote = vote_map(x, y, z);
					lanmdark_voxel[0] = x; lanmdark_voxel[1] = y; lanmdark_voxel[2] = z;
				}
			}
		}
	}

	vect3<double> landmark_world;
	mxImageUtils::Voxel2World(vote_map, lanmdark_voxel, landmark_world);

	return std::make_pair(landmark_world, max_vote);
}


template <typename T>
void JAT_detect<T>::mask_pos(mxImage<double>& vote_map, const vect3<double>& pos, const vect3<double>& radius)
{
	vect3<int> voxel;
	mxImageUtils::World2Voxel(vote_map, pos, voxel);

	vect3<double> spacing = vote_map.GetSpacing();
	vect3<int> voxel_radius;
	for (int i = 0; i < 3; ++i)
		voxel_radius[i] = static_cast<int>(radius[i] / spacing[i] + 0.5);

	vect3<int> sp, ep;
	for (int i = 0; i < 3; ++i)
	{
		sp[i] = voxel[i] - voxel_radius[i];
		ep[i] = voxel[i] + voxel_radius[i];
	}

	for (int z = sp[2]; z <= ep[2]; ++z)
	{
		for (int y = sp[1]; y <= ep[1]; ++y)
		{
			for (int x = sp[0]; x <= ep[0]; ++x)
			{
				if (!vote_map.PtInImage(x, y, z))
					continue;

				vote_map(x, y, z) = 0;
			}
		}
	}
}


template <typename T>
vect3<double> JAT_detect<T>::DetectLandmarkByMaxVote(const mxImage<double>& vote_map)
{
	std::pair<JAT_lm_type, double> pos = find_pos_by_max_vote(vote_map);
	return pos.first;
}


template <typename T>
JAT_lm_type JAT_detect<T>::DetectLandmarkByAdaptiveThreshold(const mxImage<double>& vote_map, double sigma, double threshold_ratio)
{
	mxImage<double> smooth_map;
	if (sigma < 0)
		mxImageUtils::Copy(vote_map, smooth_map);
	else
		mxImageUtils::GaussianSmooth(vote_map, smooth_map, sigma);

	vect3<unsigned int> image_size = vote_map.GetImageSize();

	double max_vote = mxImageUtils::MaxIntensity(smooth_map);
	double threshold = max_vote * threshold_ratio;
	mxImageUtils::Threshold<double>(smooth_map, threshold, 255.0, 0.0);

	vect3<double> landmark(0, 0, 0);
	int count = 0;

	for (unsigned int z = 0; z < image_size[2]; ++z)
	{
		for (unsigned int y = 0; y < image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < image_size[0]; ++x)
			{
				if (smooth_map(x, y, z) > 128)
				{
					landmark[0] += x;
					landmark[1] += y;
					landmark[2] += z;

					++count;
				}
			}
		}
	}

	for (int i = 0; i < 3; ++i)
		landmark[i] /= count;

	mxImageUtils::Voxel2World(vote_map, landmark);
	return landmark;
}


template <typename T>
JAT_lm_type JAT_detect<T>::DetectLandmarkByMultMaxVote(const mxImage<double>& vote_map, double smooth_sigma, double threshold_ratio, const vect3<double>& mask_radius, int candidate_num, JAT_select_type select_type)
{
	mxImage<double> copy_map;
	if (smooth_sigma > 0)
		mxImageUtils::GaussianSmooth(vote_map, copy_map, smooth_sigma);
	else
		mxImageUtils::Copy(vote_map, copy_map);

	double max_vote = -std::numeric_limits<double>::max();
	vect3<double> ret;

	for (int i = 0; i < candidate_num; ++i)
	{
		std::pair<JAT_lm_type, double> detected_pos = find_pos_by_max_vote(copy_map);
		
		// std::cout << "pos: " << detected_pos.first << ", vote: " << detected_pos.second << std::endl;

		if (i == 0)
		{
			max_vote = detected_pos.second;
			ret = detected_pos.first;
		}
		else
		{
			if (detected_pos.second < max_vote * threshold_ratio)
				break;

			switch (select_type)
			{
			case BRIC::IDEA::FISH::JAT_X_SMALL:

				if (detected_pos.first[0] < ret[0])
					ret = detected_pos.first;
				break;

			case BRIC::IDEA::FISH::JAT_X_LARGE: 
				
				if (detected_pos.first[0] > ret[0])
					ret = detected_pos.first;
				break;

			case BRIC::IDEA::FISH::JAT_Y_SMALL: 
				
				if (detected_pos.first[1] < ret[1])
					ret = detected_pos.first;
				break;

			case BRIC::IDEA::FISH::JAT_Y_LARGE: 
				
				if (detected_pos.first[1] > ret[1])
					ret = detected_pos.first;
				break;

			case BRIC::IDEA::FISH::JAT_Z_SMALL: 
				
				if (detected_pos.first[2] < ret[2])
					ret = detected_pos.first;
				break;

			case BRIC::IDEA::FISH::JAT_Z_LARGE: 
				
				if (detected_pos.first[2] > ret[2])
					ret = detected_pos.first;
				break;

			default: err_message("unrecognized select type");
			}
		}

		mask_pos(copy_map, detected_pos.first, mask_radius);
	}

	return ret;
}


template <typename T>
void JAT_detect<T>::GlobalDetectAnatomy(const char* root_folder, const JAT_detect_config_unit& step, std::pair< vect3<double>, int > init_pos[3], JAT_anatomy_positions& positions)
{
	// get global displacement maps;
	SetDetector(root_folder, step.name, step.level, step.scale);

	JAT_DMapType dmaps;

	vect3<int> stride(step.stride, step.stride, step.stride);
	ComputeGlobalDisplacementMaps(init_pos, stride, dmaps);

	// setup the position structure
	positions.image_num = 1;
	positions.ids.resize(m_detector_info.anatomy_ids.size());
	::memcpy(&(positions.ids[0]), &(m_detector_info.anatomy_ids[0]), sizeof(int) * m_detector_info.anatomy_ids.size() );

	positions.lm_worlds.resize(1);
	positions.lm_worlds[0].resize( positions.ids.size() );

	// detect
	for (size_t i = 0; i < m_detector_info.anatomy_ids.size(); ++i)
	{
		mxImage<double> vote_map;
		LandmarkGlobalVote(dmaps, i, vote_map);

		if (step.detect_type == JAT_MAX_VOTE)
			positions.lm_worlds[0][i] = DetectLandmarkByMaxVote(vote_map);
		else if (step.detect_type == JAT_ADAPTIVE_THRESHOLD)
			positions.lm_worlds[0][i] = DetectLandmarkByAdaptiveThreshold(vote_map, step.smooth_sigma, step.threshold_ratio);
		else if (step.detect_type == JAT_MULT_MAX_VOTE)
			positions.lm_worlds[0][i] = DetectLandmarkByMultMaxVote(vote_map, step.smooth_sigma, step.threshold_ratio, step.mask_radius, step.candidate_num, step.select_type);
		else
			err_message("unrecognized detection type");
	}
}


template <typename T>
void JAT_detect<T>::LocalDetectAnatomy(const char* root_folder, const JAT_detect_config_unit& step, JAT_anatomy_positions& positions)
{
	// set local displacement maps
	SetDetector(root_folder, step.name, step.level, step.scale);

	JAT_DMapType dmaps;
	vect3<double> radius = vect3<double>(-1, -1, -1);

	vect3<int> stride(step.stride, step.stride, step.stride);
	ComputeLocalDisplacementMaps(positions, radius, stride, dmaps);

	// detect / refine
	for (size_t i = 0; i < m_detector_info.anatomy_ids.size(); ++i)
	{
		mxImage<double> vote_map;

		if (step.vote_type == JAT_LOCAL_VOTE)
			LandmarkLocalVote(dmaps, i, positions, radius, vote_map);
		else if (step.vote_type == JAT_JOINT_VOTE)
			LandmarkLocalVote(dmaps, i, vote_map);
		else
			err_message("unrecognized vote type");

		if (step.detect_type == JAT_MAX_VOTE)
			positions.lm_worlds[0][i] = DetectLandmarkByMaxVote(vote_map);
		else if (step.detect_type == JAT_ADAPTIVE_THRESHOLD)
			positions.lm_worlds[0][i] = DetectLandmarkByAdaptiveThreshold(vote_map, step.smooth_sigma, step.threshold_ratio);
		else if (step.detect_type == JAT_MULT_MAX_VOTE)
			positions.lm_worlds[0][i] = DetectLandmarkByMultMaxVote(vote_map, step.smooth_sigma, step.threshold_ratio, step.mask_radius, step.candidate_num, step.select_type);
		else
			err_message("unrecognized detection type");
	}
}


template <typename T>
void JAT_detect<T>::DetectAnatomy( const char* root_folder, const JAT_detect_config& config, JAT_anatomy_positions& positions )
{
	assert_message(config.m_steps.size() > 0, "empty step information");

	// compute the required landmarks 
	std::map< int, vect3<double> > required_landmarks;
	ComputeRequiredLandmarks(root_folder, config, required_landmarks);
	SetConstraintLandmarks(required_landmarks);

	std::pair< vect3<double>, int > global_init[3];
	GetGlobalInitialPosition(required_landmarks, config, global_init);

	{
		//boost::timer::auto_cpu_timer timer;
		GlobalDetectAnatomy(root_folder, config.m_steps[0], global_init, positions);
		//std::cout << "(G0)";
	}
	
	for (size_t i = 1; i < config.m_steps.size(); ++i) {
		//boost::timer::auto_cpu_timer timer;
		LocalDetectAnatomy(root_folder, config.m_steps[i], positions);
		//std::cout << "(L" << i << ")";
	}
}


template <typename T>
void JAT_detect<T>::GetGlobalInitialPosition(const std::map<int, vect3<double> >& required_landmarks, const JAT_detect_config& config, std::pair< vect3<double>, int > init_pos[3])
{
	for (int i = 0; i < 3; ++i)
	{
		if (config.m_init.names[i].size() == 0)
		{
			init_pos[i].first = vect3<double>(0, 0, 0);
			init_pos[i].second = -1;
			continue;
		}

		int init_id = stringUtils::str2num<int>(config.m_init.names[i]);
		int init_dim = config.m_init.dims[i];

		if (init_id < 0)
		{
			init_pos[i].first = vect3<double>(0, 0, 0);
			init_pos[i].second = -1;
		}
		else
		{
			std::map< int, vect3<double> >::const_iterator it = required_landmarks.find(init_id);
			if (it == required_landmarks.end())
				err_message( (std::string("landmark ") + stringUtils::num2str<int>(init_id) + std::string(" not found")).c_str() );

			init_pos[i].first = it->second;
			init_pos[i].second = init_dim;
		}
	}
}


template <typename T>
void JAT_detect<T>::ComputeRequiredLandmarks(const char* root_folder, const JAT_detect_config& config, std::map< int, vect3<double> >& all_landmarks)
{
	// landmarks required in the initialization
	for (int i = 0; i < 3; ++i)
	{
		if (config.m_init.names[i].size() == 0)
			continue;

		int id = stringUtils::str2num<int>(config.m_init.names[i]);
		if (id < 0)
			continue;

		all_landmarks[id] = vect3<double>(0, 0, 0);
	}

	// get landmark names
	for (size_t i = 0; i < config.m_steps.size(); ++i)
	{
		JAnatomyMap aux;
		aux.SetDetector(root_folder, config.m_steps[i].name.c_str(), config.m_steps[i].level, config.m_steps[i].scale);

		JATDetectorInfo info;
		if (!aux.ReadDetectorInfo(info))
			err_message("fail to read detector info");

		for (size_t j = 0; j < info.constraint_landmarks.size(); ++j)
		{
			int lm_id = stringUtils::str2num<int>(info.constraint_landmarks[j]);

			if (lm_id < 0)
				err_message("negative constraint landmark id");

			all_landmarks[lm_id] = vect3<double>(0,0,0);
		}
	}

	// compute all required landmarks
	
	// skip if no required landmarks
	if (all_landmarks.size() == 0)
		return;

	// backup the current state
	std::string old_root_folder = m_root_folder;
	std::string old_anatomy_name = m_anatomy_name;
	int old_level = m_level;
	int old_scale = m_scale;
	JATDetectorInfo old_detector_info = m_detector_info;
	SimpleRegressor::FusionType old_fusion_type = m_fusion_type;

	std::map<int, vect3<double> >::const_iterator lm_it;

	// detect the required landmarks
	lm_it = all_landmarks.begin();
	while (lm_it != all_landmarks.end())
	{
		int lm_id = lm_it->first;
		std::string sub_config_path = std::string(root_folder) + stringUtils::Slash() + "config" + stringUtils::num2str(lm_id) + ".ini";

		JAT_detect_config sub_config;
		if (!sub_config.read(sub_config_path.c_str()))
			err_message("fail to read configuration for constraint landmark detection");

		JAT_anatomy_positions sub_position;
		DetectAnatomy(root_folder, sub_config, sub_position);

		vect3<double> lm_world = sub_position.get_lm_world(0, 0);
		all_landmarks[lm_id] = lm_world;

		++lm_it;
	}

	// print out constraint landmarks
	lm_it = all_landmarks.begin();
	while (lm_it != all_landmarks.end())
	{
		std::cout << "Pre-detected Landmarks (" << lm_it->first << "): " << lm_it->second << std::endl;
		++lm_it;
	}

	// restore the previous state
	m_root_folder = old_root_folder;
	m_anatomy_name = old_anatomy_name;
	m_level = old_level;
	m_scale = old_scale;
	m_detector_info = old_detector_info;
	m_fusion_type = old_fusion_type;
}


} } }


#endif