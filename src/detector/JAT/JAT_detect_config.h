//
//  JAT_detect_config.h
//  FISH
//
//  Created by Yaozong Gao on 07/25/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __JAT_detect_config_h__
#define __JAT_detect_config_h__

#include <vector>
#include <string>
#include "extern/BoostHelper.h"

namespace BRIC { namespace IDEA { namespace FISH {

enum JAT_detect_type
{
	JAT_MAX_VOTE,
	JAT_ADAPTIVE_THRESHOLD,
	JAT_MULT_MAX_VOTE
};

enum JAT_vote_type
{
	JAT_LOCAL_VOTE,
	JAT_JOINT_VOTE
};

// options for multiple max votes
enum JAT_select_type
{
	JAT_X_SMALL,
	JAT_X_LARGE,
	JAT_Y_SMALL,
	JAT_Y_LARGE,
	JAT_Z_SMALL,
	JAT_Z_LARGE
};


struct JAT_detect_config_unit
{
	// basic information
	std::string name;
	int scale;
	int level;
	std::vector<int> input;

	int stride;

	JAT_detect_type detect_type;
	JAT_vote_type vote_type;

	/* specific information for each detect type */
	double threshold_ratio;							// for relative thresholding ratio

	/* adaptive threshold */
	double smooth_sigma;							// for smoothing the voting map

	/* multiple max vote */
	vect3<double> mask_radius;				// radius for masking out each vote
	int candidate_num;								// maximum number for searching peak candidates
	JAT_select_type select_type;			// position select type

	/* initialization */
	JAT_detect_config_unit() 
	{
		scale = -1;
		level = -1;
		stride = 0;
		detect_type = JAT_MAX_VOTE;
		vote_type = JAT_LOCAL_VOTE;
		threshold_ratio = 0.8;
		smooth_sigma = -1;
		mask_radius = vect3<double>(-1,-1,-1);
		candidate_num = 0;
		select_type = JAT_X_SMALL;
	}
};


struct JAT_init_unit
{
	std::string names[3];
	int dims[3];
};


class JAT_detect_config
{
public:
	
	bool read(const char* path);
	static bool write_sample(const char* path);

	JAT_init_unit m_init;
	std::vector< JAT_detect_config_unit > m_steps;
};


//////////////////////////////////////////////////////////////////////////


bool JAT_detect_config::read(const char* path)
{
	using boost::property_tree::ptree;

	ptree pt;
	try{
		boost::property_tree::ini_parser::read_ini(path, pt);
	}
	catch (boost::property_tree::ini_parser_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	try {
		int step_num = pt.get<int>("General.StepNumber");
		assert_message(step_num > 0, "step number must be >= 1");

		// begin read init unit
		boost::optional<std::string> str_names = pt.get_optional<std::string>("Init.Names");
		boost::optional<std::string> str_dims = pt.get_optional<std::string>("Init.Dims");

		if (!str_names || !str_dims)
		{
			m_init.names[0] = m_init.names[1] = m_init.names[2] = "";
			m_init.dims[0] = m_init.dims[1] = m_init.dims[2] = -1;
		}
		else
		{
			if (str_names.get().size() == 0 || str_dims.get().size() == 0)
			{
				m_init.names[0] = m_init.names[1] = m_init.names[2] = "";
				m_init.dims[0] = m_init.dims[1] = m_init.dims[2] = -1;
			}
			else
			{
				// parse name string
				std::vector<std::string> tokens = stringUtils::Split(str_names.get(), ' ');
				
				for (int i = 0; i < 3; ++i)
					m_init.names[i] = "";

				int end = std::min<int>(tokens.size(), 3);
				for (int i = 0; i < end; ++i)
					m_init.names[i] = tokens[i];

				// parse dimension string
				tokens = stringUtils::Split(str_dims.get(), ' ');

				for (int i = 0; i < 3; ++i)
					m_init.dims[i] = -1;

				end = std::min<int>(tokens.size(), 3);
				for (int i = 0; i < end; ++i)
					m_init.dims[i] = stringUtils::str2num<int>(tokens[i]);
			}
		}
		// end read init unit

		m_steps.resize(step_num);
		for (int i = 0; i < step_num; ++i)
		{
			std::string section_name = "Detector" + stringUtils::num2str(i+1);
			m_steps[i].name = pt.get<std::string>(section_name + ".name");
			m_steps[i].level = pt.get<int>(section_name + ".level");
			m_steps[i].scale = pt.get<int>(section_name + ".scale");
			m_steps[i].stride = pt.get<int>(section_name + ".stride");

			std::string str_type = pt.get<std::string>(section_name + ".detect");
			if (str_type == "MAX_VOTE")
				m_steps[i].detect_type = JAT_MAX_VOTE;
			else if (str_type == "ADAPTIVE_THRESHOLD")
			{
				m_steps[i].detect_type = JAT_ADAPTIVE_THRESHOLD;
				m_steps[i].smooth_sigma = pt.get<double>(section_name + ".smooth_sigma");
				m_steps[i].threshold_ratio = pt.get<double>(section_name + ".threshold_ratio");
			}
			else if (str_type == "MULT_MAX_VOTE")
			{
				m_steps[i].detect_type = JAT_MULT_MAX_VOTE;
				m_steps[i].smooth_sigma = pt.get<double>(section_name + ".smooth_sigma");
				m_steps[i].threshold_ratio = pt.get<double>(section_name + ".threshold_ratio");
				m_steps[i].mask_radius = stringUtils::ParseVect3<double>(pt.get<std::string>(section_name + ".mask_radius"), ' ');
				m_steps[i].candidate_num = pt.get<int>(section_name + ".candidate_num");

				std::string str_select_type = pt.get<std::string>(section_name + ".select_type");
				if (str_select_type == "X_SMALL")
					m_steps[i].select_type = JAT_X_SMALL;
				else if (str_select_type == "X_LARGE")
					m_steps[i].select_type = JAT_X_LARGE;
				else if (str_select_type == "Y_SMALL")
					m_steps[i].select_type = JAT_Y_SMALL;
				else if (str_select_type == "Y_LARGE")
					m_steps[i].select_type = JAT_Y_LARGE;
				else if (str_select_type == "Z_SMALL")
					m_steps[i].select_type = JAT_Z_SMALL;
				else if (str_select_type == "Z_LARGE")
					m_steps[i].select_type = JAT_Z_LARGE;
				else
					err_message("unrecognized select type for MULT_MAX_VOTE");

			}else {
				err_message("unrecognized detection type");
			}

			std::string str_vote_type = pt.get<std::string>(section_name + ".vote");
			if (str_vote_type == "LOCAL")
				m_steps[i].vote_type = JAT_LOCAL_VOTE;
			else if (str_vote_type == "JOINT")
				m_steps[i].vote_type = JAT_JOINT_VOTE;
			else
				err_message("unrecognized vote type");

			if (i == 0)
				m_steps[i].input.clear();
			else
			{
				std::string input_str = pt.get<std::string>(section_name + ".input");
				if (input_str == "-1")
					m_steps[i].input.clear();
				else
					BoostCmdHelper::ParseIdxOption(input_str, m_steps[i].input);
			}
		}
	}
	catch (boost::property_tree::ptree_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}


bool JAT_detect_config::write_sample(const char* path)
{
	const char* text[] = {
		"[General]\n",
		"StepNumber=3\n",
		"\n",
		"[Init]\n",
		"Names=201 -1 -1\n",
		"Dims=0 -1 -1\n",
		"\n",
		"[Detector1]\n",
		"name=all\n",
		"scale=3\n",
		"level=1\n",
		"stride=2\n",
		"detect=ADAPTIVE_THRESHOLD\n",
		"vote=LOCAL\n",
		"smooth_ratio=1\n",
		"threshold_ratio=0.8\n",
		"\n",
		"[Detector2]\n",
		"name=all\n",
		"input=-1\n",
		"scale=2\n",
		"level=1\n",
		"stride=2\n",
		"detect=MAX_VOTE\n",
		"vote=LOCAL\n",
		"\n",
		"[Detector3]\n",
		"name=pro\n",
		"input=1-6\n",
		"scale=2\n",
		"level=1\n"
		"stride=2\n",
		"detect=MULT_MAX_VOTE\n",
		"vote=LOCAL\n",
		"threshold_ratio=0.8\n",
		"mask_radius=5.0 5.0 5.0\n",
		"candidate_num=2\n",
		"select_type=X_SMALL\n"
	};

	FILE* fp = fopen(path, "w");
	if (!fp) {
		std::cerr << "fail to open file for write: " << path << std::endl;
		return false;
	}

	int num_lines = sizeof(text) / sizeof(const char*);
	for (int i = 0; i < num_lines; ++i)
		fprintf(fp, text[i]);
	fclose(fp);

	return true;
}


} } }

#endif