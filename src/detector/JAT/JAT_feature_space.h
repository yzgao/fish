//
//  JAT_feature_space.h
//  FISH
//
//  Created by Yaozong Gao on 9/23/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __JAT_feature_space_h__
#define __JAT_feature_space_h__

#include "feature/IFeatureSpace.h"
#include "feature/haar3d/Haar3DFeatures.h"
#include "detector/JAT/JAT_ini_struct.h"

namespace BRIC { namespace IDEA { namespace FISH {

class JAT_feature_space;

class JAT_image_feature_functor: public IImageFeatureFunctor
{
public:
	typedef JAT_feature_space FeatureSpaceType;

	JAT_image_feature_functor(const JAT_feature_space& space);
	~JAT_image_feature_functor();
	double operator() (unsigned int featureIndex) const;

	// overrides
	virtual void SetImage(const mxImage<double>& image, int index);
	virtual void SetVoxel(const vect3<unsigned int>& pos, int index);
	virtual double GetResponse(unsigned int featureIndex) const;
	virtual void PreprocessInput(mxImage<double>& image, int index) const;

	void SetConstraintLandmarks(const std::map< int, vect3<double> >* landmarks);
	double ComputeGeometricFeatures(const vect3<double>& pos, int gfeatIdx) const;

private:

	const JAT_feature_space* m_featureSpace;
	const mxImage<double>* m_image;
	vect3<unsigned int> m_position;

	const std::map<int, vect3<double> >* m_constraintLandmarks;

	vect3<unsigned int> m_intensity_patchsize;
};


class JAT_feature_space : public IFeatureSpace
{
private:
	JAT_feature_space(const JAT_feature_space& copy);
	JAT_feature_space& operator=(const JAT_feature_space& rhs);

public:
	friend class JAT_image_feature_functor;

	typedef JAT_image_feature_functor ImageFunctorType;

	JAT_feature_space();
	~JAT_feature_space();

	/** @brief interfaces */
	virtual void RandomizeFeatureSpace(Random& random, unsigned int numFeatures);
	virtual IPatchFeatureFunctor* CreatePatchFeatureFunctor() const;	// not supported yet
	virtual JAT_image_feature_functor* CreateImageFeatureFunctor() const;
	virtual vect3<unsigned int> GetPatchSize(int index) const;
	virtual int GetFeatureNumber() const;
	virtual int GetNumberFeatureSpaces() const;
	virtual void Serialize(std::ostream& o) const;
	static JAT_feature_space* Deserialize(std::istream& in);

	/** @brief access */
	void AddIntensityFilterSize(const vect3<unsigned int>& size);
	void SetIntensityPatchSize(const vect3<int>& patch_size);
	vect3<int> GetIntensityPatchSize() const;

	void SetGeometricRules(const std::vector<GeometricRule>& rules);
	int GetNumOfGeometricFeatures() const;

private:

	Haar3DRect RandomizeHaar3DRect(Random& random, const std::vector< vect3<unsigned int> >& filtersizes, const vect3<int>& patchsize) const;
	void RandomizeIntensityFeatures(Random& random, int start_idx, int num);

	std::vector< Haar3DFilter > m_filters;

	// geometric ids and codes
	std::vector<int> m_gids1;
	std::vector<int> m_gids2;
	std::vector<int> m_gcodes;

	std::vector< vect3<unsigned int> > m_intensityFilterSizes;			// used to generate random intensity Haar filters
	vect3<int> m_intensity_patchsize;									// as the name suggests

	static const char* m_binaryHeader;									// name of feature space
	FS_REGISTER_DEC_TYPE(JAT_feature_space);
};


//////////////////////////////////////////////////////////////////////////
// JAT_image_feature_functor

JAT_image_feature_functor::JAT_image_feature_functor(const JAT_feature_space& space)
{
	m_featureSpace = &space;
	m_image = NULL;
	m_intensity_patchsize = space.m_intensity_patchsize.to<unsigned int>();
}

JAT_image_feature_functor::~JAT_image_feature_functor()
{
}

double JAT_image_feature_functor::operator () (unsigned int featureIndex) const
{
	return GetResponse(featureIndex);
}

void JAT_image_feature_functor::SetImage(const mxImage<double>& image, int index)
{
	assert(index == 0);
	m_image = &image;
}

void JAT_image_feature_functor::SetConstraintLandmarks(const std::map<int, vect3<double> >* landmarks)
{
	m_constraintLandmarks = landmarks;
}

void JAT_image_feature_functor::SetVoxel(const vect3<unsigned int>& pos, int index)
{
	assert(index == 0);
	m_position = pos;
}

double JAT_image_feature_functor::ComputeGeometricFeatures(const vect3<double>& pos, int gfeatIdx) const
{
	assert_message(gfeatIdx >= 0 && gfeatIdx < m_featureSpace->GetNumOfGeometricFeatures(), "geometric index out of bound");
	assert_message(m_constraintLandmarks != NULL, "constraint landmarks missing");

	int idx1 = m_featureSpace->m_gids1[gfeatIdx];
	int idx2 = m_featureSpace->m_gids2[gfeatIdx];
	GeometricCode code = static_cast<GeometricCode>(m_featureSpace->m_gcodes[gfeatIdx]);

	std::map<int, vect3<double> >::const_iterator it;
	vect3<double> target1, target2, diff1, diff2;

	switch (code)
	{
	case COORD_X:
	case COORD_Y:
	case COORD_Z:

		it = m_constraintLandmarks->find(idx1);
		if (it == m_constraintLandmarks->end())
			err_message( (std::string("landmark ") + stringUtils::num2str(idx1) + " missing").c_str() );

		target1 = it->second;
		diff1 = target1 - pos;
		return diff1[static_cast<int>(code)];

	case MEAN_COORD_X:
	case MEAN_COORD_Y:
	case MEAN_COORD_Z:

		it = m_constraintLandmarks->find(idx1);
		if (it == m_constraintLandmarks->end())
			err_message( (std::string("landmark ") + stringUtils::num2str(idx1) + "missing").c_str() );
		target1 = it->second;
		
		it = m_constraintLandmarks->find(idx2);
		if (it == m_constraintLandmarks->end())
			err_message( (std::string("landmark ") + stringUtils::num2str(idx2) + "missing").c_str() );
		target2 = it->second;

		diff1 = (target1 + target2) / 2.0 - pos;
		return diff1[static_cast<int>(code)];

	case NORM:

		it = m_constraintLandmarks->find(idx1);
		if (it == m_constraintLandmarks->end())
			err_message((std::string("landmark ") + stringUtils::num2str(idx1) + "missing").c_str());
		target1 = it->second;

		diff1 = target1 - pos;
		return diff1.norm();

	case NORM_DIFF:

		it = m_constraintLandmarks->find(idx1);
		if (it == m_constraintLandmarks->end())
			err_message((std::string("landmark ") + stringUtils::num2str(idx1) + "missing").c_str());
		target1 = it->second;

		it = m_constraintLandmarks->find(idx2);
		if (it == m_constraintLandmarks->end())
			err_message((std::string("landmark ") + stringUtils::num2str(idx2) + "missing").c_str());
		target2 = it->second;

		diff1 = target1 - pos;
		diff2 = target2 - pos;

		return diff1.norm() - diff2.norm();

	default: 
		
		err_message("unrecognized geometric code"); 
	}

	return 0;
}

double JAT_image_feature_functor::GetResponse(unsigned int featureIndex) const
{
	assert(featureIndex >= 0 && featureIndex < m_featureSpace->GetFeatureNumber());

	if (featureIndex < m_featureSpace->m_filters.size())
	{
		const Haar3DFilter& filter = m_featureSpace->m_filters[featureIndex];
		return filter.GetResponseFromImage(*m_image, m_position, m_intensity_patchsize);
	}
	else {
		int geometric_idx = featureIndex - m_featureSpace->m_filters.size();

		vect3<double> world_pos;
		mxImageUtils::Voxel2World(*m_image, m_position, world_pos);
		return ComputeGeometricFeatures(world_pos, geometric_idx);
	}
}

void JAT_image_feature_functor::PreprocessInput(mxImage<double>& image, int index) const
{
	Haar3DBasics::IntegralImage(image);
}


//////////////////////////////////////////////////////////////////////////
// JAT_feature_space implementations

JAT_feature_space::JAT_feature_space()
{
}

JAT_feature_space::~JAT_feature_space()
{
}

void JAT_feature_space::SetGeometricRules(const std::vector<GeometricRule>& rules)
{
	if (rules.size() == 0)
	{
		m_gids1.clear();
		m_gids2.clear();
		m_gcodes.clear();
		return;
	}

	m_gids1.resize(rules.size());
	m_gids2.resize(rules.size());
	m_gcodes.resize(rules.size());

	for (size_t i = 0; i < rules.size(); ++i)
	{
		m_gids1[i] = rules[i].id1;
		m_gids2[i] = rules[i].id2;
		m_gcodes[i] = static_cast<int>(rules[i].code);
	}
}

void JAT_feature_space::AddIntensityFilterSize(const vect3<unsigned int>& size)
{
	m_intensityFilterSizes.push_back(size);
}

void JAT_feature_space::SetIntensityPatchSize(const vect3<int>& patch_size)
{
	m_intensity_patchsize = patch_size;
}

vect3<int> JAT_feature_space::GetIntensityPatchSize() const
{
	return m_intensity_patchsize;
}

int JAT_feature_space::GetNumOfGeometricFeatures() const
{
	return static_cast<int>(m_gids1.size());
}

Haar3DRect JAT_feature_space::RandomizeHaar3DRect(Random& random, const std::vector< vect3<unsigned int> >& filter_sizes, const vect3<int>& patch_size) const
{
	Haar3DRect rect;

	vect3<unsigned int> filterSize = filter_sizes[random.Next(0, static_cast<int>(filter_sizes.size()) - 1)];
	rect.SetSize(filterSize);

	vect3<unsigned int> lowerBound, upperBound;
	for (int k = 0; k < 3; ++k)
	{
		lowerBound[k] = filterSize[k] / 2;
		upperBound[k] = patch_size[k] + filterSize[k] / 2 - filterSize[k];
	}

	vect3<unsigned int> center;
	for (int k = 0; k < 3; ++k)
		center[k] = random.Next(lowerBound[k], upperBound[k]);

	rect.SetCenter(center);

	if (random.NextDouble() > 0.5)
		rect.SetFactor(1);
	else
		rect.SetFactor(-1);

	return rect;
}

void JAT_feature_space::RandomizeIntensityFeatures(Random& random, int start_idx, int num)
{
	for (unsigned int i = 0; i < m_intensityFilterSizes.size(); ++i)
	{
		if (m_intensityFilterSizes[i][0] > m_intensity_patchsize[0] || m_intensityFilterSizes[i][1] > m_intensity_patchsize[1] ||
			m_intensityFilterSizes[i][2] > m_intensity_patchsize[2])
			err_message("filter size is larger than patch size");
	}

	int end_idx = start_idx + num;
	for (int i = start_idx; i < end_idx; ++i)
	{
		int numRects = random.Next(1, 2);	// either one block or two blocks

		if (numRects == 1)
		{
			Haar3DRect rect = RandomizeHaar3DRect(random, m_intensityFilterSizes, m_intensity_patchsize);
			rect.SetFactor(1.0);
			m_filters[i].AddRect(rect);
		}
		else if (numRects == 2)
		{
			Haar3DRect rect1 = RandomizeHaar3DRect(random, m_intensityFilterSizes, m_intensity_patchsize);
			Haar3DRect rect2 = RandomizeHaar3DRect(random, m_intensityFilterSizes, m_intensity_patchsize);

			rect1.SetFactor(1.0);
			rect2.SetFactor(-1.0);

			m_filters[i].AddRect(rect1);
			m_filters[i].AddRect(rect2);
		}
		else
			err_message("number of rectangles > 2, unexpected");
	}
}

void JAT_feature_space::RandomizeFeatureSpace(Random& random, unsigned int numFeatures)
{
	assert_message(m_intensityFilterSizes.size() > 0, "no intensity filter size is specified");

	// allocate buffer for filters
	m_filters.resize(numFeatures);

	// random filters
	RandomizeIntensityFeatures(random, 0, numFeatures);
}

IPatchFeatureFunctor* JAT_feature_space::CreatePatchFeatureFunctor() const
{
	err_message("Patch feature functor not supported");
	return NULL;
}

JAT_image_feature_functor* JAT_feature_space::CreateImageFeatureFunctor() const
{
	return new JAT_image_feature_functor(*this);
}

vect3<unsigned int> JAT_feature_space::GetPatchSize(int index) const
{
	assert(index == 0);
	return m_intensity_patchsize.to<unsigned int>();
}

int JAT_feature_space::GetFeatureNumber() const
{
	return static_cast<int>(m_filters.size() + GetNumOfGeometricFeatures());
}

int JAT_feature_space::GetNumberFeatureSpaces() const
{
	return 1;
}

void JAT_feature_space::Serialize(std::ostream& o) const
{
	char header[BinaryHeaderSize] = { 0 };
	strncpy(header, m_binaryHeader, strlen(m_binaryHeader) + 1);
	o.write(header, BinaryHeaderSize);

	// write out intensity patch
	m_intensity_patchsize.Serialize(o);

	// write out filters
	unsigned int size = static_cast<unsigned int>(m_filters.size());
	o.write((const char*)(&size), sizeof(unsigned int));
	for (unsigned int i = 0; i < size; ++i)
		m_filters[i].Serialize(o);

	// write out geometric descriptors
	size = static_cast<unsigned int>(m_gids1.size());
	o.write((const char*)(&size), sizeof(unsigned int));
	if (size > 0)
	{
		o.write((const char*)(&(m_gids1[0])), sizeof(int) * size);
		o.write((const char*)(&(m_gids2[0])), sizeof(int) * size);
		o.write((const char*)(&(m_gcodes[0])), sizeof(int) * size);
	}
	
	// write out others
	size = static_cast<unsigned int>(m_intensityFilterSizes.size());
	o.write((const char*)(&size), sizeof(unsigned int));
	for (unsigned int i = 0; i < size; ++i)
		m_intensityFilterSizes[i].Serialize(o);

	o.flush();
}

JAT_feature_space* JAT_feature_space::Deserialize(std::istream& in)
{
	char header[BinaryHeaderSize] = { 0 };
	in.read(header, BinaryHeaderSize);
	if (strncmp(header, m_binaryHeader, strlen(m_binaryHeader)) != 0) {
		std::cerr << "binary header incorrect (JAT_feature_space)" << std::endl;
		exit(-1);
	}

	JAT_feature_space* featureSpace = new JAT_feature_space;
	featureSpace->m_intensity_patchsize.Deserialize(in);

	// read filters
	unsigned int size = 0;
	in.read((char*)(&size), sizeof(unsigned int));
	featureSpace->m_filters.resize(size);

	for (unsigned int i = 0; i < size; ++i)
		featureSpace->m_filters[i].Deserialize(in);

	// read geometric descriptors
	in.read((char*)(&size), sizeof(unsigned int));
	if (size > 0)
	{
		featureSpace->m_gids1.resize(size);
		featureSpace->m_gids2.resize(size);
		featureSpace->m_gcodes.resize(size);

		in.read((char*)(&(featureSpace->m_gids1[0])), sizeof(int) * size);
		in.read((char*)(&(featureSpace->m_gids2[0])), sizeof(int) * size);
		in.read((char*)(&(featureSpace->m_gcodes[0])), sizeof(int) * size);
	}

	// read others
	size = 0;
	in.read((char*)(&size), sizeof(unsigned int));
	featureSpace->m_intensityFilterSizes.resize(size);
	for (unsigned int i = 0; i < size; ++i)
		featureSpace->m_intensityFilterSizes[i].Deserialize(in);

	return featureSpace;
}

const char* JAT_feature_space::m_binaryHeader = "BRIC.IDEA.FISH.JAT_feature_space";

FS_REGISTER_DEF_TYPE(JAT_feature_space, BRIC.IDEA.FISH.JAT_feature_space)


} } }


#endif