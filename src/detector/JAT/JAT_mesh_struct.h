//
//  JAT_mesh_struct.h
//  FISH
//
//  Created by Yaozong Gao on 02/11/15.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __JAT_mesh_struct_h__
#define __JAT_mesh_struct_h__


#include "common/stringUtils.h"

namespace BRIC { namespace IDEA { namespace FISH {


class JAT_mesh_struct
{
public:
	
	int subject_num;
	int bd_num;

	std::vector< std::vector<std::string> > subject_mesh_paths;

	bool parse(const char* path);
	void clear();
	std::string get_mesh_path(int subject_idx, int bd_idx);
};


//////////////////////////////////////////////////////////////////////////


std::string JAT_mesh_struct::get_mesh_path(int subject_idx, int bd_idx)
{
	return subject_mesh_paths[subject_idx][bd_idx];
}


void JAT_mesh_struct::clear()
{
	subject_num = 0;
	bd_num = 0;
	subject_mesh_paths.clear();
}


bool JAT_mesh_struct::parse(const char* path)
{
	std::vector< std::string > lines;
	if (!stringUtils::ReadLines(path, lines)) {
		std::cerr << "fails to read lines from " << path << std::endl;
		return false;
	}
	assert_message(lines.size() > 0, "empty file");

	sscanf(lines[0].c_str(), "%d\t%d", &subject_num, &bd_num);
	assert_message(lines.size() >= (1 + bd_num * subject_num), "fewer line number");

	subject_mesh_paths.resize(subject_num);
	for (int i = 0; i < subject_num; ++i)
	{
		subject_mesh_paths[i].resize(bd_num);

		for (int j = 0; j < bd_num; ++j)
		{
			int line_idx = 1 + i * bd_num + j;
			subject_mesh_paths[i][j] = lines[line_idx];
		}
	}

	return true;
}





} } }


#endif