//
//  ContextLandmarkTrainer.h
//  FISH
//
//  Created by Yaozong Gao on 9/27/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __ContextLandmarkTrainer_h__
#define __ContextLandmarkTrainer_h__

#include "stdafx.h"
#include "detector/ContextRFTrainer.h"

namespace BRIC { namespace IDEA { namespace FISH {

struct ContextLandmarkParameters
{
	ContextRFParameters rfParameters;

	std::string annotFile;
	double boxRadius;
	int samplesPerRadius;
	double radiusStep;

	// methods
	bool Load(const char* file);
	static bool WriteDefaultConfig(const char* file);
};


template <typename T>
class ContextLandmarkTrainer: public ContextRFTrainer<T,3>
{
public:
	typedef typename ContextRFTrainer<T,3>::TreeType TreeType;

	// General
	bool LoadConfigFile(const char* file);
	const ContextLandmarkParameters& GetParameters() const { return m_params; }
	std::string GetDetectorFolder() const;

	// training
	std::auto_ptr<IFeatureSpace> CreateFeatureSpace();
	std::auto_ptr<TreeType> TrainTree(Random& random, IFeatureSpace* fs);

	// implementation
	virtual const std::vector<std::string>& GetImagePathVector() { return m_imagePaths; }
	virtual void DrawSamples(const mxImage<T>& image, unsigned int imageIdx, Random& random, std::vector< vect3<unsigned int> >& voxels, std::vector<double>& targets);

	// io
	bool CreateOutputFolder() const;
	bool SaveTree(const typename ContextLandmarkTrainer<T>::TreeType* tree) const;
	bool SaveFeatureSpace(const IFeatureSpace* fs, double treeID) const;
	bool SaveDetectionInfo() const;

private:

	bool LoadAnnotation(const char* file);

	ContextLandmarkParameters m_params;
	std::vector< std::string > m_imagePaths;
	std::vector< vect3<double> > m_landmarks;	// one landmark per image
};

//////////////////////////////////////////////////////////////////////////


bool ContextLandmarkParameters::Load(const char* file)
{
	using boost::property_tree::ptree;

	ptree pt;
	try{
		boost::property_tree::ini_parser::read_ini(file, pt);
	} catch ( boost::property_tree::ini_parser_error& error ) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	try {
		// General section
		rfParameters.detectorName = pt.get<std::string>("General.Name");
		annotFile = pt.get<std::string>("General.Annotation");
		rfParameters.rootFolder = pt.get<std::string>("General.RootFolder");
		rfParameters.contextLevel = pt.get<int>("General.Level");
		rfParameters.scale = pt.get<int>("General.Scale");
		rfParameters.spacing = stringUtils::ParseVect3<double>(pt.get<std::string>("General.Spacing"));
		boxRadius = pt.get<double>("General.BoxRadius");
		samplesPerRadius = pt.get<int>("General.TrainingSampleNumberPerRadius");
		radiusStep = pt.get<double>("General.RadiusStep");
		
		std::vector<std::string> contextDetectorNames;
		stringUtils::Split(pt.get<std::string>("General.ContextDetectorNames"), ' ', contextDetectorNames);
		rfParameters.sources.resize(contextDetectorNames.size() + 1);
		for (unsigned int i = 0; i < contextDetectorNames.size(); ++i)
			rfParameters.sources[i+1].detectorName = contextDetectorNames[i];

		// Forest section	
		rfParameters.forestParams.numTrees = pt.get<unsigned int>("Forest.NumTrees");
		rfParameters.forestParams.treeParameters.maxTreeDepth = pt.get<unsigned int>("Forest.MaxTreeDepth");
		rfParameters.forestParams.treeParameters.numOfCandidateThresholdsPerWeakLearner = pt.get<unsigned int>("Forest.NumThresholds");
		rfParameters.forestParams.treeParameters.numOfRandomWeakLearners = pt.get<unsigned int>("Forest.NumWeakLearners");
		rfParameters.forestParams.treeParameters.minElementsOfLeafNode = pt.get<unsigned int>("Forest.MinLeafNum");
		rfParameters.forestParams.treeParameters.minInformationGain = pt.get<double>("Forest.MinInfoGain");

		// Intensity section
		rfParameters.normalizationType = mxImageUtils::GetNormalizationByString( pt.get<std::string>("Intensity.Normalization") );
		rfParameters.sources[0].level = -1;
		rfParameters.sources[0].weight = pt.get<double>("Intensity.Weight");
		stringUtils::str2vec<unsigned int>(pt.get<std::string>("Intensity.FilterSize"), rfParameters.sources[0].filterSizes, ' ');
		rfParameters.sources[0].patchSize = stringUtils::ParseVect3<unsigned int>(pt.get<std::string>("Intensity.PatchSize"));

		// Context section
		for (unsigned int i = 0; i < contextDetectorNames.size(); ++i)
		{
			std::string section_name = "Context"+contextDetectorNames[i];
			rfParameters.sources[i + 1].level = pt.get<int>(section_name + ".Level");
			rfParameters.sources[i + 1].diffOption = pt.get<std::string>(section_name + ".Diff");
			rfParameters.sources[i + 1].weight = pt.get<double>(section_name+".Weight");
			stringUtils::str2vec<unsigned int>(pt.get<std::string>(section_name+".FilterSize"), rfParameters.sources[i+1].filterSizes, ' ');
			rfParameters.sources[i + 1].patchSize = stringUtils::ParseVect3<unsigned int>(pt.get<std::string>(section_name+".PatchSize"));
		}

	} catch ( boost::property_tree::ptree_error& error ) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}

bool ContextLandmarkParameters::WriteDefaultConfig(const char* file)
{
	using boost::property_tree::ptree;
	ptree pt;
	try{
		pt.put("General.Name", "Please input the detector name");
		pt.put("General.Annotation", "Please input the path for annotation file");
		pt.put("General.RootFolder", "Please specify the root folder (also used as output folder)");
		pt.put("General.Level", "Please input context level (0+)");
		pt.put("General.Scale", "Please input scale index (typically 1-4)");
		pt.put("General.Spacing", "Please input three doubles of spacing for this scale (mm)");
		pt.put("General.BoxRadius", "Please specify the radius size for sampling box (mm)");
		pt.put("General.TrainingSampleNumberPerRadius", "number of training points per radius");
		pt.put("General.RadiusStep", "stepsize for sampling radius (the finest spacing in your application)");
		pt.put("General.ContextDetectorNames", "a set of detector names separated by spaces (e.g., name1 name2)");

		pt.put("Forest.NumTrees",1);
		pt.put("Forest.MaxTreeDepth",15);
		pt.put("Forest.NumThresholds",1000);
		pt.put("Forest.NumWeakLearners",1000);
		pt.put("Forest.MinLeafNum",5);
		pt.put("Forest.MinInfoGain",0);

		pt.put("Intensity.Weight", 1);
		pt.put("Intensity.FilterSize", "3 5");
		pt.put("Intensity.PatchSize", "30 30 30");
		pt.put("Intensity.Normalization", "NONE");

		pt.put("Contextname1.Weight", 1);
		pt.put("Contextname1.Level", 0);
		pt.put("Contextname1.FilterSize", "3 5");
		pt.put("Contextname1.PatchSize", "30 30 30");

		pt.put("Contextname2.Weight", 1);
		pt.put("Contextname2.Level", 0);
		pt.put("Contextname2.FilterSize", "3 5");
		pt.put("Contextname2.PatchSize", "30 30 30");

	} catch ( boost::property_tree::ptree_error& error ) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	std::ofstream out(file);
	if(!out) {
		std::cerr << "could not open " << file << " for write" << std::endl; return false;
	}

	try {
		boost::property_tree::ini_parser::write_ini(out, pt);
	} catch ( boost::property_tree::ini_parser_error& error ) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	out.close();
	return true;
}

template <typename T>
bool ContextLandmarkTrainer<T>::LoadConfigFile(const char* file)
{
	if(!m_params.Load(file))
		return false;

	if(!LoadAnnotation(m_params.annotFile.c_str())) {
		std::cerr << "fails to load annotation from file " << m_params.annotFile << std::endl; exit(-1);
	}
	return true;
}

template <typename T>
bool ContextLandmarkTrainer<T>::LoadAnnotation(const char* file) 
{
	std::vector< std::string > lines;
	if( !stringUtils::ReadLines(file, lines) ) {
		std::cerr << "fails to read annotations from " << file << std::endl;
		return false;
	}

	m_imagePaths.clear();
	m_landmarks.clear();

	for( unsigned int i = 0; i < lines.size(); i += 2) {

		if( i + 1 >= lines.size() ) {
			std::cerr << "incomplete annotation files" << std::endl;
			return false;
		}

		std::vector< std::string > coords;
		stringUtils::Split(lines[i+1], '\t', coords);

		unsigned int splitNum = coords.size();
		if( splitNum != 8 ) {
			std::cerr << "annotation format problem" << std::endl;
			return false;
		}

		vect3<double> worldCoord( atof(coords[splitNum-3].c_str()), atof(coords[splitNum-2].c_str()), atof(coords[splitNum-1].c_str()) );

		std::vector< std::string > imagePathSplits;
		stringUtils::Split(lines[i], '\t', imagePathSplits);
		if( imagePathSplits.size() < 1 ) {
			std::cerr << "path format problem" << std::endl;
			return false;
		}

		m_imagePaths.push_back( imagePathSplits[0] );
		m_landmarks.push_back( worldCoord );
	}

	return true;
}

template <typename T>
std::string ContextLandmarkTrainer<T>::GetDetectorFolder() const
{
	std::string detectorFolder;
	if (m_params.rfParameters.contextLevel == 0)
		detectorFolder = m_params.rfParameters.rootFolder + stringUtils::Slash() + m_params.rfParameters.detectorName;
	else
		detectorFolder = m_params.rfParameters.rootFolder + stringUtils::Slash() + m_params.rfParameters.detectorName + "_C" + stringUtils::num2str(m_params.rfParameters.contextLevel);

	return detectorFolder;
}

template <typename T>
std::auto_ptr<IFeatureSpace> ContextLandmarkTrainer<T>::CreateFeatureSpace()
{
	return ContextRFTrainer<T,3>::CreateFeatureSpace(m_params.rfParameters);
}

template <typename T>
std::auto_ptr<typename ContextLandmarkTrainer<T>::TreeType> ContextLandmarkTrainer<T>::TrainTree(Random& random, IFeatureSpace* fs)
{
	return ContextRFTrainer<T,3>::TrainTree(random, fs, m_params.rfParameters);
}

template <typename T>
void ContextLandmarkTrainer<T>::DrawSamples(const mxImage<T>& image, unsigned int imageIdx, Random& random, std::vector< vect3<unsigned int> >& voxels, std::vector<double>& targets)
{
	// parameters
	double maxRadius = m_params.boxRadius;
	unsigned int numPerSphere = m_params.samplesPerRadius;
	double radiusStep = m_params.radiusStep;

	// compute lower and upper bound
	vect3<unsigned int> imageSize = image.GetImageSize();
	vect3<unsigned int> lowerBound, upperBound;
	for (unsigned int i = 0; i < 3; ++i) {
		lowerBound[i] = 0;
		upperBound[i] = imageSize[i] - 1;
	}

	// spherical sampling

	// take position first
	voxels.reserve( static_cast<unsigned int>( (maxRadius / radiusStep) + 0.5 ) * numPerSphere );

	// randomly and uniformly sampling on sphere
	for (double radius = 0; radius <= maxRadius; radius += radiusStep)
	{
		for (unsigned int i = 0; i < numPerSphere; ++i)
		{
			vect3<double> ptWorld;

			double z = random.NextDouble(-1, 1);
			double t = random.NextDouble(0, 2*M_PI);

			double r = sqrt(1-z*z);
			double x = r * cos(t);
			double y = r * sin(t);

			ptWorld[0] = m_landmarks[imageIdx][0] + x * radius;
			ptWorld[1] = m_landmarks[imageIdx][1] + y * radius;
			ptWorld[2] = m_landmarks[imageIdx][2] + z * radius;

			vect3<int> ptVoxel;
			mxImageUtils::World2Voxel(image, ptWorld, ptVoxel);

			// within the valid region
			if(ptVoxel[0] >= static_cast<int>(lowerBound[0]) && ptVoxel[0] <= static_cast<int>(upperBound[0]) &&
				ptVoxel[1] >= static_cast<int>(lowerBound[1]) && ptVoxel[1] <= static_cast<int>(upperBound[1]) &&
				ptVoxel[2] >= static_cast<int>(lowerBound[2]) && ptVoxel[2] <= static_cast<int>(upperBound[2]) )
				voxels.push_back( vect3<unsigned int>(ptVoxel[0], ptVoxel[1], ptVoxel[2]) );
		}
	}

	// compute target values
	vect3<double> landmark = m_landmarks[imageIdx];
	mxImageUtils::World2Voxel(image,landmark);

	unsigned int oldsize = targets.size();
	targets.resize( oldsize + voxels.size()*3 );
	for (unsigned int i = 0; i < voxels.size(); ++i)
	{
		targets[oldsize+i*3] = landmark[0]-static_cast<double>(voxels[i][0]);
		targets[oldsize+i*3+1] = landmark[1]-static_cast<double>(voxels[i][1]);
		targets[oldsize+i*3+2] = landmark[2]-static_cast<double>(voxels[i][2]);
	}

}

template <typename T>
bool ContextLandmarkTrainer<T>::CreateOutputFolder() const
{
	std::string detectorFolder = GetDetectorFolder();
	if (!boost::filesystem::is_directory(detectorFolder))
		boost::filesystem::create_directories(detectorFolder);

	return true;
}

template <typename T>
bool ContextLandmarkTrainer<T>::SaveTree(const typename ContextLandmarkTrainer<T>::TreeType* tree) const
{
	std::string detectorFolder = GetDetectorFolder();
	char treeFileName[2048] = {0};
	sprintf(treeFileName, "%s%sTree_R%d_%s.bin", detectorFolder.c_str(), stringUtils::Slash().c_str(), m_params.rfParameters.scale, stringUtils::DoubleToHexString(tree->GetUniqueID()).c_str());
	
	std::ofstream out(treeFileName, std::ios::binary);
	if(!out) {
		std::cerr << "fails to open " << treeFileName << " for write" << std::endl; return false;
	}
	tree->Serialize(out);
	out.close();
	return true;
}

template <typename T>
bool ContextLandmarkTrainer<T>::SaveFeatureSpace(const IFeatureSpace* fs, double treeID) const
{
	std::string detectorFolder = GetDetectorFolder();
	char fsFileName[2048] = {0};
	sprintf(fsFileName, "%s%sTree_R%d_%s.fd", detectorFolder.c_str(), stringUtils::Slash().c_str(), m_params.rfParameters.scale, stringUtils::DoubleToHexString(treeID).c_str());
	
	std::ofstream out(fsFileName, std::ios::binary);
	if (!out) {
		std::cerr << "fails to open " << fsFileName << " for write" << std::endl; return false;
	}
	fs->Serialize(out);
	out.close();
	return true;
}

template <typename T>
bool ContextLandmarkTrainer<T>::SaveDetectionInfo() const
{
	boost::property_tree::ptree pt;

	char buffer[100] ={0};
	sprintf(buffer, "%.3f %.3f %.3f", m_params.rfParameters.spacing[0], m_params.rfParameters.spacing[1], m_params.rfParameters.spacing[2]);
	pt.put("Info.Resolution", buffer);
	pt.put("Info.IntensityNormalization", mxImageUtils::GetNormalizationString(m_params.rfParameters.normalizationType));
	pt.put("Info.BoxRadius", m_params.boxRadius);
	pt.put("Context.Level", m_params.rfParameters.contextLevel);

	if (m_params.rfParameters.contextLevel > 0)
	{
		std::string detectorNames;
		for (unsigned int i = 1; i < m_params.rfParameters.sources.size(); ++i) {
			if (i != 1)
				detectorNames += " " + m_params.rfParameters.sources[i].detectorName;
			else
				detectorNames = m_params.rfParameters.sources[i].detectorName;

			std::string section_name = std::string("Context") + m_params.rfParameters.sources[i].detectorName;
			pt.put(section_name + ".Level", m_params.rfParameters.sources[i].level);
			pt.put(section_name + ".Diff", m_params.rfParameters.sources[i].diffOption);
		}
		pt.put("Context.Detectors", detectorNames.c_str());
	}

	std::string detectorFolder = GetDetectorFolder();
	char path[2048] = {0};
	sprintf(path, "%s%sinfo_R%d.ini", detectorFolder.c_str(), stringUtils::Slash().c_str(), m_params.rfParameters.scale);

	try{
		boost::property_tree::ini_parser::write_ini(path, pt);
	} catch (boost::property_tree::ptree_error& /*error*/) {

		// commented to avoid error message when multiple trees are trained in parallel
		// std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}


} } }

#endif
