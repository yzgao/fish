//
//  Temporal2DLandmarkTrainer.h
//  FISH
//
//  Created by Yaozong Gao on 10/18/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __Temporal2DLandmarkTrainer_h__
#define __Temporal2DLandmarkTrainer_h__

#include "stdafx.h"
#include "detector/ContextRFTrainer.h"

namespace BRIC { namespace IDEA { namespace FISH {

struct Temporal2DLandmarkParameters
{
	ContextRFParameters rfParameters;

	std::string annotFile;
	int timeStep;
	double sampleSigma;
	int sampleNumber;

	// methods
	bool Load(const char* file);
	static bool WriteDefaultConfig(const char* file);
};


template <typename T>
class Temporal2DLandmarkTrainer: public ContextRFTrainer<T,3>
{
public:
	typedef typename ContextRFTrainer<T,3>::TreeType TreeType;

	// General
	bool LoadConfigFile(const char* file);
	const Temporal2DLandmarkParameters& GetParameters() const { return m_params; }
	std::string GetDetectorFolder() const;

	// training
	std::auto_ptr<IFeatureSpace> CreateFeatureSpace();
	std::auto_ptr<TreeType> TrainTree(Random& random, IFeatureSpace* fs);

	// implementation
	virtual const std::vector<std::string>& GetImagePathVector() { return m_imagePaths; }
	virtual void DrawSamples(const mxImage<T>& image, unsigned int imageIdx, Random& random, std::vector< vect3<unsigned int> >& voxels, std::vector<double>& targets);

	// io
	bool CreateOutputFolder() const;
	bool SaveTree(const typename Temporal2DLandmarkTrainer<T>::TreeType* tree) const;
	bool SaveFeatureSpace(const IFeatureSpace* fs, double treeID) const;
	bool SaveDetectionInfo() const;

private:

	bool LoadAnnotation(const char* file);
	std::vector< std::pair<int,int> > LoadLandmarks(const char* file);

	Temporal2DLandmarkParameters m_params;
	std::vector< std::string > m_imagePaths;
	std::vector< std::vector< std::pair<int,int> > > m_landmarks;	// one landmark per frame
};

//////////////////////////////////////////////////////////////////////////


bool Temporal2DLandmarkParameters::Load(const char* file)
{
	using boost::property_tree::ptree;

	ptree pt;
	try{
		boost::property_tree::ini_parser::read_ini(file, pt);
	} catch ( boost::property_tree::ini_parser_error& error ) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	try {
		// General section
		rfParameters.detectorName = pt.get<std::string>("General.Name");
		annotFile = pt.get<std::string>("General.Annotation");
		rfParameters.rootFolder = pt.get<std::string>("General.RootFolder");
		rfParameters.contextLevel = pt.get<int>("General.Level");
		rfParameters.scale = pt.get<int>("General.Scale");
		rfParameters.spacing = stringUtils::ParseVect3<double>(pt.get<std::string>("General.Spacing"));

		timeStep = pt.get<int>("General.TimeStep");
		sampleSigma = pt.get<double>("General.SampleSigma");
		sampleNumber = pt.get<int>("General.SampleNumber");

		std::vector<std::string> contextDetectorNames;
		stringUtils::Split(pt.get<std::string>("General.ContextDetectorNames"), ' ', contextDetectorNames);
		rfParameters.sources.resize(contextDetectorNames.size() + 1);
		for (unsigned int i = 0; i < contextDetectorNames.size(); ++i)
			rfParameters.sources[i+1].detectorName = contextDetectorNames[i];

		// Forest section	
		rfParameters.forestParams.numTrees = pt.get<unsigned int>("Forest.NumTrees");
		rfParameters.forestParams.treeParameters.maxTreeDepth = pt.get<unsigned int>("Forest.MaxTreeDepth");
		rfParameters.forestParams.treeParameters.numOfCandidateThresholdsPerWeakLearner = pt.get<unsigned int>("Forest.NumThresholds");
		rfParameters.forestParams.treeParameters.numOfRandomWeakLearners = pt.get<unsigned int>("Forest.NumWeakLearners");
		rfParameters.forestParams.treeParameters.minElementsOfLeafNode = pt.get<unsigned int>("Forest.MinLeafNum");
		rfParameters.forestParams.treeParameters.minInformationGain = pt.get<double>("Forest.MinInfoGain");

		// Intensity section
		rfParameters.normalizationType = mxImageUtils::GetNormalizationByString( pt.get<std::string>("Intensity.Normalization") );
		rfParameters.sources[0].level = -1;
		rfParameters.sources[0].weight = pt.get<double>("Intensity.Weight");
		stringUtils::str2vec<unsigned int>(pt.get<std::string>("Intensity.FilterSize"), rfParameters.sources[0].filterSizes, ' ');
		rfParameters.sources[0].patchSize = stringUtils::ParseVect3<unsigned int>(pt.get<std::string>("Intensity.PatchSize"));

		// Context section
		for (unsigned int i = 0; i < contextDetectorNames.size(); ++i)
		{
			std::string section_name = "Context"+contextDetectorNames[i];
			rfParameters.sources[i + 1].level = pt.get<int>(section_name + ".Level");
			rfParameters.sources[i + 1].diffOption = pt.get<std::string>(section_name + ".Diff");
			rfParameters.sources[i + 1].weight = pt.get<double>(section_name+".Weight");
			stringUtils::str2vec<unsigned int>(pt.get<std::string>(section_name+".FilterSize"), rfParameters.sources[i+1].filterSizes, ' ');
			rfParameters.sources[i + 1].patchSize = stringUtils::ParseVect3<unsigned int>(pt.get<std::string>(section_name+".PatchSize"));
		}

	} catch ( boost::property_tree::ptree_error& error ) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}

bool Temporal2DLandmarkParameters::WriteDefaultConfig(const char* file)
{
	using boost::property_tree::ptree;
	ptree pt;
	try{
		pt.put("General.Name", "Please input the detector name");
		pt.put("General.Annotation", "Please input the path for annotation file");
		pt.put("General.RootFolder", "Please specify the root folder (also used as output folder)");
		pt.put("General.Level", "Please input context level (0+)");
		pt.put("General.Scale", "Please input scale index (typically 1-4)");
		pt.put("General.Spacing", "Please input three doubles of spacing for this scale (mm)");
		pt.put("General.TimeStep", "Please specify which neighboring frame to predict");
		pt.put("General.SampleSigma", "Please specify the sigma for Gaussian sampling");
		pt.put("General.SampleNumber", "Please specify the sample number per video");
		pt.put("General.ContextDetectorNames", "a set of detector names separated by spaces (e.g., name1 name2)");

		pt.put("Forest.NumTrees",1);
		pt.put("Forest.MaxTreeDepth",15);
		pt.put("Forest.NumThresholds",100);
		pt.put("Forest.NumWeakLearners",1000);
		pt.put("Forest.MinLeafNum",8);
		pt.put("Forest.MinInfoGain",0);

		pt.put("Intensity.Weight", 1);
		pt.put("Intensity.FilterSize", "3 5");
		pt.put("Intensity.PatchSize", "30 30 30");
		pt.put("Intensity.Normalization", "NONE");

		pt.put("Contextname1.Weight", 1);
		pt.put("Contextname1.Level", 0);
		pt.put("Contextname1.FilterSize", "3 5");
		pt.put("Contextname1.PatchSize", "30 30 30");

		pt.put("Contextname2.Weight", 1);
		pt.put("Contextname2.Level", 0);
		pt.put("Contextname2.FilterSize", "3 5");
		pt.put("Contextname2.PatchSize", "30 30 30");

	} catch ( boost::property_tree::ptree_error& error ) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	std::ofstream out(file);
	if(!out) {
		std::cerr << "could not open " << file << " for write" << std::endl; return false;
	}

	try {
		boost::property_tree::ini_parser::write_ini(out, pt);
	} catch ( boost::property_tree::ini_parser_error& error ) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	out.close();
	return true;
}


//////////////////////////////////////////////////////////////////////////

template <typename T>
bool Temporal2DLandmarkTrainer<T>::LoadConfigFile(const char* file)
{
	if(!m_params.Load(file))
		return false;

	if(!LoadAnnotation(m_params.annotFile.c_str())) {
		std::cerr << "fails to load annotation from file " << m_params.annotFile << std::endl; exit(-1);
	}
	return true;
}

template <typename T>
std::vector< std::pair<int, int> > Temporal2DLandmarkTrainer<T>::LoadLandmarks(const char* file)
{
	std::vector< std::string > lines;
	if (!stringUtils::ReadLines(file, lines)) {
		std::cerr << "fails to read landmarks from " << file << std::endl;
		exit(-1);
	}

	std::vector< std::pair<int, int> > landmarks;
	for (int i = 0; i < lines.size(); ++i)
	{
		if (lines[i].size() == 0)
			continue;

		std::vector<std::string> tokens;
		stringUtils::Split(lines[i], '\t', tokens);
		if (tokens.size() < 2)
			continue;

		std::pair<int,int> coord = std::make_pair(stringUtils::str2num<int>(tokens[0]), stringUtils::str2num<int>(tokens[1]));
		landmarks.push_back(coord);
	}

	return landmarks;
}

template <typename T>
bool Temporal2DLandmarkTrainer<T>::LoadAnnotation(const char* file) 
{
	std::vector< std::string > lines;
	if( !stringUtils::ReadLines(file, lines) ) {
		std::cerr << "fails to read annotations from " << file << std::endl;
		return false;
	}

	m_imagePaths.clear();
	m_landmarks.clear();

	for( unsigned int i = 0; i < lines.size(); i += 2) {

		if( i + 1 >= lines.size() ) {
			std::cerr << "incomplete annotation files" << std::endl;
			return false;
		}

		std::vector< std::string > imagePathSplits;
		stringUtils::Split(lines[i], '\t', imagePathSplits);
		if( imagePathSplits.size() < 1 ) {
			std::cerr << "path format problem" << std::endl;
			return false;
		}
		std::string image_path = imagePathSplits[0];
		m_imagePaths.push_back(image_path);

		imagePathSplits.clear();
		stringUtils::Split(lines[i+1], '\t', imagePathSplits);
		if ( imagePathSplits.size() < 1 ) {
			std::cerr << "annotation format problem" << std::endl;
			return false;
		}
		std::string annot_path = imagePathSplits[0];

		std::vector< std::pair<int, int> > landmarks = LoadLandmarks(annot_path.c_str());
		m_landmarks.push_back(landmarks);
	}

	return true;
}

template <typename T>
std::string Temporal2DLandmarkTrainer<T>::GetDetectorFolder() const
{
	std::string detectorFolder;
	if (m_params.rfParameters.contextLevel == 0)
		detectorFolder = m_params.rfParameters.rootFolder + stringUtils::Slash() + m_params.rfParameters.detectorName;
	else
		detectorFolder = m_params.rfParameters.rootFolder + stringUtils::Slash() + m_params.rfParameters.detectorName + "_C" + stringUtils::num2str(m_params.rfParameters.contextLevel);

	return detectorFolder;
}

template <typename T>
std::auto_ptr<IFeatureSpace> Temporal2DLandmarkTrainer<T>::CreateFeatureSpace()
{
	return ContextRFTrainer<T,3>::CreateFeatureSpace(m_params.rfParameters);
}

template <typename T>
std::auto_ptr<typename Temporal2DLandmarkTrainer<T>::TreeType> Temporal2DLandmarkTrainer<T>::TrainTree(Random& random, IFeatureSpace* fs)
{
	return ContextRFTrainer<T,3>::TrainTree(random, fs, m_params.rfParameters);
}

template <typename T>
void Temporal2DLandmarkTrainer<T>::DrawSamples(const mxImage<T>& image, unsigned int imageIdx, Random& random, std::vector< vect3<unsigned int> >& voxels, std::vector<double>& targets)
{
	// parameters
	double sample_sigma = m_params.sampleSigma;
	double sample_num = m_params.sampleNumber;
	int time_step = m_params.timeStep;

	// compute lower and upper bound
	vect3<unsigned int> imageSize = image.GetImageSize();

	vect3<unsigned int> lowerBound, upperBound;
	lowerBound[0] = 0;	upperBound[0] = imageSize[0] - 1;
	lowerBound[1] = 0;	upperBound[1] = imageSize[1] - 1;
	lowerBound[2] = std::max(0, -time_step);	 upperBound[2] = std::min(imageSize[2] - 1, imageSize[2] - 1 - time_step);

	// spherical sampling

	// take position first
	voxels.reserve( sample_num );

	// randomly and uniformly sampling on sphere
	for (unsigned int i = 0; i < sample_num; ++i)
	{
		int xcoord, ycoord, zcoord;

		do {
			zcoord = random.Next(lowerBound[2], upperBound[2]);
			double r = random.GaussianNextDouble(0, sample_sigma);
			double t = random.NextDouble(0, 2 * M_PI);

			double x = r * cos(t);
			double y = r * sin(t);

			xcoord = static_cast<int>(x + m_landmarks[imageIdx][zcoord].first + 0.5);
			ycoord = static_cast<int>(y + m_landmarks[imageIdx][zcoord].second + 0.5);

		} while (xcoord < lowerBound[0] || ycoord < lowerBound[1] || xcoord > upperBound[0] || ycoord > upperBound[1]);
		
		vect3<unsigned int> coords(xcoord, ycoord, zcoord);
		voxels.push_back(coords);
	}

	// compute target values
	unsigned int oldsize = targets.size();
	targets.resize( oldsize + voxels.size()*3 );
	for (unsigned int i = 0; i < voxels.size(); ++i)
	{
		int z = voxels[i][2] + time_step;
		std::pair<int,int> lm = m_landmarks[imageIdx][z];

		targets[oldsize + i * 3] = lm.first - static_cast<int>(voxels[i][0]);
		targets[oldsize + i * 3 + 1] = lm.second - static_cast<int>(voxels[i][1]);
		targets[oldsize + i * 3 + 2] = time_step;
	}

}

template <typename T>
bool Temporal2DLandmarkTrainer<T>::CreateOutputFolder() const
{
	std::string detectorFolder = GetDetectorFolder();
	if (!boost::filesystem::is_directory(detectorFolder))
		boost::filesystem::create_directories(detectorFolder);

	return true;
}

template <typename T>
bool Temporal2DLandmarkTrainer<T>::SaveTree(const typename Temporal2DLandmarkTrainer<T>::TreeType* tree) const
{
	std::string detectorFolder = GetDetectorFolder();
	char treeFileName[2048] = {0};
	sprintf(treeFileName, "%s%sTree_R%d_%s.bin", detectorFolder.c_str(), stringUtils::Slash().c_str(), m_params.rfParameters.scale, stringUtils::DoubleToHexString(tree->GetUniqueID()).c_str());
	
	std::ofstream out(treeFileName, std::ios::binary);
	if(!out) {
		std::cerr << "fails to open " << treeFileName << " for write" << std::endl; return false;
	}
	tree->Serialize(out);
	out.close();
	return true;
}

template <typename T>
bool Temporal2DLandmarkTrainer<T>::SaveFeatureSpace(const IFeatureSpace* fs, double treeID) const
{
	std::string detectorFolder = GetDetectorFolder();
	char fsFileName[2048] = {0};
	sprintf(fsFileName, "%s%sTree_R%d_%s.fd", detectorFolder.c_str(), stringUtils::Slash().c_str(), m_params.rfParameters.scale, stringUtils::DoubleToHexString(treeID).c_str());
	
	std::ofstream out(fsFileName, std::ios::binary);
	if (!out) {
		std::cerr << "fails to open " << fsFileName << " for write" << std::endl; return false;
	}
	fs->Serialize(out);
	out.close();
	return true;
}

template <typename T>
bool Temporal2DLandmarkTrainer<T>::SaveDetectionInfo() const
{
	boost::property_tree::ptree pt;

	char buffer[100] ={0};
	sprintf(buffer, "%.3f %.3f %.3f", m_params.rfParameters.spacing[0], m_params.rfParameters.spacing[1], m_params.rfParameters.spacing[2]);
	pt.put("Info.Resolution", buffer);
	pt.put("Info.IntensityNormalization", mxImageUtils::GetNormalizationString(m_params.rfParameters.normalizationType));
	pt.put("Info.SampleSigma", m_params.sampleSigma);
	pt.put("Context.Level", m_params.rfParameters.contextLevel);

	if (m_params.rfParameters.contextLevel > 0)
	{
		std::string detectorNames;
		for (unsigned int i = 1; i < m_params.rfParameters.sources.size(); ++i) {
			if (i != 1)
				detectorNames += " " + m_params.rfParameters.sources[i].detectorName;
			else
				detectorNames = m_params.rfParameters.sources[i].detectorName;

			std::string section_name = std::string("Context") + m_params.rfParameters.sources[i].detectorName;
			pt.put(section_name + ".Level", m_params.rfParameters.sources[i].level);
			pt.put(section_name + ".Diff", m_params.rfParameters.sources[i].diffOption);
		}
		pt.put("Context.Detectors", detectorNames.c_str());
	}

	std::string detectorFolder = GetDetectorFolder();
	char path[2048] = {0};
	sprintf(path, "%s%sinfo_R%d.ini", detectorFolder.c_str(), stringUtils::Slash().c_str(), m_params.rfParameters.scale);

	try{
		boost::property_tree::ini_parser::write_ini(path, pt);
	} catch (boost::property_tree::ptree_error& /*error*/) {

		// commented to avoid error message when multiple trees are trained in parallel
		// std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}


} } }

#endif
