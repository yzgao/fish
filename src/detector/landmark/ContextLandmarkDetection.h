//
//  ContextLandmarkDetection.h
//  FISH
//
//  Created by Yaozong Gao on 12/16/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __ContextLandmarkDetection_h__
#define __ContextLandmarkDetection_h__

#include "detector/DetectorIO.h"
#include "detector/ContextRF.h"
#include "forest/stat/MultiDimensionStatisticsAggregator.h"
#include "common/mxImageUtils.h"
#include "common/stringUtils.h"
#include "extern/ImageHelper.h"

namespace BRIC { namespace IDEA { namespace FISH {

// context landmark detection strategy
enum ContextLandmarkDetectionStrategy
{
	LM_VOTING,
	LM_JUMPING
};

// context landmark detection parameters
struct ContextLandmarkDetectionParameters
{
	std::string root;
	std::vector< std::string > names;
	int contextLevel;
	int maxscale;
	int minscale;

	// detection strategy
	ContextLandmarkDetectionStrategy option;

	double boxScaleFactor;		// default 1

	// parameters for jumping (keep default unless you know the details)
	int jump_numPointsPerDim;		// default 4
	int jump_maxJump;				// default 20
	double jump_jumpDistanceTol;	// default -2

	// parameters for voting (keep default unless you know the details)
	int vote_stepsize;			// default 1

	ContextLandmarkDetectionParameters(): option(LM_VOTING), jump_numPointsPerDim(4), jump_maxJump(20), jump_jumpDistanceTol(-2), boxScaleFactor(0.5), vote_stepsize(1) {}
};

// perform context landmark detection
class ContextLandmarkDetection
{
public:

	typedef MemoryAxisAlignedWeakLearner W;
	typedef MultiDimensionStatisticsAggregator<3> S;
	typedef Tree<W,S> TreeType;
	typedef Forest<W,S> ForestType;

	void SetParameters(const ContextLandmarkDetectionParameters& params) { m_params = params; }

	template <typename T>
	void DetectLandmarks(const mxImage<T>& image, std::map< std::string, vect3<double> >& landmarks);

private:

	void ReadDetectorInfo(const std::string& detectorFolder, int scale, vect3<double>& spacing, double& boxRadius);

	template <typename T>
	vect3<double> DetectLandmark_(const mxImage<T>& image, std::string rootFolder, std::string name, int level, int scale, const vect3<double>& initLM);

	vect3<double> PointJumpOnOffsetImage(const mxImage< boost::array<double,3> >& offsetImage);
	vect3<double> PointVoteOnOffsetImage(const mxImage< boost::array<double,3> >& offsetImage);

	ContextLandmarkDetectionParameters m_params;
};

//////////////////////////////////////////////////////////////////////////

void ContextLandmarkDetection::ReadDetectorInfo(const std::string& detectorFolder, int scale, vect3<double>& spacing, double& boxRadius)
{
	std::string infoPath = detectorFolder + stringUtils::Slash() + "info_R" + stringUtils::num2str(scale) + ".ini";

	boost::property_tree::ptree pt;
	try {
		boost::property_tree::ini_parser::read_ini(infoPath, pt);
		std::string stringSpacing = pt.get<std::string>("Info.Resolution");
		spacing = stringUtils::ParseVect3<double>(stringSpacing);
		boxRadius = pt.get<double>("Info.BoxRadius");
	} catch ( std::runtime_error& error ) {
		std::cerr << error.what() << std::endl;
		exit(-1);
	}
}

template <typename T>
void ContextLandmarkDetection::DetectLandmarks(const mxImage<T>& image, std::map< std::string, vect3<double> >& landmarks)
{
	// boost::timer::auto_cpu_timer timer;

	if (m_params.contextLevel > 0 && m_params.names.size() == 0) {
		std::cerr << "empty context detector names for non-zero context level" << std::endl;
		exit(-1);
	}

	if (m_params.minscale > m_params.maxscale) {
		std::cerr << "max scale (coarsest) must be greater than min scale (finest)" << std::endl;
		exit(-1);
	}

	// iterate over all landmarks to be detected
	for (unsigned int i = 0; i < m_params.names.size(); ++i)
	{
		// detect one landmark
		std::string name = m_params.names[i];
		vect3<double> lm = mxImageUtils::WorldCenter(image);

		// iterate over all possible scales (from coarse to fine)
		for (int scale = m_params.maxscale; scale >= m_params.minscale; --scale)
		{
			// detect under one scale
			lm = DetectLandmark_(image, m_params.root, name, m_params.contextLevel, scale, lm);

			vect3<int> voxel;
			mxImageUtils::World2Voxel(image, lm, voxel);
			// std::cout << scale << ": " << voxel[0] << " " << voxel[1] << " " << voxel[2] << std::endl;
		}

		landmarks[name] = lm;
	}
}

template <typename T>
vect3<double> ContextLandmarkDetection::DetectLandmark_(const mxImage<T>& image, std::string rootFolder, std::string name, int level, int scale, const vect3<double>& initLM)
{
	std::string detectorFolder = rootFolder + stringUtils::Slash() + name;
	if (level > 0)
		detectorFolder += "_C" + stringUtils::num2str(level);

	// load detector info
	double boxRadius = 0;
	vect3<double> spacing;
	ReadDetectorInfo(detectorFolder, scale, spacing, boxRadius);

	// resample image into isotropic spacing
	mxImage<T> resampledImage;
	mxImageUtils::Resample(image, spacing, resampledImage);
	vect3<unsigned int> resampleImageSize = resampledImage.GetImageSize();

	// compute the bounding box
	vect3<int> sp, ep;
	if (scale == m_params.maxscale) {
		for (unsigned int i = 0; i < 3; ++i)
		{
			sp[i] = 0;
			ep[i] = static_cast<int>(resampleImageSize[i])-1;
		}
	}else {

		// determine the voxel location of initial landmark
		vect3<int> voxel;
		mxImageUtils::World2Voxel(resampledImage, initLM, voxel);
		if (!resampledImage.PtInImage(voxel[0], voxel[1], voxel[2])) {
			std::cerr << "initial landmark out of the image" << std::endl;
			exit(-1);
		}

		// compute box size
		vect3<int> boxsize;
		for (unsigned int i = 0; i < 3; ++i)
			boxsize[i] = static_cast<int>( (boxRadius*2*m_params.boxScaleFactor) / spacing[i] + 0.5 );

		// compute detection bounding box
		for (unsigned int i = 0; i < 3; ++i) 
		{
			sp[i] = voxel[i] - boxsize[i]/2;
			ep[i] = sp[i] + boxsize[i] - 1;

			if (sp[i] < 0)
				sp[i] = 0;
			if (ep[i] >= static_cast<int>(resampleImageSize[i]))
				ep[i] = static_cast<int>(resampleImageSize[i])-1;

			if (ep[i] < sp[i]) {
				std::cerr << "empty detection bounding box" << std::endl;
				exit(-1);
			}
		}
	}

	vect3<double> lmWorld;

	// predict offset image (from sp to ep)
	typename ContextRF<T,3>::VectorImageType offsetImage;
	ContextRF<T,3>::localRF(resampledImage, rootFolder, name, level, scale, sp, ep, offsetImage);

	resampledImage.Clear();

	switch (m_params.option)
	{
	case LM_JUMPING: 
		lmWorld = PointJumpOnOffsetImage(offsetImage); break;
	case LM_VOTING: 
		lmWorld = PointVoteOnOffsetImage(offsetImage); break;
	default:
		std::cerr << "unrecognized landmark detection strategy" << std::endl;
		exit(-1);
	}

	return lmWorld;
}

vect3<double> ContextLandmarkDetection::PointJumpOnOffsetImage(const mxImage< boost::array<double,3> >& offsetImage)
{
	// sample a grid of initial jump points
	vect3<unsigned int> imageSize = offsetImage.GetImageSize();

	vect3<unsigned int> stepsize;
	for (int i = 0; i < 3; ++i)
		stepsize[i] = imageSize[i] / (m_params.jump_numPointsPerDim + 1);

	// gather initial jump points
	std::vector< vect3<int> > startPts;
	for (int z = 1; z <= static_cast<int>(m_params.jump_numPointsPerDim); ++z)
	{
		for (int y = 1; y <= static_cast<int>(m_params.jump_numPointsPerDim); ++y)
		{
			for (int x = 1; x <= static_cast<int>(m_params.jump_numPointsPerDim); ++x)
			{
				vect3<int> pt;
				pt[0] = static_cast<int>( stepsize[0]*x );
				pt[1] = static_cast<int>( stepsize[1]*y );
				pt[2] = static_cast<int>( stepsize[2]*z );

				startPts.push_back( pt );
			}
		}
	}

	std::vector< vect3<int> > endPts;
	endPts.resize( startPts.size() );

	std::set< int > positionTags;

	// iterate over all start jump points, jump one by one
	for (unsigned int i = 0; i < startPts.size(); ++i)
	{
		vect3<int> pt = startPts[i];
		endPts[i] = pt;
		double prevJumpDistance = std::numeric_limits<double>::max();

		int jumpcount = 0;
		while (jumpcount <= m_params.jump_maxJump)
		{
			if (!offsetImage.PtInImage(pt[0], pt[1], pt[2]))
				break;	// jump outside the image domain

			int posIdx = pt[2]*imageSize[0]*imageSize[1] + pt[1]*imageSize[0] + pt[0];
			if (positionTags.count(posIdx)) { // jump to previously visited position

				// pick a point with smaller predicted distance
				const boost::array<double,3>& offset1= offsetImage(endPts[i][0], endPts[i][1], endPts[i][2]);
				const boost::array<double,3>& offset2 = offsetImage(pt[0], pt[1], pt[2]);

				double distance1 = sqrt(offset1[0]*offset1[0]+offset1[1]*offset1[1]+offset1[2]*offset1[2]);
				double distance2 = sqrt(offset2[0]*offset2[0]+offset2[1]*offset2[1]+offset2[2]*offset2[2]);

				if (distance2 < distance1)
					endPts[i] =  pt;

				endPts[i] = pt;
				break;

			}else 
				positionTags.insert(posIdx);

			// jump
			endPts[i] = pt;

			const boost::array<double,3>& offset = offsetImage(pt[0], pt[1], pt[2]);
			vect3<int> nextPt( static_cast<int>(pt[0]+offset[0]+0.5), static_cast<int>(pt[1]+offset[1]+0.5), static_cast<int>(pt[2]+offset[2]+0.5) );

			if (pt == nextPt)
				break;	// converge

			double jumpDistance = std::sqrt(offset[0]*offset[0] + offset[1]*offset[1] + offset[2]*offset[2]);
			if (prevJumpDistance-jumpDistance <= m_params.jump_jumpDistanceTol)
				break;	// stop when the jump distance is significantly increased

			pt = nextPt;
			++ jumpcount;
		}
	}

	// pick the landmark location as the one with minimum predicted distance to the landmark
	double minDistance = std::numeric_limits<double>::max();
	vect3<double> lmWorld;
	for (unsigned int i = 0; i < endPts.size(); ++i)
	{
		vect3<int> pt = endPts[i];
		const boost::array<double,3>& offset = offsetImage(pt[0],pt[1],pt[2]);
		double distance = sqrt(offset[0]*offset[0]+offset[1]*offset[1]+offset[2]*offset[2]);
		if (distance < minDistance)
		{
			minDistance = distance;
			lmWorld[0] = pt[0];
			lmWorld[1] = pt[1];
			lmWorld[2] = pt[2];
		}
	}

	mxImageUtils::Voxel2World(offsetImage, lmWorld);
	return lmWorld;
}

vect3<double> ContextLandmarkDetection::PointVoteOnOffsetImage(const mxImage< boost::array<double,3> >& offsetImage)
{
	vect3<unsigned int> imageSize = offsetImage.GetImageSize();
	mxImage<int> voteImage;
	voteImage.CopyImageInfo(offsetImage);
	voteImage.SetImageSize(imageSize);
	voteImage.Fill(0);

	// voting
	for (unsigned int z = 0; z < imageSize[2]; z += m_params.vote_stepsize)
	{
		for (unsigned int y = 0; y < imageSize[1]; y += m_params.vote_stepsize)
		{
			for (unsigned int x = 0; x < imageSize[0]; x += m_params.vote_stepsize)
			{
				vect3<int> pt(x,y,z);
				const boost::array<double,3>& offset = offsetImage(x,y,z);

				vect3<int> votePt( static_cast<int>(pt[0]+offset[0]+0.5), static_cast<int>(pt[1]+offset[1]+0.5), static_cast<int>(pt[2]+offset[2]+0.5) );

				if (voteImage.PtInImage(votePt[0], votePt[1], votePt[2]))
					voteImage(votePt[0], votePt[1], votePt[2]) += 1;
			}
		}
	}

	// Gaussian smoothing
	mxImage<int>& smooth_voteImage = voteImage;

	// mxImage<double> smooth_voteImage;
	// mxImageUtils::GaussianSmooth(voteImage, smooth_voteImage, 1);

	// find the position with maximum votes
	int maxVote = -1;
	vect3<double> lmWorld;

	for (unsigned int z = 0; z < imageSize[2]; ++z)
	{
		for (unsigned int y = 0; y < imageSize[1]; ++y)
		{
			for (unsigned int x = 0; x < imageSize[0]; ++x)
			{
				if (smooth_voteImage(x, y, z) > maxVote)
				{
					maxVote = smooth_voteImage(x,y,z);
					lmWorld[0] = x; lmWorld[1] = y; lmWorld[2] = z;
				}
			}
		}
	}

	// convert voxel to world coordinate
	mxImageUtils::Voxel2World(smooth_voteImage, lmWorld);
	return lmWorld;
}


} } }

#endif
