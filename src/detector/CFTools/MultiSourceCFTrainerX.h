//
//  MultiSourceCFTrainerX.h (Easy-use of CFTrainer by taking sample positions from disk)
//  FISH
//
//  Created by Yaozong Gao on 11/19/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __MultiSourceCFTrainerX_h__
#define __MultiSourceCFTrainerX_h__

#include "forest/ForestTrainer.h"
#include "forest/wl/MemoryAxisAlignedWeakLearner.h"
#include "forest/stat/HistogramStatisticsAggregator.h"
#include "forest/context/ClassificationExhaustSearchTrainingContext.h"
#include "feature/haar3d/Haar3DFeatureSpace.h"
#include "feature/CompositeFeatureSpace.h"
#include "common/mxImageUtils.h"
#include "extern/ImageHelper.h"
#include "common/mxImageWrapper.h"
#include "detector/DetectorIO.h"

namespace BRIC { namespace IDEA { namespace FISH {

// parameter for each source
struct SourceParam
{
	double weight;
	std::vector< vect3<unsigned int> > filterSizes;
	vect3<unsigned int> patchSize;
	NormalizationType normalizedType;
};


// multi source classification forest parameters
struct MultiSourceCFTrainerXParam
{
public:
	std::string m_annotFile;
	std::string m_outFolder;
	int m_numClasses;
	int m_scale;
	vect3<double> m_spacing;
	int m_sourceNum;
	ForestTrainingParameters m_forestParams;
	std::vector< SourceParam > m_sourceParams;
	// IO interfaces
	static bool WriteDefaultIni(const char* path);
	bool ReadIni(const char* path);

private:
	NormalizationType ReadNormalizationType(std::string strNormalization);
	void ReadSourceParam(boost::property_tree::ptree& pt, int sourceIdx, SourceParam& param);
};

// main training workhorse
class MultiSourceCFTrainerX
{
public:
	// type definitions
	typedef MemoryAxisAlignedWeakLearner W;
	typedef HistogramStatisticsAggregator S;
	typedef Tree<W,S> TreeType;
	typedef Forest<W,S> ForestType;

	bool LoadIni(const char* path);
	std::auto_ptr<TreeType> TrainTree(Random& random, CompositeFeatureSpace* fs) const;

	const MultiSourceCFTrainerXParam& GetParams() const { return m_param; }
	std::auto_ptr<CompositeFeatureSpace> CreateFeatureSpace() const;
	bool WriteInfoIni() const;
	bool WriteTree(const TreeType* tree, const CompositeFeatureSpace* fs) const;

	bool LoadAnnotation(const char* path);
	bool LoadSampleFile(const char* path, std::vector< vect3<double> >& samplePositions, std::vector<int>& sampleLabels);

private:
	void LoadSourceImages(const std::vector< std::string >& imagePaths, const vect3<double>& spacing, std::vector< mxImageWrapper >& images) const;
	void SamplePatchPositions(int imageIdx, const mxImageWrapper& image, Random& random, std::vector< vect3<unsigned int> >& voxels, std::vector<int>& labels) const;

	std::vector< std::vector< std::string > > m_imagePaths;
	std::vector< std::vector< vect3<double> > > m_samplePositions;
	std::vector< std::vector<int> > m_sampleLabels;

	MultiSourceCFTrainerXParam m_param;
};


//////////////////////////////////////////////////////////////////////////
// Implementations

bool MultiSourceCFTrainerXParam::WriteDefaultIni(const char* path)
{
	using boost::property_tree::ptree;
	ptree pt;
	try{
		pt.put("General.Annotation","ToBeDetermined");
		pt.put("General.DetectorFolder","ToBeDetermined");
		pt.put("General.NumClasses",2);
		pt.put("General.Scale",1);
		pt.put("General.Spacing","1 1 1");
		pt.put("General.SourceNumber",2);

		pt.put("Forest.NumTree", 1);
		pt.put("Forest.MaxTreeDepth", 15);
		pt.put("Forest.NumThresholds", 1000);
		pt.put("Forest.NumWeakLearners", 1000);
		pt.put("Forest.MinLeafNum", 5);
		pt.put("Forest.MinInfoGain", 0);

		pt.put("Source0.Weight",1);
		pt.put("Source0.FilterSize", "3 5");
		pt.put("Source0.PatchSize", "30 30 30");
		pt.put("Source0.Normalization","NONE");

		pt.put("Source1.Weight",1);
		pt.put("Source1.FilterSize", "3 5");
		pt.put("Source1.PatchSize", "30 30 30");
		pt.put("Source1.Normalization","NONE");

	} catch ( boost::property_tree::ptree_error& error ) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	std::ofstream out(path);
	if(!out) {
		std::cerr << "could not open " << path << " for write" << std::endl; return false;
	}

	try {
		boost::property_tree::ini_parser::write_ini(out, pt);
	} catch ( boost::property_tree::ini_parser_error& error ) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	out.close();
	return true;
}

NormalizationType MultiSourceCFTrainerXParam::ReadNormalizationType(std::string strNormalization)
{
	if (strNormalization == "NONE")
		return NONE;
	else if(strNormalization == "UNIT_NORM")
		return UNIT_NORM;
	else if(strNormalization == "MAX_ONE")
		return MAX_ONE;
	else if(strNormalization == "MIN_MAX")
		return MIN_MAX;
	else if (strNormalization == "ZERO_MEAN_ONE_STD")
		return ZERO_MEAN_ONE_STD;
	else {
		std::cerr << "normalization type unsupported" << std::endl;
		std::cerr << "Use: NONE, UNIT_NORM, MAX_ONE, MIN_MAX, ZERO_MEAN_ONE_STD" << std::endl;
		exit(-1);
	}
	return NONE;
}

void MultiSourceCFTrainerXParam::ReadSourceParam(boost::property_tree::ptree& pt, int sourceIdx, SourceParam& param)
{
	std::string section_name = "Source" + stringUtils::num2str(sourceIdx);

	param.weight = pt.get<double>(section_name + ".Weight");

	std::string filterSizeString = pt.get<std::string>(section_name + ".FilterSize");
	std::vector< std::string > elements;
	stringUtils::Split(filterSizeString, ' ', elements);

	param.filterSizes.clear();
	for (unsigned int i = 0; i < elements.size(); ++i) {
		unsigned int filterSize = atoi(elements[i].c_str());
		param.filterSizes.push_back( vect3<unsigned int>(filterSize, filterSize, filterSize) );
	}

	std::string patchSizeString = pt.get<std::string>(section_name + ".PatchSize");
	sscanf(patchSizeString.c_str(), "%u %u %u", &param.patchSize[0], &param.patchSize[1], &param.patchSize[2]);

	std::string strNormalization = pt.get<std::string>(section_name + ".Normalization");
	param.normalizedType = ReadNormalizationType(strNormalization);
}

bool MultiSourceCFTrainerXParam::ReadIni(const char* path)
{
	using boost::property_tree::ptree;
	ptree pt;
	try{
		boost::property_tree::ini_parser::read_ini(path, pt);
	} catch ( boost::property_tree::ini_parser_error& error ) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	try {
		m_annotFile = pt.get<std::string>("General.Annotation");
		m_outFolder = pt.get<std::string>("General.DetectorFolder");
		m_numClasses = pt.get<int>("General.NumClasses");
		m_scale = pt.get<int>("General.Scale");
		std::string spacingString = pt.get<std::string>("General.Spacing");
		sscanf(spacingString.c_str(), "%lf %lf %lf", &m_spacing[0], &m_spacing[1], &m_spacing[2]);
		m_sourceNum = pt.get<int>("General.SourceNumber");
		if (m_sourceNum <= 0) {
			std::cerr << "source number > 0" << std::endl; exit(-1);
		}

		m_forestParams.numTrees = pt.get<unsigned int>("Forest.NumTree");
		m_forestParams.treeParameters.maxTreeDepth = pt.get<unsigned int>("Forest.MaxTreeDepth");
		m_forestParams.treeParameters.numOfCandidateThresholdsPerWeakLearner = pt.get<unsigned int>("Forest.NumThresholds");
		m_forestParams.treeParameters.numOfRandomWeakLearners = pt.get<unsigned int>("Forest.NumWeakLearners");
		m_forestParams.treeParameters.minElementsOfLeafNode = pt.get<unsigned int>("Forest.MinLeafNum");
		m_forestParams.treeParameters.minInformationGain = pt.get<double>("Forest.MinInfoGain");

		m_sourceParams.resize(m_sourceNum);
		for (int i = 0; i < m_sourceNum; ++i)
			ReadSourceParam(pt, i, m_sourceParams[i]);

	} catch ( boost::property_tree::ptree_error& error ) {
		std::cerr << error.what() << std::endl;
		return false;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////

bool MultiSourceCFTrainerX::LoadIni(const char* path)
{
	if (!m_param.ReadIni(path)) {
		std::cerr << "fail to read INI file" << std::endl; return false;
	}
	if (!LoadAnnotation(m_param.m_annotFile.c_str())) {
		std::cerr << "fail to load annotation file" << std::endl; return false;
	}
	return true;
}

bool MultiSourceCFTrainerX::LoadSampleFile(const char* path, std::vector< vect3<double> >& samplePositions, std::vector<int>& sampleLabels)
{
	std::vector< std::string > lines;
	if ( !stringUtils::ReadLines(path, lines) ) {
		std::cerr << "fails to read lines from " << path << std::endl;
		return false;
	}
	samplePositions.resize(lines.size());
	sampleLabels.resize(lines.size());

	for (unsigned int i = 0; i < lines.size(); ++i)
	{
		if(lines[i].length() <= 2) 
			continue;

		std::vector< std::string > elements;
		stringUtils::Split(lines[i], ' ', elements);

		if (elements.size() < 4) {
			std::cerr << "wrong line format" << std::endl; return false;
		}

		samplePositions[i][0] = atof(elements[0].c_str());
		samplePositions[i][1] = atof(elements[1].c_str());
		samplePositions[i][2] = atof(elements[2].c_str());
		sampleLabels[i] = atoi(elements[3].c_str());
	}
	return true;
}

bool MultiSourceCFTrainerX::LoadAnnotation(const char* path) 
{
	std::vector< std::string> lines;
	if( !stringUtils::ReadLines(path, lines) ) {
		std::cerr << "fails to read lines from " << path << std::endl; return false;
	}

	int linesPerItem = m_param.m_sourceNum + 1;

	if (lines.size() % linesPerItem != 0) {
		std::cerr << "wrong annotation format (line number problem)" << std::endl; return false;
	}

	unsigned int numImages = lines.size() / linesPerItem;
	m_imagePaths.resize(numImages);
	m_samplePositions.resize(numImages);
	m_sampleLabels.resize(numImages);

	for (unsigned int i = 0; i < numImages; ++i)
	{
		m_imagePaths[i].resize(m_param.m_sourceNum);
		for (int j = 0; j < m_param.m_sourceNum; ++j)
			m_imagePaths[i][j] = lines[i*linesPerItem+j];

		std::string samplePath = lines[(i+1)*linesPerItem-1];

		if( !LoadSampleFile(samplePath.c_str(), m_samplePositions[i], m_sampleLabels[i]) ) {
			std::cerr << "fails to load sample info from " << samplePath << std::endl; return false;
		}

		// verify labels
		for (unsigned int j = 0; j < m_sampleLabels[i].size(); ++j)
		{
			if(m_sampleLabels[i][j] < 0 || m_sampleLabels[i][j] >= static_cast<int>(m_param.m_numClasses)) {
				std::cerr << "label " << m_sampleLabels[i][j] << "??" << std::endl; 
				exit(-1); 
			}
		}
	}

	return true;
}

std::auto_ptr<CompositeFeatureSpace> MultiSourceCFTrainerX::CreateFeatureSpace() const
{
	std::auto_ptr<CompositeFeatureSpace> cfs ( new CompositeFeatureSpace );

	for (int i = 0; i < m_param.m_sourceNum; ++i)
	{
		Haar3DFeatureSpace* hfs = new Haar3DFeatureSpace;
		for (unsigned int j = 0; j < m_param.m_sourceParams[i].filterSizes.size(); ++j) 
			hfs->AddFilterSize( m_param.m_sourceParams[i].filterSizes[j] );
		hfs->SetPatchSize( m_param.m_sourceParams[i].patchSize );
		cfs->AddFeatureSpace( std::auto_ptr<IFeatureSpace>(hfs) );
	}

	std::vector<double> weights;
	weights.resize(m_param.m_sourceNum);
	for (unsigned int i = 0; i < weights.size(); ++i)
		weights[i] = m_param.m_sourceParams[i].weight;
	cfs->SetWeights(&weights[0], weights.size());

	return cfs;
}

bool MultiSourceCFTrainerX::WriteInfoIni() const
{
	char filePath[2048] = {0};
	sprintf(filePath, "%s%sinfo_R%d.ini", m_param.m_outFolder.c_str(), stringUtils::Slash().c_str(), m_param.m_scale);

	boost::property_tree::ptree pt;

	char buffer[100] ={0};
	sprintf(buffer, "%lf %lf %lf", m_param.m_spacing[0], m_param.m_spacing[1], m_param.m_spacing[2]);
	pt.put("Info.Resolution", buffer);

	pt.put("Info.SourceNumber", m_param.m_sourceNum);

	for (int i = 0; i < m_param.m_sourceNum; ++i)
	{
		std::string section_name = "Source" + stringUtils::num2str(i);
		switch (m_param.m_sourceParams[i].normalizedType)
		{
		case BRIC::IDEA::FISH::NONE:
			pt.put(section_name + ".Normalization", "NONE");
			break;
		case BRIC::IDEA::FISH::UNIT_NORM:
			pt.put(section_name + ".Normalization", "UNIT_NORM");
			break;
		case BRIC::IDEA::FISH::MAX_ONE:
			pt.put(section_name + ".Normalization", "MAX_ONE");
			break;
		case BRIC::IDEA::FISH::MIN_MAX:
			pt.put(section_name + ".Normalization", "MIN_MAX");
			break;
		case BRIC::IDEA::FISH::ZERO_MEAN_ONE_STD:
			pt.put(section_name + ".Normalization", "ZERO_MEAN_ONE_STD");
			break;
		default:
			std::cerr << "unknown normalization type" << std::endl;
			return false;
		}
	}

	try{
		boost::property_tree::ini_parser::write_ini(filePath, pt);
	} catch (boost::property_tree::ptree_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}
	return true;
}

bool MultiSourceCFTrainerX::WriteTree(const TreeType* tree, const CompositeFeatureSpace* fs) const
{
	std::string outputTreePath = m_param.m_outFolder + stringUtils::Slash();
	outputTreePath += "Tree_R" + stringUtils::num2str(m_param.m_scale) + "_" + stringUtils::DoubleToHexString(tree->GetUniqueID()) + ".bin";;
	std::string outputFeatureSpacePath = m_param.m_outFolder + stringUtils::Slash();
	outputFeatureSpacePath += "Tree_R" + stringUtils::num2str(m_param.m_scale) + "_" + stringUtils::DoubleToHexString(tree->GetUniqueID()) + ".fd";

	if(!DetectorIO::WriteTree(tree, outputTreePath.c_str())) {
		std::cerr << "fail to write tree to " << outputTreePath << std::endl; return false;
	}

	if(!DetectorIO::WriteFeatureSpace(fs, outputFeatureSpacePath.c_str())) {
		std::cerr << "fail to write feature space to " << outputFeatureSpacePath << std::endl; return false;
	}
	return true;
}

void MultiSourceCFTrainerX::SamplePatchPositions(int imageIdx, const mxImageWrapper& image, Random& random, std::vector< vect3<unsigned int> >& voxels, std::vector<int>& labels) const
{
	const std::vector< vect3<double> >& worldPts = m_samplePositions[imageIdx];
	voxels.resize(worldPts.size());

	for (unsigned int i = 0; i < worldPts.size(); ++i) {
		image.World2Voxel(worldPts[i], voxels[i]);
		if (!image.PtInImage(voxels[i])) {
			std::cerr << "sample point is not on the image domain" << std::endl; exit(-1);
		}
	}

	if(m_sampleLabels[imageIdx].size() != voxels.size()) {
		std::cerr << "unexpected! label size != voxel size" << std::endl; exit(-1);
	}

	unsigned int num = labels.size();
	labels.resize( num + voxels.size() );
	for (unsigned int i = 0; i < voxels.size(); ++i)
		labels[i+num] = m_sampleLabels[imageIdx][i];
}

void MultiSourceCFTrainerX::LoadSourceImages(const std::vector< std::string >& imagePaths, const vect3<double>& spacing, std::vector< mxImageWrapper >& images) const
{
	int sourceNumber = static_cast<int>(imagePaths.size());
	if(sourceNumber <= 0) {
		std::cerr << "number of sources should be greater or equal than 1" << std::endl;
		exit(-1);
	}

	images.resize( sourceNumber );

	vect3<unsigned int> imageSize; 
	vect3<double> imageSpacing;

	for (int i = 0; i < sourceNumber; ++i) 
	{	  
		if (!images[i].ReadImage(imagePaths[i].c_str()))  {
			std::cerr << "fail to read image from " << imagePaths[i] << std::endl; 
			exit(-1);
		}
		images[i].ResampleImage(spacing);

		// check consistency
		if (i == 0) {
			imageSize = images[i].GetImageSize();
			imageSpacing = images[i].GetSpacing();
		}else {
			vect3<unsigned int> tmpImageSize = images[i].GetImageSize();
			vect3<double> tmpImageSpacing = images[i].GetSpacing();

			for (int j = 0; j < 3; ++j) {
				if (tmpImageSize[j] != imageSize[j]) {
					std::cerr << "input images should be of the same image size" << std::endl;
					exit(-1);
				}
				if (std::abs(tmpImageSpacing[j]-imageSpacing[j]) > 1e-6) {
					std::cerr << "input images should be of the same spacing" << std::endl;
					exit(-1);
				}
			}
		}
	}
}

std::auto_ptr<MultiSourceCFTrainerX::TreeType> MultiSourceCFTrainerX::TrainTree(Random& random, CompositeFeatureSpace* fs) const
{
	// get parameters
	TreeTrainingParameters treeParams = m_param.m_forestParams.treeParameters;
	unsigned int numRandomFeatures = treeParams.numOfRandomWeakLearners;

	vect3<double> spacing = m_param.m_spacing;
	int scale = m_param.m_scale;
	int numClasses = m_param.m_numClasses;
	int numImages = static_cast<int>(m_imagePaths.size());
	int sourceNumber = m_param.m_sourceNum;

	// generate random features
	fs->RandomizeFeatureSpace(random, numRandomFeatures);

	// prepare training data buffer
	MemoryDataCollection trainingData;
	trainingData.SetFeatureNumber(numRandomFeatures);
	trainingData.SetClassNumber(numClasses);

	std::vector<double>& data = trainingData.GetDataVector();
	std::vector<int>& labels = trainingData.GetLabelVector();

	unsigned int totalNumSamples = 0;

	for (int imageIdx = 0; imageIdx < numImages; ++imageIdx)
	{
		const std::vector< std::string >& imagePaths = m_imagePaths[imageIdx];
		
		// load source images
		std::vector< mxImageWrapper > images;
		LoadSourceImages(imagePaths, spacing, images);

		// randomly sampling
		std::vector< vect3<unsigned int> > voxels; 
		for (int i = 0; i < sourceNumber; ++i) {
			std::cout << "Sampling " << imagePaths[i];
			if(i != sourceNumber-1)
				std::cout << std::endl;
		}
		SamplePatchPositions(imageIdx, images[0], random, voxels, labels);
		std::cout << " (" << voxels.size() << ")" << std::endl;

		// allocate buffer for features
		try {
			data.resize( (totalNumSamples+voxels.size()) * static_cast<size_t>(numRandomFeatures) );
		} catch (std::exception& exp) {
			std::cerr << exp.what() << std::endl; exit(-1);
		}

		// parallel computing features for all sampled patches
		#pragma omp parallel for
		for (int i = 0; i < static_cast<int>(voxels.size()); ++i)
		{
			const vect3<unsigned int>& voxel = voxels[i];

			std::vector< mxImage<double> > patches;
			patches.resize(sourceNumber);

			double defaultval = 0;
			std::auto_ptr<IPatchFeatureFunctor> functor( fs->CreatePatchFeatureFunctor() );
			for (int j = 0; j < sourceNumber; ++j)
			{
				vect3<unsigned int> patchSize = fs->GetPatchSize(j);
				images[j].CropPatch(voxel, patchSize, patches[j], defaultval);

				NormalizationType type = m_param.m_sourceParams[j].normalizedType;
				mxImageUtils::Normalize(patches[j], type);

				functor->PreprocessInput(patches[j],j);
				functor->SetPatch(patches[j],j);
			}

			size_t currentPointer = static_cast<size_t>(totalNumSamples) * numRandomFeatures + static_cast<size_t>(i) * numRandomFeatures;
			for (int j = 0; j < static_cast<int>(numRandomFeatures); ++j) {
				data[currentPointer + j] = functor->GetResponse(j);
			}
		}

		totalNumSamples = static_cast<unsigned int>( totalNumSamples + voxels.size() );
	}
	trainingData.SetSampleNumber(totalNumSamples);

	// run training
	ClassificationExhaustSearchTrainingContext context(numRandomFeatures, numClasses);
	std::auto_ptr< TreeType > tree = TreeTrainer<W,S>::TrainTree(context, treeParams, &trainingData, random);

	return tree;
}


} } }

#endif
