//
//  MultiSourceCF.h (classification based on multiple sources)
//  FISH
//
//  Created by Yaozong Gao on 11/21/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __MultiSourceCF_h__
#define __MultiSourceCF_h__

#include "common/mxImageWrapper.h"
#include "forest/Forest.h"
#include "forest/wl/MemoryAxisAlignedWeakLearner.h"
#include "forest/stat/HistogramStatisticsAggregator.h"
#include "feature/CompositeFeatureSpace.h"
#include "feature/FeatureSpaceArray.h"
#include "forest/tester/ForestClassifier.h"
#include "common/stringUtils.h"

namespace BRIC { namespace IDEA { namespace FISH {

class MultiSourceCF 
{
public:
	// type definitions
	typedef MemoryAxisAlignedWeakLearner W;
	typedef HistogramStatisticsAggregator S;
	typedef Tree<W,S> TreeType;
	typedef Forest<W,S> ForestType;

	// images are re-sampled into the same image dimension before input for classification
	static void CF(std::vector<mxImageWrapper>& images, const std::vector<NormalizationType>& types, const FeatureSpaceArray<CompositeFeatureSpace>& fss, const ForestType* forest, mxImage< std::vector<double> >& probImage);
	static void CF_mask(std::vector<mxImageWrapper>& images, const std::vector<NormalizationType>& types, const mxImage<char>& maskImage, const FeatureSpaceArray<CompositeFeatureSpace>& fss, const ForestType* forest, mxImage< std::vector<double> >& probImage);
	static void ReadInfoIni(const char* detectorFolder, int scale, vect3<double>& spacing, std::vector<NormalizationType>& normalizationTypes);

private:
	static void CheckParams(std::vector<mxImageWrapper>& images, const std::vector<NormalizationType>& types, const FeatureSpaceArray<CompositeFeatureSpace>& fss, const ForestType* forest);
	static void GetFunctorTypes(const std::vector<NormalizationType>& types, std::vector<FunctorType>& functorTypes, int& patchFunctorNumber);
	static int GetClassNumber(const ForestType* forest);
};

//////////////////////////////////////////////////////////////////////////

void MultiSourceCF::CF(std::vector<mxImageWrapper>& images, const std::vector<NormalizationType>& types, const FeatureSpaceArray<CompositeFeatureSpace>& fss, const ForestType* forest, mxImage< std::vector<double> >& probImage)
{
	boost::timer::auto_cpu_timer timer;

	CheckParams(images, types, fss, forest);

	// allocate memory space before image padding
	vect3<unsigned int> imageSize = images[0].GetImageSize();
	images[0].CopyImageInfoTo(probImage);
	probImage.SetImageSize(imageSize);

	// construct functor type array
	int patchFunctorNumber = 0;
	std::vector<FunctorType> functorTypes;
	GetFunctorTypes(types, functorTypes, patchFunctorNumber);

	// get patch sizes for different source images
	std::vector< vect3<unsigned int> > patchSizes;
	patchSizes.resize(fss[0].GetNumberFeatureSpaces());
	for (unsigned int i = 0; i < patchSizes.size(); ++i)
		patchSizes[i] = fss[0].GetPatchSize(i);

	// preprocess the images for ImageFunctorType
	std::auto_ptr<HybridImageFeatureFunctor> tmpFunctor(fss[0].CreateHybridFeatureFunctor(functorTypes));
	for (unsigned int i = 0; i < functorTypes.size(); ++i)
	{
		if (functorTypes[i]==ImageFunctorType) 
		{
			vect3<unsigned int> patchSize = patchSizes[i];
			images[i].PadImage<double>(patchSize, static_cast<double>(0));
			tmpFunctor->PreprocessInput(*(images[i].as<double>()),i);
		}
	}

	#pragma omp parallel for schedule(dynamic)
	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z)
	{
		std::cout << z << "/" << imageSize[2] << std::endl;

		// create feature functors 
		std::vector<HybridImageFeatureFunctor> functors;
		functors.resize(fss.size());
		for (unsigned int j = 0; j < functors.size(); ++j)
			new (&functors[j]) HybridImageFeatureFunctor(fss[j], functorTypes);
		
		// setup image feature functors
		for (unsigned int i = 0; i < functorTypes.size(); ++i)
		{
			if (functorTypes[i] == ImageFunctorType)
			{
				for (unsigned int j = 0; j < functors.size(); ++j)
					functors[j].SetImage(*images[i].as<double>(),i);
			}
		}

		for(int y = 0; y < static_cast<int>(imageSize[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
			{
				std::vector< mxImage<double> > patchBuffer;
				if (patchFunctorNumber != 0) 
					patchBuffer.resize(patchFunctorNumber);
				int patchIndex = 0;

				vect3<unsigned int> voxel(x,y,z);
				for (unsigned int i = 0; i < functorTypes.size(); ++i)
				{
					vect3<unsigned int> patchSize = patchSizes[i];
					if (functorTypes[i] == ImageFunctorType) {
						vect3<unsigned int> padvoxel;
						padvoxel[0] = x + patchSize[0]/2; padvoxel[1] = y + patchSize[1]/2; padvoxel[2] = z + patchSize[2]/2;
						for (unsigned int j = 0; j < functors.size(); ++j)
							functors[j].SetVoxel(padvoxel, i);
					}else if (functorTypes[i] == PatchFunctorType) {
						images[i].CropPatch(voxel, patchSize, patchBuffer[patchIndex], static_cast<double>(0));
						mxImageUtils::Normalize(patchBuffer[patchIndex],types[i]);
						tmpFunctor->PreprocessInput(patchBuffer[patchIndex],i);
						for (unsigned int j = 0; j < functors.size(); ++j)
							functors[j].SetImage(patchBuffer[patchIndex],i);
						++ patchIndex;
					}else {
						std::cerr << "unrecognized functor type" << std::endl; exit(-1);
					}
				}
				ForestClassifier::Classify2(forest,functors,probImage(x,y,z));
			} // end x
		} // end y
	} // end z

}


void MultiSourceCF::CF_mask(std::vector<mxImageWrapper>& images, const std::vector<NormalizationType>& types, const mxImage<char>& maskImage, const FeatureSpaceArray<CompositeFeatureSpace>& fss, const ForestType* forest, mxImage< std::vector<double> >& probImage)
{
	boost::timer::auto_cpu_timer timer;

	CheckParams(images, types, fss, forest);

	vect3<unsigned int> imageSize = images[0].GetImageSize();
	images[0].CopyImageInfoTo(probImage);
	probImage.SetImageSize(imageSize);

	if (maskImage.GetImageSize() != images[0].GetImageSize()) {
		std::cerr << "mask image must be of the same dimension with original image" << std::endl;

		vect3<unsigned int> maskSize = maskImage.GetImageSize();
		std::cerr << "mask image size :" << maskSize[0] << "," << maskSize[1] << "," << maskSize[2] << std::endl;
		std::cerr << "orig image size :" << imageSize[0] << "," << imageSize[1] << "," << imageSize[2] << std::endl;
		exit(-1);
	}

	int numClasses = GetClassNumber(forest);

	// construct functor type array
	int patchFunctorNumber = 0;
	std::vector<FunctorType> functorTypes;
	GetFunctorTypes(types, functorTypes, patchFunctorNumber);

	// get patch sizes for different source images
	std::vector< vect3<unsigned int> > patchSizes;
	patchSizes.resize(fss[0].GetNumberFeatureSpaces());
	for (unsigned int i = 0; i < patchSizes.size(); ++i)
		patchSizes[i] = fss[0].GetPatchSize(i);

	// preprocess the images for ImageFunctorType
	std::auto_ptr<HybridImageFeatureFunctor> tmpFunctor(fss[0].CreateHybridFeatureFunctor(functorTypes));
	for (unsigned int i = 0; i < functorTypes.size(); ++i)
	{
		if (functorTypes[i]==ImageFunctorType) 
		{
			vect3<unsigned int> patchSize = patchSizes[i];
			images[i].PadImage<double>(patchSize, static_cast<double>(0));
			tmpFunctor->PreprocessInput(*(images[i].as<double>()),i);
		}
	}

	#pragma omp parallel for schedule(dynamic)
	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z)
	{
		std::cout << z << "/" << imageSize[2] << std::endl;

		// create feature functors 
		std::vector<HybridImageFeatureFunctor> functors;
		functors.resize(fss.size());
		for (unsigned int j = 0; j < functors.size(); ++j)
			new (&functors[j]) HybridImageFeatureFunctor(fss[j], functorTypes);

		// setup image feature functors
		for (unsigned int i = 0; i < functorTypes.size(); ++i)
		{
			if (functorTypes[i] == ImageFunctorType)
			{
				for (unsigned int j = 0; j < functors.size(); ++j)
					functors[j].SetImage(*images[i].as<double>(),i);
			}
		}

		for(int y = 0; y < static_cast<int>(imageSize[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
			{
				if (maskImage(x,y,z) == 0) {
					probImage(x,y,z).resize(numClasses);
					for (int i = 0; i < numClasses; ++i)
						probImage(x,y,z)[i] = 0;
					continue;
				}

				std::vector< mxImage<double> > patchBuffer;
				if (patchFunctorNumber != 0) 
					patchBuffer.resize(patchFunctorNumber);
				int patchIndex = 0;

				vect3<unsigned int> voxel(x,y,z);
				for (unsigned int i = 0; i < functorTypes.size(); ++i)
				{
					vect3<unsigned int> patchSize = patchSizes[i];
					if (functorTypes[i] == ImageFunctorType) {
						vect3<unsigned int> padvoxel;
						padvoxel[0] = x + patchSize[0]/2; padvoxel[1] = y + patchSize[1]/2; padvoxel[2] = z + patchSize[2]/2;
						for (unsigned int j = 0; j < functors.size(); ++j)
							functors[j].SetVoxel(padvoxel, i);
					}else if (functorTypes[i] == PatchFunctorType) {
						images[i].CropPatch(voxel, patchSize, patchBuffer[patchIndex], static_cast<double>(0));
						mxImageUtils::Normalize(patchBuffer[patchIndex],types[i]);
						tmpFunctor->PreprocessInput(patchBuffer[patchIndex],i);
						for (unsigned int j = 0; j < functors.size(); ++j)
							functors[j].SetImage(patchBuffer[patchIndex],i);
						++ patchIndex;
					}else {
						std::cerr << "unrecognized functor type" << std::endl; exit(-1);
					}
				}
				ForestClassifier::Classify2(forest,functors,probImage(x,y,z));
			} // end x
		} // end y
	} // end z

}


void MultiSourceCF::ReadInfoIni(const char* detectorFolder, int scale, vect3<double>& spacing, std::vector<NormalizationType>& normalizationTypes)
{
	char filename[1024] = {0};
	sprintf(filename, "%s%sinfo_R%d.ini", detectorFolder, stringUtils::Slash().c_str(), scale);

	// load information from INI file
	NormalizationType type = NONE;
	boost::property_tree::ptree pt;
	try {
		boost::property_tree::ini_parser::read_ini(filename, pt);
		std::string strSpacing = pt.get<std::string>("Info.Resolution");
		spacing = stringUtils::ParseVect3<double>(strSpacing);

		int sourceNumber = pt.get<int>("Info.SourceNumber");
		if(sourceNumber < 1) {
			std::cerr << "source number should >= 1" << std::endl; exit(-1);
		}
		normalizationTypes.resize(sourceNumber);

		for (int i = 0; i < sourceNumber; ++i)
		{
			std::string field_name = "Source"+stringUtils::num2str(i)+".Normalization";
			std::string strType = pt.get<std::string>(field_name);
			if (strType == "NONE") {
				normalizationTypes[i] = NONE;
			}else if(strType == "UNIT_NORM") {
				normalizationTypes[i] = UNIT_NORM;
			}else if(strType == "MAX_ONE") {
				normalizationTypes[i] = MAX_ONE;
			}else if(strType == "MIN_MAX") {
				normalizationTypes[i] = MIN_MAX;
			}else if(strType == "ZERO_MEAN_ONE_STD") {
				normalizationTypes[i] = ZERO_MEAN_ONE_STD;
			}else {
				std::cerr << "normalization type unsupported" << std::endl;
				std::cerr << "Use: NONE, UNIT_NORM, MAX_ONE, MIN_MAX, ZERO_MEAN_ONE_STD" << std::endl;
				exit(-1);
			}
		}
	} catch ( std::runtime_error& error ) {
		std::cerr << error.what() << std::endl;
		exit(-1);
	}	
}

void MultiSourceCF::GetFunctorTypes(const std::vector<NormalizationType>& types, std::vector<FunctorType>& functorTypes, int& patchFunctorNumber)
{
	functorTypes.resize(types.size());
	for (unsigned int i = 0; i < types.size(); ++i) 
	{
		if (types[i] == NONE) 
			functorTypes[i] = ImageFunctorType;
		else {
			functorTypes[i] = PatchFunctorType;
			++ patchFunctorNumber;
		}
	}
}

void MultiSourceCF::CheckParams(std::vector<mxImageWrapper>& images, const std::vector<NormalizationType>& types, const FeatureSpaceArray<CompositeFeatureSpace>& fss, const ForestType* forest)
{
	if (forest->GetTreeNumber() == 0) {
		std::cerr << "empty forest" << std::endl; exit(-1);
	}
	if (images.size() == 0) {
		std::cerr << "empty image set" << std::endl; exit(-1);
	}
	if (images.size() != types.size()) {
		std::cerr << "image number (" << images.size() << ")" << "!= type number (" << types.size() << ")" << std::endl; exit(-1);
	}
	if (fss.size() == 0) {
		std::cerr << "empty feature space" << std::endl; exit(-1);
	}
	if (fss[0].GetNumberFeatureSpaces() != static_cast<int>(images.size())) {
		std::cerr << "only " << fss[0].GetNumberFeatureSpaces() << " source images is required, but the input number is " << images.size() << std::endl; exit(-1);
	}

	vect3<unsigned int> imageSize = images[0].GetImageSize();
	for (unsigned int i = 1; i < images.size(); ++i) {
		vect3<unsigned int> tmpImageSize = images[i].GetImageSize();
		if (tmpImageSize[0]!=imageSize[0] || tmpImageSize[1]!=imageSize[1] || tmpImageSize[2]!=imageSize[2]) {
			std::cerr << "source images should be of the same dimension" << std::endl; exit(-1);
		}
	}
}

int MultiSourceCF::GetClassNumber(const ForestType* forest)
{
	const HistogramStatisticsAggregator& tmpNode = forest->GetTree(0)->GetAnyLeafStatisticsAggregator();
	int numClasses = tmpNode.GetClassNumber();
	return numClasses;
}

} } }

#endif