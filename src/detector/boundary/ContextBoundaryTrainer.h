//
//  ContextBoundaryTrainer.h
//  FISH
//
//  Created by Yaozong Gao on 02/15/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __ContextBoundaryTrainer_h__
#define __ContextBoundaryTrainer_h__

#include "stdafx.h"
#include "detector/ContextRFTrainer.h"
#include "sample/ImageSampler.h"
#include "common/mxImageUtils.h"

// debug headers
#include "extern/ImageHelper.h"

namespace BRIC { namespace IDEA { namespace FISH {

// context boundary training parameters
struct ContextBoundaryParameters
{
	ContextRFParameters rfParameters;

	std::string annotFile;
	double sampleSigmaIn;
	double sampleSigmaOut;
	unsigned int samplePerImage;
	vect3<double> finalSpacing;

	// methods
	bool Load(const char* file);
	static bool WriteDefaultConfig(const char* file);
};

// context boundary trainer
template <typename T, int Dimension>
class ContextBoundaryTrainer : public ContextRFTrainer<T, Dimension>
{
public:
	typedef typename ContextRFTrainer<T, Dimension>::TreeType TreeType;
	
	// General
	bool LoadConfigFile(const char* file);
	const ContextBoundaryParameters& GetParameters() const { return m_params; }
	std::string GetDetectorFolder() const;

	// training
	std::auto_ptr<IFeatureSpace> CreateFeatureSpace();
	std::auto_ptr<TreeType> TrainTree(Random& random, IFeatureSpace* fs);

	// implementation
	virtual const std::vector<std::string>& GetImagePathVector() { return m_imagePaths; }
	virtual void DrawSamples(const mxImage<T>& image, unsigned int imageIdx, Random& random, std::vector< vect3<unsigned int> >& voxels, std::vector<double>& targets);

	// io
	bool CreateOutputFolder() const;
	bool SaveTree(const typename ContextBoundaryTrainer<T, Dimension>::TreeType* tree) const;
	bool SaveFeatureSpace(const IFeatureSpace* fs, double treeID) const;
	bool SaveDetectionInfo() const;

private:
	bool LoadAnnotation(const char* file);
	vect3<double> GetDisplacement(const NearestPointIndexer<double>& bk_nn, const vect3<unsigned int>& testVoxel);

	ContextBoundaryParameters m_params;

	std::vector<std::string> m_imagePaths;
	std::vector<std::string> m_segImagePaths;
};

//////////////////////////////////////////////////////////////////////////


bool ContextBoundaryParameters::Load(const char* file)
{
	using boost::property_tree::ptree;

	ptree pt;
	try{
		boost::property_tree::ini_parser::read_ini(file, pt);
	}
	catch (boost::property_tree::ini_parser_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	try {
		// General section
		rfParameters.detectorName = pt.get<std::string>("General.Name");
		annotFile = pt.get<std::string>("General.Annotation");
		rfParameters.rootFolder = pt.get<std::string>("General.RootFolder");
		rfParameters.contextLevel = pt.get<int>("General.Level");
		rfParameters.scale = pt.get<int>("General.Scale");
		rfParameters.spacing = stringUtils::ParseVect3<double>(pt.get<std::string>("General.Spacing"));
		finalSpacing = stringUtils::ParseVect3<double>(pt.get<std::string>("General.FinalSpacing"));
		sampleSigmaIn = pt.get<double>("General.SampleSigmaIn");
		sampleSigmaOut = pt.get<double>("General.SampleSigmaOut");
		samplePerImage = pt.get<unsigned int>("General.TrainingSampleNumberPerImage");

		std::vector<std::string> contextDetectorNames;
		stringUtils::Split(pt.get<std::string>("General.ContextDetectorNames"), ' ', contextDetectorNames);
		rfParameters.sources.resize(contextDetectorNames.size() + 1);
		for (unsigned int i = 0; i < contextDetectorNames.size(); ++i)
			rfParameters.sources[i + 1].detectorName = contextDetectorNames[i];

		// Forest section	
		rfParameters.forestParams.numTrees = pt.get<unsigned int>("Forest.NumTrees");
		rfParameters.forestParams.treeParameters.maxTreeDepth = pt.get<unsigned int>("Forest.MaxTreeDepth");
		rfParameters.forestParams.treeParameters.numOfCandidateThresholdsPerWeakLearner = pt.get<unsigned int>("Forest.NumThresholds");
		rfParameters.forestParams.treeParameters.numOfRandomWeakLearners = pt.get<unsigned int>("Forest.NumWeakLearners");
		rfParameters.forestParams.treeParameters.minElementsOfLeafNode = pt.get<unsigned int>("Forest.MinLeafNum");
		rfParameters.forestParams.treeParameters.minInformationGain = pt.get<double>("Forest.MinInfoGain");

		// Intensity section
		rfParameters.normalizationType = mxImageUtils::GetNormalizationByString(pt.get<std::string>("Intensity.Normalization"));
		rfParameters.sources[0].level = -1;
		rfParameters.sources[0].weight = pt.get<double>("Intensity.Weight");
		stringUtils::str2vec<unsigned int>(pt.get<std::string>("Intensity.FilterSize"), rfParameters.sources[0].filterSizes, ' ');
		rfParameters.sources[0].patchSize = stringUtils::ParseVect3<unsigned int>(pt.get<std::string>("Intensity.PatchSize"));

		// Context section
		for (unsigned int i = 0; i < contextDetectorNames.size(); ++i)
		{
			std::string section_name = "Context" + contextDetectorNames[i];
			rfParameters.sources[i + 1].level = pt.get<int>(section_name + ".Level");
			rfParameters.sources[i + 1].diffOption = pt.get<std::string>(section_name + ".Diff");
			rfParameters.sources[i + 1].weight = pt.get<double>(section_name + ".Weight");
			stringUtils::str2vec<unsigned int>(pt.get<std::string>(section_name + ".FilterSize"), rfParameters.sources[i + 1].filterSizes, ' ');
			rfParameters.sources[i + 1].patchSize = stringUtils::ParseVect3<unsigned int>(pt.get<std::string>(section_name + ".PatchSize"));
		}

	}
	catch (boost::property_tree::ptree_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}

bool ContextBoundaryParameters::WriteDefaultConfig(const char* file)
{
	using boost::property_tree::ptree;
	ptree pt;
	try{
		pt.put("General.Name", "Please input the detector name");
		pt.put("General.Annotation", "Please input the path for annotation file");
		pt.put("General.RootFolder", "Please specify the root folder (also used as output folder)");
		pt.put("General.Level", "Please input context level (0+)");
		pt.put("General.Scale", "Please input scale index (typically 1-4)");
		pt.put("General.Spacing", "Please input three doubles of spacing for this scale (mm)");
		pt.put("General.FinalSpacing", "Please input the spacing of the finest resolution (mm)");
		pt.put("General.SampleSigmaOut", "Please specify the sigma for Gaussian Sampling Outside (mm)");
		pt.put("General.SampleSigmaIn", "Please specify the sigma for Gaussian Sampling Inside (mm)");
		pt.put("General.TrainingSampleNumberPerImage", "Please specify how many training samples per image");
		pt.put("General.ContextDetectorNames", "a set of detector names separated by spaces (e.g., name1 name2)");

		pt.put("Forest.NumTrees", 1);
		pt.put("Forest.MaxTreeDepth", 15);
		pt.put("Forest.NumThresholds", 1000);
		pt.put("Forest.NumWeakLearners", 1000);
		pt.put("Forest.MinLeafNum", 5);
		pt.put("Forest.MinInfoGain", 0);

		pt.put("Intensity.Weight", 1);
		pt.put("Intensity.FilterSize", "3 5");
		pt.put("Intensity.PatchSize", "15 15 15");
		pt.put("Intensity.Normalization", "NONE");

		pt.put("Contextname1.Weight", 1);
		pt.put("Contextname1.Level", 0);
		pt.put("Contextname1.Diff", "NONE");
		pt.put("Contextname1.FilterSize", "3 5");
		pt.put("Contextname1.PatchSize", "30 30 30");

		pt.put("Contextname2.Weight", 1);
		pt.put("Contextname2.Level", 0);
		pt.put("Contextname2.Diff", "name1");
		pt.put("Contextname2.FilterSize", "3 5");
		pt.put("Contextname2.PatchSize", "30 30 30");

	}
	catch (boost::property_tree::ptree_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	std::ofstream out(file);
	if (!out) {
		std::cerr << "could not open " << file << " for write" << std::endl; return false;
	}

	try {
		boost::property_tree::ini_parser::write_ini(out, pt);
	}
	catch (boost::property_tree::ini_parser_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	out.close();
	return true;
}


//////////////////////////////////////////////////////////////////////////

// General
template <typename T, int Dimension>
bool ContextBoundaryTrainer<T, Dimension>::LoadConfigFile(const char* file)
{
	if (!m_params.Load(file))
		return false;

	if (!LoadAnnotation(m_params.annotFile.c_str())) {
		std::cerr << "fails to load annotation from file " << m_params.annotFile << std::endl; exit(-1);
	}
	return true;
}

template <typename T, int Dimension>
bool ContextBoundaryTrainer<T, Dimension>::LoadAnnotation(const char* file)
{
	m_imagePaths.clear();
	m_segImagePaths.clear();

	std::vector< std::string > lines;
	if (!stringUtils::ReadLines(file, lines)) {
		std::cerr << "fails to read annotations from " << file << std::endl;
		return false;
	}

	for (unsigned int i = 0; i < lines.size(); i += 2) {

		if (i + 1 >= lines.size()) {
			std::cerr << "incomplete annotation files" << std::endl;
			return false;
		}

		std::vector< std::string > imagePathSplits;
		stringUtils::Split(lines[i], '\t', imagePathSplits);
		if (imagePathSplits.size() < 1) {
			std::cerr << "path format problem" << std::endl; return false;
		}
		m_imagePaths.push_back(imagePathSplits[0]);
		imagePathSplits.clear();

		stringUtils::Split(lines[i+1], '\t', imagePathSplits);
		if (imagePathSplits.size() < 1) {
			std::cerr << "path format problem" << std::endl; return false;
		}
		m_segImagePaths.push_back(imagePathSplits[0]);
	}

	return true;
}

template <typename T, int Dimension>
std::string ContextBoundaryTrainer<T, Dimension>::GetDetectorFolder() const
{
	std::string detectorFolder;
	if (m_params.rfParameters.contextLevel == 0)
		detectorFolder = m_params.rfParameters.rootFolder + stringUtils::Slash() + m_params.rfParameters.detectorName;
	else
		detectorFolder = m_params.rfParameters.rootFolder + stringUtils::Slash() + m_params.rfParameters.detectorName + "_C" + stringUtils::num2str(m_params.rfParameters.contextLevel);

	return detectorFolder;
}

template <typename T, int Dimension>
std::auto_ptr<IFeatureSpace> ContextBoundaryTrainer<T, Dimension>::CreateFeatureSpace()
{
	return ContextRFTrainer<T, Dimension>::CreateFeatureSpace(m_params.rfParameters);
}

template <typename T, int Dimension>
std::auto_ptr<typename ContextBoundaryTrainer<T, Dimension>::TreeType> ContextBoundaryTrainer<T, Dimension>::TrainTree(Random& random, IFeatureSpace* fs)
{
	return ContextRFTrainer<T, Dimension>::TrainTree(random, fs, m_params.rfParameters);
}

template <typename T, int Dimension>
void ContextBoundaryTrainer<T, Dimension>::DrawSamples(const mxImage<T>& image, unsigned int imageIdx, Random& random, std::vector< vect3<unsigned int> >& voxels, std::vector<double>& targets)
{
	vect3<unsigned int> imageSize = image.GetImageSize();

	vect3<unsigned int> lowerBound, upperBound;
	for (int i = 0; i < 3; ++i) {
		lowerBound[i] = 0;
		upperBound[i] = imageSize[i] - 1;
	}

	//////////////////////////////////////////////////////////////////////////
	// Gaussian sampling scheme (more samples near the surface, less samples far away)

	mxImage<unsigned char> isoSegImage;
	{
		mxImage<unsigned char> segImage;
		if (!ImageHelper::ReadImage<T, unsigned char>(m_segImagePaths[imageIdx].c_str(), segImage)) {
			std::cerr << "fail to read image from " << m_segImagePaths[imageIdx] << std::endl;
			exit(-1);
		}
		mxImageUtils::Resample(segImage, m_params.finalSpacing, isoSegImage);
	}

	Surface surf;
	unsigned char isoval = 255;
	SurfaceUtils::IsoSurfaceFromImage(isoSegImage, surf, isoval);
	SurfaceUtils::SmoothSurface(surf, 30);
	surf.UpdateNormals();

	std::vector< vect3<double> > points;
	ImageSampler::SamplePointsNearSurface(surf, m_params.samplePerImage, m_params.sampleSigmaIn, m_params.sampleSigmaOut, random, points);

	voxels.reserve(points.size());
	for (size_t i = 0; i < points.size(); ++i)
	{
		vect3<int> voxel;
		mxImageUtils::World2Voxel(image, points[i], voxel);
		if (!image.PtInImage(voxel[0], voxel[1], voxel[2]))
			continue;

		voxels.push_back(vect3<unsigned int>(voxel[0], voxel[1], voxel[2]));
	}

	//////////////////////////////////////////////////////////////////////////
	// compute surface displacements

	std::vector< vect3<double> > bkBoundaryVoxels;
	unsigned char binary_threshold = 128;

	// find points on object boundary (background)
	mxImageUtils::ExtractBoundaryPoints2(isoSegImage, binary_threshold, bkBoundaryVoxels);
	for (size_t i = 0; i < bkBoundaryVoxels.size(); ++i)
	{
		mxImageUtils::Voxel2World(isoSegImage, bkBoundaryVoxels[i]);
		mxImageUtils::World2Voxel(image, bkBoundaryVoxels[i]);
	}

	NearestPointIndexer<double> bk_nn;
	bk_nn.Initialize(bkBoundaryVoxels);

	size_t old_size = targets.size();
	targets.resize(old_size + voxels.size() * Dimension);

	if (Dimension == 1)
	{
		mxImage<unsigned char> tmpSegImage;
		{
			mxImage<unsigned char> segImage;
			if (!ImageHelper::ReadImage<T, unsigned char>(m_segImagePaths[imageIdx].c_str(), segImage)) {
				std::cerr << "fail to read image from " << m_segImagePaths[imageIdx] << std::endl;
				exit(-1);
			}
			mxImageUtils::Resample(segImage, m_params.rfParameters.spacing, tmpSegImage);
		}

		for (size_t i = 0; i < voxels.size(); ++i) {

			vect3<double> offset = GetDisplacement(bk_nn, voxels[i]);

			if (tmpSegImage(voxels[i][0], voxels[i][1], voxels[i][2]) == 0)
				targets[old_size + i] = offset.norm();
			else
				targets[old_size + i] = -offset.norm();
		}
	}
	else if (Dimension == 3) {
		for (size_t i = 0; i < voxels.size(); ++i) {
			vect3<double> offset = GetDisplacement(bk_nn, voxels[i]);
			targets[old_size + i * 3] = offset[0];
			targets[old_size + i * 3 + 1] = offset[1];
			targets[old_size + i * 3 + 2] = offset[2];
		}
	}
	else {
		std::cerr << "Dimension can only be 1 or 3" << std::endl;
		exit(-1);
	}


	//////////////////////////////////////////////////////////////////////////
	// debug session
	//mxImage< boost::array<double, 3> > displacementField;
	//displacementField.CopyImageInfo(isoSegImage);
	//displacementField.SetImageSize(isoSegImage.GetImageSize());

	//vect3<unsigned int> isoImageSize = isoSegImage.GetImageSize();
	//for (size_t i = 0; i < voxels.size(); ++i)
	//{
	//	boost::array<double, 3>& ref = displacementField(voxels[i]);
	//	ref[0] = targets[old_size + i * 3];
	//	ref[1] = targets[old_size + i * 3 + 1];
	//	ref[2] = targets[old_size + i * 3 + 2];
	//}

	//mxImage<int> sampleImage;
	//sampleImage.CopyImageInfo(image);
	//sampleImage.SetImageSize(image.GetImageSize());
	//sampleImage.Fill(0);
	//for (size_t i = 0; i < voxels.size(); ++i)
	//	sampleImage(voxels[i]) += 1;

	//// save sample image
	//ImageHelper::WriteImage<int,int>(sampleImage, "/Users/Yaozong/Desktop/sample.mha");

	//// save distance image
	//mxImage<double> distanceMap;
	//mxImageUtils::Vector2Norm<double, 3>(displacementField, distanceMap);
	//ImageHelper::WriteImage<double, double>(distanceMap, "/Users/yzgao/Desktop/dist.mha");

	// save iso seg
	//mxImage<unsigned char> segImage;
	//if (!ImageHelper::ReadImage<T, unsigned char>(m_segImagePaths[imageIdx].c_str(), segImage)) {
	//	std::cerr << "fail to read image from " << m_segImagePaths[imageIdx] << std::endl;
	//	exit(-1);
	//}
	//mxImageUtils::Resample(segImage, m_params.rfParameters.spacing, isoSegImage);
	//ImageHelper::WriteImage<unsigned char, unsigned char>(isoSegImage, "/Users/Yaozong/Desktop/seg.mha");

	//// save xmap, ymap, zmap
	//mxImage<double> xmap, ymap, zmap;
	//xmap.CopyImageInfo(displacementField);
	//ymap.CopyImageInfo(displacementField);
	//zmap.CopyImageInfo(displacementField);

	//xmap.SetImageSize(displacementField.GetImageSize());
	//ymap.SetImageSize(displacementField.GetImageSize());
	//zmap.SetImageSize(displacementField.GetImageSize());

	//for (unsigned int z = 0; z < isoImageSize[2]; ++z) {
	//	for (unsigned int y = 0; y < isoImageSize[1]; ++y) {
	//		for (unsigned int x = 0; x < isoImageSize[0]; ++x) {
	//			boost::array<double, 3>& ref = displacementField(x, y, z);
	//			xmap(x, y, z) = ref[0];
	//			ymap(x, y, z) = ref[1];
	//			zmap(x, y, z) = ref[2];
	//		}
	//	}
	//}

	//ImageHelper::WriteImage<double,double>(xmap, "/Users/yzgao/Desktop/xmap.mha");
	//ImageHelper::WriteImage<double,double>(ymap, "/Users/yzgao/Desktop/ymap.mha");
	//ImageHelper::WriteImage<double,double>(zmap, "/Users/yzgao/Desktop/zmap.mha");
}

template <typename T, int Dimension>
vect3<double> ContextBoundaryTrainer<T, Dimension>::GetDisplacement(const NearestPointIndexer<double>& bk_nn, const vect3<unsigned int>& testVoxel)
{
	vect3<double> query( testVoxel[0], testVoxel[1], testVoxel[2] );
	vect3<double> nn_pt = bk_nn.SearchNearestNeighbor(query);
	vect3<double> displacement = nn_pt - query;

	return displacement;
}

// io
template <typename T, int Dimension>
bool ContextBoundaryTrainer<T, Dimension>::CreateOutputFolder() const
{
	std::string detectorFolder = GetDetectorFolder();
	if (!boost::filesystem::is_directory(detectorFolder))
		boost::filesystem::create_directories(detectorFolder);

	return true;
}

template <typename T, int Dimension>
bool ContextBoundaryTrainer<T, Dimension>::SaveTree(const typename ContextBoundaryTrainer<T, Dimension>::TreeType* tree) const
{
	std::string detectorFolder = GetDetectorFolder();
	char treeFileName[2048] = { 0 };
	sprintf(treeFileName, "%s%sTree_R%d_%s.bin", detectorFolder.c_str(), stringUtils::Slash().c_str(), m_params.rfParameters.scale, stringUtils::DoubleToHexString(tree->GetUniqueID()).c_str());

	std::ofstream out(treeFileName, std::ios::binary);
	if (!out) {
		std::cerr << "fails to open " << treeFileName << " for write" << std::endl; return false;
	}
	tree->Serialize(out);
	out.close();
	return true;
}

template <typename T, int Dimension>
bool ContextBoundaryTrainer<T, Dimension>::SaveFeatureSpace(const IFeatureSpace* fs, double treeID) const
{
	std::string detectorFolder = GetDetectorFolder();
	char fsFileName[2048] = { 0 };
	sprintf(fsFileName, "%s%sTree_R%d_%s.fd", detectorFolder.c_str(), stringUtils::Slash().c_str(), m_params.rfParameters.scale, stringUtils::DoubleToHexString(treeID).c_str());

	std::ofstream out(fsFileName, std::ios::binary);
	if (!out) {
		std::cerr << "fails to open " << fsFileName << " for write" << std::endl; return false;
	}
	fs->Serialize(out);
	out.close();
	return true;
}

template <typename T, int Dimension>
bool ContextBoundaryTrainer<T, Dimension>::SaveDetectionInfo() const
{
	boost::property_tree::ptree pt;

	char buffer[100] = { 0 };
	sprintf(buffer, "%.3f %.3f %.3f", m_params.rfParameters.spacing[0], m_params.rfParameters.spacing[1], m_params.rfParameters.spacing[2]);
	pt.put("Info.Resolution", buffer);
	pt.put("Info.IntensityNormalization", mxImageUtils::GetNormalizationString(m_params.rfParameters.normalizationType));
	pt.put("Info.SampleSigmaIn", m_params.sampleSigmaIn);
	pt.put("Info.SampleSigmaOut", m_params.sampleSigmaOut);
	pt.put("Context.Level", m_params.rfParameters.contextLevel);

	if (m_params.rfParameters.contextLevel > 0)
	{
		std::string detectorNames;
		for (unsigned int i = 1; i < m_params.rfParameters.sources.size(); ++i) {
			if (i != 1)
				detectorNames += " " + m_params.rfParameters.sources[i].detectorName;
			else
				detectorNames = m_params.rfParameters.sources[i].detectorName;

			std::string section_name = std::string("Context") + m_params.rfParameters.sources[i].detectorName;
			pt.put(section_name + ".Level", m_params.rfParameters.sources[i].level);
			pt.put(section_name + ".Diff", m_params.rfParameters.sources[i].diffOption);
		}
		pt.put("Context.Detectors", detectorNames.c_str());
	}

	std::string detectorFolder = GetDetectorFolder();
	char path[2048] = { 0 };
	sprintf(path, "%s%sinfo_R%d.ini", detectorFolder.c_str(), stringUtils::Slash().c_str(), m_params.rfParameters.scale);

	try{
		boost::property_tree::ini_parser::write_ini(path, pt);
	}
	catch (boost::property_tree::ptree_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}

} } }

#endif 