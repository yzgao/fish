//
//  JAT_bd_multires_tester.h
//  FISH
//
//  Created by Yaozong Gao on 04/07/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __JAT_bd_multires_tester_h__
#define __JAT_bd_multires_tester_h__


#include "forest/RandomForest.h"
#include "detector/DetectorIO.h"
#include "common/mxImageUtils.h"
#include "detector/DetectorIO.h"
#include "detector/boundary/JAT_bd_tester.h"
#include "deform/BDModel.h"

namespace BRIC { namespace IDEA { namespace FISH {


/************************************************************************/
/* multi-resolution configuration file                                  */
/************************************************************************/

class SingleResolutionSetting
{
public:
	std::vector<std::string> names;
	std::vector<int> levels;
	int scale;
	double smooth_sigma_mm;

	int output_map;			// whether output intermediate maps
};

class MultiResolutionConfigFile
{
public:
	int num_resolutions;
	std::vector<SingleResolutionSetting> singleres_predict_config;
	std::vector< std::vector<BDModel_config> > singleres_deform_config;

	std::vector < std::string > bdids;
	std::vector < std::string > surf_paths;

	bool Read(const char* file);
	static bool WriteDefault(const char* file);
};


/************************************************************************/
/* mask provider                                                        */
/*   - give updated mask region for multi-resolution map estimation     */
/************************************************************************/

class TestMaskProvider
{
public:

	// set boundary surfaces
	void SetBoundarySurfaces(Surface* surfs, int num_surfs) { m_surfs = surfs; m_num_surfs = num_surfs;  }

	// get one specific boundary surface
	const Surface* GetBoundarySurface(int bd_idx) const { assert_message(bd_idx >= 0 && bd_idx < m_num_surfs, "bd_idx out of bound");  return &(m_surfs[bd_idx]); }
	Surface* GetBoundarySurface(int bd_idx) { assert_message(bd_idx >= 0 && bd_idx < m_num_surfs, "bd_idx out of bound");  return &(m_surfs[bd_idx]); }
	
	// set deformation configurations for each resolution
	void SetDeformationConfigs(const std::vector< std::vector<BDModel_config> >& configs) { m_deform_configs = configs;  }
	
	// notify with the updated maps
	void Notify(int res_idx, int bd_idx, const mxImage<double>* label_maps, const mxImage<double>* disp_maps);

	// get update mask region
	template <typename T>
	void GetUpdateMaskRegion(int bd_idx, double band_world, const mxImage<T>& ref_header, mxImage<unsigned char>& out_mask, vect3<int>& sp, vect3<int>& ep) const;

private:

	std::vector< std::vector<BDModel_config> > m_deform_configs;
	Surface* m_surfs;
	int m_num_surfs;
};


/************************************************************************/
/* multi-resolution detector info                                       */
/************************************************************************/

class JAT_singleres_detector_info
{
public:

	// attributes
	vect3<double> spacing;													// resolution
	int level;																// context level
	int modality_num;														// modality num
	bool discard_background;												// discard background
	std::vector< std::string > bdids;										// boundary names
	std::vector< std::vector<double> > intensity_scale_factors;				// intensity scale factors

	std::vector< double > band_sizes;										// band sizes for different boundaries

	std::vector< std::string > context_names;								// context names
	std::vector< int > context_levels;										// context levels
	std::vector< std::string >  context_feature_types;						// context feature types
	std::vector< double > context_scale_factors;							// context scale factors

	// methods
	bool Read(const char* file);
};


class JAT_singleres_detector
{
public:

	std::string root_folder;
	std::string name;
	int level;
	int scale;

	JAT_singleres_detector_info info;
	std::auto_ptr< Forest<MemoryAxisAlignedWeakLearner, CRStatisticsAggregator> > forest;
	FeatureSpaceArray<CompositeFeatureSpace> fss;

	JAT_singleres_detector() {}
	JAT_singleres_detector(const JAT_singleres_detector& copy) : root_folder(copy.root_folder), name(copy.name), level(copy.level), scale(copy.scale) {}
	JAT_singleres_detector& operator=(const JAT_singleres_detector& copy) { root_folder = copy.root_folder; name = copy.name; level = copy.level; scale = copy.scale; }

	// methods
	bool LoadDetectorInfo();
	bool LoadDetector();

	int GetLabelDimension() const;
	int GetTargetDimension() const;

private:

	std::string get_detector_folder() const;
	std::string get_detector_info_path() const;
};

/************************************************************************/
/* multi-resolution BD tester                                           */
/************************************************************************/

template <typename T>
class JAT_bd_multires_tester
{
public:

	// type definition
	typedef MemoryAxisAlignedWeakLearner W;
	typedef CRStatisticsAggregator S;
	typedef Tree<W, S> TreeType;
	typedef Forest<W, S> ForestType;

	// global masking
	void SetMask(const mxImage<unsigned char>* mask) { m_mask = mask;  }

	// set output images
	void SetOutputInfo(const MultiResolutionConfigFile& config, std::string label_prefix, std::string disp_prefix, std::string out_prefix, std::string format) 
	{
		m_config = &config;
		m_label_prefix = label_prefix;
		m_disp_prefix = disp_prefix;
		m_out_prefix = out_prefix;
		m_out_format = format;
	}

	// automatic contouring
	void Contour(
		const std::vector< mxImage<T> >& images,										// original images
		const std::vector< std::vector<JAT_singleres_detector> >& multires_detector,	// multires_detector, each resolution can have multiple single-resolution detectors
		TestMaskProvider& mask_provider													// provide updated mask region across resolutions
		);

private:

	// compute context maps
	void compute_context_maps(int res_idx, int det_idx, const vect3<double>& sp, const vect3<double>& ep, std::vector< mxImage<double> >& label_maps, std::vector< mxImage<double> >& disp_maps, const mxImage<unsigned char>* mask);

	// deform surface
	void deform_surface(int res_idx, int det_idx, const std::vector< mxImage<double> >& label_maps, const std::vector< mxImage<double> >& disp_maps);

	// get global boundary indices
	void get_global_bd_idxs(int res_idx, int det_idx, std::vector<int>& bd_idxs);

	// output intermediate map
	void intermediate_output(int res_idx, int det_idx, const std::vector< mxImage<double> >& label_maps, const std::vector< mxImage<double> >& disp_maps);

	// get resolution header
	void get_resolution_header(int res_idx, mxImage<T>& header);

	// get merged mask
	void compute_merged_mask(const std::vector< mxImage<unsigned char> >& bd_masks, const std::vector< vect3<int> >& bd_sps, const std::vector< vect3<int> >& bd_eps, const mxImage<T>& header, const vect3<double>& sp, const vect3<double>& ep, mxImage<unsigned char>& merged_mask);

	// get merged box
	void compute_merged_mask_box(const std::vector< vect3<int> >& bd_sps, const std::vector< vect3<int> >& bd_eps, vect3<int>& merged_sp, vect3<int>& merged_ep);

	/************************************************************************/
	/* private attributes                                                   */
	/************************************************************************/

	// preset mask pointer
	const mxImage<unsigned char>* m_mask;

	// output info
	const MultiResolutionConfigFile* m_config;
	std::string m_label_prefix;
	std::string m_disp_prefix;
	std::string m_out_prefix;
	std::string m_out_format;

	// private input variables
	const std::vector< mxImage<T> >* m_images;
	const std::vector< std::vector<JAT_singleres_detector> >* m_multires_detector;
	TestMaskProvider* m_mask_provider;
};

//////////////////////////////////////////////////////////////////////////

template <typename T>
void JAT_bd_multires_tester<T>::Contour(

	const std::vector< mxImage<T> >& images,											// original images
	const std::vector< std::vector<JAT_singleres_detector> >& multires_detector,		// multires_detector, each resolution can have multiple single-resolution detectors
	TestMaskProvider& mask_provider														// provide updated mask region across resolutions

	)
{
	// setup private input variables
	m_images = &images;
	m_multires_detector = &multires_detector;
	m_mask_provider = &mask_provider;

	// assertions
	assert_message(images.size() > 0, "empty image sizes");
	assert_message(multires_detector.size() > 0, "empty detector");

	// entire image domain
	vect3<double> sp, ep;
	for (int i = 0; i < 3; ++i)
	{
		sp[i] = 0;
		ep[i] = images[0].GetImageSize()[i] - 1;
	}
	mxImageUtils::Voxel2World(images[0], sp);
	mxImageUtils::Voxel2World(images[0], ep);

	// compute coarsest context maps
	int res_idx = 0, det_idx = 0;
	
	std::vector< mxImage<double> > label_maps, disp_maps;
	compute_context_maps(res_idx, det_idx, sp, ep, label_maps, disp_maps, m_mask);

	// deform surface
	deform_surface(res_idx, det_idx, label_maps, disp_maps);

	// output intermediate maps
	intermediate_output(res_idx, det_idx, label_maps, disp_maps);

	// contouring across resolutions
	for (int res_idx = 1; res_idx < multires_detector.size(); ++ res_idx)
	{
		mxImage<T> scale_header;
		get_resolution_header(res_idx, scale_header);

		// across detectors
		for (size_t det_idx = 0; det_idx < multires_detector[res_idx].size(); ++det_idx)
		{
			label_maps.clear();
			disp_maps.clear();

			const JAT_singleres_detector& det = multires_detector[res_idx][det_idx];
			assert_message(det.info.spacing == multires_detector[res_idx][0].info.spacing, "spacings not same for one resolution");

			// obtain boundary indexes of this detector
			std::vector<int> bd_idxs;
			get_global_bd_idxs(res_idx, det_idx, bd_idxs);

			// get update mask and bounding box for each boundary
			std::vector< mxImage<unsigned char> > bd_masks(bd_idxs.size());
			std::vector< vect3<int> > bd_sps(bd_idxs.size());
			std::vector< vect3<int> > bd_eps(bd_idxs.size());

			for (size_t idx = 0; idx < bd_idxs.size(); ++idx)
				mask_provider.GetUpdateMaskRegion(bd_idxs[idx], det.info.band_sizes[idx], scale_header, bd_masks[idx], bd_sps[idx], bd_eps[idx]);

			// compute box for updated region
			vect3<int> box_sp_voxel, box_ep_voxel;
			vect3<double> box_sp_world, box_ep_world;
			compute_merged_mask_box(bd_sps, bd_eps, box_sp_voxel, box_ep_voxel);
			mxImageUtils::Voxel2World(scale_header, box_sp_voxel, box_sp_world);
			mxImageUtils::Voxel2World(scale_header, box_ep_voxel, box_ep_world);

			// compute merged mask for updated region
			mxImage<unsigned char> merged_mask;
			compute_merged_mask(bd_masks, bd_sps, bd_eps, scale_header, box_sp_world, box_ep_world, merged_mask);

			if (m_mask)
			{
				vect3<unsigned int> merged_size = merged_mask.GetImageSize();

				vect3<int> voxel_sp, voxel_ep;
				for (int i = 0; i < 3; ++i)
				{
					voxel_sp[i] = 0;
					voxel_ep[i] = merged_size[i] - 1;
				}

				mxImageUtils::ImageMask(merged_mask, voxel_sp, voxel_ep, *m_mask);
			}

			// compute local context maps
			compute_context_maps(res_idx, det_idx, box_sp_world, box_ep_world, label_maps, disp_maps, &merged_mask);

			// deform surface based on context maps
			deform_surface(res_idx, det_idx, label_maps, disp_maps);

			// output intermediate
			intermediate_output(res_idx, det_idx, label_maps, disp_maps);
		}
	}
}

template <typename T>
void JAT_bd_multires_tester<T>::compute_merged_mask_box(const std::vector< vect3<int> >& bd_sps, const std::vector< vect3<int> >& bd_eps, vect3<int>& merged_sp, vect3<int>& merged_ep)
{
	assert_message(bd_sps.size() > 0, "empty bd_sps (function merge_masks)");
	assert_message(bd_sps.size() == bd_eps.size(), "not same number (function merge_masks)");

	// compute merged box
	merged_sp = bd_sps[0];
	merged_ep = bd_eps[0];

	for (size_t i = 1; i < bd_sps.size(); ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			if (bd_sps[i][j] < merged_sp[j])
				merged_sp[j] = bd_sps[i][j];

			if (bd_eps[i][j] > merged_ep[j])
				merged_ep[j] = bd_eps[i][j];
		}
	}
}

template <typename T>
void JAT_bd_multires_tester<T>::compute_merged_mask(const std::vector< mxImage<unsigned char> >& bd_masks, const std::vector< vect3<int> >& bd_sps, const std::vector< vect3<int> >& bd_eps, const mxImage<T>& header, const vect3<double>& sp, const vect3<double>& ep, mxImage<unsigned char>& merged_mask)
{
	// setup merged_mask
	vect3<int> merged_sp, merged_ep;
	mxImageUtils::World2Voxel(header, sp, merged_sp);
	mxImageUtils::World2Voxel(header, ep, merged_ep);

	mxImageUtils::GetCropHeader2(header, merged_sp, merged_ep, merged_mask);
	merged_mask.Allocate();
	merged_mask.Fill(0);

	// compute merged mask
	for (size_t i = 0; i < bd_masks.size(); ++i)
	{
		vect3<unsigned int> bd_size = bd_masks[i].GetImageSize();

		#pragma omp parallel for
		for (int z = 0; z < static_cast<int>(bd_size[2]); ++z)
		{
			for (int y = 0; y < static_cast<int>(bd_size[1]); ++y)
			{
				for (int x = 0; x < static_cast<int>(bd_size[0]); ++x)
				{
					if (bd_masks[i](x, y, z))
					{
						vect3<int> out_pos(x, y, z);
						out_pos = out_pos + bd_sps[i] - merged_sp;
						merged_mask(out_pos[0], out_pos[1], out_pos[2]) = 255;
					}
				}
			}
		}
	}
}

// compute context maps
template <typename T>
void JAT_bd_multires_tester<T>::compute_context_maps(int res_idx, int det_idx, const vect3<double>& sp, const vect3<double>& ep, std::vector< mxImage<double> >& label_maps, std::vector< mxImage<double> >& disp_maps, const mxImage<unsigned char>* mask)
{
	boost::timer::cpu_timer timer;

	const JAT_singleres_detector& detector = (*m_multires_detector)[res_idx][det_idx];

	JAT_bd_tester aux;
	if (mask != NULL)
		aux.SetMask(mask, true);

	aux.SetDetector(detector.root_folder.c_str(), detector.name.c_str(), detector.level);
	aux.ComputeSingleResolutionMaps(*m_images, detector.scale, sp, ep, &label_maps, &disp_maps);

	//////////////////////////////////////////////////////////////////////////
	// Experimental Steps (Remove after the experiments)
	//for (size_t i = 0; i < label_maps.size(); ++i) {
	//	mxImageUtils::Prob2Disp(label_maps[i], 0.5, &(disp_maps[i * 3]), mask);
	//}
	//////////////////////////////////////////////////////////////////////////


	double smooth_sigma_mm = m_config->singleres_predict_config[res_idx].smooth_sigma_mm;
	double smooth_sigma_voxel = smooth_sigma_mm / detector.info.spacing[0];

	if (smooth_sigma_voxel > 0)
	{
		mxImage<double> tmp_image;

		for (size_t i = 0; i < label_maps.size(); ++i)
		{
			mxImageUtils::GaussianSmooth(label_maps[i], tmp_image, smooth_sigma_voxel);
			mxImageUtils::Copy(tmp_image, label_maps[i]);
		}

		for (size_t i = 0; i < disp_maps.size(); ++i)
		{
			mxImageUtils::GaussianSmooth(disp_maps[i], tmp_image, smooth_sigma_voxel);
			mxImageUtils::Copy(tmp_image, disp_maps[i]);
		}
	}

	std::cout << "R" << detector.scale << " " << detector.name << " Predict: " << timer.format();
}

// deform surface
template <typename T>
void JAT_bd_multires_tester<T>::deform_surface(int res_idx, int det_idx, const std::vector< mxImage<double> >& label_maps, const std::vector< mxImage<double> >& disp_maps)
{
	const JAT_singleres_detector& det = (*m_multires_detector)[res_idx][det_idx];

	std::vector< int > bd_idxs;
	get_global_bd_idxs(res_idx, det_idx, bd_idxs);
	assert_message(bd_idxs.size() == label_maps.size(), "Boundary num must be equal to label map num");
	assert_message(bd_idxs.size() * 3 == disp_maps.size(), "Boundary num must be equal to 1/3 displacement map num");

	for (size_t i = 0; i < bd_idxs.size(); ++i)
	{
		boost::timer::cpu_timer timer;
		m_mask_provider->Notify(res_idx, bd_idxs[i], &(label_maps[i]), &(disp_maps[i * 3]));

		std::cout << "R" << det.scale << " " << det.info.bdids[i] << " Deform: " << timer.format();
	}
}

// get global indices
template <typename T>
void JAT_bd_multires_tester<T>::get_global_bd_idxs(int resIdx, int detIdx, std::vector<int>& bd_idxs)
{
	const JAT_singleres_detector_info& current = (*m_multires_detector)[resIdx][detIdx].info;
	const JAT_singleres_detector_info& coarse = (*m_multires_detector)[0][0].info;

	bd_idxs.resize(current.bdids.size());

	// loop over current boundaries
	for (size_t k = 0; k < current.bdids.size(); ++k)
	{
		// find the index for each boundary
		int idx = -1;
		for (size_t kk = 0; kk < coarse.bdids.size(); ++kk)
		{
			if (current.bdids[k] == coarse.bdids[kk])
			{
				idx = static_cast<int>(kk);
				break;
			}
		}
		assert_message(idx >= 0 && idx < coarse.bdids.size(), (current.bdids[k] + " not found").c_str());
		bd_idxs[k] = idx;
	}
}

// output intermediate results
template <typename T>
void JAT_bd_multires_tester<T>::intermediate_output(int res_idx, int det_idx, const std::vector< mxImage<double> >& label_maps, const std::vector< mxImage<double> >& disp_maps)
{
	if (m_config == NULL || m_config->singleres_predict_config[res_idx].output_map == 0)
		return;

	std::vector< int > bd_idxs;
	get_global_bd_idxs(res_idx, det_idx, bd_idxs);

	int bd_num = static_cast<int>(bd_idxs.size());

	// output label maps
	for (int i = 0; i < bd_num; ++i)
	{
		std::string path = m_label_prefix + "_" + m_config->bdids[ bd_idxs[i] ] + "_R" + stringUtils::num2str(res_idx) + "." + m_out_format;
		if (!ImageHelper::WriteImage<double, double>(label_maps[i], path.c_str())) {
			std::cerr << "fails to write label map " << m_config->bdids[ bd_idxs[i] ] << std::endl; exit(-1);
		}
	}

	// output displacement maps
	for (int i = 0; i < bd_num; ++i)
	{
		// output displacement norm image
		vect3<unsigned int> image_size = disp_maps[i * 3].GetImageSize();

		mxImage<double> dist_image;
		dist_image.CopyImageInfo(disp_maps[i * 3]);
		dist_image.SetImageSize(image_size);

		#pragma omp parallel for
		for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
		{
			for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
			{
				for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
				{
					double dx = disp_maps[i * 3](x, y, z);
					double dy = disp_maps[i * 3 + 1](x, y, z);
					double dz = disp_maps[i * 3 + 2](x, y, z);
					dist_image(x, y, z) = sqrt(dx * dx + dy * dy + dz * dz);
				}
			}
		}

		std::string output_path = m_disp_prefix + "_" + m_config->bdids[ bd_idxs[i] ] + "_R" + stringUtils::num2str(res_idx) + "_norm" + "." + m_out_format;
		if (!ImageHelper::WriteImage<double, double>(dist_image, output_path.c_str())) {
			std::cerr << "fails to write image to " << output_path << std::endl; exit(-1);
		}

		output_path = m_disp_prefix + "_" + m_config->bdids[bd_idxs[i]] + "_R" + stringUtils::num2str(res_idx) + "_";
		for (int k = 0; k < 3; ++k)
			if (!ImageHelper::WriteImage<double, double>(disp_maps[i * 3 + k], (output_path + stringUtils::num2str(k) + "." + m_out_format).c_str())) {
				std::cerr << "fails to write image to " << output_path << std::endl; exit(-1);
			}
	}

	// output deformed surfaces
	for (int i = 0; i < bd_num; ++i)
	{
		std::string path = m_out_prefix + "_" + m_config->bdids[ bd_idxs[i] ] + "_R" + stringUtils::num2str(res_idx) + ".vtk";
		const Surface* surf = m_mask_provider->GetBoundarySurface( bd_idxs[i] );

		if (!SurfaceUtils::SaveSurface(*surf, path.c_str())) {
			std::cerr << "fails to write surface to " << path << std::endl; exit(-1);
		}
	}
}

// get resolution header
template <typename T>
void JAT_bd_multires_tester<T>::get_resolution_header(int res_idx, mxImage<T>& header)
{
	mxImageUtils::GetResampleHeader((*m_images)[0], (*m_multires_detector)[res_idx][0].info.spacing, header);
}

//////////////////////////////////////////////////////////////////////////
// TestMaskProvider implementation

template <typename T>
void TestMaskProvider::GetUpdateMaskRegion(int bd_idx, double band_world, const mxImage<T>& ref_header, mxImage<unsigned char>& out_mask, vect3<int>& sp, vect3<int>& ep) const
{
	assert_message(bd_idx >= 0 && bd_idx < m_num_surfs, "bd idx out of bound");

	// calculate voxel band size
	int band_size = static_cast<int>(band_world / ref_header.GetSpacing()[0] + 0.5);
	vect3<int> pad_size(band_size + 1, band_size + 1, band_size + 1);

	// carve surface into image & compute bounding box on ref_header
	unsigned char outside_val = 0, inside_val = 255;
	SurfaceUtils::BoundingBox(m_surfs[bd_idx], ref_header, sp, ep);
	for (int i = 0; i < 3; ++i) {
		sp[i] -= pad_size[i];
		ep[i] += pad_size[i];
	}

	mxImage<T> cropped_refImage;
	mxImageUtils::GetCropHeader2(ref_header, sp, ep, cropped_refImage);
	SurfaceUtils::CarveSurface(m_surfs[bd_idx], cropped_refImage, outside_val, inside_val, out_mask);

	// compute narrow-band region
	mxImage<unsigned char> dilated_mask;
	mxImageUtils::Copy(out_mask, dilated_mask);
 
	for (int i = 0; i < band_size; ++i)
		mxImageUtils::BinaryDilate(dilated_mask, inside_val);
	for (int i = 0; i < band_size; ++i)
		mxImageUtils::BinaryErode(out_mask, inside_val);

	// compute band region by difference
	vect3<unsigned int> image_size = dilated_mask.GetImageSize();

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
			{
				if (dilated_mask(x, y, z) > 0 && out_mask(x, y, z) == 0)
					out_mask(x, y, z) = 255;
				else
					out_mask(x, y, z) = 0;
			}
		}
	}
}

void TestMaskProvider::Notify(int res_idx, int bd_idx, const mxImage<double>* label_maps, const mxImage<double>* disp_maps)
{
	assert_message(bd_idx >= 0 && bd_idx < m_num_surfs, "bd idx out of bound");
	assert_message(res_idx >= 0 && res_idx < m_deform_configs.size(), "res idx out of bound");

	Surface& surf = m_surfs[bd_idx];
	const BDModel_config& config = m_deform_configs[res_idx][bd_idx];

	BDModel::deform(*label_maps, disp_maps, config, surf);
}

//////////////////////////////////////////////////////////////////////////
// JAT_singleres_detector_info implementation

bool JAT_singleres_detector_info::Read(const char* file)
{
	boost::property_tree::ptree pt;
	try {
		boost::property_tree::ini_parser::read_ini(file, pt);
	}
	catch (boost::property_tree::ini_parser_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	try {

		// get general information
		spacing = stringUtils::ParseVect3<double>(pt.get<std::string>("Info.Resolution"));
		level = pt.get<int>("Info.Level");
		modality_num = pt.get<int>("Info.ModalityNum");
		bdids = stringUtils::Split(pt.get<std::string>("Info.BDIDs"), ' ');
		discard_background = pt.get<int>("Info.DiscardBK") == 0 ? false : true;

		// get intensity scale factors
		intensity_scale_factors.resize(modality_num);
		for (int i = 0; i < modality_num; ++i)
		{
			std::string section_name = std::string("Intensity") + stringUtils::num2str(i);
			int num_scales = pt.get<int>(section_name + ".NumScales");
			intensity_scale_factors[i].resize(num_scales);

			for (int j = 0; j < num_scales; ++j) {
				std::string entry_name = section_name + ".Scale" + stringUtils::num2str(j) + "_ScaleFactor";
				intensity_scale_factors[i][j] = pt.get<double>(entry_name);
			}
		}

		// get sampling band sizes
		band_sizes.resize(bdids.size());
		for (size_t i = 0; i < band_sizes.size(); ++i)
		{
			std::string section_name = "Sampling-" + bdids[i];

			std::string entry_name1 = section_name + ".ROIBand";
			std::string entry_name2_1 = section_name + ".SigmaIn";
			std::string entry_name2_2 = section_name + ".SigmaOut";

			boost::optional<double> value = pt.get_optional<double>(entry_name1);
			if (value)
				band_sizes[i] = value.get();
			else {
				boost::optional<double> value2 = pt.get_optional<double>(entry_name2_1);
				boost::optional<double> value3 = pt.get_optional<double>(entry_name2_2);
				if (value2 && value3)
					band_sizes[i] = std::max(value2.get(), value3.get()) * 2;
				else
					err_message(("fail to read band size from detector (" + std::string(file) + ")").c_str());
			}
		}

		// get context information
		if (level > 0)
		{
			context_names = stringUtils::Split(pt.get<std::string>("Context.Names"), ' ');
			stringUtils::str2vec<int>(pt.get<std::string>("Context.Levels"), context_levels, ' ');
			context_feature_types = stringUtils::Split(pt.get<std::string>("Context.Features"), ' ');

			assert_message(context_names.size() == context_levels.size(), "inconsistent number of context detectors");
			assert_message(context_names.size() == context_feature_types.size(), "inconsistent number of context detectors");

			int num_scales = pt.get<int>("Context.NumScales");
			context_scale_factors.resize(num_scales);

			for (int j = 0; j < num_scales; ++j) {
				std::string entry_name = "Context.Scale" + stringUtils::num2str(j) + "_ScaleFactor";
				context_scale_factors[j] = pt.get<double>(entry_name);
			}
		}
	}
	catch (boost::property_tree::ptree_error& error)
	{
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}


//////////////////////////////////////////////////////////////////////////
// JAT_singlres_detector implementation

std::string JAT_singleres_detector::get_detector_folder() const
{
	std::string det_folder;
	if (level == 0)
		det_folder = root_folder + stringUtils::Slash() + name;
	else
		det_folder = root_folder + stringUtils::Slash() + name + "_C" + stringUtils::num2str(level);

	return det_folder;
}

std::string JAT_singleres_detector::get_detector_info_path() const
{
	std::string det_folder = get_detector_folder();
	return det_folder + stringUtils::Slash() + "info_R" + stringUtils::num2str(scale) + ".ini";
}

bool JAT_singleres_detector::LoadDetectorInfo()
{
	std::string path = get_detector_info_path();
	if (!info.Read(path.c_str()))
		return false;
	return true;
}

bool JAT_singleres_detector::LoadDetector()
{
	if (!LoadDetectorInfo())
		return false;

	forest = DetectorIO::ReadDetector< Forest<MemoryAxisAlignedWeakLearner, CRStatisticsAggregator>, CompositeFeatureSpace >(get_detector_folder().c_str(), scale, fss);
	if (fss.GetFeatureSpaces().size() == 0 || forest->GetTreeNumber() == 0)
		err_message("empty tree and feature space");

	return true;
}

int JAT_singleres_detector::GetLabelDimension() const
{
	assert_message(forest->GetTreeNumber() != 0, "load detector first");
	const CRStatisticsAggregator& s = forest->GetTree(0)->GetAnyLeafStatisticsAggregator();
	return s.GetClassNumber();
}

int JAT_singleres_detector::GetTargetDimension() const
{
	assert_message(forest->GetTreeNumber() != 0, "load detector first");
	const CRStatisticsAggregator& s = forest->GetTree(0)->GetAnyLeafStatisticsAggregator();
	return s.GetTargetDimension();
}


//////////////////////////////////////////////////////////////////////////
// MultiResolutionConfigFile

bool MultiResolutionConfigFile::Read(const char* file)
{
	boost::property_tree::ptree pt;
	try
	{
		boost::property_tree::ini_parser::read_ini(file, pt);
	}
	catch (boost::property_tree::ini_parser_error& error)
	{
		std::cerr << error.what() << std::endl;
		return false;
	}

	try {

		bdids = stringUtils::Split(pt.get<std::string>("General.BDIDs"), ' ');
		surf_paths.resize(bdids.size());

		for (size_t i = 0; i < bdids.size(); ++i)
			surf_paths[i] = pt.get<std::string>("General." + bdids[i]);

		num_resolutions = pt.get<int>("General.Resolution");
		singleres_predict_config.resize(num_resolutions);
		singleres_deform_config.resize(num_resolutions);

		for (int i = 0; i < num_resolutions; ++i)
		{
			std::string section_name = std::string("Resolution") + stringUtils::num2str(i);
			singleres_predict_config[i].names = stringUtils::Split(pt.get<std::string>(section_name + ".name"), ' ');
			stringUtils::str2vec<int>(pt.get<std::string>(section_name + ".level"), singleres_predict_config[i].levels, ' ');
			singleres_predict_config[i].scale = pt.get<int>(section_name + ".scale");

			// read in kernel size for Gaussian smoothing (prediction maps)
			boost::optional<double> sigma_value = pt.get_optional<double>(section_name + ".smooth_sigma_mm");
			if (sigma_value)
				singleres_predict_config[i].smooth_sigma_mm = sigma_value.get();
			else
				singleres_predict_config[i].smooth_sigma_mm = -1;

			singleres_predict_config[i].output_map = pt.get<int>(section_name + ".output");

			assert_message(singleres_predict_config[i].names.size() == singleres_predict_config[i].levels.size(), "not same (names and levels)");

			singleres_deform_config[i].resize(bdids.size());
			for (size_t j = 0; j < bdids.size(); ++j)
			{
				std::string deform_path = boost::filesystem::path(file).parent_path().string();
				deform_path += stringUtils::Slash() + pt.get<std::string>(section_name + "." + bdids[j] + "_deform");

				if (!singleres_deform_config[i][j].parse(deform_path.c_str()))
				{
					std::cerr << "fails to parse deform ini: " << deform_path << std::endl;
					return false;
				}
			}
		}

	}
	catch (boost::property_tree::ptree_error& error)
	{
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}

bool MultiResolutionConfigFile::WriteDefault(const char* file)
{
	FILE* fp = fopen(file, "w");

	fprintf(fp, "[General]\n");
	fprintf(fp, "BDIDs=pro bla rec\n");
	fprintf(fp, "pro=C:\\data\\PlanCT\\mean_masks\\pro_ref.vtk\n");
	fprintf(fp, "bla=C:\\data\\PlanCT\\mean_masks\\bla_ref.vtk\n");
	fprintf(fp, "rec=C:\\data\\PlanCT\\mean_masks\\rec_ref.vtk\n");
	fprintf(fp, "Resolution=4\n");
	fprintf(fp, "\n");
	fprintf(fp, "[Resolution0]\n");
	fprintf(fp, "name=pelvic\n");
	fprintf(fp, "level=2\n");
	fprintf(fp, "scale=4\n");
	fprintf(fp, "output=1\n");
	fprintf(fp, "pro_deform=bd.ini\n");
	fprintf(fp, "bla_deform=bd.ini\n");
	fprintf(fp, "rec_deform=bd_ini\n");
	fprintf(fp, "\n");
	fprintf(fp, "[Resolution1]\n");
	fprintf(fp, "name=pelvic\n");
	fprintf(fp, "level=2\n");
	fprintf(fp, "scale=3\n");
	fprintf(fp, "output=1\n");
	fprintf(fp, "pro_deform=bd.ini\n");
	fprintf(fp, "bla_deform=bd.ini\n");
	fprintf(fp, "rec_deform=bd_ini\n");
	fprintf(fp, "\n");
	fprintf(fp, "[Resolution2]\n");
	fprintf(fp, "name=pro bla rec\n");
	fprintf(fp, "level=2 2 2\n");
	fprintf(fp, "scale=2\n");
	fprintf(fp, "output=1\n");
	fprintf(fp, "pro_deform=bd.ini\n");
	fprintf(fp, "bla_deform=bd.ini\n");
	fprintf(fp, "rec_deform=bd_ini\n");
	fprintf(fp, "\n");
	fprintf(fp, "[Resolution3]\n");
	fprintf(fp, "name=pro bla rec\n");
	fprintf(fp, "level=2 2 2\n");
	fprintf(fp, "scale=1\n");
	fprintf(fp, "output=1\n");
	fprintf(fp, "pro_deform=bd.ini\n");
	fprintf(fp, "bla_deform=bd.ini\n");
	fprintf(fp, "rec_deform=bd_ini\n");
	fprintf(fp, "\n");
	fclose(fp);

	return true;
}


} } }


#endif