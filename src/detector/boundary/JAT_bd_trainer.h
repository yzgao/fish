//
//  JAT_bd_trainer.h
//  FISH
//
//  Created by Yaozong Gao on 1/8/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __JAT_bd_trainer_h__
#define __JAT_bd_trainer_h__


#include "forest/context/CRTrainingContext.h"
#include "forest/stat/CRStatisticsAggregator.h"
#include "forest/ForestTrainer.h"
#include "forest/stat/HistogramStatisticsAggregator.h"
#include "forest/context/ClassificationExhaustSearchTrainingContext.h"

#include "common/mxImageUtils.h"
#include "common/mathUtils.h"
#include "feature/CompositeFeatureSpace.h"

#include "detector/DetectorIO.h"
#include "detector/boundary/JAT_bd_ini.h"
#include "detector/boundary/JAT_bd_struct.h"
#include "detector/boundary/JAT_bd_tester.h"

#include "extern/NearestPointIndexer.h"


namespace BRIC { namespace IDEA { namespace FISH {


// training module for boundary joint classification and regression
template <typename T>
class JAT_bd_trainer
{
public:

	JAT_bd_struct m_annot;
	JAT_bd_ini m_ini;

public:

	typedef MemoryAxisAlignedWeakLearner W;
	typedef CRStatisticsAggregator S;
	typedef Tree<W, S> TreeType;
	typedef Forest<W, S> ForestType;

public:

	bool LoadINI(const char* ini_path);
	std::auto_ptr<CompositeFeatureSpace> CreateFeatureSpace(Random& random);
	std::auto_ptr<TreeType> TrainTree(Random& random, CompositeFeatureSpace* fs);

	std::string GetDetectorFolder(std::string name, int level) const;
	void CreateOutputFolder() const;
	bool SaveTree(const Tree<W, S>* tree, const CompositeFeatureSpace* fs) const;
	bool SaveDetectionInfo() const;
	int GetTreeNumber() const;

	const JAT_bd_ini& GetINI();
	const JAT_bd_struct& GetAnnot();

private:

	Haar3DFeatureSpace* create_haar3d_feature_space(const std::vector< vect3<unsigned int> >& filter_sizes, const vect3<unsigned int>& patch_size);
	CompositeFeatureSpace* create_appearance_feature_space(Random& random);
	CompositeFeatureSpace* create_autocontext_feature_space(Random& random);

	int read_output_dimension(std::string name, int level, JAT_context_feature_type type);


	/********************** main work flow *****************************/

	/* read all-modality images from one subject */
	void read_images_for_one_subject(
		int subjectIdx,													
		std::vector< mxImage<T> >& intensity_images								// original images of all modalities
	);

	/* read mask images of all boundaries, and extract boundary points from all boundaries */
	void read_masks_extract_boundaries_for_one_subject(
		int subjectIdx,													
		const std::vector<int>& bd_map,											// map boundary index in INI to that in ANNOT
		std::vector< mxImage<unsigned char> >& mask_images,						// cropped mask images of all boundaries
		std::vector< NearestPointIndexer<double> >& boundaries					// boundary points in the world space
	);

	/* take training points based on mask images, convert them into the same coordinate system of intensity images */
	void draw_training_points_for_one_subject(
		const std::vector< mxImage<T> >& intensity_images,						// original intensity images of all modalities
		const std::vector< mxImage<unsigned char> >& mask_images,				// cropped mask images of all boundaries
		Random& random,					
		std::vector< vect3<double> >& sample_pts								// sample points in the world space
	);

	/* extract 3D displacements to all boundaries and labels of training voxels */
	void extract_targets_and_labels(
		const std::vector< mxImage<unsigned char> >& mask_images,				// cropped mask images
		const std::vector< NearestPointIndexer<double> >& boundaries,			// boundary points in world space
		const std::vector< vect3<double> >& sample_pts,							// sample points in the world space
		std::vector<double>& target_vector,										// 3D displacement in the world space
		std::vector<int>& label_vector
	);

	/** setup intensity images at different scales */
	void setup_scale_intensity_images(
		const std::vector< mxImage<T> >& intensity_images,						// original intensity images
		const std::vector< vect3<double> >& sample_pts,							// sample points in the world space
		std::vector< mxImage<double> >& input_images							// input images of different scales
	);

	/** compute necessary context image region */
	void compute_context_image_box(
		const std::vector< mxImage<T> >& intensity_images,						// original intensity images
		const std::vector< vect3<double> >& sample_pts,							// sample points in the world space
		vect3<double>& sp,														// start point of bounding box in current resolution
		vect3<double>& ep														// end point of bounding box in current resolution
	);

	/** setup context images at different scales */
	void setup_scale_context_images(
		const std::vector< mxImage<T> >& intensity_images,						// original intensity images
		const std::vector< vect3<double> >& sample_pts,							// sample points in the world space
		std::vector< mxImage<double> >& input_images							// input images of different scales
	);

	/* extract 3D Haar-like features from all-modality images and context maps */
	void extract_features(
		const std::vector< mxImage<T> >& intensity_images,						// original intensity images
		const std::vector< vect3<double> >& sample_pts,							// sample points in the world space
		const CompositeFeatureSpace* fs, 
		std::vector<double>& data_vector
	);

	/* fill training data into MemoryDataCollection */
	void fill_train_data(
		MemoryDataCollection& train_data,
		Random& random,
		const CompositeFeatureSpace* fs
	);

	/** auxiliary sampling function */
	void uniform_sampling(const std::vector< mxImage<T> >& intensity_images, const std::vector< mxImage<unsigned char> >& mask_images, int bd_idx, Random& random, std::vector< vect3<double> >& pts);
	void mask_uniform_sampling(const std::vector< mxImage<T> >& intensity_images, const std::vector< mxImage<unsigned char> >& mask_images, int bd_idx, Random& random, std::vector< vect3<double> >& pts);
	void slice_mask_uniform_sampling(const std::vector< mxImage<T> >& intensity_images, const std::vector< mxImage<unsigned char> >& mask_images, int bd_idx, Random& random, std::vector< vect3<double> >& pts);
	void surface_gaussian_sampling(const std::vector< mxImage<T> >& intensity_images, const std::vector< mxImage<unsigned char> >& mask_images, int bd_idx, Random& random, std::vector< vect3<double> >& pts);

	/******************** debug tools ***********************/
	void _save_sample_map(const mxImage<T>& resampled_image, const std::vector< vect3<double> >& sample_voxels, const char* outmap_path);
	void _save_label_map(const mxImage<T>& resampled_image, const std::vector< vect3<double> >& sample_voxels, const int* label, const char* output_path);
	void _save_dist_map(const mxImage<T>& resampled_image, const std::vector< vect3<double> >& sample_voxels, const double* targets, const char* output_folder);
};


//////////////////////////////////////////////////////////////////////////
// implementations

template <typename T>
bool JAT_bd_trainer<T>::LoadINI(const char* ini_path)
{
	if (!m_ini.parse(ini_path))
	{
		std::cerr << "fail to load INI from " << ini_path << std::endl;
		return false;
	}

	if (!m_annot.parse(m_ini.general.annot_path.c_str()))
	{
		std::cerr << "fail to load ANNOT from " << m_ini.general.annot_path << std::endl;
		return false;
	}

	assert_message(m_ini.general.modality_num == m_annot.modality_num, "modality number mismatch between INI and ANNOT");
	return true;
}

template <typename T>
Haar3DFeatureSpace* JAT_bd_trainer<T>::create_haar3d_feature_space(const std::vector< vect3<unsigned int> >& filter_sizes, const vect3<unsigned int>& patch_size)
{
	Haar3DFeatureSpace* fs = new Haar3DFeatureSpace;

	for (size_t i = 0; i < filter_sizes.size(); ++i)
		fs->AddFilterSize(filter_sizes[i]);
	fs->SetPatchSize(patch_size);

	return fs;
}

template <typename T>
CompositeFeatureSpace* JAT_bd_trainer<T>::create_appearance_feature_space(Random& random)
{
	CompositeFeatureSpace* cfs = new CompositeFeatureSpace;
	std::vector<double> weights;

	// create feature spaces for each modality
	for (int i = 0; i < m_annot.modality_num; ++i)
	{
		double scale_weight = m_ini.intensity[i].weight / m_ini.intensity[i].scales.size();

		// create feature spaces for different scales
		for (size_t j = 0; j < m_ini.intensity[i].scales.size(); ++j)
		{
			const std::vector< vect3<unsigned int> >& filter_sizes = m_ini.intensity[i].scales[j].filter_sizes;
			const vect3<unsigned int>& patch_size = m_ini.intensity[i].scales[j].patch_size;

			// create a feature space for this scale and modality
			Haar3DFeatureSpace* fs = create_haar3d_feature_space(filter_sizes, patch_size);
			cfs->AddFeatureSpace(std::auto_ptr<IFeatureSpace>(fs));

			// add its weight
			weights.push_back(scale_weight);
		}
	}

	// length of weight vector should be the same with number of feature spaces
	assert_message(weights.size() == cfs->GetNumberFeatureSpaces(), "number mismatched between weights and feature spaces");

	// assign weights for all modalities
	cfs->SetWeights(&weights[0], weights.size());

	// randomize feature spaces
	std::vector<unsigned int> num_array;
	cfs->DistrubuteFeatures(m_ini.forest_params.treeParameters.numOfRandomWeakLearners, num_array);

	int feature_space_num = cfs->GetNumberFeatureSpaces();
	for (int i = 0; i < feature_space_num; ++i)
	{
		Haar3DFeatureSpace* fs_ptr = static_cast<Haar3DFeatureSpace*>(cfs->GetFeatureSpace(i));
		fs_ptr->RandomizeFeatureSpace_twoblocks(random, num_array[i]);
		//fs_ptr->RandomizeFeatureSpace_radiation(random, num_array[i]);
	}

	return cfs;
}

template <typename T>
int JAT_bd_trainer<T>::read_output_dimension(std::string name, int level, JAT_context_feature_type type)
{
	std::string detector_folder = GetDetectorFolder(name, level);

	std::auto_ptr<TreeType> tree = DetectorIO::ReadAnyTree<TreeType>(detector_folder.c_str(), m_ini.general.resolution);
	const CRStatisticsAggregator& stat = tree->GetAnyLeafStatisticsAggregator();

	int class_number = stat.GetClassNumber();
	int target_dim = stat.GetTargetDimension();

	int ret_dim = 0;
	switch (type)
	{
	case BRIC::IDEA::FISH::JAT_REGRESSION_MAP: ret_dim = target_dim; break;
	case BRIC::IDEA::FISH::JAT_CLASSIFICATION_MAP: ret_dim = class_number - 1; break;
	case BRIC::IDEA::FISH::JAT_ALL_MAPS: ret_dim = target_dim + class_number - 1; break;
	default: err_message("unrecognized context feature type");
	}

	return ret_dim;
}

template <typename T>
CompositeFeatureSpace* JAT_bd_trainer<T>::create_autocontext_feature_space(Random& random)
{
	if (m_ini.context.names.size() == 0)
		err_message("no context images available according to INI");

	CompositeFeatureSpace* cfs = new CompositeFeatureSpace;
	std::vector<double> weights;

	// add intensity feature spaces of different scales and set weights
	for (int i = 0; i < m_annot.modality_num; ++i)
	{
		// compute weight for one scale one modality
		double scale_weight = m_ini.intensity[i].weight / m_ini.intensity[i].scales.size();

		for (size_t j = 0; j < m_ini.intensity[i].scales.size(); ++j)
		{
			const std::vector< vect3<unsigned int> >& filter_sizes = m_ini.intensity[i].scales[j].filter_sizes;
			const vect3<unsigned int>& patch_size = m_ini.intensity[i].scales[j].patch_size;

			// create feature space for this scale and this modality
			Haar3DFeatureSpace* fs = create_haar3d_feature_space(filter_sizes, patch_size);
			cfs->AddFeatureSpace(std::auto_ptr<IFeatureSpace>(fs));

			// add its weight
			weights.push_back(scale_weight);
		}
	}

	// add context feature spaces of different scales and set weights
	int num_context_maps = 0;
	for (int i = 0; i < m_ini.context.names.size(); ++i)
	{
		// dimension of output from this detector
		int output_dim = read_output_dimension(m_ini.context.names[i], m_ini.context.levels[i], m_ini.context.types[i]);

		// for each scale
		for (size_t j = 0; j < m_ini.context.scales.size(); ++j)
		{
			const std::vector< vect3<unsigned int> >& filter_sizes = m_ini.context.scales[j].filter_sizes;
			const vect3<unsigned int>& patch_size = m_ini.context.scales[j].patch_size;

			// create feature space for each dimension and each context map
			for (int dim = 0; dim < output_dim; ++dim)
			{
				Haar3DFeatureSpace* fs = create_haar3d_feature_space(filter_sizes, patch_size);
				cfs->AddFeatureSpace(std::auto_ptr<IFeatureSpace>(fs));

				++num_context_maps;
			}
		}
	}

	// setup context weights
	double context_weight = m_ini.context.weight / num_context_maps;
	for (int i = 0; i < num_context_maps; ++i)
		weights.push_back(context_weight);

	// length of weight vector should be the same with number of feature spaces
	assert_message(weights.size() == cfs->GetNumberFeatureSpaces(), "number mismatched between weights and feature spaces");

	// assign weights for all modalities
	cfs->SetWeights(&weights[0], weights.size());

	// randomize feature space
	std::vector<unsigned int> num_array;
	cfs->DistrubuteFeatures(m_ini.forest_params.treeParameters.numOfRandomWeakLearners, num_array);

	int feature_space_num = cfs->GetNumberFeatureSpaces();

	for (int i = 0; i < feature_space_num; ++i)
	{
		Haar3DFeatureSpace* fs_ref = static_cast<Haar3DFeatureSpace*>(cfs->GetFeatureSpace(i));
		fs_ref->RandomizeFeatureSpace_twoblocks(random, num_array[i]);
	}

	//// randomize intensity feature spaces
	//int num_intensity_fs = feature_space_num - num_context_maps;
	//for (int i = 0; i < num_intensity_fs; ++i)
	//{
	//	Haar3DFeatureSpace* fs_ref = static_cast<Haar3DFeatureSpace*>(cfs->GetFeatureSpace(i));
	//	fs_ref->RandomizeFeatureSpace_twoblocks(random, num_array[i]);
	//}
	//
	//// randomize context feature spaces
	//for (int i = 0; i < num_context_maps; ++i)
	//{
	//	Haar3DFeatureSpace* fs_ref = static_cast<Haar3DFeatureSpace*>(cfs->GetFeatureSpace(num_intensity_fs + i));
	//	fs_ref->RandomizeFeatureSpace_radiation(random, num_array[num_intensity_fs + i]);
	//}

	return cfs;
}

template <typename T>
std::auto_ptr<CompositeFeatureSpace> JAT_bd_trainer<T>::CreateFeatureSpace(Random& random)
{
	if (m_ini.general.level == 0)
	{
		CompositeFeatureSpace* fs = create_appearance_feature_space(random);
		return std::auto_ptr<CompositeFeatureSpace>(fs);
	}
	else
	{
		CompositeFeatureSpace* fs = create_autocontext_feature_space(random);
		return std::auto_ptr<CompositeFeatureSpace>(fs);
	}
}

template <typename T>
void JAT_bd_trainer<T>::read_images_for_one_subject(int subjectIdx, std::vector< mxImage<T> >& intensity_images)
{
	intensity_images.clear();
	intensity_images.resize(m_annot.modality_num);

	// read modality one by one
	for (int mod_idx = 0; mod_idx < m_annot.modality_num; ++mod_idx)
	{
		std::string image_path = m_annot.get_image_path(subjectIdx, mod_idx);
		std::cout << "   * " << image_path << std::endl;

		// load intensity image
		if (!ImageHelper::ReadImage<T, T>(image_path.c_str(), intensity_images[mod_idx]))
			err_message((std::string("fails to load image from ") + image_path).c_str());

		if (mod_idx != 0)
			assert_message(intensity_images[mod_idx].GetImageSize() == intensity_images[0].GetImageSize(), "all modalities should have the same image size");
	}
}

template <typename T>
void JAT_bd_trainer<T>::read_masks_extract_boundaries_for_one_subject(int subjectIdx, const std::vector<int>& bd_map, std::vector< mxImage<unsigned char> >& mask_images, std::vector< NearestPointIndexer<double> >& boundaries)
{
	int bd_num = static_cast<int>(m_ini.general.bd_ids.size());

	mask_images.clear();
	boundaries.clear();

	mask_images.resize(bd_num);
	boundaries.resize(bd_num);

	for (int i = 0; i < bd_num; ++i)
	{
		std::string mask_path = m_annot.get_mask_path(subjectIdx, bd_map[i]);

		std::cout << "   * " << mask_path << std::endl;

		if (!ImageHelper::ReadImage<T, unsigned char>(mask_path.c_str(), mask_images[i])) {
			std::cerr << "fails to read mask image from " << mask_path << std::endl; exit(-1);
		}

		// reduce mask image size for efficiency
		const vect3<int> pad_size(5, 5, 5);
		mxImageUtils::ShrinkMaskImage(mask_images[i], pad_size);

		// re-sample mask image for surface extraction
		vect3<double> surface_spacing = m_ini.sample_objects[i]->GetSurfaceSpacing();
		mxImage<unsigned char> resample_mask;
		mxImageUtils::Resample(mask_images[i], surface_spacing, resample_mask, IT_NEAREST_NEIGHBOR_INTERPOLATION);

		// extract background boundary voxels
		unsigned char binary_threshold = 128;
		std::vector< vect3<double> > bk_boundary_voxels;
		mxImageUtils::ExtractBoundaryPoints2(resample_mask, binary_threshold, bk_boundary_voxels);

		// convert coordinate system to the current one
		for (size_t j = 0; j < bk_boundary_voxels.size(); ++j)
			mxImageUtils::Voxel2World(resample_mask, bk_boundary_voxels[j]);

		boundaries[i].Initialize(bk_boundary_voxels);
	}
}

template <typename T>
void JAT_bd_trainer<T>::uniform_sampling(const std::vector< mxImage<T> >& intensity_images, const std::vector< mxImage<unsigned char> >& mask_images, int bd_idx, Random& random, std::vector< vect3<double> >& pts)
{
	mxImage<T> resample_header;
	mxImageUtils::GetResampleHeader(intensity_images[0], m_ini.general.spacing, resample_header);

	mxImage<unsigned char> target_mask;
	mxImageUtils::TransformMaskImage(resample_header, mask_images[bd_idx], target_mask);

	std::vector< vect3<double> > tmp_pts;
	m_ini.sample_objects[bd_idx]->Sample(target_mask, m_ini.sample_nums[bd_idx], random, tmp_pts);

	for (size_t i = 0; i < tmp_pts.size(); ++i)
	{
		vect3<double> pt = tmp_pts[i];

		vect3<int> voxel;
		mxImageUtils::World2Voxel(resample_header, pt, voxel);

		if (resample_header.PtInImage(voxel[0], voxel[1], voxel[2]))
			pts.push_back(pt);
	}
}

template <typename T>
void JAT_bd_trainer<T>::mask_uniform_sampling(const std::vector< mxImage<T> >& intensity_images, const std::vector< mxImage<unsigned char> >& mask_images, int bd_idx, Random& random, std::vector< vect3<double> >& pts)
{
	MaskUniformSampleObject* obj = static_cast<MaskUniformSampleObject*>(m_ini.sample_objects[bd_idx]);

	//////////////////////////////////////////////////////////////////////////
	// compute the bounding box for sampling

	// 1) get bounding box of organ (cropped original space)
	vect3<int> box_sp, box_ep;
	mxImageUtils::BoundingBox(mask_images[bd_idx], box_sp, box_ep);

	// 2) get padded bounding box (cropped original space)
	vect3<int> pad_voxels;
	for (int i = 0; i < 3; ++i)
		pad_voxels[i] = static_cast<int>(obj->m_pad_radius[i] / mask_images[bd_idx].GetSpacing()[i] + 0.5);

	for (int i = 0; i < 3; ++i)
	{
		box_sp[i] -= pad_voxels[i];
		box_ep[i] += pad_voxels[i];
	}

	// 3) get world coordinates of bounding box
	vect3<double> box_sp_world, box_ep_world;
	mxImageUtils::Voxel2World(mask_images[bd_idx], box_sp, box_sp_world);
	mxImageUtils::Voxel2World(mask_images[bd_idx], box_ep, box_ep_world);

	// 4) get voxel coordinates of bounding box in current space
	mxImage<T> header;
	mxImageUtils::GetResampleHeader(intensity_images[0], m_ini.general.spacing, header);
	
	mxImageUtils::World2Voxel(header, box_sp_world, box_sp);
	mxImageUtils::World2Voxel(header, box_ep_world, box_ep);

	for (int i = 0; i < 3; ++i)
	{
		if (box_sp[i] < 0)
			box_sp[i] = 0;
		if (box_ep[i] >= header.GetImageSize()[i])
			box_ep[i] = header.GetImageSize()[i] - 1;
	}

	// 5) get the header for the sampling region
	mxImage<T> sampling_header;
	mxImageUtils::GetCropHeader2(header, box_sp, box_ep, sampling_header);

	// 6) get body mask (with the same size as sampling_header)
	mxImage<unsigned char> body_mask;
	mxImageUtils::ExtractBodyFromCT<T,T>(intensity_images[0], body_mask, &sampling_header);

	// 7) get ROI mask (target organ mask)
	mxImage<unsigned char> roi_mask;
	mxImageUtils::TransformMaskImage(body_mask, mask_images[bd_idx], roi_mask);

	// 8) sampling
	std::vector< vect3<double> > tmp_pts;
	obj->SetROIMask(&roi_mask);
	obj->Sample(body_mask, m_ini.sample_nums[bd_idx], random, tmp_pts);
	obj->SetROIMask(NULL);

	// 9) assertion
	if (tmp_pts.size() == 0) {
		std::cerr << "fails to draw any sample from boundary " << m_ini.general.bd_ids[bd_idx] << std::endl; exit(-1);
	}

	// 10) append new sample points to the array
	size_t old_size = pts.size();
	pts.resize(old_size + tmp_pts.size());
	for (size_t i = 0; i < tmp_pts.size(); ++i)
		pts[old_size + i] = tmp_pts[i];
}

template <typename T>
void JAT_bd_trainer<T>::surface_gaussian_sampling(const std::vector< mxImage<T> >& intensity_images, const std::vector< mxImage<unsigned char> >& mask_images, int bd_idx, Random& random, std::vector< vect3<double> >& pts)
{
	std::vector< vect3<double> > tmp_pts;
	m_ini.sample_objects[bd_idx]->Sample(mask_images[bd_idx], m_ini.sample_nums[bd_idx], random, tmp_pts);

	mxImage<T> resample_header;
	mxImageUtils::GetResampleHeader(intensity_images[0], m_ini.general.spacing, resample_header);

	for (size_t i = 0; i < tmp_pts.size(); ++i)
	{
		vect3<double> pt = tmp_pts[i];

		vect3<int> voxel;
		mxImageUtils::World2Voxel(resample_header, pt, voxel);

		if (resample_header.PtInImage(voxel[0], voxel[1], voxel[2]))
			pts.push_back(pt);
	}
}

template <typename T>
void JAT_bd_trainer<T>::draw_training_points_for_one_subject(const std::vector< mxImage<T> >& intensity_images, const std::vector< mxImage<unsigned char> >& mask_images, Random& random, std::vector< vect3<double> >& sample_pts)
{
	assert_message(mask_images.size() == m_ini.general.bd_ids.size(), "number of mask images mismatch");
	sample_pts.clear();

	int bd_num = static_cast<int>(m_ini.general.bd_ids.size());
	for (int i = 0; i < bd_num; ++i)
	{
		if (m_ini.sample_objects[i]->GetSampleTypeName() == "Uniform")
			uniform_sampling(intensity_images, mask_images, i, random, sample_pts);
		else if (m_ini.sample_objects[i]->GetSampleTypeName() == "MaskUniform")
			mask_uniform_sampling(intensity_images, mask_images, i, random, sample_pts);
		else if (m_ini.sample_objects[i]->GetSampleTypeName() == "Gaussian" || m_ini.sample_objects[i]->GetSampleTypeName() == "SliceGaussian")
			surface_gaussian_sampling(intensity_images, mask_images, i, random, sample_pts);
		else
			err_message("unrecognized sampling type in function draw_training_points_for_one_subject");
	}
}

template <typename T>
void JAT_bd_trainer<T>::extract_targets_and_labels(const std::vector< mxImage<unsigned char> >& mask_images, const std::vector< NearestPointIndexer<double> >& boundaries, const std::vector< vect3<double> >& sample_pts, std::vector<double>& target_vector, std::vector<int>& label_vector)
{
	size_t target_oldsize = target_vector.size();
	size_t label_oldsize = label_vector.size();
	int bd_num = static_cast<int>(m_ini.general.bd_ids.size());

	// make sure target vector size is consistent with label vector size
	assert_message( target_oldsize == label_oldsize * bd_num * 3, "label and target vector inconsistent");

	// allocate the buffer
	target_vector.resize(target_oldsize + sample_pts.size() * bd_num * 3);
	label_vector.resize(label_oldsize + sample_pts.size());

	// loop over each sample pt
	for (size_t i = 0; i < sample_pts.size(); ++i)
	{
		size_t target_pos = target_oldsize + i * bd_num * 3;
		size_t label_pos = label_oldsize + i;
		vect3<double> test_pt = sample_pts[i];

		bool is_labeled = false;

		// extract 3D offset and label of each sample pt
		for (int j = 0; j < bd_num; ++j)		
		{
			// determine 3D offset vector to one boundary
			vect3<double> nearest_pt = boundaries[j].SearchNearestNeighbor(test_pt);
			vect3<double> displacement = nearest_pt - test_pt;
			for (int k = 0; k < 3; ++k)
				target_vector[target_pos + j * 3 + k] = displacement[k];

			// if not labeled
			if (!is_labeled)
			{
				vect3<int> mask_voxel;
				mxImageUtils::World2Voxel(mask_images[j], test_pt, mask_voxel);

				if (!mask_images[j].PtInImage(mask_voxel[0], mask_voxel[1], mask_voxel[2]))
					continue;

				if (mask_images[j](mask_voxel[0], mask_voxel[1], mask_voxel[2]) > 0)
				{
					label_vector[label_pos] = j + 1;
					is_labeled = true;
				}
			}
		}

		if (!is_labeled)
			label_vector[label_pos] = 0;
	}
}

template <typename T>
void JAT_bd_trainer<T>::setup_scale_intensity_images(const std::vector< mxImage<T> >& intensity_images, const std::vector< vect3<double> >& sample_pts, std::vector< mxImage<double> >& input_images)
{
	// compute total-scale intensity images
	int total_scale_intensity_images = 0;
	for (int i = 0; i < m_annot.modality_num; ++i)
		total_scale_intensity_images += static_cast<int>(m_ini.intensity[i].scales.size());

	// allocate intensity image buffer
	input_images.resize(total_scale_intensity_images);

	// re-sample and setup scale images
	int image_idx = 0;
	for (int i = 0; i < m_annot.modality_num; ++i)
	{
		const IntensityInfo& mod_info = m_ini.intensity[i];		// modality info

		for (size_t j = 0; j < mod_info.scales.size(); ++j)
		{
			const ScaleInfo& scale_info = mod_info.scales[j];	// scale info

			// re-sample image to the specified scale
			mxImage<T> resampled_image;
			vect3<double> scale_spacing = m_ini.general.spacing * scale_info.scale_factor;
			mxImageUtils::Resample(intensity_images[i], scale_spacing, resampled_image, IT_LINEAR_INTERPOLATION);

			// convert sample points from world to voxel
			std::vector< vect3<int> > sample_voxels(sample_pts.size());
			mxImageUtils::World2Voxel(resampled_image, sample_pts, sample_voxels);

			// compute bounding box
			vect3<int> box_sp, box_ep;
			points_bounding_box<int>(sample_voxels, box_sp, box_ep);

			// crop scale images
			mxImageUtils::Crop2_pad<T, double>(resampled_image, box_sp, box_ep, scale_info.patch_size, input_images[image_idx], 0.0);

			++image_idx;
		}
	}
	assert_message(image_idx == total_scale_intensity_images, "unexpected error on total_scale_intensity_images");
}

template <typename T>
void JAT_bd_trainer<T>::compute_context_image_box(const std::vector< mxImage<T> >& intensity_images, const std::vector< vect3<double> >& sample_pts, vect3<double>& box_sp, vect3<double>& box_ep)
{
	mxImage<double> current_image_header;
	mxImageUtils::GetResampleHeader(intensity_images[0], m_ini.general.spacing, current_image_header);

	vect3<int> sp, ep;
	for (size_t j = 0; j < m_ini.context.scales.size(); ++j)
	{
		vect3<double> scale_spacing = m_ini.general.spacing * m_ini.context.scales[j].scale_factor;
		vect3<unsigned int> patch_size = m_ini.context.scales[j].patch_size;

		// adjust scale
		mxImage<double> scale_image_header;
		mxImageUtils::GetResampleHeader(intensity_images[0], scale_spacing, scale_image_header);

		// convert to current scale
		std::vector< vect3<int> > sample_voxels(sample_pts.size());
		mxImageUtils::World2Voxel(scale_image_header, sample_pts, sample_voxels);

		// compute bounding box
		vect3<int> scale_sp, scale_ep;
		points_bounding_box<int>(sample_voxels, scale_sp, scale_ep);
		mxImageUtils::PadBox(scale_sp, scale_ep, patch_size);

		// convert back to current resolution
		vect3<double> mapped_sp, mapped_ep;
		mxImageUtils::Voxel2World(scale_image_header, scale_sp, mapped_sp);
		mxImageUtils::Voxel2World(scale_image_header, scale_ep, mapped_ep);

		mxImageUtils::World2Voxel(current_image_header, mapped_sp);
		mxImageUtils::World2Voxel(current_image_header, mapped_ep);

		// convert to integer coordinate
		vect3<int> round_sp, round_ep;
		for (int k = 0; k < 3; ++k)
		{
			round_sp[k] = static_cast<int>(mapped_sp[k]);
			round_ep[k] = static_cast<int>(mapped_ep[k]) + 1;
		}

		// update box_sp, box_ep
		if (j == 0)
		{
			sp = round_sp;
			ep = round_ep;
		}
		else {

			for (int k = 0; k < 3; ++k)
			{
				if (round_sp[k] < sp[k])
					sp[k] = round_sp[k];

				if (round_ep[k] > ep[k])
					ep[k] = round_ep[k];
			}

		}
	}

	mxImageUtils::Voxel2World(current_image_header, sp, box_sp);
	mxImageUtils::Voxel2World(current_image_header, ep, box_ep);
}

template <typename T>
void JAT_bd_trainer<T>::setup_scale_context_images(const std::vector< mxImage<T> >& intensity_images, const std::vector< vect3<double> >& sample_pts, std::vector< mxImage<double> >& input_images)
{
	std::auto_ptr < mxImage<unsigned char> > body_mask;
	if (m_ini.general.discard_background)
	{
		// if background is not interested (which is often the case)
		// save the mask in the coarsest resolution for memory efficiency
		body_mask.reset( new mxImage<unsigned char> );
		mxImageUtils::ExtractBodyFromCT<T,T>(intensity_images[0], *body_mask, NULL);
	}

	for (size_t i = 0; i < m_ini.context.names.size(); ++i)
	{
		std::cout << "   * Computing context maps from detector " << m_ini.context.names[i] << " (Level " << m_ini.context.levels[i] << ")" << std::endl;

		/** compute necessary bounding box for context image */
		vect3<double> box_sp, box_ep;
		compute_context_image_box(intensity_images, sample_pts, box_sp, box_ep);

		/** get context map in the specified region */
		JAT_bd_tester aux;
		aux.SetDetector(m_ini.general.root_folder.c_str(), m_ini.context.names[i].c_str(), m_ini.context.levels[i]);

		if (m_ini.general.discard_background)
			aux.SetMask(body_mask.get(), true);
		else
			aux.SetMask(NULL, false);
	
		std::vector< mxImage<double> > context_maps;
		if (m_ini.context.types[i] == JAT_CLASSIFICATION_MAP)
			aux.ComputeSingleResolutionMaps(intensity_images, m_ini.general.resolution, box_sp, box_ep, &context_maps, NULL);
		else if (m_ini.context.types[i] == JAT_REGRESSION_MAP)
			aux.ComputeSingleResolutionMaps(intensity_images, m_ini.general.resolution, box_sp, box_ep, NULL, &context_maps);
		else if (m_ini.context.types[i] == JAT_ALL_MAPS)
			aux.ComputeSingleResolutionMaps(intensity_images, m_ini.general.resolution, box_sp, box_ep, &context_maps, &context_maps);
		else
			err_message("unrecognized context feature type");

		/** get input context images in different scales */
		size_t old_size = input_images.size();
		input_images.resize(old_size + context_maps.size() * m_ini.context.scales.size());

		/** re-sample and crop context images */
		for (size_t j = 0; j < m_ini.context.scales.size(); ++j)
		{
			size_t start_idx = old_size + j * context_maps.size();
			vect3<double> scale_spacing = m_ini.general.spacing * m_ini.context.scales[j].scale_factor;

			// get scale image info
			mxImage<double> scale_image_header;
			mxImageUtils::GetResampleHeader(context_maps[0], scale_spacing, scale_image_header);
			
			// convert sample points from world to voxel
			std::vector< vect3<int> > sample_voxels;
			mxImageUtils::World2Voxel(scale_image_header, sample_pts, sample_voxels);

			// compute bounding box
			vect3<int> scale_sp, scale_ep;
			points_bounding_box<int>(sample_voxels, scale_sp, scale_ep);
			mxImageUtils::PadBox(scale_sp, scale_ep, m_ini.context.scales[j].patch_size);

			for (size_t k = 0; k < context_maps.size(); ++k)
			{
				mxImage<double> tmp_image;
				mxImageUtils::Resample(context_maps[k], scale_spacing, tmp_image, IT_LINEAR_INTERPOLATION);
				mxImageUtils::Crop2<double,double>(tmp_image, scale_sp, scale_ep, input_images[start_idx + k], 0.0);
			}
		}
	}

	/* debug */
	//int num_intensity_images = 0;
	//for (int i = 0; i < m_ini.general.modality_num; ++i)
	//	num_intensity_images += m_ini.intensity[i].scales.size();

	//std::string debug_outfolder = "C:\\Users\\yzgao\\Desktop\\real\\";
	//for (size_t i = num_intensity_images; i < input_images.size(); ++i)
	//	ImageHelper::WriteImage<double, double>(input_images[i], (debug_outfolder + "context" + stringUtils::num2str(i) + ".mha").c_str());
}

template <typename T>
void JAT_bd_trainer<T>::extract_features(const std::vector< mxImage<T> >& intensity_images, const std::vector< vect3<double> >& sample_pts, const CompositeFeatureSpace* fs, std::vector<double>& data_vector)
{
	/** setup input images */
	std::vector < mxImage<double> > input_images;
	setup_scale_intensity_images(intensity_images, sample_pts, input_images);
	if (m_ini.general.level > 0)
		setup_scale_context_images(intensity_images, sample_pts, input_images);
	assert_message(fs->GetNumberFeatureSpaces() == static_cast<int>(input_images.size()), "number of input images != number of feature spaces");

	/** compute integral images */
	std::auto_ptr<CompositeImageFeatureFunctor> preprocess_functor(fs->CreateImageFeatureFunctor());
	for (size_t i = 0; i < input_images.size(); ++i)
		preprocess_functor->PreprocessInput(input_images[i], i);

	/* allocate memory for features */
	unsigned int feature_num = fs->GetFeatureNumber();
	size_t old_data_size = data_vector.size();
	try {
		data_vector.resize(old_data_size + feature_num * sample_pts.size());
	}
	catch (std::exception&) {
		err_message("memory allocation fails");
	}

	std::cout << "   * Calculating 3D Haar-like features" << std::endl;

	/* voxel-wise feature extraction */
	#pragma omp parallel for
	for (int pt_idx = 0; pt_idx < static_cast<int>(sample_pts.size()); ++ pt_idx)
	{
		vect3<double> pt = sample_pts[pt_idx];

		// setup image functor
		std::auto_ptr<CompositeImageFeatureFunctor> thread_functor(fs->CreateImageFeatureFunctor());
		for (size_t j = 0; j < input_images.size(); ++j)
		{
			vect3<unsigned int> input_voxel;
			mxImageUtils::World2Voxel(input_images[j], pt, input_voxel);
			thread_functor->SetVoxel(input_voxel, j);
			thread_functor->SetImage(input_images[j], j);
		}

		size_t mem_p = old_data_size + static_cast<size_t>(pt_idx * feature_num);
		for (size_t featIdx = 0; featIdx < static_cast<size_t>(feature_num); ++featIdx)
			data_vector[mem_p + featIdx] = (*thread_functor)(featIdx);
	}
}

template <typename T>
void JAT_bd_trainer<T>::fill_train_data(MemoryDataCollection& train_data, Random& random, const CompositeFeatureSpace* fs)
{
	// train data - internal data structure
	std::vector<double>& data_vector = train_data.GetDataVector();
	std::vector<double>& target_vector = train_data.GetTargetVector();
	std::vector<int>& label_vector = train_data.GetLabelVector();

	// get basic information about the training data
	int feature_num = fs->GetFeatureNumber();
	int subject_num = static_cast<int>(m_annot.subject_num);

	// reserve memory to avoid extra memory during resize call for copying old data to new data memory
	size_t total_samples_per_image = 0;
	for (size_t i = 0; i < m_ini.sample_nums.size(); ++i)
		total_samples_per_image += m_ini.sample_nums[i];

	data_vector.reserve( total_samples_per_image * size_t(feature_num) * size_t(subject_num) );
	target_vector.reserve( total_samples_per_image * m_ini.general.bd_ids.size() * size_t(subject_num) );
	label_vector.reserve( total_samples_per_image * size_t(subject_num) );

	// total number of training samples
	size_t total_sample_num = 0;

	// boundary idx mapping from INI to ANNOT
	int bd_num = static_cast<int>(m_ini.general.bd_ids.size());
	std::vector<int> bd_map(bd_num);
	for (int i = 0; i < bd_num; ++i)
	{
		std::string name = m_ini.general.bd_ids[i];
		int found_idx = m_annot.get_bdid_by_name(name);
		assert_message(found_idx >= 0, (std::string(name) + " not found in ANNOT file").c_str());
		bd_map[i] = found_idx;
	}

	// extract features from each image
	for (int subjectIdx = 0; subjectIdx < subject_num; ++subjectIdx)
	{
		/* load intensity images */
		std::cout << "\n  - Loading subject ( " << subjectIdx + 1 << " / " << subject_num << " ): " << std::endl;
		std::vector< mxImage<T> > intensity_images;
		read_images_for_one_subject(subjectIdx, intensity_images);

		/* load mask images and extract boundaries */
		std::cout << "  - Loading boundary mask images and extracting boundary points:" << std::endl;
		std::vector< mxImage<unsigned char> > mask_images;
		std::vector< NearestPointIndexer<double> > boundaries;		// world coordinates
		read_masks_extract_boundaries_for_one_subject(subjectIdx, bd_map, mask_images, boundaries);

		/* sampling training points (world coordinate) */
		std::cout << "  - Drawing training points" << std::endl;
		std::vector< vect3<double> > sample_pts;
		draw_training_points_for_one_subject(intensity_images, mask_images, random, sample_pts);
		//_save_sample_map(intensity_images[0], sample_pts, (std::string("C:\\Users\\yzgao\\Desktop\\real\\sample") + stringUtils::num2str(subjectIdx) + ".mha").c_str());

		/* target vectors and class labels */
		size_t old_label_size = label_vector.size(), old_target_size = target_vector.size();

		std::cout << "  - Extracting 3D displacements and labels for training voxels" << std::endl;
		extract_targets_and_labels(mask_images, boundaries, sample_pts, target_vector, label_vector);
		//_save_label_map(intensity_images[0], sample_pts, &(label_vector[old_label_size]), "C:\\Users\\yzgao\\Desktop\\label.mha");
		//_save_dist_map(intensity_images[0], sample_pts, &(target_vector[old_target_size]), "C:\\Users\\yzgao\\Desktop\\real");

		// release mask images and boundary objects
		mask_images.clear();
		boundaries.clear();

		/* extract features */
		std::cout << "  - Extract 3D Haar features for training voxels" << std::endl;
		extract_features(intensity_images, sample_pts, fs, data_vector);

		// track the total number of voxels
		total_sample_num += static_cast<unsigned int>(sample_pts.size());
	}

	train_data.SetSampleNumber(static_cast<unsigned int>(total_sample_num));
	train_data.SetFeatureNumber(feature_num);
	train_data.SetTargetDim(bd_num * 3);
	train_data.SetClassNumber(bd_num + 1);
}

template <typename T>
std::auto_ptr< Tree<MemoryAxisAlignedWeakLearner, CRStatisticsAggregator> > JAT_bd_trainer<T>::TrainTree(Random& random, CompositeFeatureSpace* fs)
{
	MemoryDataCollection train_data;
	fill_train_data(train_data, random, fs);
	std::cout << "  - Number of Elements in Feature Array: " << train_data.GetDataVector().size() << std::endl;

	const TreeTrainingParameters& tree_params = m_ini.forest_params.treeParameters;

	unsigned int feature_num = fs->GetFeatureNumber();
	unsigned int target_dim = train_data.GetTargetDimension();
	unsigned int class_number = train_data.GetClassNumber();

	assert_message(feature_num != 0 && target_dim != 0 && class_number != 0, "feature number, target dimension and class number must be > 0");

	std::cout << "  - Computing Root Statistics" << std::endl;
	CRStatisticsAggregator rootStats(target_dim, class_number);
	for (unsigned int i = 0; i < train_data.GetSampleNumber(); ++i)
		rootStats.Aggregate(&train_data, i);

	std::cout << "  - Tree Training\n" << std::endl;
	CRTrainingContext train_context(feature_num, target_dim, class_number, rootStats.Entropy(), rootStats.MeanVariance(), m_ini.general.cr_ratio);
	return TreeTrainer<W, S>::TrainTree(train_context, tree_params, &train_data, random);
}

template <typename T>
std::string JAT_bd_trainer<T>::GetDetectorFolder(std::string name, int level) const
{
	std::string detectorFolder;
	if (level == 0)
		detectorFolder = m_ini.general.root_folder + stringUtils::Slash() + name;
	else
		detectorFolder = m_ini.general.root_folder + stringUtils::Slash() + name + "_C" + stringUtils::num2str(level);

	return detectorFolder;
}

template <typename T>
void JAT_bd_trainer<T>::CreateOutputFolder() const
{
	std::string detector_folder = GetDetectorFolder(m_ini.general.name, m_ini.general.level);
	if (!boost::filesystem::is_directory(detector_folder))
		boost::filesystem::create_directories(detector_folder);
}

template <typename T>
bool JAT_bd_trainer<T>::SaveTree(const Tree<W, S>* tree, const CompositeFeatureSpace* fs) const
{
	std::string detector_folder = GetDetectorFolder(m_ini.general.name, m_ini.general.level);

	char tree_file_path[2048] = { 0 };
	char feature_space_path[2048] = { 0 };

	// tree name & feature space name
	sprintf(tree_file_path, "%s%sTree_R%d_%s.bin", detector_folder.c_str(), stringUtils::Slash().c_str(), m_ini.general.resolution, stringUtils::DoubleToHexString(tree->GetUniqueID()).c_str());
	sprintf(feature_space_path, "%s%sTree_R%d_%s.fd", detector_folder.c_str(), stringUtils::Slash().c_str(), m_ini.general.resolution, stringUtils::DoubleToHexString(tree->GetUniqueID()).c_str());

	// tree output stream
	std::ofstream tree_out(tree_file_path, std::ios::binary);
	if (!tree_out) {
		std::cerr << "fails to open " << tree_file_path << " for write" << std::endl;
		return false;
	}
	tree->Serialize(tree_out);
	tree_out.close();

	// feature space output stream
	std::ofstream featurespace_out(feature_space_path, std::ios::binary);
	if (!featurespace_out) {
		std::cerr << "fails to open " << feature_space_path << " for write" << std::endl;
		return false;
	}
	fs->Serialize(featurespace_out);
	featurespace_out.close();

	return true;
}

template <typename T>
bool JAT_bd_trainer<T>::SaveDetectionInfo() const
{
	boost::property_tree::ptree pt;

	// resolution and context level
	char buffer[100] = { 0 };
	sprintf(buffer, "%f %f %f", m_ini.general.spacing[0], m_ini.general.spacing[1], m_ini.general.spacing[2]);
	pt.put("Info.Resolution", buffer);
	pt.put("Info.Level", m_ini.general.level);
	pt.put("Info.ModalityNum", m_ini.general.modality_num);
	if (m_ini.general.discard_background)
		pt.put("Info.DiscardBK", 1);
	else
		pt.put("Info.DiscardBK", 0);

	// boundary names
	std::string str_bd_ids = stringUtils::vec2str(m_ini.general.bd_ids, ' ');
	pt.put("Info.BDIDs", str_bd_ids);

	// sampling objects
	for (size_t i = 0; i < m_ini.general.bd_ids.size(); ++i)
	{
		std::string section_name = "Sampling-" + m_ini.general.bd_ids[i];
		m_ini.sample_objects[i]->AppendIniFile(pt, section_name);
	}

	// intensity scale information
	for (size_t i = 0; i < m_ini.general.modality_num; ++i)
	{
		std::string section_name = "Intensity" + stringUtils::num2str(i);
		pt.put(section_name + ".NumScales", m_ini.intensity[i].scales.size());

		for (size_t j = 0; j < m_ini.intensity[i].scales.size(); ++j)
		{
			std::string subsection_name = "Scale" + stringUtils::num2str(j);
			pt.put(section_name + "." + subsection_name + "_ScaleFactor", m_ini.intensity[i].scales[j].scale_factor);
		}
	}

	// context names and levels
	if (m_ini.general.level > 0)
	{
		std::string context_names = stringUtils::token2str(m_ini.context.names, ' ');
		std::string levels = stringUtils::vec2str(m_ini.context.levels, ' ');
		std::string feature_types;

		for (size_t i = 0; i < m_ini.context.names.size(); ++i) 
		{
			std::string tmp_str;
			switch (m_ini.context.types[i])
			{
			case JAT_REGRESSION_MAP: tmp_str = "R"; break;
			case JAT_CLASSIFICATION_MAP: tmp_str = "C"; break;
			case JAT_ALL_MAPS: tmp_str = "A"; break;
			default: err_message("unrecognized context feature types");
			}

			if (i == 0)
				feature_types += tmp_str;
			else
				feature_types += " " + tmp_str;
		}

		pt.put("Context.Names", context_names.c_str());
		pt.put("Context.Levels", levels.c_str());
		pt.put("Context.Features", feature_types.c_str());
		pt.put("Context.NumScales", m_ini.context.scales.size());

		// context scale information
		for (size_t j = 0; j < m_ini.context.scales.size(); ++j)
		{
			std::string section_name = "Context.Scale" + stringUtils::num2str(j);
			pt.put(section_name + "_ScaleFactor", m_ini.context.scales[j].scale_factor);
		}
	}

	// output file
	std::string detectorFolder = GetDetectorFolder(m_ini.general.name, m_ini.general.level);
	char path[2048] = { 0 };
	sprintf(path, "%s%sinfo_R%d.ini", detectorFolder.c_str(), stringUtils::Slash().c_str(), m_ini.general.resolution);

	try{
		boost::property_tree::ini_parser::write_ini(path, pt);
	}
	catch (boost::property_tree::ptree_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}

template <typename T>
int JAT_bd_trainer<T>::GetTreeNumber() const
{
	return m_ini.forest_params.numTrees;
}

template <typename T>
const JAT_bd_ini& JAT_bd_trainer<T>::GetINI()
{
	return m_ini;
}

template <typename T>
const JAT_bd_struct& JAT_bd_trainer<T>::GetAnnot()
{
	return m_annot;
}

//////////////////////////////////////////////////////////////////////////
// debug tools

template <typename T>
void JAT_bd_trainer<T>::_save_sample_map(const mxImage<T>& intensity_image, const std::vector< vect3<double> >& sample_pts, const char* outmap_path)
{
	mxImage<T> resampled_header;
	mxImageUtils::GetResampleHeader(intensity_image, m_ini.general.spacing, resampled_header);

	// world-to-voxel coordinate conversion
	std::vector< vect3<double> > sample_voxels;
	mxImageUtils::World2Voxel(resampled_header, sample_pts, sample_voxels);

	mxImage < unsigned char > sample_image;
	sample_image.CopyImageInfo(resampled_header);
	sample_image.SetImageSize(resampled_header.GetImageSize());
	sample_image.Fill(0);

	for (size_t i = 0; i < sample_voxels.size(); ++i)
	{
		int x = static_cast<int>(sample_voxels[i][0] + 0.5);
		int y = static_cast<int>(sample_voxels[i][1] + 0.5);
		int z = static_cast<int>(sample_voxels[i][2] + 0.5);
		if (!sample_image.PtInImage(x, y, z))
		{
			std::cerr << "one sample voxel (" << x << "," << y << "," << z << ") is out of image domain" << std::endl;
			exit(-1);
		}

		sample_image(x, y, z) += 1;
		//sample_image(x, y, z) = 1;
	}

	if (!ImageHelper::WriteImage<T, unsigned char>(sample_image, outmap_path))
	{
		std::cerr << "fail to write sample image to " << outmap_path << std::endl;
		exit(-1);
	}
}

template <typename T>
void JAT_bd_trainer<T>::_save_label_map(const mxImage<T>& intensity_image, const std::vector< vect3<double> >& sample_pts, const int* label, const char* output_path)
{
	mxImage<T> resampled_header;
	mxImageUtils::GetResampleHeader(intensity_image, m_ini.general.spacing, resampled_header);

	// world-to-voxel coordinate conversion
	std::vector< vect3<double> > sample_voxels;
	mxImageUtils::World2Voxel(resampled_header, sample_pts, sample_voxels);

	mxImage < int > sample_image;
	sample_image.SetImageSize(resampled_header.GetImageSize());
	sample_image.CopyImageInfo(resampled_header);
	sample_image.Fill(0);

	std::vector<int> label_counts(m_ini.general.bd_ids.size() + 1, 0);

	for (size_t i = 0; i < sample_voxels.size(); ++i)
	{
		int x = static_cast<int>(sample_voxels[i][0] + 0.5);
		int y = static_cast<int>(sample_voxels[i][1] + 0.5);
		int z = static_cast<int>(sample_voxels[i][2] + 0.5);
		if (!sample_image.PtInImage(x, y, z))
		{
			std::cerr << "one sample voxel (" << x << "," << y << "," << z << ") is out of image domain" << std::endl;
			exit(-1);
		}

		sample_image(x, y, z) = label[i] + 1;

		label_counts[label[i]] += 1;
	}

	if (!ImageHelper::WriteImage<int, int>(sample_image, output_path))
	{
		std::cerr << "fail to write sample image to " << output_path << std::endl;
		exit(-1);
	}

	for (size_t i = 0; i < label_counts.size(); ++i)
		std::cout << "label " << i << ": " << label_counts[i] << std::endl;
}

template <typename T>
void JAT_bd_trainer<T>::_save_dist_map(const mxImage<T>& intensity_image, const std::vector< vect3<double> >& sample_pts, const double* targets, const char* output_folder)
{
	mxImage<T> resampled_header;
	mxImageUtils::GetResampleHeader(intensity_image, m_ini.general.spacing, resampled_header);

	// world-to-voxel coordinate conversion
	std::vector< vect3<double> > sample_voxels;
	mxImageUtils::World2Voxel(resampled_header, sample_pts, sample_voxels);

	std::vector< mxImage <double> > sample_images(m_ini.general.bd_ids.size());
	for (size_t i = 0; i < m_ini.general.bd_ids.size(); ++i)
	{
		sample_images[i].SetImageSize(resampled_header.GetImageSize());
		sample_images[i].CopyImageInfo(resampled_header);
		sample_images[i].Fill(0);
	}

	std::vector< vect3<double> > disps(m_ini.general.bd_ids.size());

	for (size_t i = 0; i < sample_voxels.size(); ++i)
	{
		int x = static_cast<int>(sample_voxels[i][0] + 0.5);
		int y = static_cast<int>(sample_voxels[i][1] + 0.5);
		int z = static_cast<int>(sample_voxels[i][2] + 0.5);

		if (!sample_images[0].PtInImage(x, y, z))
		{
			std::cerr << "one sample voxel (" << x << "," << y << "," << z << ") is out of image domain" << std::endl;
			exit(-1);
		}

		for (size_t j = 0; j < m_ini.general.bd_ids.size(); ++j)
		{
			disps[j][0] = targets[i * m_ini.general.bd_ids.size() * 3 + j * 3];
			disps[j][1] = targets[i * m_ini.general.bd_ids.size() * 3 + j * 3 + 1];
			disps[j][2] = targets[i * m_ini.general.bd_ids.size() * 3 + j * 3 + 2];

			sample_images[j](x, y, z) = sqrt(disps[j][0] * disps[j][0] + disps[j][1] * disps[j][1] + disps[j][2] * disps[j][2]);
		}
	}

	for (size_t j = 0; j < m_ini.general.bd_ids.size(); ++j)
	{
		std::string output_path = std::string(output_folder) + stringUtils::Slash() + "dist" + stringUtils::num2str(j) + ".mha";
		if (!ImageHelper::WriteImage<double, double>(sample_images[j], output_path.c_str()))
			err_message("fails to write output image in function _save_disp_map");
	}
}




} } }


#endif