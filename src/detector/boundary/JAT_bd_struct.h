//
//  JAT_bd_struct.h
//  FISH
//
//  Created by Yaozong Gao on 09/01/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __JAT_bd_struct_h__
#define __JAT_bd_struct_h__


#include <vector>
#include <string>
#include "common/stringUtils.h"

namespace BRIC { namespace IDEA { namespace FISH {


class JAT_bd_struct
{
public:
	
	int subject_num;
	int modality_num;
	int bd_num;

	std::vector<std::string> bd_ids;

	std::vector< std::vector<std::string> > subject_image_paths;
	std::vector< std::vector<std::string> > subject_mask_paths;

	// public functions
	bool parse(const char* file);
	static bool write_default(const char* file);

	int get_bdid_by_name(std::string name);

	std::string get_image_path(int subject_idx, int modality_idx);
	std::string get_mask_path(int subject_idx, int bd_idx);

	void clear();

	void print_out_basic(std::ostream& os) const;
};



//////////////////////////////////////////////////////////////////////////
// Implementations
//////////////////////////////////////////////////////////////////////////

int JAT_bd_struct::get_bdid_by_name(std::string name)
{
	int ret = -1;
	for (int i = 0; i < static_cast<int>(bd_ids.size()); ++i)
	{
		if (bd_ids[i] == name)
		{
			ret = i;
			break;
		}
	}

	return ret;
}

void JAT_bd_struct::clear()
{
	subject_num = 0;
	modality_num = 0;
	bd_num = 0;

	bd_ids.clear();
	subject_image_paths.clear();
	subject_mask_paths.clear();
}

std::string JAT_bd_struct::get_image_path(int subject_idx, int modality_idx)
{
	return subject_image_paths[subject_idx][modality_idx];
}

std::string JAT_bd_struct::get_mask_path(int subject_idx, int bd_idx)
{
	return subject_mask_paths[subject_idx][bd_idx];
}

bool JAT_bd_struct::write_default(const char* file)
{
	FILE* fp = fopen(file, "w");
	if (fp == NULL)
	{
		std::cerr << "fail to open file " << file << " for write" << std::endl;
		return false;
	}

	fprintf(fp, "SUB\t2\n");
	fprintf(fp, "MOD\t2\n");
	fprintf(fp, "BD\t2\n");
	fprintf(fp, "BDID\tpro\tbla\n");
	fprintf(fp, "IMAGE\tC:\\Data\\subject1_CT.mhd\n");
	fprintf(fp, "IMAGE\tC:\\Data\\subject1_MR.mhd\n");
	fprintf(fp, "MASK\tC:\\Data\\subject1_CT_pro.mhd\n");
	fprintf(fp, "MASK\tC:\\Data\\subject1_CT_bla.mhd\n");
	fprintf(fp, "IMAGE\tC:\\Data\\subject2_CT.mhd\n");
	fprintf(fp, "IMAGE\tC:\\Data\\subject2_MR.mhd\n");
	fprintf(fp, "MASK\tC:\\Data\\subject2_CT_pro.mhd\n");
	fprintf(fp, "MASK\tC:\\Data\\subject2_CT_bla.mhd\n");

	fclose(fp);
	return true;
}


bool JAT_bd_struct::parse(const char* file)
{
	std::vector<std::string> lines;
	if (!stringUtils::ReadLines(file, lines))
	{
		std::cerr << "fail to read lines from " << file << std::endl;
		return false;
	}

	assert_message(lines.size() >= 3, "empty file");

	// clear
	clear();

	// read numbers
	std::vector<std::string> tokens;
	stringUtils::Split(lines[0], '\t', tokens);
	if (tokens.size() != 2 || tokens[0] != "SUB")
		err_message("first line format: SUBJECT\tNumber");
	subject_num = stringUtils::str2num<int>(tokens[1]);
	
	tokens.clear();
	stringUtils::Split(lines[1], '\t', tokens);
	if (tokens.size() != 2 || tokens[0] != "MOD")
		err_message("second line format: MODALITY\tNumber");
	modality_num = stringUtils::str2num<int>(tokens[1]);

	tokens.clear();
	stringUtils::Split(lines[2], '\t', tokens);
	if (tokens.size() != 2 || tokens[0] != "BD")
		err_message("third line format: BD\tNumber");
	bd_num = stringUtils::str2num<int>(tokens[1]);

	// read bd ids
	tokens.clear();
	stringUtils::Split(lines[3], '\t', tokens);
	if (tokens.size() != (bd_num + 1) || tokens[0] != "BDID")
		err_message("fourth line format: BDID\tID1\tID2 ...");

	bd_ids.resize(bd_num);
	for (int i = 0; i < bd_num; ++i)
		bd_ids[i] = tokens[i + 1];

	// allocate space
	subject_image_paths.resize(subject_num);
	subject_mask_paths.resize(subject_num);

	for (int i = 0; i < subject_num; ++i)
	{
		subject_image_paths[i].resize(modality_num);
		subject_mask_paths[i].resize(bd_num);
	}

	int start_line = 4;
	int end_line = start_line + (modality_num + bd_num) * subject_num - 1;

	if (lines.size() < end_line + 1)
	{
		std::cerr << "line number less than " << end_line + 1 << std::endl;
		exit(-1);
	}

	for (int i = 0; i < subject_num; ++i)
	{
		int subject_start = start_line + (modality_num + bd_num) * i;
		
		// fill in modality image paths
		for (int j = 0; j < modality_num; ++j)
		{
			tokens.clear();
			stringUtils::Split(lines[subject_start + j], '\t', tokens);
			if (tokens.size() != 2 || tokens[0] != "IMAGE")
				err_message("image line format: IMAGE\tPATH");

			subject_image_paths[i][j] = tokens[1];
		}

		subject_start += modality_num;
		// fill in mask paths
		for (int j = 0; j < bd_num; ++j)
		{
			tokens.clear();
			stringUtils::Split(lines[subject_start + j], '\t', tokens);
			if (tokens.size() != 2 || tokens[0] != "MASK")
				err_message("mask line format: MASK\tPATH");

			subject_mask_paths[i][j] = tokens[1];
		}
	}

	return true;
}


void JAT_bd_struct::print_out_basic(std::ostream& os) const
{
	os << "Total Number of Subjects:   " << subject_num << std::endl;
	os << "Number of Modalities:       " << modality_num << std::endl;
	os << "Number of Boundaries:       " << bd_num << std::endl;

	os << "Boundary IDs:               ";
	for (size_t i = 0; i < bd_ids.size(); ++i)
	{
		if (i != 0)
			os << ", " << bd_ids[i];
		else
			os << bd_ids[i];
	}
	os << std::endl;
}



} } }


#endif