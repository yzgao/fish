//
//  JAT_bd_tester.h
//  FISH
//
//  Created by Yaozong Gao on 02/09/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __JAT_bd_tester_h__
#define __JAT_bd_tester_h__

#include "forest/RandomForest.h"
#include "detector/DetectorIO.h"
#include "common/mxImageUtils.h"


namespace BRIC { namespace IDEA { namespace FISH {

/************************************************************************/
/* bd detector info                                                     */
/************************************************************************/

class JAT_bd_detector_info
{
public:
	
	// attributes
	vect3<double> spacing;													// resolution
	int level;																// context level
	int modality_num;														// modality num
	bool discard_background;												// discard background
	std::vector< std::string > bdids;										// boundary names
	std::vector< std::vector<double> > intensity_scale_factors;				// intensity scale factors

	std::vector< std::string > context_names;								// context names
	std::vector< int > context_levels;										// context levels
	std::vector< std::string >  context_feature_types;						// context feature types
	std::vector< double > context_scale_factors;							// context scale factors

	// methods
	bool Read(const char* file);
};


struct JAT_bd_detector
{
	JAT_bd_detector_info info;
	std::auto_ptr< Forest<MemoryAxisAlignedWeakLearner, CRStatisticsAggregator> > forest;
	FeatureSpaceArray<CompositeFeatureSpace> fss;
	int scale;

	JAT_bd_detector() {}
	int GetLabelDimension() const;
	int GetTargetDimension() const;
};


/************************************************************************/
/* bd tester                                                            */
/************************************************************************/

class JAT_bd_tester
{
public:

	// type definition
	typedef MemoryAxisAlignedWeakLearner W;
	typedef CRStatisticsAggregator S;
	typedef Tree<W, S> TreeType;
	typedef Forest<W, S> ForestType;

	// attributes
	const char* m_root_folder;				// detector root_folder
	const char* m_name;						// detector name
	int m_level;							// detector level

	// mask
	const mxImage<unsigned char>* m_mask;		// mask in any coordinate system is fine
	bool m_recursive_mask;						// indicate whether mask is recursive applied across context levels

	// methods
	JAT_bd_tester();
	void SetDetector(const char* root_folder, const char* name, int level);
	void SetMask(const mxImage<unsigned char>* mask, bool recursive = true);
	std::string GetDetectorFolder() const;
	std::string GetDetectorInfoPath(int scale) const;

	// compute single-resolution label and displacement maps
	template <typename T>
	void ComputeSingleResolutionMaps(
		const std::vector< mxImage<T> >& images,			// original images
		int scale,
		const vect3<double>& sp,							// bounding box sp (world) 
		const vect3<double>& ep,							// bounding box ep (world)
		std::vector< mxImage<double> >* label_maps,
		std::vector< mxImage<double> >* disp_maps
	) const;

private:

	template <typename T>
	void voxelwise_run(
		const std::vector< mxImage<T> >& images,
		const vect3<double>& sp,
		const vect3<double>& ep,
		const JAT_bd_detector& detector,
		mxImage<double>* label_maps,
		mxImage<double>* disp_maps
	) const;

	template <typename T>
	void setup_scale_intensity_images(
		const std::vector< mxImage<T> >& intensity_images,
		const vect3<double>& sp,
		const vect3<double>& ep,
		const JAT_bd_detector& detector,
		std::vector< mxImage<double> >& input_images
	) const;

	template <typename T>
	void setup_scale_context_images(
		const std::vector< mxImage<T> >& intensity_images,
		const vect3<double>& sp,
		const vect3<double>& ep,
		const JAT_bd_detector& detector,
		std::vector< mxImage<double> >& input_images
	) const;

	/************************************************************************/
	/* auxiliary functions                                                  */
	/************************************************************************/
	
	std::string get_detector_folder(std::string name, int level) const;
	void read_detector(int scale, JAT_bd_detector& detector) const;
	template <typename T> void get_current_header(const std::vector< mxImage<T> >& images, const JAT_bd_detector& detector, mxImage<T>& header) const;
	template <typename T1, typename T2> void get_cropped_header(const mxImage<T1>& header, const vect3<double>& sp, const vect3<double>& ep, mxImage<T2>& crop_header) const;
	std::auto_ptr< mxImage<unsigned char> > transform_mask(const mxImage<unsigned char>* original_mask, const mxImage<unsigned char>* ref_mask) const;
	template <typename T> void compute_effective_sp_ep(const mxImage<T>& image, const vect3<int>& sp, const vect3<int>& ep, vect3<int>& eff_sp, vect3<int>& eff_ep) const;
	int read_output_dimension(std::string name, int level, int scale, std::string type) const;
	void get_context_patchsizes(const JAT_bd_detector& detector, std::vector< vect3<unsigned int> >& patch_sizes) const;
	template <typename T> void compute_context_box(const std::vector< mxImage<T> >& images, const vect3<double>& sp, const vect3<double>& ep, const JAT_bd_detector& detector, vect3<double>& boxsp, vect3<double>& boxep) const;

};



//////////////////////////////////////////////////////////////////////////
// JAT_bd_detector_info implementation

bool JAT_bd_detector_info::Read(const char* file)
{
	boost::property_tree::ptree pt;
	try {
		boost::property_tree::ini_parser::read_ini(file, pt);
	}
	catch (boost::property_tree::ini_parser_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	try {

		// get general information
		spacing = stringUtils::ParseVect3<double>(pt.get<std::string>("Info.Resolution"));
		level = pt.get<int>("Info.Level");
		modality_num = pt.get<int>("Info.ModalityNum");
		bdids = stringUtils::Split(pt.get<std::string>("Info.BDIDs"), ' ');
		discard_background = pt.get<int>("Info.DiscardBK") == 0 ? false : true;
		
		// get intensity scale factors
		intensity_scale_factors.resize(modality_num);
		for (int i = 0; i < modality_num; ++i)
		{
			std::string section_name = std::string("Intensity") + stringUtils::num2str(i);
			int num_scales = pt.get<int>(section_name + ".NumScales");
			intensity_scale_factors[i].resize(num_scales);

			for (int j = 0; j < num_scales; ++j) {
				std::string entry_name = section_name + ".Scale" + stringUtils::num2str(j) + "_ScaleFactor";
				intensity_scale_factors[i][j] = pt.get<double>(entry_name);
			}
		}

		// get context information
		if (level > 0)
		{
			context_names = stringUtils::Split(pt.get<std::string>("Context.Names"), ' ');
			stringUtils::str2vec<int>(pt.get<std::string>("Context.Levels"), context_levels, ' ');
			context_feature_types = stringUtils::Split(pt.get<std::string>("Context.Features"), ' ');

			assert_message(context_names.size() == context_levels.size(), "inconsistent number of context detectors");
			assert_message(context_names.size() == context_feature_types.size(), "inconsistent number of context detectors");

			int num_scales = pt.get<int>("Context.NumScales");
			context_scale_factors.resize(num_scales);

			for (int j = 0; j < num_scales; ++j) {
				std::string entry_name = "Context.Scale" + stringUtils::num2str(j) + "_ScaleFactor";
				context_scale_factors[j] = pt.get<double>(entry_name);
			}
		}
	}
	catch (boost::property_tree::ptree_error& error)
	{
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}


//////////////////////////////////////////////////////////////////////////
// JAT_bd_detector implementation

int JAT_bd_detector::GetLabelDimension() const
{
	const CRStatisticsAggregator& s = forest->GetTree(0)->GetAnyLeafStatisticsAggregator();
	return s.GetClassNumber();
}

int JAT_bd_detector::GetTargetDimension() const
{
	const CRStatisticsAggregator& s = forest->GetTree(0)->GetAnyLeafStatisticsAggregator();
	return s.GetTargetDimension();
}


//////////////////////////////////////////////////////////////////////////
// JAT_bd_tester implementation

JAT_bd_tester::JAT_bd_tester()
{
	m_root_folder = NULL;
	m_name = NULL;
	m_level = -1;

	m_mask = NULL;
	m_recursive_mask = false;
}

void JAT_bd_tester::SetDetector(const char* root_folder, const char* name, int level)
{
	m_root_folder = root_folder;
	m_name = name;
	m_level = level;
}

void JAT_bd_tester::SetMask(const mxImage<unsigned char>* mask, bool recursive)
{
	m_mask = mask;
	m_recursive_mask = recursive;
}

std::string JAT_bd_tester::GetDetectorFolder() const
{
	return get_detector_folder(m_name, m_level);
}

std::string JAT_bd_tester::GetDetectorInfoPath(int scale) const
{
	std::string det_folder = GetDetectorFolder();
	return det_folder + stringUtils::Slash() + "info_R" + stringUtils::num2str(scale) + ".ini";
}

std::string JAT_bd_tester::get_detector_folder(std::string name, int level) const
{
	std::string det_folder;
	if (level == 0)
		det_folder = std::string(m_root_folder) + stringUtils::Slash() + std::string(name);
	else
		det_folder = std::string(m_root_folder) + stringUtils::Slash() + std::string(name) + "_C" + stringUtils::num2str(level);

	return det_folder;
}

template <typename T>
void JAT_bd_tester::get_current_header(const std::vector< mxImage<T> >& images, const JAT_bd_detector& detector, mxImage<T>& header) const
{
	mxImageUtils::GetResampleHeader(images[0], detector.info.spacing, header);
}

void JAT_bd_tester::read_detector(int scale, JAT_bd_detector& detector) const
{
	if (!detector.info.Read(GetDetectorInfoPath(scale).c_str()))
		err_message("fails to read detector info");

	detector.forest = DetectorIO::ReadDetector< Forest<W,S>, CompositeFeatureSpace >(GetDetectorFolder().c_str(), scale, detector.fss);

	if (detector.fss.GetFeatureSpaces().size() == 0 || detector.forest->GetTreeNumber() == 0)
		err_message("empty tree and feature space");

	detector.scale = scale;
}

template <typename T1, typename T2>
void JAT_bd_tester::get_cropped_header(const mxImage<T1>& header, const vect3<double>& sp, const vect3<double>& ep, mxImage<T2>& crop_header) const
{
	vect3<int> voxel_sp, voxel_ep;
	mxImageUtils::World2Voxel(header, sp, voxel_sp);
	mxImageUtils::World2Voxel(header, ep, voxel_ep);
	mxImageUtils::GetCropHeader2(header, voxel_sp, voxel_ep, crop_header);
}

std::auto_ptr< mxImage<unsigned char> > JAT_bd_tester::transform_mask(const mxImage<unsigned char>* original_mask, const mxImage<unsigned char>* ref_header) const
{
	std::auto_ptr< mxImage<unsigned char> > outmask_ptr;
	if (original_mask == NULL)
		outmask_ptr.reset(NULL);
	else {
		outmask_ptr.reset(new mxImage<unsigned char>);
		mxImageUtils::TransformMaskImage(*ref_header, *original_mask, *outmask_ptr);
	}
	return outmask_ptr;
}

template <typename T>
void JAT_bd_tester::compute_effective_sp_ep(const mxImage<T>& image, const vect3<int>& sp, const vect3<int>& ep, vect3<int>& eff_sp, vect3<int>& eff_ep) const
{
	vect3<unsigned int> image_size = image.GetImageSize();

	for (int i = 0; i < 3; ++i)
	{
		eff_sp[i] = sp[i];
		eff_ep[i] = ep[i];

		if (eff_sp[i] < 0)
			eff_sp[i] = 0;

		if (eff_sp[i] >= static_cast<int>(image_size[i]))
			eff_sp[i] = static_cast<int>(image_size[i]) - 1;

		if (eff_ep[i] < 0)
			eff_ep[i] = 0;

		if (eff_ep[i] >= static_cast<int>(image_size[i]))
			eff_ep[i] = static_cast<int>(image_size[i]) - 1;
	}
}

int JAT_bd_tester::read_output_dimension(std::string name, int level, int scale, std::string type) const
{
	std::string detector_folder = get_detector_folder(name, level);

	std::auto_ptr<TreeType> tree = DetectorIO::ReadAnyTree<TreeType>(detector_folder.c_str(), scale);
	const CRStatisticsAggregator& stat = tree->GetAnyLeafStatisticsAggregator();

	int class_number = stat.GetClassNumber();
	int target_dim = stat.GetTargetDimension();

	int ret_dim = 0;
	if (type == "R")
		ret_dim = target_dim;
	else if (type == "C")
		ret_dim = class_number;
	else if (type == "A")
		ret_dim = target_dim + class_number;
	else
		err_message("unrecognized context feature type");

	return ret_dim;
}

void JAT_bd_tester::get_context_patchsizes(const JAT_bd_detector& detector, std::vector< vect3<unsigned int> >& patch_sizes) const
{
	// context patch sizes
	patch_sizes.resize(detector.info.context_scale_factors.size());

	// intensity scale image number
	int num_intensity_images = 0;
	for (int i = 0; i < detector.info.modality_num; ++i)
		num_intensity_images += detector.info.intensity_scale_factors[i].size();

	// first context output dimension
	int out_dim = read_output_dimension(detector.info.context_names[0], detector.info.context_levels[0], detector.scale, detector.info.context_feature_types[0]);

	for (size_t i = 0; i < patch_sizes.size(); ++i)
		patch_sizes[i] = detector.fss[0].GetPatchSize(num_intensity_images + i * out_dim);
}

template <typename T>
void JAT_bd_tester::compute_context_box(const std::vector< mxImage<T> >& images, const vect3<double>& sp, const vect3<double>& ep, const JAT_bd_detector& detector, vect3<double>& box_sp_world, vect3<double>& box_ep_world) const
{
	// get current header
	mxImage<T> header;
	get_current_header(images, detector, header);

	// get context patch sizes
	std::vector< vect3<unsigned int> > context_patch_sizes;
	get_context_patchsizes(detector, context_patch_sizes);
	assert_message(context_patch_sizes.size() == detector.info.context_scale_factors.size(), "inconsistent scales in compute_context_box");

	// compute bounding box
	vect3 < int > box_sp, box_ep;
	for (size_t i = 0; i < detector.info.context_scale_factors.size(); ++i)
	{
		vect3<double> spacing = detector.info.spacing * detector.info.context_scale_factors[i];
		vect3<unsigned int> patch_size = context_patch_sizes[i];

		mxImage<T> scale_header;
		mxImageUtils::GetResampleHeader(images[0], spacing, scale_header);

		vect3<double> scale_sp, scale_ep;
		mxImageUtils::World2Voxel(scale_header, sp, scale_sp);
		mxImageUtils::World2Voxel(scale_header, ep, scale_ep);

		for (int k = 0; k < 3; ++k)
		{
			scale_sp[k] = scale_sp[k] - patch_size[k] / 2;
			scale_ep[k] = scale_ep[k] - patch_size[k] / 2 + patch_size[k] - 1;
		}

		vect3<double> mapped_sp, mapped_ep;
		mxImageUtils::Voxel2World(scale_header, scale_sp, mapped_sp);
		mxImageUtils::World2Voxel(header, mapped_sp);

		mxImageUtils::Voxel2World(scale_header, scale_ep, mapped_ep);
		mxImageUtils::World2Voxel(header, mapped_ep);

		vect3<int> round_sp, round_ep;
		for (int k = 0; k < 3; ++k)
		{
			round_sp[k] = static_cast<int>(mapped_sp[k]);
			round_ep[k] = static_cast<int>(mapped_ep[k]) + 1;
		}

		// update minimum bounding box
		if (i == 0)
		{
			box_sp = round_sp;
			box_ep = round_ep;
		}
		else
		{
			for (int k = 0; k < 3; ++k)
			{
				if (round_sp[k] < box_sp[k])
					box_sp[k] = round_sp[k];

				if (round_ep[k] > box_ep[k])
					box_ep[k] = round_ep[k];
			}
		}
	} // end context scales

	mxImageUtils::Voxel2World(header, box_sp, box_sp_world);
	mxImageUtils::Voxel2World(header, box_ep, box_ep_world);
}


//////////////////////////////////////////////////////////////////////////
// detail implementation for single resolution maps

template <typename T>
void JAT_bd_tester::ComputeSingleResolutionMaps(const std::vector< mxImage<T> >& images, int scale, const vect3<double>& sp, const vect3<double>& ep, std::vector< mxImage<double> >* label_maps, std::vector< mxImage<double> >* disp_maps) const
{
	// assertions
	assert_message(images.size() > 0, "empty image sizes");

	// read detector
	JAT_bd_detector detector;
	read_detector(scale, detector);

	// get header for current resolution
	mxImage<T> header;
	get_current_header(images, detector, header);

	// compute output image information (origin, size)
	vect3<int> out_sp, out_ep;
	mxImageUtils::World2Voxel(header, sp, out_sp);
	mxImageUtils::World2Voxel(header, ep, out_ep);

	vect3<unsigned int> output_size = (out_ep - out_sp + vect3<int>(1,1,1)).to<unsigned int>();
	vect3<double> output_origin = sp;

	// allocate label and displacement maps (keep existing maps for recursive function calls)
	int label_dim = detector.GetLabelDimension();
	int disp_dim = detector.GetTargetDimension();

	size_t old_label_size = 0, old_disp_size = 0;
	if (label_maps != NULL)
	{
		old_label_size = label_maps->size();
		label_maps->resize(old_label_size + label_dim - 1);

		for (size_t i = old_label_size; i < label_maps->size(); ++i)
		{
			(*label_maps)[i].CopyImageInfo(header);
			(*label_maps)[i].SetOrigin(output_origin);
			(*label_maps)[i].SetImageSize(output_size);
			(*label_maps)[i].Fill(0);
		}
	}

	if (disp_maps != NULL)
	{
		old_disp_size = disp_maps->size();
		disp_maps->resize(old_disp_size + disp_dim);

		for (size_t i = old_disp_size; i < disp_maps->size(); ++i)
		{
			(*disp_maps)[i].CopyImageInfo(header);
			(*disp_maps)[i].SetOrigin(output_origin);
			(*disp_maps)[i].SetImageSize(output_size);
			(*disp_maps)[i].Fill(0);
		}
	}

	// run main function
	mxImage<double>* out_label_maps = ( label_maps == NULL ? NULL : &((*label_maps)[old_label_size]) );
	mxImage<double>* out_disp_maps = ( disp_maps == NULL ? NULL : &((*disp_maps)[old_disp_size]) );

	voxelwise_run(images, sp, ep, detector, out_label_maps, out_disp_maps);
}

template <typename T>
void JAT_bd_tester::voxelwise_run(const std::vector< mxImage<T> >& images, const vect3<double>& sp, const vect3<double>& ep, const JAT_bd_detector& detector, mxImage<double>* label_maps, mxImage<double>* disp_maps) const
{
	// assertions
	assert_message(images.size() <= detector.fss[0].GetNumberFeatureSpaces(), "too many input images");

	// get class and displacement dimension
	int class_num = detector.GetLabelDimension();
	int target_dim = detector.GetTargetDimension();

	// get current and output header
	mxImage < T > header; mxImage<unsigned char> out_header;
	get_current_header(images, detector, header);
	get_cropped_header(header, sp, ep, out_header);

	// 1) transform mask image coordinate
	std::auto_ptr< mxImage<unsigned char> > mapped_mask = transform_mask(m_mask, &out_header);

	// 2) get box of region to compute
	vect3<int> out_sp, out_ep, eff_sp, eff_ep;
	mxImageUtils::World2Voxel(header, sp, out_sp);
	mxImageUtils::World2Voxel(header, ep, out_ep);
	compute_effective_sp_ep(header, out_sp, out_ep, eff_sp, eff_ep);

	vect3<double> eff_sp_world, eff_ep_world;
	mxImageUtils::Voxel2World(header, eff_sp, eff_sp_world);
	mxImageUtils::Voxel2World(header, eff_ep, eff_ep_world);

	// 3) prepare input images
	std::vector< mxImage<double> > input_images;
	setup_scale_intensity_images(images, eff_sp_world, eff_ep_world, detector, input_images);
	if (m_level > 0)
		setup_scale_context_images(images, eff_sp_world, eff_ep_world, detector, input_images);
	assert_message(input_images.size() == detector.fss[0].GetNumberFeatureSpaces(), "inconsistent number of images in FSS and inputs");

	std::auto_ptr<CompositeImageFeatureFunctor> preprocess_functor(detector.fss[0].CreateImageFeatureFunctor());
	for (size_t i = 0; i < input_images.size(); ++i)
		preprocess_functor->PreprocessInput(input_images[i], i);

	// 4) voxel-wise compute label / displacement maps
	#pragma omp parallel for schedule(dynamic)
	for (int z = out_sp[2]; z <= out_ep[2]; ++z)
	{
		// create thread functors
		std::vector < CompositeImageFeatureFunctor > thread_functors;
		thread_functors.resize(detector.fss.size());
		for (unsigned int j = 0; j < thread_functors.size(); ++j)
			new (&thread_functors[j]) CompositeImageFeatureFunctor(detector.fss[j]);

		// thread variables
		std::vector<double> disps(target_dim, 0);
		std::vector<double> probs(class_num, 0);
		std::vector< vect3<unsigned int> > positions(input_images.size());

		for (int y = out_sp[1]; y <= out_ep[1]; ++y)
		{
			for (int x = out_sp[0]; x <= out_ep[0]; ++x)
			{
				if (x < eff_sp[0] || x > eff_ep[0] || y < eff_sp[1] || y > eff_ep[1] || z < eff_sp[2] || z > eff_ep[2])
					continue;

				vect3<unsigned int> out_pos(x - out_sp[0], y - out_sp[1], z - out_sp[2]);
				if (mapped_mask.get() != NULL && (*mapped_mask)(out_pos) == 0)
					continue;

				vect3<double> world(x, y, z);
				mxImageUtils::Voxel2World(header, world);

				// compute image positions
				for (size_t img_idx = 0; img_idx < input_images.size(); ++img_idx)
					mxImageUtils::World2Voxel(input_images[img_idx], world, positions[img_idx]);

				// set images and positions
				for (size_t functor_idx = 0; functor_idx < thread_functors.size(); ++functor_idx)
				{
					for (size_t img_idx = 0; img_idx < input_images.size(); ++img_idx)
					{
						thread_functors[functor_idx].SetImage(input_images[img_idx], static_cast<int>(img_idx));
						thread_functors[functor_idx].SetVoxel(positions[img_idx], static_cast<int>(img_idx));
					}
				}

				// make prediction
				CRtester::Predict(detector.forest.get(), thread_functors, &(probs[0]), &(disps[0]));

				// output estimated probabilities and displacements
				if (label_maps)
				{
					for (int j = 1; j < class_num; ++j)
						label_maps[j - 1](out_pos) = probs[j];
				}

				if (disp_maps)
				{
					for (int j = 0; j < target_dim; ++j)
						disp_maps[j](out_pos) = disps[j];
				}
			} // end x
		} // end y
	} // end z
}

template <typename T>
void JAT_bd_tester::setup_scale_intensity_images(const std::vector< mxImage<T> >& intensity_images, const vect3<double>& sp, const vect3<double>& ep, const JAT_bd_detector& detector, std::vector< mxImage<double> >& input_images) const
{
	// compute total-scale intensity images
	int num_scale_intensity_images = 0;
	for (int i = 0; i < detector.info.modality_num; ++i)
		num_scale_intensity_images += static_cast<int>(detector.info.intensity_scale_factors[i].size());

	// allocate intensity image buffer
	input_images.resize(num_scale_intensity_images);

	// re-sample and setup scale images
	int image_idx = 0;
	for (int i = 0; i < detector.info.modality_num; ++i)
	{
		for (size_t j = 0; j < detector.info.intensity_scale_factors[i].size(); ++j)
		{
			vect3<double> scale_spacing = detector.info.spacing * detector.info.intensity_scale_factors[i][j];

			// re-sample image to the specified scale
			mxImage<T> out_image;
			mxImageUtils::Resample(intensity_images[i], scale_spacing, out_image, IT_LINEAR_INTERPOLATION);

			// compute bounding box
			vect3<int> voxel_sp, voxel_ep;
			mxImageUtils::World2Voxel(out_image, sp, voxel_sp);
			mxImageUtils::World2Voxel(out_image, ep, voxel_ep);

			// crop scale images
			vect3<unsigned int> patch_size = detector.fss[0].GetPatchSize(image_idx);
			mxImageUtils::Crop2_pad<T, double>(out_image, voxel_sp, voxel_ep, patch_size, input_images[image_idx], 0.0);

			++image_idx;
		}
	}
	assert_message(image_idx == num_scale_intensity_images, "unexpected error on num_scale_intensity_images");
}

template <typename T>
void JAT_bd_tester::setup_scale_context_images(const std::vector< mxImage<T> >& intensity_images, const vect3<double>& sp, const vect3<double>& ep, const JAT_bd_detector& detector, std::vector< mxImage<double> >& input_images) const
{
	// compute context box
	vect3<double> box_sp, box_ep;
	compute_context_box(intensity_images, sp, ep, detector, box_sp, box_ep);
	
	// compute context patch sizes
	std::vector< vect3<unsigned int> > context_patch_sizes;
	get_context_patchsizes(detector, context_patch_sizes);

	for (size_t i = 0; i < detector.info.context_names.size(); ++i)
	{
		// context detector output dimension
		size_t scale_num = detector.info.context_scale_factors.size();

		JAT_bd_tester aux;
		aux.SetDetector(m_root_folder, detector.info.context_names[i].c_str(), detector.info.context_levels[i]);

		if (m_recursive_mask)
			aux.SetMask(m_mask, true);
		else
			aux.SetMask(NULL, false);

		std::vector < mxImage<double> > output_images;
		if (detector.info.context_feature_types[i] == "R")
			aux.ComputeSingleResolutionMaps(intensity_images, detector.scale, box_sp, box_ep, NULL, &output_images);
		else if (detector.info.context_feature_types[i] == "C")
			aux.ComputeSingleResolutionMaps(intensity_images, detector.scale, box_sp, box_ep, &output_images, NULL);
		else if (detector.info.context_feature_types[i] == "A")
			aux.ComputeSingleResolutionMaps(intensity_images, detector.scale, box_sp, box_ep, &output_images, &output_images);
		else
			err_message("unrecognized context feature types");

		// allocate images
		size_t old_image_size = input_images.size();
		input_images.resize(old_image_size + output_images.size() * scale_num);

		int image_idx = 0;
		for (size_t j = 0; j < scale_num; ++j)
		{
			vect3<double> spacing = detector.info.spacing * detector.info.context_scale_factors[j];

			for (int k = 0; k < output_images.size(); ++k)
			{
				mxImage<double> scale_context_map;
				mxImageUtils::Resample(output_images[k], spacing, scale_context_map, IT_LINEAR_INTERPOLATION);

				vect3<int> scale_sp, scale_ep;
				mxImageUtils::World2Voxel(scale_context_map, sp, scale_sp);
				mxImageUtils::World2Voxel(scale_context_map, ep, scale_ep);
				
				mxImageUtils::Crop2_pad(scale_context_map, scale_sp, scale_ep, context_patch_sizes[j], input_images[old_image_size + image_idx], 0.0);

				// debug codes
				//std::string debug_outfolder = std::string("C:\\Users\\yzgao\\Desktop\\real\\context") + stringUtils::num2str(image_idx) + ".mha";
				//ImageHelper::WriteImage<double,double>(input_images[old_image_size + image_idx], debug_outfolder.c_str());

				++image_idx;
			}
		}
		assert_message(image_idx == output_images.size() * scale_num, "inconsistent image index in setup_scale_context_images");
	}
}


} } }



#endif
