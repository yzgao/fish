//
//  JAT_ini_struct.h
//  FISH
//
//  Created by Yaozong Gao on 06/16/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __JAT_ini_struct_h__
#define __JAT_ini_struct_h__

#include "forest/ForestTrainer.h"
#include "common/stringUtils.h"
#include "sample/BDSampleObjects.h"


namespace BRIC { namespace IDEA { namespace FISH {


enum JAT_context_feature_type
{
	JAT_REGRESSION_MAP = 0,
	JAT_CLASSIFICATION_MAP,
	JAT_ALL_MAPS
};

struct ScaleInfo
{
	double scale_factor;
	std::vector < vect3<unsigned int> > filter_sizes;
	vect3<unsigned int> patch_size;

	void clear();
	bool parse(boost::property_tree::ptree& pt, std::string section_name, int scale_idx);
	void print_out(std::ostream& os, std::string section_name, int scale_idx) const;
};

struct IntensityInfo
{
	double weight;
	std::vector< ScaleInfo > scales;

	void clear();
	bool parse(boost::property_tree::ptree& pt, int modIdx);
	void print_out(std::ostream& os, int modIdx) const;
};

struct ContextInfo
{
	double weight;
	std::vector< std::string > names;
	std::vector< int > levels;
	std::vector< JAT_context_feature_type > types;
	std::vector< ScaleInfo > scales;

	void clear();
	bool parse(boost::property_tree::ptree& pt);
	void print_out(std::ostream& os) const;
};

struct GeneralInfo
{
	std::string name;
	std::string annot_path;
	std::string root_folder;
	int level;
	int resolution;
	vect3<double> spacing;
	double cr_ratio;
	std::vector< std::string > bd_ids;
	int modality_num;
	bool discard_background;

	void clear();
	bool parse(boost::property_tree::ptree& pt);
	void print_out(std::ostream& os) const;
};

struct PreconditionInfo
{
	boost::function<bool(bool, bool)> fusion_op;
	std::vector< boost::function<bool(double, double)>  > comp_ops;
	std::vector< double > comp_thresholds;
	double label;

	std::string fusion_op_str;
	std::vector < std::string > comp_op_strs;

	void clear();
	bool parse(boost::property_tree::ptree& pt, std::string organ);
	void print_out(std::ostream& os, std::string organ) const;

	bool hold(double value) const;
};

struct JAT_bd_ini
{
	GeneralInfo general;

	// sampling
	std::vector< IBDSampleObject* > sample_objects;
	std::vector< int > sample_nums;

	ForestTrainingParameters forest_params;

	std::vector<IntensityInfo> intensity;
	ContextInfo context;

	// organ precondition 
	std::vector<PreconditionInfo> preconditions;

	// destructor
	~JAT_bd_ini();

	void clear();												// clear structure
	bool parse(const char* filepath);							// parse JAT_bd_ini from INI file
	static bool write_default(const char* filepath);			// write default JAT_bd_ini to INI file
	void print_out(std::ostream& os) const;						// print out the INI 

};

/************************************************************************/
/* general information implementation                                   */
/************************************************************************/

void GeneralInfo::clear()
{
	name = "";
	annot_path = "";
	root_folder = "";
	level = 0;
	resolution = 0;
	spacing = vect3<double>(0, 0, 0);
	cr_ratio = 1;
	bd_ids.clear();
	modality_num = 0;
	discard_background = true;
}

bool GeneralInfo::parse(boost::property_tree::ptree& pt)
{
	try {

		name = pt.get<std::string>("General.Name");
		annot_path = pt.get<std::string>("General.Annotation");
		root_folder = pt.get<std::string>("General.RootFolder");
		level = pt.get<int>("General.Level");
		resolution = pt.get<int>("General.Resolution");
		spacing = stringUtils::ParseVect3<double>(pt.get<std::string>("General.Spacing"));
		cr_ratio = pt.get<double>("General.CRRatio");
		stringUtils::Split(pt.get<std::string>("General.BDID"), ' ', bd_ids);
		modality_num = pt.get<int>("General.ModalityNum");
		discard_background = pt.get<int>("General.DiscardBK") == 0 ? false : true;

	}
	catch (boost::property_tree::ptree_error& error)
	{
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}

void GeneralInfo::print_out(std::ostream& os) const
{
	os << "Name:          " << name << std::endl;
	os << "Annot:         " << annot_path << std::endl;
	os << "Root:          " << root_folder << std::endl;
	os << "Level:         " << level << std::endl;
	os << "Resolution:    " << resolution << std::endl;
	os << "Spacing:       " << spacing << std::endl;
	os << "CRRatio:       " << cr_ratio << std::endl;
	os << "BDID:          " << stringUtils::token2str(bd_ids, ' ') << std::endl;
	os << "Modality:      " << modality_num << std::endl;
	os << "DiscardBK:     " << discard_background << std::endl;
}


/************************************************************************/
/* scale information implementation                                     */
/************************************************************************/

void ScaleInfo::clear()
{
	scale_factor = 1.0;
	filter_sizes.clear();
	patch_size = vect3<unsigned int>(0, 0, 0);
}

bool ScaleInfo::parse(boost::property_tree::ptree& pt, std::string section_name, int scale_idx)
{
	try {
		std::string prefix = section_name + ".Scale" + stringUtils::num2str(scale_idx);

		scale_factor = pt.get<double>((prefix + "_SpacingFactor").c_str());
		assert_message(scale_factor >= 1, "scale factor must be >= 1");
		std::string filtersize_str = pt.get<std::string>(prefix + "_FilterSize");

		std::vector<std::string> tmp = stringUtils::Split(filtersize_str, ' ');
		for (size_t j = 0; j < tmp.size(); ++j) {
			int tmp_size = stringUtils::str2num<unsigned int>(tmp[j]);
			filter_sizes.push_back(vect3<unsigned int>(tmp_size, tmp_size, tmp_size));
		}

		patch_size = stringUtils::ParseVect3<unsigned int>(pt.get<std::string>((prefix + "_PatchSize")), ' ');

	}
	catch (boost::property_tree::ptree_error& error)
	{
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}

void ScaleInfo::print_out(std::ostream& os, std::string, int scale_idx) const
{
	os << "Scale" << scale_idx << "_ScaleFactor:   " << scale_factor << std::endl;

	std::string filter_size_str;
	for (size_t i = 0; i < filter_sizes.size(); ++i)
	{
		if (i == 0)
			filter_size_str += stringUtils::num2str(filter_sizes[i][0]);
		else
			filter_size_str += " " + stringUtils::num2str(filter_sizes[i][0]);
	}

	os << "Scale" << scale_idx << "_FilterSize:    " << filter_size_str << std::endl;
	os << "Scale" << scale_idx << "_PatchSize:     " << patch_size << std::endl;
}


/************************************************************************/
/* intensity information implementation                                 */
/************************************************************************/

void IntensityInfo::clear()
{
	weight = 0;
	scales.clear();
}

bool IntensityInfo::parse(boost::property_tree::ptree& pt, int modIdx)
{
	std::string section_name = "Intensity" + stringUtils::num2str(modIdx);

	try {
		weight = pt.get<double>(section_name + ".Weight");

		int num_scales = pt.get<int>(section_name + ".NumScales");
		scales.resize(num_scales);
		for (size_t i = 0; i < scales.size(); ++i)
		{
			if (!scales[i].parse(pt, section_name, static_cast<int>(i)))
			{
				std::cerr << "fails to parse " << section_name << ".Scale" << i << std::endl;
				return false;
			}
		}

	}
	catch (boost::property_tree::ptree_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}

void IntensityInfo::print_out(std::ostream& os, int modIdx) const
{
	std::string section_name = "Intensity" + stringUtils::num2str(modIdx);

	os << "Weight:               " << weight << std::endl;
	for (size_t i = 0; i < scales.size(); ++i)
		scales[i].print_out(os, section_name, static_cast<int>(i));
}


/************************************************************************/
/* context information implementation                                   */
/************************************************************************/

void ContextInfo::clear() 
{
	weight = 0;
	names.clear();
	levels.clear();
	types.clear();
	scales.clear();
}

bool ContextInfo::parse(boost::property_tree::ptree& pt)
{
	try {

		weight = pt.get<double>("Context.Weight");
		names = stringUtils::Split(pt.get<std::string>("Context.Names"), ' ');
		stringUtils::str2vec<int>(pt.get<std::string>("Context.Levels"), levels, ' ');

		std::vector<std::string> tokens = stringUtils::Split(pt.get<std::string>("Context.Features"), ' ');
		types.resize(tokens.size());
		for (size_t i = 0; i < tokens.size(); ++i)
		{
			if (tokens[i] == "R")
				types[i] = JAT_REGRESSION_MAP;
			else if (tokens[i] == "C")
				types[i] = JAT_CLASSIFICATION_MAP;
			else if (tokens[i] == "A")
				types[i] = JAT_ALL_MAPS;
			else
				err_message("unrecognized context feature types");
		}

		int num_scales = pt.get<int>("Context.NumScales");
		scales.resize(num_scales);

		for (size_t i = 0; i < scales.size(); ++i)
		{
			if (!scales[i].parse(pt, "Context", static_cast<int>(i)))
			{
				std::cerr << "fails to parse Context.Scale" << i << std::endl;
				return false;
			}
		}

	}
	catch (boost::property_tree::ptree_error& error)
	{
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}

void ContextInfo::print_out(std::ostream& os) const
{
	os << "Weight:               " << weight << std::endl;
	os << "Names:                " << stringUtils::token2str(names, ' ') << std::endl;
	os << "Levels:               " << stringUtils::vec2str(levels, ' ') << std::endl;

	std::string format_str;
	for (size_t i = 0; i < types.size(); ++i)
	{
		std::string token;
		if (types[i] == JAT_CLASSIFICATION_MAP)
			token = "C";
		else if (types[i] == JAT_REGRESSION_MAP)
			token = "R";
		else if (types[i] == JAT_ALL_MAPS)
			token = "A";
		else
			err_message("unrecognized context feature types");

		if (i == 0)
			format_str += token;
		else
			format_str += " " + token;
	}

	os << "Types:                " << format_str << std::endl;

	for (size_t i = 0; i < scales.size(); ++i)
	{
		scales[i].print_out(os, "Context", static_cast<int>(i));
	}
}


/************************************************************************/
/* precondition implementation                                          */
/************************************************************************/

void PreconditionInfo::clear()
{
	fusion_op.clear();
	comp_ops.clear();
	comp_thresholds.clear();
	label = 0;

	fusion_op_str.clear();
	comp_op_strs.clear();
}

bool PreconditionInfo::parse(boost::property_tree::ptree& pt, std::string organ)
{
	std::string prefix = "Precondition-" + organ;

	try {
		fusion_op_str = pt.get<std::string>(prefix + ".Fusion");

		if (fusion_op_str == "OR")
			fusion_op = boost::bind(std::logical_or < bool > {}, _1, _2);
		else if (fusion_op_str == "AND")
			fusion_op = boost::bind(std::logical_and < bool > {}, _1, _2);
		else if (fusion_op_str == "NONE")
			fusion_op.clear();
		else
			err_message("unrecognized precondition fusion type");

		// if no precondition is set
		if (fusion_op.empty())
		{
			comp_op_strs.clear();
			comp_ops.clear();
			comp_thresholds.clear();
			return true;
		}

		std::string type_str = pt.get<std::string>(prefix + ".Types");
		std::string threshold_str = pt.get<std::string>(prefix + ".Thresholds");

		comp_op_strs = stringUtils::Split(type_str, ' ');
		std::vector<std::string> threshold_tokens = stringUtils::Split(threshold_str, ' ');

		assert_message(comp_op_strs.size() == threshold_tokens.size(), ("number mismatch between precondition types and thresholds (" + organ + ")").c_str() );

		comp_ops.resize(comp_op_strs.size());
		comp_thresholds.resize(threshold_tokens.size());

		for (size_t i = 0; i < comp_ops.size(); ++i)
		{
			if (comp_op_strs[i] == "GREATER")
				comp_ops[i] = boost::bind(std::greater < double > {}, _1, _2);
			else if (comp_op_strs[i] == "SMALLER")
				comp_ops[i] = boost::bind(std::less < double > {}, _1, _2);
			else
				err_message("unrecognized precondition comp type");

			comp_thresholds[i] = stringUtils::str2num<double>(threshold_tokens[i]);
		}

		label = pt.get<double>(prefix + ".Label");
	}
	catch (boost::property_tree::ptree_error& error)
	{
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}

void PreconditionInfo::print_out(std::ostream& os, std::string) const
{
	os << "Fusion:               " << fusion_op_str << std::endl;

	if (fusion_op.empty() || comp_op_strs.size() == 0)
		return;

	std::string comp_type_str;
	std::string threshold_str;
	for (size_t i = 0; i < comp_op_strs.size(); ++i)
	{
		if (i == 0)
		{
			comp_type_str += comp_op_strs[i];
			threshold_str += stringUtils::num2str(comp_thresholds[i]);
		}
		else
		{
			comp_type_str += " " + comp_op_strs[i];
			threshold_str += " " + stringUtils::num2str(comp_thresholds[i]);
		}
	}

	os << "Types:         " << comp_type_str << std::endl;
	os << "Threshold:     " << threshold_str << std::endl;
	os << "Label:         " << label << std::endl;
}

bool PreconditionInfo::hold(double value) const
{
	if (fusion_op.empty() || comp_ops.size() == 0)
		return false;

	bool ret = comp_ops[0](value, comp_thresholds[0]);
	for (size_t i = 1; i < comp_ops.size(); ++i)
		ret = fusion_op(ret, comp_ops[i](value, comp_thresholds[i]));

	return ret;
}

/************************************************************************/
/* implementation                                                       */
/************************************************************************/

JAT_bd_ini::~JAT_bd_ini()
{
	clear();
}

void JAT_bd_ini::clear()
{
	general.clear();

	if (sample_objects.size() > 0)
	{
		for (size_t i = 0; i < sample_objects.size(); ++i)
		{
			if (sample_objects[i] != NULL)
			{
				delete sample_objects[i];
				sample_objects[i] = NULL;
			}
		}
	}
	sample_objects.clear();
	sample_nums.clear();
	
	for (size_t i = 0; i < intensity.size(); ++i)
		intensity[i].clear();
	intensity.clear();
	context.clear();
	preconditions.clear();
}

bool JAT_bd_ini::parse(const char* file)
{
	clear();

	using boost::property_tree::ptree;

	ptree pt;
	try
	{
		boost::property_tree::ini_parser::read_ini(file, pt);
	}
	catch (boost::property_tree::ini_parser_error& error) 
	{
		std::cerr << error.what() << std::endl;
		return false;
	}

	try {

		// decode general section
		if (!general.parse(pt))
			return false;
	
		assert_message(general.modality_num > 0, "number of modality images must be non-zero");

		// decode sampling objects
		sample_nums.resize(general.bd_ids.size());
		sample_objects.resize(general.bd_ids.size());

		for (size_t i = 0; i < general.bd_ids.size(); ++i)
		{
			std::string section_name = "Sampling-" + general.bd_ids[i];
			std::string type = pt.get<std::string>(section_name + ".type");

			int sample_num = pt.get<int>(section_name + ".SampleNum");
			sample_nums[i] = sample_num;

			if (type == "Uniform")
			{
				UniformSampleObject* obj = new UniformSampleObject;
				vect3<double> surface_spacing = stringUtils::ParseVect3<double>(pt.get<std::string>(section_name + ".SurfaceSpacing"), ' ');

				obj->SetSurfaceSpacing(surface_spacing);

				sample_objects[i] = obj;
			}
			else if (type == "Gaussian")
			{
				GaussianBDSampleObject* obj = new GaussianBDSampleObject;
				vect3<double> surface_spacing = stringUtils::ParseVect3<double>(pt.get<std::string>(section_name +  ".SurfaceSpacing"), ' ');
				double smooth_sigma = pt.get<double>(section_name + ".MaskSmooth");
				double sigma_out = pt.get<double>(section_name + ".SigmaOut");
				double sigma_in = pt.get<double>(section_name + ".SigmaIn");

				obj->SetSurfaceSpacing(surface_spacing);
				obj->SetSigmaOut(sigma_out);
				obj->SetSigmaIn(sigma_in);
				obj->SetSmoothSigma(smooth_sigma);

				sample_objects[i] = obj;
			}
			else if (type == "SliceGaussian")
			{
				SliceGaussianBDSampleObject* obj = new SliceGaussianBDSampleObject;
				vect3<double> surface_spacing = stringUtils::ParseVect3<double>(pt.get<std::string>(section_name + ".SurfaceSpacing"), ' ');
				double smooth_sigma = pt.get<double>(section_name + ".MaskSmooth");
				double sigma_out = pt.get<double>(section_name + ".SigmaOut");
				double sigma_in = pt.get<double>(section_name + ".SigmaIn");

				obj->SetSurfaceSpacing(surface_spacing);
				obj->SetSigmaOut(sigma_out);
				obj->SetSigmaIn(sigma_in);
				obj->SetSmoothSigma(smooth_sigma);

				sample_objects[i] = obj;
			}
			else if (type == "MaskUniform")
			{
				MaskUniformSampleObject* obj = new MaskUniformSampleObject;
				vect3<double> surface_spacing = stringUtils::ParseVect3<double>(pt.get<std::string>(section_name + ".SurfaceSpacing"), ' ');
				double sigma = pt.get<double>(section_name + ".MaskSmooth");
				vect3<double> pad_radius = stringUtils::ParseVect3<double>(pt.get<std::string>(section_name + ".Pad"), ' ');
				double roi_portion = pt.get<double>(section_name + ".ROIRate");
				double roi_band = pt.get<double>(section_name + ".ROIBand");

				obj->SetSurfaceSpacing(surface_spacing);
				obj->SetSmoothSigma(sigma);
				obj->SetPadRadius(pad_radius);
				obj->SetROIRate(roi_portion);
				obj->SetROIBand(roi_band);

				sample_objects[i] = obj;
			}
			else
			{
				err_message("unknown type of sampling object");
			}
		}

		// decode forest parameters
		forest_params.numTrees = pt.get<int>("Forest.NumTrees");
		forest_params.treeParameters.maxTreeDepth = pt.get<int>("Forest.MaxTreeDepth");
		forest_params.treeParameters.numOfCandidateThresholdsPerWeakLearner = pt.get<int>("Forest.NumThresholds");
		forest_params.treeParameters.numOfRandomWeakLearners = pt.get<int>("Forest.NumWeakLearners");
		forest_params.treeParameters.minElementsOfLeafNode = pt.get<int>("Forest.MinLeafNum");
		forest_params.treeParameters.minInformationGain = pt.get<int>("Forest.MinInfoGain");

		// decode intensity feature parameters
		intensity.resize(general.modality_num);
		for (int i = 0; i < general.modality_num; ++i)
		{
			intensity[i].clear();

			if (!intensity[i].parse(pt, i))
				return false;
		}

		// decode context feature parameters
		if (general.level > 0)
		{
			if (!context.parse(pt))
				return false;
		}

		// decode organ preconditions
		preconditions.resize(general.bd_ids.size());
		for (size_t i = 0; i < preconditions.size(); ++i)
		{
			std::string organ = general.bd_ids[i];
			if (!preconditions[i].parse(pt, organ))
				return false;
		}

	}
	catch (boost::property_tree::ptree_error& error)
	{
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}

void JAT_bd_ini::print_out(std::ostream& os) const
{
	// print out general section
	os << std::endl << "[General]" << std::endl;
	general.print_out(os);

	// print out sampling section
	for (size_t i = 0; i < general.bd_ids.size(); ++i)
	{
		os << std::endl << "[Sampling-" << general.bd_ids[i] << "]" << std::endl;
		sample_objects[i]->Print(os);
	}

	// print out forest section
	os << std::endl << "[Forest]" << std::endl;
	os << "Trees:         " << forest_params.numTrees << std::endl;
	os << "Depth:         " << forest_params.treeParameters.maxTreeDepth << std::endl;
	os << "Thresholds:    " << forest_params.treeParameters.numOfCandidateThresholdsPerWeakLearner << std::endl;
	os << "Features:      " << forest_params.treeParameters.numOfRandomWeakLearners << std::endl;
	os << "MinLeaf:       " << forest_params.treeParameters.minElementsOfLeafNode << std::endl;

	// print out intensity feature section
	for (int i = 0; i < static_cast<int>(intensity.size()); ++i)
	{
		os << std::endl << "[Intensity" << i << "]" << std::endl;
		intensity[i].print_out(os, i);
	}

	// print out context feature section
	if (general.level > 0)
	{
		os << std::endl << "[Context]" << std::endl;
		context.print_out(os);
	}

	// print out organ pre-conditions
	for (size_t i = 0; i < general.bd_ids.size(); ++i)
	{
		os << std::endl << "[Preconditions-" << general.bd_ids[i] << "]" << std::endl;
		preconditions[i].print_out(os, general.bd_ids[i]);
	}
}

bool JAT_bd_ini::write_default(const char* file)
{
	std::ofstream out(file);

	if (!out)
	{
		std::cerr << "fail to open file for write: " << file << std::endl;
		return false;
	}

	const char* lines[] = {
		"; A sample INI file for BDTrain",
		"",
		"[General]",
		"Name=DetectorName",
		"Annotation=/Users/Test/annot.txt",
		"RootFolder=/Users/Test/OutFolder",
		"Level=1",
		"Resolution=1",
		"Spacing=1.0 1.0 1.0",
		"BDID=pro bla",
		"CRRatio=0.5",
		"ModalityNum=1",
		"DiscardBK=0",
		"",
		"[Sampling-pro]",
		"type=Gaussian",
		"SurfaceSpacing=1.0 1.0 1.0",
		"MaskSmooth=2.0",
		"SigmaOut=12",
		"SigmaIn=12",
		"SampleNum=10000",
		"",
		"[Sampling-bla]",
		"type=SliceGaussian",
		"SurfaceSpacing=1.0 1.0 1.0",
		"MaskSmooth=2.0",
		"SigmaOut=12",
		"SigmaIn=12",
		"SampleNum=10000",
		"",
		"[Forest]",
		"NumTrees=1",
		"MaxTreeDepth=100",
		"NumThresholds=100",
		"NumWeakLearners=2000",
		"MinLeafNum=8",
		"MinInfoGain=0",
		"",
		"[Intensity0]",
		"Weight=1",
		"Normalization=NONE",
		"NumScales=3",
		"Scale0_SpacingFactor=4",
		"Scale0_FilterSize=3 5",
		"Scale0_PatchSize=30 30 30",
		"Scale1_SpacingFactor=2",
		"Scale1_FilterSize=3 5",
		"Scale1_PatchSize=30 30 30",
		"Scale2_SpacingFactor=1",
		"Scale2_FilterSize=3 5",
		"Scale2_PatchSize=30 30 30",
		"",
		"[Context]",
		"Weight=1",
		"Names=Name1 Name2 Name3",
		"Levels=0 0 0",
		"Features=C R A",
		"NumScales=3",
		"Scale0_SpacingFactor=4",
		"Scale0_FilterSize=3 5",
		"Scale0_PatchSize=30 30 30",
		"Scale1_SpacingFactor=2",
		"Scale1_FilterSize=3 5",
		"Scale1_PatchSize=30 30 30",
		"Scale2_SpacingFactor=1",
		"Scale2_FilterSize=3 5",
		"Scale2_PatchSize=30 30 30",
		"",
		"[Precondition-pro]",
		"Fusion=OR",
		"Types=GREATER SMALLER",
		"Thresholds=800 1300",
		"",
		"[Precondition-bla]",
		"Fusion=NONE"
	};

	int num_lines = sizeof(lines) / sizeof(lines[0]);
	for (int i = 0; i < num_lines; ++i)
		out << lines[i] << std::endl;

	out.close();
	return true;
}

} } }

#endif