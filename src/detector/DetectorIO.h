//
//  DetectorIO.h
//  Fish
//
//  Created by Yaozong Gao on 9/26/13.
//

#ifndef __DetectorIO_h__
#define __DetectorIO_h__

#include "stdafx.h"
#include <iostream>
#include "common/stringUtils.h"
#include "feature/FeatureSpaceArray.h"
#include "feature/haar3d/Haar3DFeatureSpace.h"

namespace BRIC { namespace IDEA { namespace FISH {

struct DetectorInfo
{
	vect3<double> spacing;
	vect3<double> boxRadius;
};

class DetectorIO
{
public:
	// definitions
	static const char* STR_INI_RESOLUTION;
	static const char* STR_INI_BOXRADIUS;

	// shortcuts for read
	template <class TreeType> static std::auto_ptr<TreeType> ReadTree(const char* path);
	template <class TFeatureSpace> static std::auto_ptr<TFeatureSpace> ReadFeatureSpace(const char* path);
	template <class ForestType, class TFeatureSpace> static std::auto_ptr<ForestType> ReadDetector(const char* folder, int scale, FeatureSpaceArray<TFeatureSpace>& fss);
	template <class TreeType> static std::auto_ptr<TreeType> ReadAnyTree(const char* folder, int scale);
	template <typename T> static T ReadDetectorField(const char* folder, int scale, std::string fieldname);

	// deprecated
	static DetectorInfo ReadDetectorInfo(const char* folder, int scale);
	static vect3<double> ReadDetectorResolution(const char* folder, int scale);

	// shortcuts for write
	template <class TreeType> static bool WriteTree(const TreeType* tree, const char* path);
	static bool WriteFeatureSpace(const IFeatureSpace* fs, const char* path);

private:
	static void find_pairs(const char* folder, int scale, std::vector< std::pair<std::string, std::string> >& pairs);
};

const char* DetectorIO::STR_INI_RESOLUTION = "Info.Resolution";
const char* DetectorIO::STR_INI_BOXRADIUS = "Info.BoxRadius";

//////////////////////////////////////////////////////////////////////////

void DetectorIO::find_pairs(const char* folder, int scale, std::vector< std::pair<std::string,std::string> >& pairs)
{
	const int max_path_len = 1024;
	char treePrefix[max_path_len] = {0};
	sprintf(treePrefix, "Tree_R%d", scale);

	const char* treeExtension = ".bin";
	const char* fsExtension = ".fd";

	boost::filesystem::directory_iterator end_iter;
	for (boost::filesystem::directory_iterator iter(folder); iter != end_iter; ++iter) {

		// not a directory
		if( !boost::filesystem::is_directory(iter->status()) ) {

			std::string filename = iter->path().filename().string();
			std::string extension = iter->path().extension().string();

			if ( filename.find(treePrefix) != std::string::npos && extension == std::string(treeExtension) ) {

				std::string fsPath = iter->path().parent_path().string() + stringUtils::Slash() + iter->path().stem().string() + fsExtension;
				if( !boost::filesystem::exists(fsPath) ) 
					continue;

				pairs.push_back( std::make_pair(iter->path().string(), fsPath) ); 
			}
		}
	}
}


template <class TreeType> 
std::auto_ptr<TreeType> DetectorIO::ReadTree(const char* path)
{
	std::ifstream in(path, std::ios::binary);
	if(!in) {
		std::cerr << "could not read tree from " << path << std::endl; exit(-1);
	}
	std::auto_ptr< TreeType > tree = TreeType::Deserialize(in);
	in.close();

	return tree;
}

template <class TFeatureSpace> 
std::auto_ptr<TFeatureSpace> DetectorIO::ReadFeatureSpace(const char* path)
{
	std::ifstream in(path, std::ios::binary);
	if(!in) {
		std::cerr << "could not read feature space from " << path << std::endl; exit(-1);
	}
	std::auto_ptr<TFeatureSpace> fs( TFeatureSpace::Deserialize(in) );
	in.close();

	return fs;
}

template <class ForestType, class TFeatureSpace> 
std::auto_ptr<ForestType> DetectorIO::ReadDetector(const char* folder, int scale, FeatureSpaceArray<TFeatureSpace>& fss)
{
	std::vector< std::pair<std::string,std::string> > pairs;
	find_pairs(folder, scale, pairs);

	std::auto_ptr<ForestType> forest(new ForestType);
	fss.clear();

	for (unsigned int i = 0; i < pairs.size(); ++i)
	{
		std::auto_ptr<typename ForestType::TreeType> tree = ReadTree<typename ForestType::TreeType>(pairs[i].first.c_str());
		std::auto_ptr<TFeatureSpace> fs = ReadFeatureSpace<TFeatureSpace>(pairs[i].second.c_str());

		forest->AddTree(tree);
		fss.AddFeatureSpace(fs.release());
	}
	return forest;
}

template <class TreeType>
std::auto_ptr<TreeType> DetectorIO::ReadAnyTree(const char* folder, int scale)
{
	std::vector< std::pair<std::string, std::string> > pairs;
	find_pairs(folder, scale, pairs);

	assert_message(pairs.size() > 0, "empty tree and feature space pairs");

	std::auto_ptr<TreeType> tree = ReadTree<TreeType>(pairs[0].first.c_str());

	return tree;
}

DetectorInfo DetectorIO::ReadDetectorInfo(const char* folder, int scale)
{
	char filename[1024] = {0};
	sprintf(filename, "%s%sinfo_R%d.ini", folder, stringUtils::Slash().c_str(), scale);

	// load information from INI file
	DetectorInfo ret;
	boost::property_tree::ptree pt;
	try {
		boost::property_tree::ini_parser::read_ini(filename, pt);
		std::string stringSpacing = pt.get<std::string>(STR_INI_RESOLUTION);
		std::string stringBoxRadius = pt.get<std::string>(STR_INI_BOXRADIUS);

		ret.spacing = stringUtils::ParseVect3<double>(stringSpacing);
		ret.boxRadius = stringUtils::ParseVect3<double>(stringBoxRadius);
	} catch ( std::runtime_error& error ) {
		std::cerr << error.what() << std::endl;
		exit(-1);
	}
	return ret;
}

vect3<double> DetectorIO::ReadDetectorResolution(const char* folder, int scale)
{
	char filename[1024] = {0};
	sprintf(filename, "%s%sinfo_R%d.ini", folder, stringUtils::Slash().c_str(), scale);

	// load information from INI file
	vect3<double> spacing;
	boost::property_tree::ptree pt;
	try {
		boost::property_tree::ini_parser::read_ini(filename, pt);
		std::string stringSpacing = pt.get<std::string>(STR_INI_RESOLUTION);
		spacing = stringUtils::ParseVect3<double>(stringSpacing);
	} catch ( std::runtime_error& error ) {
		std::cerr << error.what() << std::endl;
		exit(-1);
	}
	return spacing;
}

template <typename T> 
T DetectorIO::ReadDetectorField(const char* folder, int scale, std::string fieldname)
{
	char filename[1024] = {0};
	sprintf(filename, "%s%sinfo_R%d.ini", folder, stringUtils::Slash().c_str(), scale);

	// load information from INI file
	T field;
	boost::property_tree::ptree pt;
	try {
		boost::property_tree::ini_parser::read_ini(filename, pt);
		field = pt.get<T>(fieldname);
	} catch ( std::runtime_error& error ) {
		std::cerr << error.what() << std::endl;
		exit(-1);
	}
	return field;
}


template <class TreeType> 
bool DetectorIO::WriteTree(const TreeType* tree, const char* path)
{
	std::ofstream out(path, std::ios::binary);
	if(!out) return false;
	tree->Serialize(out);
	out.close();
	return true;
}

bool DetectorIO::WriteFeatureSpace(const IFeatureSpace* fs, const char* path)
{
	std::ofstream out(path, std::ios::binary);
	if(!out) return false;
	fs->Serialize(out);
	out.close();
	return true;
}

} } }

#endif


