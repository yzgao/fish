//
//  ContextRFTrainer.h
//  FISH
//
//  Created by Yaozong Gao on 12/08/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __ContextRFTrainer_h__
#define __ContextRFTrainer_h__

#include "common/Random.h"
#include "common/mxImageUtils.h"

#include "forest/ForestTrainer.h"
#include "forest/DataCollection.h"
#include "forest/context/ExhaustSearchTrainingContext.h"
#include "forest/wl/MemoryAxisAlignedWeakLearner.h"
#include "forest/stat/MultiDimensionStatisticsAggregator.h"

#include "extern/ImageHelper.h"
#include "feature/CompositeFeatureSpace.h"
#include "feature/haar3d/Haar3DFeatureSpace.h"
#include "detector/ContextRF.h"

#include <boost/array.hpp>
#include <boost/timer/timer.hpp>

namespace BRIC { namespace IDEA { namespace FISH {

// source parameters
struct ContextSourceParameters
{
	std::string detectorName;						// detector name
	int level;										// context level
	std::string diffOption;							// NONE if not using difference displacement, "XX" if using difference with "XX" detector
	double weight;									// weight for feature numbers
	std::vector<unsigned int> filterSizes;			// filter sizes
	vect3<unsigned int> patchSize;					// patch size

	ContextSourceParameters() : diffOption("NONE") {}
};

// ContextRFTrainer parameters
struct ContextRFParameters
{
	// general section
	std::string detectorName;
	std::string rootFolder;
	int contextLevel;
	int scale;
	vect3<double> spacing;

	// forest section
	ForestTrainingParameters forestParams;

	// intensity section
	NormalizationType normalizationType;

	// intensity & context section
	std::vector<ContextSourceParameters> sources;
};


// context-sensitive regression forest trainer
template <typename T, int Dimension>
class ContextRFTrainer
{
public:
	// type definitions
	typedef MemoryAxisAlignedWeakLearner W;
	typedef MultiDimensionStatisticsAggregator<Dimension> S;
	typedef Tree<W,S> TreeType;
	typedef Forest<W,S> ForestType;
	typedef boost::array<double,Dimension> VectorType;
	typedef mxImage<VectorType> VectorImageType;

	// interface to be implemented
	virtual const std::vector<std::string>& GetImagePathVector() = 0;
	virtual void DrawSamples(const mxImage<T>& image, unsigned int imageIdx, Random& random, std::vector< vect3<unsigned int> >& voxels, std::vector<double>& targets) = 0;

	// main training interface
	std::auto_ptr<TreeType> TrainTree(Random& random, IFeatureSpace* fs, ContextRFParameters& params);
	std::auto_ptr<IFeatureSpace> CreateFeatureSpace(ContextRFParameters& params);

private:
	void GetBoundingBox(const std::vector< vect3<unsigned int> >& voxels, vect3<unsigned int>& sp, vect3<unsigned int>& ep);
	void PrepareInputImages(const mxImage<T>& image, const vect3<unsigned int>& sp, const vect3<unsigned int>& ep, IFeatureSpace* fs, const ContextRFParameters& params, std::vector< mxImage<double> >& inputImages);
	void _save_sample_map(const mxImage<T>& intensity_image, const std::vector< vect3<unsigned int> >& sample_voxels, const char* outmap_path);
};

//////////////////////////////////////////////////////////////////////////

template <typename T, int Dimension>
void ContextRFTrainer<T,Dimension>::GetBoundingBox(const std::vector< vect3<unsigned int> >& voxels, vect3<unsigned int>& sp, vect3<unsigned int>& ep)
{
	sp[0] = sp[1] = sp[2] = std::numeric_limits<unsigned int>::max();
	ep[0] = ep[1] = ep[2] = std::numeric_limits<unsigned int>::min();

	for (unsigned int i = 0; i < voxels.size(); ++i)
	{
		const vect3<unsigned int>& voxel = voxels[i];
		for (unsigned int j = 0; j < 3; ++j)
		{
			if (voxel[j] < sp[j])
				sp[j] = voxel[j];
			if (voxel[j] > ep[j])
				ep[j] = voxel[j];
		}
	}
}

template <typename T, int Dimension>
std::auto_ptr<IFeatureSpace> ContextRFTrainer<T,Dimension>::CreateFeatureSpace(ContextRFParameters& params)
{
	if (params.contextLevel == 0)
	{
		if (params.sources.size() != 1) {
			std::cerr << "number of source images must be 1 if context level is 0" << std::endl;
			exit(-1);
		}

		Haar3DFeatureSpace* fs = new Haar3DFeatureSpace;
		for (unsigned int i = 0; i < params.sources[0].filterSizes.size(); ++i) {
			unsigned int size = params.sources[0].filterSizes[i];
			fs->AddFilterSize( vect3<unsigned int>(size, size, size) );
		}
		fs->SetPatchSize(params.sources[0].patchSize);

		return std::auto_ptr<IFeatureSpace>(fs);

	}else {

		if (params.sources.size() <= 1) {
			std::cerr << "number of source images must be greater than 1 if context level > 0" << std::endl; exit(-1);
		}

		CompositeFeatureSpace* cfs = new CompositeFeatureSpace;
		for (unsigned int i = 0; i < params.sources.size(); ++i)
		{
			Haar3DFeatureSpace* fs = new Haar3DFeatureSpace;
			for (unsigned int j = 0; j < params.sources[i].filterSizes.size(); ++j)
			{
				unsigned int size = params.sources[i].filterSizes[j];
				fs->AddFilterSize(vect3<unsigned int>(size, size, size));
			}
			fs->SetPatchSize(params.sources[i].patchSize);
			cfs->AddFeatureSpace(std::auto_ptr<IFeatureSpace>(fs));
		}

		std::vector<double> weights;
		weights.resize(params.sources.size());
		for (unsigned int i = 0; i < params.sources.size(); ++i)
			weights[i] = params.sources[i].weight;
		cfs->SetWeights(&weights[0], weights.size());

		return std::auto_ptr<IFeatureSpace>(cfs);
	}
}

template <typename T, int Dimension>
void ContextRFTrainer<T, Dimension>::PrepareInputImages(const mxImage<T>& image, const vect3<unsigned int>& sp, const vect3<unsigned int>& ep, IFeatureSpace* fs, const ContextRFParameters& params, std::vector< mxImage<double> >& inputImages)
{
	const std::vector<ContextSourceParameters>& sources = params.sources;
	std::string rootFolder = params.rootFolder;
	int scale = params.scale;

	if (sources.size() == 0) {
		std::cerr << "no sources?" << std::endl; exit(-1);
	}

	inputImages.resize(sources.size());

	{ // prepare the intensity source
		vect3<int> req_sp(static_cast<int>(sp[0]), static_cast<int>(sp[1]), static_cast<int>(sp[2]));
		vect3<int> req_ep(static_cast<int>(ep[0]), static_cast<int>(ep[1]), static_cast<int>(ep[2]));

		vect3<unsigned int> patchSize = fs->GetPatchSize(0);
		mxImageUtils::PadBox(req_sp, req_ep, patchSize);

		mxImageUtils::Crop2(image, req_sp, req_ep, inputImages[0]);
		std::cout << "Cropping the intensity image by bounding box \t\tDONE" << std::endl;
	}

	if ( sources.size() <= 1 ) 
		return;
	
	// get displacement images from all other sources
	std::vector< VectorImageType > displacementImages;	
	displacementImages.resize( sources.size() - 1 );

	for (size_t i = 1; i < sources.size(); ++i)
	{
		vect3<int> req_sp(static_cast<int>(sp[0]), static_cast<int>(sp[1]), static_cast<int>(sp[2]));
		vect3<int> req_ep(static_cast<int>(ep[0]), static_cast<int>(ep[1]), static_cast<int>(ep[2]));
		vect3<unsigned int> patchSize = fs->GetPatchSize(i);
		mxImageUtils::PadBox(req_sp, req_ep, patchSize);

		ContextRF<T, Dimension>::localRF(image, rootFolder, sources[i].detectorName, sources[i].level, scale, req_sp, req_ep, displacementImages[i-1]);
		std::cout << "Computing the displacement image from detector " << sources[i].detectorName << " \t\tDONE" << std::endl;
	}

	// compute inter-displacement features if specified
	for (size_t i = 1; i < sources.size(); ++i)
	{
		if (sources[i].diffOption != "NONE") {

			// find the difference target
			size_t target_source_idx = 0;
			for (size_t j = 1; j < sources.size(); ++j) {
				if (sources[j].detectorName == sources[i].diffOption) {
					target_source_idx = j; break;
				}
			}

			if (target_source_idx == 0) {
				std::cerr << "no difference target named (" << sources[i].diffOption << ") is found" << std::endl; exit(-1);
			}

			// compute L2-norm distance map of displacement[i-1] - displacement[j-1]
			vect3<unsigned int> image_size = displacementImages[i - 1].GetImageSize();
			inputImages[i].CopyImageInfo(displacementImages[i-1]);
			inputImages[i].SetImageSize(image_size);

			#pragma omp parallel for
			for (int z = 0; z < static_cast<int>(image_size[2]); ++z) {
				for (int y = 0; y < static_cast<int>(image_size[1]); ++y) {
					for (int x = 0; x < static_cast<int>(image_size[0]); ++x) {
						
						const VectorType& p1 = displacementImages[i - 1](x, y, z);
						const VectorType& p2 = displacementImages[target_source_idx - 1](x, y, z);

						if (Dimension == 1)
						{
							double v1 = p1[0], v2 = p2[0];
							inputImages[i](x, y, z) = v1 - v2;
						}
						else {
							double v = 0;
							for (int k = 0; k < 3; ++k)
								v += (p1[k] - p2[k]) * (p1[k] - p2[k]);
							inputImages[i](x, y, z) = sqrt(v);

							//double v1 = sqrt(p1[0] * p1[0] + p1[1] * p1[1] + p1[2] * p1[2]);
							//double v2 = sqrt(p2[0] * p2[0] + p2[1] * p2[1] + p2[2] * p2[2]);
							//inputImages[i](x, y, z) = v1 - v2;
						}
					}
				}
			}
		}
		else {
			if (Dimension == 1) {
				// extract first component since dimension = 1
				mxImageUtils::ExtractFromVectorImage<double, double, Dimension>(displacementImages[i - 1], 0, inputImages[i]);
			} else {
				// compute L2-norm distance map
				mxImageUtils::Vector2Norm<double, Dimension>(displacementImages[i - 1], inputImages[i]);
			}
		}
	}
}

template <typename T, int Dimension>
std::auto_ptr<typename ContextRFTrainer<T,Dimension>::TreeType> ContextRFTrainer<T,Dimension>::TrainTree(Random& random, IFeatureSpace* fs, ContextRFParameters& params)
{
	boost::timer::auto_cpu_timer timer;

	// check parameter consistency
	if ( params.sources.size() != fs->GetNumberFeatureSpaces() ) {
		std::cerr << "number of feature spaces != detector number + 1" << std::endl; exit(-1);
	}

	// read parameters
	TreeTrainingParameters& treeParams = params.forestParams.treeParameters;
	vect3<double> spacing = params.spacing;
	int numRandomFeatures = treeParams.numOfRandomWeakLearners;
	int contextLevel = params.contextLevel;
	int scale = params.scale;
	const std::vector<ContextSourceParameters>& sources = params.sources;
	std::string rootFolder = params.rootFolder;
	NormalizationType normalizationType = params.normalizationType;
	const std::vector<std::string>& imagePaths = GetImagePathVector();
	int numTrainingImages = static_cast<int>(imagePaths.size());

	// generate random features
	if (fs->GetNumberFeatureSpaces() == 1)
		fs->RandomizeFeatureSpace(random, numRandomFeatures);
	else {
		CompositeFeatureSpace* cfs = static_cast<CompositeFeatureSpace*>(fs);
		std::vector<unsigned int> num_array;
		cfs->DistrubuteFeatures(numRandomFeatures, num_array);

		int feature_space_num = cfs->GetNumberFeatureSpaces();
		for (int i = 0; i < feature_space_num; ++i)
		{
			Haar3DFeatureSpace* fs_ref = static_cast<Haar3DFeatureSpace*>( cfs->GetFeatureSpace(i) );
			if (i == 0)
				fs_ref->RandomizeFeatureSpace_twoblocks(random, num_array[i]);
			else
				fs_ref->RandomizeFeatureSpace_oneblock(random, num_array[i]);
		}
	}

	// prepare training data buffer
	MemoryDataCollection trainingData;
	trainingData.SetFeatureNumber(numRandomFeatures);
	trainingData.SetTargetDim(Dimension);

	std::vector<double>& data = trainingData.GetDataVector();
	std::vector<double>& targets = trainingData.GetTargetVector();

	const int buffered_sample_num = 10000;
	data.reserve(numTrainingImages * numRandomFeatures * buffered_sample_num);
	targets.reserve(numTrainingImages * Dimension * buffered_sample_num);

	unsigned int totalNumSamples = 0;

	// extract features from all training images
	for (int imageIdx = 0; imageIdx < numTrainingImages; ++imageIdx)
	{
		const std::string& imagePath = imagePaths[imageIdx];
		std::cout << "Reading image " << imagePath << std::endl;

		// read and re-sample image
		mxImage<T> image;
		{
			mxImage<T> origImage;
			if(!ImageHelper::ReadImage<T>(imagePath.c_str(),origImage)) {
				std::cerr << "fail to read image from " << imagePath << std::endl; exit(-1);
			}
			mxImageUtils::Resample(origImage,spacing,image);
		}

		// draw samples
		std::vector< vect3<unsigned int> > voxels; 
		std::cout << "Sampling " << imagePath;
		DrawSamples(image, imageIdx, random, voxels, targets);
		std::cout << " (" << voxels.size() << ")" << std::endl;
		//_save_sample_map(image, voxels, "C:\\Users\\yzgao\\Desktop\\hello.mha");

		// compute bounding box for training sample points
		vect3<unsigned int> sp, ep;
		GetBoundingBox(voxels, sp, ep);

		// compute the input images (intensity image + context images)
		std::vector< mxImage<double> > inputImages;
		PrepareInputImages(image, sp, ep, fs, params, inputImages);

		// allocate training data buffer for image features
		int numTrainingVoxels = static_cast<int>( voxels.size() );
		try {
			data.resize( static_cast<size_t>(totalNumSamples+ numTrainingVoxels) * static_cast<size_t>(numRandomFeatures) );
		} catch (std::exception& exp) {
			double memoryGB = static_cast<double>( (totalNumSamples+numTrainingVoxels)*numRandomFeatures*sizeof(double) ) / (1024.0*1024.0*1024.0);
			std::cerr << "memory allocation fails (" << exp.what() << "), requested " << memoryGB << " GB memory" << std::endl;
			exit(-1);
		}

		// parallel computing features for all sampled patches
		#pragma omp parallel for
		for (int i = 0; i < numTrainingVoxels; ++i)
		{
			std::auto_ptr<IPatchFeatureFunctor> functor( fs->CreatePatchFeatureFunctor() );

			std::vector< mxImage<double> > inputPatches;
			inputPatches.resize( fs->GetNumberFeatureSpaces() );

			for (unsigned int j = 0; j < fs->GetNumberFeatureSpaces(); ++j)
			{
				vect3<unsigned int> patchSize = fs->GetPatchSize(j);
				vect3<unsigned int> adjusted_voxel;
				adjusted_voxel = voxels[i] - sp + patchSize/2;

				mxImageUtils::Crop3(inputImages[j], adjusted_voxel, patchSize, inputPatches[j], static_cast<double>(0));

				if (j == 0 && normalizationType != NONE)
					mxImageUtils::Normalize(inputImages[j], normalizationType);

				functor->PreprocessInput(inputPatches[j], j);
				functor->SetPatch(inputPatches[j], j);
			}

			size_t currentPointer = static_cast<size_t>(totalNumSamples) * numRandomFeatures + static_cast<size_t>(i) * numRandomFeatures;
			for (int j = 0; j < static_cast<int>(numRandomFeatures); ++j)
				data[currentPointer + j] = functor->GetResponse(j);
		}
		totalNumSamples = static_cast<unsigned int>( totalNumSamples + numTrainingVoxels );
	}

	trainingData.SetSampleNumber(totalNumSamples);

	// run training
	ExhaustSearchTrainingContext<S> context(numRandomFeatures);
	std::auto_ptr< TreeType > tree = TreeTrainer<W,S>::TrainTree(context, treeParams, &trainingData, random);

	return tree;
}


template <typename T, int Dimension>
void ContextRFTrainer<T, Dimension>::_save_sample_map(const mxImage<T>& intensity_image, const std::vector< vect3<unsigned int> >& sample_voxels, const char* outmap_path)
{
	mxImage < unsigned char > sample_image;
	sample_image.CopyImageInfo(intensity_image);
	sample_image.SetImageSize(intensity_image.GetImageSize());
	sample_image.Fill(0);

	for (size_t i = 0; i < sample_voxels.size(); ++i)
	{
		int x = sample_voxels[i][0];
		int y = sample_voxels[i][1];
		int z = sample_voxels[i][2];
		if (!sample_image.PtInImage(x, y, z))
		{
			std::cerr << "one sample voxel (" << x << "," << y << "," << z << ") is out of image domain" << std::endl;
			exit(-1);
		}

		sample_image(x, y, z) += 1;
		//sample_image(x, y, z) = 1;
	}

	if (!ImageHelper::WriteImage<T, unsigned char>(sample_image, outmap_path))
	{
		std::cerr << "fail to write sample image to " << outmap_path << std::endl;
		exit(-1);
	}
}


} } }

#endif
