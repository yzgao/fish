//
//  BDSampleObjects.h
//  FISH
//
//  Created by Yaozong Gao on 01/09/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __BDSampleObjects_h__
#define __BDSampleObjects_h__

#include "common/mxImageUtils.h"
#include "sample/ImageSampler.h"
#include "mesh/surface/SurfaceUtils.h"


namespace BRIC { namespace IDEA { namespace FISH {


// interface for sample object
class IBDSampleObject
{
public:
	virtual void Sample(const mxImage<unsigned char>& mask, int num, Random& random, std::vector< vect3<double> >& pts) = 0;
	virtual vect3<double> GetSurfaceSpacing() const = 0;

	virtual void Print(std::ostream& os) = 0;
	virtual void AppendIniFile(boost::property_tree::ptree& pt, std::string section_name) = 0;
	virtual std::string GetSampleTypeName() const = 0;
};

// Uniform sample object
class UniformSampleObject : public IBDSampleObject
{
public:
	virtual void Sample(const mxImage<unsigned char>& mask, int num, Random& random, std::vector< vect3<double> >& pts);
	virtual vect3<double> GetSurfaceSpacing() const { return m_surface_spacing;  }
	void SetSurfaceSpacing(const vect3<double>& spacing) { m_surface_spacing = spacing;  }

	virtual void Print(std::ostream& os) {
		os << "Type:           " << "Uniform" << std::endl;
		os << "SurfSpacing:    " << m_surface_spacing << std::endl;
	}

	virtual void AppendIniFile(boost::property_tree::ptree& pt, std::string section_name) {
		pt.put(section_name + ".Type", "Uniform");
		std::string str_spacing = stringUtils::num2str(m_surface_spacing[0]) + " " + stringUtils::num2str(m_surface_spacing[1]) + " " + stringUtils::num2str(m_surface_spacing[2]);
		pt.put(section_name + ".ROIBand", 300);
		pt.put(section_name + ".SurfSpacing", m_surface_spacing.to_string());
	}

	virtual std::string GetSampleTypeName() const {
		return std::string("Uniform");
	}

public:
	vect3<double> m_surface_spacing;
};

// Mask-Uniform sample object
class MaskUniformSampleObject : public IBDSampleObject
{
public:
	virtual void Sample(const mxImage<unsigned char>& mask, int num, Random& random, std::vector< vect3<double> >& pts);
	virtual vect3<double> GetSurfaceSpacing() const { return m_surface_spacing; }

	void SetSurfaceSpacing(const vect3<double>& spacing) { m_surface_spacing = spacing; }
	void SetSmoothSigma(double sigma) { m_smooth_sigma = sigma;  }
	void SetROIMask(const mxImage<unsigned char>* roi_mask) { m_roi_mask = roi_mask; }
	void SetPadRadius(const vect3<double>& pad_radius) { m_pad_radius = pad_radius; }
	void SetROIRate(double rate) { m_roi_rate = rate;  }
	void SetROIBand(double band_len) { m_band_len = band_len; }

	virtual void Print(std::ostream& os) {
		os << "Type:          " << "MaskUniform" << std::endl;
		os << "SurfSpacing:   " << m_surface_spacing << std::endl;
		os << "SmoothSigma:   " << m_smooth_sigma << std::endl;
		os << "Pad (mm):      " << m_pad_radius << std::endl;
		os << "ROI Rate:      " << m_roi_rate << std::endl;
		os << "Band (mm):     " << m_band_len << std::endl;
	}

	virtual void AppendIniFile(boost::property_tree::ptree& pt, std::string section_name) {

		pt.put(section_name + ".Type", "MaskUniform");
		
		std::string str_spacing = stringUtils::num2str(m_surface_spacing[0]) + " " + stringUtils::num2str(m_surface_spacing[1]) + " " + stringUtils::num2str(m_surface_spacing[2]);

		pt.put(section_name + ".SurfSpacing", m_surface_spacing.to_string());
		pt.put(section_name + ".SmoothSigma", m_smooth_sigma);
		pt.put(section_name + ".Pad", m_pad_radius.to_string());
		pt.put(section_name + ".ROIRate", m_roi_rate);
		pt.put(section_name + ".ROIBand", m_band_len);
	}

	virtual std::string GetSampleTypeName() const {
		return std::string("MaskUniform");
	}

public:
	vect3<double> m_surface_spacing;
	double m_smooth_sigma;
	vect3<double> m_pad_radius;
	double m_roi_rate;
	double m_band_len;
	const mxImage<unsigned char>* m_roi_mask;
};

// Gaussian sample object
class GaussianBDSampleObject : public IBDSampleObject
{
public:
	virtual void Sample(const mxImage<unsigned char>& mask, int num, Random& random, std::vector< vect3<double> >& pts);
	virtual vect3<double> GetSurfaceSpacing() const { return m_surface_spacing; }

	void SetSurfaceSpacing(const vect3<double>& spacing) { m_surface_spacing = spacing; }
	void SetSmoothSigma(double sigma) { m_smooth_sigma = sigma;  }
	void SetSigmaIn(double sigma_in) { m_sigma_in = sigma_in; }
	void SetSigmaOut(double sigma_out) { m_sigma_out = sigma_out;  }

	virtual void Print(std::ostream& os) {
		os << "Type:          " << "Gaussian" << std::endl;
		os << "SurfSpacing:   " << m_surface_spacing << std::endl;
		os << "SmoothSigma:   " << m_smooth_sigma << std::endl;
		os << "SigmaIn:       " << m_sigma_in << std::endl;
		os << "SigmaOut:      " << m_sigma_out << std::endl;
	}

	virtual void AppendIniFile(boost::property_tree::ptree& pt, std::string section_name) {

		pt.put(section_name + ".Type", "Gaussian");

		std::string str_spacing = stringUtils::num2str(m_surface_spacing[0]) + " " + stringUtils::num2str(m_surface_spacing[1]) + " " + stringUtils::num2str(m_surface_spacing[2]);
		pt.put(section_name + ".SurfSpacing", str_spacing);
		pt.put(section_name + ".SmoothSigma", m_smooth_sigma);
		pt.put(section_name + ".SigmaIn", m_sigma_in);
		pt.put(section_name + ".SigmaOut", m_sigma_out);
	}

	virtual std::string GetSampleTypeName() const {
		return std::string("Gaussian");
	}

public:
	vect3<double> m_surface_spacing;
	double m_smooth_sigma;

	double m_sigma_in;
	double m_sigma_out;
};

// Between-slice Gaussian sample object
class SliceGaussianBDSampleObject : public IBDSampleObject
{
public:
	virtual void Sample(const mxImage<unsigned char>& mask, int num, Random& random, std::vector< vect3<double> >& pts);
	virtual vect3<double> GetSurfaceSpacing() const { return m_surface_spacing; }

	void SetSurfaceSpacing(const vect3<double>& spacing) { m_surface_spacing = spacing;  }
	void SetSmoothSigma(double sigma) { m_smooth_sigma = sigma;  }
	void SetSigmaIn(double sigma_in) { m_sigma_in = sigma_in;  }
	void SetSigmaOut(double sigma_out) { m_sigma_out = sigma_out;  }

	virtual void Print(std::ostream& os) {
		os << "Type:          " << "SliceGaussian" << std::endl;
		os << "SurfSpacing:   " << m_surface_spacing << std::endl;
		os << "SmoothSigma:   " << m_smooth_sigma << std::endl;
		os << "SigmaIn:       " << m_sigma_in << std::endl;
		os << "SigmaOut:      " << m_sigma_out << std::endl;
	}

	virtual void AppendIniFile(boost::property_tree::ptree& pt, std::string section_name) {

		pt.put(section_name + ".Type", "SliceGaussian");

		std::string str_spacing = stringUtils::num2str(m_surface_spacing[0]) + " " + stringUtils::num2str(m_surface_spacing[1]) + " " + stringUtils::num2str(m_surface_spacing[2]);
		pt.put(section_name + ".SurfSpacing", str_spacing);
		pt.put(section_name + ".SmoothSigma", m_smooth_sigma);
		pt.put(section_name + ".SigmaIn", m_sigma_in);
		pt.put(section_name + ".SigmaOut", m_sigma_out);
	}

	virtual std::string GetSampleTypeName() const {
		return std::string("SliceGaussian");
	}

public:
	vect3<double> m_surface_spacing;
	double m_smooth_sigma;

	double m_sigma_in;
	double m_sigma_out;
};


//////////////////////////////////////////////////////////////////////////

void UniformSampleObject::Sample(const mxImage<unsigned char>& mask, int num, Random& random, std::vector< vect3<double> >& pts)
{
	const unsigned char* mask_raw_data = mask.GetData();
	vect3<unsigned int> image_size = mask.GetImageSize();

	std::vector<size_t> bk_indexs, obj_indexs;
	size_t total = image_size[0] * image_size[1] * image_size[2];

	for (size_t i = 0; i < total; ++i)
	{
		if (mask_raw_data[i] > 0)
			obj_indexs.push_back(i);
		else
			bk_indexs.push_back(i);
	}

	int bk_num = num / 2;
	int obj_num = num - bk_num;
	for (int i = 0; i < bk_num; ++i)
	{
		size_t index = random.Next<size_t>(0, bk_indexs.size() - 1);
		vect3<int> voxel_index;
		voxel_index[2] = bk_indexs[index] / (image_size[0] * image_size[1]);
		voxel_index[1] = bk_indexs[index] % (image_size[0] * image_size[1]) / image_size[0];
		voxel_index[0] = bk_indexs[index] % (image_size[0] * image_size[1]) % image_size[0];

		vect3<double> pt;
		mxImageUtils::Voxel2World(mask, voxel_index, pt);
		pts.push_back(pt);
	}

	for (int i = 0; i < obj_num; ++i)
	{
		size_t index = random.Next<size_t>(0, obj_indexs.size() - 1);
		vect3<int> voxel_index;
		voxel_index[2] = obj_indexs[index] / (image_size[0] * image_size[1]);
		voxel_index[1] = obj_indexs[index] % (image_size[0] * image_size[1]) / image_size[0];
		voxel_index[0] = obj_indexs[index] % (image_size[0] * image_size[1]) % image_size[0];

		vect3<double> pt;
		mxImageUtils::Voxel2World(mask, voxel_index, pt);
		pts.push_back(pt);
	}
}

void MaskUniformSampleObject::Sample(const mxImage<unsigned char>& mask, int num, Random& random, std::vector< vect3<double> >& pts)
{
	assert_message(m_roi_mask != NULL, "organ mask is not set");
	assert_message(m_roi_mask->GetImageSize() == mask.GetImageSize(), "size should be same (mask, organ_mask)");
	assert_message(m_roi_rate >= 0 && m_roi_rate <= 1, "ROI portion must be (0,1)");

	vect3<double> spacing = mask.GetSpacing();
	vect3<unsigned int> image_size = mask.GetImageSize();

	// ROI and BK sample numbers
	int roi_num = static_cast<int>(num * m_roi_rate + 0.5), bk_num = num - roi_num;

	// 1) compute ROI region
	int band_voxel_len = static_cast<int>(m_band_len / mask.GetSpacing()[0] + 0.5);
	mxImage<unsigned char> band_mask;
	mxImageUtils::BinaryNarrowBand(*m_roi_mask, band_voxel_len, band_mask);

	// 2) get non-ROI index vector
	const unsigned char* mask_raw_data = mask.GetData();
	const unsigned char* roi_raw_data = band_mask.GetData();

	std::vector<size_t> bk_indexs;
	for (int z = 0; z < static_cast<int>(image_size[2]); ++z) {
		for (int y = 0; y < static_cast<int>(image_size[1]); ++y) {
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x) {

				size_t index = z * image_size[0] * image_size[1] + y * image_size[0] + x;
				if (mask_raw_data[index] != 0 && roi_raw_data[index] == 0)
					bk_indexs.push_back(index);
			}
		}
	}

	// 3) Gaussian Sampling on near-boundary region
	if (roi_num != 0)
	{
		mxImage<unsigned char> shrinked_mask;
		mxImageUtils::Copy(*m_roi_mask, shrinked_mask);

		vect3<int> pad_size(5, 5, 5);
		mxImageUtils::ShrinkMaskImage(shrinked_mask, pad_size);

		Surface surf;
		SurfaceUtils::IsoSurfaceFromImage2(shrinked_mask, m_smooth_sigma, m_surface_spacing[0], 128, surf);
		surf.UpdateNormals();
		ImageSampler::SamplePointsNearSurface(surf, roi_num, m_band_len / 2, m_band_len / 2, random, pts);
	}

	// 4) random sample voxels in non - ROI regions
	if (bk_num != 0 && bk_indexs.size() != 0)
	{
		for (int i = 0; i < bk_num; ++i)
		{
			size_t index = random.Next<size_t>(0, bk_indexs.size() - 1);
			vect3<int> voxel_index;
			voxel_index[2] = bk_indexs[index] / (image_size[0] * image_size[1]);
			voxel_index[1] = bk_indexs[index] % (image_size[0] * image_size[1]) / image_size[0];
			voxel_index[0] = bk_indexs[index] % (image_size[0] * image_size[1]) % image_size[0];

			vect3<double> pt;
			mxImageUtils::Voxel2World(mask, voxel_index, pt);
			pts.push_back(pt);
		}
	}
}

void GaussianBDSampleObject::Sample(const mxImage<unsigned char>& mask, int num, Random& random, std::vector< vect3<double> >& pts)
{
	// generate surface
	Surface surf;
	SurfaceUtils::IsoSurfaceFromImage2(mask, m_smooth_sigma, m_surface_spacing[0], 128, surf);
	surf.UpdateNormals();

	ImageSampler::SamplePointsNearSurface(surf, num, m_sigma_in, m_sigma_out, random, pts);
}

void SliceGaussianBDSampleObject::Sample(const mxImage<unsigned char>& mask, int num, Random& random, std::vector< vect3<double> >& pts)
{
	// generate surface
	Surface surf;
	SurfaceUtils::IsoSurfaceFromImage2(mask, m_smooth_sigma, m_surface_spacing[0], 128, surf);
	surf.UpdateNormals();

	ImageSampler::SamplePointsNearSurface_BetweenSlices(surf, mask, num, m_sigma_in, m_sigma_out, random, pts);
}


} } }


#endif
