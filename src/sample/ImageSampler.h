//
//  ImageSampler.h
//  FISH
//
//  Created by Yaozong Gao on 01/23/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __ImageSampler_h__
#define __ImageSampler_h__

#include "common/mxImageUtils.h"
#include "common/Random.h"
#include "mesh/surface/SurfaceUtils.h"

namespace BRIC { namespace IDEA { namespace FISH {

class ImageSampler
{
public:
	/** @brief sample points near surface (input as image) */
	static void SamplePointsNearSurface(const mxImage<unsigned char>& maskImage, const vect3<double>& spacing, unsigned char isoval, unsigned int num, double insigma, double outsigma, unsigned int smooth_iterations, Random& random, std::vector< vect3<double> >& points);

	/** @brief sample points near surface (input as surface) */
	static void SamplePointsNearSurface(const Surface& surf, unsigned int sample_num, double sample_insigma, double sample_outsigma, Random& random, std::vector< vect3<double> >& points);

	/** @brief sample points near surface between a slice range (input as surface) */
	static void SamplePointsNearSurface_BetweenSlices(const Surface& surf, const mxImage<unsigned char>& maskImage, unsigned int sample_num, double sample_insigma, double sample_outsigma, Random& random, std::vector< vect3<double> >& points);
};

//////////////////////////////////////////////////////////////////////////


void ImageSampler::SamplePointsNearSurface(const mxImage<unsigned char>& maskImage, const vect3<double>& spacing, unsigned char isoval, unsigned int num, double insigma, double outsigma, unsigned int smooth_iterations, Random& random, std::vector< vect3<double> >& points)
{
	//////////////////////////////////////////////////////////////////////////
	// Gaussian sampling scheme (more samples near the surface, less samples far away)
	mxImage<unsigned char> isoMaskImage;
	mxImageUtils::Resample(maskImage, spacing, isoMaskImage);

	Surface surf;
	SurfaceUtils::IsoSurfaceFromImage(isoMaskImage, surf, isoval);
	SurfaceUtils::SmoothSurface(surf, smooth_iterations);
	surf.UpdateNormals();

	//SurfaceUtils::SaveSurface(surf, "/Users/yzgao/Desktop/surf.vtk");
	//std::cout << "number of points: " << surf.GetPoints().size() / 3 << std::endl;
	//std::cout << "number of faces: " << surf.GetFaces().size() << std::endl;

	std::vector< vect3<double> > candidatePoints;
	SamplePointsNearSurface(surf, num, insigma, outsigma, random, candidatePoints);

	points.reserve(candidatePoints.size());
	for (unsigned int i = 0; i < candidatePoints.size(); ++i) {
		vect3<int> voxel;
		mxImageUtils::World2Voxel(maskImage, candidatePoints[i], voxel);

		if (!maskImage.PtInImage(voxel[0], voxel[1], voxel[2]))
			continue;

		points.push_back( candidatePoints[i] );
	}
}

void ImageSampler::SamplePointsNearSurface(const Surface& surf, unsigned int sample_num, double sample_insigma, double sample_outsigma, Random& random, std::vector< vect3<double> >& points)
{
	const std::vector< float >& verts = surf.GetPoints();
	const std::vector< vect3<float> >& normals = surf.GetNormals();
	unsigned int vert_num = verts.size() / 3;

	points.resize(sample_num);
	for (unsigned int i = 0; i < sample_num; ++i)
	{
		unsigned int vert_idx = random.Next<unsigned int>(0, vert_num - 1);
		vect3<float> vert(verts[vert_idx * 3], verts[vert_idx * 3 + 1], verts[vert_idx * 3 + 2]);

		vect3<float> point;

		double in_or_out = random.NextDouble() > 0.5;
		if (in_or_out)
		{
			double radius = std::abs(random.GaussianNextDouble(0, sample_insigma));
			point = vert - normals[vert_idx] * radius;
		}
		else {
			double radius = std::abs(random.GaussianNextDouble(0, sample_outsigma));
			point = vert + normals[vert_idx] * radius;
		}

		points[i][0] = point[0];
		points[i][1] = point[1];
		points[i][2] = point[2];
	}
}

void ImageSampler::SamplePointsNearSurface_BetweenSlices(const Surface& surf, const mxImage<unsigned char>& maskImage, unsigned int sample_num, double sample_insigma, double sample_outsigma, Random& random, std::vector< vect3<double> >& points)
{
	// find slice range
	unsigned int slice_sp = 0, slice_ep = 0;
	mxImageUtils::FindSliceRange(maskImage, slice_sp, slice_ep);

	const std::vector< float >& verts = surf.GetPoints();
	const std::vector< vect3<float> >& normals = surf.GetNormals();
	unsigned int vert_num = verts.size() / 3;

	points.resize(sample_num);
	for (unsigned int i = 0; i < sample_num; ++i)
	{
		vect3<float> point;

		do{
			unsigned int vert_idx = random.Next<unsigned int>(0, vert_num - 1);
			vect3<float> vert(verts[vert_idx * 3], verts[vert_idx * 3 + 1], verts[vert_idx * 3 + 2]);

			bool in_or_out = random.NextDouble() > 0.5;
			if (in_or_out)
			{
				double radius = std::abs(random.GaussianNextDouble(0, sample_insigma));
				point = vert - normals[vert_idx] * radius;
			}
			else {
				double radius = std::abs(random.GaussianNextDouble(0, sample_outsigma));
				point = vert + normals[vert_idx] * radius;
			}

			vect3<int> voxel_coord;
			mxImageUtils::World2Voxel(maskImage, point, voxel_coord);
			if (voxel_coord[2] >= static_cast<int>(slice_sp) && voxel_coord[2] <= static_cast<int>(slice_ep))
				break;

		} while (true);

		points[i][0] = point[0];
		points[i][1] = point[1];
		points[i][2] = point[2];
	}
}


} } }

#endif
