//
//  NearestPointIndexer.h
//  FISH
//
//  Created by Yaozong Gao on 4/19/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __NearestPointIndexer_h__
#define __NearestPointIndexer_h__


#include "itkVector.h"
#include "itkListSample.h"
#include "itkKdTreeGenerator.h"

#include "common/vect.h"

namespace BRIC { namespace IDEA { namespace FISH {
    

/** @brief 3D point indexer for nearest neighbor search
 *
 * Use KD-Tree inside to accelerate the search
 * DON'T use unsigned type, which will cause problems
 *
 */
template <typename T>
class NearestPointIndexer 
{
public:
    
    typedef itk::Vector<T,3> MeasurementVectorType;
	typedef itk::Statistics::ListSample< MeasurementVectorType > SampleType;
	typedef itk::Statistics::KdTreeGenerator< SampleType > TreeGeneratorType;
	typedef typename TreeGeneratorType::KdTreeType TreeType;
    
    /** @brief constructor */
    NearestPointIndexer() {
		m_kdTree = NULL;
		m_sample = NULL;
	}

	/** @brief destructor */
	~NearestPointIndexer() {
		Clear();
	}

	/** @brief clear */
	void Clear() {
		
		if(m_kdTree.IsNotNull())
			m_kdTree = NULL;
		if(m_sample.IsNotNull())
			m_sample = NULL;
	}

	/** @brief build KD-tree for given points */
    void Initialize(const std::vector< vect3<T> >& points) {
        
		m_sample = SampleType::New();
		m_sample->SetMeasurementVectorSize(3);

		// push into sample vector
		MeasurementVectorType mv;
		for (unsigned int i = 0; i < points.size(); ++i)
		{
			for (unsigned int j = 0; j < 3; ++j) {
				mv[j] = points[i][j]; 
			}
			m_sample->PushBack(mv);
		}

		// generate KD tree
		typename TreeGeneratorType::Pointer treeGenerator = TreeGeneratorType::New();
		treeGenerator->SetSample(m_sample);
		treeGenerator->SetBucketSize(16);
		treeGenerator->Update();

		m_kdTree = treeGenerator->GetOutput();
    }

	/** @brief build KD-tree for given points */
	void Initialize(const std::vector<T>& points) {

		unsigned int pt_num = static_cast<unsigned int>(points.size() / 3);

		m_sample = SampleType::New();
		m_sample->SetMeasurementVectorSize(3);

		// push into sample vector
		MeasurementVectorType mv;
		for (unsigned int i = 0; i < pt_num; ++i)
		{
			for (unsigned int j = 0; j < 3; ++j) {
				mv[j] = points[i*3+j];
			}
			m_sample->PushBack(mv);
		}

		// generate KD tree
		typename TreeGeneratorType::Pointer treeGenerator = TreeGeneratorType::New();
		treeGenerator->SetSample(m_sample);
		treeGenerator->SetBucketSize(16);
		treeGenerator->Update();

		m_kdTree = treeGenerator->GetOutput();
	}

    /** @brief search nearest neighbor for testing point */
	vect3<T> SearchNearestNeighbor( const vect3<T>& point ) const {

		if (m_kdTree.GetPointer() == NULL) {
			std::cerr << "uninitialized indexer" << std::endl;
			exit(-1);
		}
		
		MeasurementVectorType mv;
		for (unsigned int i = 0; i < 3; ++i)
			mv[i] = point[i];
		
		typename TreeType::InstanceIdentifierVectorType neighbors;
		unsigned int neighborNum = 1;
		m_kdTree->Search(mv, neighborNum, neighbors);

		if (neighbors.empty()) {
			std::cerr << "empty point set" << std::endl;
			exit(-1);
		}

		mv = m_kdTree->GetMeasurementVector(neighbors[0]);

		vect3<T> ret(mv[0], mv[1], mv[2]);
		return ret;
	}

	/** @brief get the index of nearest neighbor for testing point */
	size_t SearchNearestNeighborIndex(const vect3<T>& point) const {

		if (m_kdTree.GetPointer() == NULL) {
			std::cerr << "uninitialized indexer" << std::endl;
			exit(-1);
		}

		MeasurementVectorType mv;
		for (unsigned int i = 0; i < 3; ++i)
			mv[i] = point[i];

		typename TreeType::InstanceIdentifierVectorType neighbors;
		unsigned int neighborNum = 1;
		m_kdTree->Search(mv, neighborNum, neighbors);

		if (neighbors.empty()) {
			std::cerr << "empty point set" << std::endl;
			exit(-1);
		}

		return static_cast<size_t>(neighbors[0]);
	}

	/** @brief get indexed training sample */
	vect3<T> GetPoint(size_t index) const {

		MeasurementVectorType mv;
		mv = m_kdTree->GetMeasurementVector(index);

		vect3<T> ret( mv[0], mv[1], mv[2] );
		return ret;
	}

private:
    
	typename TreeType::Pointer m_kdTree;
	typename SampleType::Pointer m_sample;

};
    
    
    
    
} } }



#endif
