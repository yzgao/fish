//
//  BoostHelper.h
//  Fish
//
//  Created by Yaozong Gao on 9/20/13.
//
//

#ifndef __BoostHelper_h__
#define __BoostHelper_h__

#include "stdafx.h"
#include "common/stringUtils.h"
#include "common/aux_func.h"

namespace BRIC { namespace IDEA { namespace FISH {

class BoostCmdHelper
{
public:
	template <typename T>
	static T Get(boost::program_options::variables_map& vm, const char* name);

	static void ParseIdxOption(std::string str, std::vector<int>& indexes);
};


template <typename T>
T BoostCmdHelper::Get(boost::program_options::variables_map& vm, const char* name)
{
	T val; 
	if( vm.count(name) ) {
		val = vm[name].as<T>();
	}else {
		std::cerr << name << " option missing" << std::endl;
		exit(-1);
	}
	return val;
}


void BoostCmdHelper::ParseIdxOption(std::string str, std::vector<int>& indexes)
{
	std::vector<std::string> tokens = stringUtils::Split(str, ',');

	for (size_t i = 0; i < tokens.size(); ++i)
	{
		size_t j = tokens[i].find('-');
		if (j == std::string::npos)
		{
			indexes.push_back(stringUtils::str2num<int>(tokens[i]));
		}
		else
		{
			int sp = stringUtils::str2num<int>(tokens[i].substr(0, j));
			int ep = stringUtils::str2num<int>(tokens[i].substr(j + 1));

			if (sp > ep)
				err_message("sp must be <= ep");

			for (int k = sp; k <= ep; ++k)
				indexes.push_back(k);
		}
	}
}


} } }

#endif