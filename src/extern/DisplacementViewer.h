//
//  DisplacementViewer.h
//  FISH
//
//  Created by Yaozong Gao on 10/16/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __DisplacementViewer_h__
#define __DisplacementViewer_h__

#include "common/mxImage.h"
#include "common/mxImageUtils.h"
#include "common/stringUtils.h"

#include "itkImage.h"
#include "itkVectorImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImageRegionIterator.h"
#include "itkResampleImageFilter.h"
#include "itkIdentityTransform.h"


namespace BRIC { namespace IDEA { namespace FISH {

template <typename T>
class DisplacementViewer
{
public:

	typedef itk::Image<itk::RGBPixel<unsigned char>, 2> RGBImageType;

	DisplacementViewer() : m_slice(0, 0, 0), m_display_min(0), m_display_max(0), m_image(NULL), m_dispImage(NULL), m_width(0), m_height(0), m_arrowsize(1), m_stride(1), m_color("red") {
		m_pic = RGBImageType::New();
	}

	// set attributes
	void SetImage(mxImage<T>* image) { m_image = image; }
	void SetDisplacements(mxImage<double>* disp) { m_dispImage = disp; }
	void SetSliceX(int val) { m_slice = vect3<int>(val, 0, 0);  }
	void SetSliceY(int val) { m_slice = vect3<int>(0, val, 0);  }
	void SetSliceZ(int val) { m_slice = vect3<int>(0, 0, val);  }
	void SetDisplayRange(double min, double max) { m_display_min = min; m_display_max = max; }
	void SetPicWidth(int width) { m_width = width; }
	void SetColor(std::string color) { m_color = color; }
	void SetStride(int stride) { m_stride = stride;  }
	void SetArrowSize(int arrowsize) { m_arrowsize = arrowsize;  }

	// get results
	RGBImageType::Pointer GetPic() { return m_pic; }

	// produce displacement image
	void Plot();

private:

	// get a RGB slice
	void get_slice_x();
	void get_slice_y();
	void get_slice_z();
	void copy_array_to_pic(std::vector<unsigned char>& arr);

	void create_arrow_patch(int arrowsize, mxImage<unsigned char>& arrow_patch);
	itk::RGBPixel<unsigned char> get_rgb_color(std::string name);
	void plot_arrow(int x, int y, double dx, double dy, mxImage<unsigned char>& arrow_patch);
	itk::RGBPixel<unsigned char> jet_color(double v, double min, double max);
	void compute_disp_minmax();

	mxImage<T>* m_image;
	mxImage<double>* m_dispImage;

	vect3<int> m_slice;
	double m_display_min;
	double m_display_max;

	int m_width;	// use width to scaling ratio
	int m_height;

	int m_arrowsize;
	int m_stride;

	std::string m_color;

	RGBImageType::Pointer m_pic;
	std::vector< std::pair<double, double> > m_disp;
	std::vector< double > m_disp_norm;
	double m_disp_min; 
	double m_disp_max;
};


//////////////////////////////////////////////////////////////////////////

template <typename T>
void DisplacementViewer<T>::Plot()
{
	if (m_slice[1] == 0 && m_slice[2] == 0)
		get_slice_x();
	else if (m_slice[0] == 0 && m_slice[2] == 0)
		get_slice_y();
	else if (m_slice[0] == 0 && m_slice[1] == 0)
		get_slice_z();
	else
		err_message("unspecified slice index");

	mxImage<unsigned char> arrow_patch;
	create_arrow_patch(m_arrowsize, arrow_patch);

	for (int i = 0; i < m_height; i += m_stride) {
		for (int j = 0; j < m_width; j += m_stride) {

			int idx = i * m_width + j;
			double dx = m_disp[idx].first;
			double dy = m_disp[idx].second;

			if (abs(dx) < 1e-14 && abs(dy) < 1e-14)
			{
				//int dot_size = std::max(m_arrowsize / 5 - 1, 0);
				//itk::RGBPixel<unsigned char> pixel_val = jet_color(0, m_disp_min, m_disp_max);

				//for (int p = -dot_size; p <= dot_size; ++p)
				//	for (int q = -dot_size; q <= dot_size; ++q)
				//	{
				//		if (i + p < 0 || j + q < 0 || i + p >= m_height || j + q >= m_width)
				//			continue;
				//		itk::Index<2> itk_idx;
				//		itk_idx[0] = j + q; itk_idx[1] = i + p;
				//		m_pic->SetPixel(itk_idx, pixel_val);
				//	}
			}
			else
				plot_arrow(j, i, dx, dy, arrow_patch);
		}
	}
}

template <typename T>
void DisplacementViewer<T>::plot_arrow(int x, int y, double dx, double dy, mxImage<unsigned char>& arrow_patch)
{
	itk::RGBPixel<unsigned char> pixel_val = jet_color(m_disp_norm[y * m_width + x], m_disp_min, m_disp_max);
	itk::Size<2> size = m_pic->GetLargestPossibleRegion().GetSize();

	vect3<unsigned int> arrow_patch_size = arrow_patch.GetImageSize();
	int sp_x = x - arrow_patch_size[0] / 2;
	int ep_x = sp_x + arrow_patch_size[0] - 1;
	int sp_y = y - arrow_patch_size[1] / 2;
	int ep_y = sp_y + arrow_patch_size[1] - 1;

	if (sp_x < 0 || sp_y < 0 || ep_x >= static_cast<int>(size[0]) || ep_y >= static_cast<int>(size[1]))
		return;

	mxImage<unsigned char> rotated_arrow;
	vect3<double> voxel_center(arrow_patch_size[0] / 2, arrow_patch_size[1] / 2, 0);

	vect3<double> degrees;
	degrees[0] = degrees[1] = 0;
	degrees[2] = atan2(dy, dx) * 180 / M_PI;

	//mxImageUtils::Copy(arrow_patch, rotated_arrow);
	mxImageUtils::RotateImage(arrow_patch, voxel_center, degrees, rotated_arrow);

	#pragma omp parallel for
	for (int y = sp_y; y <= ep_y; ++y) {
		for (int x = sp_x; x <= ep_x; ++x) {

			int arrow_x = x - sp_x;
			int arrow_y = y - sp_y;

			itk::Index<2> itk_idx;
			itk_idx[0] = x;
			itk_idx[1] = y;

			unsigned char mask_val = rotated_arrow(arrow_x, arrow_y, 0);
			if (mask_val > 0) {

				itk::RGBPixel<unsigned char> final_val;
				itk::RGBPixel<unsigned char> orig_val = m_pic->GetPixel(itk_idx);

				double alpha = mask_val / 255.0;
				alpha = (alpha > 1) ? 1 : ((alpha < 0) ? 0 : alpha);

				for (int i = 0; i < 3; ++i)
					final_val[i] = static_cast<unsigned char>(pixel_val[i] * alpha + orig_val[i] * (1 - alpha) + 0.5);

				m_pic->SetPixel(itk_idx, final_val);
			}

		}
	}
}

template <typename T>
itk::RGBPixel<unsigned char> DisplacementViewer<T>::get_rgb_color(std::string name) {

	itk::RGBPixel<unsigned char> ret;

	if (name == "black") {
		ret[0] = ret[1] = ret[2] = 0;
	}
	else if (name == "white") {
		ret[0] = ret[1] = ret[2] = 25;
	}
	else if (name == "red") {
		ret[0] = 255; ret[1] = ret[2] = 0;
	}
	else if (name == "lime") {
		ret[0] = 0; ret[1] = 255; ret[2] = 0;
	}
	else if (name == "blue") {
		ret[0] = 0; ret[1] = 0; ret[2] = 255;
	}
	else if (name == "yellow") {
		ret[0] = 255; ret[1] = 255; ret[2] = 0;
	}
	else if (name == "cyan") {
		ret[0] = 0; ret[1] = 255; ret[2] = 255;
	}
	else if (name == "magenta") {
		ret[0] = 255; ret[1] = 0; ret[2] = 255;
	}
	else if (name == "silver") {
		ret[0] = 192; ret[1] = 192; ret[2] = 192;
	}
	else if (name == "gray") {
		ret[0] = 128; ret[1] = 128; ret[2] = 128;
	}
	else if (name == "maroon") {
		ret[0] = 128; ret[1] = 0; ret[2] = 0;
	}
	else if (name == "olive") {
		ret[0] = 128; ret[1] = 128; ret[2] = 0;
	}
	else if (name == "green") {
		ret[0] = 0; ret[1] = 128; ret[2] = 0;
	}
	else if (name == "purple") {
		ret[0] = 128; ret[1] = 0; ret[2] = 128;
	}
	else if (name == "teal") {
		ret[0] = 0; ret[1] = 128; ret[2] = 128;
	}
	else if (name == "navy") {
		ret[0] = 0; ret[1] = 0; ret[2] = 128;
	}
	else
		err_message("unrecognized color type");

	return ret;
}

template <typename T>
void DisplacementViewer<T>::create_arrow_patch(int arrowsize, mxImage<unsigned char>& arrow_patch)
{
	vect3<unsigned int> image_size;
	image_size[0] = arrowsize * 2 + 1;
	image_size[1] = arrowsize * 2 + 1;
	image_size[2] = 1;

	arrow_patch.SetImageSize(image_size);
	arrow_patch.Fill(0);

	for (unsigned int x = 0; x < image_size[0]; ++x)
		arrow_patch(x, arrowsize, 0) = 255;

	int px = image_size[0] - 1;
	int py = arrowsize;
	for (int dx = 0; dx < image_size[0] / 2; ++dx)
	{
		int x = px - dx;
		arrow_patch(x, py + dx, 0) = 255;
		arrow_patch(x, py - dx, 0) = 255;
	}

	int weight = arrowsize / 5;
	for (int i = 1; i < weight; ++i)
	{
		for (unsigned int x = 0; x < image_size[0] - i; ++x)
		{
			arrow_patch(x, arrowsize - i, 0) = 255;
			arrow_patch(x, arrowsize + i, 0) = 255;
		}

		for (int dx = 0; dx < image_size[0] / 2; ++dx)
		{
			int px = image_size[0] - 1 - (i - 1) * 2 - 1;
			int py = arrowsize;

			int x = px - dx;
			arrow_patch(x, py + dx, 0) = 255;
			arrow_patch(x, py - dx, 0) = 255;

			px = image_size[0] - 1 - i * 2;
			x = px - dx;
			arrow_patch(x, py + dx, 0) = 255;
			arrow_patch(x, py - dx, 0) = 255;
		}
	}
}

template <typename T>
void DisplacementViewer<T>::compute_disp_minmax()
{
	m_disp_min = std::numeric_limits<double>::max();
	m_disp_max = 0;

	for (size_t i = 0; i < m_disp_norm.size(); ++i)
	{
		if (m_disp_norm[i] > m_disp_max)
			m_disp_max = m_disp_norm[i];
		if (m_disp_norm[i] < m_disp_min)
			m_disp_min = m_disp_norm[i];
	}
}

template <typename T>
void DisplacementViewer<T>::get_slice_x()
{
	vect3<double> spacing = m_dispImage[0].GetSpacing();
	vect3<unsigned int> dims = m_dispImage[0].GetImageSize();
	vect3<double> origin = m_dispImage[0].GetOrigin();
	vect3<double> physical_sizes( spacing[0] * dims[0], spacing[1] * dims[1], spacing[2] * dims[2] );
	
	double stepsize = physical_sizes[1] / m_width;
	m_height = static_cast<int>(physical_sizes[2] / stepsize + 0.5);

	std::vector<unsigned char> data_buffer(m_width * m_height);
	m_disp.resize(m_width * m_height);
	m_disp_norm.resize(m_width * m_height);

	#pragma omp parallel for
	for (int i = 0; i < m_height; ++i) {

		ImageNearestNeighborInterpolator<T, double> interp;
		ImageNearestNeighborInterpolator< double, double > interp2;

		for (int j = 0; j < m_width; ++j) {
			
			vect3<double> pt = origin;
			pt[0] = m_image->GetOrigin()[0] + m_image->GetSpacing()[0] * m_slice[0];
			pt[1] += stepsize * j;
			pt[2] += stepsize * (m_height - 1 - i);

			vect3<double> img_voxel;
			mxImageUtils::World2Voxel(*m_image, pt, img_voxel);
			double val = interp.Get(*m_image, img_voxel);
			unsigned char out_val = (val <= m_display_min) ? 0 : (val >= m_display_max ? 255 : static_cast<unsigned char>((val - m_display_min) / (m_display_max - m_display_min) * 255) ) ;

			mxImageUtils::World2Voxel(m_dispImage[0], pt, img_voxel);
			double dy = interp2.Get(m_dispImage[1], img_voxel);
			double dz = interp2.Get(m_dispImage[2], img_voxel);

			int idx = i * m_width + j;
			data_buffer[idx] = out_val;
			m_disp[idx] = std::make_pair(dy, -dz);

			double dx = interp2.Get(m_dispImage[0], img_voxel);
			m_disp_norm[idx] = sqrt(dx*dx + dy*dy + dz*dz);
		}
	}

	copy_array_to_pic(data_buffer);
	compute_disp_minmax();
}

template <typename T>
void DisplacementViewer<T>::get_slice_y()
{
	vect3<double> spacing = m_dispImage[0].GetSpacing();
	vect3<unsigned int> dims = m_dispImage[0].GetImageSize();
	vect3<double> origin = m_dispImage[0].GetOrigin();
	vect3<double> physical_sizes(spacing[0] * dims[0], spacing[1] * dims[1], spacing[2] * dims[2]);

	double stepsize = physical_sizes[0] / m_width;
	m_height = static_cast<int>(physical_sizes[2] / stepsize + 0.5);

	std::vector<unsigned char> data_buffer(m_width * m_height);
	m_disp.resize(m_width * m_height);
	m_disp_norm.resize(m_width * m_height);

	#pragma omp parallel for
	for (int i = 0; i < m_height; ++i) {

		ImageNearestNeighborInterpolator<T, double> interp;
		ImageNearestNeighborInterpolator< double, double > interp2;

		for (int j = 0; j < m_width; ++j) {

			vect3<double> pt = origin;
			pt[0] += stepsize * j;
			pt[1] = m_image->GetOrigin()[1] + m_image->GetSpacing()[1] * m_slice[1];
			pt[2] += stepsize * (m_height - 1 - i);

			vect3<double> img_voxel;
			mxImageUtils::World2Voxel(*m_image, pt, img_voxel);
			double val = interp.Get(*m_image, img_voxel);
			unsigned char out_val = (val <= m_display_min) ? 0 : (val >= m_display_max ? 255 : static_cast<unsigned char>((val - m_display_min) / (m_display_max - m_display_min) * 255));

			mxImageUtils::World2Voxel(m_dispImage[0], pt, img_voxel);
			double dx = interp2.Get(m_dispImage[0], img_voxel);
			double dz = interp2.Get(m_dispImage[2], img_voxel);

			int idx = i * m_width + j;
			data_buffer[idx] = out_val;
			m_disp[idx] = std::make_pair(dx, -dz);

			double dy = interp2.Get(m_dispImage[1], img_voxel);
			m_disp_norm[idx] = sqrt(dx*dx + dy*dy + dz*dz);
		}
	}

	copy_array_to_pic(data_buffer); 
	compute_disp_minmax();
}

template <typename T>
void DisplacementViewer<T>::get_slice_z()
{
	vect3<double> spacing = m_dispImage[0].GetSpacing();
	vect3<unsigned int> dims = m_dispImage[0].GetImageSize();
	vect3<double> origin = m_dispImage[0].GetOrigin();
	vect3<double> physical_sizes(spacing[0] * dims[0], spacing[1] * dims[1], spacing[2] * dims[2]);

	double stepsize = physical_sizes[0] / m_width;
	m_height = static_cast<int>(physical_sizes[1] / stepsize + 0.5);

	std::vector<unsigned char> data_buffer(m_width * m_height);
	m_disp.resize(m_width * m_height);
	m_disp_norm.resize(m_width * m_height);

	#pragma omp parallel for
	for (int i = 0; i < m_height; ++i) {

		ImageNearestNeighborInterpolator<T, double> interp;
		ImageNearestNeighborInterpolator< double, double > interp2;

		for (int j = 0; j < m_width; ++j) {

			vect3<double> pt = origin;
			pt[0] += stepsize * j;
			pt[1] += stepsize * i;
			pt[2] = m_image->GetOrigin()[2] + m_image->GetSpacing()[2] * m_slice[2];

			vect3<double> img_voxel;
			mxImageUtils::World2Voxel(*m_image, pt, img_voxel);
			double val = interp.Get(*m_image, img_voxel);
			unsigned char out_val = (val <= m_display_min) ? 0 : (val >= m_display_max ? 255 : static_cast<unsigned char>((val - m_display_min) / (m_display_max - m_display_min) * 255));

			mxImageUtils::World2Voxel(m_dispImage[0], pt, img_voxel);
			double dx = interp2.Get(m_dispImage[0], img_voxel);
			double dy = interp2.Get(m_dispImage[1], img_voxel);

			int idx = i * m_width + j;
			data_buffer[idx] = out_val;
			m_disp[idx] = std::make_pair(dx, dy);

			double dz = interp2.Get(m_dispImage[2], img_voxel);
			m_disp_norm[idx] = sqrt(dx*dx + dy*dy + dz*dz);
		}
	}

	copy_array_to_pic(data_buffer);
	compute_disp_minmax();
}

template <typename T>
void DisplacementViewer<T>::copy_array_to_pic(std::vector<unsigned char>& arr)
{
	m_pic->Initialize();
	itk::ImageRegion<2> region;
	{
		itk::Index<2> idx;
		idx[0] = idx[1] = 0;

		itk::Size<2> size;
		size[0] = m_width;
		size[1] = m_height;

		region.SetIndex(idx);
		region.SetSize(size);
	}

	m_pic->SetRegions(region);
	m_pic->Allocate();

	typedef typename itk::ImageRegionIterator<RGBImageType> IteratorType;
	IteratorType it(m_pic, m_pic->GetLargestPossibleRegion());
	it.GoToBegin();

	unsigned int idx = 0;
	while (!it.IsAtEnd()) {

		itk::RGBPixel<unsigned char> pixel_value;
		pixel_value[0] = pixel_value[1] = pixel_value[2] = arr[idx];

		it.Set(pixel_value);
		++it; ++idx;
	}
}


template <typename T>
itk::RGBPixel<unsigned char> DisplacementViewer<T>::jet_color(double v, double vmin, double vmax)
{
	itk::RGBPixel<unsigned char> c; // white

	v = (v - vmin) / (vmax - vmin);
	if (v < 0) v = 0;
	if (v > 1) v = 1;

	int r = 0, g = 0, b = 0;
	if (v <= 0) {
		r = 0x00; g = 0x00; 
		b = 0x80;
	} 
	else if (v <= 0.1) {
		r = 0x00; g = 0x00;
		b = int(1270 * v + 128 + 0.5);
	}
	else if (v <= 0.36) {
		r = 0x00; 
		g = int((v - 0.1) / 0.26 * 255 + 0.5);
		b = 0xff;
	}
	else if (v <= 0.6) {
		r = int((v - 0.36) / 0.24 * 255 + 0.5);
		g = 0xff;
		b = int((0.36 - v) / 0.24 * 255 + 255 + 0.5);
	}
	else if (v <= 0.9) {
		r = 0xff;
		g = int((0.6 - v) / 0.3 * 255 + 255 + 0.5);
		b = 0x00;
	}
	else if (v <= 1.0) {
		r = int((0.9 - v) / 0.1 * 128 + 255 + 0.5);
		g = b = 0x00;
	}
	else {
		r = 0x80;
		g = b = 0x00;
	}

	c.SetRed(r > 255 ? 255 : (r < 0 ? 0 : r));
	c.SetGreen(g > 255 ? 255 : (g < 0 ? 0 : g));
	c.SetBlue(b > 255 ? 255 : (b < 0 ? 0 : b));

	//double dv;
	//if (v < vmin)
	//	v = vmin;
	//if (v > vmax)
	//	v = vmax;
	//dv = vmax - vmin;

	//double r = 1.0, g = 1.0, b = 1.0;
	//if (v < (vmin + 0.25 * dv)) {
	//	r = 0;
	//	g = 4 * (v - vmin) / dv;
	//}
	//else if (v < (vmin + 0.5 * dv)) {
	//	r = 0;
	//	b = 1 + 4 * (vmin + 0.25 * dv - v) / dv;
	//}
	//else if (v < (vmin + 0.75 * dv)) {
	//	r = 4 * (v - vmin - 0.5 * dv) / dv;
	//	b = 0;
	//}
	//else {
	//	g = 1 + 4 * (vmin + 0.75 * dv - v) / dv;
	//	b = 0;
	//}

	//vect3<int> cc;
	//cc[0] = static_cast<int>(r * 255 + 0.5);
	//cc[1] = static_cast<int>(g * 255 + 0.5);
	//cc[2] = static_cast<int>(b * 255 + 0.5);

	//c.SetRed(cc[0] > 255 ? 255 : (cc[0] < 0 ? 0 : cc[0]));
	//c.SetGreen(cc[1] > 255 ? 255 : (cc[1] < 0 ? 0 : cc[1]));
	//c.SetBlue(cc[2] > 255 ? 255 : (cc[2] < 0 ? 0 : cc[2]));

	return (c);
}


} } }


#endif