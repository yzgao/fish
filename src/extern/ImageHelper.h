//
//  ImageHelper.h
//  FISH
//
//  Created by Yaozong Gao on 4/17/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __ImageHelper_h__
#define __ImageHelper_h__


#include "common/mxImage.h"
#include "common/mxImageUtils.h"
#include "common/stringUtils.h"

#include "itkImage.h"
#include "itkVectorImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImageRegionIterator.h"
#include "itkResampleImageFilter.h"
#include "itkIdentityTransform.h"
#include "itkSignedMaurerDistanceMapImageFilter.h"
#include "itkDiscreteGaussianImageFilter.h"
#include "itkAntiAliasBinaryImageFilter.h"
#include "itkPermuteAxesImageFilter.h"

#include "extern/NearestPointIndexer.h"

namespace BRIC { namespace IDEA { namespace FISH {
    
class ImageHelper 
{
public:

	enum PixelType {
		UNKNOWN, UCHAR, CHAR, USHORT, SHORT, UINT, INT, ULONG, LONG, FLOAT, DOUBLE
	};

    /** @brief read image from file with ITK support */
    template <typename InputType, typename OutputType>
    static bool ReadImage( const char* path, mxImage<OutputType>& mxImg ) {
        
        typedef itk::Image<InputType,3> ImageType;
        typedef itk::ImageFileReader< ImageType > ReaderType;
        
        typename ReaderType::Pointer reader = ReaderType::New();
        
        reader->SetFileName(path);
        try {
            reader->Update();
        } catch ( const itk::ExceptionObject& exp ) {
            std::cerr << exp.GetDescription() << std::endl;
            return false;
        } 
        
        itk2mx<InputType,OutputType>( reader->GetOutput(), mxImg );
        
        return true;
    }
    
    /** @brief write image to file with ITK support */
    template <typename InputType, typename OutputType>
    static bool WriteImage( const mxImage<InputType>& mxImg, const char* path ) {
        
        typedef itk::Image<OutputType,3> ImageType;
        typedef itk::ImageFileWriter< ImageType > WriterType;
        
        typename ImageType::Pointer internalImage = ImageType::New();
        mx2itk<InputType,OutputType>(mxImg, internalImage);
        
        typename WriterType::Pointer writer = WriterType::New();
        
        writer->SetInput(internalImage);
        writer->SetFileName(path);
        try {
            writer->Update();
        } catch ( const itk::ExceptionObject& exp ) {
            std::cerr << exp.GetDescription() << std::endl;
            return false;
        } 
        
        return true;
    }

	/** @brief get color */
	static itk::RGBPixel<unsigned char> GetRGBColor(std::string name) {

		itk::RGBPixel<unsigned char> ret;

		if (name == "black") {
			ret[0] = ret[1] = ret[2] = 0;
		}
		else if (name == "white") {
			ret[0] = ret[1] = ret[2] = 25;
		}
		else if (name == "red") {
			ret[0] = 255; ret[1] = ret[2] = 0;
		}
		else if (name == "lime") {
			ret[0] = 0; ret[1] = 255; ret[2] = 0;
		}
		else if (name == "blue") {
			ret[0] = 0; ret[1] = 0; ret[2] = 255;
		}
		else if (name == "yellow") {
			ret[0] = 255; ret[1] = 255; ret[2] = 0;
		}
		else if (name == "cyan") {
			ret[0] = 0; ret[1] = 255; ret[2] = 255;
		}
		else if (name == "magenta") {
			ret[0] = 255; ret[1] = 0; ret[2] = 255;
		}
		else if (name == "silver") {
			ret[0] = 192; ret[1] = 192; ret[2] = 192;
		}
		else if (name == "gray") {
			ret[0] = 128; ret[1] = 128; ret[2] = 128;
		}
		else if (name == "maroon") {
			ret[0] = 128; ret[1] = 0; ret[2] = 0;
		}
		else if (name == "olive") {
			ret[0] = 128; ret[1] = 128; ret[2] = 0;
		}
		else if (name == "green") {
			ret[0] = 0; ret[1] = 128; ret[2] = 0;
		}
		else if (name == "purple") {
			ret[0] = 128; ret[1] = 0; ret[2] = 128;
		}
		else if (name == "teal") {
			ret[0] = 0; ret[1] = 128; ret[2] = 128;
		}
		else if (name == "navy") {
			ret[0] = 0; ret[1] = 0; ret[2] = 128;
		}
		else
			err_message("unrecognized color type");

		return ret;
	}


	/** @brief convert a mxImage sagittal slice to itk 2D image */
	template <typename T>
	static void ConvertXSliceToITK( const mxImage<T>& image, int slice_idx, double min, double max, itk::Image<itk::RGBPixel<unsigned char>,2>::Pointer itk_image ) {

		typedef itk::Image<itk::RGBPixel<unsigned char>, 2> itkImageType;
		vect3<unsigned int> image_size = image.GetImageSize();
		assert_message(slice_idx >= 0 && slice_idx < image_size[0], "out of bound : slice idx");

		std::vector<char> data_buffer(image_size[1] * image_size[2]);
		for (unsigned int z = 0; z < image_size[2]; ++z) {
			for (unsigned int y = 0; y < image_size[1]; ++y) {
				double val = static_cast<double>(image(slice_idx, y, z));
				unsigned int buffer_idx = (image_size[2] - 1 - z) * image_size[1] + y;
				unsigned char out_val = 0;
				if (val <= min)
					out_val = 0;
				else if (val >= max)
					out_val = 255;
				else 
					out_val = static_cast<unsigned char>( (val - min) / (max - min) * 255 );
				data_buffer[buffer_idx] = out_val;
			}
		}

		itk_image->Initialize();
		itk::ImageRegion<2> region;
		{
			itk::Index<2> idx;
			idx[0] = idx[1] = 0;
			
			itk::Size<2> size;
			size[0] = image_size[1];
			size[1] = image_size[2];

			region.SetIndex(idx);
			region.SetSize(size);
		}

		itk_image->SetRegions(region);
		itk_image->Allocate();

		typedef typename itk::ImageRegionIterator<itkImageType> IteratorType;
		IteratorType it(itk_image, itk_image->GetLargestPossibleRegion());
		it.GoToBegin();

		unsigned int idx = 0;
		while (!it.IsAtEnd()) {
			
			itk::RGBPixel<unsigned char> pixel_value;
			pixel_value[0] = pixel_value[1] = pixel_value[2] = data_buffer[idx];

			it.Set(pixel_value);
			++it; ++idx;
		}
	}

	static void CreateArrowPatch(int arrowsize, mxImage<unsigned char>& arrow_patch)
	{
		vect3<unsigned int> image_size;
		image_size[0] = arrowsize * 2 + 1;
		image_size[1] = arrowsize * 2 + 1;
		image_size[2] = 1;

		arrow_patch.SetImageSize(image_size);
		arrow_patch.Fill(0);

		for (unsigned int x = 0; x < image_size[0]; ++x)
			arrow_patch(x, arrowsize, 0) = 255;

		int px = image_size[0] - 1; 
		int py = arrowsize;
		for (int dx = 0; dx < image_size[0] / 2; ++dx)
		{
			int x = px - dx; 
			arrow_patch(x, py + dx, 0) = 255;
			arrow_patch(x, py - dx, 0) = 255;
		}

		int weight = arrowsize / 5;
		for (int i = 1; i < weight; ++i)
		{
			for (unsigned int x = 0; x < image_size[0] - i; ++x)
			{
				arrow_patch(x, arrowsize - i, 0) = 255;
				arrow_patch(x, arrowsize + i, 0) = 255;
			}

			for (int dx = 0; dx < image_size[0] / 2; ++dx)
			{
				int px = image_size[0] - 1 - (i - 1) * 2 - 1;
				int py = arrowsize;

				int x = px - dx;
				arrow_patch(x, py + dx, 0) = 255;
				arrow_patch(x, py - dx, 0) = 255;

				px = image_size[0] - 1 - i * 2;
				x = px - dx;
				arrow_patch(x, py + dx, 0) = 255;
				arrow_patch(x, py - dx, 0) = 255;
			}

		}
	}


	/** @brief plot sagittal displacement field */
	template <typename T>
	static void PlotXDisp(const mxImage<T>& image, std::vector< mxImage<double> >& disp_maps, int slice_idx, double min, double max, int arrow_size, int stride_x, int stride_y, std::string color, itk::Image<itk::RGBPixel<unsigned char>, 2>::Pointer slice)
	{
		assert_message(disp_maps.size() == 3, "must have three displacement maps");

		ConvertXSliceToITK(image, slice_idx, min, max, slice);

		mxImage<unsigned char> arrow_patch;
		CreateArrowPatch(arrow_size, arrow_patch);

		vect3<unsigned int> image_size = image.GetImageSize();
		for (unsigned int z = 0; z < image_size[2]; z += stride_y) {
			for (unsigned int y = 0; y < image_size[1]; y += stride_x) {

				vect3<double> pos(slice_idx, y, z);
				mxImageUtils::Voxel2World(image, pos);

				vect3<int> voxel;
				mxImageUtils::World2Voxel(disp_maps[0], pos, voxel);

				if (!disp_maps[0].PtInImage(voxel[0], voxel[1], voxel[2]))
					continue;

				double dx = disp_maps[1](voxel[0], voxel[1], voxel[2]);
				double dy = disp_maps[2](voxel[0], voxel[1], voxel[2]);

				if (abs(dx) < 1e-14 && abs(dy) < 1e-14)
				{
					itk::Index<2> itk_idx;
					itk_idx[0] = y; itk_idx[1] = image_size[2] - 1 - z;

					itk::RGBPixel<unsigned char> pixel_val = GetRGBColor(color);
					slice->SetPixel(itk_idx, pixel_val);
				}
				else
					PlotArrow(slice, y, image_size[2] - 1 - z, dx, -dy, arrow_patch, color);
			}
		}

	}

	/** @brief plot an arrow on a pixel */
	static void PlotArrow(itk::Image<itk::RGBPixel<unsigned char>,2>::Pointer itk_image, int x, int y, double dx, double dy, const mxImage<unsigned char>& arrow_patch, std::string color)
	{
		itk::RGBPixel<unsigned char> pixel_val = GetRGBColor(color);
		itk::Size<2> size = itk_image->GetLargestPossibleRegion().GetSize();

		vect3<unsigned int> arrow_patch_size = arrow_patch.GetImageSize();
		int sp_x = x - arrow_patch_size[0] / 2;
		int ep_x = sp_x + arrow_patch_size[0] - 1;
		int sp_y = y - arrow_patch_size[1] / 2;
		int ep_y = sp_y + arrow_patch_size[1] - 1;

		if (sp_x < 0 || sp_y < 0 || ep_x >= static_cast<int>(size[0]) || ep_y >= static_cast<int>(size[1]))
			return;

		mxImage<unsigned char> rotated_arrow;
		vect3<double> voxel_center( arrow_patch_size[0] / 2, arrow_patch_size[1] / 2, 0 );

		vect3<double> degrees;
		degrees[0] = degrees[1] = 0;
		degrees[2] = atan2(dy, dx) * 180 / M_PI;

		//mxImageUtils::Copy(arrow_patch, rotated_arrow);
		mxImageUtils::RotateImage(arrow_patch, voxel_center, degrees, rotated_arrow);

		#pragma omp parallel for
		for (int y = sp_y; y <= ep_y; ++y) {
			for (int x = sp_x; x <= ep_x; ++x) {
			
				int arrow_x = x - sp_x;
				int arrow_y = y - sp_y;

				itk::Index<2> itk_idx;
				itk_idx[0] = x;
				itk_idx[1] = y;

				unsigned char mask_val = rotated_arrow(arrow_x, arrow_y, 0);
				if (mask_val > 0) {

					itk::RGBPixel<unsigned char> final_val;
					itk::RGBPixel<unsigned char> orig_val = itk_image->GetPixel(itk_idx);

					double alpha = mask_val / 255.0;
					alpha = (alpha > 1) ? 1 : ((alpha < 0) ? 0 : alpha);

					for (int i = 0; i < 3; ++i)
						final_val[i] = static_cast<unsigned char>( pixel_val[i] * alpha + orig_val[i] * (1 - alpha) + 0.5 );

					itk_image->SetPixel(itk_idx, final_val);
				}

			}
		}
	}

    
	/** @brief plot a sagittal contour on the slice */
	static void PlotXSliceContour( const mxImage<unsigned char>& seg, int slice_idx, std::string color, int linesize, itk::Image<itk::RGBPixel<unsigned char>,2>::Pointer itk_image ) {

		itk::Size<2> size2d = itk_image->GetLargestPossibleRegion().GetSize();
		vect3<unsigned int> image_size = seg.GetImageSize();

		assert_message(slice_idx >= 0 && slice_idx < static_cast<int>(image_size[0]), "slice idx out of bound");
		assert_message( size2d[0] == image_size[1], "segmentation and image size mismatch" );
		assert_message( size2d[1] == image_size[2], "segmentation and image size mismatch" );

		std::vector< std::pair<int, int> > edge_voxels;
		for (unsigned int z = 0; z < image_size[2]; ++z) {
			for (unsigned int y = 0; y < image_size[1]; ++y) {

				if (seg(slice_idx, y, z) == 0)
					continue;

				bool is_edge = false;
				for (int dz = -1; dz <= 1; ++dz) {
					for (int dy = -1; dy <= 1; ++dy) {
						int nb_z = static_cast<int>(z) + dz;
						int nb_y = static_cast<int>(y) + dy;

						if (nb_z < 0 || nb_z >= static_cast<int>(image_size[2]) || nb_y < 0 || nb_y >= static_cast<int>(image_size[1])) {
							is_edge = true; break;
						}

						if (seg(slice_idx, nb_y, nb_z) == 0) {
							is_edge = true; break;
						}
					}
					if (is_edge) break;
				}

				if (is_edge)
					edge_voxels.push_back( std::make_pair(y, image_size[2] - 1 - z) );
			}
		}

		// thicken line
		if (linesize > 1)
		{
			mxImage<unsigned char> tmp_mask;
			vect3<unsigned int> tmp_size(size2d[0], size2d[1], 1);
			tmp_mask.SetImageSize(tmp_size);
			tmp_mask.Fill(0);

			for (size_t i = 0; i < edge_voxels.size(); ++i)
				tmp_mask(edge_voxels[i].first, edge_voxels[i].second, 0) = 255;

			unsigned char foreground_val = 255;
			for (int i = 0; i < linesize - 1; ++i)
				mxImageUtils::BinaryDilate2D(tmp_mask, 0, foreground_val);

			edge_voxels.clear();
			for (unsigned int y = 0; y < size2d[1]; ++y) {
				for (unsigned int x = 0; x < size2d[0]; ++x) {
					if (tmp_mask(x, y, 0) > 0)
						edge_voxels.push_back(std::make_pair(x, y));
				}
			}
		}

		itk::RGBPixel<unsigned char> pixel_value = GetRGBColor(color);
		for (size_t i = 0; i < edge_voxels.size(); ++i)
		{
			itk::Index<2> idx;
			idx[0] = edge_voxels[i].first; idx[1] = edge_voxels[i].second;
			itk_image->SetPixel(idx, pixel_value);
		}
	}

	/** @brief write itk image slice */
	template <typename T>
	static bool WriteItkImage( typename T::Pointer itk_image, const char* path )
	{
		typedef itk::ImageFileWriter<T> WriterType;
		typename WriterType::Pointer writer = WriterType::New();

		writer->SetInput(itk_image);
		writer->SetFileName(path);
		try{
			writer->Update();
		}
		catch ( const itk::ExceptionObject& exp ) {
			std::cerr << exp.GetDescription() << std::endl;
			return false;
		}
		return true;
	}

    /** @brief read and resample image with ITK support */
    template <typename InputType, typename OutputType>
    static bool ReadImageWithResample( const char* path, const vect3<double>& outputSpacing, mxImage<OutputType>& mxImg ) 
    {
        typedef itk::Image<InputType,3> ImageType;
        typedef itk::ImageFileReader< ImageType > ReaderType;
        
        typename ReaderType::Pointer reader = ReaderType::New();
        
        reader->SetFileName(path);
        try {
            reader->Update();
        } catch ( const itk::ExceptionObject& exp ) {
            std::cerr << exp.GetDescription() << std::endl;
            return false;
        } 
        
        itk::Vector<double,3> internalOutputSpacing;
        for (unsigned int i = 0; i < 3; ++i) 
            internalOutputSpacing[i] = outputSpacing[i];
        
        typename ImageType::Pointer resampledImage = _ResampleImage<InputType>(reader->GetOutput(), internalOutputSpacing);
        
        itk2mx<InputType,OutputType>( resampledImage, mxImg );
        
        return true;
    }
    
    /** @brief resample image with ITK support */
    template <typename T>
    static void ResampleImage(const mxImage<T>& inputImage, const vect3<double>& outputSpacing, mxImage<T>& outputImage) 
	{
        typedef itk::Image<T,3> ImageType;
        typename ImageType::Pointer internalInputImage = ImageType::New();
        mx2itk<T,T>(inputImage, internalInputImage);
        
        itk::Vector<double,3> internalOutputSpacing;
        for (unsigned int i = 0; i < 3; ++i)
            internalOutputSpacing[i] = outputSpacing[i];
        
        typename ImageType::Pointer internalOutputImage = _ResampleImage<T>(internalInputImage, internalOutputSpacing);
        
        itk2mx<T,T>(internalOutputImage, outputImage);
	}
    
    /** @brief generate a distance image from binary image with ITK support */
    template <typename T>
    static void DistanceImage(const mxImage<T>& inputImage, mxImage<float>& distanceImage) 
    {
        typedef itk::Image<T,3> ImageType;
        typedef itk::Image<float,3> DistanceImageType;
        
        typename ImageType::Pointer image = ImageType::New();
        mx2itk<T,T>(inputImage, image);
        
        typedef itk::SignedMaurerDistanceMapImageFilter< ImageType, DistanceImageType > FilterType;
        typename FilterType::Pointer filter = FilterType::New();
        
        filter->SetInput(image);
        filter->UseImageSpacingOn();
        filter->SquaredDistanceOff();
        filter->Update();

        itk2mx<float,float>(filter->GetOutput(), distanceImage);
    }


	/** @brief generate an anti-alias distance map from a binary image with ITK support */
	template <typename T>
	static void DistanceImage_with_AntiAlias(const mxImage<T>& inputImage, mxImage<float>& distanceImage)
	{
		typedef itk::Image<T, 3> ImageType;
		typedef itk::Image<float, 3> DistanceImageType;

		typename ImageType::Pointer image = ImageType::New();
		mx2itk<T, T>(inputImage, image);

		typedef itk::AntiAliasBinaryImageFilter< ImageType, DistanceImageType > FilterType;
		typename FilterType::Pointer filter = FilterType::New();

		filter->SetInput(image);
		filter->Update();

		itk2mx<float, float>(filter->GetOutput(), distanceImage);
	}

	/** @brief read image header */
	template <typename T>
	static bool ReadImageHeader(const char* inputPath, mxImage<T>& image)
	{
		boost::filesystem::path path(inputPath);
		if (!boost::filesystem::exists(path)) {
			std::cerr << "the file doesn't exist: " << inputPath << std::endl;
			return false;
		}

		itk::ImageIOBase::Pointer imageIO = itk::ImageIOFactory::CreateImageIO(inputPath, itk::ImageIOFactory::ReadMode);

		imageIO->SetFileName(inputPath);
		imageIO->ReadImageInformation();

		const size_t nDims = imageIO->GetNumberOfDimensions();
		if(nDims != 3) {
			std::cerr << "the image dimension is " << nDims << std::endl;
			return false;
		}

		// spacing
		vect3<double> spacing;
		for (unsigned int i = 0; i < nDims; ++i)
			spacing[i] = imageIO->GetSpacing(i);
		image.SetSpacing(spacing);

		// sizes
		vect3<unsigned int> size;
		for (unsigned int i = 0; i < nDims; ++i) 
			size[i] = imageIO->GetDimensions(i);
		image.SetRawImageSize(size);

		// origin
		vect3<double> origin;
		for (unsigned int i = 0; i < nDims; ++i)
			origin[i] = imageIO->GetOrigin(i);
		image.SetOrigin(origin);

		// direction
		std::vector<double> x_direction = imageIO->GetDirection(0);
		std::vector<double> y_direction = imageIO->GetDirection(1);
		std::vector<double> z_direction = imageIO->GetDirection(2);
		std::vector<double> directions;
		directions.resize(9);
		for (unsigned int i = 0; i < nDims; ++i)
		{
			directions[i] = x_direction[i];
			directions[3+i] = y_direction[i];
			directions[6+i] = z_direction[i];
		}
		image.SetAxis(&directions[0]);

		return true;
	}

	/** @brief read pixel type */
	static PixelType ReadPixelType(const char* path) {

		typedef itk::ImageIOBase::IOComponentType ScalarPixelType;
		itk::ImageIOBase::Pointer imageIO = itk::ImageIOFactory::CreateImageIO(path, itk::ImageIOFactory::ReadMode);
		if (imageIO.IsNull()) {
			std::cerr << "image file does not exist: " << path << std::endl; exit(-1);
		}

		imageIO->SetFileName(path);
		imageIO->ReadImageInformation();

		if (imageIO->GetPixelType() != itk::ImageIOBase::SCALAR) {
			std::cerr << "the pixel type is not a scalar" << std::endl; exit(-1);
		}

		const ScalarPixelType pixelType = imageIO->GetComponentType();
		switch (pixelType)
		{
		case itk::ImageIOBase::UCHAR: return UCHAR; break;
		case itk::ImageIOBase::CHAR: return CHAR; break;
		case itk::ImageIOBase::USHORT: return USHORT; break;
		case itk::ImageIOBase::SHORT: return SHORT; break;
		case itk::ImageIOBase::UINT: return UINT; break;
		case itk::ImageIOBase::INT: return INT; break;
		case itk::ImageIOBase::ULONG: return ULONG; break;
		case itk::ImageIOBase::LONG: return LONG; break;
		case itk::ImageIOBase::FLOAT: return FLOAT; break;
		case itk::ImageIOBase::DOUBLE: return DOUBLE; break;
		default:
			return UNKNOWN;
			break;
		}
	}

	/** @brief Contour data to mask volume */
	template <typename T1, typename T2>
	static void RTTSContourData2MaskVolume(const char* path, const mxImage<T1>& refImage, mxImage<T2>& maskImage)
	{
		// read contour points out
		std::vector<std::string> lines;
		if (!stringUtils::ReadLines(path, lines)) {
			std::cerr << "fails to read line from " << path << std::endl;
			return;
		}

		int lineIdx = 0;
		int numContours = stringUtils::str2num<int>(lines[lineIdx++]);
		
		std::vector< std::vector< vect3<double> > > contours;
		contours.resize(numContours);

		for (int i = 0; i < numContours; ++i)
		{
			if (static_cast<size_t>(lineIdx) >= lines.size()) {
				std::cerr << "corrupted file" << std::endl; exit(-1);
			}

			int numPts = stringUtils::str2num<int>(lines[lineIdx++]);
			contours[i].resize(numPts);

			for (int j = 0; j < numPts; ++j)
				contours[i][j] = stringUtils::ParseVect3<double>(lines[lineIdx+j]);
			lineIdx += numPts;
		}

		// process each slice-contour 
		maskImage.CopyImageInfo(refImage);
		maskImage.SetImageSize(refImage.GetImageSize());
		maskImage.Fill(0);

		for (size_t i = 0; i < contours.size(); ++i)
		{
			if (contours[i].size() == 0) {
				std::cerr << "empty contour" << std::endl; exit(-1);
			}

			vect3<int> tmp_voxel;
			mxImageUtils::World2Voxel(maskImage, contours[i][0], tmp_voxel);
			int sliceIdx = tmp_voxel[2];

			std::cout << "slice: " << sliceIdx << std::endl;

			for (size_t j = 0; j < contours[i].size(); ++j)
			{
				// first draw the contour on each slice
				size_t nextIdx = (j+1) % contours[i].size();
				vect3<double> pt = contours[i][j], nextPt = contours[i][nextIdx];

				mxImageUtils::World2Voxel(maskImage, pt, tmp_voxel);
				if (tmp_voxel[2] != sliceIdx) {
					std::cerr << "contour points are not located on the same slice" << std::endl; exit(-1);
				}

				vect3<double> direct = nextPt - pt;
				double norm = direct.norm();
				if (std::abs(norm) <= VECT_EPSILON)
					continue;
				direct = direct / norm;
				
				double stepsize = 0.1;
				for (double len = 0; len < norm; len += stepsize) {
					vect3<double> tmp = pt + direct * len;
					mxImageUtils::World2Voxel(maskImage, tmp, tmp_voxel);

					if (!maskImage.PtInImage(tmp_voxel[0], tmp_voxel[1], tmp_voxel[2])) {
						std::cerr << "point not in the image domain" << std::endl; exit(-1);
					}
					
					maskImage(tmp_voxel[0], tmp_voxel[1], tmp_voxel[2]) = 255;
				}

				// fill in the hole on each slice
				mxImageUtils::BinaryFillHoles2D(maskImage, sliceIdx);
			}
		}
	}

	template <typename T>
	static void PermuteAxes(const mxImage<T>& refImage, const std::vector<unsigned int>& o, mxImage<T>& outImage)
	{
		vect3<unsigned int> image_size = refImage.GetImageSize();
		vect3<unsigned int> out_image_size;
		int flag = 0;

		out_image_size[0] = image_size[o[0]];
		out_image_size[1] = image_size[o[1]];
		out_image_size[2] = image_size[o[2]];

		outImage.CopyImageInfo(refImage);
		outImage.SetImageSize(out_image_size);
		outImage.Fill(0);

		#pragma omp parallel for
		for (int z = 0; z < out_image_size[2]; ++z)
		{
			for (unsigned int y = 0; y < out_image_size[1]; ++y)
			{
				for (unsigned int x = 0; x < out_image_size[0]; ++x)
				{
					vect3<unsigned int> out_coord(x, y, z);
					vect3<unsigned int> in_coord;

					in_coord[o[0]] = out_coord[0];
					in_coord[o[1]] = out_coord[1];
					in_coord[o[2]] = out_coord[2];

					outImage(x, y, z) = refImage(in_coord[0], in_coord[1], in_coord[2]);
				}
			}
		}
	}

private:
    
    // resample image (itk)
    template <typename T>
    static typename itk::Image<T,3>::Pointer _ResampleImage(typename itk::Image<T,3>::Pointer inputImage, const itk::Vector<double,3>& outputSpacing)
    {
        typedef itk::Image<T,3> ImageType;
        
        itk::Vector<double,3> inputSpacing = inputImage->GetSpacing();
        typename ImageType::SizeType inputSize = inputImage->GetLargestPossibleRegion().GetSize();
        
		itk::Size<3> outputSize;
		for(unsigned int i = 0; i < 3; ++i)
			outputSize[i] = static_cast<unsigned int>( inputSpacing[i] * inputSize[i] / outputSpacing[i] + 0.5 );
        
		typedef itk::IdentityTransform<double, 3> TransformType;
		typedef itk::ResampleImageFilter<ImageType,ImageType> ResampleFilterType;
		typename ResampleFilterType::Pointer resampleFilter = ResampleFilterType::New();
        
		resampleFilter->SetInput(inputImage);
		resampleFilter->SetDefaultPixelValue(0);
		resampleFilter->SetSize(outputSize);
		resampleFilter->SetOutputSpacing(outputSpacing);
		resampleFilter->SetTransform(TransformType::New());
		resampleFilter->SetOutputDirection(inputImage->GetDirection());
		resampleFilter->SetOutputOrigin(inputImage->GetOrigin());	
		resampleFilter->UpdateLargestPossibleRegion();
		
        return resampleFilter->GetOutput();
    }
    
public:
    
    // used internally
    // conversion between itk::Image and mxImage
    
    /** @brief conversion between itkImage and mxImage */
    template <typename InPixelType, typename OutPixelType>
    static void itk2mx(const typename itk::Image<InPixelType,3>::Pointer itkImg, mxImage<OutPixelType>& mxImg) {
        
        typedef typename itk::Image<InPixelType,3> itkImageType;
        
        mxImg.Clear();
        
        itk::Size<3> size = itkImg->GetLargestPossibleRegion().GetSize();
        itk::Point<double,3> origin = itkImg->GetOrigin();
        itk::Vector<double,3> spacing = itkImg->GetSpacing();
        typename itkImageType::DirectionType direction = itkImg->GetDirection();
        
        vect3<unsigned int> imageSize;
        vect3<double> imageOrigin, imageSpacing;
        double imageAxis[9] = {0};
        
        for (unsigned int i = 0; i < 3; ++i) {
            imageSize[i] = static_cast<unsigned int>( size[i] );
            imageOrigin[i] = origin[i];
            imageSpacing[i] = spacing[i];
        }

        for (unsigned int col = 0; col < 3; ++col) {
            for (unsigned int row = 0; row < 3; ++row) {
                imageAxis[col*3+row] = direction[row][col];
            }
        }
        
        mxImg.SetImageSize( imageSize );
        mxImg.SetOrigin( imageOrigin );
        mxImg.SetSpacing( imageSpacing );
        mxImg.SetAxis( imageAxis );
        
        OutPixelType* data = mxImg.GetData();
        
        typedef typename itk::ImageRegionIterator<itkImageType> IteratorType;
        IteratorType it(itkImg, itkImg->GetLargestPossibleRegion());
        it.GoToBegin();
        
        unsigned int idx = 0;
        while (! it.IsAtEnd() ) {
            data[idx] = static_cast<OutPixelType>(it.Get());
            ++ it; ++idx;
        }
    }
    
    /** @brief conversion between mxImage and itkImage */
    template <typename InPixelType, typename OutPixelType>
    static void mx2itk(const mxImage<InPixelType>& mxImg, typename itk::Image<OutPixelType,3>::Pointer itkImg) {
        
        typedef typename itk::Image<OutPixelType,3> itkImageType;
        
        itkImg->Initialize();
        
        itk::ImageRegion<3> region;
        {
            itk::Index<3> idx;
            idx[0] = idx[1] = idx[2] = 0;
            itk::Size<3> size;
            
            vect3<unsigned int> imageSize = mxImg.GetImageSize();
            for (unsigned int i = 0; i < 3; ++i)
                size[i] = imageSize[i];
            
            region.SetIndex(idx);
            region.SetSize(size);
        }
        
        itkImg->SetRegions(region);
        itkImg->Allocate();
        
        typedef typename itk::ImageRegionIterator<itkImageType> IteratorType;
        IteratorType it(itkImg, itkImg->GetLargestPossibleRegion());
        it.GoToBegin();
        
        unsigned int idx = 0;
        const InPixelType* data = mxImg.GetData();
        
        while (! it.IsAtEnd() ) {
            it.Set(static_cast<OutPixelType>(data[idx]));
            ++ it; ++ idx;
        }
        
        // set origin
        itk::Point<double,3> origin;
        vect3<double> imageOrigin = mxImg.GetOrigin();
        for (unsigned int i = 0; i < 3; ++i)
            origin[i] = imageOrigin[i];
        itkImg->SetOrigin(origin);
        
        // set spacing
        itk::Vector<double,3> spacing;
        vect3<double> imageSpacing = mxImg.GetSpacing();
        for (unsigned int i = 0; i < 3; ++i) 
            spacing[i] = imageSpacing[i];
        itkImg->SetSpacing(spacing);
        
        // set direction
        const double* imageAxis = mxImg.GetAxis();
        typename itkImageType::DirectionType direction;
        for (unsigned int col = 0; col < 3; ++col) {
            for (unsigned int row = 0; row < 3; ++row) {
                direction[row][col] = imageAxis[col*3+row];
            }
        }
        itkImg->SetDirection(direction);
    }
};


    
} } }


#endif
