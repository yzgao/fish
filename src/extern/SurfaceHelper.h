
//
// surfaceUtils Class 
// contain surface utilities
// Written by Yaozong Gao
// 
// email: gaoking132@gmail.com
// Date: Nov 06 2012
//
// Revisions:
//  
// 2012-11-07 Fix a bug in ISOSurfaceFromImage function
// 

#ifndef __surfaceUtils_h__
#define __surfaceUtils_h__

#include <string>
#include <limits>

#include "surface/surface.h"

// ITK headers
#include "itkMesh.h"
#include "itkVTKPolyDataReader.h"
#include "itkVTKPolyDataWriter.h"
#include "itkRGBAPixel.h"
#include "itkTriangleMeshToBinaryImageFilter.h"


// VTK headers
#include "vtkPolyData.h"
#include "vtkCellArray.h"
#include "extern/itkImageToVTKImageFilter.h"
#include "vtkSmartPointer.h"
#include "vtkMarchingCubes.h"
#include "vtkWindowedSincPolyDataFilter.h"


namespace BRIC { namespace IDEA { namespace FISH {

//
// Surface Utils class
//

class SurfaceHelper
{
public:

	// Mesh Type Definition
    static const unsigned int PointDimension = 3;   // 3D space
    static const unsigned int MaxCellDimension = 2; // surface
    typedef unsigned char PixelType;                // dummy type, not used
    typedef unsigned char CellDataType;             // dummy type, not used
    typedef float CoordRepType;                     // float coordinates
    typedef double InterpolationWeightType;         // use double to do interpolation
    
    typedef itk::DefaultStaticMeshTraits< PixelType, PointDimension, MaxCellDimension, CoordRepType, InterpolationWeightType, CellDataType > MeshTraits;
    typedef itk::Mesh< PixelType, PointDimension, MeshTraits > MeshType;
    
    
    /////////////////////////////////////////////////////////////////////////////////////////
    //
    // Surface IO (Load Surface) (without updating nbrs)
    //
    /////////////////////////////////////////////////////////////////////////////////////////
    
    static bool LoadSurfaceFromVTK(const char* vtkFilename, Surface& surface)
    {
        typedef itk::VTKPolyDataReader<MeshType> MeshReaderType;
        MeshReaderType::Pointer meshReader = MeshReaderType::New();
        
        meshReader->SetFileName(vtkFilename);
        
        try {
            meshReader->Update();
        } catch (const itk::ExceptionObject& exp) {
            std::cerr << exp.GetDescription() << std::endl;
            return false;
        }
        
        MeshType::Pointer mesh = meshReader->GetOutput();
        
        // setup vertices
        surface.m_verts.resize(mesh->GetNumberOfPoints());
        
        // printf("The number of vertices loaded : %ld\n", mesh->GetNumberOfPoints());
        
        typedef MeshType::PointsContainer::Iterator PointIteratorType;
        typedef MeshType::PointType PointType;
        
        PointIteratorType pointIterator = mesh->GetPoints()->Begin();
        PointIteratorType pointEnd = mesh->GetPoints()->End();
        
        unsigned int idx = 0;
        while (pointIterator != pointEnd) {
            
            PointType pt = pointIterator.Value();
            surface.m_verts[idx].x = pt[0];
            surface.m_verts[idx].y = pt[1];
            surface.m_verts[idx].z = pt[2];
            
            // printf("%f, %f, %f\n", surface.m_verts[idx].x, surface.m_verts[idx].y, surface.m_verts[idx].z);
            
            ++ idx;
            ++ pointIterator;
        }
        
        // setup faces
        typedef itk::TriangleCell<MeshType::CellType> TriangleType;
        typedef MeshType::CellsContainer::Iterator CellIterator;
        CellIterator cellIterator = mesh->GetCells()->Begin();
        CellIterator cellEnd = mesh->GetCells()->End();
        
        idx = 0;
        // setup faces
        surface.m_faces.resize(mesh->GetNumberOfCells());
        
        // printf("The number of cells loaded: %ld\n", mesh->GetNumberOfCells());
        
        while (cellIterator != cellEnd) {
            
            MeshType::CellType* cellptr = cellIterator.Value();
            TriangleType* cell = dynamic_cast<TriangleType *>(cellptr);
            
            assert(cell->GetNumberOfPoints() == 3);
            
            TriangleType::PointIdIterator idIterator = cell->PointIdsBegin();
            TriangleType::PointIdIterator idEnd = cell->PointIdsEnd();
            
            unsigned int subidx = 0;
            while (idIterator != idEnd) {
                
                assert(subidx < 3);
                
                int ptIdx = (*idIterator);
                surface.m_faces[idx][subidx] = ptIdx;
                
                ++ subidx;
                ++ idIterator;
            }
            
            // printf("%d, %d, %d\n", surface.m_faces[idx][0], surface.m_faces[idx][1], surface.m_faces[idx][2]);
            
            ++ idx;
            ++ cellIterator;
        }
        
        return true;            
    }
    
    
    
    static bool LoadSurfaceFromISO(const char* filename, Surface& surface)
	{
        const unsigned int MAX_LINE = 2048;
        
		int i;
		FILE *fp;
        
		fp=fopen(filename,"r");
		if (NULL==fp)
		{
			printf("Can not open file %s for reading.\n",filename);
			return false;
		}
        
		char string[MAX_LINE];
		int verNum,faceNum;
        
		fgets (string , MAX_LINE , fp);
		//puts (string);
		sscanf(string,"%d %d",&verNum,&faceNum);
		printf("verNum=%d triangleNum=%d\n",verNum,faceNum);
        
		// Reading vertices		
		surface.m_verts.resize(verNum);		
		for(i=0;i<verNum;i++)
		{  
			fgets (string , MAX_LINE , fp);
            
			///////////////////////////////////////////////////////
			// Notice: x,y might be flipped
			///////////////////////////////////////////////////////			
			sscanf(string,"%f %f %f",&surface.m_verts[i].x,&surface.m_verts[i].y,&surface.m_verts[i].z);			
		}
        
        surface.m_faces.resize(faceNum);
		for(i=0;i<faceNum;i++)
		{
			fgets (string , MAX_LINE , fp);
			sscanf(string,"%d %d %d",&surface.m_faces[i][0],&surface.m_faces[i][1],&surface.m_faces[i][2]);
		}
		
		fclose(fp);
        
		return true;
	}
    
    
    
    static bool LoadSurface(const char* filename, Surface& surface) 
	{
		std::string extension = find_extension(filename);
        
		if (extension == ".vtk")
		{
			return LoadSurfaceFromVTK(filename, surface);
		}else if (extension == ".iso")
		{
			return LoadSurfaceFromISO(filename, surface);
		}else {
			std::cerr << "Unsupported Mesh Type" << std::endl;
			return false;
		}
	}
    
    
    //
    // Surface IO (Save Surface)
    //
    
    static bool SaveSurfaceAsVTK(const Surface& surface, const char* vtkFilename) 
    {
        MeshType::Pointer mesh = MeshType::New();
        
        // setup vertices
        for (unsigned int i = 0; i < surface.m_verts.size(); ++i) {
            
            MeshType::PointType pt;
            
            for (unsigned int j = 0; j < 3; ++ j)
                pt[j] = surface.m_verts[i][j];
            
            mesh->SetPoint(i, pt);
        }
        
		// printf("The number of points in the mesh: %ld \n", mesh->GetNumberOfPoints());
        
        typedef itk::TriangleCell<MeshType::CellType> TriangleType;
        typedef MeshType::CellType::CellAutoPointer CellAutoPointer;
        
        // setup faces
        for (unsigned int i = 0; i < surface.m_faces.size(); ++i) {
            
            CellAutoPointer triangle;
            triangle.TakeNoOwnership(new TriangleType);
            
            triangle->SetPointId(0, surface.m_faces[i][0]);
            triangle->SetPointId(1, surface.m_faces[i][1]);
            triangle->SetPointId(2, surface.m_faces[i][2]);
            
            mesh->SetCell(i, triangle);
        }
        
        // printf("The number of triangles in the mesh: %ld \n", mesh->GetNumberOfCells());
        
        typedef itk::VTKPolyDataWriter<MeshType> MeshWriterType;
        MeshWriterType::Pointer meshWriter = MeshWriterType::New();
        
        meshWriter->SetFileName(vtkFilename);
        meshWriter->SetInput(mesh);
        try {
            meshWriter->Update();
        } catch (const itk::ExceptionObject& exp) {
            std::cerr << exp.GetDescription() << std::endl; 
            return false;
        }
        
        return true;        
    }
    
    
    static bool SaveSurfaceAsISO(const Surface& surface, const char* filename) {
        
        int i;
        FILE *fp;
        
        fp=fopen(filename,"w");
        if (NULL==fp)
        {
			printf("Can not open file %s for writing\n",filename);
			return false;
		}
		
		int verNum,faceNum;
        
		verNum=surface.m_verts.size();
		faceNum=surface.m_faces.size();
        
		fprintf(fp,"%d\t%d\n",verNum,faceNum);
        
		// Write vertices				
		for(i=0;i<verNum;i++)
		{
			///////////////////////////////////////////////////////
			// Notice: x,y might be flipped
			///////////////////////////////////////////////////////			
			fprintf(fp,"%f\t%f\t%f\n",surface.m_verts[i].x,surface.m_verts[i].y,surface.m_verts[i].z);
            
		} 
		
		// Write m_faces
		for(i=0;i<faceNum;i++)
		{
			fprintf(fp,"%d\t%d\t%d\n",surface.m_faces[i][0],surface.m_faces[i][1],surface.m_faces[i][2]);			
		}
        
		fclose(fp);
		return true;       
        
    }
    
	static bool SaveSurface(const Surface& surface, const char* filename) {
        
		std::string extension = find_extension(filename);
        
		if (extension == ".vtk")
		{
			return SaveSurfaceAsVTK(surface, filename);
		}else if (extension == ".iso")
		{
			return SaveSurfaceAsISO(surface, filename);
		}else {
			std::cerr << "Unsupported Output Mesh Type! Use iso and vtk as extension." << std::endl;
			return false;
		}
	}

	static bool SaveSurface(const vnl_vector<float>& pt_vect, const std::vector< vect3<int> >& faces, const char* filename) {

		assert(pt_vect.size() % 3 == 0);
		if (pt_vect.size() % 3 != 0) {
			printf("point vector should have a 3x length: %u.\n", pt_vect.size());
			return false;
		}
		
		Surface tmp_surface;
		tmp_surface.m_verts.resize(pt_vect.size() / 3);
		for (unsigned int i = 0; i < tmp_surface.m_verts.size(); ++i) {
			tmp_surface.m_verts[i][0] = pt_vect[i * 3];
			tmp_surface.m_verts[i][1] = pt_vect[i * 3 + 1];
			tmp_surface.m_verts[i][2] = pt_vect[i * 3 + 2];
		}
		
		tmp_surface.m_faces = faces;

		return SaveSurface(tmp_surface, filename);
	}

	static bool SaveSurface(const std::vector< vect3<float> >& verts, const std::vector< vect3<int> >& faces, const char* filename ) {

		Surface tmp_surface;
		tmp_surface.m_verts = verts;
		tmp_surface.m_faces = faces;
		return SaveSurface(tmp_surface, filename);
	}

    
    // only applicable in vtk format 
    static bool SaveSurfaceAsVTKWithColors(const Surface& surface, unsigned int nColorVertices, const char* filename) 
    {
        
        std::string str_filename = filename;
        std::string extenstion = str_filename.substr(str_filename.find_last_of('.'));
        
        if (extenstion != ".vtk") {
            printf("SaveSurfaceWithColor only supports vtk file format. \n");
            return false;
        }
        
        if (surface.m_verts.size() < nColorVertices) {
            printf("number of color vertices exceeds the maximum number of vertices. \n");
            return false;
        }
        
        
        if(! SaveSurfaceAsVTK(surface, filename) ) 
            return false;
        
        // generate rgb color for the first "nColorVertices" vertices.
        // the color for rest vertices is set to be white. 
		std::vector< itk::RGBAPixel<float> > vertexColors;
		vertexColors.resize(surface.m_verts.size());
        
        for (unsigned int i = 0; i < vertexColors.size(); ++i) {
            
            if (i < nColorVertices) {
                
                float val = i * 1.0f / nColorVertices;
                vect3<float> jet_color = get_jet_color(val);
                
                vertexColors[i][0] = jet_color[0];
                vertexColors[i][1] = jet_color[1];
                vertexColors[i][2] = jet_color[2];
                vertexColors[i][3] = 1.0f;
                
            }else {
                vertexColors[i][0] = 1.0f;
                vertexColors[i][1] = 1.0f;
                vertexColors[i][2] = 1.0f;
                vertexColors[i][3] = 1.0f;
            }
            
        }
        
        // append color information for each vertex
		FILE* fp = fopen(filename, "a+");
		if (fp == NULL) {
			printf("Failure to open the saved mesh to append vertex color information. \n");
			return false;
		}
        
        fprintf(fp, "POINT_DATA %ld\n", vertexColors.size());
        fprintf(fp, "COLOR_SCALARS color_scalars 4\n"); // RGBA mode
        for (unsigned int i = 0; i < vertexColors.size(); ++i) {
            
            itk::RGBAPixel<float> color = vertexColors[i];
            fprintf(fp, "%f %f %f %f\n", color.GetRed(), color.GetGreen(), color.GetBlue(), color.GetAlpha());
        }
        
        fclose(fp);
        
        return true;
    }
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
	//
	// Surface Conversion
	//
    ////////////////////////////////////////////////////////////////////////////////////////
    
	static void vtkPolyData2Surface(vtkPolyData* polyData, Surface& surface) 
	{
		const unsigned int numberOfPoints = polyData->GetNumberOfPoints();
        
		surface.m_verts.resize(numberOfPoints);
		for (unsigned int i = 0; i < numberOfPoints; ++i)
		{
			const vtkFloatingPointType* point = polyData->GetPoint(i);
			surface.m_verts[i].x = point[0];
			surface.m_verts[i].y = point[1];
			surface.m_verts[i].z = point[2];
		}
		
		//
		// Transfer the cells from the vtkPolyData into the itk::Mesh
		//
		vtkCellArray * triangleStrips = polyData->GetStrips();
        
		vtkIdType  * cellPoints;
		vtkIdType    numberOfCellPoints;
        
        
		// First count the total number of triangles from all the triangle strips.
		unsigned int numberOfTriangles = 0;
        
		triangleStrips->InitTraversal();
		while( triangleStrips->GetNextCell( numberOfCellPoints, cellPoints ) ) {
			numberOfTriangles += numberOfCellPoints-2;
		}
        
		// then count the regular triangles
		vtkCellArray * polygons = polyData->GetPolys();
		polygons->InitTraversal();
		while( polygons->GetNextCell( numberOfCellPoints, cellPoints ) ) {
			if( numberOfCellPoints == 3 ) {
				numberOfTriangles ++;
			}
		}
        
		//
		// Reserve memory in the surface for all those triangles
		//
		surface.m_faces.resize(numberOfTriangles);
        
		// 
		// Copy the triangles from vtkPolyData into the surface
		//
        
		int cellId = 0;
        
		// first copy the triangle strips
		triangleStrips->InitTraversal();
		while( triangleStrips->GetNextCell( numberOfCellPoints, cellPoints ) )  {
            
			unsigned int numberOfTrianglesInStrip = numberOfCellPoints - 2;
			unsigned long pointIds[3];
			pointIds[0] = cellPoints[0];
			pointIds[1] = cellPoints[1];
			pointIds[2] = cellPoints[2];
            
			for( unsigned int t=0; t < numberOfTrianglesInStrip; t++ ) {
                
				surface.m_faces[cellId][0] = pointIds[0];
				surface.m_faces[cellId][1] = pointIds[1];
				surface.m_faces[cellId][2] = pointIds[2];
                
				cellId++;
				pointIds[0] = pointIds[1];
				pointIds[1] = pointIds[2];
				pointIds[2] = cellPoints[t+3];
			}
		}
        
		// then copy the normal triangles
		polygons->InitTraversal();
		while( polygons->GetNextCell( numberOfCellPoints, cellPoints ) ) {
			if( numberOfCellPoints !=3 ) // skip any non-triangle.
			{
				continue;
			}
            
			surface.m_faces[cellId][0] = cellPoints[0];
			surface.m_faces[cellId][1] = cellPoints[1];
			surface.m_faces[cellId][2] = cellPoints[2];
            
			cellId++;
		}
        
		// printf("Number of Points: %d, Number of Faces: %d\n", (int)surface.m_ver.size(), (int)surface.m_faces.size());
        
	}
    
	static void Surface2itkMesh(const Surface& surface, MeshType::Pointer mesh)
	{
		// set vertex
		unsigned int numberOfPoints = surface.m_verts.size();
		mesh->GetPoints()->Reserve(numberOfPoints);
        
		for(unsigned int p =0; p < numberOfPoints; ++p) {
			MeshType::PointType pt;
			pt[0] = surface.m_verts[p][0]; pt[1] = surface.m_verts[p][1]; pt[2] = surface.m_verts[p][2];
            
			mesh->SetPoint( p, pt );
		}
        
		typedef MeshType::CellType   CellType;
		typedef itk::TriangleCell< CellType > TriangleCellType;
        
		// set faces
		unsigned int numberOfFaces = surface.m_faces.size();
		mesh->GetCells()->Reserve(numberOfFaces);
        
		for (unsigned int p = 0; p < numberOfFaces; ++p) {
			
			unsigned long cell_points[3] = {0};
			cell_points[0] = static_cast<unsigned long>(surface.m_faces[p][0]);
			cell_points[1] = static_cast<unsigned long>(surface.m_faces[p][1]);
			cell_points[2] = static_cast<unsigned long>(surface.m_faces[p][2]);
            
			MeshType::CellAutoPointer c;
			TriangleCellType* cell = new TriangleCellType;
			cell->SetPointIds(cell_points);
			c.TakeOwnership(cell);
			mesh->SetCell(p, c);
		}
        
	}
    
	// invoker should be responsible for deleting the polyData
	static void Surface2vtkPolyData(const Surface& surface, vtkPolyData*& polyData) 
	{
		polyData = vtkPolyData::New();
        
		// Create vtkPoints for insertion into polyData
		vtkPoints *points = vtkPoints::New();
        
		unsigned int nPoints = surface.m_verts.size();
		for (unsigned int i = 0; i < nPoints; ++i)
			points->InsertPoint(i, surface.m_verts[i][0], surface.m_verts[i][1], surface.m_verts[i][2]);
        
		polyData->SetPoints(points);
		points->Delete();
        
		// Copy all cells into the vtkPolyData structure
		// Create vtkCellArray into which the cells are copied
		vtkCellArray* triangles = vtkCellArray::New();
        
		for (unsigned int i = 0; i < surface.m_faces.size(); ++i)
		{
			vtkIdList* pts = vtkIdList::New();
			pts->InsertNextId(surface.m_faces[i][0]);
			pts->InsertNextId(surface.m_faces[i][1]);
			pts->InsertNextId(surface.m_faces[i][2]);
            
			triangles->InsertNextCell(pts);
		}
        
		polyData->SetPolys(triangles);
		triangles->Delete();
	}
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Surface Extraction
	//
    ///////////////////////////////////////////////////////////////////////////////////////////////
    
	template <class ImageType>
	static void ISOSurfaceFromImage(typename ImageType::Pointer image, Surface& surface, typename ImageType::PixelType iso_val) 
	{
		typename ImageType::Pointer target_image = image;
        
		typedef itk::ImageToVTKImageFilter<ImageType> ConvertFilterType;
		typename ConvertFilterType::Pointer convertFilter = ConvertFilterType::New();
		convertFilter->SetInput(target_image);
        
		// marching cubes
		vtkSmartPointer<vtkMarchingCubes> mc = vtkSmartPointer<vtkMarchingCubes>::New();
		mc->SetInput(convertFilter->GetOutput());
		mc->SetValue(0, iso_val);
		mc->Update();
        
		vtkPolyData* polydata = mc->GetOutput();
		vtkPolyData2Surface(polydata, surface);
        
        // followup codes fix the loss of orientation information in generating surface meshes
        typename ImageType::PointType origin = target_image->GetOrigin();
        typename ImageType::SpacingType spacing = target_image->GetSpacing();
        
        for (unsigned int i = 0; i < surface.m_verts.size(); ++i) {
            surface.m_verts[i][0] = (surface.m_verts[i][0] - origin[0]) / spacing[0];
            surface.m_verts[i][1] = (surface.m_verts[i][1] - origin[1]) / spacing[1];
            surface.m_verts[i][2] = (surface.m_verts[i][2] - origin[2]) / spacing[2];
        }
        
        Voxel2World<ImageType>(surface, image);
	}
    
    /////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Carve Surface to Image
	//
    /////////////////////////////////////////////////////////////////////////////////////////////////
    
	template <class TImage>
	static bool CarveSurface(Surface& surface, const typename TImage::Pointer ref_image, double tolerance, typename TImage::PixelType outside_val, typename TImage::PixelType inside_val, typename TImage::Pointer& output_image)
	{
		MeshType::Pointer mesh = MeshType::New();
		Surface2itkMesh(surface, mesh);
        
		typename TImage::PointType origin = ref_image->GetOrigin();
		typename TImage::SpacingType spacing = ref_image->GetSpacing();
		typename TImage::IndexType idx = ref_image->GetLargestPossibleRegion().GetIndex();
		typename TImage::SizeType size = ref_image->GetLargestPossibleRegion().GetSize();
		typename TImage::DirectionType direction = ref_image->GetDirection();
        
		typedef itk::TriangleMeshToBinaryImageFilter<MeshType, TImage> Mesh2ImageFilterType;
		typename Mesh2ImageFilterType::Pointer mesh2ImageFilter = Mesh2ImageFilterType::New();
		mesh2ImageFilter->SetInput(mesh);
		mesh2ImageFilter->SetTolerance(tolerance);
		mesh2ImageFilter->SetOrigin(origin);
		mesh2ImageFilter->SetSpacing(spacing);
		mesh2ImageFilter->SetIndex(idx);
		mesh2ImageFilter->SetSize(size);
		mesh2ImageFilter->SetDirection(direction);
		mesh2ImageFilter->SetInsideValue(inside_val);
		mesh2ImageFilter->SetOutsideValue(outside_val);
        
		try {
			mesh2ImageFilter->Update();
		} catch(itk::ExceptionObject& exp) {
			std::cerr << exp.GetDescription() << std::endl;
			return false;
		}
        
		output_image = mesh2ImageFilter->GetOutput();
		return true;
	}
    
    /////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Smooth Surface 
	//
    /////////////////////////////////////////////////////////////////////////////////////////////
    
    //
    // Pass band (copyed from vtk documentation): 
    // Lower PassBand values produce more smoothing. A good default value for the PassBand is 0.1 (for those interested, the PassBand (and frequencies) for PolyData are based on the valence of the vertices, this limits all the frequency modes in a polyhedral mesh to between 0 and 2.
    //
    
	static bool SmoothSurface(Surface& surface, unsigned int iterations, double pass_band = 0.1) 
	{
		if (iterations == 0)
			// no smoothing performed
			return true;
        
		if (pass_band > 2 || pass_band < 0)
		{
			std::cerr << "pass_band should be between 0 and 2" << std::endl;
			return false;
		}
        
		vtkPolyData* polyData;
		Surface2vtkPolyData(surface, polyData);
        
		vtkSmartPointer<vtkWindowedSincPolyDataFilter> smoothFilter = vtkSmartPointer<vtkWindowedSincPolyDataFilter>::New();
		smoothFilter->SetInput(polyData);
		smoothFilter->SetNumberOfIterations(iterations);
		smoothFilter->BoundarySmoothingOff();
		smoothFilter->FeatureEdgeSmoothingOff();
		smoothFilter->NonManifoldSmoothingOff();
		smoothFilter->NormalizeCoordinatesOn();
		smoothFilter->SetPassBand(pass_band);
		smoothFilter->Update();
        
		vtkPolyData2Surface(smoothFilter->GetOutput(), surface);
        
		return true;
	}
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Voxel to World, World to Voxel (Coordinate Conversion)
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////
    
    //
    // convert voxel coordinates to world coordinates
    //
    
	template <class TImage>
	static void Voxel2World(Surface& surface, typename TImage::Pointer ref_image)
	{
		typename TImage::PointType origin = ref_image->GetOrigin();
		typename TImage::DirectionType direction_matrix = ref_image->GetDirection();
		typename TImage::SpacingType spacing = ref_image->GetSpacing();
        
		vnl_matrix<double> diag_spacing_matrix(3,3);
		diag_spacing_matrix.fill(0);
		for (unsigned int i = 0; i < 3; ++i)
			diag_spacing_matrix[i][i] = spacing[i];
		
		vnl_matrix<double> point_matrix;
		point_matrix.set_size(3, surface.m_verts.size());
		for (unsigned int j = 0; j < 3; ++j)
			for (unsigned int i = 0; i < surface.m_verts.size(); ++i)
				point_matrix[j][i] = surface.m_verts[i][j];
		
		vnl_matrix<double> transformed_points = direction_matrix * (diag_spacing_matrix * point_matrix);
        
		for (unsigned int j = 0; j < 3; ++j)
			for (unsigned int i = 0; i < surface.m_verts.size(); ++i)
				transformed_points[j][i] += origin[j];
        
		// copy back
		for (unsigned int j = 0; j < 3; ++j)
			for (unsigned int i = 0; i < surface.m_verts.size(); ++i)
				surface.m_verts[i][j] = transformed_points[j][i];	
	}
    
    
private:
    
    // extract extension
	static std::string find_extension(const char* path)
	{
		std::string path_str(path);
        
		std::size_t idx = path_str.rfind('.');
		if(idx == std::string::npos)
			return std::string();
		else
			return path_str.substr(idx);
	}
    
    //
    // get jet color, val in [0,1]
    //
    static vect3<float> get_jet_color(float val) 
    {
        float fourValue = 4 * val;
        float red = std::min(fourValue - 1.5, -fourValue + 4.5);
        float green = std::min(fourValue - 0.5, -fourValue + 3.5);
        float blue = std::min(fourValue + 0.5, -fourValue + 2.5);
        
        if(red > 1.0f) red = 1.0f;
        if (red < 0.0f) red = 0.0f;
        
        if(green > 1.0f) green = 1.0f;
        if(green < 0.0f) green = 0.0f;
        
        if (blue > 1.0f) blue = 1.0f;
        if (blue < 0.0f) blue = 0.0f;
        
        vect3<float> ret;
        ret[0] = red; ret[1] = green; ret[2] = blue;
        return ret;
    }
    
    
};

} } }

#endif