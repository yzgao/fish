//
//  DrawTools.cpp
//  FISH
//
//  Created by Yaozong Gao on 02/26/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//

#include "stdafx.h"
#include "extern/ImageHelper.h"
#include "extern/BoostHelper.h"
#include "common/stringUtils.h"
#include "extern/DisplacementViewer.h"

using namespace BRIC::IDEA::FISH;

void print_usage()
{
	std::cerr << "[ Draw a sagittal image slice with color contours ]" << std::endl;
	std::cerr << "DrawTools --xslice --in image --seg path1;path2;path3 --color red;green;blue --idx sliceIdx --min a --max b --line 2 --out pic.png\n" << std::endl;

	std::cerr << "[ Draw displacement field over image slice ]" << std::endl;
	std::cerr << "DrawTools --dispplot --in image --disp prefix --format mha {--xidx|--yidx|--zidx} x --stride x --arrowsize x --width x --out pic.png [--color red --min 900 --max 1300]\n" << std::endl;

	//std::cerr << "[ Draw a sagittal displacement field over the image slice ]" << std::endl;
	//std::cerr << "DrawTools --xdisp --in image --disp prefix --format mha --color red --idx sliceIdx --min a --max b --stride x,y --arrowsize val --out pic.png\n" << std::endl;

	std::cerr << "[ Draw a centered-box 3D image ]" << std::endl;
	std::cerr << "DrawTools --box --size box_x,box_y,box_z,image_x,image_y,image_z --fg 255 --out image\n" << std::endl;

	std::cerr << "[ Draw a centered-sphere 3D image ]" << std::endl;
	std::cerr << "DrawTools --sphere --size sphere_x,sphere_y,sphere_z,image_x,image_y,image_z --fg 255 --out image\n" << std::endl;

	std::cerr << "[ Color options ]" << std::endl;
	std::cerr << "black; white; red; lime; blue; yellow; cyan; magenta; silver; gray; maroon; olive; green; purple; teal; navy\n" << std::endl;
}


template <typename T>
void write_xslice_base(std::string input_path, std::vector<std::string>& seg_paths, std::vector<std::string>& colors, int slice_idx, double min, double max, int linesize, std::string output_path)
{
	assert_message(seg_paths.size() == colors.size(), "number of segmentation and color mismatches");

	mxImage<T> image;
	if (!ImageHelper::ReadImage<T,T>(input_path.c_str(), image)) {
		std::cerr << "fail to read intensity image from " << input_path << std::endl; exit(-1);
	}

	std::vector< mxImage<unsigned char> > seg_images(seg_paths.size());
	for (size_t i = 0; i < seg_paths.size(); ++i)
	{
		if (!ImageHelper::ReadImage<T,unsigned char>(seg_paths[i].c_str(), seg_images[i])) {
			std::cerr << "fail to read segmentation images from " << seg_paths[i] << std::endl; exit(-1);
		}
	}

	typedef itk::Image<itk::RGBPixel<unsigned char>, 2> itkImageType;
	itkImageType::Pointer slice = itkImageType::New();
	ImageHelper::ConvertXSliceToITK(image, slice_idx, min, max, slice);
	for (size_t i = 0; i < seg_paths.size(); ++i)
		ImageHelper::PlotXSliceContour(seg_images[i], slice_idx, colors[i], linesize, slice);
	
	if (!ImageHelper::WriteItkImage<itkImageType>(slice, output_path.c_str())) {
		std::cerr << "fails to write image to " << output_path << std::endl; exit(-1);
	}
}

void write_xslice(boost::program_options::variables_map& vm)
{
	std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
	std::string seg_str = BoostCmdHelper::Get<std::string>(vm, "seg");

	std::vector<std::string> seg_paths;
	seg_paths = stringUtils::Split(seg_str, ';');

	std::string color_str = BoostCmdHelper::Get<std::string>(vm, "color");
	std::vector<std::string> colors;
	colors = stringUtils::Split(color_str, ';');

	int slice_idx = BoostCmdHelper::Get<int>(vm, "idx");
	std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

	double min = BoostCmdHelper::Get<double>(vm, "min");
	double max = BoostCmdHelper::Get<double>(vm, "max");

	int linesize = BoostCmdHelper::Get<int>(vm, "line");

	ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());

	switch (type)
	{
	case BRIC::IDEA::FISH::ImageHelper::UCHAR: write_xslice_base<unsigned char>(input_path, seg_paths, colors, slice_idx, min, max, linesize, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::CHAR: write_xslice_base<char>(input_path, seg_paths, colors, slice_idx, min, max, linesize, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::USHORT: write_xslice_base<unsigned short>(input_path, seg_paths, colors, slice_idx, min, max, linesize, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::SHORT: write_xslice_base<short>(input_path, seg_paths, colors, slice_idx, min, max, linesize, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::UINT: write_xslice_base<unsigned int>(input_path, seg_paths, colors, slice_idx, min, max, linesize, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::INT: write_xslice_base<int>(input_path, seg_paths, colors, slice_idx, min, max, linesize, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::ULONG: write_xslice_base<unsigned long>(input_path, seg_paths, colors, slice_idx, min, max, linesize, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::LONG: write_xslice_base<long>(input_path, seg_paths, colors, slice_idx, min, max, linesize, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::FLOAT: write_xslice_base<float>(input_path, seg_paths, colors, slice_idx, min, max, linesize, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::DOUBLE: write_xslice_base<double>(input_path, seg_paths, colors, slice_idx, min, max, linesize, output_path); break;
	default: err_message("unrecognized pixel type"); 
	}
}

template <typename T>
void write_xdisp_base(std::string input_path, std::string disp_prefix, std::string format, int slice_idx, double min_val, double max_val, int stride_x, int stride_y, int arrowsize, std::string color, std::string output_path)
{
	mxImage<T> image;
	std::vector < mxImage<double> > disp_maps;

	{
		mxImage<T> raw_image;
		if (!ImageHelper::ReadImage<T, T>(input_path.c_str(), raw_image)) {
			std::cerr << "fails to read image from " << input_path << std::endl; exit(-1);
		}

		vect3<double> resample_spacing(1.0, 1.0, 1.0);
		mxImageUtils::Resample(raw_image, vect3<double>(1.0, 1.0, 1.0), image, IT_LINEAR_INTERPOLATION);
		
		slice_idx = static_cast<int>( slice_idx * raw_image.GetSpacing()[0] / resample_spacing[0] + 0.5 );
	}
	
	disp_maps.resize(3);
	for (size_t i = 0; i < disp_maps.size(); ++i)
	{
		std::string disp_path = disp_prefix + stringUtils::num2str(i) + "." + format;
		if (!ImageHelper::ReadImage<double,double>(disp_path.c_str(), disp_maps[i])) {
			std::cerr << "fails to read images from " << disp_path << std::endl; exit(-1);
		}
	}

	typedef itk::Image<itk::RGBPixel<unsigned char>, 2> itkImageType;
	itkImageType::Pointer out_image = itkImageType::New();
	ImageHelper::PlotXDisp(image, disp_maps, slice_idx, min_val, max_val, arrowsize, stride_x, stride_y, color, out_image);
	if (!ImageHelper::WriteItkImage<itkImageType>(out_image, output_path.c_str())) {
		std::cerr << "fail to write itk image to " << output_path << std::endl; exit(-1);
	}
}

void write_xdisp(boost::program_options::variables_map& vm)
{
	std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
	std::string disp_prefix = BoostCmdHelper::Get<std::string>(vm, "disp");
	std::string format = BoostCmdHelper::Get<std::string>(vm, "format");
	std::string color = BoostCmdHelper::Get<std::string>(vm, "color");
	int slice_idx = BoostCmdHelper::Get<int>(vm, "idx");
	double min_val = BoostCmdHelper::Get<double>(vm, "min");
	double max_val = BoostCmdHelper::Get<double>(vm, "max");
	std::string stride_str = BoostCmdHelper::Get<std::string>(vm, "stride");
	std::vector<std::string> stride_strs;
	stride_strs = stringUtils::Split(stride_str, ',');
	assert_message(stride_strs.size() == 2, "only two numbers for stride");
	int stride_x = stringUtils::str2num<int>(stride_strs[0]);
	int stride_y = stringUtils::str2num<int>(stride_strs[1]);
	int arrowsize = BoostCmdHelper::Get<int>(vm, "arrowsize");
	std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

	ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());

	switch (type)
	{
	case BRIC::IDEA::FISH::ImageHelper::UCHAR: write_xdisp_base<unsigned char>(input_path, disp_prefix, format, slice_idx, min_val, max_val, stride_x, stride_y, arrowsize, color, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::CHAR: write_xdisp_base<char>(input_path, disp_prefix, format, slice_idx, min_val, max_val, stride_x, stride_y, arrowsize, color, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::USHORT: write_xdisp_base<unsigned short>(input_path, disp_prefix, format, slice_idx, min_val, max_val, stride_x, stride_y, arrowsize, color, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::SHORT: write_xdisp_base<short>(input_path, disp_prefix, format, slice_idx, min_val, max_val, stride_x, stride_y, arrowsize, color, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::UINT: write_xdisp_base<unsigned int>(input_path, disp_prefix, format, slice_idx, min_val, max_val, stride_x, stride_y, arrowsize, color, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::INT: write_xdisp_base<int>(input_path, disp_prefix, format, slice_idx, min_val, max_val, stride_x, stride_y, arrowsize, color, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::ULONG: write_xdisp_base<unsigned long>(input_path, disp_prefix, format, slice_idx, min_val, max_val, stride_x, stride_y, arrowsize, color, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::LONG: write_xdisp_base<long>(input_path, disp_prefix, format, slice_idx, min_val, max_val, stride_x, stride_y, arrowsize, color, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::FLOAT: write_xdisp_base<float>(input_path, disp_prefix, format, slice_idx, min_val, max_val, stride_x, stride_y, arrowsize, color, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::DOUBLE: write_xdisp_base<double>(input_path, disp_prefix, format, slice_idx, min_val, max_val, stride_x, stride_y, arrowsize, color, output_path); break;
	default: err_message("unrecognized pixel type");
	}
}

template <typename T>
void write_disp_base(boost::program_options::variables_map& vm)
{
	DisplacementViewer<T> viewer;

	std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
	std::string disp_prefix = BoostCmdHelper::Get<std::string>(vm, "disp");
	std::string format = BoostCmdHelper::Get<std::string>(vm, "format");
	
	if (vm.count("xidx"))
		viewer.SetSliceX(vm["xidx"].as<int>());
	else if (vm.count("yidx"))
		viewer.SetSliceY(vm["yidx"].as<int>());
	else if (vm.count("zidx"))
		viewer.SetSliceZ(vm["zidx"].as<int>());
	else
		err_message("no xidx yidx zidx");

	int stride = atoi( BoostCmdHelper::Get<std::string>(vm, "stride").c_str() );
	int arrow_size = BoostCmdHelper::Get<int>(vm, "arrowsize");
	int width = BoostCmdHelper::Get<int>(vm, "width");
	std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
	std::string color = BoostCmdHelper::Get<std::string>(vm, "color");
	double display_min = BoostCmdHelper::Get<double>(vm, "min");
	double display_max = BoostCmdHelper::Get<double>(vm, "max");

	mxImage<T> image;
	if (!ImageHelper::ReadImage<T,T>(input_path.c_str(), image)) {
		std::cerr << "fail to read image from " << input_path << std::endl;
		exit(-1);
	}

	std::vector< mxImage<double> > disp_images(3);
	for (size_t i = 0; i < disp_images.size(); ++i)
	{
		std::string disp_path = disp_prefix + stringUtils::num2str(i) + "." + format;
		if (!ImageHelper::ReadImage<double,double>(disp_path.c_str(), disp_images[i])) {
			std::cerr << "fail to read displacement image from " << disp_path << std::endl;
			exit(-1);
		}
	}

	viewer.SetImage(&image);
	viewer.SetDisplacements(&disp_images[0]);
	viewer.SetColor(color);
	viewer.SetDisplayRange(display_min, display_max);
	viewer.SetPicWidth(width);
	viewer.SetArrowSize(arrow_size);
	viewer.SetStride(stride);

	viewer.Plot();

	DisplacementViewer<T>::RGBImageType::Pointer pic = viewer.GetPic();
	if (!ImageHelper::WriteItkImage<DisplacementViewer<T>::RGBImageType>(pic, output_path.c_str())) {
		std::cerr << "fail to write itk image to " << output_path << std::endl; exit(-1);
	}
}

void write_disp(boost::program_options::variables_map& vm)
{
	std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
	ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());

	switch (type)
	{
	case ImageHelper::UCHAR: write_disp_base<unsigned char>(vm); break;
	case ImageHelper::CHAR: write_disp_base<char>(vm); break;
	case ImageHelper::USHORT: write_disp_base<unsigned short>(vm); break;
	case ImageHelper::SHORT: write_disp_base<short>(vm); break;
	case ImageHelper::UINT: write_disp_base<unsigned int>(vm); break;
	case ImageHelper::INT: write_disp_base<int>(vm); break;
	case ImageHelper::ULONG: write_disp_base<unsigned long>(vm); break;
	case ImageHelper::LONG: write_disp_base<long>(vm); break;
	case ImageHelper::FLOAT: write_disp_base<float>(vm); break;
	case ImageHelper::DOUBLE: write_disp_base<double>(vm); break;
	default: err_message("unrecognized pixel type");
	}
}

void plot_box3d(boost::program_options::variables_map& vm)
{
	std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
	std::string size_str = BoostCmdHelper::Get<std::string>(vm, "size");
	int foreground_value = BoostCmdHelper::Get<int>(vm, "fg");

	std::vector<std::string> size_tokens;
	stringUtils::Split(size_str, ',', size_tokens);

	if (size_tokens.size() != 6) {
		std::cerr << "must provide six numbers for size option" << std::endl; 
		exit(-1);
	}

	vect3<int> box_size;
	for (int i = 0; i < 3; ++i)
		box_size[i] = stringUtils::str2num<int>(size_tokens[i]);

	vect3<int> image_size;
	for (int i = 0; i < 3; ++i)
		image_size[i] = stringUtils::str2num<int>(size_tokens[i + 3]);

	assert_message(image_size[0] >= box_size[0] && image_size[1] >= box_size[1] && image_size[2] >= box_size[2], "image size must >= box size");

	mxImage<unsigned short> image;
	image.SetImageSize(image_size.to<unsigned int>());
	image.Fill(0);

	vect3<int> sp, ep;
	for (int i = 0; i < 3; ++i)
	{
		sp[i] = image_size[i] / 2 - box_size[i] / 2;
		ep[i] = sp[i] + box_size[i] - 1;
	}

	for (int z = sp[2]; z <= ep[2]; ++z)
	{
		for (int y = sp[1]; y <= ep[1]; ++y)
		{
			for (int x = sp[0]; x <= ep[0]; ++x)
			{
				image(x, y, z) = static_cast<unsigned short>(foreground_value);
			}
		}
	}

	if (!ImageHelper::WriteImage<unsigned short, unsigned short>(image, output_path.c_str()))
	{
		std::cerr << "fails to write image to " << output_path << std::endl;
		exit(-1);
	}

}

void plot_sphere3d(boost::program_options::variables_map& vm)
{
	std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
	std::string size_str = BoostCmdHelper::Get<std::string>(vm, "size");
	int foreground_value = BoostCmdHelper::Get<int>(vm, "fg");

	std::vector<std::string> size_tokens;
	stringUtils::Split(size_str, ',', size_tokens);

	if (size_tokens.size() != 6) {
		std::cerr << "must provide six numbers for size option" << std::endl;
		exit(-1);
	}

	vect3<int> sphere_size;
	for (int i = 0; i < 3; ++i)
		sphere_size[i] = stringUtils::str2num<int>(size_tokens[i]);

	vect3<int> image_size;
	for (int i = 0; i < 3; ++i)
		image_size[i] = stringUtils::str2num<int>(size_tokens[i + 3]);

	assert_message(image_size[0] >= sphere_size[0] && image_size[1] >= sphere_size[1] && image_size[2] >= sphere_size[2], "image size must >= sphere size");

	mxImage<unsigned short> image;
	image.SetImageSize(image_size.to<unsigned int>());
	image.Fill(0);

	vect3<int> sphere_radius = sphere_size / 2;

	#pragma omp parallel for
	for (int z = 0; z < image_size[2]; ++z)
	{
		for (int y = 0; y < image_size[1]; ++y)
		{
			for (int x = 0; x < image_size[0]; ++x)
			{
				vect3<double> offset;
				offset[0] = x - image_size[0] / 2.0;
				offset[1] = y - image_size[1] / 2.0;
				offset[2] = z - image_size[2] / 2.0;

				double val = 0;
				for (int i = 0; i < 3; ++i)
					val += offset[i] * offset[i] / (sphere_radius[i] * sphere_radius[i]);

				if (val <= 1.0)
					image(x, y, z) = static_cast<unsigned short>(foreground_value);
				else
					image(x, y, z) = 0;
			}
		}
	}

	if (!ImageHelper::WriteImage<unsigned short, unsigned short>(image, output_path.c_str()))
	{
		std::cerr << "fail to write image to " << output_path << std::endl;
		exit(-1);
	}
}


int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display help information");
		generic.add_options()("xslice", "write sagittal slice");
		generic.add_options()("xdisp", "plot sagittal displacement field");
		generic.add_options()("dispplot", "plot displacement field");
		generic.add_options()("box", "plot a 3D box");
		generic.add_options()("sphere", "plot a 3D sphere");

		options_description params("Input parameters");
		params.add_options()("in", value<std::string>(), "input image");
		params.add_options()("seg", value<std::string>(), "segmentation image");
		params.add_options()("disp", value<std::string>(), "displacement prefix");
		params.add_options()("color", value<std::string>()->default_value("red"), "color string");
		params.add_options()("width", value<int>(), "pic width");
		params.add_options()("idx", value<int>(), "idx");
		params.add_options()("xidx", value<int>(), "xidx");
		params.add_options()("yidx", value<int>(), "yidx");
		params.add_options()("zidx", value<int>(), "zidx");
		params.add_options()("min", value<double>()->default_value(900), "minimum value");
		params.add_options()("max", value<double>()->default_value(1300), "maximum value");
		params.add_options()("out", value<std::string>(), "output path");
		params.add_options()("arrowsize", value<int>(), "arrowsize");
		params.add_options()("stride", value<std::string>(), "stride");
		params.add_options()("format", value<std::string>(), "format");
		params.add_options()("line", value<int>(), "line size");
		params.add_options()("size", value<std::string>(), "multi-dimensional size vector");
		params.add_options()("fg", value<int>(), "foreground value");

		options_description cmd_options;
		cmd_options.add(generic).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			std::cerr << cmd_options << std::endl;
			print_usage();
			return -1;
		}

		if (vm.count("xslice"))
		{
			write_xslice(vm);
			return 0;
		}

		if (vm.count("xdisp"))
		{
			write_xdisp(vm);
			return 0;
		}

		if (vm.count("box"))
		{
			plot_box3d(vm);
			return 0;
		}

		if (vm.count("sphere"))
		{
			plot_sphere3d(vm);
			return 0;
		}

		if (vm.count("dispplot"))
		{
			write_disp(vm);
			return 0;
		}

	}
	catch (boost::program_options::error& exp) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}
