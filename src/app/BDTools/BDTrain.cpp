//
//  BDTrain.cpp
//  FISH
//
//  Created by Yaozong Gao on 01/07/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//

#include "stdafx.h"
#include "detector/boundary/JAT_bd_trainer.h"
#include "extern/BoostHelper.h"

using namespace BRIC::IDEA::FISH;

void print_usage()
{
	std::cerr << "[ train a boundary detection model ]" << std::endl;
	std::cerr << "BDTrain --file iniFile --type ushort --seed int\n" << std::endl;

	std::cerr << "[ create an example INI file ]" << std::endl;
	std::cerr << "BDTrain --example iniFile\n" << std::endl;

	std::cerr << "[ create an example ANNOT file ]" << std::endl;
	std::cerr << "BDTrain --example2 annotFile\n" << std::endl;
}


template <typename T>
void TrainGeneral(boost::program_options::variables_map& vm)
{
	boost::timer::auto_cpu_timer forest_timer;

	typedef typename JAT_bd_trainer<T>::W W;
	typedef typename JAT_bd_trainer<T>::S S;

	Random random;
	if (vm.count("seed"))
	{
		random.Seed(vm["seed"].as<int>());
	}

	std::string ini_file = BoostCmdHelper::Get<std::string>(vm, "file");

	JAT_bd_trainer<T> trainer;
	if (!trainer.LoadINI(ini_file.c_str()))
	{
		std::cerr << "cannot load file : " << ini_file << std::endl;
		exit(-1);
	}

	trainer.CreateOutputFolder();
	trainer.SaveDetectionInfo();

	/************************************************************************/
	/* print out training information                                       */
	/************************************************************************/
	const JAT_bd_ini& ini = trainer.GetINI();
	const JAT_bd_struct& annot = trainer.GetAnnot();

	ini.print_out(std::cout);

	std::cout << std::endl;
	std::cout << "Total Number of Subjects:   " << annot.subject_num << std::endl;
	std::cout << "Number of Modalities    :   " << annot.modality_num << std::endl;
	std::cout << "Number of Boundaries    :   " << ini.general.bd_ids.size() << std::endl;
	std::cout << "Boundary IDs            :   " << stringUtils::token2str(ini.general.bd_ids, ' ') << std::endl;


	/************************************************************************/
	/* start training                                                       */
	/************************************************************************/

	int tree_number = trainer.GetTreeNumber();
	for (int i = 0; i < tree_number; ++i)
	{
		boost::timer::auto_cpu_timer tree_timer;

		std::cout << std::endl;

		std::cout << "- Creating feature space for tree (" << i + 1 << " / " << tree_number << ")" << std::endl;
		std::auto_ptr<CompositeFeatureSpace> fs = trainer.CreateFeatureSpace(random);

		std::cout << "- Training tree (" << i + 1 << " / " << tree_number << ")" << std::endl;
		std::auto_ptr< Tree<W, S> > tree = trainer.TrainTree(random, fs.get());

		if (!trainer.SaveTree(tree.get(), fs.get()))
			err_message("fail to save tree");

		std::cout << std::endl;
	}

	std::cout << std::endl;
}



int main(int argc, char** argv)
{
	try {

		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display help information");

		options_description params("Input parameters");
		params.add_options()("file", value<std::string>(), "iniFile");
		params.add_options()("seed", value<int>(), "seed");
		params.add_options()("example", value<std::string>(), "example iniFile");
		params.add_options()("example2", value<std::string>(), "example annotFile");
		params.add_options()("type", value<std::string>(), "pixel type");

		options_description cmd_options;
		cmd_options.add(generic).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			std::cerr << cmd_options << std::endl;
			print_usage();
			return -1;
		}

		// output example ini file
		if (vm.count("example")) {

			std::string path = vm["example"].as<std::string>();

			if (!JAT_bd_ini::write_default(path.c_str())) {
				std::cerr << "fails to output default INI file" << std::endl;
				return -1;
			}

			return 0;
		}


		// output example ANNOT file
		if (vm.count("example2")) {

			std::string path = vm["example2"].as<std::string>();

			if (!JAT_bd_struct::write_default(path.c_str())) {
				std::cerr << "fails to output default ANNOT file" << std::endl;
				return -1;
			}

			return 0;
		}


		std::string type = BoostCmdHelper::Get<std::string>(vm, "type");
		if (type == "char")
			TrainGeneral<char>(vm);
		else if (type == "uchar")
			TrainGeneral<unsigned char>(vm);
		else if (type == "short")
			TrainGeneral<short>(vm);
		else if (type == "ushort")
			TrainGeneral<unsigned short>(vm);
		else if (type == "int")
			TrainGeneral<int>(vm);
		else if (type == "uint")
			TrainGeneral<unsigned int>(vm);
		else if (type == "long")
			TrainGeneral<long>(vm);
		else if (type == "ulong")
			TrainGeneral<unsigned long>(vm);
		else if (type == "float")
			TrainGeneral<float>(vm);
		else if (type == "double")
			TrainGeneral<double>(vm);
		else {
			std::cerr << "unrecognized pixel type" << std::endl;
			return -1;
		}

	}
	catch (boost::program_options::error& exp) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}


	return 0;
}

