//
//  BDTest.cpp
//  FISH
//
//  Created by Yaozong Gao on 01/07/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//

#include "stdafx.h"
#include "detector/boundary/JAT_bd_tester.h"
#include "detector/boundary/JAT_bd_multires_tester.h"
#include "extern/BoostHelper.h"
#include "extern/ImageHelper.h"
#include "common/mxImageUtils.h"
#include "mesh/surface/SurfaceUtils.h"

using namespace BRIC::IDEA::FISH;

void print_usage()
{
	std::cerr << "[ single-resolution - joint classification and regression ]" << std::endl;
	std::cerr << "BDTest --single {options} --in imagePath1 ... imagePathN --root folder --name str --level idx --scale idx --label prefix --disp prefix --format mhd\n" << std::endl;

	std::cerr << "valid options:\n" << std::endl;
	std::cerr << "--global                    apply model on the entire image domain" << std::endl;
	std::cerr << "--mask path                 apply model on the region marked by mask (not implemented)" << std::endl;
	std::cerr << "--surface path --pad mm^3   apply model on the region near the surface" << std::endl;
	std::cerr << "--exp path --pad voxel^3    apply model using the ground truth with padding (for experiments)\n" << std::endl;

	std::cerr << "[ multi-resolution - joint classification and regression ]" << std::endl;
	std::cerr << "BDTest --multi --in imagePath1 ... imagePathN --root folder --config path --label prefix --disp prefix --out prefix --format mhd\n" << std::endl;

	std::cerr << "[ output sample config file ]" << std::endl;
	std::cerr << "BDTest --multi_config path\n" << std::endl;

}


/************************************************************************/
/* IO Utility                                                           */
/************************************************************************/

template <typename T>
void read_images(const std::vector<std::string>& image_paths, std::vector< mxImage<T> >& images)
{
	images.resize(image_paths.size());
	for (size_t i = 0; i < image_paths.size(); ++i)
	{
		if (!ImageHelper::ReadImage<T, T>(image_paths[i].c_str(), images[i])) {
			std::cerr << "fails to read image from " << image_paths[i] << std::endl; exit(-1);
		}
		if (i != 0)
			assert_message(images[i].GetImageSize() == images[0].GetImageSize(), "input images must have same size");
	}
}

void write_label_maps(const std::vector< mxImage<double> >& label_maps, std::string label_prefix, std::string output_format)
{
	for (size_t i = 0; i < label_maps.size(); ++i)
	{
		std::string output_path = label_prefix + stringUtils::num2str(i) + "." + output_format;
		if (!ImageHelper::WriteImage<double, double>(label_maps[i], output_path.c_str())) {
			std::cerr << "fails to write image to " << output_path << std::endl; exit(-1);
		}
	}
}

void write_disp_maps(const std::vector< mxImage<double> >& disp_maps, std::string disp_prefix, std::string output_format)
{
	for (size_t i = 0; i < disp_maps.size(); ++i)
	{
		std::string output_path = disp_prefix + stringUtils::num2str(i) + "." + output_format;
		if (!ImageHelper::WriteImage<double, double>(disp_maps[i], output_path.c_str())) {
			std::cerr << "fails to write image to " << output_path << std::endl; exit(-1);
		}
	}
}

void write_norm_maps(const std::vector< mxImage<double> >& disp_maps, std::string disp_prefix, std::string output_format)
{
	size_t num_norm_images = disp_maps.size() / 3;
	for (size_t i = 0; i < num_norm_images; ++i)
	{
		vect3<unsigned int> dist_image_size = disp_maps[i * 3].GetImageSize();

		mxImage<double> dist_image;
		dist_image.CopyImageInfo(disp_maps[i * 3]);
		dist_image.SetImageSize(dist_image_size);

		#pragma omp parallel for
		for (int z = 0; z < static_cast<int>(dist_image_size[2]); ++z)
		{
			for (int y = 0; y < static_cast<int>(dist_image_size[1]); ++y)
			{
				for (int x = 0; x < static_cast<int>(dist_image_size[0]); ++x)
				{
					double dx = disp_maps[i * 3](x, y, z);
					double dy = disp_maps[i * 3 + 1](x, y, z);
					double dz = disp_maps[i * 3 + 2](x, y, z);

					dist_image(x, y, z) = sqrt(dx * dx + dy * dy + dz * dz);
				}
			}
		}

		std::string output_path = disp_prefix + "_norm" + stringUtils::num2str(i) + "." + output_format;
		if (!ImageHelper::WriteImage<double, double>(dist_image, output_path.c_str())) {
			std::cerr << "fails to write image to " << output_path << std::endl; exit(-1);
		}
	}
}


/************************************************************************/
/* BD Run Single-Scale with different options                           */
/************************************************************************/

template <typename T>
void BD_run_global(const std::vector<std::string>& image_paths, std::string root_folder, std::string name, int level, int scale, std::string label_prefix, std::string disp_prefix, std::string output_format)
{
	JAT_bd_tester aux;
	aux.SetDetector(root_folder.c_str(), name.c_str(), level);

	std::string info_path = aux.GetDetectorInfoPath(scale);
	JAT_bd_detector_info info;
	if (!info.Read(info_path.c_str())) {
		std::cerr << "fails to read detector info from " << info_path << std::endl; exit(-1);
	}

	// set timer for image loading
	boost::timer::cpu_timer timer;

	std::vector< mxImage<T> > images;
	read_images(image_paths, images);

	std::cout << "Loading Time: " << timer.format();

	// compute box
	mxImage<T> resample_header;
	mxImageUtils::GetResampleHeader(images[0], info.spacing, resample_header);
	vect3<unsigned int> resample_image_size = resample_header.GetImageSize();
	vect3<int> sp(0, 0, 0), ep(resample_image_size[0] - 1, resample_image_size[1] - 1, resample_image_size[2] - 1);

	vect3<double> sp_world, ep_world;
	mxImageUtils::Voxel2World(resample_header, sp, sp_world);
	mxImageUtils::Voxel2World(resample_header, ep, ep_world);

	// set mask
	std::auto_ptr< mxImage<unsigned char> > mask;
	if (info.discard_background)
	{
		mask.reset(new mxImage<unsigned char>);
		mxImageUtils::ExtractBodyFromCT<T, T>(images[0], *mask, NULL);
		aux.SetMask(mask.get(), true);
	}
	else
		aux.SetMask(NULL, false);

	// compute label and displacement maps
	timer.start();

	std::vector< mxImage<double> > label_maps, disp_maps;
	aux.ComputeSingleResolutionMaps(images, scale, sp_world, ep_world, &label_maps, &disp_maps);

	std::cout << "Detection Time: " << timer.format();

	// output label and displacement maps
	write_label_maps(label_maps, label_prefix, output_format);
	write_disp_maps(disp_maps, disp_prefix, output_format);
	write_norm_maps(disp_maps, disp_prefix, output_format);
}

template <typename T>
void BD_run_surface(const std::vector<std::string>& image_paths, std::string root_folder, std::string name, int level, int scale, std::string label_prefix, std::string disp_prefix, std::string output_format, std::string surface_path, const vect3<double>& pad_size)
{
	JAT_bd_tester aux;
	aux.SetDetector(root_folder.c_str(), name.c_str(), level);

	// load detector info
	std::string info_path = aux.GetDetectorInfoPath(scale);
	JAT_bd_detector_info info;
	if (!info.Read(info_path.c_str())) {
		std::cerr << "fails to read detector info from " << info_path << std::endl; exit(-1);
	}

	boost::timer::cpu_timer timer;

	timer.start();
	
	// read intensity images
	std::vector< mxImage<T> > images;
	read_images(image_paths, images);

	timer.stop();
	std::cout << "Loading Time: " << timer.format();

	// compute bounding box
	mxImage<T> resample_header;
	mxImageUtils::GetResampleHeader(images[0], info.spacing, resample_header);

	vect3<int> voxel_pad;
	for (int i = 0; i < 3; ++i)
		voxel_pad[i] = static_cast<int>(pad_size[i] / info.spacing[i] + 0.5);

	Surface surf;
	if (!SurfaceUtils::LoadSurface(surface_path.c_str(), surf)) {
		std::cerr << "fails to load surface from " << surface_path << std::endl; exit(-1);
	}

	vect3<int> sp, ep;
	SurfaceUtils::BoundingBox(surf, resample_header, sp, ep);
	for (int i = 0; i < 3; ++i) {
		sp[i] -= voxel_pad[i];
		ep[i] += voxel_pad[i];
	}

	vect3<double> sp_world, ep_world;
	mxImageUtils::Voxel2World(resample_header, sp, sp_world);
	mxImageUtils::Voxel2World(resample_header, ep, ep_world);

	// set mask
	std::auto_ptr< mxImage<unsigned char> > mask;
	if (info.discard_background)
	{
		mask.reset(new mxImage<unsigned char>);
		mxImageUtils::ExtractBodyFromCT<T,T>(images[0], *mask, NULL);
		aux.SetMask(mask.get(), true);
	}
	else
		aux.SetMask(NULL, false);

	timer.start();
	// compute label and displacement maps
	std::vector< mxImage<double> > label_maps, disp_maps;
	aux.ComputeSingleResolutionMaps(images, scale, sp_world, ep_world, &label_maps, &disp_maps);

	timer.stop();
	std::cout << "Predict Time: " << timer.format();

	// output label and displacement maps
	write_label_maps(label_maps, label_prefix, output_format);
	write_disp_maps(disp_maps, disp_prefix, output_format);
	write_norm_maps(disp_maps, disp_prefix, output_format);
}

template <typename T>
void BD_run_exp(const std::vector<std::string>& image_paths, std::string root_folder, std::string name, int level, int scale, std::string label_prefix, std::string disp_prefix, std::string output_format, std::string gt_path, const vect3<int>& pad_size)
{
	JAT_bd_tester aux;
	aux.SetDetector(root_folder.c_str(), name.c_str(), level);

	// read detector info
	std::string info_path = aux.GetDetectorInfoPath(scale);
	JAT_bd_detector_info info;
	if (!info.Read(info_path.c_str())) {
		std::cerr << "fails to read detector info from " << info_path << std::endl; exit(-1);
	}

	// read and re-sample mask image
	mxImage<unsigned char> mask_image;
	{
		mxImage<unsigned char> tmp_image;
		if (!ImageHelper::ReadImage<T, unsigned char>(gt_path.c_str(), tmp_image)) {
			std::cerr << "fails to read image from " << gt_path << std::endl; exit(-1);
		}
		mxImageUtils::Resample(tmp_image, info.spacing, mask_image, IT_NEAREST_NEIGHBOR_INTERPOLATION);
	}

	// read original input images
	std::vector< mxImage<T> > images;
	read_images(image_paths, images);

	// find bounding box
	vect3<int> sp, ep;
	mxImageUtils::BoundingBox(mask_image, sp, ep);

	vect3<unsigned int> resample_image_size = mask_image.GetImageSize();
	for (int i = 0; i < 3; ++i)
	{
		sp[i] -= pad_size[i];
		if (sp[i] < 0)
			sp[i] = 0;

		ep[i] += pad_size[i];
		if (ep[i] >= resample_image_size[i])
			ep[i] = resample_image_size[i] - 1;
	}

	vect3<double> sp_world, ep_world;
	mxImageUtils::Voxel2World(mask_image, sp, sp_world);
	mxImageUtils::Voxel2World(mask_image, ep, ep_world);

	// set mask
	std::auto_ptr< mxImage<unsigned char> > mask;
	if (info.discard_background)
	{
		mask.reset(new mxImage<unsigned char>);
		mxImageUtils::ExtractBodyFromCT<T,T>(images[0], *mask, NULL);
		aux.SetMask(mask.get(), true);
	}
	else
		aux.SetMask(NULL, false);

	// compute label and displacement maps
	std::vector< mxImage<double> > label_maps, disp_maps;
	aux.ComputeSingleResolutionMaps(images, scale, sp_world, ep_world, &label_maps, &disp_maps);

	// output label and displacement maps
	write_label_maps(label_maps, label_prefix, output_format);
	write_disp_maps(disp_maps, disp_prefix, output_format);
	write_norm_maps(disp_maps, disp_prefix, output_format);
}

template <typename T>
void BD_Run(const std::vector<std::string>& image_paths, std::string root_folder, std::string name, int level, int scale, std::string label_prefix, std::string disp_prefix, std::string output_format, boost::program_options::variables_map& vm)
{
	boost::timer::auto_cpu_timer timer;

	if (vm.count("exp"))
	{
		std::string gt_path = BoostCmdHelper::Get<std::string>(vm, "exp");
		vect3<int> pad_size = stringUtils::ParseVect3<int>(BoostCmdHelper::Get<std::string>(vm, "pad"), ',');
		BD_run_exp<T>(image_paths, root_folder, name, level, scale, label_prefix, disp_prefix, output_format, gt_path, pad_size);
	}
	else if (vm.count("surface"))
	{
		std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "surface");
		vect3<double> pad_size = stringUtils::ParseVect3<double>(BoostCmdHelper::Get<std::string>(vm, "pad"), ',');
		BD_run_surface<T>(image_paths, root_folder, name, level, scale, label_prefix, disp_prefix, output_format, surf_path, pad_size);
	}
	else if (vm.count("global"))
	{
		BD_run_global<T>(image_paths, root_folder, name, level, scale, label_prefix, disp_prefix, output_format);
	}else {
		err_message("other options not implemented yet");
	}
}

/************************************************************************/
/* BD Run multi-resolution                                              */
/************************************************************************/

template <typename T>
void BD_run_multi(const std::vector<std::string>& image_paths, std::string root_folder, const MultiResolutionConfigFile& config, std::string label_prefix, std::string disp_prefix, std::string out_prefix, std::string output_format)
{
	// read input images
	std::vector< mxImage<T> > images;
	read_images(image_paths, images);

	// load multi-resolution detectors
	std::vector< std::vector<JAT_singleres_detector> > multires_detector(config.num_resolutions);
	for (int i = 0; i < config.num_resolutions; ++i)
	{
		const SingleResolutionSetting& single = config.singleres_predict_config[i];
		multires_detector[i].resize(single.names.size());

		for (size_t j = 0; j < single.names.size(); ++j)
		{
			multires_detector[i][j].name = single.names[j];
			multires_detector[i][j].level = single.levels[j];
			multires_detector[i][j].scale = single.scale;
			multires_detector[i][j].root_folder = root_folder;

			if (!multires_detector[i][j].LoadDetector())
				err_message("fails to load detector");
		}
	}

	// compute global mask if want
	std::auto_ptr< mxImage<unsigned char> > mask;
	if (multires_detector[0][0].info.discard_background)
	{
		mask.reset(new mxImage<unsigned char>);
		mxImageUtils::ExtractBodyFromCT<T, T>(images[0], *mask, NULL);
	}

	// load reference surfaces
	std::vector< Surface > reference_surfaces( config.surf_paths.size() );
	for (size_t i = 0; i < config.surf_paths.size(); ++i)
	{
		if (!SurfaceUtils::LoadSurface(config.surf_paths[i].c_str(), reference_surfaces[i])) {
			std::cerr << "fails to load surface from " << config.surf_paths[i] << std::endl;
			exit(-1);
		}
	}

	// prepare test mask provider
	TestMaskProvider mask_provider;
	mask_provider.SetBoundarySurfaces(&reference_surfaces[0], reference_surfaces.size());
	mask_provider.SetDeformationConfigs(config.singleres_deform_config);

	// hierarchical label image and deform surface
	JAT_bd_multires_tester<T> aux;
	aux.SetMask(mask.get());
	aux.SetOutputInfo(config, label_prefix, disp_prefix, out_prefix, output_format);
	aux.Contour(images, multires_detector, mask_provider);

	// write final label maps and displacement maps if want
	for (size_t i = 0; i < config.bdids.size(); ++i)
	{
		std::string output_path = out_prefix + "_" + config.bdids[i] + "_final.vtk";
		const Surface* surf = mask_provider.GetBoundarySurface(i);
		if (!SurfaceUtils::SaveSurface(*surf, output_path.c_str())) 
		{
			std::cerr << "fails to save final surface to " << output_path << std::endl;
			exit(-1);
		}
	}
}

/************************************************************************/
/* Other auxiliary functions                                             */
/************************************************************************/

ImageHelper::PixelType GetCompatiblePixelType(const std::vector< std::string >& image_paths)
{
	ImageHelper::PixelType type;
	for (size_t i = 0; i < image_paths.size(); ++i)
	{
		if (i == 0)
			type = ImageHelper::ReadPixelType(image_paths[i].c_str());
		else
		{
			ImageHelper::PixelType tmp_type = ImageHelper::ReadPixelType(image_paths[i].c_str());
			if (tmp_type > type)
				type = tmp_type;
		}
	}
	return type;
}

/************************************************************************/
/* Main function                                                        */
/************************************************************************/

int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display help information");
		generic.add_options()("single", "single-resolution detection");
		generic.add_options()("multi", "multi-resolution detection");

		options_description params("Input parameters");
		params.add_options()("in", value< std::vector<std::string> >()->multitoken(), "input source images");
		params.add_options()("root", value<std::string>(), "root folder");
		params.add_options()("name", value<std::string>(), "detector name");
		params.add_options()("level", value<int>(), "context level");
		params.add_options()("scale", value<int>(), "scale index");
		params.add_options()("label", value<std::string>(), "output label prefix");
		params.add_options()("disp", value<std::string>(), "output disp prefix");
		params.add_options()("out", value<std::string>(), "output prefix");
		params.add_options()("format", value<std::string>(), "output image format");
		params.add_options()("pad", value<std::string>(), "pad size mm^3");
		params.add_options()("config", value<std::string>(), "configuration file");
		params.add_options()("multi_config", value<std::string>(), "multi-resolution configuration");

		params.add_options()("global", "apply the model on the entire image (optional)");
		params.add_options()("mask", value<std::string>(), "mask image (optional)");
		params.add_options()("exp", value<std::string>(), "ground truth mask image (optional)");
		params.add_options()("surface", value<std::string>(), "initialized surface (optional)");

		options_description cmd_options;
		cmd_options.add(generic).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			std::cerr << cmd_options << std::endl;
			print_usage();
			return -1;
		}

		if (vm.count("single"))
		{
			std::vector< std::string > imagePaths;
			if (vm.count("in")) {
				imagePaths = vm["in"].as< std::vector<std::string> >();
			}
			else {
				std::cerr << "in option missing" << std::endl; exit(-1);
			}

			std::string root_folder = BoostCmdHelper::Get<std::string>(vm, "root");
			std::string name = BoostCmdHelper::Get<std::string>(vm, "name");
			int level = BoostCmdHelper::Get<int>(vm, "level");
			int scale = BoostCmdHelper::Get<int>(vm, "scale");
			std::string label_prefix = BoostCmdHelper::Get<std::string>(vm, "label");
			std::string disp_prefix = BoostCmdHelper::Get<std::string>(vm, "disp");
			std::string format = BoostCmdHelper::Get<std::string>(vm, "format");

			// pick the most compatible pixel type
			ImageHelper::PixelType type = GetCompatiblePixelType(imagePaths);

			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: BD_Run<unsigned char>(imagePaths, root_folder, name, level, scale, label_prefix, disp_prefix, format, vm);  break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: BD_Run<unsigned>(imagePaths, root_folder, name, level, scale, label_prefix, disp_prefix, format, vm);  break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: BD_Run<unsigned short>(imagePaths, root_folder, name, level, scale, label_prefix, disp_prefix, format, vm);  break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: BD_Run<short>(imagePaths, root_folder, name, level, scale, label_prefix, disp_prefix, format, vm);  break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: BD_Run<unsigned int>(imagePaths, root_folder, name, level, scale, label_prefix, disp_prefix, format, vm);  break;
			case BRIC::IDEA::FISH::ImageHelper::INT: BD_Run<int>(imagePaths, root_folder, name, level, scale, label_prefix, disp_prefix, format, vm);  break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: BD_Run<unsigned long>(imagePaths, root_folder, name, level, scale, label_prefix, disp_prefix, format, vm);  break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: BD_Run<long>(imagePaths, root_folder, name, level, scale, label_prefix, disp_prefix, format, vm);  break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: BD_Run<float>(imagePaths, root_folder, name, level, scale, label_prefix, disp_prefix, format, vm);  break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: BD_Run<double>(imagePaths, root_folder, name, level, scale, label_prefix, disp_prefix, format, vm);  break;
			default: err_message("unrecognized pixel type");
			}
		}
		else if (vm.count("multi"))
		{
			// read image paths
			std::vector< std::string > imagePaths;
			if (vm.count("in")) {
				imagePaths = vm["in"].as< std::vector<std::string> >();
			}
			else {
				std::cerr << "in option missing" << std::endl; exit(-1);
			}

			// read input parameters
			std::string root_folder = BoostCmdHelper::Get<std::string>(vm, "root");
			std::string config_path = BoostCmdHelper::Get<std::string>(vm, "config");
			std::string label_prefix = BoostCmdHelper::Get<std::string>(vm, "label");
			std::string disp_prefix = BoostCmdHelper::Get<std::string>(vm, "disp");
			std::string out_prefix = BoostCmdHelper::Get<std::string>(vm, "out");
			std::string format = BoostCmdHelper::Get<std::string>(vm, "format");

			MultiResolutionConfigFile config;
			if (!config.Read(config_path.c_str()))
				err_message("fail to load multi-resolution config file");

			// pick the most compatible pixel type
			ImageHelper::PixelType type = GetCompatiblePixelType(imagePaths);

			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: BD_run_multi<unsigned char>(imagePaths, root_folder, config, label_prefix, disp_prefix, out_prefix, format);  break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: BD_run_multi<unsigned>(imagePaths, root_folder, config, label_prefix, disp_prefix, out_prefix, format);  break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: BD_run_multi<unsigned short>(imagePaths, root_folder, config, label_prefix, disp_prefix, out_prefix, format);  break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: BD_run_multi<short>(imagePaths, root_folder, config, label_prefix, disp_prefix, out_prefix, format);  break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: BD_run_multi<unsigned int>(imagePaths, root_folder, config, label_prefix, disp_prefix, out_prefix, format);  break;
			case BRIC::IDEA::FISH::ImageHelper::INT: BD_run_multi<int>(imagePaths, root_folder, config, label_prefix, disp_prefix, out_prefix, format);  break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: BD_run_multi<unsigned long>(imagePaths, root_folder, config, label_prefix, disp_prefix, out_prefix, format);  break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: BD_run_multi<long>(imagePaths, root_folder, config, label_prefix, disp_prefix, out_prefix, format);  break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: BD_run_multi<float>(imagePaths, root_folder, config, label_prefix, disp_prefix, out_prefix, format);  break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: BD_run_multi<double>(imagePaths, root_folder, config, label_prefix, disp_prefix, out_prefix, format);  break;
			default: err_message("unrecognized pixel type");
			}

		}
		else if (vm.count("multi_config"))
		{
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "multi_config");

			if (!MultiResolutionConfigFile::WriteDefault(out_path.c_str()))
				err_message("fail to output sample multi-resolution config file");
		}
		else
			err_message("use single or multi option");

	}
	catch (boost::program_options::error& exp) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}

