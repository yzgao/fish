//
//  BDModel.cpp
//  FISH
//
//  Created by Yaozong Gao on 02/13/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//


#include "stdafx.h"
#include "extern/BoostHelper.h"
#include "deform/BDModel.h"

using namespace BRIC::IDEA::FISH;

void print_usage()
{
	std::cerr << "[ Binary Segmentation with BDModel ]" << std::endl;
	std::cerr << "BDModel --label prefix --disp prefix --format str --idx val --init path {--centering} --config path --out path\n" << std::endl;

	std::cerr << "[ Initialize Surface with Classification Mass Center ]" << std::endl;
	std::cerr << "BDModel --prob path --init path --out path\n" << std::endl;

	std::cerr << "[ Output Sample Config File ]" << std::endl;
	std::cerr << "BDModel --sample path\n" << std::endl;
}


void initialize_with_classification_mass_center(std::string prob_path, std::string init_path, std::string out_path)
{
	mxImage < double > prob_map;
	if (!ImageHelper::ReadImage<double,double>(prob_path.c_str(), prob_map))
	{
		std::cerr << "fails to read image from " << prob_path << std::endl;
		exit(-1);
	}

	Surface surf;
	if (!SurfaceUtils::LoadSurface(init_path.c_str(), surf))
	{
		std::cerr << "fails to load surface from " << init_path << std::endl;
		exit(-1);
	}

	vect3<unsigned int> image_size = prob_map.GetImageSize();

	vect3<float> center(0, 0, 0);
	float mass = 0;

	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
			{
				float prob = static_cast<float>(prob_map(x, y, z));

				if (prob > 0.1)
				{
					mass += prob;
					center[0] += (x * prob);
					center[1] += (y * prob);
					center[2] += (z * prob);
				}
			}
		}
	}

	center /= mass;
	
	std::cout << center << std::endl;

	mxImageUtils::Voxel2World(prob_map, center);

	surf.MoveTo(center);

	if (!SurfaceUtils::SaveSurface(surf, out_path.c_str()))
	{
		std::cerr << "fails to save surface to " << out_path << std::endl;
		exit(-1);
	}
}


int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display help information");

		options_description params("Input parameters");
		params.add_options()("label", value<std::string>(), "output label prefix");
		params.add_options()("disp", value<std::string>(), "output disp prefix");
		params.add_options()("idx", value<int>(), "index for the target organ");
		params.add_options()("out", value<std::string>(), "output image path");
		params.add_options()("init", value<std::string>(), "initial surface path");
		params.add_options()("config", value<std::string>(), "config path");
		params.add_options()("format", value<std::string>(), "file format");
		params.add_options()("sample", value<std::string>(), "sample config file");
		params.add_options()("centering", "put initial model on the image center");
		params.add_options()("prob", value<std::string>(), "probability map");

		options_description cmd_options;
		cmd_options.add(generic).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			std::cerr << cmd_options << std::endl;
			print_usage();
			return -1;
		}

		if (vm.count("sample"))
		{
			std::string sample_path = BoostCmdHelper::Get<std::string>(vm, "sample");
			if (!BDModel_config::write_default(sample_path.c_str()))
			{
				std::cerr << "fails to write sample config file to " << sample_path << std::endl;
				exit(-1);
			}
			return true;
		}


		if (vm.count("prob"))
		{
			std::string prob_path = BoostCmdHelper::Get<std::string>(vm, "prob");
			std::string init_path = BoostCmdHelper::Get<std::string>(vm, "init");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			initialize_with_classification_mass_center(prob_path, init_path, out_path);

			return true;
		}


		std::string label_prefix = BoostCmdHelper::Get<std::string>(vm, "label");
		std::string disp_prefix = BoostCmdHelper::Get<std::string>(vm, "disp");
		int organ_idx = BoostCmdHelper::Get<int>(vm, "idx");
		std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
		std::string init_path = BoostCmdHelper::Get<std::string>(vm, "init");
		std::string config_path = BoostCmdHelper::Get<std::string>(vm, "config");
		std::string file_format = BoostCmdHelper::Get<std::string>(vm, "format");
	
		BDModel_config config;
		if (!config.parse(config_path.c_str())) {
			std::cerr << "fail to read config file from " << config_path << std::endl; exit(-1);
		}

		mxImage<double> prob_map;
		std::string prob_path = label_prefix + stringUtils::num2str(organ_idx) + "." + file_format;
		if (!ImageHelper::ReadImage<double,double>(prob_path.c_str(), prob_map)) {
			std::cerr << "fail to read prob image from " << prob_path << std::endl; exit(-1);
		}

		std::vector< mxImage<double> > disp_maps;
		disp_maps.resize(3);
		for (size_t i = 0; i < disp_maps.size(); ++i)
		{
			std::string disp_path = disp_prefix + stringUtils::num2str(organ_idx * 3 + i) + "." + file_format;
			if (!ImageHelper::ReadImage<double,double>(disp_path.c_str(), disp_maps[i])) {
				std::cerr << "fail to read displacement image from " << disp_path << std::endl; exit(-1);
			}
		}

		Surface surf;
		if (!SurfaceUtils::LoadSurface(init_path.c_str(), surf)) {
			std::cerr << "fail to load surface from " << init_path << std::endl; exit(-1);
		}

		if (vm.count("centering"))
		{
			vect3<double> world_center = mxImageUtils::WorldCenter(disp_maps[0]);
			surf.MoveTo(world_center.to<float>());
		}

		BDModel::deform(prob_map, &disp_maps[0], config, surf);

		if (!SurfaceUtils::SaveSurface(surf, output_path.c_str())) {
			std::cerr << "fail to save surface to " << output_path << std::endl; exit(-1);
		}
	}
	catch (boost::program_options::error& exp) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}