//
//  Temporal2DLandmarkTrain.cpp
//  FISH
//
//  Created by Yaozong Gao on 10/18/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//

#include "stdafx.h"
#include "detector/landmark/Temporal2DLandmarkTrainer.h"

using namespace BRIC::IDEA::FISH;

void print_usage()
{
	std::cerr << "[ Temporal 2D Landmark Train ]" << std::endl;
	std::cerr << "Temporal2DLandmarkTrain --file some.ini --type uchar --seed 1\n" << std::endl;

	std::cerr << "[ Temporal 2D Landmark Example INI ]" << std::endl;
	std::cerr << "Temporal2DLandmarkTrain --example some.ini\n" << std::endl;
}


template <typename T>
void Temporal2DLandmarkTrain(std::string iniFile, boost::program_options::variables_map& vm)
{
	Temporal2DLandmarkTrainer<T> trainer;
	if (!trainer.LoadConfigFile(iniFile.c_str())) {
		std::cerr << "fail to load config file from " << iniFile << std::endl; exit(-1);
	}

	const Temporal2DLandmarkParameters& params = trainer.GetParameters();
	std::auto_ptr<IFeatureSpace> fs = trainer.CreateFeatureSpace();

	Random random;
	if (vm.count("seed")) {
		int seed = vm["seed"].as<int>();
		random.Seed(seed);
	}

	trainer.CreateOutputFolder();
	trainer.SaveDetectionInfo();

	for (unsigned int i = 0; i < params.rfParameters.forestParams.numTrees; ++i)
	{
		std::cout << "############### start to train tree " << i+1 << "/" << params.rfParameters.forestParams.numTrees << " ###############" << std::endl;

		typedef typename Temporal2DLandmarkTrainer<T>::TreeType TreeType;
		std::auto_ptr<TreeType> tree = trainer.TrainTree(random, fs.get());

		if (!trainer.SaveTree(tree.get())) {
			std::cerr << "fails to save tree to disk" << std::endl; exit(-1);
		}

		if (!trainer.SaveFeatureSpace(fs.get(), tree->GetUniqueID())) {
			std::cerr << "fails to save feature space to disk" << std::endl; exit(-1);
		}
	}
}

int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display help information");

		options_description functions("Function options");
		functions.add_options()("example", value<std::string>(), "create an example INI file for training");
		functions.add_options()("file", value<std::string>(), "pass an INI file for training");

		options_description params("Parameters options");
		params.add_options()("seed", value<int>(), "seed parameter");
		params.add_options()("type", value<std::string>(), "pixel type");

		options_description cmd_options;
		cmd_options.add(generic).add(functions).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			std::cerr << cmd_options << std::endl;
			print_usage();
			return -1;
		}

		if (vm.count("example")) {
			std::string path = vm["example"].as<std::string>();
			if( !Temporal2DLandmarkParameters::WriteDefaultConfig( path.c_str() ) ) {
				std::cerr << "fails to write a default config to " << path << std::endl; return -1;
			}
			return 0;
		}

		std::string iniFile = vm["file"].as<std::string>();
		std::string pixeltype = vm["type"].as<std::string>();

		if (pixeltype == "uchar") {
			Temporal2DLandmarkTrain<unsigned char>(iniFile, vm);
		}else if (pixeltype == "char") {
			Temporal2DLandmarkTrain<char>(iniFile, vm);
		}else if (pixeltype == "ushort") {
			Temporal2DLandmarkTrain<unsigned short>(iniFile, vm);
		}else if (pixeltype == "short") {
			Temporal2DLandmarkTrain<short>(iniFile, vm);
		}else if (pixeltype == "uint") {
			Temporal2DLandmarkTrain<unsigned int>(iniFile, vm);
		}else if (pixeltype == "int") {
			Temporal2DLandmarkTrain<int>(iniFile, vm);
		}else if (pixeltype == "float") {
			Temporal2DLandmarkTrain<float>(iniFile, vm);
		}else if (pixeltype == "double") {
			Temporal2DLandmarkTrain<double>(iniFile, vm);
		}else {
			std::cerr << "invalid pixel type" << std::endl;
			return -1;
		}
	} catch ( boost::program_options::error& exp ) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}