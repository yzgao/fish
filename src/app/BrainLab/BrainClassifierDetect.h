//
//  BrainClassifierDetect.h
//  FISH
//
//  Created by Yaozong Gao on 9/10/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __BrainClassifierDetect_h__
#define __BrainClassifierDetect_h__

#include "stdafx.h"
#include "forest/RandomForest.h"
#include "feature/haar3d/Haar3DFeatureSpace.h"
#include "extern/ImageHelper.h"
#include "mesh/PDMUtils.h"
#include "detector/DetectorIO.h"

namespace BRIC { namespace IDEA { namespace FISH {


// classifier detection parameters
class BrainClassifierDetectionParameters
{
public:
	std::string root_folder;
	std::string name;
};


// brain MRI classifier
struct BrainClassifier
{
	std::auto_ptr< Forest<MemoryAxisAlignedWeakLearner, HistogramStatisticsAggregator> > forest;
	FeatureSpaceArray<Haar3DFeatureSpace> fss;
	std::vector< std::vector<int> > lm_idxs;
	vect3<double> spacing;
	int num_landmarks;
};


// feature functor
class LandmarkFeatureFunctor
{
public:
	LandmarkFeatureFunctor() : m_int_image(NULL), m_space(NULL), m_lm_idxs(NULL) {}

	void Initialize(const Haar3DFeatureSpace& space, const std::vector<int>& lm_idxs) 
	{
		m_space = &space;
		m_lm_idxs = &lm_idxs;
		m_patch_size = space.GetPatchSize(0);
	}

	void SetImage(const mxImage<double>& image, const PDM& shape)
	{
		m_int_image = &image;
		m_shape = &shape;
	}

	double operator() (unsigned int feature_index) const
	{
		const std::vector<float>& pts = m_shape->GetPoints();
		int lm_idx = (*m_lm_idxs)[feature_index];
		
		vect3<float> pt(pts[lm_idx * 3], pts[lm_idx * 3 + 1], pts[lm_idx * 3 + 2]);
		vect3<unsigned int> voxel;
		mxImageUtils::World2Voxel(*m_int_image, pt, voxel);

		return m_space->GetFilters()[feature_index].GetResponseFromImage(*m_int_image, voxel, m_patch_size);
	}

	void PreprocessInput(mxImage<double>& image) {
		Haar3DBasics::IntegralImage(image);
	}

private:

	const mxImage<double>* m_int_image;
	vect3<unsigned int> m_patch_size;

	const Haar3DFeatureSpace* m_space;
	const PDM* m_shape;
	const std::vector<int>* m_lm_idxs;
};


// classifier detection module
template <typename T>
class BrainClassifierDetection
{
public:

	typedef MemoryAxisAlignedWeakLearner W;
	typedef HistogramStatisticsAggregator S;
	typedef Tree<W, S> TreeType;
	typedef Forest<W, S> ForestType;

public:

	void SetParameters(const BrainClassifierDetectionParameters& params)  { m_params = params; }
	void ReadDetector(BrainClassifier& classifier);
	std::vector<double> Classify(const mxImage<T>& image, const PDM& shape, BrainClassifier& classifier);

private:

	void find_tuples(std::string folder, std::vector<std::string>& filenames);
	void read_lm_idxs(std::string file, std::vector<int>& lm_idxs);
	void read_detector_info(const std::string& detectorFolder, vect3<double>& spacing, int& numLandmarks);
	void extract_landmark_features(const mxImage<double>& image, const PDM& shape, const Haar3DFeatureSpace* fs, const std::vector<int>& lm_idxs, std::vector<double>& data);

private:

	BrainClassifierDetectionParameters m_params;
};


//////////////////////////////////////////////////////////////////////////


template <typename T>
std::vector<double> BrainClassifierDetection<T>::Classify(const mxImage<T>& image, const PDM& shape, BrainClassifier& classifier)
{
	mxImage<double> normalized_image;
	mxImageUtils::MRIntensityNormalization(image, 0.997, normalized_image);

	mxImage<double> resample_image;
	mxImageUtils::Resample(normalized_image, classifier.spacing, resample_image);

	std::vector<LandmarkFeatureFunctor> functors( classifier.forest->GetTreeNumber() );
	functors[0].PreprocessInput(resample_image);

	for (size_t i = 0; i < functors.size(); ++i)
	{
		functors[i].Initialize(classifier.fss[i], classifier.lm_idxs[i]);
		functors[i].SetImage(resample_image, shape);
	}

	std::vector<double> probs;
	ForestClassifier::Classify2(classifier.forest.get(), functors, probs);

	return probs;
}

template <typename T>
void BrainClassifierDetection<T>::read_detector_info(const std::string& detectorFolder, vect3<double>& spacing, int& numLandmarks)
{
	std::string infoPath = detectorFolder + stringUtils::Slash() + "info.ini";

	boost::property_tree::ptree pt;
	try {
		boost::property_tree::ini_parser::read_ini(infoPath, pt);
		std::string stringSpacing = pt.get<std::string>("Info.Resolution");
		spacing = stringUtils::ParseVect3<double>(stringSpacing);
		numLandmarks = pt.get<int>("Info.NumLandmarks");
	}
	catch (std::runtime_error& error) {
		std::cerr << error.what() << std::endl;
		exit(-1);
	}
}


template <typename T>
void BrainClassifierDetection<T>::find_tuples(std::string folder, std::vector<std::string>& filenames)
{
	const char* treePrefix = "Tree_";
	const char* treeExtension = ".bin";
	const char* fsExtension = ".fd";
	const char* lmExtension = ".txt";

	boost::filesystem::directory_iterator end_iter;
	for (boost::filesystem::directory_iterator iter(folder); iter != end_iter; ++iter) {

		// not a directory
		if (!boost::filesystem::is_directory(iter->status())) {

			std::string filename = iter->path().filename().string();
			std::string extension = iter->path().extension().string();

			if (filename.find(treePrefix) != std::string::npos && extension == std::string(treeExtension)) {

				std::string fsPath = iter->path().parent_path().string() + stringUtils::Slash() + iter->path().stem().string() + fsExtension;
				if (!boost::filesystem::exists(fsPath))
					continue;

				std::string lmPath = iter->path().parent_path().string() + stringUtils::Slash() + iter->path().stem().string() + lmExtension;
				if (!boost::filesystem::exists(lmPath))
					continue;

				filenames.push_back(iter->path().stem().string());
			}
		}
	}
}


template <typename T>
void BrainClassifierDetection<T>::read_lm_idxs(std::string file, std::vector<int>& lm_idxs)
{
	std::vector< std::string > lines;
	if (!stringUtils::ReadLines(file.c_str(), lines))
	{
		std::cerr << "fails to read lines from file " << file << std::endl;
		exit(-1);
	}

	lm_idxs.resize(lines.size());
	for (size_t i = 0; i < lm_idxs.size(); ++i)
		lm_idxs[i] = stringUtils::str2num<int>(lines[i]);
}


template <typename T>
void BrainClassifierDetection<T>::ReadDetector(BrainClassifier& classifier)
{
	std::string detector_folder = m_params.root_folder + stringUtils::Slash() + m_params.name;

	read_detector_info(detector_folder, classifier.spacing, classifier.num_landmarks);

	std::vector<std::string> filenames;
	find_tuples(detector_folder, filenames);

	classifier.forest.reset(new ForestType);
	classifier.fss.clear();
	classifier.lm_idxs.resize(filenames.size());

	for (size_t i = 0; i < filenames.size(); ++i)
	{
		std::string prefix = detector_folder + stringUtils::Slash() + filenames[i];
		std::string tree_path = prefix + ".bin";
		std::string fs_path = prefix + ".fd";
		std::string lm_path = prefix + ".txt";

		std::auto_ptr<typename ForestType::TreeType> tree = DetectorIO::ReadTree<typename ForestType::TreeType>(tree_path.c_str());
		std::auto_ptr<Haar3DFeatureSpace> fs = DetectorIO::ReadFeatureSpace<Haar3DFeatureSpace>(fs_path.c_str());
		read_lm_idxs(lm_path, classifier.lm_idxs[i]);

		classifier.forest->AddTree(tree);
		classifier.fss.AddFeatureSpace(fs.release());
	}

}




} } }


#endif