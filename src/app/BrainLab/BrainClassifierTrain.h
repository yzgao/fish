//
//  BrainClassifierTrain.h
//  FISH
//
//  Created by Yaozong Gao on 9/10/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __BrainClassifierTrain_h__
#define __BrainClassifierTrain_h__

#include "stdafx.h"
#include "forest/RandomForest.h"
#include "feature/haar3d/Haar3DFeatureSpace.h"
#include "extern/ImageHelper.h"
#include "mesh/PDMUtils.h"

namespace BRIC { namespace IDEA { namespace FISH {


// Brain Classifier Parameters
struct BrainClassifierParameters
{
	std::string name;
	std::string annot_path;
	std::string root_folder;

	vect3<double> spacing;

	std::vector<unsigned int> filter_sizes;
	vect3<unsigned int> patch_size;

	ForestTrainingParameters forest_params;

public:

	bool Load(const char* file);
	static bool WriteDefaultConfig(const char* file);
	static bool WriteDefaultAnnot(const char* file);

};


// Brain Classifier Training Module
template <typename T>
class BrainClassifierTrain
{
public:

	typedef MemoryAxisAlignedWeakLearner W;
	typedef HistogramStatisticsAggregator S;
	typedef Tree<W, S> TreeType;
	typedef Forest<W, S> ForestType;

	bool LoadConfigFile(std::string iniFile);
	const BrainClassifierParameters& GetParameters() const { return m_params;  }
	std::string GetDetectorFolder() const { return m_params.root_folder + stringUtils::Slash() + m_params.name;  }
	std::auto_ptr<Haar3DFeatureSpace> CreateFeatureSpace();
	std::auto_ptr<TreeType> TrainTree(Random& random, Haar3DFeatureSpace* fs, std::vector<int>& lm_idxs);
	bool CreateOutputFolder() const;
	bool SaveTree(const TreeType* tree) const;
	bool SaveFeatureSpace(const Haar3DFeatureSpace* fs, const std::vector<int>& lm_idxs, double treeID) const;
	bool SaveDetectionInfo() const;

private:

	bool load_annot(std::string file);
	void extract_landmark_features(const mxImage<double>& image, const PDM& shape, const Haar3DFeatureSpace* fs, const std::vector<int>& lm_idxs, double* data);

private:

	std::vector< std::string > m_image_paths;
	std::vector< int > m_labels;
	std::vector< PDM > m_shapes;

	int m_classNumber;

	BrainClassifierParameters m_params;
};



//////////////////////////////////////////////////////////////////////////


template <typename T>
bool BrainClassifierTrain<T>::LoadConfigFile(std::string iniFile)
{
	if (!m_params.Load(iniFile.c_str()))
	{
		std::cerr << "fails to load INI file from " << iniFile << std::endl;
		return false;
	}

	if (!load_annot(m_params.annot_path.c_str()))
	{
		std::cerr << "fails to load annotation file from " << m_params.annot_path << std::endl;
		return false;
	}
	return true;
}


template <typename T>
bool BrainClassifierTrain<T>::load_annot(std::string file)
{
	std::vector<std::string> lines;
	if (!stringUtils::ReadLines(file.c_str(), lines))
	{
		std::cerr << "fails to read lines from " << file << std::endl;
		return false;
	}

	std::vector< std::string > tokens, lm_paths;
	int num_images = lines.size() / 2;

	m_image_paths.resize(num_images);
	lm_paths.resize(num_images);
	m_labels.resize(num_images);
	m_classNumber = -1;

	for (int i = 0; i < num_images; ++i)
	{
		tokens = stringUtils::Split(lines[i * 2], '\t');
		assert_message(tokens.size() == 2, "first line of each item must have the format path\tlabel\n");

		m_image_paths[i] = tokens[0];
		m_labels[i] = stringUtils::str2num<int>(tokens[1]);

		if (m_labels[i] > m_classNumber)
			m_classNumber = m_labels[i];
		
		tokens = stringUtils::Split(lines[i * 2 + 1], '\t');
		lm_paths[i] = tokens[0];
	}

	m_classNumber += 1;

	m_shapes.resize(num_images);
	for (int i = 0; i < num_images; ++i)
	{
		if (!PDMUtils::ReadPDMFromTxt(lm_paths[i].c_str(), m_shapes[i], '\t'))
		{
			std::cerr << "fail to read PDM from file " << lm_paths[i] << std::endl;
			return false;
		}

		if (i != 0)
			assert_message(m_shapes[i].GetPoints().size() == m_shapes[i-1].GetPoints().size(), "inconsistent numbers of points");
	}

	return true;
}


template <typename T>
bool BrainClassifierTrain<T>::CreateOutputFolder() const
{
	std::string detectorFolder = GetDetectorFolder();
	if (!boost::filesystem::is_directory(detectorFolder))
		boost::filesystem::create_directories(detectorFolder);

	return true;
}


template <typename T>
bool BrainClassifierTrain<T>::SaveTree(const typename BrainClassifierTrain<T>::TreeType* tree) const
{
	std::string detectorFolder = GetDetectorFolder();
	char treeFileName[2048] = { 0 };
	sprintf(treeFileName, "%s%sTree_%s.bin", detectorFolder.c_str(), stringUtils::Slash().c_str(), stringUtils::DoubleToHexString(tree->GetUniqueID()).c_str());

	std::ofstream out(treeFileName, std::ios::binary);
	if (!out) {
		std::cerr << "fails to open " << treeFileName << " for write" << std::endl; return false;
	}
	tree->Serialize(out);
	out.close();
	return true;
}


template <typename T>
bool BrainClassifierTrain<T>::SaveFeatureSpace(const Haar3DFeatureSpace* fs, const std::vector<int>& lm_idxs, double treeID) const
{
	std::string detectorFolder = GetDetectorFolder();
	char fsFileName[2048] = { 0 };
	sprintf(fsFileName, "%s%sTree_%s.fd", detectorFolder.c_str(), stringUtils::Slash().c_str(), stringUtils::DoubleToHexString(treeID).c_str());

	std::ofstream out(fsFileName, std::ios::binary);
	if (!out) {
		std::cerr << "fails to open " << fsFileName << " for write" << std::endl; return false;
	}
	fs->Serialize(out);
	out.close();

	char fs2FileName[2048] = { 0 };
	sprintf(fs2FileName, "%s%sTree_%s.txt", detectorFolder.c_str(), stringUtils::Slash().c_str(), stringUtils::DoubleToHexString(treeID).c_str());
	FILE* fp = fopen(fs2FileName, "w");
	for (size_t i = 0; i < lm_idxs.size(); ++i)
		fprintf(fp, "%d\n", lm_idxs[i]);
	fclose(fp);

	return true;
}


template <typename T>
bool BrainClassifierTrain<T>::SaveDetectionInfo() const
{
	boost::property_tree::ptree pt;

	char buffer[100] = { 0 };
	sprintf(buffer, "%.3f %.3f %.3f", m_params.spacing[0], m_params.spacing[1], m_params.spacing[2]);
	pt.put("Info.Resolution", buffer);
	pt.put("Info.NumLandmarks", m_shapes[0].GetPoints().size());

	std::string detectorFolder = GetDetectorFolder();
	char path[2048] = { 0 };
	sprintf(path, "%s%sinfo.ini", detectorFolder.c_str(), stringUtils::Slash().c_str());

	try{
		boost::property_tree::ini_parser::write_ini(path, pt);
	}
	catch (boost::property_tree::ptree_error& /*error*/) {

		// commented to avoid error message when multiple trees are trained in parallel
		// std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}


template <typename T>
std::auto_ptr<Haar3DFeatureSpace> BrainClassifierTrain<T>::CreateFeatureSpace()
{
	Haar3DFeatureSpace* fs = new Haar3DFeatureSpace;
	for (unsigned int i = 0; i < m_params.filter_sizes.size(); ++i) {
		unsigned int size = m_params.filter_sizes[i];
		fs->AddFilterSize(vect3<unsigned int>(size, size, size));
	}
	fs->SetPatchSize(m_params.patch_size);
	return std::auto_ptr<Haar3DFeatureSpace>(fs);
}


template <typename T>
std::auto_ptr< Tree<MemoryAxisAlignedWeakLearner, HistogramStatisticsAggregator> > 
	BrainClassifierTrain<T>::TrainTree(Random& random, Haar3DFeatureSpace* fs, std::vector<int>& lm_idxs)
{
	int num_features = m_params.forest_params.treeParameters.numOfRandomWeakLearners;
	fs->RandomizeFeatureSpace_twoblocks(random, num_features);

	int num_points = static_cast<int>( m_shapes[0].GetPoints().size() / 3 );
	int num_images = static_cast<int>( m_image_paths.size() );

	lm_idxs.resize(num_features);
	for (size_t i = 0; i < lm_idxs.size(); ++i)
		lm_idxs[i] = random.Next(0, num_points - 1);

	MemoryDataCollection train_data;
	train_data.SetFeatureNumber(num_features);
	train_data.SetClassNumber(m_classNumber);
	train_data.SetSampleNumber(num_images);

	std::vector<double>& data = train_data.GetDataVector();
	std::vector<int>& labels = train_data.GetLabelVector();

	data.resize(num_images * num_features);
	labels.resize(num_images);

	for (int img_idx = 0; img_idx < num_images; ++img_idx)
	{
		const std::string& path = m_image_paths[img_idx];
		std::cout << "Reading image " << path << std::endl;

		// read and re-sample image
		mxImage<double> image;
		{
			mxImage<T> orig_image;
			if (!ImageHelper::ReadImage<T, T>(path.c_str(), orig_image)) {
				std::cerr << "fail to read image from " << path << std::endl; exit(-1);
			}

			// global MR intensity normalization
			mxImage<double> normalized_image;
			double ratio = 0.997;
			mxImageUtils::MRIntensityNormalization(orig_image, ratio, normalized_image);

			// re-sample
			mxImageUtils::Resample(normalized_image, m_params.spacing, image);
		}

		const PDM& shape = m_shapes[img_idx];

		extract_landmark_features(image, shape, fs, lm_idxs, &(data[num_features * img_idx]));
		labels[img_idx] = m_labels[img_idx];
	}

	// run training
	ClassificationExhaustSearchTrainingContext context(num_features, m_classNumber);
	std::auto_ptr< TreeType > tree = TreeTrainer<W, S>::TrainTree(context, m_params.forest_params.treeParameters, &train_data, random);

	return tree;
}


template <typename T>
void BrainClassifierTrain<T>::extract_landmark_features(const mxImage<double>& image, const PDM& shape, const Haar3DFeatureSpace* fs, const std::vector<int>& lm_idxs, double* data)
{
	const std::vector< float >& pts = shape.GetPoints();
	vect3<unsigned int> patch_size = fs->GetPatchSize(0);

	#pragma omp parallel for
	for (int i = 0; i < static_cast<int>(lm_idxs.size()); ++i)
	{
		std::auto_ptr<Haar3DPatchFeatureFunctor> functor(fs->CreatePatchFeatureFunctor());

		int lm_idx = lm_idxs[i];
		vect3<float> pt(pts[lm_idx * 3], pts[lm_idx * 3 + 1], pts[lm_idx * 3 + 2]);
		vect3<unsigned int> voxel;
		mxImageUtils::World2Voxel(image, pt, voxel);

		if (!image.PtInImage(voxel))
			err_message("detected landmark out of image domain");

		mxImage<double> patch;
		mxImageUtils::Crop3(image, voxel, patch_size, patch, static_cast<double>(0));
		
		functor->PreprocessInput(patch, 0);
		functor->SetPatch(patch, 0);
		data[i] = functor->GetResponse(i);
	}
}


//////////////////////////////////////////////////////////////////////////


bool BrainClassifierParameters::Load(const char* file)
{
	using boost::property_tree::ptree;

	ptree pt;
	try{
		boost::property_tree::ini_parser::read_ini(file, pt);
	}
	catch (boost::property_tree::ini_parser_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	try {
		// General section
		name = pt.get<std::string>("General.Name");
		annot_path = pt.get<std::string>("General.Annotation");
		root_folder = pt.get<std::string>("General.RootFolder");
		spacing = stringUtils::ParseVect3<double>(pt.get<std::string>("General.Spacing"));

		// Forest section	
		forest_params.numTrees = pt.get<unsigned int>("Forest.NumTrees");
		forest_params.treeParameters.maxTreeDepth = pt.get<unsigned int>("Forest.MaxTreeDepth");
		forest_params.treeParameters.numOfCandidateThresholdsPerWeakLearner = pt.get<unsigned int>("Forest.NumThresholds");
		forest_params.treeParameters.numOfRandomWeakLearners = pt.get<unsigned int>("Forest.NumWeakLearners");
		forest_params.treeParameters.minElementsOfLeafNode = pt.get<unsigned int>("Forest.MinLeafNum");
		forest_params.treeParameters.minInformationGain = pt.get<double>("Forest.MinInfoGain");

		// Intensity section
		stringUtils::str2vec<unsigned int>(pt.get<std::string>("Intensity.FilterSize"), filter_sizes, ' ');
		patch_size = stringUtils::ParseVect3<unsigned int>(pt.get<std::string>("Intensity.PatchSize"));
	}
	catch (boost::property_tree::ptree_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}


bool BrainClassifierParameters::WriteDefaultConfig(const char* file)
{
	using boost::property_tree::ptree;
	ptree pt;
	try{
		pt.put("General.Name", "n/a");
		pt.put("General.Annotation", "n/a");
		pt.put("General.RootFolder", "n/a");

		pt.put("Forest.NumTrees", 1);
		pt.put("Forest.MaxTreeDepth", 50);
		pt.put("Forest.NumThresholds", 100);
		pt.put("Forest.NumWeakLearners", 1000);
		pt.put("Forest.MinLeafNum", 8);
		pt.put("Forest.MinInfoGain", 0);

		pt.put("Intensity.FilterSize", "3 5");
		pt.put("Intensity.PatchSize", "30 30 30");
	}
	catch (boost::property_tree::ptree_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	std::ofstream out(file);
	if (!out) {
		std::cerr << "could not open " << file << " for write" << std::endl; return false;
	}

	try {
		boost::property_tree::ini_parser::write_ini(out, pt);
	}
	catch (boost::property_tree::ini_parser_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	out.close();
	return true;
}


bool BrainClassifierParameters::WriteDefaultAnnot(const char* file)
{
	FILE* fp = fopen(file, "w");
	if (fp == NULL)
		return false;

	fprintf(fp, "/netscr/yzgao/somefolder/1.mhd\t0\n");
	fprintf(fp, "/netscr/yzgao/somefolder/1.txt\n");
	fprintf(fp, "/netscr/yzgao/somefolder/1.mhd\t1\n");
	fprintf(fp, "/netscr/yzgao/somefolder/1.txt\n");

	fclose(fp);
	return true;
}



} } }


#endif