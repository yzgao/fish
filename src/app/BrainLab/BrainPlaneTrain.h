//
//  BrainPlaneTrain.h
//  FISH
//
//  Created by Yaozong Gao on 8/31/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __BrainPlaneTrain_h__
#define __BrainPlaneTrain_h__

#include "stdafx.h"
#include "forest/RandomForest.h"
#include "feature/haar3d/Haar3DFeatureSpace.h"
#include "extern/ImageHelper.h"

namespace BRIC { namespace IDEA { namespace FISH {

// plane parameters

struct BrainPlaneParameters
{
public:
	std::string name;
	std::string annot_path;
	std::string root_folder;
	int scale;
	vect3<double> spacing;
	double box_radius;
	int num_samples;

	ForestTrainingParameters forest_params;

	std::vector<unsigned int> filter_sizes;
	vect3<unsigned int> patch_size;

public:
	bool Load(const char* file);
	static bool WriteDefaultConfig(const char* file);
	static bool WriteDefaultAnnot(const char* file);
};



// train module

template <typename T>
class BrainPlaneTrain
{
public:

	typedef MemoryAxisAlignedWeakLearner W;
	typedef SimpleRegressionStatisticsAggregator S;
	typedef Tree<W, S> TreeType;

	bool LoadConfigFile(const char* file);
	const BrainPlaneParameters& GetParameters() const { return m_params; }
	std::string GetDetectorFolder() const;
	std::auto_ptr<Haar3DFeatureSpace> CreateFeatureSpace();
	std::auto_ptr<TreeType> TrainTree(Random& random, Haar3DFeatureSpace* fs);
	bool CreateOutputFolder() const;
	bool SaveTree(const TreeType* tree) const;
	bool SaveFeatureSpace(const Haar3DFeatureSpace* fs, double treeID) const;
	bool SaveDetectionInfo() const;


private:

	bool load_annot(const char* file);
	void draw_samples(const mxImage<double>& image, const std::vector< vect3<double> >& planes, Random& random, std::vector< vect3<unsigned int> >& voxels, std::vector<double>& targets);
	void get_bounding_box(const std::vector< vect3<unsigned int> >& voxels, vect3<unsigned int>& sp, vect3<unsigned int>& ep);
	void prepare_input_image(const mxImage<double>& image, const vect3<unsigned int>& sp, const vect3<unsigned int>& ep, Haar3DFeatureSpace* fs, mxImage<double>& input_images);
	void _save_sample_map(const mxImage<double>& resampled_image, int img_idx, const std::vector< vect3<unsigned int> >& sample_voxels, const std::vector<double>& targets, const char* outmap_path);

private:

	BrainPlaneParameters m_params;

	int num_images;  
	int num_planes;

	std::vector< std::string > m_image_paths;						// image paths	
	std::vector< std::vector< vect3<double> > > m_planes;			// planes per image
};


//////////////////////////////////////////////////////////////////////////

template <typename T>
bool BrainPlaneTrain<T>::LoadConfigFile(const char* file)
{
	if (!m_params.Load(file))
		return false;

	if (!load_annot(m_params.annot_path.c_str())) 
	{
		std::cerr << "fails to load annotation from file " << m_params.annot_path << std::endl; 
		return false;
	}

	return true;
}


template <typename T>
std::string BrainPlaneTrain<T>::GetDetectorFolder() const
{
	std::string detectorFolder;
	detectorFolder = m_params.root_folder + stringUtils::Slash() + m_params.name;
	return detectorFolder;
}


template <typename T>
std::auto_ptr<Haar3DFeatureSpace> BrainPlaneTrain<T>::CreateFeatureSpace()
{
	Haar3DFeatureSpace* fs = new Haar3DFeatureSpace;
	for (unsigned int i = 0; i < m_params.filter_sizes.size(); ++i) {
		unsigned int size = m_params.filter_sizes[i];
		fs->AddFilterSize(vect3<unsigned int>(size, size, size));
	}
	fs->SetPatchSize(m_params.patch_size);
	return std::auto_ptr<Haar3DFeatureSpace>(fs);
}


template <typename T>
void BrainPlaneTrain<T>::draw_samples(const mxImage<double>& image, const std::vector< vect3<double> >& planes, Random& random, std::vector< vect3<unsigned int> >& voxels, std::vector<double>& targets)
{
	voxels.reserve(m_params.num_samples);

	vect3<unsigned int> image_size = image.GetImageSize();
	vect3<double> spacing = image.GetSpacing();
	vect3<double> sp = image.GetOrigin();
	vect3<double> ep; vect3<unsigned int> ep_voxel(image_size[0] - 1, image_size[1] - 1, image_size[2] - 1);
	mxImageUtils::Voxel2World(image, ep_voxel, ep);

	for (int i = 0; i < m_params.num_samples; ++i)
	{
		size_t plane_idx = random.Next<size_t>(0, planes.size() - 1);
		vect3<double> plane_world = planes[plane_idx];

		// sample one voxel
		vect3<int> sampled_voxel;
		while (true)
		{
			// pick a point on the surface
			double z = random.NextDouble(sp[0], ep[0]);
			double y = random.NextDouble(sp[1], ep[1]);
			double x = (1 - plane_world[2] * z - plane_world[1] * y) / plane_world[0];

			double sigma = m_params.box_radius / 2;
			double dist = random.GaussianNextDouble(0, sigma);

			vect3<double> normal = plane_world.normalized();

			vect3<double> ptWorld(x, y, z);
			ptWorld = ptWorld + normal * dist;

			mxImageUtils::World2Voxel(image, ptWorld, sampled_voxel);

			if (!image.PtInImage(sampled_voxel[0], sampled_voxel[1], sampled_voxel[2]))
				continue;

			voxels.push_back( vect3<unsigned int>(sampled_voxel[0], sampled_voxel[1], sampled_voxel[2]) );
			break;
		}
	}

	// compute target displacements
	for (size_t i = 0; i < voxels.size(); ++i)
	{
		vect3<unsigned int> sampled_voxel = voxels[i];
		vect3<double> sampled_world;
		mxImageUtils::Voxel2World(image, sampled_voxel, sampled_world);

		for (size_t j = 0; j < planes.size(); ++j)
		{
			vect3<double> normal = planes[j].normalized();
			double signed_dist = ((planes[j][0] * sampled_world[0] + planes[j][1] * sampled_world[1] + planes[j][2] * sampled_world[2]) - 1) / planes[j].l2norm();
			vect3<double> disp = normal * (- signed_dist);

			for (int k = 0; k < 3; ++k)
				disp[k] = disp[k] / spacing[k];

			targets.push_back( disp[0] );
			targets.push_back( disp[1] );
			targets.push_back( disp[2] );
		}
	}
}


template <typename T>
void BrainPlaneTrain<T>::get_bounding_box(const std::vector< vect3<unsigned int> >& voxels, vect3<unsigned int>& sp, vect3<unsigned int>& ep)
{
	sp[0] = sp[1] = sp[2] = std::numeric_limits<unsigned int>::max();
	ep[0] = ep[1] = ep[2] = std::numeric_limits<unsigned int>::min();

	for (unsigned int i = 0; i < voxels.size(); ++i)
	{
		const vect3<unsigned int>& voxel = voxels[i];
		for (unsigned int j = 0; j < 3; ++j)
		{
			if (voxel[j] < sp[j])
				sp[j] = voxel[j];
			if (voxel[j] > ep[j])
				ep[j] = voxel[j];
		}
	}
}


template <typename T>
void BrainPlaneTrain<T>::prepare_input_image(const mxImage<double>& image, const vect3<unsigned int>& sp, const vect3<unsigned int>& ep, Haar3DFeatureSpace* fs, mxImage<double>& input_image)
{
	// prepare the intensity source
	vect3<int> req_sp(static_cast<int>(sp[0]), static_cast<int>(sp[1]), static_cast<int>(sp[2]));
	vect3<int> req_ep(static_cast<int>(ep[0]), static_cast<int>(ep[1]), static_cast<int>(ep[2]));

	vect3<unsigned int> patchSize = fs->GetPatchSize(0);
	mxImageUtils::PadBox(req_sp, req_ep, patchSize);

	mxImageUtils::Crop2(image, req_sp, req_ep, input_image);
	std::cout << "Cropping the intensity image by bounding box \t\tDONE" << std::endl;
}


template <typename T>
std::auto_ptr< Tree<MemoryAxisAlignedWeakLearner, SimpleRegressionStatisticsAggregator> > 
	BrainPlaneTrain<T>::TrainTree(Random& random, Haar3DFeatureSpace* fs)
{
	int num_features = m_params.forest_params.treeParameters.numOfRandomWeakLearners;
	fs->RandomizeFeatureSpace_twoblocks(random, num_features); 

	MemoryDataCollection train_data;
	train_data.SetFeatureNumber(num_features);
	train_data.SetTargetDim(num_planes * 3);

	std::vector<double>& data = train_data.GetDataVector();
	std::vector<double>& targets = train_data.GetTargetVector();

	data.reserve(num_images * m_params.num_samples * num_features);
	targets.reserve(num_images * m_params.num_samples * num_planes * 3);

	unsigned int total_num = 0;

	// extract features from all training images
	for (int img_idx = 0; img_idx < num_images; ++img_idx)
	{
		const std::string& path = m_image_paths[img_idx];
		std::cout << "Reading image " << path << std::endl;

		// read and re-sample image
		mxImage<double> image;
		{
			mxImage<T> orig_image;
			if (!ImageHelper::ReadImage<T,T>(path.c_str(), orig_image)) {
				std::cerr << "fail to read image from " << path << std::endl; exit(-1);
			}

			// global MR intensity normalization
			mxImage<double> normalized_image;
			double ratio = 0.997;
			mxImageUtils::MRIntensityNormalization(orig_image, ratio, normalized_image);

			mxImageUtils::Resample(normalized_image, m_params.spacing, image);
		}

		// draw samples
		std::vector< vect3<unsigned int> > voxels;
		std::cout << "Sampling " << path;
		draw_samples(image, m_planes[img_idx], random, voxels, targets);
		std::cout << " (" << voxels.size() << ")" << std::endl;
		//_save_sample_map(image, img_idx, voxels, targets, "C:\\Users\\yzgao\\Desktop\\sample.mha");

		// compute bounding box for training sample points
		vect3<unsigned int> sp, ep;
		get_bounding_box(voxels, sp, ep);

		// compute the input image
		mxImage<double> input_image;
		prepare_input_image(image, sp, ep, fs, input_image);

		// allocate training data buffer for image features
		int num_samples = static_cast<int>(voxels.size());
		try {
			data.resize(static_cast<size_t>(total_num + num_samples) * static_cast<size_t>(num_features));
		}
		catch (std::exception& exp) {
			double memoryGB = static_cast<double>((total_num + num_samples) * num_features * sizeof(double)) / (1024.0*1024.0*1024.0);
			std::cerr << "memory allocation fails (" << exp.what() << "), requested " << memoryGB << " GB memory" << std::endl;
			exit(-1);
		}

		// parallel computing features for all sampled patches
		#pragma omp parallel for
		for (int i = 0; i < num_samples; ++i)
		{
			std::auto_ptr<Haar3DPatchFeatureFunctor> functor(fs->CreatePatchFeatureFunctor());

			mxImage<double> input_patch;
			vect3<unsigned int> patch_size = fs->GetPatchSize(0);
			vect3<unsigned int> adjusted_voxel = voxels[i] - sp + patch_size / 2;
			mxImageUtils::Crop3(input_image, adjusted_voxel, patch_size, input_patch, static_cast<double>(0));

			functor->PreprocessInput(input_patch, 0);
			functor->SetPatch(input_patch, 0);

			size_t currentPointer = static_cast<size_t>(total_num)* num_features + static_cast<size_t>(i)* num_features;
			for (int j = 0; j < static_cast<int>(num_features); ++j)
				data[currentPointer + j] = functor->GetResponse(j);
		}
		total_num = static_cast<unsigned int>(total_num + num_samples);
	}

	train_data.SetSampleNumber(total_num);

	// run training
	SimpleRegressionExTrainingContext context(num_features, num_planes * 3);
	std::auto_ptr< TreeType > tree = TreeTrainer<W, S>::TrainTree(context, m_params.forest_params.treeParameters, &train_data, random);

	return tree;
}


template <typename T>
bool BrainPlaneTrain<T>::load_annot(const char* file)
{
	std::vector< std::string > lines;
	if (!stringUtils::ReadLines(file, lines)) {
		std::cerr << "fails to read annotations from " << file << std::endl;
		return false;
	}

	m_image_paths.clear();
	m_planes.clear();

	// read number of images and landmarks
	sscanf(lines[0].c_str(), "%d\t%d", &num_images, &num_planes);
	m_image_paths.resize(num_images);
	m_planes.resize(num_images);

	for (int i = 0; i < num_images; ++i)
	{
		int path_line_idx = (num_planes + 1) * i + 1;
		if (path_line_idx + num_planes >= lines.size())
		{
			std::cerr << "incomplete annotation files" << std::endl; 
			return false;
		}

		std::vector< std::string > tokens;
		stringUtils::Split(lines[path_line_idx], '\t', tokens);
		m_image_paths[i] = tokens[0];

		m_planes[i].resize(num_planes);
		for (int j = 0; j < num_planes; ++j)
		{
			int line_idx = path_line_idx + j + 1;
			vect3<double> plane = stringUtils::ParseVect3<double>(lines[line_idx], '\t');
			m_planes[i][j] = plane;
		}
	}

	return true;
}


template <typename T>
bool BrainPlaneTrain<T>::CreateOutputFolder() const
{
	std::string detectorFolder = GetDetectorFolder();
	if (!boost::filesystem::is_directory(detectorFolder))
		boost::filesystem::create_directories(detectorFolder);

	return true;
}


template <typename T>
bool BrainPlaneTrain<T>::SaveTree(const typename BrainPlaneTrain<T>::TreeType* tree) const
{
	std::string detectorFolder = GetDetectorFolder();
	char treeFileName[2048] = { 0 };
	sprintf(treeFileName, "%s%sTree_R%d_%s.bin", detectorFolder.c_str(), stringUtils::Slash().c_str(), m_params.scale, stringUtils::DoubleToHexString(tree->GetUniqueID()).c_str());

	std::ofstream out(treeFileName, std::ios::binary);
	if (!out) {
		std::cerr << "fails to open " << treeFileName << " for write" << std::endl; return false;
	}
	tree->Serialize(out);
	out.close();
	return true;
}


template <typename T>
bool BrainPlaneTrain<T>::SaveFeatureSpace(const Haar3DFeatureSpace* fs, double treeID) const
{
	std::string detectorFolder = GetDetectorFolder();
	char fsFileName[2048] = { 0 };
	sprintf(fsFileName, "%s%sTree_R%d_%s.fd", detectorFolder.c_str(), stringUtils::Slash().c_str(), m_params.scale, stringUtils::DoubleToHexString(treeID).c_str());

	std::ofstream out(fsFileName, std::ios::binary);
	if (!out) {
		std::cerr << "fails to open " << fsFileName << " for write" << std::endl; return false;
	}
	fs->Serialize(out);
	out.close();
	return true;
}


template <typename T>
bool BrainPlaneTrain<T>::SaveDetectionInfo() const
{
	boost::property_tree::ptree pt;

	char buffer[100] = { 0 };
	sprintf(buffer, "%.3f %.3f %.3f", m_params.spacing[0], m_params.spacing[1], m_params.spacing[2]);
	pt.put("Info.Resolution", buffer);
	pt.put("Info.BoxRadius", m_params.box_radius);
	pt.put("Info.NumPlanes", m_planes[0].size());

	std::string detectorFolder = GetDetectorFolder();
	char path[2048] = { 0 };
	sprintf(path, "%s%sinfo_R%d.ini", detectorFolder.c_str(), stringUtils::Slash().c_str(), m_params.scale);

	try{
		boost::property_tree::ini_parser::write_ini(path, pt);
	}
	catch (boost::property_tree::ptree_error& /*error*/) {

		// commented to avoid error message when multiple trees are trained in parallel
		// std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}


template <typename T>
void BrainPlaneTrain<T>::_save_sample_map(const mxImage<double>& resampled_image, int img_idx, const std::vector< vect3<unsigned int> >& sample_voxels, const std::vector< double >& targets, const char* outmap_path)
{
	mxImage < double > sample_image;
	sample_image.SetImageSize(resampled_image.GetImageSize());
	sample_image.CopyImageInfo(resampled_image);
	sample_image.Fill(0);

	for (size_t i = 0; i < sample_voxels.size(); ++i)
	{
		int x = static_cast<int>(sample_voxels[i][0] + 0.5);
		int y = static_cast<int>(sample_voxels[i][1] + 0.5);
		int z = static_cast<int>(sample_voxels[i][2] + 0.5);
		if (!sample_image.PtInImage(x, y, z))
		{
			std::cerr << "one sample voxel (" << x << "," << y << "," << z << ") is out of image domain" << std::endl;
			exit(-1);
		}

		double norm = sqrt(targets[i * 3] * targets[i * 3] + targets[i * 3 + 1] * targets[i * 3 + 1] + targets[i * 3 + 2] * targets[i * 3 + 2]);

		sample_image(x, y, z) = norm;
		//sample_image(x, y, z) += 1;
	}

	if (!ImageHelper::WriteImage<double, double>(sample_image, outmap_path))
	{
		std::cerr << "fail to write sample image to " << outmap_path << std::endl;
		exit(-1);
	}
}


bool BrainPlaneParameters::Load(const char* file)
{
	using boost::property_tree::ptree;

	ptree pt;
	try{
		boost::property_tree::ini_parser::read_ini(file, pt);
	}
	catch (boost::property_tree::ini_parser_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	try {
		// General section
		name = pt.get<std::string>("General.Name");
		annot_path = pt.get<std::string>("General.Annotation");
		root_folder = pt.get<std::string>("General.RootFolder");
		scale = pt.get<int>("General.Scale");
		spacing = stringUtils::ParseVect3<double>(pt.get<std::string>("General.Spacing"));
		box_radius = pt.get<double>("General.BoxRadius");
		num_samples = pt.get<int>("General.SamplesPerImage");

		// Forest section	
		forest_params.numTrees = pt.get<unsigned int>("Forest.NumTrees");
		forest_params.treeParameters.maxTreeDepth = pt.get<unsigned int>("Forest.MaxTreeDepth");
		forest_params.treeParameters.numOfCandidateThresholdsPerWeakLearner = pt.get<unsigned int>("Forest.NumThresholds");
		forest_params.treeParameters.numOfRandomWeakLearners = pt.get<unsigned int>("Forest.NumWeakLearners");
		forest_params.treeParameters.minElementsOfLeafNode = pt.get<unsigned int>("Forest.MinLeafNum");
		forest_params.treeParameters.minInformationGain = pt.get<double>("Forest.MinInfoGain");

		// Intensity section
		stringUtils::str2vec<unsigned int>(pt.get<std::string>("Intensity.FilterSize"), filter_sizes, ' ');
		patch_size = stringUtils::ParseVect3<unsigned int>(pt.get<std::string>("Intensity.PatchSize"));
	}
	catch (boost::property_tree::ptree_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}


bool BrainPlaneParameters::WriteDefaultConfig(const char* file)
{
	using boost::property_tree::ptree;
	ptree pt;
	try{
		pt.put("General.Name", "n/a");
		pt.put("General.Annotation", "n/a");
		pt.put("General.RootFolder", "n/a");
		pt.put("General.Scale", "1-4");
		pt.put("General.Spacing", "1 1 1");
		pt.put("General.BoxRadius", "mm");
		pt.put("General.SamplesPerImage", "n/a");

		pt.put("Forest.NumTrees", 1);
		pt.put("Forest.MaxTreeDepth", 50);
		pt.put("Forest.NumThresholds", 100);
		pt.put("Forest.NumWeakLearners", 1000);
		pt.put("Forest.MinLeafNum", 8);
		pt.put("Forest.MinInfoGain", 0);

		pt.put("Intensity.FilterSize", "3 5");
		pt.put("Intensity.PatchSize", "30 30 30");
	}
	catch (boost::property_tree::ptree_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	std::ofstream out(file);
	if (!out) {
		std::cerr << "could not open " << file << " for write" << std::endl; return false;
	}

	try {
		boost::property_tree::ini_parser::write_ini(out, pt);
	}
	catch (boost::property_tree::ini_parser_error& error) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	out.close();
	return true;
}


bool BrainPlaneParameters::WriteDefaultAnnot(const char* file)
{
	FILE* fp = fopen(file, "w");
	if (fp == NULL)
		return false;

	fprintf(fp, "2\t1\n");
	fprintf(fp, "/netscr/yzgao/somefolder/1.mhd\n");
	fprintf(fp, "0.0076\t0.0001\t0.0001\n");
	fprintf(fp, "/netscr/yzgao/somefolder/2.mhd\n");
	fprintf(fp, "0.0082\t0.0002\t0.0003\n");

	fclose(fp);
	return true;
}

} } }

#endif