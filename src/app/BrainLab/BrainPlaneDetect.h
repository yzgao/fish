//
//  BrainPlaneDetect.h
//  FISH
//
//  Created by Yaozong Gao on 9/1/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __BrainPlaneDetect_h__
#define __BrainPlaneDetect_h__

#include "stdafx.h"
#include "forest/RandomForest.h"
#include "feature/haar3d/Haar3DFeatureSpace.h"
#include "common/mxImageUtils.h"
#include "detector/DetectorIO.h"

namespace BRIC { namespace IDEA { namespace FISH {


// detection parameters
class BrainPlaneDetectionParameters
{
public:

	std::string root_folder;
	std::string name;
	int min_scale;
	int max_scale;

	int stepsize;
};



// detection module

template <typename T>
class BrainPlaneDetection
{
public:

	typedef MemoryAxisAlignedWeakLearner W;
	typedef SimpleRegressionStatisticsAggregator S;
	typedef Tree<W, S> TreeType;
	typedef Forest<W, S> ForestType;

public:

	void SetParameters(const BrainPlaneDetectionParameters& params)  { m_params = params; }
	std::vector< vect3<double> > DetectPlanes(const mxImage<T>& image, const mxImage<unsigned char>& mask);

private:

	void read_detector_info(const std::string& detectorFolder, int scale, vect3<double>& spacing, double& boxRadius, int& numPlanes);
	std::auto_ptr<ForestType> read_detector(int scale, FeatureSpaceArray<Haar3DFeatureSpace>& fss);
	std::vector< vect3<double> > detect(const mxImage<double>& image, const mxImage<unsigned char>& mask, int scale, const std::vector< vect3<double> >& init_planes);
	void disp_predict(const mxImage<double>& resampled_image, const mxImage<unsigned char>& resampled_mask, int scale, const vect3<int>& sp, const vect3<int>& ep, std::vector< mxImage<double> >& disp_image);
	std::vector< vect3<double> > vote(const std::vector< mxImage<double> >& disp_image, const mxImage<unsigned char>& resampled_mask, const vect3<int>& sp, const vect3<int>& ep);

private:

	BrainPlaneDetectionParameters m_params;
};



//////////////////////////////////////////////////////////////////////////


template <typename T>
std::vector< vect3<double> > BrainPlaneDetection<T>::DetectPlanes(const mxImage<T>& image, const mxImage<unsigned char>& brain_mask)
{
	// MR Intensity Normalization (Global)
	mxImage<double> normalized_image;
	mxImageUtils::MRIntensityNormalization(image, 0.997, normalized_image);

	std::string detectorFolder = m_params.root_folder + stringUtils::Slash() + m_params.name;
	vect3<double> spacing;    double box_radius;    int numPlanes;
	read_detector_info(detectorFolder, m_params.max_scale, spacing, box_radius, numPlanes);

	// detect one landmark
	std::vector< vect3<double> > planes(numPlanes);
	for (size_t i = 0; i < planes.size(); ++i)
		planes[i] = mxImageUtils::WorldCenter(normalized_image);

	// iterate over all possible scales (from coarse to fine)
	for (int scale = m_params.max_scale; scale >= m_params.min_scale; --scale)
	{
		// detect under one scale
		planes = detect(normalized_image, brain_mask, scale, planes);
	}

	return planes;
}


template <typename T>
void BrainPlaneDetection<T>::read_detector_info(const std::string& detectorFolder, int scale, vect3<double>& spacing, double& boxRadius, int& numPlanes)
{
	std::string infoPath = detectorFolder + stringUtils::Slash() + "info_R" + stringUtils::num2str(scale) + ".ini";

	boost::property_tree::ptree pt;
	try {
		boost::property_tree::ini_parser::read_ini(infoPath, pt);
		std::string stringSpacing = pt.get<std::string>("Info.Resolution");
		spacing = stringUtils::ParseVect3<double>(stringSpacing);
		boxRadius = pt.get<double>("Info.BoxRadius");
		numPlanes = pt.get<int>("Info.NumPlanes");
	}
	catch (std::runtime_error& error) {
		std::cerr << error.what() << std::endl;
		exit(-1);
	}
}


template <typename T>
std::auto_ptr< Forest<MemoryAxisAlignedWeakLearner, SimpleRegressionStatisticsAggregator> >
BrainPlaneDetection<T>::read_detector(int scale, FeatureSpaceArray<Haar3DFeatureSpace>& fss)
{
	// read detector
	std::string detectorFolder = m_params.root_folder + stringUtils::Slash() + m_params.name;
	std::auto_ptr<ForestType> forest = DetectorIO::ReadDetector<ForestType>(detectorFolder.c_str(), scale, fss);

	// check whether the detector is successfully loaded
	if (fss.size() == 0 || forest->GetTreeNumber() == 0) {
		std::cerr << "empty forest or feature space" << std::endl;
		exit(-1);
	}

	return forest;
}


template <typename T>
std::vector< vect3<double> > BrainPlaneDetection<T>::detect(const mxImage<double>& image, const mxImage<unsigned char>& mask_image, int scale, const std::vector< vect3<double> >& init_planes)
{
	std::string detectorFolder = m_params.root_folder + stringUtils::Slash() + m_params.name;

	// load detector info
	double box_radius = 0;   vect3<double> spacing;		int num_planes;
	read_detector_info(detectorFolder, scale, spacing, box_radius, num_planes);

	// re-sample image into isotropic spacing
	mxImage<double> resampledImage;  mxImage<unsigned char> resampledMask;
	mxImageUtils::Resample(image, spacing, resampledImage);
	mxImageUtils::Resample(mask_image, spacing, resampledMask);

	vect3<unsigned int> resampleImageSize = resampledImage.GetImageSize();

	// compute the bounding box
	vect3<int> sp, ep;
	if (scale == m_params.max_scale)
	{
		for (int i = 0; i < 3; ++i)
		{
			sp[i] = 0;
			ep[i] = static_cast<int>(resampleImageSize[i]) - 1;
		}
	}
	else {

		vect3<double> worldCenter = mxImageUtils::WorldCenter(image);
		int radius_voxel = static_cast<int>(box_radius / spacing[0] + 0.5);

		// the following piece of code only works for sagittal planes
		for (size_t i = 0; i < init_planes.size(); ++i)
		{
			vect3<double> mapped_center = worldCenter;
			mapped_center[0] = (1 - init_planes[i][1] * worldCenter[1] - init_planes[i][2] * worldCenter[2]) / init_planes[i][0];

			mxImageUtils::World2Voxel(resampledImage, mapped_center);

			int left = static_cast<int>(mapped_center[0] - radius_voxel + 0.5);
			int right = static_cast<int>(mapped_center[0] + radius_voxel + 0.5);

			vect3<int> tmp_sp(left, 0, 0), tmp_ep(right, resampleImageSize[1] - 1, resampleImageSize[2] - 1);
		
			if (i == 0)
			{
				sp = tmp_sp; ep = tmp_ep;
			}
			else
			{
				for (int j = 0; j < 3; ++j)
				{
					if (tmp_sp[j] < sp[j])
						sp[j] = tmp_sp[j];
					if (tmp_ep[j] > ep[j])
						ep[j] = tmp_ep[j];
				}
			}
		}

		for (int j = 0; j < 3; ++j)
		{
			if (sp[j] < 0)
				sp[j] = 0;
			if (ep[j] >= resampleImageSize[j])
				ep[j] = resampleImageSize[j] - 1;
		}
	}

	// predict offset image (from sp to ep)
	std::vector< mxImage<double> > disp_image(3 * num_planes);

	{
		boost::timer::auto_cpu_timer timer;
		disp_predict(resampledImage, resampledMask, scale, sp, ep, disp_image);
	}

	//for (int i = 0; i < num_planes; ++i)
	//{
	//	mxImage<double> norm_image;
	//	norm_image.CopyImageInfo(disp_image[i * 3]);
	//	norm_image.SetImageSize(disp_image[i * 3].GetImageSize());
	//	for (int z = 0; z < disp_image[i * 3].GetImageSize()[2]; ++z)
	//	{
	//		for (int y = 0; y < disp_image[i * 3].GetImageSize()[1]; ++y)
	//		{
	//			for (int x = 0; x < disp_image[i * 3].GetImageSize()[0]; ++x)
	//			{
	//				double dx = disp_image[i * 3](x, y, z);
	//				double dy = disp_image[i * 3 + 1](x, y, z);
	//				double dz = disp_image[i * 3 + 2](x, y, z);
	//				norm_image(x, y, z) = sqrt(dx*dx + dy*dy + dz*dz);
	//			}
	//		}
	//	}
	//	ImageHelper::WriteImage<double, double>(norm_image, (std::string("C:\\Users\\yzgao\\Desktop\\norm") + stringUtils::num2str(i) + ".mha").c_str());
	//}

	// voting
	std::vector< vect3<double> > planes_world = vote(disp_image, resampledMask, sp, ep);
	return planes_world;
}


template <typename T>
void BrainPlaneDetection<T>::disp_predict(const mxImage<double>& resampled_image, const mxImage<unsigned char>& resampled_mask, int scale, const vect3<int>& sp, const vect3<int>& ep, std::vector< mxImage<double> >& disp_image)
{
	// read detector
	FeatureSpaceArray<Haar3DFeatureSpace> fss;
	std::auto_ptr<ForestType> forest = read_detector(scale, fss);

	// setup output image
	vect3<double> origin_new;
	mxImageUtils::Voxel2World(resampled_image, sp, origin_new);

	vect3<unsigned int> outputImageSize(ep[0] - sp[0] + 1, ep[1] - sp[1] + 1, ep[2] - sp[2] + 1);

	for (size_t i = 0; i < disp_image.size(); ++i)
	{
		disp_image[i].CopyImageInfo(resampled_image);
		disp_image[i].SetOrigin(origin_new);
		disp_image[i].SetImageSize(outputImageSize);
		disp_image[i].Fill(0);
	}

	// crop necessary region from intensity region
	vect3<unsigned int> patchSize = fss[0].GetPatchSize(0);

	mxImage<double> localImage;
	{
		vect3<int> req_sp = sp, req_ep = ep;
		mxImageUtils::PadBox(req_sp, req_ep, patchSize);
		mxImageUtils::Crop2(resampled_image, req_sp, req_ep, localImage, static_cast<double>(0));
	}

	// voxel-wise prediction
	std::auto_ptr< ImageFeatureFunctorArray< Haar3DImageFeatureFunctor > > tmpFunctors = fss.CreateImageFeatureFunctor();
	(*tmpFunctors)[0].PreprocessInput(localImage, 0);

	#pragma omp parallel for schedule(dynamic)
	for (int z = 0; z < static_cast<int>(outputImageSize[2]); z += m_params.stepsize)
	{
		std::auto_ptr< ImageFeatureFunctorArray<Haar3DImageFeatureFunctor> > functors = fss.CreateImageFeatureFunctor();
		functors->SetImage(localImage, 0);

		std::vector<double> disp(disp_image.size());

		for (int y = 0; y < static_cast<int>(outputImageSize[1]); y += m_params.stepsize)
		{
			for (int x = 0; x < static_cast<int>(outputImageSize[0]); x += m_params.stepsize)
			{
				vect3<unsigned int> origvoxel;
				origvoxel[0] = x + sp[0];
				origvoxel[1] = y + sp[1];
				origvoxel[2] = z + sp[2];

				if (resampled_mask(origvoxel) == 0)
					continue;

				vect3<unsigned int> padvoxel;
				padvoxel[0] = x + patchSize[0] / 2;
				padvoxel[1] = y + patchSize[1] / 2;
				padvoxel[2] = z + patchSize[2] / 2;

				functors->SetVoxel(padvoxel, 0);

				SimpleRegressor::Regress2(forest.get(), *functors, SimpleRegressor::EqualWeighted, &disp[0]);

				for (size_t k = 0; k < disp.size(); ++k)
					disp_image[k](x, y, z) = disp[k];
			}
		}
	}
}


template <typename T>
std::vector< vect3<double> > BrainPlaneDetection<T>::vote(const std::vector< mxImage<double> >& disp_image, const mxImage<unsigned char>& mask_image, const vect3<int>& sp, const vect3<int>& ep)
{
	int num_planes = static_cast<int>(disp_image.size() / 3);
	std::vector< vect3<double> > planes(num_planes);

	// prepare vote image
	vect3<unsigned int> imageSize = disp_image[0].GetImageSize();
	mxImage<int> voteImage;
	voteImage.CopyImageInfo(disp_image[0]);
	voteImage.SetImageSize(imageSize);

	for (int i = 0; i < num_planes; ++i)
	{
		voteImage.Fill(0);
	
		// voting
		for (unsigned int z = 0; z < imageSize[2]; z += m_params.stepsize)
		{
			for (unsigned int y = 0; y < imageSize[1]; y += m_params.stepsize)
			{
				for (unsigned int x = 0; x < imageSize[0]; x += m_params.stepsize)
				{
					vect3<int> origvoxel(x + sp[0], y + sp[1], z + sp[2]);
					if (mask_image(origvoxel[0], origvoxel[1], origvoxel[2]) == 0)
						continue;

					vect3<double> disp(disp_image[i * 3](x, y, z), disp_image[i * 3 + 1](x, y, z), disp_image[i * 3 + 2](x, y, z));
					vect3<int> votePt(static_cast<int>(x + disp[0] + 0.5), static_cast<int>(y + disp[1] + 0.5), static_cast<int>(z + disp[2] + 0.5));

					if (voteImage.PtInImage(votePt[0], votePt[1], votePt[2]))
						voteImage(votePt[0], votePt[1], votePt[2]) += 1;
				}
			}
		}

		// ImageHelper::WriteImage<int, int>(voteImage, (std::string("C:\\Users\\yzgao\\Desktop\\vote") + stringUtils::num2str(i) + ".mha").c_str());

		// plane fitting
		int threshold = 5;

		std::vector< vect3<double> > pts;
		for (unsigned int z = 0; z < imageSize[2]; z += m_params.stepsize)
		{
			for (unsigned int y = 0; y < imageSize[1]; y += m_params.stepsize)
			{
				for (unsigned int x = 0; x < imageSize[0]; x += m_params.stepsize)
				{
					if (voteImage(x, y, z) > threshold)
					{
						int count = voteImage(x, y, z) - threshold;
						vect3<double> pt(x, y, z);
						mxImageUtils::Voxel2World(disp_image[0], pt);

						for (int j = 0; j < count; ++j)
							pts.push_back(pt);
					}
				}
			}
		}

		planes[i] = mathUtils::PlaneFit(pts);
	}

	return planes;
}




} } }


#endif