//
//  ContextLandmarkTrain.cpp
//  FISH
//
//  Created by Yaozong Gao on 11/01/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#include "stdafx.h"
#include "common/stringUtils.h"
#include "common/mathUtils.h"
#include "extern/BoostHelper.h"
#include "BrainPlaneTrain.h"
#include "BrainPlaneDetect.h"
#include "BrainLandmarkTrain.h"
#include "BrainLandmarkDetect.h"
#include "BrainClassifierTrain.h"
#include "BrainClassifierDetect.h"
#include "mesh/PDMUtils.h"
#include "mesh/PDMTransform.h"

#include "detector/boundary/JAT_bd_tester.h"
#include "detector/boundary/JAT_bd_multires_tester.h"
#include "mesh/surface/SurfaceUtils.h"

using namespace BRIC::IDEA::FISH;

void print_usage()
{
	std::cerr << "[ Automatic AC-PC Alignment ]" << std::endl;
	std::cerr << "BrainLab --acpc --file path --root folder --name ac,pc,mid --out path --outT path\n" << std::endl;

	std::cerr << "[ Merge Multiple Annotations into One ]" << std::endl;
	std::cerr << "BrainLab --mergetxt --file path;path;path --out path\n" << std::endl;

	std::cerr << "[ Convert Old Context Annotations to New Annotations ]" << std::endl;
	std::cerr << "BrainLab --txt2txt --file file --out path\n" << std::endl;

	std::cerr << "[ Merge Multiple Annotations into Displayable Annotations ]" << std::endl;
	std::cerr << "BrainLab --displaytxt --folder path --start idx --end idx\n" << std::endl;

	std::cerr << "[ Generate Plane Annotations from Landmarks ]" << std::endl;
	std::cerr << "BrainLab --planetxt --folder path --start idx --end idx --out path\n" << std::endl;

	std::cerr << "[ Convert Interested Points into Mask Image ]" << std::endl;
	std::cerr << "BrainLab --txt2mask --file image_path;lm_path --out path\n" << std::endl;

	std::cerr << "[ Train Brain Landmarks (Individual/Joint) ]" << std::endl;
	std::cerr << "BrainLab --lmtrain --file file --type ushort --seed 1\n" << std::endl;

	std::cerr << "[ Brain Landmark Example INI and Annotation File ]" << std::endl;
	std::cerr << "BrainLab --lmtrain --folder path\n" << std::endl;

	std::cerr << "[ Train Brain Landmarks (Individual/Joint) ]" << std::endl;
	std::cerr << "BrainLab --planetrain --file file --type ushort --seed 1\n" << std::endl;

	std::cerr << "[ Detect Brain Landmarks (Individual/Joint) ]" << std::endl;
	std::cerr << "BrainLab --lmdetect --file path --root folder --name name --scales 1:3 --out path\n" << std::endl;

	std::cerr << "[ Detect Brain Saggital Planes ]" << std::endl;
	std::cerr << "BrainLab --planedetect --file path --root folder --name name --scales 1:3 --out path\n" << std::endl;

	std::cerr << "[ Detect Interested Points ]" << std::endl;
	std::cerr << "BrainLab --ptdetect --file path --root folder --name start:end --scales 2:3 --folder meanfolder --out path\n" << std::endl;

	std::cerr << "[ Train Disease Classifier ]" << std::endl;
	std::cerr << "BrainLab --clstrain --file ini --type pixeltype --seed num\n" << std::endl;

	std::cerr << "[ Detect Disease with MRI and landmarks ]" << std::endl;
	std::cerr << "BrainLab --clsdetect --file image,shape --root folder --name name --out path\n" << std::endl;

	std::cerr << "[ Skull Stripping ]" << std::endl;
	std::cerr << "BrainLab --skullstrip --file image --root folder --name name --out path\n" << std::endl;
}


void load_annot(std::string file, std::vector< std::string >& image_paths, std::vector< std::vector< vect3<double> > >& lms)
{
	std::vector< std::string > lines;
	if (!stringUtils::ReadLines(file.c_str(), lines)) {
		std::cerr << "fails to read annotations from " << file << std::endl;
		exit(-1);
	}

	image_paths.clear();
	lms.clear();

	// read number of images and landmarks
	int num_images = 0, num_lms = 0;
	sscanf(lines[0].c_str(), "%d\t%d", &num_images, &num_lms);
	image_paths.resize(num_images);
	lms.resize(num_images);

	for (int i = 0; i < num_images; ++i)
	{
		int path_line_idx = (num_lms + 1) * i + 1;
		if (path_line_idx + num_lms >= lines.size())
		{
			std::cerr << "incomplete annotation files" << std::endl;
			exit(-1);
		}

		std::vector< std::string > tokens;
		stringUtils::Split(lines[path_line_idx], '\t', tokens);
		image_paths[i] = tokens[0];

		lms[i].resize(num_lms);
		for (int j = 0; j < num_lms; ++j)
		{
			int line_idx = path_line_idx + j + 1;
			vect3<double> lm = stringUtils::ParseVect3<double>(lines[line_idx], '\t');
			lms[i][j] = lm;
		}
	}
}


void merge_txt(std::vector<std::string>& paths, std::string output_path)
{
	std::vector< std::string > image_paths;
	std::vector< std::vector< vect3<double> > > lms;
	load_annot(paths[0], image_paths, lms);

	for (size_t i = 1; i < paths.size(); ++i)
	{
		std::vector< std::string > image_paths2;
		std::vector< std::vector< vect3<double> > > lms2;
		load_annot(paths[i], image_paths2, lms2);

		assert_message(image_paths2.size() == image_paths.size(), "different image numbers");

		for (size_t j = 0; j < image_paths2.size(); ++j)
		{
			assert_message(image_paths[j] == image_paths2[j], "different image paths");
			for (size_t k = 0; k < lms2[j].size(); ++k)
				lms[j].push_back(lms2[j][k]);
		}
	}

	FILE* fp = fopen(output_path.c_str(), "w");
	fprintf(fp, "%d\t%d\n", image_paths.size(), lms[0].size());
	for (size_t i = 0; i < image_paths.size(); ++i)
	{
		fprintf(fp, "%s\n", image_paths[i].c_str());
		for (size_t j = 0; j < lms[i].size(); ++j)
			fprintf(fp, "%lf\t%lf\t%lf\n", lms[i][j][0], lms[i][j][1], lms[i][j][2]);
	}
	fclose(fp);
}


void convert_txt(std::string input_path, std::string output_path)
{
	std::vector< std::string > image_paths;
	std::vector< vect3<double> > landmarks;

	std::vector< std::string > lines;
	if (!stringUtils::ReadLines(input_path.c_str(), lines)) {
		std::cerr << "fails to read lines from " << input_path << std::endl;
		exit(-1);
	}

	size_t num_images = lines.size() / 2;
	image_paths.resize(num_images);
	landmarks.resize(num_images);

	FILE* fp = fopen(output_path.c_str(), "w");
	if (!fp) {
		std::cerr << "fails to write " << output_path << std::endl;
		exit(-1);
	}

	fprintf(fp, "%d\t1\n", num_images);

	for (size_t i = 0; i < num_images; ++i)
	{
		assert_message(i * 2 + 1 < lines.size(), "incomplete annotation file");

		fprintf(fp, "%s\n", lines[i * 2].c_str());
		
		std::vector<std::string> tokens;
		stringUtils::Split(lines[i * 2 + 1],'\t',tokens);
		assert_message(tokens.size() > 3, "incomplete line");

		vect3<double> coord;
		coord[0] = atof(tokens[tokens.size() - 3].c_str());
		coord[1] = atof(tokens[tokens.size() - 2].c_str());
		coord[2] = atof(tokens[tokens.size() - 1].c_str());

		fprintf(fp, "%f\t%f\t%f\n", coord[0], coord[1], coord[2]);
	}

	fclose(fp);
}


void merge_into_display_txt(std::string folder, int start, int end)
{
	for (int i = start; i <= end; ++i)
	{
		std::string path = folder + stringUtils::Slash() + stringUtils::num2str(i) + ".txt";

		std::string image_path;
		vect3<double> voxel, world;

		std::vector<std::string> lines;
		stringUtils::ReadLines(path.c_str(), lines);

		std::vector<std::string> tokens;
		std::vector<int> int_tokens;

		stringUtils::str2vec<int>(lines[0], int_tokens, '\t');
		int num_lms = int_tokens[1];

		stringUtils::Split(lines[1], '\t', tokens);
		image_path = tokens[0];

		std::vector<double> double_tokens;

		for (int j = 0; j < num_lms; ++j)
		{
			stringUtils::str2vec<double>(lines[2 + j], double_tokens, '\t');

			std::string out_path = folder + stringUtils::Slash() + "Landmark_" + stringUtils::num2str(j+1) + ".txt";
			FILE* fp = fopen(out_path.c_str(), "a");

			fprintf(fp, "%s\n", image_path.c_str());
			fprintf(fp, "Annotator\t100\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n", double_tokens[0], double_tokens[1], double_tokens[2], double_tokens[3], double_tokens[4], double_tokens[5]);

			fclose(fp);
		}

	}
}


void txt2mask(std::string image_path, std::string lm_path, std::string output_path)
{
	mxImage<unsigned char> image_header;
	if (!ImageHelper::ReadImageHeader<unsigned char>(image_path.c_str(), image_header)) {
		std::cerr << "fails to read image header from file " << image_path << std::endl;
		exit(-1);
	}

	PDM pdm;
	PDMUtils::ReadPDMFromTxt(lm_path.c_str(), pdm, '\t');

	std::vector<float>& pts = pdm.GetPoints();
	int num_pts = pts.size() / 3;

	image_header.Allocate();
	image_header.Fill(0);

	for (size_t i = 0; i < num_pts; ++ i)
	{
		vect3<float> pt(pts[i * 3], pts[i * 3 + 1], pts[i * 3 + 2]);
		vect3<int> voxel;
		mxImageUtils::World2Voxel(image_header, pt, voxel);

		if (image_header.PtInImage(voxel[0], voxel[1], voxel[2])) {
			image_header(voxel[0], voxel[1], voxel[2]) = 255;
		}
	}

	if (!ImageHelper::WriteImage<unsigned char, unsigned char>(image_header, output_path.c_str()))
	{
		std::cerr << "fail to write image to " << output_path << std::endl;
		exit(-1);
	}
}


void create_example_files(std::string folder)
{
	std::string ini_path = folder + stringUtils::Slash() + "example.ini";
	std::string annot_path = folder + stringUtils::Slash() + "example.txt";

	if (!BrainLandmarkParameters::WriteDefaultConfig(ini_path.c_str())) 
	{
		std::cerr << "fails to write a default config to " << ini_path << std::endl; 
		exit(-1);
	}

	if (!BrainLandmarkParameters::WriteDefaultAnnot(annot_path.c_str()))
	{
		std::cerr << "fails to write a default annotation to " << annot_path << std::endl;
		exit(-1);
	}
}


template <typename T>
void brain_landmark_train(std::string iniFile, boost::program_options::variables_map& vm)
{
	BrainLandmarkTrain<T> trainer;
	if (!trainer.LoadConfigFile(iniFile.c_str())) 
	{
		std::cerr << "fail to load config file from " << iniFile << std::endl; 
		exit(-1);
	}

	const BrainLandmarkParameters& params = trainer.GetParameters();
	std::auto_ptr<Haar3DFeatureSpace> fs = trainer.CreateFeatureSpace();

	Random random;
	if (vm.count("seed")) {
		int seed = vm["seed"].as<int>();
		random.Seed(seed);
	}

	trainer.CreateOutputFolder();
	trainer.SaveDetectionInfo();

	for (unsigned int i = 0; i < params.forest_params.numTrees; ++i)
	{
		std::cout << "############### start to train tree " << i + 1 << "/" << params.forest_params.numTrees << " ###############" << std::endl;

		typedef typename BrainLandmarkTrain<T>::TreeType TreeType;
		std::auto_ptr<TreeType> tree = trainer.TrainTree(random, fs.get());

		if (!trainer.SaveTree(tree.get())) 
		{
			std::cerr << "fails to save tree to disk" << std::endl; 
			exit(-1);
		}

		if (!trainer.SaveFeatureSpace(fs.get(), tree->GetUniqueID())) 
		{
			std::cerr << "fails to save feature space to disk" << std::endl; 
			exit(-1);
		}
	}
}


template <typename T>
void brain_plane_train(std::string iniFile, boost::program_options::variables_map& vm)
{
	BrainPlaneTrain<T> trainer;
	if (!trainer.LoadConfigFile(iniFile.c_str()))
	{
		std::cerr << "fail to load config file from " << iniFile << std::endl;
		exit(-1);
	}

	const BrainPlaneParameters& params = trainer.GetParameters();
	std::auto_ptr<Haar3DFeatureSpace> fs = trainer.CreateFeatureSpace();

	Random random;
	if (vm.count("seed")) {
		int seed = vm["seed"].as<int>();
		random.Seed(seed);
	}

	trainer.CreateOutputFolder();
	trainer.SaveDetectionInfo();

	for (unsigned int i = 0; i < params.forest_params.numTrees; ++i)
	{
		std::cout << "############### start to train tree " << i + 1 << "/" << params.forest_params.numTrees << " ###############" << std::endl;

		typedef typename BrainPlaneTrain<T>::TreeType TreeType;
		std::auto_ptr<TreeType> tree = trainer.TrainTree(random, fs.get());

		if (!trainer.SaveTree(tree.get()))
		{
			std::cerr << "fails to save tree to disk" << std::endl;
			exit(-1);
		}

		if (!trainer.SaveFeatureSpace(fs.get(), tree->GetUniqueID()))
		{
			std::cerr << "fails to save feature space to disk" << std::endl;
			exit(-1);
		}
	}
}


template <typename T>
void brain_landmark_detect(std::string input_path, const BrainLandmarkDetectionParameters& params, std::string output_path)
{
	boost::timer::auto_cpu_timer timer;

	mxImage<T> image;
	if (!ImageHelper::ReadImage<T,T>(input_path.c_str(), image))
	{
		std::cerr << "fail to read image from " << input_path << std::endl;
		exit(-1);
	}

	BrainLandmarkDetection<T> det;

	det.SetParameters(params);
	std::vector< vect3<double> > lms_world = det.DetectLandmarks(image);

	std::vector< vect3<int> > lms_voxel(lms_world.size());
	for (size_t i = 0; i < lms_world.size(); ++i)
		mxImageUtils::World2Voxel(image, lms_world[i], lms_voxel[i]);

	FILE* fp = fopen(output_path.c_str(), "w");
	if (fp == NULL)
	{
		std::cerr << "fail to write output to " << output_path << std::endl;
		exit(-1);
	}
	
	fprintf(fp, "1\t%d\n", lms_world.size());
	fprintf(fp, "%s\n", input_path.c_str());
	for (size_t i = 0; i < lms_world.size(); ++i)
		fprintf(fp, "%d\t%d\t%d\t%f\t%f\t%f\n", lms_voxel[i][0], lms_voxel[i][1], lms_voxel[i][2], lms_world[i][0], lms_world[i][1], lms_world[i][2]);

	fclose(fp);
}


template <typename T>
void interested_points_detect(std::string input_path, std::string root_folder, int id_start, int id_end, int min_scale, int max_scale, std::string mean_folder, std::string output_path)
{
	boost::timer::auto_cpu_timer timer;

	mxImage<T> image;
	if (!ImageHelper::ReadImage<T, T>(input_path.c_str(), image))
	{
		std::cerr << "fail to read image from " << input_path << std::endl;
		exit(-1);
	}

	PDM detected_anchors;
	std::vector<float>& pts = detected_anchors.GetPoints();
	int num_pts = id_end - id_start + 1;
	pts.resize(num_pts * 3);

	for (int id = id_start; id <= id_end; ++id)
	{
		std::cout << "Detecting Anchor " << id << " ..." << std::endl;
		BrainLandmarkDetection<T> det;

		BrainLandmarkDetectionParameters params;
		params.name = stringUtils::num2str(id);
		params.root_folder = root_folder;
		params.min_scale = min_scale;
		params.max_scale = max_scale;
		params.stepsize = 1;

		det.SetParameters(params);
		std::vector< vect3<double> > lms_world = det.DetectLandmarks(image);

		int idx = id - id_start;
		pts[idx * 3] = static_cast<float>(lms_world[0][0]);
		pts[idx * 3 + 1] = static_cast<float>(lms_world[0][1]);
		pts[idx * 3 + 2] = static_cast<float>(lms_world[0][2]);
	}

	PDM mean_anchors, mean_shape;
	std::string mean_anchor_path = mean_folder + stringUtils::Slash() + "mean_anchors.txt";
	std::string mean_shape_path = mean_folder + stringUtils::Slash() + "mean_shape.txt";

	if (!PDMUtils::ReadPDMFromTxt(mean_anchor_path.c_str(), mean_anchors, '\t')) {
		std::cerr << "fail to find mean_anchors.txt under folder " << mean_folder << std::endl;
		exit(-1);
	}

	if (!PDMUtils::ReadPDMFromTxt(mean_shape_path.c_str(), mean_shape, '\t')) {
		std::cerr << "fail to find mean_shape.txt under folder " << mean_folder << std::endl;
		exit(-1);
	}

	PDMAffineTransform transform;
	transform.Estimate(mean_anchors, detected_anchors);

	PDM detected_lms;
	transform.Apply(mean_shape, detected_lms);

	if (!PDMUtils::WritePDMtoTxt(detected_lms, output_path.c_str(), '\t')) {
		std::cerr << "fail to write detected landmarks to file " << output_path << std::endl;
		exit(-1);
	}
}


template <typename T>
void compute_brain_mask(const mxImage<T>& image, std::string root_folder, std::string name, int level, int scale, mxImage<unsigned char>& mask)
{
	boost::timer::cpu_timer timer;
	timer.start();

	JAT_bd_tester aux;
	aux.SetDetector(root_folder.c_str(), name.c_str(), level);

	std::string info_path = aux.GetDetectorInfoPath(scale);
	JAT_bd_detector_info info;
	if (!info.Read(info_path.c_str())) {
		std::cerr << "fails to read detector info from " << info_path << std::endl; exit(-1);
	}

	// set timer for image loading
	std::vector< mxImage<double> > images(1);
	mxImageUtils::MRIntensityNormalization(image, 0.997, images[0]);

	// compute box
	mxImage<double> resample_header;
	mxImageUtils::GetResampleHeader(images[0], info.spacing, resample_header);
	vect3<unsigned int> resample_image_size = resample_header.GetImageSize();
	vect3<int> sp(0, 0, 0), ep(resample_image_size[0] - 1, resample_image_size[1] - 1, resample_image_size[2] - 1);

	vect3<double> sp_world, ep_world;
	mxImageUtils::Voxel2World(resample_header, sp, sp_world);
	mxImageUtils::Voxel2World(resample_header, ep, ep_world);

	// set mask
	aux.SetMask(NULL, false);

	// compute label and displacement maps
	std::vector< mxImage<double> > label_maps;
	aux.ComputeSingleResolutionMaps(images, scale, sp_world, ep_world, &label_maps, NULL);

	mxImage<unsigned char> brain_mask;
	mxImageUtils::Threshold(label_maps[0], brain_mask, static_cast<double>(0.5), static_cast<unsigned char>(255), static_cast<unsigned char>(0));

	mxImageUtils::TransformMaskImage(image, brain_mask, mask);

	std::cout << "Mask Time: " << timer.format();

	//ImageHelper::WriteImage<unsigned char, unsigned char>(mask, "C:\\Users\\yzgao\\Desktop\\brain_mask.mha");
}


template <typename T>
void brain_plane_detect(std::string input_path, const BrainPlaneDetectionParameters& params, std::string output_path)
{
	boost::timer::auto_cpu_timer timer;

	mxImage<T> image;
	if (!ImageHelper::ReadImage<T, T>(input_path.c_str(), image))
	{
		std::cerr << "fail to read image from " << input_path << std::endl;
		exit(-1);
	}

	mxImage<unsigned char> brain_mask;
	int brain_level = 0, brain_scale = 3;
	std::string brain_detector_name("brain");
	compute_brain_mask(image, params.root_folder, brain_detector_name, brain_level, brain_scale, brain_mask);

	BrainPlaneDetection<T> det;

	det.SetParameters(params);
	std::vector< vect3<double> > planes_world = det.DetectPlanes(image, brain_mask);

	FILE* fp = fopen(output_path.c_str(), "w");
	if (fp == NULL)
	{
		std::cerr << "fail to write output to " << output_path << std::endl;
		exit(-1);
	}

	fprintf(fp, "1\t%d\n", planes_world.size());
	fprintf(fp, "%s\n", input_path.c_str());
	for (size_t i = 0; i < planes_world.size(); ++i)
		fprintf(fp, "%f\t%f\t%f\n", planes_world[i][0], planes_world[i][1], planes_world[i][2]);

	fclose(fp);
}


void generate_plane_txt(std::string folder, int start, int end, std::string output_path)
{
	std::vector< std::vector<std::string> > file_lines(end - start + 1);

	for (size_t i = start; i <= end; ++i)
	{
		std::string path = folder + stringUtils::Slash() + "landmark_" + stringUtils::num2str(i) + ".txt";
		if (!stringUtils::ReadLines(path.c_str(), file_lines[i - start]))
		{
			std::cerr << "fails to read file " << path << std::endl;
			exit(-1);
		}
	}

	size_t num_lines = file_lines[0].size();
	for (size_t i = 1; i < file_lines.size(); ++i)
		assert_message(num_lines == file_lines[i].size(), "line number inconsistent");
	assert_message(num_lines % 2 == 0, "must be even lines");


	FILE* fp = fopen(output_path.c_str(), "w");

	int num_files = num_lines / 2;
	for (size_t i = 0; i < num_files; ++i)
	{
		std::string image_path;
		std::vector< vect3<double> > lms;

		for (size_t j = 0; j < file_lines.size(); ++j)
		{
			std::vector<std::string> tokens;
			if (j == 0)
			{
				stringUtils::Split(file_lines[j][i * 2], '\t', tokens);
				image_path = tokens[0];
			}

			tokens.clear();
			stringUtils::Split(file_lines[j][i * 2 + 1], '\t', tokens);
			vect3<double> lm;
			lm[0] = stringUtils::str2num<double>( tokens[tokens.size() - 4] );
			lm[1] = stringUtils::str2num<double>( tokens[tokens.size() - 3] );
			lm[2] = stringUtils::str2num<double>( tokens[tokens.size() - 2] );

			lms.push_back(lm);
		}

		vect3<double> plane = mathUtils::PlaneFit(lms);
		fprintf(fp, "%s\n", image_path.c_str());
		fprintf(fp, "%lf\t%lf\t%lf\n", plane[0], plane[1], plane[2]);
	}

	fclose(fp);

}


template <typename T>
void autoalign_acpc(std::string input_path, std::string root_folder, const std::vector<std::string>& names, std::string output_path, std::string outputT_path)
{
	boost::timer::auto_cpu_timer timer;

	mxImage<T> image;
	if (!ImageHelper::ReadImage<T, T>(input_path.c_str(), image))
	{
		std::cerr << "fail to read image from " << input_path << std::endl;
		exit(-1);
	}


	std::vector< vect3<double> > lms;

	{
		// brain AC/PC landmark detection

		BrainLandmarkDetectionParameters params;
		params.min_scale = 1;
		params.max_scale = 3;
		params.root_folder = root_folder;
		params.stepsize = 1;

		for (int i = 0; i < 2; ++i)
		{
			params.name = names[i];

			BrainLandmarkDetection<T> det;
			det.SetParameters(params);
			std::vector< vect3<double> > lms_world = det.DetectLandmarks(image);
			lms.push_back(lms_world[0]);
		}

		//std::vector< vect3<double> > lms = { vect3<double>(128,111,147), vect3<double>(129,141,144) };

		std::cout << "AC: " << lms[0] << std::endl;
		std::cout << "PC: " << lms[1] << std::endl;
	}


	vect3<double> n1;
	{
		// brain mid-sagittal plane detection

		mxImage<unsigned char> brain_mask;
		int brain_level = 0, brain_scale = 3;
		std::string brain_detector_name("brain");
		compute_brain_mask(image, root_folder, brain_detector_name, brain_level, brain_scale, brain_mask);

		BrainPlaneDetection<T> det;
		BrainPlaneDetectionParameters plane_params;
		plane_params.min_scale = 2;
		plane_params.max_scale = 3;
		plane_params.name = names[2];
		plane_params.root_folder = root_folder;
		plane_params.stepsize = 1;

		det.SetParameters(plane_params);
		std::vector< vect3<double> > planes_world = det.DetectPlanes(image, brain_mask);

		n1 = planes_world[0];
		n1 = n1.normalized();

		std::cout << "MID: " << n1 << std::endl;
	}

	// mid-sagittal alignment
	vect3<double> ref(1, 0, 0);
	vnl_matrix<double> rotmid = mathUtils::RotationMatrixBetweenVectors(n1, ref);
	
	// ac-pc alignment
	double angle = atan(-(lms[1][2] - lms[0][2]) / (lms[1][1] - lms[0][1]));
	vnl_matrix<double> rotx;
	mathUtils::GetRotationMatrix_X(angle, rotx);

	vnl_matrix<double> rot = rotx * rotmid;
	vnl_matrix<double> inv_rot = vnl_matrix_inverse<double>(rot).inverse();

	mxImage<T> outImage;
	mxImageUtils::RotateImage(image, inv_rot, outImage);

	// save transformation matrix;
	if (outputT_path.length() > 0)
	{
		FILE* fp = fopen(outputT_path.c_str(), "w");
		for (int i = 0; i < 3; ++i)
			fprintf(fp, "%f %f %f 0\n", rot(i, 0), rot(i, 1), rot(i, 2));
		fprintf(fp, "0 0 0 1\n");
		fclose(fp);
	}

	// save transformed image
	if (output_path.length() > 0)
	{
		if (!ImageHelper::WriteImage<T, T>(outImage, output_path.c_str()))
		{
			std::cerr << "fail to write image to " << output_path << std::endl;
			exit(-1);
		}
	}
}


template <typename T>
void brain_classifier_train(std::string iniFile, boost::program_options::variables_map& vm)
{
	BrainClassifierTrain<T> trainer;
	if (!trainer.LoadConfigFile(iniFile.c_str()))
	{
		std::cerr << "fail to load config file from " << iniFile << std::endl;
		exit(-1);
	}

	const BrainClassifierParameters& params = trainer.GetParameters();
	std::auto_ptr<Haar3DFeatureSpace> fs = trainer.CreateFeatureSpace();

	Random random;
	if (vm.count("seed")) {
		int seed = vm["seed"].as<int>();
		random.Seed(seed);
	}

	trainer.CreateOutputFolder();
	trainer.SaveDetectionInfo();

	for (unsigned int i = 0; i < params.forest_params.numTrees; ++i)
	{
		std::cout << "############### start to train tree " << i + 1 << "/" << params.forest_params.numTrees << " ###############" << std::endl;

		typedef typename BrainClassifierTrain<T>::TreeType TreeType;

		std::vector<int> lm_idxs;
		std::auto_ptr<TreeType> tree = trainer.TrainTree(random, fs.get(), lm_idxs);

		if (!trainer.SaveTree(tree.get()))
		{
			std::cerr << "fails to save tree to disk" << std::endl;
			exit(-1);
		}

		if (!trainer.SaveFeatureSpace(fs.get(), lm_idxs, tree->GetUniqueID()))
		{
			std::cerr << "fails to save feature space to disk" << std::endl;
			exit(-1);
		}
	}
}


template <typename T>
void brain_classifier_test(std::string input_path, std::string shape_path, std::string root_folder, std::string name, std::string output_path)
{
	boost::timer::auto_cpu_timer timer;

	mxImage<T> image;
	if (!ImageHelper::ReadImage<T,T>(input_path.c_str(), image))
	{
		std::cerr << "fail to read image from " << input_path << std::endl;
		exit(-1);
	}

	PDM shape;
	if (!PDMUtils::ReadPDMFromTxt(shape_path.c_str(), shape, '\t'))
	{
		std::cerr << "fail to read shape from " << shape_path << std::endl;
		exit(-1);
	}

	BrainClassifierDetectionParameters params;
	params.name = name;
	params.root_folder = root_folder;

	BrainClassifier classifier;

	BrainClassifierDetection<T> detection;
	detection.SetParameters(params);
	detection.ReadDetector(classifier);
	std::vector<double> probs = detection.Classify(image, shape, classifier);

	size_t label = std::distance(probs.begin(), std::max_element(probs.begin(), probs.end()));
	FILE* fp = fopen(output_path.c_str(), "a");
	for (size_t i = 0; i < probs.size(); ++i)
		fprintf(fp, "%f\t", probs[i]);
	fprintf(fp, "%d\n", label);
	fclose(fp);

}


template <typename T>
void brain_skull_strip(std::string image_path, std::string root_folder, std::string name, std::string mean_shape_path, MultiResolutionConfigFile& config, std::string output_path)
{
	mxImage<T> image;
	if (!ImageHelper::ReadImage<T,T>(image_path.c_str(), image))
	{
		std::cerr << "fail to read image from " << image_path << std::endl;
		exit(-1);
	}

	// read input images
	std::vector< mxImage<double> > images(1);
	mxImageUtils::MRIntensityNormalization(image, 0.997, images[0]);

	// load multi-resolution detectors
	std::vector< std::vector<JAT_singleres_detector> > multires_detector(config.num_resolutions);
	for (int i = 0; i < config.num_resolutions; ++i)
	{
		const SingleResolutionSetting& single = config.singleres_predict_config[i];
		multires_detector[i].resize(single.names.size());

		for (size_t j = 0; j < single.names.size(); ++j)
		{
			multires_detector[i][j].name = single.names[j];
			multires_detector[i][j].level = single.levels[j];
			multires_detector[i][j].scale = single.scale;
			multires_detector[i][j].root_folder = root_folder;

			if (!multires_detector[i][j].LoadDetector())
				err_message("fails to load detector");
		}
	}

	// set off output options
	for (int i = 0; i < config.num_resolutions; ++i)
		config.singleres_predict_config[i].output_map = 0;

	// load reference surfaces
	std::vector< Surface > reference_surfaces(1);
	if (!SurfaceUtils::LoadSurface(mean_shape_path.c_str(), reference_surfaces[0])) {
		std::cerr << "fails to load surface from " << mean_shape_path << std::endl;
		exit(-1);
	}

	// prepare test mask provider
	TestMaskProvider mask_provider;
	mask_provider.SetBoundarySurfaces(&reference_surfaces[0], reference_surfaces.size());
	mask_provider.SetDeformationConfigs(config.singleres_deform_config);

	// hierarchical label image and deform surface
	JAT_bd_multires_tester<double> aux;
	aux.SetMask(NULL);
	aux.SetOutputInfo(config, std::string(""), std::string(""), std::string(""), std::string(""));
	aux.Contour(images, multires_detector, mask_provider);

	// write final surface
	const Surface* final_surf = mask_provider.GetBoundarySurface(0);
	if (!SurfaceUtils::SaveSurface(*final_surf, output_path.c_str()))
	{
		std::cerr << "fails to save brain surface to " << output_path << std::endl;
		exit(-1);
	}
}

int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display help information");

		options_description functions("Function options");
		functions.add_options()("lmtrain", "train brain landmark(s)");
		functions.add_options()("planetrain", "train saggital plane detectors");
		functions.add_options()("planedetect", "detect saggital plane");
		functions.add_options()("lmdetect", "detect brain landmark");
		functions.add_options()("mergetxt", "merge multiple txt files");
		functions.add_options()("txt2txt", "convert old format to new");
		functions.add_options()("displaytxt", "merge into display txt");
		functions.add_options()("planetxt", "generate plane annotations from landmarks");
		functions.add_options()("acpc", "automatic AC/PC alignment");
		functions.add_options()("ptdetect", "interested point detection");
		functions.add_options()("txt2mask", "convert pt txt to mask image");
		functions.add_options()("clstrain", "train brain classifier");
		functions.add_options()("clsdetect", "detect brain disease");
		functions.add_options()("skullstrip", "skull stripping");

		options_description params("Parameters options");
		params.add_options()("folder", value<std::string>(), "folder");
		params.add_options()("file", value<std::string>(), "file");
		params.add_options()("root", value<std::string>(), "detector folder");
		params.add_options()("name", value<std::string>(), "detector name");
		params.add_options()("scales", value<std::string>(), "scales");
		params.add_options()("type", value<std::string>(), "pixel type");
		params.add_options()("seed", value<int>(), "seed");
		params.add_options()("out", value<std::string>(), "output path");
		params.add_options()("outT", value<std::string>(), "output transform matrix");
		params.add_options()("start", value<int>(), "start index");
		params.add_options()("end", value<int>(), "end index");

		options_description cmd_options;
		cmd_options.add(generic).add(functions).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			std::cerr << cmd_options << std::endl;
			print_usage();
			return -1;
		}

		if (vm.count("lmtrain") && vm.count("folder"))
		{
			create_example_files(BoostCmdHelper::Get<std::string>(vm, "folder"));
			return 0;
		}

		if (vm.count("lmtrain"))
		{
			std::string iniFile = BoostCmdHelper::Get<std::string>(vm, "file");
			std::string pixeltype = BoostCmdHelper::Get<std::string>(vm, "type");

			if (pixeltype == "uchar") {
				brain_landmark_train<unsigned char>(iniFile, vm);
			}
			else if (pixeltype == "char") {
				brain_landmark_train<char>(iniFile, vm);
			}
			else if (pixeltype == "ushort") {
				brain_landmark_train<unsigned short>(iniFile, vm);
			}
			else if (pixeltype == "short") {
				brain_landmark_train<short>(iniFile, vm);
			}
			else if (pixeltype == "uint") {
				brain_landmark_train<unsigned int>(iniFile, vm);
			}
			else if (pixeltype == "int") {
				brain_landmark_train<int>(iniFile, vm);
			}
			else if (pixeltype == "float") {
				brain_landmark_train<float>(iniFile, vm);
			}
			else if (pixeltype == "double") {
				brain_landmark_train<double>(iniFile, vm);
			}
			else {
				std::cerr << "invalid pixel type" << std::endl;
				return -1;
			}
			return 0;
		}


		if (vm.count("clstrain"))
		{
			std::string iniFile = BoostCmdHelper::Get<std::string>(vm, "file");
			std::string pixeltype = BoostCmdHelper::Get<std::string>(vm, "type");

			if (pixeltype == "uchar") {
				brain_classifier_train<unsigned char>(iniFile, vm);
			}
			else if (pixeltype == "char") {
				brain_classifier_train<char>(iniFile, vm);
			}
			else if (pixeltype == "ushort") {
				brain_classifier_train<unsigned short>(iniFile, vm);
			}
			else if (pixeltype == "short") {
				brain_classifier_train<short>(iniFile, vm);
			}
			else if (pixeltype == "uint") {
				brain_classifier_train<unsigned int>(iniFile, vm);
			}
			else if (pixeltype == "int") {
				brain_classifier_train<int>(iniFile, vm);
			}
			else if (pixeltype == "float") {
				brain_classifier_train<float>(iniFile, vm);
			}
			else if (pixeltype == "double") {
				brain_classifier_train<double>(iniFile, vm);
			}
			else {
				std::cerr << "invalid pixel type" << std::endl;
				return -1;
			}

			return 0;
		}


		if (vm.count("clsdetect"))
		{
			std::string files = BoostCmdHelper::Get<std::string>(vm, "file");
			std::vector< std::string > tokens = stringUtils::Split(files, ',');
			assert_message(tokens.size() == 2, "file option must contain two files separated by ,");
			std::string image_path = tokens[0], shape_path = tokens[1];

			std::string root_folder = BoostCmdHelper::Get<std::string>(vm, "root");
			std::string name = BoostCmdHelper::Get<std::string>(vm, "name");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(image_path.c_str());
			switch (type)
			{
			case ImageHelper::CHAR: brain_classifier_test<char>(image_path, shape_path, root_folder, name, output_path); break;
			case ImageHelper::UCHAR: brain_classifier_test<unsigned char>(image_path, shape_path, root_folder, name, output_path); break;
			case ImageHelper::SHORT: brain_classifier_test<short>(image_path, shape_path, root_folder, name, output_path);  break;
			case ImageHelper::USHORT: brain_classifier_test<unsigned short>(image_path, shape_path, root_folder, name, output_path); break;
			case ImageHelper::INT: brain_classifier_test<int>(image_path, shape_path, root_folder, name, output_path); break;
			case ImageHelper::UINT: brain_classifier_test<unsigned int>(image_path, shape_path, root_folder, name, output_path); break;
			case ImageHelper::LONG: brain_classifier_test<long>(image_path, shape_path, root_folder, name, output_path); break;
			case ImageHelper::ULONG: brain_classifier_test<unsigned long>(image_path, shape_path, root_folder, name, output_path); break;
			case ImageHelper::FLOAT: brain_classifier_test<float>(image_path, shape_path, root_folder, name, output_path); break;
			case ImageHelper::DOUBLE: brain_classifier_test<double>(image_path, shape_path, root_folder, name, output_path); break;
			default: std::cerr << "unrecognized pixel type" << std::endl; exit(-1);
			}

			return 0;
		}


		if (vm.count("planetrain"))
		{
			std::string iniFile = BoostCmdHelper::Get<std::string>(vm, "file");
			std::string pixeltype = BoostCmdHelper::Get<std::string>(vm, "type");

			if (pixeltype == "uchar") {
				brain_plane_train<unsigned char>(iniFile, vm);
			}
			else if (pixeltype == "char") {
				brain_plane_train<char>(iniFile, vm);
			}
			else if (pixeltype == "ushort") {
				brain_plane_train<unsigned short>(iniFile, vm);
			}
			else if (pixeltype == "short") {
				brain_plane_train<short>(iniFile, vm);
			}
			else if (pixeltype == "uint") {
				brain_plane_train<unsigned int>(iniFile, vm);
			}
			else if (pixeltype == "int") {
				brain_plane_train<int>(iniFile, vm);
			}
			else if (pixeltype == "float") {
				brain_plane_train<float>(iniFile, vm);
			}
			else if (pixeltype == "double") {
				brain_plane_train<double>(iniFile, vm);
			}
			else {
				std::cerr << "invalid pixel type" << std::endl;
				return -1;
			}
			return 0;
		}


		if (vm.count("lmdetect"))
		{
			std::string image_path = BoostCmdHelper::Get<std::string>(vm, "file");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			BrainLandmarkDetectionParameters params;
			params.root_folder = BoostCmdHelper::Get<std::string>(vm, "root");
			params.name = BoostCmdHelper::Get<std::string>(vm, "name");
			
			std::string scales = BoostCmdHelper::Get<std::string>(vm, "scales");
			std::vector<int> tokens;
			stringUtils::str2vec<int>(scales, tokens, ':');
			assert_message(tokens.size() == 2, "scales option format: end:start");

			params.min_scale = tokens[0];
			params.max_scale = tokens[1];

			params.stepsize = 1;

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(image_path.c_str());

			switch (type)
			{
			case ImageHelper::CHAR: brain_landmark_detect<char>(image_path, params, output_path);  break;
			case ImageHelper::UCHAR: brain_landmark_detect<unsigned char>(image_path, params, output_path); break;
			case ImageHelper::SHORT: brain_landmark_detect<short>(image_path, params, output_path); break;
			case ImageHelper::USHORT: brain_landmark_detect<unsigned short>(image_path, params, output_path); break;
			case ImageHelper::INT: brain_landmark_detect<int>(image_path, params, output_path); break;
			case ImageHelper::UINT: brain_landmark_detect<unsigned int>(image_path, params, output_path); break;
			case ImageHelper::FLOAT: brain_landmark_detect<float>(image_path, params, output_path); break;
			case ImageHelper::DOUBLE: brain_landmark_detect<double>(image_path, params, output_path); break;
			case ImageHelper::LONG: brain_landmark_detect<long>(image_path, params, output_path); break;
			case ImageHelper::ULONG: brain_landmark_detect<unsigned long>(image_path, params, output_path); break;
			default: std::cerr << "unknown pixel type" << std::endl; return -1;
			}
			return 0;
		}


		if (vm.count("ptdetect"))
		{
			std::string image_path = BoostCmdHelper::Get<std::string>(vm, "file");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			std::string root_folder = BoostCmdHelper::Get<std::string>(vm, "root");
			std::string names_ = BoostCmdHelper::Get<std::string>(vm, "name");
			std::vector< std::string > tokens = stringUtils::Split(names_, ':');
			assert_message(tokens.size() == 2, "name option must have start:end");

			int id_start = stringUtils::str2num<int>(tokens[0]);
			int id_end = stringUtils::str2num<int>(tokens[1]);

			std::string scales_ = BoostCmdHelper::Get<std::string>(vm, "scales");
			tokens = stringUtils::Split(scales_, ':');
			assert_message(tokens.size() == 2, "scales option must have min:max");

			int min_scale = stringUtils::str2num<int>(tokens[0]);
			int max_scale = stringUtils::str2num<int>(tokens[1]);

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(image_path.c_str());

			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR:
				interested_points_detect<unsigned char>(image_path, root_folder, id_start, id_end, min_scale, max_scale, root_folder, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR:
				interested_points_detect<char>(image_path, root_folder, id_start, id_end, min_scale, max_scale, root_folder, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT:
				interested_points_detect<unsigned short>(image_path, root_folder, id_start, id_end, min_scale, max_scale, root_folder, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT:
				interested_points_detect<short>(image_path, root_folder, id_start, id_end, min_scale, max_scale, root_folder, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::UINT:
				interested_points_detect<unsigned int>(image_path, root_folder, id_start, id_end, min_scale, max_scale, root_folder, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::INT:
				interested_points_detect<int>(image_path, root_folder, id_start, id_end, min_scale, max_scale, root_folder, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG:
				interested_points_detect<unsigned long>(image_path, root_folder, id_start, id_end, min_scale, max_scale, root_folder, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::LONG:
				interested_points_detect<long>(image_path, root_folder, id_start, id_end, min_scale, max_scale, root_folder, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT:
				interested_points_detect<float>(image_path, root_folder, id_start, id_end, min_scale, max_scale, root_folder, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE:
				interested_points_detect<double>(image_path, root_folder, id_start, id_end, min_scale, max_scale, root_folder, output_path);
				break;
			default: std::cerr << "unrecognized pixel type" << std::endl;  break;
			}
			return 0;
		}

		if (vm.count("skullstrip"))
		{
			std::string image_path = BoostCmdHelper::Get<std::string>(vm, "file");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			std::string root_folder = BoostCmdHelper::Get<std::string>(vm, "root");
			std::string name = BoostCmdHelper::Get<std::string>(vm, "name");

			std::string config_path = root_folder + stringUtils::Slash() + "config.ini";
			std::string mean_brain_path = root_folder + stringUtils::Slash() + "mean_brain.vtk";

			MultiResolutionConfigFile config;
			if (!config.Read(config_path.c_str()))
				err_message("fail to load multi-resolution config file");

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(image_path.c_str());

			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: brain_skull_strip<unsigned char>(image_path, root_folder, name, mean_brain_path, config, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: brain_skull_strip<char>(image_path, root_folder, name, mean_brain_path, config, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: brain_skull_strip<unsigned short>(image_path, root_folder, name, mean_brain_path, config, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: brain_skull_strip<short>(image_path, root_folder, name, mean_brain_path, config, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: brain_skull_strip<unsigned int>(image_path, root_folder, name, mean_brain_path, config, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::INT: brain_skull_strip<int>(image_path, root_folder, name, mean_brain_path, config, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: brain_skull_strip<unsigned long>(image_path, root_folder, name, mean_brain_path, config, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: brain_skull_strip<long>(image_path, root_folder, name, mean_brain_path, config, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: brain_skull_strip<float>(image_path, root_folder, name, mean_brain_path, config, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: brain_skull_strip<double>(image_path, root_folder, name, mean_brain_path, config, output_path); break;
			default: err_message("unrecognized pixel type");
			}

			return 0;
		}


		if (vm.count("planedetect"))
		{
			std::string image_path = BoostCmdHelper::Get<std::string>(vm, "file");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			BrainPlaneDetectionParameters params;
			params.root_folder = BoostCmdHelper::Get<std::string>(vm, "root");
			params.name = BoostCmdHelper::Get<std::string>(vm, "name");

			std::string scales = BoostCmdHelper::Get<std::string>(vm, "scales");
			std::vector<int> tokens;
			stringUtils::str2vec<int>(scales, tokens, ':');
			assert_message(tokens.size() == 2, "scales option format: end:start");

			params.min_scale = tokens[0];
			params.max_scale = tokens[1];

			params.stepsize = 1;

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(image_path.c_str());

			switch (type)
			{
			case ImageHelper::CHAR: brain_plane_detect<char>(image_path, params, output_path);  break;
			case ImageHelper::UCHAR: brain_plane_detect<unsigned char>(image_path, params, output_path); break;
			case ImageHelper::SHORT: brain_plane_detect<short>(image_path, params, output_path); break;
			case ImageHelper::USHORT: brain_plane_detect<unsigned short>(image_path, params, output_path); break;
			case ImageHelper::INT: brain_plane_detect<int>(image_path, params, output_path); break;
			case ImageHelper::UINT: brain_plane_detect<unsigned int>(image_path, params, output_path); break;
			case ImageHelper::FLOAT: brain_plane_detect<float>(image_path, params, output_path); break;
			case ImageHelper::DOUBLE: brain_plane_detect<double>(image_path, params, output_path); break;
			case ImageHelper::LONG: brain_plane_detect<long>(image_path, params, output_path); break;
			case ImageHelper::ULONG: brain_plane_detect<unsigned long>(image_path, params, output_path); break;
			default: std::cerr << "unknown pixel type" << std::endl; return -1;
			}
			return 0;
		}


		if (vm.count("acpc"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "file");

			std::string output_path;
			if (vm.count("out"))
				output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			std::string outputT_path;
			if (vm.count("outT"))
				outputT_path = BoostCmdHelper::Get<std::string>(vm, "outT");

			std::string root_folder = BoostCmdHelper::Get<std::string>(vm, "root");
			std::string names = BoostCmdHelper::Get<std::string>(vm, "name");
			std::vector<std::string> name = stringUtils::Split(names, ',');
			assert_message(name.size() == 3, "four names required (AC, PC, MID)");
			
			ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());

			switch (type)
			{
			case ImageHelper::CHAR: autoalign_acpc<char>(input_path, root_folder, name, output_path, outputT_path);  break;
			case ImageHelper::UCHAR: autoalign_acpc<unsigned char>(input_path, root_folder, name, output_path, outputT_path); break;
			case ImageHelper::SHORT: autoalign_acpc<short>(input_path, root_folder, name, output_path, outputT_path); break;
			case ImageHelper::USHORT: autoalign_acpc<unsigned short>(input_path, root_folder, name, output_path, outputT_path); break;
			case ImageHelper::INT: autoalign_acpc<int>(input_path, root_folder, name, output_path, outputT_path); break;
			case ImageHelper::UINT: autoalign_acpc<unsigned int>(input_path, root_folder, name, output_path, outputT_path); break;
			case ImageHelper::FLOAT: autoalign_acpc<float>(input_path, root_folder, name, output_path, outputT_path); break;
			case ImageHelper::DOUBLE: autoalign_acpc<double>(input_path, root_folder, name, output_path, outputT_path); break;
			case ImageHelper::LONG: autoalign_acpc<long>(input_path, root_folder, name, output_path, outputT_path); break;
			case ImageHelper::ULONG: autoalign_acpc<unsigned long>(input_path, root_folder, name, output_path, outputT_path); break;
			default: std::cerr << "unknown pixel type" << std::endl; return -1;
			}
			return 0;

		}

		
		if (vm.count("txt2mask"))
		{
			std::string paths = BoostCmdHelper::Get<std::string>(vm, "file");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			std::vector< std::string > tokens = stringUtils::Split(paths, ';');
			assert_message(tokens.size() == 2, "file option should contain two paths: path1;path2");

			std::string image_path = tokens[0];
			std::string lm_path = tokens[1];

			txt2mask(image_path, lm_path, output_path);

			return 0;
		}


		if (vm.count("mergetxt"))
		{
			std::string paths = BoostCmdHelper::Get<std::string>(vm, "file");
			std::vector< std::string > tokens;
			stringUtils::Split(paths, ';', tokens);

			std::string out = BoostCmdHelper::Get<std::string>(vm, "out");

			merge_txt(tokens, out);
			return 0;
		}

		if (vm.count("txt2txt"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "file");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			convert_txt(input_path, output_path);
			return 0;
		}

		if (vm.count("displaytxt"))
		{
			std::string folder = BoostCmdHelper::Get<std::string>(vm, "folder");
			int start = BoostCmdHelper::Get<int>(vm, "start");
			int end = BoostCmdHelper::Get<int>(vm, "end");

			merge_into_display_txt(folder, start, end);
			return 0;
		}

		if (vm.count("planetxt"))
		{
			std::string folder = BoostCmdHelper::Get<std::string>(vm, "folder");
			std::string out = BoostCmdHelper::Get<std::string>(vm, "out");
			int start = BoostCmdHelper::Get<int>(vm, "start");
			int end = BoostCmdHelper::Get<int>(vm, "end");

			generate_plane_txt(folder, start, end, out);
			return 0;
		}

	} catch ( boost::program_options::error& exp ) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}