# distance map regression with auto-context

option( BUILD_Boundary "Build Regression-based Boundary Detection With Auto-context" OFF )

if( BUILD_Boundary )

	# training module
	add_executable(ContextBoundaryTrain ContextBoundaryTrain.cpp ../../stdafx.cpp)
	target_link_libraries(ContextBoundaryTrain ${ITK_LIBRARIES} ${Boost_LIBRARIES} ${VTK_LIBRARIES})
	setup_fish_app(ContextBoundaryTrain)

	# testing module
	add_executable(ContextRF ContextRF.cpp ../../stdafx.cpp)
	target_link_libraries(ContextRF ${ITK_LIBRARIES} ${Boost_LIBRARIES} ${VTK_LIBRARIES})
	setup_fish_app(ContextRF)

endif()