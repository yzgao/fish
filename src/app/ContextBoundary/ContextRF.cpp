//
//  ContextRF.cpp
//  FISH
//
//  Created by Yaozong Gao on 12/12/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#include "stdafx.h"
#include "extern/ImageHelper.h"
#include "extern/BoostHelper.h"
#include "common/stringUtils.h"
#include "detector/ContextRF.h"
#include "detector/DetectorIO.h"
#include "common/mxImageUtils.h"
#include "mesh/surface/SurfaceUtils.h"

#include "feature/haar3d/Haar3DFeatureSpace.h"

using namespace BRIC::IDEA::FISH;

void print_usage()
{
	std::cerr << "[ Context-based image regression (local/global) ]" << std::endl;
	std::cerr << "ContextRF {--dim3 | --dim1} --image imagePath --root rootFolder --name detectorName --level levelIndex --scale scaleIndex --out normImagePath [--voxel center | --world center] [--size worldsize]" << std::endl;
	std::cerr << "ContextRF {--dim3 | --dim1} --image imagePath --root rootFolder --name detectorName --level levelIndex --scale scaleIndex --out normImagePath [--surface surfPath --pad padSize]" << std::endl;
}

template <typename T, int Dimension>
void contextRF(std::string imagePath, std::string rootFolder, std::string detectorName, int contextLevel, int scale, std::string outPath, boost::program_options::variables_map& vm)
{
	boost::timer::auto_cpu_timer rf_timer;

	std::string detectorFolder;
	if (contextLevel == 0) 
		detectorFolder = rootFolder + stringUtils::Slash() + detectorName;
	else
		detectorFolder = rootFolder + stringUtils::Slash() + detectorName + "_C" + stringUtils::num2str(contextLevel);

	vect3<double> spacing = DetectorIO::ReadDetectorResolution(detectorFolder.c_str(), scale);

	mxImage<T> resampledImage, origImage;
	if (!ImageHelper::ReadImage<T,T>(imagePath.c_str(), origImage)) {
		std::cerr << "fail to read image from " << imagePath << std::endl; exit(-1);
	}
	mxImageUtils::Resample(origImage, spacing, resampledImage);

	vect3<unsigned int> imageSize = resampledImage.GetImageSize();
	vect3<int> sp, ep;

	if (vm.count("surface") && vm.count("pad"))
	{
		std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "surface");
		std::vector<double> padsize = BoostCmdHelper::Get< std::vector<double> >(vm, "pad");

		if (padsize.size() < 3) {
			std::cerr << "pad size should be of lengthy 3" << std::endl; exit(-1);
		}

		vect3<int> pad_voxelsize;
		for (int k = 0; k < 3; ++k)
			pad_voxelsize[k] = static_cast<int>( padsize[k] / spacing[k] + 0.5 );
		
		Surface surf;
		if (!SurfaceUtils::LoadSurface(surf_path.c_str(), surf)) {
			std::cerr << "fails to load surface from " << surf_path << std::endl; exit(-1);
		}

		SurfaceUtils::BoundingBox(surf, resampledImage, sp, ep);
		for (int k = 0; k < 3; ++k) {
			sp[k] = sp[k] - pad_voxelsize[k];
			ep[k] = ep[k] + pad_voxelsize[k];
			if (sp[k] < 0)
				sp[k] = 0;
			if (ep[k] >= static_cast<int>(imageSize[k]))
				ep[k] = static_cast<int>(imageSize[k]);
		}
	}
	else if (vm.count("voxel") || vm.count("world")) { // local RF

		vect3<double> centerVoxel;

		if (vm.count("voxel")) { // voxel unit of center

			std::vector<int> tmpVoxel = vm["voxel"].as< std::vector<int> >();

			if (tmpVoxel.size() != 3) {
				std::cerr << "the dimension of voxel center != 3" << std::endl; exit(-1);
			}

			centerVoxel[0] = tmpVoxel[0];
			centerVoxel[1] = tmpVoxel[1];
			centerVoxel[2] = tmpVoxel[2];

			mxImageUtils::Voxel2World(origImage, centerVoxel);
			mxImageUtils::World2Voxel(resampledImage, centerVoxel);

		}else { // world unit of center

			std::vector<double> tmpWorld = vm["world"].as< std::vector<double> >();

			if (tmpWorld.size() != 3) {
				std::cerr << "the dimension of world center != 3" << std::endl;
				exit(-1);
			}

			vect3<double> centerVoxel(tmpWorld[0], tmpWorld[1], tmpWorld[2]);
			mxImageUtils::World2Voxel(resampledImage, centerVoxel);
		}

		std::vector<double> world_size = vm["size"].as< std::vector<double> >();
		if (world_size.size() != 3) {
			std::cerr << "the dimension of world size != 3" << std::endl; exit(-1);
		}
		
		for (unsigned int i = 0; i < 3; ++i) 
		{
			int voxel_size = static_cast<int>(world_size[i]/spacing[i]+0.5);
			sp[i] = static_cast<int>(centerVoxel[i]+0.5) - voxel_size/2;
			ep[i] = sp[i] + voxel_size - 1;
		}

		for (unsigned int i = 0; i < 3; ++i) 
		{
			if (sp[i] < 0)
				sp[i] = 0;
			if (ep[i] >= static_cast<int>(imageSize[i]))
				ep[i] = static_cast<int>(imageSize[i]-1);

			if (ep[i] < sp[i]) {
				std::cerr << "empty regression region" << std::endl; exit(-1);
			}
		}

	}else { // global RF

		for (int i = 0; i < 3; i++)
		{
			sp[i] = 0;
			ep[i] = static_cast<int>(imageSize[i]-1);
		}
	}

	typename ContextRF<T,Dimension>::VectorImageType vectorImage;
	ContextRF<T,Dimension>::localRF(resampledImage, rootFolder, detectorName, contextLevel, scale, sp, ep, vectorImage);

	mxImage<double> normImage;
	if (Dimension == 1) {
		mxImageUtils::ExtractFromVectorImage<double, double, Dimension>(vectorImage, 0, normImage);
	} else
		mxImageUtils::Vector2Norm<double,Dimension>(vectorImage, normImage);

	if (!ImageHelper::WriteImage<double,double>(normImage, outPath.c_str()))
	{
		std::cerr << "fail to write image to " << outPath << std::endl;
		exit(-1);
	}
}

int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display help information");

		options_description params("Parameter options");
		params.add_options()("dim3", "three dimensional regression");
		params.add_options()("dim1", "one dimensional regression");
		params.add_options()("image", value<std::string>(), "input image path");
		params.add_options()("root", value<std::string>(), "root folder");
		params.add_options()("name", value<std::string>(), "detector name");
		params.add_options()("level", value<int>(), "context level index");
		params.add_options()("scale", value<int>(), "scale index");
		params.add_options()("out", value<std::string>(), "norm image path (output)");
		params.add_options()("voxel", value< std::vector<int> >()->multitoken(), "center in voxel unit of original image");
		params.add_options()("world", value< std::vector<double> >()->multitoken(), "center in world unit");
		params.add_options()("size", value< std::vector<double> >()->multitoken(), "size in world unit" );
		params.add_options()("surface", value<std::string>(), "initial surface path");
		params.add_options()("pad", value< std::vector<double> >()->multitoken(), "pad world size");

		options_description cmd_options;
		cmd_options.add(generic).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			std::cerr << cmd_options << std::endl;
			print_usage();
			return -1;
		}

		std::string imagePath = BoostCmdHelper::Get<std::string>(vm, "image");
		std::string rootFolder = BoostCmdHelper::Get<std::string>(vm, "root");
		std::string detectorName = BoostCmdHelper::Get<std::string>(vm, "name");
		int contextLevel = BoostCmdHelper::Get<int>(vm, "level");
		int scale = BoostCmdHelper::Get<int>(vm, "scale");
		std::string outPath = BoostCmdHelper::Get<std::string>(vm, "out");

		ImageHelper::PixelType pixelType = ImageHelper::ReadPixelType(imagePath.c_str());


		if (vm.count("dim3"))
		{
			switch (pixelType)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR:
				contextRF<unsigned char, 3>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR:
				contextRF<char, 3>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT:
				contextRF<unsigned short, 3>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT:
				contextRF<short, 3>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::UINT:
				contextRF<unsigned int, 3>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::INT:
				contextRF<int, 3>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG:
				contextRF<unsigned long, 3>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::LONG:
				contextRF<long, 3>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT:
				contextRF<float, 3>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE:
				contextRF<double, 3>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			default:
				std::cerr << "unknown pixel type" << std::endl;
				exit(-1);
				break;
			}
		}
		else if (vm.count("dim1"))
		{
			switch (pixelType)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR:
				contextRF<unsigned char, 1>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR:
				contextRF<char, 1>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT:
				contextRF<unsigned short, 1>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT:
				contextRF<short, 1>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::UINT:
				contextRF<unsigned int, 1>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::INT:
				contextRF<int, 1>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG:
				contextRF<unsigned long, 1>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::LONG:
				contextRF<long, 1>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT:
				contextRF<float, 1>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE:
				contextRF<double, 1>(imagePath, rootFolder, detectorName, contextLevel, scale, outPath, vm);
				break;
			default:
				std::cerr << "unknown pixel type" << std::endl;
				exit(-1);
				break;
			}
		}
		else {
			std::cerr << "please specify the dimension" << std::endl;
			return -1;
		}

	} catch ( boost::program_options::error& exp ) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}
