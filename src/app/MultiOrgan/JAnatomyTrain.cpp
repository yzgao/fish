//
//  JAnatomyTrainer.cpp
//  FISH
//
//  Created by Yaozong Gao on 05/28/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#include "stdafx.h"
#include "detector/JAT/JAnatomyTrainer.h"
#include "extern/BoostHelper.h"


using namespace BRIC::IDEA::FISH;


void print_usage()
{
	std::cerr << "[ train a regression forest for multiple anatomy detections ]" << std::endl;
	std::cerr << "JAnatomyTrain --file iniFile --type ushort --seed value\n" << std::endl;

	std::cerr << "[ create an example ini file ]" << std::endl;
	std::cerr << "JAnatomyTrain --example iniFile\n" << std::endl;
}


template <typename T>
void TrainGeneral(boost::program_options::variables_map& vm)
{
	boost::timer::auto_cpu_timer forest_timer;

	typedef typename JAnatomyTrainer<T>::W W;
	typedef typename JAnatomyTrainer<T>::S S;

	Random random;
	if (vm.count("seed"))
	{
		random.Seed(vm["seed"].as<int>());
	}

	std::string ini_file = BoostCmdHelper::Get<std::string>(vm, "file");

	JAnatomyTrainer<T> trainer;
	if (!trainer.LoadINI(ini_file.c_str()))
	{
		std::cerr << "cannot load file : " << ini_file << std::endl;
		exit(-1);
	}

	trainer.CreateOutputFolder();
	trainer.SaveDetectionInfo();
	
	/************************************************************************/
	/* print out training information                                       */
	/************************************************************************/
	const JAT_ini_struct& ini = trainer.GetINI();
	const JAT_annot_struct& annot = trainer.GetAnnot();

	ini.print_out(std::cout);
	annot.print_out_basic(std::cout);

	/************************************************************************/
	/* start training                                                       */
	/************************************************************************/

	int tree_number = trainer.GetTreeNumber();
	for (int i = 0; i < tree_number; ++i)
	{
		boost::timer::auto_cpu_timer tree_timer;

		std::cout << std::endl;

		std::cout << "Start to create feature space for tree ( " << i + 1 << " / " << tree_number << " )\n" << std::endl;
		std::auto_ptr<IFeatureSpace> fs = trainer.CreateFeatureSpace(random);

		std::cout << "Start to train tree ( " << i + 1 << " / " << tree_number << " )\n" << std::endl;
		std::auto_ptr< Tree<W, S> > tree = trainer.TrainTree(random, fs.get());

		if (!trainer.SaveTree(tree.get(), fs.get()))
			err_message("fail to save tree");

		std::cout << std::endl;
	}

	std::cout << std::endl;
}


int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display help information");

		options_description params("Input parameters");
		params.add_options()("file", value<std::string>(), "iniFile");
		params.add_options()("seed", value<int>(), "seed");
		params.add_options()("example", value<std::string>(), "example iniFile");
		params.add_options()("type", value<std::string>(), "pixel type");

		options_description cmd_options;
		cmd_options.add(generic).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			std::cerr << cmd_options << std::endl;
			print_usage();
			return -1;
		}

		// output example ini file
		if (vm.count("example")) {

			std::string path = vm["example"].as<std::string>();

			if (!JAT_ini_struct::write_default(path.c_str())) {
				std::cerr << "fails to output default ini file" << std::endl;
				return -1;
			}

			return 0;
		}

		std::string type = BoostCmdHelper::Get<std::string>(vm, "type");
		if (type == "char")
			TrainGeneral<char>(vm);
		else if (type == "uchar")
			TrainGeneral<unsigned char>(vm);
		else if (type == "short")
			TrainGeneral<short>(vm);
		else if (type == "ushort")
			TrainGeneral<unsigned short>(vm);
		else if (type == "int")
			TrainGeneral<int>(vm);
		else if (type == "uint")
			TrainGeneral<unsigned int>(vm);
		else if (type == "long")
			TrainGeneral<long>(vm);
		else if (type == "ulong")
			TrainGeneral<unsigned long>(vm);
		else if (type == "float")
			TrainGeneral<float>(vm);
		else if (type == "double")
			TrainGeneral<double>(vm);
		else {
			std::cerr << "unrecognized pixel type" << std::endl;
			return -1;
		}

	}
	catch (boost::program_options::error& exp) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}
