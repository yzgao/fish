// useful tools for pelvic organ segmentations
// author: Yaozong Gao
// date: 06/15/2014


#ifndef __PelvicTools_h__
#define __PelvicTools_h__

#include "common/mxImageUtils.h"
#include "extern/ImageHelper.h"

namespace BRIC { namespace IDEA { namespace FISH {

class PelvicTools
{
public:

	typedef unsigned short TPixel;

	static void get_prostate_landmark(const mxImage<TPixel>& prostate_mask, int landmark_id, vect3<double>& voxel_coord, vect3<double>& world_coord);
	static void get_prostate_landmarks(std::string prefix, const std::vector<int>& image_indices, std::string postfix, const std::vector<int>& landmark_ids, std::vector< vect3<double> >& voxel_coords, std::vector< vect3<double> >& world_coords);
	static void get_bounding_box(const mxImage<TPixel>& mask, vect3<int>& sp_voxel, vect3<int>& ep_voxel, vect3<double>& sp_world, vect3<double>& ep_world);
	static void get_bounding_boxes(std::string prefix, const std::vector<int>& image_indices, std::string postfix, std::vector< vect3<int> >& sp_voxels, std::vector< vect3<int> >& ep_voxels, std::vector< vect3<double> >& sp_worlds, std::vector< vect3<double> >& ep_worlds);

	static void read_annotations(const char* path, std::vector<std::string>& image_paths, std::vector<int>& landmark_ids, std::vector< vect3<double> >& voxel_coords, std::vector< vect3<double> >& world_coords);
	static void write_gt_files(const char* folder, std::vector<std::string>& image_paths, std::vector<int>& landmark_ids, std::vector< vect3<double> >& voxel_coords, std::vector< vect3<double> >& world_coords);

};

//////////////////////////////////////////////////////////////////////////

void PelvicTools::write_gt_files(const char* folder, std::vector<std::string>& image_paths, std::vector<int>& landmark_ids, std::vector< vect3<double> >& voxel_coords, std::vector< vect3<double> >& world_coords)
{
	int total = static_cast<int>(image_paths.size() * landmark_ids.size());
	assert_message(total == voxel_coords.size() && total == world_coords.size(), "numbers inconsistent");

	for (size_t i = 0; i < landmark_ids.size(); ++i)
	{
		int id = landmark_ids[i];

		std::string landmark_path( folder );
		landmark_path += stringUtils::Slash() + "Landmark_" + stringUtils::num2str(id) + ".txt";

		FILE* fp = fopen(landmark_path.c_str(), "w");

		for (size_t j = 0; j < image_paths.size(); ++j)
		{
			int index = j * landmark_ids.size() + i;
			vect3<double> voxel, world;
			voxel = voxel_coords[index];
			world = world_coords[index];

			fprintf(fp, "%s\n", image_paths[j].c_str());
			fprintf(fp, "Annotator\t%d\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n", id, voxel[0], voxel[1], voxel[2], world[0], world[1], world[2]);
		}

		fclose(fp);
	}

}

void PelvicTools::read_annotations(const char* path, std::vector<std::string>& image_paths, std::vector<int>& landmark_ids, std::vector< vect3<double> >& voxel_coords, std::vector< vect3<double> >& world_coords)
{
	std::vector<std::string> lines;
	stringUtils::ReadLines(path, lines);

	int image_num = 0, landmark_num = 0;
	sscanf(lines[0].c_str(), "%d\t%d", &image_num, &landmark_num);

	for (int i = 0; i < image_num; ++i)
	{
		int line_index = 1 + i * (landmark_num + 1);
		assert_message(line_index < static_cast<int>(lines.size()), "broken file");

		image_paths.push_back( lines[line_index] );

		for (int j = 0; j < landmark_num; ++j)
		{
			int line_index = 1 + i * (landmark_num + 1) + (1 + j);

			if (line_index >= static_cast<int>(lines.size()))
			{
				std::cerr << "broken file" << std::endl;
				exit(-1);
			}

			std::vector<std::string> tokens = stringUtils::Split(lines[line_index], '\t');

			if (tokens[0] == "LM")
			{
				if (tokens.size() < 8) {
					std::cerr << "broken file (LM)" << std::endl;
					exit(-1);
				}

				if (i == 0)
				{
					landmark_ids.push_back(stringUtils::str2num<int>(tokens[1]));
				}

				vect3<double> voxel, world;
				for (int k = 0; k < 3; ++k)
				{
					voxel[k] = stringUtils::str2num<double>( tokens[2+k] );
					world[k] = stringUtils::str2num<double>( tokens[5+k] );
				}

				voxel_coords.push_back(voxel);
				world_coords.push_back(world);

			}
			else if (tokens[0] == "BOX")
			{
				if (tokens.size() < 14) {
					std::cerr << "broken file (BOX)" << std::endl;
					exit(-1);
				}

				if (i == 0)
				{
					int id = stringUtils::str2num<int>(tokens[1]);
					landmark_ids.push_back(id);
					landmark_ids.push_back(id + 1);
				}

				vect3<double> voxel1, voxel2, world1, world2;
				for (int k = 0; k < 3; ++k)
				{
					voxel1[k] = stringUtils::str2num<double>(tokens[2 + k]);
					world1[k] = stringUtils::str2num<double>(tokens[5 + k]);
					voxel2[k] = stringUtils::str2num<double>(tokens[8 + k]);
					world2[k] = stringUtils::str2num<double>(tokens[11 + k]);
				}

				voxel_coords.push_back(voxel1);
				voxel_coords.push_back(voxel2);
				world_coords.push_back(world1);
				world_coords.push_back(world2);

			}
			else {
				continue;
			}

		}
	}
}

void PelvicTools::get_prostate_landmark(const mxImage<TPixel>& prostate_mask, int landmark_id, vect3<double>& voxel_coord, vect3<double>& world_coord)
{
	unsigned int slice_start, slice_end;
	mxImageUtils::FindSliceRange(prostate_mask, slice_start, slice_end);

	if (landmark_id == 105) {

		voxel_coord = mxImageUtils::SliceMassCenter(prostate_mask, slice_start);
		mxImageUtils::Voxel2World(prostate_mask, voxel_coord, world_coord);

	}
	else if (landmark_id == 106) {

		voxel_coord = mxImageUtils::SliceMassCenter(prostate_mask, slice_end);
		mxImageUtils::Voxel2World(prostate_mask, voxel_coord, world_coord);

	}
	else {
		unsigned int slice_middle = (slice_start + slice_end) / 2;
		vect3<double> center = mxImageUtils::SliceMassCenter(prostate_mask, slice_middle);

		vect3<unsigned int> leftVoxel, rightVoxel, topVoxel, downVoxel;
		mxImageUtils::FindLinePoint(prostate_mask, vect3<int>(-1, static_cast<int>(center[1]), static_cast<int>(center[2])), leftVoxel, rightVoxel);
		mxImageUtils::FindLinePoint(prostate_mask, vect3<int>(static_cast<int>(center[0]), -1, static_cast<int>(center[2])), topVoxel, downVoxel);

		vect3<double> leftWorld, rightWorld, topWorld, downWorld;
		mxImageUtils::Voxel2World(prostate_mask, topVoxel, topWorld);
		mxImageUtils::Voxel2World(prostate_mask, downVoxel, downWorld);
		mxImageUtils::Voxel2World(prostate_mask, leftVoxel, leftWorld);
		mxImageUtils::Voxel2World(prostate_mask, rightVoxel, rightWorld);

		switch (landmark_id)
		{
		case 101:

			for (int i = 0; i < 3; ++i)
			{
				voxel_coord[i] = leftVoxel[i];
				world_coord[i] = leftWorld[i];
			}
			break;

		case 102:

			for (int i = 0; i < 3; ++i)
			{
				voxel_coord[i] = rightVoxel[i];
				world_coord[i] = rightWorld[i];
			}
			break;

		case 103:

			for (int i = 0; i < 3; ++i)
			{
				voxel_coord[i] = topVoxel[i];
				world_coord[i] = topWorld[i];
			}
			break;

		case 104:

			for (int i = 0; i < 3; ++i)
			{
				voxel_coord[i] = downVoxel[i];
				world_coord[i] = downWorld[i];
			}
			break;

		default:

			std::cerr << "landmark id " << landmark_id << " not recognized" << std::endl;
			exit(-1);
		}
	}
}

void PelvicTools::get_prostate_landmarks(std::string prefix, const std::vector<int>& image_indices, std::string postfix, const std::vector<int>& landmark_ids,
	std::vector< vect3<double> >& voxel_coords, std::vector< vect3<double> >& world_coords)
{
	for (size_t i = 0; i < image_indices.size(); ++i)
	{
		std::string prostate_mask_path = prefix + stringUtils::num2str(image_indices[i]) + postfix;
		std::cout << "Processing " << prostate_mask_path << std::endl;

		mxImage<TPixel> prostate_mask;
		if (!ImageHelper::ReadImage<TPixel, TPixel>(prostate_mask_path.c_str(), prostate_mask))
		{
			std::cerr << "fail to read image from " << prostate_mask_path << std::endl;
			exit(-1);
		}

		for (size_t j = 0; j < landmark_ids.size(); ++j)
		{
			int landmark_id = landmark_ids[j];

			vect3<double> target_voxel, target_world;
			get_prostate_landmark(prostate_mask, landmark_id, target_voxel, target_world);

			voxel_coords.push_back(target_voxel);
			world_coords.push_back(target_world);
		}

	} // end image indexes
}


void PelvicTools::get_bounding_box(const mxImage<TPixel>& mask, vect3<int>& sp_voxel, vect3<int>& ep_voxel, vect3<double>& sp_world, vect3<double>& ep_world)
{
	mxImageUtils::BoundingBox(mask, sp_voxel, ep_voxel);
	mxImageUtils::Voxel2World(mask, sp_voxel, sp_world);
	mxImageUtils::Voxel2World(mask, ep_voxel, ep_world);
}

void PelvicTools::get_bounding_boxes(std::string prefix, const std::vector<int>& image_indices, std::string postfix, std::vector< vect3<int> >& sp_voxels, std::vector< vect3<int> >& ep_voxels, std::vector< vect3<double> >& sp_worlds, std::vector< vect3<double> >& ep_worlds)
{
	for (size_t i = 0; i < image_indices.size(); ++i)
	{
		std::string mask_path = prefix + stringUtils::num2str(image_indices[i]) + postfix;
		std::cout << "Processing " << mask_path << std::endl;

		mxImage<TPixel> mask;
		if (!ImageHelper::ReadImage<TPixel, TPixel>(mask_path.c_str(), mask))
		{
			std::cerr << "fail to read image from " << mask_path << std::endl;
			exit(-1);
		}

		vect3<int> sp_voxel, ep_voxel;
		vect3<double> sp_world, ep_world;
		get_bounding_box(mask, sp_voxel, ep_voxel, sp_world, ep_world);

		sp_voxels.push_back(sp_voxel);
		ep_voxels.push_back(ep_voxel);
		sp_worlds.push_back(sp_world);
		ep_worlds.push_back(ep_world);

	} // end image indexes
}


} } }


#endif