//
//  FemurSegmentation.h
//  FISH
//
//  Created by Yaozong Gao on 05/10/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __FemurSegmentation_h__
#define __FemurSegmentation_h__


#include "common/mxImageUtils.h"

namespace BRIC { namespace IDEA { namespace FISH {

class FemurSegmentation
{
public:

	void LeftSegment(const mxImage<unsigned short>& intensity_image, mxImage<double>& prob_image, const Surface& surf, mxImage<unsigned char>& out_mask);
	void RightSegment(const mxImage<unsigned short>& intensity_image, mxImage<double>& prob_image, const Surface& surf, mxImage<unsigned char>& out_mask);

public:

	void mask_slices_with_surf();
	void maskout_slices(mxImage<double>& image, int start, int end);
	void refine_bottom(mxImage<unsigned char>& mask, const vect3<int>& box_sp, const vect3<int>& box_ep);
	void refine_left(mxImage<unsigned char>& mask, const vect3<int>& box_sp, const vect3<int>& box_ep);
	void refine_right(mxImage<unsigned char>& mask, const vect3<int>& box_sp, const vect3<int>& box_ep);

private:

	const mxImage<unsigned short>* m_intensity_image;
	mxImage<double>* m_prob_image;
	const Surface* m_surf;
};


/************************************************************************/
/* Femur Segmentation Implementation                                    */
/************************************************************************/

void FemurSegmentation::LeftSegment(const mxImage<unsigned short>& intensity_image, mxImage<double>& prob_image, const Surface& surf, mxImage<unsigned char>& out_mask)
{
	m_intensity_image = &intensity_image;
	m_prob_image = &prob_image;
	m_surf = &surf;

	mask_slices_with_surf();

	// get segmentation based on probability map
	//mxImage<unsigned char> binary_mask;
	//mxImageUtils::Threshold(prob_image, binary_mask, 0.5, static_cast<unsigned char>(255), static_cast<unsigned char>(0));
	//mxImageUtils::PickLargestComponent(binary_mask);
	
	// get segmentation based on deformable surface
	mxImage<unsigned char> binary_mask;
	SurfaceUtils::CarveSurface(surf, prob_image, static_cast<unsigned char>(0), static_cast<unsigned char>(255), binary_mask);
	
	vect3<int> box_sp, box_ep;
	mxImageUtils::BoundingBox(binary_mask, box_sp, box_ep);

	refine_bottom(binary_mask, box_sp, box_ep);
	refine_left(binary_mask, box_sp, box_ep);

	// first fill holes
	for (int z = box_sp[2]; z <= box_ep[2]; ++z)
		mxImageUtils::BinaryFillHoles2D(binary_mask, z, static_cast<unsigned char>(255));

	// dilate to fix some disconnected boundary and then fill
	int morph_times = 2;
	for (int i = 0; i < morph_times; ++i)
		mxImageUtils::BinaryDilate(binary_mask, static_cast<unsigned char>(255));

	for (int z = box_sp[2]; z <= box_ep[2]; ++z)
		mxImageUtils::BinaryFillHoles2D(binary_mask, z, static_cast<unsigned char>(255));

	// erode to remove the dilation effects
	for (int i = 0; i < morph_times; ++i)
		mxImageUtils::BinaryErode(binary_mask, static_cast<unsigned char>(255));

	mxImageUtils::PickLargestComponent(binary_mask);

	mxImage<unsigned char> transformed_mask;
	mxImageUtils::TransformMaskImage(intensity_image, binary_mask, out_mask);
}

void FemurSegmentation::RightSegment(const mxImage<unsigned short>& intensity_image, mxImage<double>& prob_image, const Surface& surf, mxImage<unsigned char>& out_mask)
{
	m_intensity_image = &intensity_image;
	m_prob_image = &prob_image;
	m_surf = &surf;

	mask_slices_with_surf();

	// get segmentation based on probability map
	//mxImage<unsigned char> binary_mask;
	//mxImageUtils::Threshold(prob_image, binary_mask, 0.5, static_cast<unsigned char>(255), static_cast<unsigned char>(0));
	//mxImageUtils::PickLargestComponent(binary_mask);

	// get segmentation based on deformable surface
	mxImage<unsigned char> binary_mask;
	SurfaceUtils::CarveSurface(surf, prob_image, static_cast<unsigned char>(0), static_cast<unsigned char>(255), binary_mask);

	vect3<int> box_sp, box_ep;
	mxImageUtils::BoundingBox(binary_mask, box_sp, box_ep);

	refine_bottom(binary_mask, box_sp, box_ep);
	refine_right(binary_mask, box_sp, box_ep);

	// first fill holes
	for (int z = box_sp[2]; z <= box_ep[2]; ++z)
		mxImageUtils::BinaryFillHoles2D(binary_mask, z, static_cast<unsigned char>(255));

	// dilate to fix some disconnected boundary and then fill
	int morph_times = 2;
	for (int i = 0; i < morph_times; ++i)
		mxImageUtils::BinaryDilate(binary_mask, static_cast<unsigned char>(255));

	for (int z = box_sp[2]; z <= box_ep[2]; ++z)
		mxImageUtils::BinaryFillHoles2D(binary_mask, z, static_cast<unsigned char>(255));

	// erode to remove the dilation effects
	for (int i = 0; i < morph_times; ++i)
		mxImageUtils::BinaryErode(binary_mask, static_cast<unsigned char>(255));

	mxImageUtils::PickLargestComponent(binary_mask);

	mxImage<unsigned char> transformed_mask;
	mxImageUtils::TransformMaskImage(intensity_image, binary_mask, out_mask);
}

void FemurSegmentation::refine_right(mxImage<unsigned char>& binary_mask, const vect3<int>& box_sp, const vect3<int>& box_ep)
{
	// fix the right part
	int x_start = box_sp[0] - 10 /* pad size */;
	int x_end = (box_sp[0] + box_ep[0]) * 2 / 5;

	if (x_start < 0)
		x_start = 0;

	for (int z = box_sp[2]; z <= box_ep[2]; ++z)
	{
		for (int y = box_sp[1]; y <= box_ep[1]; ++y)
		{
			for (int x = x_start; x <= x_end; ++x)
			{
				vect3<double> world(x, y, z);
				mxImageUtils::Voxel2World(binary_mask, world);
				vect3<int> voxel;
				mxImageUtils::World2Voxel(*m_intensity_image, world, voxel);

				if (!m_intensity_image->PtInImage(voxel[0], voxel[1], voxel[2]))
					binary_mask(x, y, z) = 0;
				else
				{
					unsigned short val = (*m_intensity_image)(voxel[0], voxel[1], voxel[2]);
					if (val >= 1200)
						binary_mask(x, y, z) = 255;
					else
						binary_mask(x, y, z) = 0;
				}

			}
		}
	}
}

void FemurSegmentation::refine_left(mxImage<unsigned char>& binary_mask, const vect3<int>& box_sp, const vect3<int>& box_ep)
{
	// fix the right part
	int x_start = (box_sp[0] + box_ep[0]) * 3 / 5;
	int x_end = box_ep[0] + 10 /* pad size */;

	if (x_end > binary_mask.GetImageSize()[0] - 1)
		x_end = binary_mask.GetImageSize()[0] - 1;

	for (int z = box_sp[2]; z <= box_ep[2]; ++z)
	{
		for (int y = box_sp[1]; y <= box_ep[1]; ++y)
		{
			for (int x = x_start; x <= x_end; ++x)
			{
				vect3<double> world(x, y, z);
				mxImageUtils::Voxel2World(binary_mask, world);
				vect3<int> voxel;
				mxImageUtils::World2Voxel(*m_intensity_image, world, voxel);

				if (!m_intensity_image->PtInImage(voxel[0], voxel[1], voxel[2]))
					binary_mask(x, y, z) = 0;
				else
				{
					unsigned short val = (*m_intensity_image)(voxel[0], voxel[1], voxel[2]);
					if (val > 1200)
						binary_mask(x, y, z) = 255;
					else
						binary_mask(x, y, z) = 0;
				}

			}
		}
	}
}

void FemurSegmentation::refine_bottom(mxImage<unsigned char>& binary_mask, const vect3<int>& box_sp, const vect3<int>& box_ep)
{
	int slice_num = box_ep[2] - box_sp[2] + 1;
	int bot_start = box_sp[2], bot_end = bot_start + slice_num / 5;

	vect3< int > slice_sp, slice_ep;
	for (int z = bot_start; z <= bot_end; ++z)
	{
		vect3<int> tmp_sp, tmp_ep;
		mxImageUtils::BoundingBox2D(binary_mask, z, tmp_sp, tmp_ep);

		if (z == bot_start)
		{
			slice_sp = tmp_sp;
			slice_ep = tmp_ep;
		}
		else
		{
			for (int i = 0; i < 2; ++i)
			{
				if (tmp_sp[i] < slice_sp[i])
					slice_sp[i] = tmp_sp[i];
				if (tmp_ep[i] > slice_ep[i])
					slice_ep[i] = tmp_ep[i];
			}
		}
	}

	// determine 2D bounding box
	vect3<int> slice_center = (slice_sp + slice_ep) / 2;
	vect3<int> slice_size = slice_ep - slice_sp + vect3<int>(1, 1, 1);
	int pad_size = 10;

	for (int i = 0; i < 2; ++i)
	{
		slice_sp[i] = slice_center[i] - static_cast<int>(slice_size[i] / 2 + pad_size);
		slice_ep[i] = slice_center[i] + static_cast<int>(slice_size[i] / 2 + pad_size);
	}

	// fix the bottom part
	for (int z = bot_start; z <= bot_end; ++z)
	{
		for (int y = slice_sp[1]; y <= slice_ep[1]; ++y)
		{
			for (int x = slice_sp[0]; x <= slice_ep[0]; ++x)
			{
				vect3<double> world(x, y, z);
				mxImageUtils::Voxel2World(binary_mask, world);
				vect3<int> voxel;
				mxImageUtils::World2Voxel(*m_intensity_image, world, voxel);

				if (!m_intensity_image->PtInImage(voxel[0], voxel[1], voxel[2]))
					binary_mask(x, y, z) = 0;
				else
				{
					unsigned short val = (*m_intensity_image)(voxel[0], voxel[1], voxel[2]);
					if (val > 1200)
						binary_mask(x, y, z) = 255;
				}
			}
		}
	}
}

void FemurSegmentation::mask_slices_with_surf()
{
	mxImage<unsigned char> carved_image;
	unsigned char outside_val = 0, inside_val = 255;
	SurfaceUtils::CarveSurface(*m_surf, *m_prob_image, outside_val, inside_val, carved_image);

	vect3<int> box_sp, box_ep;
	mxImageUtils::BoundingBox(carved_image, box_sp, box_ep);

	maskout_slices(*m_prob_image, box_sp[2], box_ep[2]);
}

void FemurSegmentation::maskout_slices(mxImage<double>& image, int start, int end)
{
	vect3<unsigned int> input_image_size = image.GetImageSize();
	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(start); ++z)
	{
		for (unsigned int y = 0; y < input_image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < input_image_size[0]; ++x)
			{
				image(x, y, z) = 0;
			}
		}
	}

	#pragma omp parallel for
	for (int z = end + 1; z < static_cast<int>(input_image_size[2]); ++z)
	{
		for (unsigned int y = 0; y < input_image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < input_image_size[0]; ++x)
			{
				image(x, y, z) = 0;
			}
		}
	}
}




} } }


#endif