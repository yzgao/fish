//
//  JAnatomyMap.cpp
//  FISH
//
//  Created by Yaozong Gao on 07/13/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#include "stdafx.h"
#include "extern/BoostHelper.h"
#include "extern/ImageHelper.h"
#include "detector/DetectorIO.h"
#include "detector/JAT/JAnatomyMap.h"
#include "detector/JAT/JAT_detect.h"
#include "detector/JAT/JAT_detect_config.h"
#include "mesh/PDMTransform.h"
#include "mesh/PDMUtils.h"
#include "mesh/surface/SurfaceUtils.h"
#include "common/stringUtils.h"
#include "common/fileUtils.h"
#include "detector/JAT/JAT_mesh_struct.h"


using namespace BRIC::IDEA::FISH;

void print_usage()
{
	std::cerr << "[ generate a sample detect config file ]" << std::endl;
	std::cerr << "JAnatomyMap --sample --out detect_config.ini\n" << std::endl;

	std::cerr << "[ combine different anatomy detection results into one file ]" << std::endl;
	std::cerr << "JAnatomyMap --fuse --folder path --name string\n" << std::endl;

	std::cerr << "[ convert a old annotation file to a new annotation file ]" << std::endl;
	std::cerr << "JAnatomyMap --old2new --file path --out path\n" << std::endl;

	std::cerr << "[ combine detection results of different patients into one file ]" << std::endl;
	std::cerr << "JAnatomyMap --aggregate --folder path --name string\n" << std::endl;

	std::cerr << "[ compute distance map of certain anatomy ]" << std::endl;
	std::cerr << "JAnatomyMap --dist --image path --root folder --config path --dims 1,1-3 --out map.mha\n" << std::endl;

	std::cerr << "[ compute voting map for certain anatomies ]" << std::endl;
	std::cerr << "JAnatomyMap --vote --image path --root folder --config path --out map.mha\n" << std::endl;

	std::cerr << "[ detect all anatomies in an unseen image ]" << std::endl;
	std::cerr << "JAnatomyMap --detect --image path --root folder --config path --out annot.txt\n" << std::endl;

	std::cerr << "[ detect all anatomies in a set of unseen images ]" << std::endl;
	std::cerr << "JAnatomyMap --detect --list path --root folder --config path --out annot.txt\n" << std::endl;

	std::cerr << "[ compute mean mesh ]" << std::endl;
	std::cerr << "JAnatomyMap --mean {--affine | --similarity | --rigid} --mesh list --out path\n" << std::endl;

	std::cerr << "[ align all meshes together with all landmarks to a common space ]" << std::endl;
	std::cerr << "JAnatomyMap --align {--affine | --similarity | --rigid} --lm list --mesh list --out folder\n" << std::endl;

	std::cerr << "[ align all meshes A together with corresponding meshes B to a common space ]" << std::endl;
	std::cerr << "JAnatomyMap --align2 {--affine | --similarity | --rigid} --meshA list --meshB list --out folder\n" << std::endl;

	std::cerr << "[ landmark-based shape initialization ]" << std::endl;
	std::cerr << "JAnatomyMap --initshape {--affine | --similarity | --rigid} --dstlm list --srclm list --srcmesh vtk --name organ --out folder --outidx val\n" << std::endl;

	std::cerr << "[ shape-based initialization ]" << std::endl;
	std::cerr << "JAnatomyMap --initshape2 {--affine | --similarity | --rigid} --dstmeshA vtk --srcmeshA vtk --srcmeshB vtk --out path\n" << std::endl;

	std::cerr << "[ rotate landmarks ]" << std::endl;
	std::cerr << "JAnatomyMap --rotate --list path [--image path | --center x,y,z] --angle x,y,z --out path\n" << std::endl;

	std::cerr << "[ convert gt format to annotation format ]" << std::endl;
	std::cerr << "JAnatomyMap --gt2new --file path --name lm_id --out path\n" << std::endl;

	std::cerr << "Indices in both --dims and --idx start with 1." << std::endl;
}

void extract_dist_map_from_displacement_maps( const JAT_DMapType& dmaps, std::vector<int>& dims, mxImage<double>& output_map)
{
	const std::vector< mxImage<double> >& displacementMaps = dmaps.displacement_maps;
	const mxImage<unsigned char>& mask = dmaps.mask;

	/************************************************************************/
	/* compute displacement norm (based on dims)                            */
	/************************************************************************/

	assert_message(displacementMaps.size() > 0, "zero displacement images");

	vect3<unsigned int> out_image_size = displacementMaps[0].GetImageSize();

	output_map.CopyImageInfo(displacementMaps[0]);
	output_map.SetImageSize(out_image_size);
	output_map.Fill(0);

	// check dimensions
	for (size_t i = 0; i < dims.size(); ++i)
	{
		dims[i] -= 1;	// make index start with 0

		if (dims[i] < 0 || dims[i] >= static_cast<int>(displacementMaps.size()))
		{
			std::cerr << "selected indexes out of bound (1 - " << displacementMaps.size() << ")" << std::endl;
			exit(-1);
		}
	}

	#pragma omp parallel for schedule(dynamic)
	for (int z = 0; z < static_cast<int>(out_image_size[2]); ++z)
	{
		for (unsigned int y = 0; y < out_image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < out_image_size[0]; ++x)
			{
				if (!mask(x, y, z))
					continue;

				double norm = 0;

				if (dims.size() == 1)
				{
					norm = displacementMaps[dims[0]](x, y, z);
				}
				else
				{
					for (size_t i = 0; i < dims.size(); ++i)
					{
						double val = displacementMaps[dims[i]](x, y, z);
						norm += val * val;
					}
					norm = sqrt(norm);
				}

				output_map(x, y, z) = norm;
			}
		}
	}
}


template <typename T>
void compute_distance_map(std::string image_path, std::string root_folder, const JAT_detect_config& config, std::vector<int>& dims, const char* out_path)
{
	assert_message(config.m_steps.size() > 0, "empty steps");

	JAT_detect<T> detector;

	// load testing image
	if (!detector.LoadImage(image_path.c_str()))
	{
		std::cerr << "fail to load testing image from " << image_path << std::endl;
		exit(-1);
	}
	detector.SetTreeFusionType(SimpleRegressor::InverseWeighted);

	// setup constraint landmarks
	std::map< int, vect3<double> > required_landmarks;
	detector.ComputeRequiredLandmarks(root_folder.c_str(), config, required_landmarks);
	detector.SetConstraintLandmarks(required_landmarks);

	std::pair< vect3<double>, int > global_init[3];
	detector.GetGlobalInitialPosition(required_landmarks, config, global_init);

	// compute displacement maps
	JAT_DMapType dmaps;

	if (config.m_steps.size() == 1)
	{
		boost::timer::auto_cpu_timer timer;

		const JAT_detect_config_unit& step = config.m_steps[0];
		vect3<int> stride(step.stride, step.stride, step.stride);

		detector.SetDetector(root_folder, step.name, step.level, step.scale);
		detector.ComputeGlobalDisplacementMaps(global_init, stride, dmaps);

		std::cout << "(G0)";
	}
	else
	{
		JAT_anatomy_positions positions;

		{	// global detection
			boost::timer::auto_cpu_timer timer;
			detector.GlobalDetectAnatomy(root_folder.c_str(), config.m_steps[0], global_init, positions);

			std::cout << "(G0)";
		}

		// local detection until the last step
		for (size_t i = 1; i < config.m_steps.size()-1; ++i) 
		{
			boost::timer::auto_cpu_timer timer;
			detector.LocalDetectAnatomy(root_folder.c_str(), config.m_steps[i], positions);
			std::cout << "(L" << i << ")";
		}

		{	// last detection step
			boost::timer::auto_cpu_timer timer;

			const JAT_detect_config_unit& last_step = config.m_steps[config.m_steps.size() - 1];
			vect3<double> radius(-1, -1, -1); // using the default one saved during training
			vect3<int> stride(last_step.stride, last_step.stride, last_step.stride);

			detector.SetDetector(root_folder, last_step.name, last_step.level, last_step.scale);
			detector.ComputeLocalDisplacementMaps(positions, radius, stride, dmaps);

			std::cout << "(L" << config.m_steps.size()-1 << ")";
		}
	}

	mxImage<double> output_map;
	extract_dist_map_from_displacement_maps(dmaps, dims, output_map);

	// output final map
	if (! ImageHelper::WriteImage<double, double>(output_map, out_path) )
	{
		std::cerr << "fail to write output image to " << out_path << std::endl;
		exit(-1);
	}
}


template <typename T>
void compute_vote_map(std::string image_path, std::string root_folder, const JAT_detect_config& config, std::vector<int>& lm_idxs, const char* out_path)
{ 
	for (size_t i = 0; i < lm_idxs.size(); i++)
	{
		lm_idxs[i] -= 1;
		assert_message(lm_idxs[i] >= 0, "given anatomy indices should start with 1");
	}

	assert_message(config.m_steps.size() > 0, "empty steps");

	JAT_detect<T> detector;

	// load testing image
	if (!detector.LoadImage(image_path.c_str()))
	{
		std::cerr << "fail to load testing image from " << image_path << std::endl;
		exit(-1);
	}
	detector.SetTreeFusionType(SimpleRegressor::InverseWeighted);

	// setup constraint landmarks
	std::map< int, vect3<double> > required_landmarks;
	detector.ComputeRequiredLandmarks(root_folder.c_str(), config, required_landmarks);
	detector.SetConstraintLandmarks(required_landmarks);

	std::pair< vect3<double>, int > global_init[3];
	detector.GetGlobalInitialPosition(required_landmarks, config, global_init);

	// compute displacement maps
	mxImage<double> vote_image;

	if (config.m_steps.size() == 1)
	{
		boost::timer::auto_cpu_timer timer;

		JAT_DMapType dmaps;

		const JAT_detect_config_unit& step = config.m_steps[0];
		vect3<int> stride(step.stride, step.stride, step.stride);

		detector.SetDetector(root_folder, step.name, step.level, step.scale);
		detector.ComputeGlobalDisplacementMaps(global_init, stride, dmaps);

		const JATDetectorInfo& info = detector.GetDetectorInfo();
		for (size_t i = 0; i < lm_idxs.size(); i++)
		{
			int lm_idx = lm_idxs[i];
			assert_message(lm_idx >= 0 && lm_idx < info.anatomy_ids.size(), "landmark index out of bound");

			if (i == 0)
				detector.LandmarkGlobalVote(dmaps, lm_idx, vote_image, true);
			else
				detector.LandmarkGlobalVote(dmaps, lm_idx, vote_image, false);
		}

		std::cout << "(G0)";
	}
	else
	{
		JAT_anatomy_positions positions;

		{	// global detection
			boost::timer::auto_cpu_timer timer;
			detector.GlobalDetectAnatomy(root_folder.c_str(), config.m_steps[0], global_init, positions);

			std::cout << "(G0)";
		}

		// local detection until the last step
		for (size_t i = 1; i < config.m_steps.size() - 1; ++i)
		{
			boost::timer::auto_cpu_timer timer;
			detector.LocalDetectAnatomy(root_folder.c_str(), config.m_steps[i], positions);
			std::cout << "(L" << i << ")";
		}

		{	// last detection step
			boost::timer::auto_cpu_timer timer;

			JAT_DMapType dmaps;

			const JAT_detect_config_unit& last_step = config.m_steps[config.m_steps.size() - 1];
			vect3<double> radius(-1, -1, -1); // using the default one saved during training
			vect3<int> stride(last_step.stride, last_step.stride, last_step.stride);

			detector.SetDetector(root_folder, last_step.name, last_step.level, last_step.scale);
			detector.ComputeLocalDisplacementMaps(positions, radius, stride, dmaps);

			const JATDetectorInfo& info = detector.GetDetectorInfo();
			for (size_t i = 0; i < lm_idxs.size(); i++)
			{
				int lm_idx = lm_idxs[i];
				assert_message(lm_idx >= 0 && lm_idx < info.anatomy_ids.size(), "landmark index out of bound");

				if (i == 0)
					detector.LandmarkLocalVote(dmaps, lm_idx, positions, radius, vote_image, true);
					//detector.LandmarkLocalVote(dmaps, anatomy_index, vote_image, true);
				else
					detector.LandmarkLocalVote(dmaps, lm_idx, positions, radius, vote_image, false);
					//detector.LandmarkLocalVote(dmaps, anatomy_index, vote_image, false);
			}

			std::cout << "(L" << config.m_steps.size() - 1 << ")";
		}
	}

	if (!ImageHelper::WriteImage<double,double>(vote_image, out_path))
	{
		std::cerr << "fail to write vote map to " << out_path << std::endl;
		exit(-1);
	}
}


template <typename T>
void anatomy_detect( const std::vector<std::string>& image_paths, std::string root_folder, const JAT_detect_config& config, std::string output_path)
{
	boost::timer::auto_cpu_timer main_timer;

	JAT_detect<T> detector;
	JAT_anatomy_positions positions;

	for (size_t i = 0; i < image_paths.size(); i++)
	{
		boost::timer::auto_cpu_timer image_timer;

		std::cout << "Detecting the anatomies on " << image_paths[i] << std::endl;

		if (!detector.LoadImage(image_paths[i].c_str()))
			err_message("fail to load input image");

		detector.SetTreeFusionType( SimpleRegressor::InverseWeighted );

		JAT_anatomy_positions positions_for_one_image;
		detector.DetectAnatomy(root_folder.c_str(), config, positions_for_one_image);

		positions.aggregate(positions_for_one_image);

		std::cout << "(All)";
	}

	std::cout << std::endl;

	if (!positions.save_to_file(image_paths, output_path.c_str()))
		err_message("fail to save detection results");
}


void compute_mean_mesh( std::string mesh_file_path, std::string transform_type, std::string output_path )
{
	JAT_mesh_struct mesh_annot;

	if (!mesh_annot.parse(mesh_file_path.c_str())) {
		std::cerr << "fail to parse mesh annotation file" << std::endl; exit(-1);
	}

	std::vector<PDM*> surf_pdms;
	{
		int bd_num = mesh_annot.bd_num;
		surf_pdms.resize(mesh_annot.subject_num);
		for (size_t i = 0; i < surf_pdms.size(); ++i)
		{
			Surface* surf = new Surface;
			for (int j = 0; j < bd_num; ++j)
			{
				std::string mesh_path = mesh_annot.get_mesh_path(i, j);

				Surface sub_surf;
				if (!SurfaceUtils::LoadSurface(mesh_path.c_str(), sub_surf))
					err_message("fail to load sub-surface");

				SurfaceUtils::AddSubsurface(*surf, sub_surf);
			}
			surf_pdms[i] = surf;
		}
	}

	// align them into template space
	if (transform_type == "affine")
	{
		PDMAffineTransform T;
		unsigned int median_idx = PDMUtils::CommonPDM(surf_pdms, T);
		PDMUtils::AlignToTemplate(surf_pdms, median_idx, T);
	}
	else if (transform_type == "rigid")
	{
		PDMRigidTransform T;
		unsigned int median_idx = PDMUtils::CommonPDM(surf_pdms, T);
		PDMUtils::AlignToTemplate(surf_pdms, median_idx, T);
	}
	else if (transform_type == "similarity")
	{
		PDMSimilarityTransform T;
		unsigned int median_idx = PDMUtils::CommonPDM(surf_pdms, T);
		PDMUtils::AlignToTemplate(surf_pdms, median_idx, T);
	}
	else {
		err_message("undefined transform");
	}

	PDM surf_mean = PDMUtils::MeanPDM(surf_pdms);
	const std::vector<float>& surf_mean_verts = surf_mean.GetPoints();
	const std::vector< vect3<int> >& surf_faces = static_cast<Surface*>(surf_pdms[0])->GetFaces();

	if (!SurfaceUtils::SaveSurface(&(surf_mean_verts[0]), surf_mean_verts.size() / 3, surf_faces, output_path.c_str()))
		err_message("fail to save mean surface to disk");

	// release resources
	for (size_t i = 0; i < surf_pdms.size(); ++i)
	{
		if (surf_pdms[i] != NULL)
		{
			delete surf_pdms[i];
			surf_pdms[i] = NULL;
		}
	}
}


void align_mesh_mesh( std::string meshA_file_path, std::string meshB_file_path, std::string transform_type, std::string output_folder )
{
	// load annotations for meshA and meshB
	JAT_mesh_struct meshA_annot, meshB_annot;

	if (!meshA_annot.parse(meshA_file_path.c_str()))
	{
		std::cerr << "fail to parse meshA annotation file" << std::endl;
		exit(-1);
	}

	if (!meshB_annot.parse(meshB_file_path.c_str()))
	{
		std::cerr << "fail to parse meshB annotation file" << std::endl;
		exit(-1);
	}

	assert_message(meshA_annot.subject_num == meshB_annot.subject_num, "inconsistent subject numbers");

	// load surfaces for meshA and meshB
	std::vector<PDM*> surfA_pdms;
	{
		int bd_num = meshA_annot.bd_num;
		surfA_pdms.resize(meshA_annot.subject_num);
		for (size_t i = 0; i < surfA_pdms.size(); ++i)
		{
			Surface* surf = new Surface;

			for (int j = 0; j < bd_num; ++j)
			{
				std::string mesh_path = meshA_annot.get_mesh_path(i, j);

				Surface sub_surf;
				if (!SurfaceUtils::LoadSurface(mesh_path.c_str(), sub_surf))
					err_message("fail to load sub-surface");

				SurfaceUtils::AddSubsurface(*surf, sub_surf);
			}

			surfA_pdms[i] = surf;
		}
	}

	std::vector<PDM*> surfB_pdms;
	{
		int bd_num = meshB_annot.bd_num;
		surfB_pdms.resize(meshB_annot.subject_num);
		for (size_t i = 0; i < surfB_pdms.size(); ++i)
		{
			Surface* surf = new Surface;

			for (int j = 0; j < bd_num; ++j)
			{
				std::string mesh_path = meshB_annot.get_mesh_path(i, j);

				Surface sub_surf;
				if (!SurfaceUtils::LoadSurface(mesh_path.c_str(), sub_surf))
					err_message("fail to load sub-surface");

				SurfaceUtils::AddSubsurface(*surf, sub_surf);
			}

			surfB_pdms[i] = surf;
		}
	}

	// align them into template space
	if (transform_type == "affine")
	{
		PDMAffineTransform T;
		unsigned int median_idx = PDMUtils::CommonPDM(surfA_pdms, T);
		PDMUtils::AlignToTemplateWithLandmarks(surfA_pdms, surfB_pdms, median_idx, T);
	}
	else if (transform_type == "rigid")
	{
		PDMRigidTransform T;
		unsigned int median_idx = PDMUtils::CommonPDM(surfA_pdms, T);
		PDMUtils::AlignToTemplateWithLandmarks(surfA_pdms, surfB_pdms, median_idx, T);
	}
	else if (transform_type == "similarity")
	{
		PDMSimilarityTransform T;
		unsigned int median_idx = PDMUtils::CommonPDM(surfA_pdms, T);
		PDMUtils::AlignToTemplateWithLandmarks(surfA_pdms, surfB_pdms, median_idx, T);
	}
	else {
		err_message("undefined transform");
	}

	// save surfA_pdms and surfB_pdms
	{
		for (size_t i = 0; i < surfA_pdms.size(); ++i)
		{
			std::string output_surf_path = output_folder + stringUtils::Slash() + "alignA" + stringUtils::num2str(i) + ".vtk";

			Surface* surf = static_cast<Surface*>(surfA_pdms[i]);
			if (!SurfaceUtils::SaveSurface(*surf, output_surf_path.c_str()))
				err_message("fail to write aligned surface to disk");
		}

		// save mean surfaceA and surfaceB
		PDM surfA_mean = PDMUtils::MeanPDM(surfA_pdms);
		const std::vector<float>& surf_mean_verts = surfA_mean.GetPoints();
		const std::vector< vect3<int> >& surf_faces = static_cast<Surface*>(surfA_pdms[0])->GetFaces();
		std::string output_surf_mean_path = output_folder + stringUtils::Slash() + "surfaceA_mean.vtk";

		if (!SurfaceUtils::SaveSurface(&(surf_mean_verts[0]), surf_mean_verts.size() / 3, surf_faces, output_surf_mean_path.c_str()))
			err_message("fail to save mean surface to disk");
	}

	{
		for (size_t i = 0; i < surfB_pdms.size(); ++i)
		{
			std::string output_surf_path = output_folder + stringUtils::Slash() + "alignB" + stringUtils::num2str(i) + ".vtk";

			Surface* surf = static_cast<Surface*>(surfB_pdms[i]);
			if (!SurfaceUtils::SaveSurface(*surf, output_surf_path.c_str()))
				err_message("fail to write aligned surface to disk");
		}

		PDM surfB_mean = PDMUtils::MeanPDM(surfB_pdms);
		const std::vector<float>& surf_mean_verts = surfB_mean.GetPoints();
		const std::vector< vect3<int> >& surf_faces = static_cast<Surface*>(surfB_pdms[0])->GetFaces();
		std::string output_surf_mean_path = output_folder + stringUtils::Slash() + "surfaceB_mean.vtk";

		if (!SurfaceUtils::SaveSurface(&(surf_mean_verts[0]), surf_mean_verts.size() / 3, surf_faces, output_surf_mean_path.c_str()))
			err_message("fail to save mean surface to disk");
	}


	// release resources
	for (size_t i = 0; i < surfA_pdms.size(); ++i)
	{
		if (surfA_pdms[i] != NULL)
		{
			delete surfA_pdms[i];
			surfA_pdms[i] = NULL;
		}
	}

	for (size_t i = 0; i < surfB_pdms.size(); ++i)
	{
		if (surfB_pdms[i] != NULL)
		{
			delete surfB_pdms[i];
			surfB_pdms[i] = NULL;
		}
	}
}


void align_mesh_landmark( std::string lm_file_path, std::string mesh_file_path, std::string transform_type, std::string output_folder )
{
	JAT_annot_struct lm_annot;
	if ( !lm_annot.parse(lm_file_path.c_str()) )
	{
		std::cerr << "fail to parse landmark annotation file" << std::endl;
		exit(-1);
	}

	JAT_mesh_struct mesh_annot;
	if ( !mesh_annot.parse(mesh_file_path.c_str()) )
	{
		std::cerr << "fail to parse mesh annotation file" << std::endl;
		exit(-1);
	}

	assert_message(lm_annot.image_paths.size() == mesh_annot.subject_num, "inconsistent image numbers");

	// construct landmark PDMs
	int lm_num = lm_annot.positions.ids.size();

	std::vector<PDM*> lm_pdms;
	lm_pdms.resize( lm_annot.image_paths.size() );
	for (size_t i = 0; i < lm_pdms.size(); ++i)
	{
		lm_pdms[i] = new PDM;
		std::vector<float>& verts = lm_pdms[i]->GetPoints();
		verts.resize( lm_num * 3 );

		for (int j = 0; j < lm_num; ++j)
		{
			vect3<double> lm_pos = lm_annot.positions.get_lm_world(i, j);
			verts[j * 3] = lm_pos[0];
			verts[j * 3 + 1] = lm_pos[1];
			verts[j * 3 + 2] = lm_pos[2];
		}
	}
	
	// load surfaces
	int bd_num = mesh_annot.bd_num;
	std::vector<PDM*> surf_pdms;
	surf_pdms.resize( mesh_annot.subject_num );
	for (size_t i = 0; i < surf_pdms.size(); ++i)
	{
		Surface* surf = new Surface;

		for (int j = 0; j < bd_num; ++j)
		{
			std::string mesh_path = mesh_annot.get_mesh_path(i, j);

			Surface sub_surf;
			if (!SurfaceUtils::LoadSurface(mesh_path.c_str(), sub_surf))
				err_message("fail to load sub-surface");

			SurfaceUtils::AddSubsurface(*surf, sub_surf);
		}

		surf_pdms[i] = surf;
	}


	// align them into template space
	if (transform_type == "affine")
	{
		PDMAffineTransform T;
		unsigned int median_idx = PDMUtils::CommonPDM(surf_pdms, T);
		PDMUtils::AlignToTemplateWithLandmarks(surf_pdms, lm_pdms, median_idx, T);
	}
	else if (transform_type == "rigid")
	{
		PDMRigidTransform T;
		unsigned int median_idx = PDMUtils::CommonPDM(surf_pdms, T);
		PDMUtils::AlignToTemplateWithLandmarks(surf_pdms, lm_pdms, median_idx, T);
	}
	else if (transform_type == "similarity")
	{
		PDMSimilarityTransform T;
		unsigned int median_idx = PDMUtils::CommonPDM(surf_pdms, T);
		PDMUtils::AlignToTemplateWithLandmarks(surf_pdms, lm_pdms, median_idx, T);
	}
	else {
		err_message("undefined transform");
	}


	// save lm_pdms
	for (size_t i = 0; i < lm_pdms.size(); ++i)
	{
		const std::vector<float>& verts = lm_pdms[i]->GetPoints();
		int vert_num = verts.size() / 3;

		for (int j = 0; j < vert_num; ++j)
		{
			vect3<double> lm_pos(verts[j * 3], verts[j * 3 + 1], verts[j * 3 + 2]);
			lm_annot.positions.set_lm_world(i, j, lm_pos);
		}
	}

	std::string output_lm_path = output_folder + stringUtils::Slash() + "landmarks_aligned.txt";
	if (!lm_annot.save_to_file(output_lm_path.c_str()))
		err_message("fail to save aligned landmarks to disk");

	// save mean_landmarks
	PDM lm_mean = PDMUtils::MeanPDM(lm_pdms);
	JAT_anatomy_positions mean_lm_pos = JAT_anatomy_positions::create_from_landmarks(lm_mean.GetPoints(), lm_annot.positions.ids);
	std::string output_mean_lm_path = output_folder + stringUtils::Slash() + "landmark_mean.txt";
	std::vector<std::string> fake_image_paths;
	fake_image_paths.push_back("MEAN_LANDMARK_PATH");
	if (!mean_lm_pos.save_to_file(fake_image_paths, output_mean_lm_path.c_str()))
		err_message("fail to save mean landmark to disk");

	// save surf_pdms
	for (size_t i = 0; i < surf_pdms.size(); ++i)
	{
		std::string output_surf_path = output_folder + stringUtils::Slash() + "align" + stringUtils::num2str(i) + ".vtk";

		Surface* surf = static_cast<Surface*>(surf_pdms[i]);
		if (!SurfaceUtils::SaveSurface(*surf, output_surf_path.c_str()))
			err_message("fail to write aligned surface to disk");
	}

	// save mean surface
	PDM surf_mean = PDMUtils::MeanPDM(surf_pdms);
	const std::vector<float>& surf_mean_verts = surf_mean.GetPoints();
	const std::vector< vect3<int> >& surf_faces = static_cast<Surface*>(surf_pdms[0])->GetFaces();
	std::string output_surf_mean_path = output_folder + stringUtils::Slash() + "surface_mean.vtk";

	if (!SurfaceUtils::SaveSurface(&(surf_mean_verts[0]), surf_mean_verts.size() / 3, surf_faces, output_surf_mean_path.c_str()))
		err_message("fail to save mean surface to disk");

	// release landmark PDMs and surfaces
	for (size_t i = 0; i < lm_pdms.size(); ++i)
	{
		if (lm_pdms[i] != NULL)
		{
			delete lm_pdms[i];
			lm_pdms[i] = NULL;
		}
	}

	for (size_t i = 0; i < surf_pdms.size(); ++i)
	{
		if (surf_pdms[i] != NULL)
		{
			delete surf_pdms[i];
			surf_pdms[i] = NULL;
		}
	}
}


template <typename T>
void mean_shape_init( std::string dst_lm_file, std::string src_lm_file, std::string src_mesh_file, std::string anatomy_name, std::string out_folder, int output_idx )
{
	JAT_annot_struct dst_annot;
	if (!dst_annot.parse(dst_lm_file.c_str()))
		err_message("fail to parse destination landmark file");

	JAT_annot_struct src_annot;
	if (!src_annot.parse(src_lm_file.c_str()))
		err_message("fail to parse source landmark file");
	assert_message(src_annot.positions.image_num == 1, "image num for src_annot should be 1");

	PDM src_lm_pdm = src_annot.positions.create_pdm(0);

	Surface src_mesh;
	if (!SurfaceUtils::LoadSurface(src_mesh_file.c_str(), src_mesh))
		err_message("fail to load mean mesh");

	int start_out_idx = output_idx;
	for (int i = 0; i < dst_annot.positions.image_num; ++i)
	{
		PDM dst_lm_pdm = dst_annot.positions.create_pdm(i);

		T transform;
		transform.Estimate(src_lm_pdm, dst_lm_pdm);

		PDM aligned_surf;
		transform.Apply(src_mesh, aligned_surf);

		std::string output_path = out_folder + stringUtils::Slash() + anatomy_name + stringUtils::num2str(start_out_idx) + ".vtk";
		++start_out_idx;

		const std::vector<float>& out_verts = aligned_surf.GetPoints();
		if (!SurfaceUtils::SaveSurface(&(out_verts[0]), out_verts.size() / 3, src_mesh.GetFaces(), output_path.c_str()))
			err_message("fail to save output surface");
	}
}


template <typename T>
void shape_based_init( std::string dst_meshA_path, std::string src_meshA_path, std::string src_meshB_path, std::string output_path )
{
	Surface src_meshA, src_meshB, dst_meshA;
	if (!SurfaceUtils::LoadSurface(src_meshA_path.c_str(), src_meshA)) {
		std::cerr << "fails to load source surface A from " << src_meshA_path << std::endl; exit(-1);
	}

	if (!SurfaceUtils::LoadSurface(src_meshB_path.c_str(), src_meshB)) {
		std::cerr << "fails to load source surface B from " << src_meshB_path << std::endl; exit(-1);
	}

	if (!SurfaceUtils::LoadSurface(dst_meshA_path.c_str(), dst_meshA)) {
		std::cerr << "fails to load destination surface A from " << dst_meshA_path << std::endl; exit(-1);
	}

	T transform;
	transform.Estimate(src_meshA, dst_meshA);

	PDM dst_surfB;
	transform.Apply(src_meshB, dst_surfB);

	const std::vector<float>& out_verts = dst_surfB.GetPoints();
	if (!SurfaceUtils::SaveSurface(&(out_verts[0]), out_verts.size() / 3, src_meshB.GetFaces(), output_path.c_str())) {
		std::cerr << "fails to write destination surface B to " << output_path << std::endl; exit(-1);
	}
}


void convert_gt_to_annot(std::string gt_path, std::string name, std::string out_path)
{
	std::vector<std::string> lines;
	if (!stringUtils::ReadLines(gt_path.c_str(), lines))
	{
		std::cerr << "fail to read gt annotation from " << gt_path << std::endl;
		exit(-1);
	}

	JAT_annot_struct annot;
	annot.positions.ids.push_back( std::atoi(name.c_str()) );
	annot.positions.image_num = 0;

	for (size_t i = 0; i < lines.size(); i += 2)
	{
		if (i + 1 >= lines.size())
			break;

		if (lines[i].size() < 4)
			break;

		std::string image_path = lines[i];
		std::vector<std::string> tokens = stringUtils::Split(lines[i+1], '\t');
		if (tokens.size() < 8)
			err_message("annotation parts less than 8");

		vect3<double> lm_world;
		lm_world[0] = std::atof(tokens[5].c_str());
		lm_world[1] = std::atof(tokens[6].c_str());
		lm_world[2] = std::atof(tokens[7].c_str());

		std::vector< vect3<double> > lms;
		lms.push_back(lm_world);

		annot.image_paths.push_back(image_path);
		annot.positions.lm_worlds.push_back(lms);
		annot.positions.image_num += 1;
	}

	if (!annot.save_to_file(out_path.c_str()))
	{
		std::cerr << "fail to save " << out_path << std::endl;
		exit(-1);
	}
}



int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display help information");
		generic.add_options()("dist", "compute distance map");
		generic.add_options()("vote", "compute vote map");
		generic.add_options()("sample", "generate sample detect config file");
		generic.add_options()("detect", "detect all anatomies in image(s)");
		generic.add_options()("align", "landmark and shape alignment");
		generic.add_options()("align2", "shape and shape alignment");
		generic.add_options()("affine", "affine transformation");
		generic.add_options()("rigid", "rigid transformation");
		generic.add_options()("similarity", "similarity transformation");
		generic.add_options()("initshape", "landmark-based shape initialization");
		generic.add_options()("initshape2", "shape-based shape initialization");
		generic.add_options()("fuse", "combine different detection results");
		generic.add_options()("aggregate", "aggregate results from different patients");
		generic.add_options()("old2new", "convert old detection output to new detection output");
		generic.add_options()("rotate", "rotate landmarks");
		generic.add_options()("gt2new", "convert gt format to new format");
		generic.add_options()("mean", "compute mean mesh");

		options_description params("Input parameters");
		params.add_options()("image", value<std::string>(), "specify the image path");
		params.add_options()("root", value<std::string>(), "specify the root folder");
		params.add_options()("name", value<std::string>(), "specify the anatomy name");
		params.add_options()("dims", value<std::string>(), "specify the dimensions to visualize");
		params.add_options()("out", value<std::string>(), "specify the output path");
		params.add_options()("idx", value<std::string>(), "specify anatomy indices");
		params.add_options()("config", value<std::string>(), "detect config file");
		params.add_options()("list", value<std::string>(), "string lists");
		params.add_options()("init", value<std::string>(), "initial positions");
		params.add_options()("radius", value<double>(), "radius (mm)");
		params.add_options()("lm", value<std::string>(), "landmark annotation path");
		params.add_options()("mesh", value<std::string>(), "mesh annotation path");
		params.add_options()("meshA", value<std::string>(), "guiding mesh");
		params.add_options()("meshB", value<std::string>(), "guided mesh");
		params.add_options()("srcmeshA", value<std::string>(), "source meshA");
		params.add_options()("srcmeshB", value<std::string>(), "source meshB");
		params.add_options()("dstmeshA", value<std::string>(), "dst meshA");
		params.add_options()("dstmeshB", value<std::string>(), "dst meshB");
		params.add_options()("srclm", value<std::string>(), "source landmark position file");
		params.add_options()("dstlm", value<std::string>(), "dstination landmark position file");
		params.add_options()("srcmesh", value<std::string>(), "source mesh file");
		params.add_options()("folder", value<std::string>(), "folder path");
		params.add_options()("file", value<std::string>(), "file path");
		params.add_options()("center", value<std::string>(), "rotation world center");
		params.add_options()("angle", value<std::string>(), "angles");
		params.add_options()("outidx", value<int>(), "output index");

		
		options_description cmd_options;
		cmd_options.add(generic).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			std::cerr << cmd_options << std::endl;
			print_usage();
			return -1;
		}

		if (vm.count("dist"))
		{
			std::string image_path = BoostCmdHelper::Get<std::string>(vm, "image");
			std::string root_folder = BoostCmdHelper::Get<std::string>(vm, "root");
			std::string config_path = BoostCmdHelper::Get<std::string>(vm, "config");
			std::string dims_str = BoostCmdHelper::Get<std::string>(vm, "dims");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			std::vector<int> dims;
			BoostCmdHelper::ParseIdxOption(dims_str, dims);

			JAT_detect_config config;
			if (!config.read(config_path.c_str())) {
				std::cerr << "fail to read config file from " << config_path << std::endl;
				return -1;
			}

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(image_path.c_str());
			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR:
				compute_distance_map<unsigned char>(image_path, root_folder, config, dims, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR:
				compute_distance_map<char>(image_path, root_folder, config, dims, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT:
				compute_distance_map<unsigned short>(image_path, root_folder, config, dims, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT:
				compute_distance_map<short>(image_path, root_folder, config, dims, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::UINT:
				compute_distance_map<unsigned int>(image_path, root_folder, config, dims, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::INT:
				compute_distance_map<int>(image_path, root_folder, config, dims, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG:
				compute_distance_map<unsigned long>(image_path, root_folder, config, dims, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::LONG:
				compute_distance_map<long>(image_path, root_folder, config, dims, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT:
				compute_distance_map<float>(image_path, root_folder, config, dims, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE:
				compute_distance_map<double>(image_path, root_folder, config, dims, out_path.c_str()); break;
			default:
				std::cerr << "unrecognized pixel type" << std::endl;
				return -1;
			}
		}
		else if (vm.count("vote"))
		{
			std::string image_path = BoostCmdHelper::Get<std::string>(vm, "image");
			std::string root_folder = BoostCmdHelper::Get<std::string>(vm, "root");
			std::string config_path = BoostCmdHelper::Get<std::string>(vm, "config");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			std::vector<int> idxs;
			if (vm.count("idx"))
			{
				std::string idx_str = BoostCmdHelper::Get<std::string>(vm, "idx");
				BoostCmdHelper::ParseIdxOption(idx_str, idxs);
			}
			else {
				err_message("please specify the anatomy index(s) to vote");
			}

			JAT_detect_config config;
			if (!config.read(config_path.c_str())) {
				std::cerr << "fail to read config file from " << config_path << std::endl;
				return -1;
			}

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(image_path.c_str());
			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR:
				compute_vote_map<unsigned char>(image_path, root_folder, config, idxs, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR:
				compute_vote_map<char>(image_path, root_folder, config, idxs, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT:
				compute_vote_map<unsigned short>(image_path, root_folder, config, idxs, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT:
				compute_vote_map<short>(image_path, root_folder, config, idxs, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::UINT:
				compute_vote_map<unsigned int>(image_path, root_folder, config, idxs, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::INT:
				compute_vote_map<int>(image_path, root_folder, config, idxs, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG:
				compute_vote_map<unsigned long>(image_path, root_folder, config, idxs, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::LONG:
				compute_vote_map<long>(image_path, root_folder, config, idxs, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT:
				compute_vote_map<float>(image_path, root_folder, config, idxs, out_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE:
				compute_vote_map<double>(image_path, root_folder, config, idxs, out_path.c_str()); break;
			default:
				std::cerr << "unrecognized pixel type" << std::endl;
				return -1;
			}

		}
		else if (vm.count("sample")) {

			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			if (!JAT_detect_config::write_sample(output_path.c_str())) 
			{
				std::cerr << "fail to output the sample detect config file" << std::endl;
				return -1;
			}

		}
		else if (vm.count("detect")) {

			std::string config_file = BoostCmdHelper::Get<std::string>(vm, "config");

			JAT_detect_config config; 
			if (!config.read(config_file.c_str())) {
				std::cerr << "fail to read config file from " << config_file << std::endl;
				return -1;
			}

			std::string root_folder = BoostCmdHelper::Get<std::string>(vm, "root");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			std::vector< std::string > image_paths;
			if ( vm.count("image") )
			{
				image_paths.push_back( BoostCmdHelper::Get<std::string>(vm, "image") );
			}
			else if (vm.count("list"))
			{
				std::string list_path = BoostCmdHelper::Get<std::string>(vm, "list");
				JAT_annot_struct annot_list;
				if (!annot_list.parse(list_path.c_str()))
					err_message("fail to parse the file");
				image_paths = annot_list.image_paths;
			}
			else 
			{
				err_message("--image or --list option missing");
			}

			assert_message( image_paths.size() > 0, "empty image paths" );

			ImageHelper::PixelType ptype = ImageHelper::ReadPixelType(image_paths[0].c_str());
			switch (ptype)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: anatomy_detect<unsigned char>(image_paths, root_folder, config, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: anatomy_detect<char>(image_paths, root_folder, config, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: anatomy_detect<unsigned short>(image_paths, root_folder, config, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: anatomy_detect<short>(image_paths, root_folder, config, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: anatomy_detect<unsigned int>(image_paths, root_folder, config, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::INT: anatomy_detect<int>(image_paths, root_folder, config, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: anatomy_detect<unsigned long>(image_paths, root_folder, config, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: anatomy_detect<long>(image_paths, root_folder, config, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: anatomy_detect<float>(image_paths, root_folder, config, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: anatomy_detect<double>(image_paths, root_folder, config, output_path); break;
			default: err_message("unrecognized pixel type");
			}

		}
		else if (vm.count("mean")) {

			std::string mesh_list_file = BoostCmdHelper::Get<std::string>(vm, "mesh");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			if (vm.count("affine"))
				compute_mean_mesh(mesh_list_file, "affine", output_path);
			else if (vm.count("rigid"))
				compute_mean_mesh(mesh_list_file, "rigid", output_path);
			else if (vm.count("similarity"))
				compute_mean_mesh(mesh_list_file, "similarity", output_path);
			else
				err_message("unrecognized transformation type");

		}
		else if (vm.count("align")) {

			std::string landmark_list_file = BoostCmdHelper::Get<std::string>(vm, "lm");
			std::string mesh_list_file = BoostCmdHelper::Get<std::string>(vm, "mesh");
			std::string out_folder = BoostCmdHelper::Get<std::string>(vm, "out");

			if (vm.count("affine"))
			{
				align_mesh_landmark(landmark_list_file, mesh_list_file, "affine", out_folder);
			}
			else if (vm.count("rigid"))
			{
				align_mesh_landmark(landmark_list_file, mesh_list_file, "rigid", out_folder);
			}
			else if (vm.count("similarity"))
			{
				align_mesh_landmark(landmark_list_file, mesh_list_file, "similarity", out_folder);
			}
			else
				err_message("unrecognized transformation type");
			
		}
		else if (vm.count("align2")) {

			std::string meshA_list_file = BoostCmdHelper::Get<std::string>(vm, "meshA");
			std::string meshB_list_file = BoostCmdHelper::Get<std::string>(vm, "meshB");
			std::string out_folder = BoostCmdHelper::Get<std::string>(vm, "out");

			if (vm.count("affine"))
			{
				align_mesh_mesh(meshA_list_file, meshB_list_file, "affine", out_folder);
			}
			else if (vm.count("rigid"))
			{
				align_mesh_mesh(meshA_list_file, meshB_list_file, "rigid", out_folder);
			}
			else if (vm.count("similarity"))
			{
				align_mesh_mesh(meshA_list_file, meshB_list_file, "similarity", out_folder);
			}
			else
				err_message("unrecognized transformation type");

		}
		else if (vm.count("initshape")) {

			std::string dst_lm_file = BoostCmdHelper::Get<std::string>(vm, "dstlm");
			std::string src_lm_file = BoostCmdHelper::Get<std::string>(vm, "srclm");
			std::string src_mesh_file = BoostCmdHelper::Get<std::string>(vm, "srcmesh");
			std::string anatomy_name = BoostCmdHelper::Get<std::string>(vm, "name");
			std::string out_folder = BoostCmdHelper::Get<std::string>(vm, "out");
			int output_idx = BoostCmdHelper::Get<int>(vm, "outidx");

			if (vm.count("affine"))
			{
				mean_shape_init<PDMAffineTransform>(dst_lm_file, src_lm_file, src_mesh_file, anatomy_name, out_folder, output_idx);
			}
			else if (vm.count("rigid"))
			{
				mean_shape_init<PDMRigidTransform>(dst_lm_file, src_lm_file, src_mesh_file, anatomy_name, out_folder, output_idx);
			}
			else if (vm.count("similarity"))
			{
				mean_shape_init<PDMSimilarityTransform>(dst_lm_file, src_lm_file, src_mesh_file, anatomy_name, out_folder, output_idx);
			}
			else
			{
				err_message("unrecognized transformation type");
			}

		} 
		else if (vm.count("initshape2")) {

			std::string dst_meshA_path = BoostCmdHelper::Get<std::string>(vm, "dstmeshA");
			std::string src_meshA_path = BoostCmdHelper::Get<std::string>(vm, "srcmeshA");
			std::string src_meshB_path = BoostCmdHelper::Get<std::string>(vm, "srcmeshB");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			if (vm.count("affine"))
			{
				shape_based_init<PDMAffineTransform>(dst_meshA_path, src_meshA_path, src_meshB_path, output_path);
			}
			else if (vm.count("rigid"))
			{
				shape_based_init<PDMRigidTransform>(dst_meshA_path, src_meshA_path, src_meshB_path, output_path);
			}
			else if (vm.count("similarity"))
			{
				shape_based_init<PDMSimilarityTransform>(dst_meshA_path, src_meshA_path, src_meshB_path, output_path);
			}
			else
			{
				err_message("unrecognized transformation type");
			}
		}
		else if (vm.count("fuse")) {

			std::string folder = BoostCmdHelper::Get<std::string>(vm, "folder");
			std::string outname = BoostCmdHelper::Get<std::string>(vm, "name");

			std::vector< std::string > files;
			fileUtils::get_all_files(folder, ".*\\.txt", files);

			JAT_annot_struct out;
			bool initialized = false;

			for (size_t i = 0; i < files.size(); ++i)
			{
				if (!initialized)
				{
					if (out.parse(files[i].c_str()))
						initialized = true;
					else
						initialized = false;
				}
				else
				{
					JAT_annot_struct tmp;
					if (!tmp.parse(files[i].c_str()))
						continue;

					// only check the first image path for convenience
					if ((tmp.image_paths.size() != out.image_paths.size()))
						err_message("number of image mismatch");

					if((tmp.image_paths[0] != out.image_paths[0]))
						err_message("first image patch mismatch");

					out.positions.fuse(tmp.positions);
				}
			}

			std::string out_path = folder + stringUtils::Slash() + outname + ".txt";
			if (!out.save_to_file(out_path.c_str()))
				err_message("could not save combined detections to file");
		}
		else if (vm.count("aggregate")) {

			std::string folder = BoostCmdHelper::Get<std::string>(vm, "folder");
			std::string outname = BoostCmdHelper::Get<std::string>(vm, "name");

			std::vector< std::string > files;
			fileUtils::get_all_files(folder, ".*\\.txt", files);

			JAT_annot_struct out;
			bool initialized = false;

			for (size_t i = 0; i < files.size(); ++i)
			{
				if (!initialized)
				{
					if (out.parse(files[i].c_str()))
						initialized = true;
					else
						initialized = false;
				}
				else
				{
					JAT_annot_struct tmp;
					if (!tmp.parse(files[i].c_str()))
						continue;

					// only check the first image path for convenience
					assert_message( tmp.positions.ids.size() == out.positions.ids.size() && tmp.positions.ids[0] == out.positions.ids[0], "mismatch in anatomy types" );

					out.aggregate(tmp);
				}
			}

			std::string out_path = folder + stringUtils::Slash() + outname + ".txt";
			if (!out.save_to_file(out_path.c_str()))
				err_message("could not save combined detections to file");
		}
		else if (vm.count("old2new")) {

			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "file");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			std::map< int, vect3<float> > lm_map;
			PDMUtils::ReadLandmarksFromTxt(input_path.c_str(), lm_map);

			FILE* fp = fopen(output_path.c_str(), "w");
			fprintf(fp, "%d\t%d\n", 1, lm_map.size());
			fprintf(fp, "UNKNOWN_PATH\n");
			
			std::map< int, vect3<float> >::const_iterator it = lm_map.begin();
			while (it != lm_map.end())
			{
				fprintf(fp, "LM\t%d\t0\t0\t0\t%f\t%f\t%f\n", it->first, it->second[0], it->second[1], it->second[2]);
				++it;
			}
			fclose(fp);

		}
		else if (vm.count("rotate")) {

			// compute rotation center
			vect3<double> center;
			if (vm.count("image"))
			{
				std::string image_path = BoostCmdHelper::Get<std::string>(vm, "image");

				mxImage<unsigned char> header;
				if (!ImageHelper::ReadImageHeader(image_path.c_str(), header))
				{
					std::cerr << "fail to read image header from " << image_path << std::endl;
					return -1;
				}

				center = mxImageUtils::WorldCenter(header);
			}
			else if (vm.count("center"))
			{
				std::string str_center = BoostCmdHelper::Get<std::string>(vm, "center");
				center = stringUtils::ParseVect3<double>(str_center, ',');
			}
			else
			{
				err_message("specify either image or center for rotation");
			}

			// parse input 
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "list");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			std::string str_angle = BoostCmdHelper::Get<std::string>(vm, "angle");
			vect3<double> angle = stringUtils::ParseVect3<double>(str_angle, ',');

			std::string append_str;
			if (std::abs(angle[0]) > 1e-6)
				append_str = std::string("x") + stringUtils::num2str<int>(static_cast<int>(angle[0]));
			if (std::abs(angle[1]) > 1e-6)
				append_str = std::string("y") + stringUtils::num2str<int>(static_cast<int>(angle[1]));
			if (std::abs(angle[2]) > 1e-6)
				append_str = std::string("z") + stringUtils::num2str<int>(static_cast<int>(angle[2]));

			// calculate angle in radians
			for (int i = 0; i < 3; ++i)
				angle[i] = angle[i] / 180 * M_PI;

			JAT_annot_struct annot;
			if (!annot.parse(input_path.c_str()))
			{
				std::cerr << "fail to parse annotation file at " << input_path << std::endl;
				return -1;
			}

			for (size_t i = 0; i < annot.image_paths.size(); ++i)
			{
				std::string folder, name, ext;
				stringUtils::get_file_parts(annot.image_paths[i], folder, name, ext);
				annot.image_paths[i] = folder + stringUtils::Slash() + name + "_" + append_str + "." + ext;
			}

			vnl_matrix<double> rot, rot_x, rot_y, rot_z;
			mathUtils::GetRotationMatrix_X(angle[0], rot_x);
			mathUtils::GetRotationMatrix_Y(angle[1], rot_y);
			mathUtils::GetRotationMatrix_Z(angle[2], rot_z);
			rot = rot_z * rot_y * rot_x;

			for (size_t i = 0; i < annot.positions.lm_worlds.size(); ++ i)
			{
				for (size_t k = 0; k < annot.positions.lm_worlds[i].size(); ++k)
				{
					vect3<double>& lm = annot.positions.lm_worlds[i][k];

					vnl_matrix<double> offset;
					offset.set_size(3, 1);
					for (int j = 0; j < 3; ++j)
						offset(j, 0) = lm[j] - center[j];

					offset = rot * offset;

					for (int j = 0; j < 3; ++j)
						lm[j] = center[j] + offset(j, 0);
				}
			}

			if (!annot.save_to_file(output_path.c_str()))
			{
				std::cerr << "fail to save annotation file to " << output_path << std::endl;
				return -1;
			}

		}
		else if (vm.count("gt2new"))
		{
			std::string gt_path = BoostCmdHelper::Get<std::string>(vm, "file");
			std::string name = BoostCmdHelper::Get<std::string>(vm, "name");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			convert_gt_to_annot(gt_path, name, out_path);
		}
		else
		{
			std::cerr << "unknown option" << std::endl;
			return -1;
		}

	}
	catch ( boost::program_options::error& exp )
	{
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}


