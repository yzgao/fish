//
//  PelvicTools.cpp
//  FISH
//
//  Created by Yaozong Gao on 05/30/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#include "stdafx.h"
#include "common/stringUtils.h"
#include "common/aux_func.h"
#include "extern/BoostHelper.h"
#include "common/mxImageUtils.h"
#include "PelvicTools.h"
#include "detector/JAT/JAT_annot_struct.h"
#include "mesh/surface/SurfaceUtils.h"
#include "FemurSegmentation.h"

using namespace BRIC::IDEA::FISH;
typedef unsigned short TPixel;


void print_usage()
{
	std::cerr << "[ Generate Prostate Landmark Annotations ]" << std::endl;
	std::cerr << "PelvicTools --pro [--id 101] --prefix path --idx 200-218,237-272 --postfix path --postfix2 path --out path\n" << std::endl;

	std::cerr << "[ Generate Bounding Box Annotations ]" << std::endl;
	std::cerr << "PelvicTools --box --ids 101,102 --prefix path --idx 200-218,237-272 --postfix path --postfix2 path --out folder\n" << std::endl;

	std::cerr << "[ Initialize shape based on detected bounding box ]" << std::endl;
	std::cerr << "PelvicTools --boxinit --ids 101,102 --folder path --surf path --out path\n" << std::endl;

	std::cerr << "[ Convert MultiLandmark Format to GT Annotation Format ]" << std::endl;
	std::cerr << "PelvicTools --gt --in txt --out folder\n" << std::endl;

	std::cerr << "[ Generate New Boundary Annotations with multiple modalities ]" << std::endl;
	std::cerr << "PelvicTools --boundary --folder path --idx 1-20 --modality {CT|MR|ALL} --out path\n" << std::endl;

	std::cerr << "[ Segment the rectum using threshold and morphological operations ]" << std::endl;
	std::cerr << "PelvicTools --segrec --in prob --ref gt_seg --threshold val --msize val --out path\n" << std::endl;
	std::cerr << "PelvicTools --segrec2 --in prob --ref ref --surf mesh --threshold val --msize val --out path\n" << std::endl;

	std::cerr << "[ Segment the organ using threshold and morphological operations ]" << std::endl;
	std::cerr << "PelvicTools --seg --in prob --ref ref --threshold val --msize val --out path\n" << std::endl;

	std::cerr << "[ Segment femur heads with probabilistic map and deformable model ]" << std::endl;
	std::cerr << "PelvicTools --segfemurL --in prob --ref ref --surf mesh --out path\n" << std::endl;
	std::cerr << "PelvicTools --segfemurR --in prob --ref ref --surf mesh --out path\n" << std::endl;

	std::cerr << "[ decompose multi-label image into single-label images]" << std::endl;
	std::cerr << "PelvicTools --decompose --in path --out folder\n" << std::endl;
}

template <typename T>
void maskout_slice(mxImage<T>& image, int start, int end)
{
	vect3<unsigned int> input_image_size = image.GetImageSize();
	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(start); ++z)
	{
		for (unsigned int y = 0; y < input_image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < input_image_size[0]; ++x)
			{
				image(x, y, z) = 0;
			}
		}
	}

	#pragma omp parallel for
	for (int z = end + 1; z < static_cast<int>(input_image_size[2]); ++z)
	{
		for (unsigned int y = 0; y < input_image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < input_image_size[0]; ++x)
			{
				image(x, y, z) = 0;
			}
		}
	}
}

void maskout_slice(const mxImage<unsigned char>& ref_image, mxImage<unsigned char>& input_image)
{
	unsigned int ref_start = 0, ref_end = 0;
	mxImageUtils::FindSliceRange(ref_image, ref_start, ref_end);

	vect3<double> tmp_start(0, 0, ref_start), tmp_end(0, 0, ref_end);
	mxImageUtils::Voxel2World(ref_image, tmp_start);
	mxImageUtils::Voxel2World(ref_image, tmp_end);

	mxImageUtils::World2Voxel(input_image, tmp_start);
	mxImageUtils::World2Voxel(input_image, tmp_end);

	ref_start = static_cast<unsigned int>(tmp_start[2] + 0.5);
	ref_end = static_cast<unsigned int>(tmp_end[2] + 0.5);

	maskout_slice(input_image, ref_start, ref_end);
}

void segment_organ(std::string prob_path, std::string ref_path, double threshold, int msize, std::string out_path)
{
	mxImage<double> prob_image;
	if (!ImageHelper::ReadImage<double, double>(prob_path.c_str(), prob_image)) {
		std::cerr << "fails to read probability image from " << prob_path << std::endl; exit(-1);
	}

	mxImage<unsigned char> ref_header;
	if (!ImageHelper::ReadImageHeader<unsigned char>(ref_path.c_str(), ref_header)) {
		std::cerr << "fails to read reference header from " << ref_path << std::endl; exit(-1);
	}

	mxImage<unsigned char> seg_image;
	unsigned char foreground_value = 255, background_value = 0;
	mxImageUtils::Threshold(prob_image, seg_image, threshold, foreground_value, background_value);

	vect3<unsigned int> image_size = seg_image.GetImageSize();
	for (int i = 0; i < msize; ++i)
		mxImageUtils::BinaryErode(seg_image, foreground_value);

	mxImageUtils::PickLargestComponent(seg_image);

	for (int i = 0; i < msize; ++i)
		mxImageUtils::BinaryDilate(seg_image, foreground_value);

	mxImageUtils::BinaryFillHoles(seg_image);

	mxImage<unsigned char> transformed_mask_image;
	mxImageUtils::TransformMaskImage(ref_header, seg_image, transformed_mask_image);

	if (!ImageHelper::WriteImage<unsigned char, unsigned char>(transformed_mask_image, out_path.c_str())) {
		std::cerr << "fails to write segmentation image to " << out_path << std::endl; exit(-1);
	}
}

void segment_rectum2(std::string prob_path, std::string mesh_path, std::string ref_path, double threshold, int msize, std::string out_path)
{
	mxImage<double> prob_image;
	if (!ImageHelper::ReadImage<double, double>(prob_path.c_str(), prob_image)) {
		std::cerr << "fails to read probability image from " << prob_path << std::endl; exit(-1);
	}

	mxImage<unsigned char> ref_header;
	if (!ImageHelper::ReadImageHeader<unsigned char>(ref_path.c_str(), ref_header)) {
		std::cerr << "fails to read reference header from " << ref_path << std::endl; exit(-1);
	}

	Surface surf;
	if (!SurfaceUtils::LoadSurface(mesh_path.c_str(), surf)) {
		std::cerr << "fails to read surface from " << mesh_path << std::endl; exit(-1);
	}

	mxImage<unsigned char> init_mask;
	unsigned char outside_val = 0, inside_val = 255;
	SurfaceUtils::CarveSurface(surf, prob_image, outside_val, inside_val, init_mask);

	mxImage<unsigned char> seg_image;
	unsigned char foreground_value = 255, background_value = 0;
	mxImageUtils::Threshold(prob_image, seg_image, threshold, foreground_value, background_value);

	vect3<unsigned int> image_size = seg_image.GetImageSize();
	for (int i = 0; i < msize; ++i)
	{
		for (unsigned int z = 0; z < image_size[2]; ++z)
			mxImageUtils::BinaryErode2D(seg_image, z, foreground_value);
	}
	for (int i = 0; i < msize; ++i)
	{
		for (unsigned int z = 0; z < image_size[2]; ++z)
			mxImageUtils::BinaryDilate2D(seg_image, z, foreground_value);
	}

	mxImageUtils::BinaryFillHoles(seg_image);

	maskout_slice(init_mask, seg_image);
	mxImageUtils::PickLargestComponent(seg_image);

	mxImage<unsigned char> transformed_mask_image;
	mxImageUtils::TransformMaskImage(ref_header, seg_image, transformed_mask_image);

	if (!ImageHelper::WriteImage<unsigned char, unsigned char>(transformed_mask_image, out_path.c_str())) {
		std::cerr << "fails to write segmentation image to " << out_path << std::endl; exit(-1);
	}
}

void segment_rectum(std::string prob_path, std::string gt_path, double threshold, int msize, std::string out_path)
{
	mxImage<double> prob_image;
	if (!ImageHelper::ReadImage<double,double>(prob_path.c_str(), prob_image)) {
		std::cerr << "fails to read probability image from " << prob_path << std::endl; exit(-1);
	}

	mxImage<unsigned char> gt_image;
	if (!ImageHelper::ReadImage<unsigned short, unsigned char>(gt_path.c_str(), gt_image)) {
		std::cerr << "fails to read ground truth from " << gt_path << std::endl; exit(-1);
	}

	mxImage<unsigned char> seg_image;
	unsigned char foreground_value = 255, background_value = 0;
	mxImageUtils::Threshold(prob_image, seg_image, threshold, foreground_value, background_value);

	vect3<unsigned int> image_size = seg_image.GetImageSize();
	for (int i = 0; i < msize; ++i)
	{
		for (unsigned int z = 0; z < image_size[2]; ++z)
			mxImageUtils::BinaryErode2D(seg_image, z, foreground_value);
	}
	for (int i = 0; i < msize; ++i)
	{
		for (unsigned int z = 0; z < image_size[2]; ++z)
			mxImageUtils::BinaryDilate2D(seg_image, z, foreground_value);
	}

	maskout_slice(gt_image, seg_image);
	mxImageUtils::PickLargestComponent(seg_image); 

	mxImage<unsigned char> transformed_mask_image;
	mxImageUtils::TransformMaskImage(gt_image, seg_image, transformed_mask_image);

	if (!ImageHelper::WriteImage<unsigned char, unsigned char>(transformed_mask_image, out_path.c_str())) {
		std::cerr << "fails to write segmentation image to " << out_path << std::endl; exit(-1);
	}
}

void output_prostate_landmarks(boost::program_options::variables_map& vm)
{
	// find out landmark ids for output
	std::vector<int> landmark_ids;
	if (vm.count("id"))
	{
		landmark_ids.push_back(vm["id"].as<int>());
	}
	else {
		for (int i = 101; i <= 106; ++i)
			landmark_ids.push_back(i);
	}

	// find out image indexes
	std::vector<int> indexes;
	BoostCmdHelper::ParseIdxOption(BoostCmdHelper::Get<std::string>(vm, "idx"), indexes);

	// find out input and output path
	std::string prefix = BoostCmdHelper::Get<std::string>(vm, "prefix");
	std::string postfix = BoostCmdHelper::Get<std::string>(vm, "postfix");
	std::string postfix2 = BoostCmdHelper::Get<std::string>(vm, "postfix2");
	std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

	std::vector< vect3<double> > voxel_coords, world_coords;
	PelvicTools::get_prostate_landmarks(prefix, indexes, postfix, landmark_ids, voxel_coords, world_coords);

	FILE* fp = fopen(out_path.c_str(), "w");
	fprintf(fp, "%d\t%d\n", indexes.size(), landmark_ids.size());

	for (size_t i = 0; i < indexes.size(); ++i)
	{
		std::string image_path = prefix + stringUtils::num2str(indexes[i]) + postfix2;
		fprintf(fp, "%s\n", image_path.c_str());

		for (size_t j = 0; j < landmark_ids.size(); ++j)
		{
			size_t index = i * landmark_ids.size() + j;

			vect3<double> voxel_coord = voxel_coords[index];
			vect3<double> world_coord = world_coords[index];

			fprintf(fp, "LM\t%d\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n", landmark_ids[j], voxel_coord[0], voxel_coord[1], voxel_coord[2], world_coord[0], world_coord[1], world_coord[2]);
		}
	}

	fclose(fp);
}

void output_bounding_box_landmarks(boost::program_options::variables_map& vm)
{
	std::vector<int> landmark_ids;
	std::string id_str = BoostCmdHelper::Get<std::string>(vm, "ids");
	stringUtils::str2vec(id_str, landmark_ids, ',');
	assert_message(landmark_ids.size() == 2, "only two ids are required");

	// find out image indexes
	std::vector<int> indexes;
	BoostCmdHelper::ParseIdxOption(BoostCmdHelper::Get<std::string>(vm, "idx"), indexes);

	// find out input and output path
	std::string prefix = BoostCmdHelper::Get<std::string>(vm, "prefix");
	std::string postfix = BoostCmdHelper::Get<std::string>(vm, "postfix");
	std::string postfix2 = BoostCmdHelper::Get<std::string>(vm, "postfix2");
	std::string out_folder = BoostCmdHelper::Get<std::string>(vm, "out");

	std::vector< vect3<int> > sp_voxels, ep_voxels;
	std::vector< vect3<double> > sp_worlds, ep_worlds;
	PelvicTools::get_bounding_boxes(prefix, indexes, postfix, sp_voxels, ep_voxels, sp_worlds, ep_worlds);


	std::string spout_path = out_folder + stringUtils::Slash() + stringUtils::num2str(landmark_ids[0]) + ".txt";
	std::string epout_path = out_folder + stringUtils::Slash() + stringUtils::num2str(landmark_ids[1]) + ".txt";

	FILE* fp = fopen(spout_path.c_str(), "w");
	fprintf(fp, "%d\t1\n", indexes.size());

	for (size_t i = 0; i < indexes.size(); ++i)
	{
		std::string image_path = prefix + stringUtils::num2str(indexes[i]) + postfix2;
		fprintf(fp, "%s\n", image_path.c_str());
		fprintf(fp, "LM\t%d\t%d\t%d\t%d\t%lf\t%lf\t%lf\n", landmark_ids[0], sp_voxels[i][0], sp_voxels[i][1], sp_voxels[i][2], sp_worlds[i][0], sp_worlds[i][1], sp_worlds[i][2]);
	}
	fclose(fp);

	fp = fopen(epout_path.c_str(), "w");
	fprintf(fp, "%d\t1\n", indexes.size());

	for (size_t i = 0; i < indexes.size(); ++i)
	{
		std::string image_path = prefix + stringUtils::num2str(indexes[i]) + postfix2;
		fprintf(fp, "%s\n", image_path.c_str());
		fprintf(fp, "LM\t%d\t%d\t%d\t%d\t%lf\t%lf\t%lf\n", landmark_ids[1], ep_voxels[i][0], ep_voxels[i][1], ep_voxels[i][2], ep_worlds[i][0], ep_worlds[i][1], ep_worlds[i][2]);
	}
	fclose(fp);

}

void convert_to_gt(boost::program_options::variables_map& vm)
{
	std::string input_file = BoostCmdHelper::Get<std::string>(vm, "in");
	std::string output_file = BoostCmdHelper::Get<std::string>(vm, "out");

	std::vector<std::string> image_paths;
	std::vector<int> landmark_ids;
	std::vector< vect3<double> > voxel_coords, world_coords;

	PelvicTools::read_annotations(input_file.c_str(), image_paths, landmark_ids, voxel_coords, world_coords);
	PelvicTools::write_gt_files(output_file.c_str(), image_paths, landmark_ids, voxel_coords, world_coords);
}

void output_new_boundary_annotation(boost::program_options::variables_map& vm)
{
	std::vector<int> indexes;
	BoostCmdHelper::ParseIdxOption(BoostCmdHelper::Get<std::string>(vm, "idx"), indexes);

	std::string folder = BoostCmdHelper::Get<std::string>(vm, "folder");
	std::string str_modality = BoostCmdHelper::Get<std::string>(vm, "modality");
	std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");


	FILE* fp = fopen(out_path.c_str(), "w");
	fprintf(fp, "SUB\t%d\n", indexes.size());
	
	if (str_modality == "MR" || str_modality == "CT")
		fprintf(fp, "MOD\t1\n");
	else if (str_modality == "ALL")
		fprintf(fp, "MOD\t2\n");
	else
		err_message("not such modality option");

	fprintf(fp, "BD\t3\n");
	fprintf(fp, "BDID\tpro\tbla\trec\n");

	for (size_t i = 0; i < indexes.size(); ++i)
	{
		std::string ct_path = folder + stringUtils::Slash() + "p" + stringUtils::num2str(indexes[i]) + stringUtils::Slash() + "ct.mhd";
		std::string mr_path = folder + stringUtils::Slash() + "p" + stringUtils::num2str(indexes[i]) + stringUtils::Slash() + "alignmr.mhd";

		if (str_modality == "CT")
			fprintf(fp, "IMAGE\t%s\n", ct_path.c_str());
		else if (str_modality == "MR")
			fprintf(fp, "IMAGE\t%s\n", mr_path.c_str());
		else if (str_modality == "ALL")
		{
			fprintf(fp, "IMAGE\t%s\n", ct_path.c_str());
			fprintf(fp, "IMAGE\t%s\n", mr_path.c_str());
		}
		else {
			err_message("unrecognized modality option");
		}

		std::string pro_path = folder + stringUtils::Slash() + "p" + stringUtils::num2str(indexes[i]) + stringUtils::Slash() + "ct_pro.mhd";
		std::string bla_path = folder + stringUtils::Slash() + "p" + stringUtils::num2str(indexes[i]) + stringUtils::Slash() + "ct_bla.mhd";
		std::string rec_path = folder + stringUtils::Slash() + "p" + stringUtils::num2str(indexes[i]) + stringUtils::Slash() + "ct_rec.mhd";

		fprintf(fp, "MASK\t%s\n", pro_path.c_str());
		fprintf(fp, "MASK\t%s\n", bla_path.c_str());
		fprintf(fp, "MASK\t%s\n", rec_path.c_str());
	}

	fclose(fp);
}

void box_based_initialize(boost::program_options::variables_map& vm)
{
	std::vector<int> landmark_ids;
	std::string lm_str = BoostCmdHelper::Get<std::string>(vm, "ids");
	stringUtils::str2vec(lm_str, landmark_ids, ',');
	assert_message(landmark_ids.size() == 2, "only two ids are required");

	std::string out_folder = BoostCmdHelper::Get<std::string>(vm, "folder");
	std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "surf");
	std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

	std::string sp_path, ep_path;
	sp_path = out_folder + stringUtils::Slash() + stringUtils::num2str(landmark_ids[0]) + ".txt";
	ep_path = out_folder + stringUtils::Slash() + stringUtils::num2str(landmark_ids[1]) + ".txt";

	JAT_annot_struct sp_annot, ep_annot;
	if (!sp_annot.parse(sp_path.c_str())) {
		std::cerr << "fails to load landmarks from " << sp_path << std::endl;
		exit(-1);
	}

	if (!ep_annot.parse(ep_path.c_str())) {
		std::cerr << "fails to load landmarks from " << ep_path << std::endl;
		exit(-1);
	}

	vect3<double> sp, ep;
	sp = sp_annot.positions.get_lm_world(0, 0);
	ep = ep_annot.positions.get_lm_world(0, 0);

	Surface surf;
	if (!SurfaceUtils::LoadSurface(surf_path.c_str(), surf)) {
		std::cerr << "fails to load surface from " << surf_path << std::endl;
		exit(-1);
	}

	vect3<float> surf_scale;
	vect3<float> surf_sp, surf_ep;
	SurfaceUtils::BoundingBox(surf, surf_sp, surf_ep);
	for (int i = 0; i < 3; ++i)
		surf_scale[i] = std::abs(surf_ep[i] - surf_sp[i]);

	vect3<float> center, scale_ratio;
	for (int i = 0; i < 3; ++i)
	{
		center[i] = (sp[i] + ep[i]) / 2.0;
		scale_ratio[i] = std::abs(ep[i] - sp[i]) / surf_scale[i];
	}

	surf.MoveTo(center);
	surf.Scale(scale_ratio);

	if (!SurfaceUtils::SaveSurface(surf, out_path.c_str()))
	{
		std::cerr << "fails to save surface to " << std::endl;
		exit(-1);
	}
}

void segment_left_femur(boost::program_options::variables_map& vm)
{
	std::string prob_path = BoostCmdHelper::Get<std::string>(vm, "in");
	std::string ref_path = BoostCmdHelper::Get<std::string>(vm, "ref");
	std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "surf");
	std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

	mxImage<double> prob_image;
	if (!ImageHelper::ReadImage<double,double>(prob_path.c_str(), prob_image)) {
		std::cerr << "fails to read image from " << prob_path << std::endl;
		exit(-1);
	}

	mxImage<unsigned short> intensity_image;
	if (!ImageHelper::ReadImage<unsigned short, unsigned short>(ref_path.c_str(), intensity_image)) {
		std::cerr << "fails to read intensity image from " << ref_path << std::endl;
		exit(-1);
	}

	Surface surf;
	if (!SurfaceUtils::LoadSurface(surf_path.c_str(), surf)) {
		std::cerr << "fails to load surface from " << surf_path << std::endl;
		exit(-1);
	}

	mxImage<unsigned char> out_mask;
	FemurSegmentation aux;
	aux.LeftSegment(intensity_image, prob_image, surf, out_mask);

	if (!ImageHelper::WriteImage<unsigned char, unsigned char>(out_mask, out_path.c_str()))
	{
		std::cerr << "fails to write output image to " << out_path << std::endl;
		exit(-1);
	}
}

void segment_right_femur(boost::program_options::variables_map& vm)
{
	std::string prob_path = BoostCmdHelper::Get<std::string>(vm, "in");
	std::string ref_path = BoostCmdHelper::Get<std::string>(vm, "ref");
	std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "surf");
	std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

	mxImage<double> prob_image;
	if (!ImageHelper::ReadImage<double, double>(prob_path.c_str(), prob_image)) {
		std::cerr << "fails to read image from " << prob_path << std::endl;
		exit(-1);
	}

	mxImage<unsigned short> intensity_image;
	if (!ImageHelper::ReadImage<unsigned short, unsigned short>(ref_path.c_str(), intensity_image)) {
		std::cerr << "fails to read intensity image from " << ref_path << std::endl;
		exit(-1);
	}

	Surface surf;
	if (!SurfaceUtils::LoadSurface(surf_path.c_str(), surf)) {
		std::cerr << "fails to load surface from " << surf_path << std::endl;
		exit(-1);
	}

	mxImage<unsigned char> out_mask;
	FemurSegmentation aux;
	aux.RightSegment(intensity_image, prob_image, surf, out_mask);

	if (!ImageHelper::WriteImage<unsigned char, unsigned char>(out_mask, out_path.c_str()))
	{
		std::cerr << "fails to write output image to " << out_path << std::endl;
		exit(-1);
	}
}


void extract_label_image(const mxImage<unsigned char>& multi_label_image, unsigned char idx, mxImage<unsigned char>& single_label_image)
{
	vect3<unsigned int> image_size = multi_label_image.GetImageSize();

	single_label_image.Clear();
	single_label_image.CopyImageInfo(multi_label_image);
	single_label_image.SetImageSize(image_size);
	single_label_image.Fill(0);

	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
			{
				if (multi_label_image(x, y, z) == idx)
					single_label_image(x, y, z) = 255;
			}
		}
	}
}

void decompose_label_image(boost::program_options::variables_map& vm)
{
	std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
	std::string output_folder = BoostCmdHelper::Get<std::string>(vm, "out");
	std::string output_path;

	mxImage<unsigned char> multi_label_image, single_label_image;
	if (!ImageHelper::ReadImage<unsigned char, unsigned char>(input_path.c_str(), multi_label_image)) {
		std::cerr << "fails to read multi-label image from " << input_path << std::endl;
		exit(-1);
	}

	output_path = output_folder + stringUtils::Slash() + "ct_pro.nii.gz";
	extract_label_image(multi_label_image, static_cast<unsigned char>(1), single_label_image);
	if (!ImageHelper::WriteImage<unsigned char, unsigned char>(single_label_image, output_path.c_str()))
	{
		std::cerr << "fails to write prostate label image to " << output_path << std::endl;
		exit(-1);
	}

	output_path = output_folder + stringUtils::Slash() + "ct_bla.nii.gz";
	extract_label_image(multi_label_image, static_cast<unsigned char>(2), single_label_image);
	if (!ImageHelper::WriteImage<unsigned char, unsigned char>(single_label_image, output_path.c_str()))
	{
		std::cerr << "fails to write bladder label image to " << output_path << std::endl;
		exit(-1);
	}

	output_path = output_folder + stringUtils::Slash() + "ct_rec.nii.gz";
	extract_label_image(multi_label_image, static_cast<unsigned char>(3), single_label_image);
	if (!ImageHelper::WriteImage<unsigned char, unsigned char>(single_label_image, output_path.c_str()))
	{
		std::cerr << "fails to write rectum label image to " << output_path << std::endl;
		exit(-1);
	}

	output_path = output_folder + stringUtils::Slash() + "ct_femurL.nii.gz";
	extract_label_image(multi_label_image, static_cast<unsigned char>(4), single_label_image);
	if (!ImageHelper::WriteImage<unsigned char, unsigned char>(single_label_image, output_path.c_str()))
	{
		std::cerr << "fails to write femurL label image to " << output_path << std::endl;
		exit(-1);
	}

	output_path = output_folder + stringUtils::Slash() + "ct_femurR.nii.gz";
	extract_label_image(multi_label_image, static_cast<unsigned char>(5), single_label_image);
	if (!ImageHelper::WriteImage<unsigned char, unsigned char>(single_label_image, output_path.c_str()))
	{
		std::cerr << "fails to write femurR label image to " << output_path << std::endl;
		exit(-1);
	}

}

int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display help information");
		generic.add_options()("pro", "prostate landmark annotations");
		generic.add_options()("box", "bounding box annotations");
		generic.add_options()("boxinit", "box-based surface initialize");
		generic.add_options()("gt", "convert multi-landmark format to GT format");
		generic.add_options()("boundary", "new boundary annotation file");
		generic.add_options()("segrec", "segment rectum");
		generic.add_options()("segrec2", "segment rectum with surface");
		generic.add_options()("seg", "segment with thresholding and morphological operations");
		generic.add_options()("segfemurL", "segment left femur head");
		generic.add_options()("segfemurR", "segment right femur head");
		generic.add_options()("decompose", "decompose multi-label image into single-label images");

		options_description params("Input parameters");
		params.add_options()("prefix", value<std::string>(), "prefix string");
		params.add_options()("postfix", value<std::string>(), "postfix string");
		params.add_options()("postfix2", value<std::string>(), "postfix2 string");
		params.add_options()("folder", value<std::string>(), "input folder path");
		params.add_options()("idx", value<std::string>(), "image indexes");
		params.add_options()("out", value<std::string>(), "output path");
		params.add_options()("id", value<int>(), "landmark id");
		params.add_options()("ids", value<std::string>(), "landmark ids");
		params.add_options()("in", value<std::string>(), "input path");
		params.add_options()("ref", value<std::string>(), "reference path");
		params.add_options()("surf", value<std::string>(), "surface path");
		params.add_options()("threshold", value<double>(), "threshold value");
		params.add_options()("msize", value<int>(), "morphological sizes");
		params.add_options()("modality", value<std::string>(), "modalities");

		options_description cmd_options;
		cmd_options.add(generic).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			std::cerr << cmd_options << std::endl;
			print_usage();
			return -1;
		}

		if (vm.count("pro")) {
			output_prostate_landmarks(vm);
			return 0;
		}

		if (vm.count("box")) {
			output_bounding_box_landmarks(vm);
			return 0;
		}

		if (vm.count("boxinit")) {
			box_based_initialize(vm);
			return 0;
		}

		if (vm.count("gt")) {
			convert_to_gt(vm);
			return 0;
		}

		if (vm.count("boundary")) {
			output_new_boundary_annotation(vm);
			return 0;
		}

		if (vm.count("segrec")) {
			std::string prob_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string gt_path = BoostCmdHelper::Get<std::string>(vm, "ref");
			double threshold = BoostCmdHelper::Get<double>(vm, "threshold");
			int msize = BoostCmdHelper::Get<int>(vm, "msize");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			segment_rectum(prob_path, gt_path, threshold, msize, out_path);

			return 0;
		}

		if (vm.count("segrec2")) {
			std::string prob_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "surf");
			std::string ref_path = BoostCmdHelper::Get<std::string>(vm, "ref");
			double threshold = BoostCmdHelper::Get<double>(vm, "threshold");
			int msize = BoostCmdHelper::Get<int>(vm, "msize");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			segment_rectum2(prob_path, surf_path, ref_path, threshold, msize, out_path);

			return 0;
		}

		if (vm.count("seg")) {
			std::string prob_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string ref_path = BoostCmdHelper::Get<std::string>(vm, "ref");
			double threshold = BoostCmdHelper::Get<double>(vm, "threshold");
			int msize = BoostCmdHelper::Get<int>(vm, "msize");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			segment_organ(prob_path, ref_path, threshold, msize, out_path);
			return 0;
		}

		if (vm.count("segfemurL")) {
			segment_left_femur(vm);
			return 0;
		}

		if (vm.count("segfemurR")) {
			segment_right_femur(vm);
			return 0;
		}

		if (vm.count("decompose")) {
			decompose_label_image(vm);
			return 0;
		}

		std::cerr << "no such option" << std::endl;

	}
	catch (boost::program_options::error& exp)
	{
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}


