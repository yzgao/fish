//
//  HeadNeckTools.cpp
//  FISH
//
//  Created by Yaozong Gao on 11/14/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#include "stdafx.h"
#include "detector/JAT/JAT_annot_struct.h"
#include "extern/BoostHelper.h"

using namespace BRIC::IDEA::FISH;

void print_usage()
{
	std::cerr << "[ extract landmark positions from fcsv files ]" << std::endl;
	std::cerr << "HeadNeckTools --landmark --in path\n" << std::endl;

	std::cerr << "[ split into N folds from entire landmark annotations ]" << std::endl;
	std::cerr << "HeadNeckTools --cv --in path --fold N --lm id --out folder\n" << std::endl;
}

void extract_landmarks(std::string input_path)
{
	std::vector< std::string > lines;
	if (!stringUtils::ReadLines(input_path.c_str(), lines))
		err_message("fail to load fcsv file");

	std::string folder, filename, extension;
	stringUtils::get_file_parts(input_path, folder, filename, extension);
	std::string output_path = folder + stringUtils::Slash() + "landmarks.txt";

	JAT_annot_struct annot;
	annot.image_paths.resize(1);
	annot.image_paths[0] = folder + stringUtils::Slash() + "img.mhd";
	annot.positions.image_num = 1;

	for (size_t i = 0; i < lines.size(); ++i)
	{
		if (lines[i][0] == '#')
			continue;

		std::vector<std::string> tokens;
		stringUtils::Split(lines[i], ',', tokens);

		if (tokens.size() != 14)
			err_message("broken fcsv file");

		// negate the first two coordinates
		vect3<double> lm_world;
		lm_world[0] = -stringUtils::str2num<double>(tokens[1]);
		lm_world[1] = -stringUtils::str2num<double>(tokens[2]);
		lm_world[2] = stringUtils::str2num<double>(tokens[3]);

		annot.positions.lm_worlds[0].push_back(lm_world);

		std::string lm_name = tokens[11];

		if (lm_name == "chin")
			annot.positions.ids.push_back(201);
		else if (lm_name == "mand_r")
			annot.positions.ids.push_back(202);
		else if (lm_name == "mand_l")
			annot.positions.ids.push_back(203);
		else if (lm_name == "odont_proc")
			annot.positions.ids.push_back(204);
		else if (lm_name == "occ_bone")
			annot.positions.ids.push_back(205);
		else
			err_message((std::string("unrecognized landmarks: ") + lm_name).c_str());
	}

	if (!annot.save_to_file(output_path.c_str()))
	{
		std::cerr << "fail to save landmark to " << output_path << std::endl;
		exit(-1);
	}
}

void split_cross_validation(std::string input_path, int fold, int lm_id, std::string out_folder)
{
	JAT_annot_struct annot;
	if (!annot.parse(input_path.c_str()))
	{
		std::cerr << "fail to parse annotation file at: " << input_path << std::endl;
		exit(-1);
	}

	int lm_idx = -1;
	for (size_t i = 0; i < annot.positions.ids.size(); ++i)
	{
		if (annot.positions.ids[i] == lm_id) {
			lm_idx = static_cast<int>(i);
			break;
		}
	}

	if (lm_idx == -1)
		err_message("fail to find the given landmark id in the annotation file");

	int num_images = annot.image_paths.size();
	int num_each_fold = num_images / fold;

	for (int i = 0; i < fold; ++i)
	{
		int start = i * num_each_fold;
		int end = start + num_each_fold;

		if (end > num_images)
			end = num_images;

		if (end <= start)
			continue;

		int num = num_images - (end - start);

		JAT_annot_struct fold_annot;
		fold_annot.image_paths.resize(num);
		fold_annot.positions.image_num = num;
		fold_annot.positions.ids.resize(1);
		fold_annot.positions.ids[0] = lm_id;
		fold_annot.positions.lm_worlds.resize(num);

		for (int j = 0; j < num; ++j)
			fold_annot.positions.lm_worlds[j].resize(1);

		int tmp_idx = 0;
		for (int j = 0; j < num_images; ++j)
		{
			if (j >= start && j < end)
				continue;

			fold_annot.image_paths[tmp_idx] = annot.image_paths[j];
			fold_annot.positions.lm_worlds[tmp_idx][0] = annot.positions.get_lm_world(j, lm_idx);
			++tmp_idx;
		}

		std::string out_path = out_folder + stringUtils::Slash() + "fold" + stringUtils::num2str(i+1) + ".txt";
		if (!fold_annot.save_to_file(out_path.c_str()))
		{
			std::cerr << "fails to write the fold annotation to: " << out_path << std::endl;
			exit(-1);
		}
	}
}

int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display help information");
		generic.add_options()("landmark", "extract landmarks from fcsv files");
		generic.add_options()("cv", "cross validation");

		options_description params("Input parameters");
		params.add_options()("in", value<std::string>(), "specify the input path");
		params.add_options()("fold", value<int>(), "N-fold");
		params.add_options()("lm", value<int>(), "landmark ids");
		params.add_options()("out", value<std::string>(), "out folder");

		options_description cmd_options;
		cmd_options.add(generic).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			std::cerr << cmd_options << std::endl;
			print_usage();
			return -1;
		}

		if (vm.count("landmark"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			extract_landmarks(input_path);
		}
		else if (vm.count("cv"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			int fold = BoostCmdHelper::Get<int>(vm, "fold");
			int lm_id = BoostCmdHelper::Get<int>(vm, "lm");
			std::string out_folder = BoostCmdHelper::Get<std::string>(vm, "out");

			split_cross_validation(input_path, fold, lm_id, out_folder);
		}
		else
		{
			std::cerr << "unrecognized option" << std::endl;
			return -1;
		}
	}
	catch ( boost::program_options::error& exp)
	{
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}