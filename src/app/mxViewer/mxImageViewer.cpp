

#include "extern/ImageHelper.h"
#include "mxImageViewer.h"
#include "common/stringUtils.h"
#include "mesh/surface/SurfaceUtils.h"
#include "mesh/PDMTransform.h"
#include "common/mxImageUtils.h"


namespace BRIC { namespace IDEA { namespace FISH {

//////////////////////////////////////////////////////////////////////////
// image sub viewer implementation

mxImageSubViewer::mxImageSubViewer()
{
	m_name = tr("");
	m_LeftClicked = false;
	m_middleClicked = false;
	m_rightClicked = false;

	m_leftStart_x = m_leftStart_y = 0;
	m_midStart_x = m_midStart_y = 0;
	m_rightStart_y = 0;

	m_fx = m_fy = m_fz = 0;
}

void mxImageSubViewer::resizeEvent(QResizeEvent* event)
{
	QLabel::resizeEvent(event);

	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());
	if (main_window->m_image == NULL)
	{
		QSize new_size = event->size();
		std::auto_ptr<QImage> empty_state(main_window->get_black_slice(new_size.width(), new_size.height()) );
		setPixmap(QPixmap::fromImage(*empty_state));
	}
	else
	{
		QSize new_size = event->size();
		QSize old_size = event->oldSize();

		if (m_name == tr("tra"))
		{
			main_window->m_tra_sp.first = main_window->m_tra_sp.first + old_size.width() / 2 - new_size.width() / 2;
			main_window->m_tra_sp.second = main_window->m_tra_sp.second + old_size.height() / 2 - new_size.height() / 2;

			std::auto_ptr<QImage> tra_slice( main_window->get_tra_slice(main_window->m_tra_sp.first, main_window->m_tra_sp.second, main_window->m_scale_ratio, new_size) );
			main_window->draw_tra_cursor(*tra_slice);
			setPixmap(QPixmap::fromImage(*tra_slice));
		}
		else if (m_name == tr("sag"))
		{
			main_window->m_sag_sp.first = main_window->m_sag_sp.first + old_size.width() / 2 - new_size.width() / 2;
			main_window->m_sag_sp.second = main_window->m_sag_sp.second + old_size.height() / 2 - new_size.height() / 2;

			std::auto_ptr<QImage> sag_slice(main_window->get_sag_slice(main_window->m_sag_sp.first, main_window->m_sag_sp.second, main_window->m_scale_ratio, new_size));
			main_window->draw_sag_cursor(*sag_slice);
			setPixmap(QPixmap::fromImage(*sag_slice));
		}
		else if (m_name == tr("cor"))
		{
			main_window->m_cor_sp.first = main_window->m_cor_sp.first + old_size.width() / 2 - new_size.width() / 2;
			main_window->m_cor_sp.second = main_window->m_cor_sp.second + old_size.height() / 2 - new_size.height() / 2;

			std::auto_ptr<QImage> cor_slice(main_window->get_cor_slice(main_window->m_cor_sp.first, main_window->m_cor_sp.second, main_window->m_scale_ratio, new_size));
			main_window->draw_cor_cursor(*cor_slice);
			setPixmap(QPixmap::fromImage(*cor_slice));
		}
		else
		{
			QMessageBox::critical(this, tr("Internal Error"), tr("unrecognized sub-viewer type"));
			exit(-1);
		}
	}
}

void mxImageSubViewer::update_cursor_and_views(int x, int y)
{
	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());
	vect3<unsigned int> size = main_window->m_image->GetImageSize();

	if (m_name == tr("tra"))
	{
		vect3<int> voxel = main_window->tra_to_voxel(x, y, width(), height(), main_window->m_cursor_z);
		voxel[0] = voxel[0] < 0 ? 0 : (voxel[0] >= size[0] ? size[0] - 1 : voxel[0]);
		voxel[1] = voxel[1] < 0 ? 0 : (voxel[1] >= size[1] ? size[1] - 1 : voxel[1]);

		main_window->m_cursor_x = voxel[0];
		main_window->m_cursor_y = voxel[1];

		vect3<double> world;
		mxImageUtils::Voxel2World(*(main_window->m_image), voxel, world);

		main_window->m_tool_panel->set_cursor(voxel, world);
		main_window->update_sub_views();
	}
	else if (m_name == tr("sag"))
	{
		vect3<int> voxel = main_window->sag_to_voxel(x, y, width(), height(), main_window->m_cursor_x);
		voxel[1] = voxel[1] < 0 ? 0 : (voxel[1] >= size[1] ? size[1] - 1 : voxel[1]);
		voxel[2] = voxel[2] < 0 ? 0 : (voxel[2] >= size[2] ? size[2] - 1 : voxel[2]);

		main_window->m_cursor_y = voxel[1];
		main_window->m_cursor_z = voxel[2];

		vect3<double> world;
		mxImageUtils::Voxel2World(*(main_window->m_image), voxel, world);

		main_window->m_tool_panel->set_cursor(voxel, world);
		main_window->update_sub_views();
	}
	else if (m_name == tr("cor"))
	{
		vect3<int> voxel = main_window->cor_to_voxel(x, y, width(), height(), main_window->m_cursor_y);
		voxel[0] = voxel[0] < 0 ? 0 : (voxel[0] >= size[0] ? size[0] - 1 : voxel[0]);
		voxel[2] = voxel[2] < 0 ? 0 : (voxel[2] >= size[2] ? size[2] - 1 : voxel[2]);

		main_window->m_cursor_x = voxel[0];
		main_window->m_cursor_z = voxel[2];

		vect3<double> world;
		mxImageUtils::Voxel2World(*(main_window->m_image), voxel, world);

		main_window->m_tool_panel->set_cursor(voxel, world);
		main_window->update_sub_views();
	}
	else
	{
		QMessageBox::critical(this, tr("Internal Error"), tr("unrecognized sub-viewer type"));
		exit(-1);
	}
}

void mxImageSubViewer::mousePressEvent(QMouseEvent* event)
{
	QLabel::mousePressEvent(event);

	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());
	if (main_window->m_image == NULL)
	{
		return;
	}
	else if (event->button() == Qt::LeftButton)
	{
		m_LeftClicked = true;
		m_leftStart_x = event->x();
		m_leftStart_y = event->y();

		int deform_state = main_window->m_tool_panel->m_deformState;
		if (deform_state == 0)
			update_cursor_and_views(event->x(), event->y());
		else if (deform_state == 1)
		{

		}
		else if (deform_state == 2 || deform_state == 3)
		{
			if (m_name == tr("tra"))
			{
				vect3<int> voxel = main_window->tra_to_voxel(m_leftStart_x, m_leftStart_y, main_window->m_tra_view->width(), main_window->m_tra_view->height(), main_window->m_cursor_z);
				vect3<double> force_pt;
				mxImageUtils::Voxel2World(*(main_window->m_image), voxel, force_pt);

				m_fx = force_pt[0];  m_fy = force_pt[1];  m_fz = force_pt[2];
			}
			else if (m_name == tr("sag"))
			{
				vect3<int> voxel = main_window->sag_to_voxel(m_leftStart_x, m_leftStart_y, main_window->m_sag_view->width(), main_window->m_sag_view->height(), main_window->m_cursor_x);
				vect3<double> force_pt;
				mxImageUtils::Voxel2World(*(main_window->m_image), voxel, force_pt);

				m_fx = force_pt[0];	 m_fy = force_pt[1];  m_fz = force_pt[2];
			}
			else if (m_name == tr("cor"))
			{
				vect3<int> voxel = main_window->cor_to_voxel(m_leftStart_x, m_leftStart_y, main_window->m_cor_view->width(), main_window->m_cor_view->height(), main_window->m_cursor_y);
				vect3<double> force_pt;
				mxImageUtils::Voxel2World(*(main_window->m_image), voxel, force_pt);

				m_fx = force_pt[0];	 m_fy = force_pt[1];  m_fz = force_pt[2];
			}
			else {
				QMessageBox::critical(this, tr("Internal Errors"), tr("unrecognized sub view type")); exit(-1);
			}
		}
		else {
			QMessageBox::critical(this, tr("Internal Errors"), tr("unrecognized deform state")); exit(-1);
		}
	}
	else if (event->button() == Qt::MiddleButton)
	{
		m_middleClicked = true;
		m_midStart_x = event->x();
		m_midStart_y = event->y();
	}
	else if (event->button() == Qt::RightButton)
	{
		m_rightClicked = true;
		m_rightStart_y = event->y();

		m_track_tra.first = main_window->m_tra_sp.first;
		m_track_tra.second = main_window->m_tra_sp.second;

		m_track_sag.first = main_window->m_sag_sp.first;
		m_track_sag.second = main_window->m_sag_sp.second;

		m_track_cor.first = main_window->m_cor_sp.first;
		m_track_cor.second = main_window->m_cor_sp.second;
	}
}

void mxImageSubViewer::mouseReleaseEvent(QMouseEvent* event)
{
	QLabel::mouseReleaseEvent(event);

	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());
	if (main_window->m_image == NULL)
	{
		return;
	}
	else if (event->button() == Qt::LeftButton)
	{
		int current_x = event->x();
		int current_y = event->y();

		int deform_state = main_window->m_tool_panel->m_deformState;
		if (deform_state == 1 || deform_state == 2 || deform_state == 3)
			update_surface(m_leftStart_x, m_leftStart_y, current_x, current_y);

		m_LeftClicked = false;
		m_leftStart_x = m_leftStart_y = 0;
	}
	else if (event->button() == Qt::MiddleButton)
	{
		m_middleClicked = false;
		m_midStart_x = m_midStart_y = 0;
	}
	else if (event->button() == Qt::RightButton)
	{
		m_rightClicked = false;
		m_rightStart_y = 0;

		m_track_tra.first = m_track_tra.second = 0;
		m_track_sag.first = m_track_sag.second = 0;
		m_track_cor.first = m_track_cor.second = 0;
	}
}

void mxImageSubViewer::mouseMoveEvent(QMouseEvent *event)
{
	QLabel::mouseMoveEvent(event);

	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());
	if (main_window->m_image == NULL)
	{
		return;
	}
	else if (event->buttons() == Qt::LeftButton && m_LeftClicked)
	{
		if (main_window->m_tool_panel->m_deformState == 0)
			update_cursor_and_views(event->x(), event->y());
	}
	else if (event->buttons() == Qt::MiddleButton && m_middleClicked)
	{
		int current_x = event->x();
		int current_y = event->y();

		if (m_name == tr("tra"))
		{
			main_window->m_tra_sp.first -= (current_x - m_midStart_x);
			main_window->m_tra_sp.second -= (current_y - m_midStart_y);
			main_window->update_sub_views();
		}
		else if (m_name == tr("sag"))
		{
			main_window->m_sag_sp.first -= (current_x - m_midStart_x);
			main_window->m_sag_sp.second -= (current_y - m_midStart_y);
			main_window->update_sub_views();
		}
		else if (m_name == tr("cor"))
		{
			main_window->m_cor_sp.first -= (current_x - m_midStart_x);
			main_window->m_cor_sp.second -= (current_y - m_midStart_y);
			main_window->update_sub_views();
		}

		m_midStart_x = current_x;
		m_midStart_y = current_y;
	}
	else if (event->buttons() == Qt::RightButton && m_rightClicked)
	{
		int current_y = event->y();

		double ratio = 1.0 - (current_y - m_rightStart_y) / main_window->m_scale_sigma;
		if (ratio > 0)
		{
			double old_client_spacing = main_window->get_client_spacing();

			main_window->m_scale_ratio *= ratio;
			if (main_window->m_scale_ratio < main_window->m_min_scale_ratio)
				main_window->m_scale_ratio = main_window->m_min_scale_ratio;
			if (main_window->m_scale_ratio > main_window->m_max_scale_ratio)
				main_window->m_scale_ratio = main_window->m_max_scale_ratio;

			double new_client_spacing = main_window->get_client_spacing();
			QSize tra_size = main_window->m_tra_view->size();
			QSize sag_size = main_window->m_sag_view->size();
			QSize cor_size = main_window->m_cor_view->size();

			m_track_tra.first = (m_track_tra.first + tra_size.width() / 2) * old_client_spacing / new_client_spacing - tra_size.width() / 2;
			m_track_tra.second = (m_track_tra.second + tra_size.height() / 2) * old_client_spacing / new_client_spacing - tra_size.height() / 2;

			main_window->m_tra_sp.first = static_cast<int>(m_track_tra.first + 0.5);
			main_window->m_tra_sp.second = static_cast<int>(m_track_tra.second + 0.5);

			m_track_sag.first = (m_track_sag.first + sag_size.width() / 2) * old_client_spacing / new_client_spacing - sag_size.width() / 2;
			m_track_sag.second = (m_track_sag.second + sag_size.height() / 2) * old_client_spacing / new_client_spacing - sag_size.height() / 2;

			main_window->m_sag_sp.first = static_cast<int>(m_track_sag.first + 0.5);
			main_window->m_sag_sp.second = static_cast<int>(m_track_sag.second + 0.5);

			m_track_cor.first = (m_track_cor.first + cor_size.width() / 2) * old_client_spacing / new_client_spacing - cor_size.width() / 2;
			m_track_cor.second = (m_track_cor.second + cor_size.height() / 2) * old_client_spacing / new_client_spacing - cor_size.height() / 2;

			main_window->m_cor_sp.first = static_cast<int>(m_track_cor.first + 0.5);
			main_window->m_cor_sp.second = static_cast<int>(m_track_cor.second + 0.5);

			main_window->update_sub_views();
		}

		m_rightStart_y = current_y;
	}
}

void mxImageSubViewer::leaveEvent(QEvent* event)
{
	QLabel::leaveEvent(event);

	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());
	if (main_window->m_image == NULL)
	{
		return;
	}
	else
	{
		m_LeftClicked = false;

		m_middleClicked = false;
		m_midStart_x = m_midStart_y = 0;

		m_rightClicked = false;
		m_rightStart_y = 0;
		m_track_tra.first = m_track_tra.second = 0;
		m_track_sag.first = m_track_sag.second = 0;
		m_track_cor.first = m_track_cor.second = 0;
	}
}

void mxImageSubViewer::wheelEvent(QWheelEvent* event)
{
	QLabel::wheelEvent(event);

	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());
	if (main_window->m_image == NULL)
	{
		return;
	}
	else
	{
		QPoint numDegrees = event->angleDelta() / 8;
		if (!numDegrees.isNull())
		{
			QPoint numSteps = numDegrees / 15;
			
			vect3<unsigned int> size = main_window->m_image->GetImageSize();
			if (m_name == tr("tra"))
			{
				main_window->m_cursor_z += numSteps.y();
				if (main_window->m_cursor_z < 0)
					main_window->m_cursor_z = 0;
				if (main_window->m_cursor_z >= size[2])
					main_window->m_cursor_z = size[2] - 1;

				vect3<int> voxel(main_window->m_cursor_x, main_window->m_cursor_y, main_window->m_cursor_z);
				vect3<double> world;
				mxImageUtils::Voxel2World(*(main_window->m_image), voxel, world);

				main_window->m_tool_panel->set_cursor(voxel, world);
				main_window->update_sub_views();
			}
			else if (m_name == tr("sag"))
			{
				main_window->m_cursor_x += numSteps.y();
				if (main_window->m_cursor_x < 0)
					main_window->m_cursor_x = 0;
				if (main_window->m_cursor_x >= size[0])
					main_window->m_cursor_x = size[0] - 1;

				vect3<int> voxel(main_window->m_cursor_x, main_window->m_cursor_y, main_window->m_cursor_z);
				vect3<double> world;
				mxImageUtils::Voxel2World(*(main_window->m_image), voxel, world);

				main_window->m_tool_panel->set_cursor(voxel, world);
				main_window->update_sub_views();
			}
			else if (m_name == tr("cor"))
			{
				main_window->m_cursor_y += numSteps.y();
				if (main_window->m_cursor_y < 0)
					main_window->m_cursor_y = 0;
				if (main_window->m_cursor_y >= size[1])
					main_window->m_cursor_y = size[1] - 1;

				vect3<int> voxel(main_window->m_cursor_x, main_window->m_cursor_y, main_window->m_cursor_z);
				vect3<double> world;
				mxImageUtils::Voxel2World(*(main_window->m_image), voxel, world);

				main_window->m_tool_panel->set_cursor(voxel, world);
				main_window->update_sub_views();
			}
		}
	}

	event->accept();
}

void mxImageSubViewer::update_surface(int startx, int starty, int endx, int endy)
{
	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());
	double client_spacing = main_window->get_client_spacing();

	vect3<float> shift, force_point;
	if (m_name == tr("tra"))
	{
		shift[0] = (endx - startx) * client_spacing;
		shift[1] = (endy - starty) * client_spacing;
		shift[2] = 0;
	}
	else if (m_name == tr("sag"))
	{
		shift[0] = 0;
		shift[1] = (endx - startx) * client_spacing;
		shift[2] = -(endy - starty) * client_spacing;
	}
	else if (m_name == tr("cor"))
	{
		shift[0] = (endx - startx) * client_spacing;
		shift[1] = 0;
		shift[2] = -(endy - starty) * client_spacing;
	}
	else {
		QMessageBox::critical(this, tr("Internal Error"), tr("unrecognized sub view type"));
		exit(-1);
	}

	int deform_state = main_window->m_tool_panel->m_deformState;

	if (deform_state == 1)	// translation
		main_window->m_surf->Translate(shift);
	else if (deform_state == 2 || deform_state == 3)	// 3: global affine, 2: free deform
	{
		vect3<float> force_pt(m_fx, m_fy, m_fz);
		Surface* surf = main_window->m_surf;
		surf->UpdateNeighbors();
		surf->UpdateNormals();
		surf->UpdateFaceNeighbors();

		std::vector<float>& pts = surf->GetPoints();
		std::vector<float> out_pts(pts.size(), 0);
		std::vector< vect3<float> >& normals = surf->GetNormals();
		int vert_num = pts.size() / 3;

		double sigma = main_window->m_tool_panel->m_deformSigma;

		//#pragma omp parallel for
		for (int i = 0; i < vert_num; ++i)
		{
			vect3<float> vert(pts[i * 3], pts[i * 3 + 1], pts[i * 3 + 2]);
			double sq_dist = (force_pt - vert).square_magnitude();
			double weight = exp(- sq_dist / ( 2 * sigma * sigma));

			vect3<float> external_force = shift * weight;

			// vertex center force
			vect3<float> internal_force = surf->GetNeighborCenter(i);
			internal_force = internal_force - vert;
			float dotp = vect3<float>::dot_product(internal_force, normals[i]);
			internal_force = internal_force - normals[i] * dotp;
			
			float dst_pt[3] = { 0 };
			for (int j = 0; j < 3; ++j)
				dst_pt[j] = vert[j] + external_force[j] + internal_force[j] * 0.3;

			if (!surf->VertexCollisonTest(i, dst_pt))
			{
				for (int j = 0; j < 3; ++j)
					out_pts[i * 3 + j] = dst_pt[j];
			}
			else
			{
				for (int j = 0; j < 3; ++j)
					out_pts[i * 3 + j] = vert[j];
			}
		}

		// use global affine to approximate the deformation
		if (deform_state == 3) {
			Surface surf_before = *surf;
			pts = out_pts;

			PDMAffineTransform transform;
			transform(surf_before, *surf, *surf);
		}
		else if (deform_state == 2) { // free deformation

			pts = out_pts;

			// smooth
			int smooth_iter = 15;
			SurfaceUtils::SmoothSurface(*surf, smooth_iter);

			// remesh
			float stepsize = 0.01;
			int remesh_iter = 100;
			SurfaceUtils::Remesh(*surf, stepsize, remesh_iter);
		}

	}
	else {
		QMessageBox::critical(this, tr("Internal Error"), tr("unrecognized deform state in function update_surface"));
		exit(-1);
	}

	main_window->update_surface();
}

//////////////////////////////////////////////////////////////////////////
// mxImageQuickToolPanel implementations

void mxImageQuickToolPanel::create_cursor_box()
{
	m_cursorBox = new QGroupBox(tr("Cursor"));

	m_voxelLabel = new QLabel(tr("voxel:"));
	m_vxField = new QSpinBox;
	m_vyField = new QSpinBox;
	m_vzField = new QSpinBox;

	m_worldLabel = new QLabel(tr("world:"));
	m_wxField = new QDoubleSpinBox;
	m_wyField = new QDoubleSpinBox;
	m_wzField = new QDoubleSpinBox;

	// event handlers
	m_vxField->setKeyboardTracking(false);
	m_vyField->setKeyboardTracking(false);
	m_vzField->setKeyboardTracking(false);

	m_wxField->setKeyboardTracking(false);
	m_wyField->setKeyboardTracking(false);
	m_wzField->setKeyboardTracking(false);

	connect(m_vxField, SIGNAL(valueChanged(int)), this, SLOT(voxelXChanged(int)));
	connect(m_vyField, SIGNAL(valueChanged(int)), this, SLOT(voxelYChanged(int)));
	connect(m_vzField, SIGNAL(valueChanged(int)), this, SLOT(voxelZChanged(int)));

	connect(m_wxField, SIGNAL(valueChanged(double)), this, SLOT(worldXChanged(double)));
	connect(m_wyField, SIGNAL(valueChanged(double)), this, SLOT(worldYChanged(double)));
	connect(m_wzField, SIGNAL(valueChanged(double)), this, SLOT(worldZChanged(double)));

	QGridLayout* main_layout = new QGridLayout;
	main_layout->addWidget(m_voxelLabel, 0, 0);
	main_layout->addWidget(m_vxField, 0, 1);
	main_layout->addWidget(m_vyField, 0, 2);
	main_layout->addWidget(m_vzField, 0, 3);
	main_layout->addWidget(m_worldLabel, 1, 0);
	main_layout->addWidget(m_wxField, 1, 1);
	main_layout->addWidget(m_wyField, 1, 2);
	main_layout->addWidget(m_wzField, 1, 3);

	m_cursorBox->setLayout(main_layout);
	m_cursorBox->setFixedSize(250, 100);
}

void mxImageQuickToolPanel::create_intensity_box()
{
	m_intensityLabel = new QLabel(tr("value:"));
	m_intensityField = new QLabel(tr("NaN"));
	m_labelLabel = new QLabel(tr("label:"));
	m_labelField = new QLabel(tr("NaN"));

	QGridLayout* main_layout = new QGridLayout;
	main_layout->addWidget(m_intensityLabel, 0, 0);
	main_layout->addWidget(m_intensityField, 0, 1);
	main_layout->addWidget(m_labelLabel, 1, 0);
	main_layout->addWidget(m_labelField, 1, 1);

	m_intensityBox = new QGroupBox(tr("Voxel Info"));
	m_intensityBox->setLayout(main_layout);
	m_intensityBox->setFixedSize(100, 100);
}

void mxImageQuickToolPanel::create_contrast_box()
{
	m_contrastBox = new QGroupBox(tr("Contrast"));
	m_minBar = new QSlider(Qt::Horizontal);
	m_maxBar = new QSlider(Qt::Horizontal);
	m_minField = new QSpinBox;
	m_maxField = new QSpinBox;

	m_minField->setKeyboardTracking(false);
	m_maxField->setKeyboardTracking(false);

	connect(m_minField, SIGNAL(valueChanged(int)), this, SLOT(minFieldChanged(int)));
	connect(m_maxField, SIGNAL(valueChanged(int)), this, SLOT(maxFieldChanged(int)));
	connect(m_minBar, SIGNAL(valueChanged(int)), this, SLOT(minBarChanged(int)));
	connect(m_maxBar, SIGNAL(valueChanged(int)), this, SLOT(maxBarChanged(int)));

	QGridLayout* main_layout = new QGridLayout;
	main_layout->addWidget(m_minField, 0, 0);
	main_layout->addWidget(m_minBar, 0, 1);
	main_layout->addWidget(m_maxField, 1, 0);
	main_layout->addWidget(m_maxBar, 1, 1);

	m_contrastBox->setLayout(main_layout);
	m_contrastBox->setFixedSize(350, 100);
}

void mxImageQuickToolPanel::create_surface_box()
{
	m_surfaceBox = new QGroupBox(tr("Surface"));

	QHBoxLayout* first_row = new QHBoxLayout;
	m_centerSurface = new QPushButton(tr("center surface"));
	m_centerSurface->setFixedWidth(100);
	connect(m_centerSurface, SIGNAL(clicked(bool)), this, SLOT(centerSurfaceButtonClicked(bool)));

	m_showSurface = new QCheckBox(tr("show surface"));
	m_showSurface->setChecked(true);
	connect(m_showSurface, SIGNAL(stateChanged(int)), this, SLOT(surfaceShowStateChanged(int)));

	m_showCursor = new QCheckBox(tr("show cursor"));
	m_showCursor->setChecked(true);
	connect(m_showCursor, SIGNAL(stateChanged(int)), this, SLOT(cursorShowStateChanged(int)));

	first_row->addWidget(m_centerSurface);
	first_row->addWidget(m_showSurface);
	first_row->addWidget(m_showCursor);

	QHBoxLayout* second_row = new QHBoxLayout;
	m_noDeformButton = new QRadioButton(tr("no deform"));
	m_shiftButton = new QRadioButton(tr("shift"));
	m_affineButton = new QRadioButton(tr("affine"));
	m_dragButton = new QRadioButton(tr("deform"));

	m_noDeformButton->setChecked(true);
	connect(m_noDeformButton, SIGNAL(toggled(bool)), this, SLOT(deformStateChanged(bool)));
	connect(m_shiftButton, SIGNAL(toggled(bool)), this, SLOT(deformStateChanged(bool)));
	connect(m_affineButton, SIGNAL(toggled(bool)), this, SLOT(deformStateChanged(bool)));
	connect(m_dragButton, SIGNAL(toggled(bool)), this, SLOT(deformStateChanged(bool)));

	m_sigmaField = new QDoubleSpinBox;
	m_deformSigma = 20;
	m_sigmaField->setValue(m_deformSigma);
	m_sigmaField->setMinimum(0.1);
	m_sigmaField->setMaximum(1000);
	m_sigmaField->setKeyboardTracking(true);

	connect(m_sigmaField, SIGNAL(valueChanged(double)), this, SLOT(sigmaChanged(double)));

	second_row->addWidget(m_noDeformButton);
	second_row->addWidget(m_shiftButton);
	second_row->addWidget(m_affineButton);
	second_row->addWidget(m_dragButton);
	second_row->addWidget(m_sigmaField);

	QVBoxLayout* main_layout = new QVBoxLayout;
	main_layout->addLayout(first_row);
	main_layout->addLayout(second_row);

	m_surfaceBox->setLayout(main_layout);
	m_surfaceBox->setFixedSize(350, 100);

	// initialize data
	m_surfaceShow = true;
	m_cursorShow = true;
	m_deformState = 0;
}

mxImageQuickToolPanel::mxImageQuickToolPanel()
{
	create_surface_box();
	create_cursor_box();
	create_intensity_box();
	create_contrast_box();

	QGridLayout* top_layout = new QGridLayout;
	//top_layout->addWidget(m_surfaceBox, 0, 0, 1, 2);
	top_layout->addWidget(m_cursorBox, 1, 0);
	top_layout->addWidget(m_intensityBox, 1, 1);
	top_layout->addWidget(m_contrastBox, 2, 0, 1, 2);
	setLayout(top_layout);

	setEnabled(false);
}

void mxImageQuickToolPanel::set_image_info(const mxImage<PixelType>& image)
{
	vect3<unsigned int> size = image.GetImageSize();
	
	vect3<double> origin(0, 0, 0), endPt(size[0]-1, size[1]-1, size[2]-1);
	mxImageUtils::Voxel2World(image, origin);
	mxImageUtils::Voxel2World(image, endPt);

	m_vxField->setMinimum(0);
	m_vxField->setMaximum(size[0]-1);

	m_vyField->setMinimum(0);
	m_vyField->setMaximum(size[1]-1);

	m_vzField->setMinimum(0);
	m_vzField->setMaximum(size[2]-1);

	if (origin[0] < endPt[0])
	{
		m_wxField->setMinimum(origin[0]);
		m_wxField->setMaximum(endPt[0]);
	}
	else
	{
		m_wxField->setMinimum(endPt[0]);
		m_wxField->setMaximum(origin[0]);
	}
	m_wxField->setSingleStep(image.GetSpacing()[0]);

	if (origin[1] < endPt[1])
	{
		m_wyField->setMinimum(origin[1]);
		m_wyField->setMaximum(endPt[1]);
	}
	else
	{
		m_wyField->setMinimum(endPt[1]);
		m_wyField->setMaximum(origin[1]);
	}
	m_wyField->setSingleStep(image.GetSpacing()[1]);

	if (origin[2] < endPt[2])
	{
		m_wzField->setMinimum(origin[2]);
		m_wzField->setMaximum(endPt[2]);
	}
	else
	{
		m_wzField->setMinimum(endPt[2]);
		m_wzField->setMaximum(origin[2]);
	}
	m_wzField->setSingleStep(image.GetSpacing()[2]);

}

void mxImageQuickToolPanel::set_cursor(const vect3<int>& voxel, const vect3<double>& world)
{
	mxImageViewer* main_window = static_cast<mxImageViewer*>(this->parent()->parent());
	const mxImage<PixelType>& image = (*(main_window->m_image));

	if (!image.PtInImage(voxel[0], voxel[1], voxel[2])) {
		QMessageBox::critical(this, tr("Internal Error"), tr("voxel outside of image in set_cursor")); exit(-1);
	}

	m_vxField->setValue(voxel[0]);
	m_vyField->setValue(voxel[1]);
	m_vzField->setValue(voxel[2]);

	m_wxField->setValue(world[0]);
	m_wyField->setValue(world[1]);
	m_wzField->setValue(world[2]);

	PixelType value = image(voxel[0], voxel[1], voxel[2]);
	QString value_str(stringUtils::num2str(value).c_str());
	m_intensityField->setText(value_str);

	if (main_window->m_mask != NULL)
	{
		int label = (*(main_window->m_mask))(voxel[0], voxel[1], voxel[2]);
		QString label_str(stringUtils::num2str(value).c_str());
		m_labelField->setText(label_str);
	}
}

void mxImageQuickToolPanel::init_contrast(int min_val, int max_val)
{
	if (max_val <= min_val) {
		QMessageBox::critical(this, tr("Internal Error"), "min >= max in init_contrast function"); exit(-1);
	}

	m_minField->setMinimum(min_val);
	m_minField->setMaximum(max_val - 1);
	m_minField->setValue(min_val);

	m_maxField->setMinimum(min_val + 1);
	m_maxField->setMaximum(max_val);
	m_maxField->setValue(max_val);

	m_minBar->setMinimum(min_val);
	m_minBar->setMaximum(max_val);
	m_minBar->setValue(min_val);

	m_maxBar->setMinimum(min_val);
	m_maxBar->setMaximum(max_val);
	m_maxBar->setValue(max_val);
}

void mxImageQuickToolPanel::set_contrast(int min_val, int max_val)
{	
	if (min_val < m_minField->minimum())
		min_val = m_minField->minimum();
	if (min_val > m_minField->maximum())
		min_val = m_minField->maximum();

	if (max_val < m_maxField->minimum())
		max_val = m_maxField->minimum();
	if (max_val > m_maxField->maximum())
		max_val = m_maxField->maximum();

	m_minField->setValue(min_val);
	m_minField->setMaximum(max_val - 1);

	m_maxField->setValue(max_val);
	m_maxField->setMinimum(min_val + 1);

	m_minBar->setValue(min_val);
	m_maxBar->setValue(max_val);

	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());
	main_window->m_min_intensity = m_minField->value();
	main_window->m_max_intensity = m_maxField->value();

	main_window->update_sub_views();
}

void mxImageQuickToolPanel::voxelXChanged(int val)
{
	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());

	vect3<int> voxel;
	voxel[0] = val; voxel[1] = main_window->m_cursor_y; voxel[2] = main_window->m_cursor_z;

	vect3<unsigned int> size = main_window->m_image->GetImageSize();
	for (int i = 0; i < 3; ++i)
		voxel[i] = voxel[i] < 0 ? 0 : (voxel[i] >= size[i] ? size[i] - 1 : voxel[i]);

	vect3<double> world;
	mxImageUtils::Voxel2World(*(main_window->m_image), voxel, world);

	main_window->m_cursor_x = voxel[0];
	main_window->m_cursor_y = voxel[1];
	main_window->m_cursor_z = voxel[2];

	set_cursor(voxel, world);
	main_window->update_sub_views();
}

void mxImageQuickToolPanel::voxelYChanged(int val)
{
	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());

	vect3<int> voxel;
	voxel[0] = main_window->m_cursor_x; voxel[1] = val; voxel[2] = main_window->m_cursor_z;

	vect3<unsigned int> size = main_window->m_image->GetImageSize();
	for (int i = 0; i < 3; ++i)
		voxel[i] = voxel[i] < 0 ? 0 : (voxel[i] >= size[i] ? size[i] - 1 : voxel[i]);

	vect3<double> world;
	mxImageUtils::Voxel2World(*(main_window->m_image), voxel, world);

	main_window->m_cursor_x = voxel[0];
	main_window->m_cursor_y = voxel[1];
	main_window->m_cursor_z = voxel[2];

	set_cursor(voxel, world);
	main_window->update_sub_views();
}

void mxImageQuickToolPanel::voxelZChanged(int val)
{
	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());

	vect3<int> voxel;
	voxel[0] = main_window->m_cursor_x; voxel[1] = main_window->m_cursor_y; voxel[2] = val;

	vect3<unsigned int> size = main_window->m_image->GetImageSize();
	for (int i = 0; i < 3; ++i)
		voxel[i] = voxel[i] < 0 ? 0 : (voxel[i] >= size[i] ? size[i] - 1 : voxel[i]);

	vect3<double> world;
	mxImageUtils::Voxel2World(*(main_window->m_image), voxel, world);

	main_window->m_cursor_x = voxel[0];
	main_window->m_cursor_y = voxel[1];
	main_window->m_cursor_z = voxel[2];

	set_cursor(voxel, world);
	main_window->update_sub_views();
}

void mxImageQuickToolPanel::worldXChanged(double val)
{
	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());

	vect3<int> voxel;
	voxel[0] = main_window->m_cursor_x; voxel[1] = main_window->m_cursor_y; voxel[2] = main_window->m_cursor_z;

	vect3<double> world;
	mxImageUtils::Voxel2World(*(main_window->m_image), voxel, world);

	world[0] = val;
	mxImageUtils::World2Voxel(*(main_window->m_image), world, voxel);

	vect3<unsigned int> size = main_window->m_image->GetImageSize();
	for (int i = 0; i < 3; ++i)
		voxel[i] = voxel[i] < 0 ? 0 : (voxel[i] >= size[i] ? size[i] - 1 : voxel[i]);

	mxImageUtils::Voxel2World(*(main_window->m_image), voxel, world);

	main_window->m_cursor_x = voxel[0];
	main_window->m_cursor_y = voxel[1];
	main_window->m_cursor_z = voxel[2];

	set_cursor(voxel, world);
	main_window->update_sub_views();
}

void mxImageQuickToolPanel::worldYChanged(double val)
{
	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());

	vect3<int> voxel;
	voxel[0] = main_window->m_cursor_x; voxel[1] = main_window->m_cursor_y; voxel[2] = main_window->m_cursor_z;

	vect3<double> world;
	mxImageUtils::Voxel2World(*(main_window->m_image), voxel, world);

	world[1] = val;
	mxImageUtils::World2Voxel(*(main_window->m_image), world, voxel);

	vect3<unsigned int> size = main_window->m_image->GetImageSize();
	for (int i = 0; i < 3; ++i)
		voxel[i] = voxel[i] < 0 ? 0 : (voxel[i] >= size[i] ? size[i] - 1 : voxel[i]);

	mxImageUtils::Voxel2World(*(main_window->m_image), voxel, world);

	main_window->m_cursor_x = voxel[0];
	main_window->m_cursor_y = voxel[1];
	main_window->m_cursor_z = voxel[2];

	set_cursor(voxel, world);
	main_window->update_sub_views();
}

void mxImageQuickToolPanel::worldZChanged(double val)
{
	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());

	vect3<int> voxel;
	voxel[0] = main_window->m_cursor_x; voxel[1] = main_window->m_cursor_y; voxel[2] = main_window->m_cursor_z;

	vect3<double> world;
	mxImageUtils::Voxel2World(*(main_window->m_image), voxel, world);

	world[2] = val;
	mxImageUtils::World2Voxel(*(main_window->m_image), world, voxel);

	vect3<unsigned int> size = main_window->m_image->GetImageSize();
	for (int i = 0; i < 3; ++i)
		voxel[i] = voxel[i] < 0 ? 0 : (voxel[i] >= size[i] ? size[i] - 1 : voxel[i]);

	mxImageUtils::Voxel2World(*(main_window->m_image), voxel, world);

	main_window->m_cursor_x = voxel[0];
	main_window->m_cursor_y = voxel[1];
	main_window->m_cursor_z = voxel[2];

	set_cursor(voxel, world);
	main_window->update_sub_views();
}

void mxImageQuickToolPanel::minFieldChanged(int val)
{
	set_contrast(val, m_maxField->value());
}

void mxImageQuickToolPanel::maxFieldChanged(int val)
{
	set_contrast(m_minField->value(), val);
}

void mxImageQuickToolPanel::minBarChanged(int val)
{
	set_contrast(val, m_maxField->value());
}

void mxImageQuickToolPanel::maxBarChanged(int val)
{
	set_contrast(m_minField->value(), val);
}

void mxImageQuickToolPanel::surfaceShowStateChanged(int state)
{
	if (state == Qt::Checked)
		m_surfaceShow = true;
	else
		m_surfaceShow = false;

	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());
	main_window->update_sub_views();
}

void mxImageQuickToolPanel::cursorShowStateChanged(int state)
{
	if (state == Qt::Checked)
		m_cursorShow = true;
	else
		m_cursorShow = false;

	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());
	main_window->update_sub_views();
}

void mxImageQuickToolPanel::centerSurfaceButtonClicked(bool bClicked)
{
	mxImageViewer* main_window = static_cast<mxImageViewer*>(parent()->parent());
	
	vect3<double> center = mxImageUtils::WorldCenter(*(main_window->m_image));
	main_window->m_surf->MoveTo(center.to<float>());

	main_window->setup_surface();
}

void mxImageQuickToolPanel::deformStateChanged(bool clicked)
{
	if (m_noDeformButton->isChecked())
	{
		m_deformState = 0;
		m_sigmaField->setEnabled(false);
	}
	else if (m_shiftButton->isChecked())
	{
		m_deformState = 1;
		m_sigmaField->setEnabled(false);
	}
	else if (m_affineButton->isChecked())
	{
		m_deformState = 3;
		m_sigmaField->setEnabled(true);
	}
	else if (m_dragButton->isChecked())
	{
		m_deformState = 2;
		m_sigmaField->setEnabled(true);
	}
}

void mxImageQuickToolPanel::sigmaChanged(double value)
{
	m_deformSigma = value;
}

//////////////////////////////////////////////////////////////////////////
// mxImage Viewer implementations

/************************************************************************/
/* Construction of mxImageViewer                                        */
/************************************************************************/

void mxImageViewer::initialize_data()
{
	m_image = NULL;
	m_mask = NULL;
	m_surf = NULL;

	m_tra_contours = m_sag_contours = m_cor_contours = NULL;

	m_last_open_image_path = tr("");
	m_last_open_surface_path = tr("");
	
	m_display_spacing = 1;

	m_scale_ratio = 1;
	m_scale_sigma = 200;
	m_min_scale_ratio = 0.2;
	m_max_scale_ratio = 20;

	m_cursor_x = m_cursor_y = m_cursor_z = 0;

	m_tra_sp.first = m_tra_sp.second = 0;
	m_sag_sp.first = m_sag_sp.second = 0;
	m_cor_sp.first = m_cor_sp.second = 0;

	m_min_intensity = 0;
	m_max_intensity = 2000;

	m_background_color = qRgb(0,0,0);
	m_cursor_color = qRgb(0, 255, 0);
	m_contour_color = qRgb(255, 0, 0);

	m_cursor_radius = -1;
}

mxImageViewer::mxImageViewer()
{
	const int min_width = 350, min_height = 350;

	std::auto_ptr<QImage> empty_state(get_black_slice(min_width, min_height));

	m_tra_view = new mxImageSubViewer;
	m_tra_view->setBackgroundRole(QPalette::Base);
	m_tra_view->set_name(tr("tra"));
	m_tra_view->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	m_tra_view->setPixmap(QPixmap::fromImage(*empty_state));

	m_sag_view = new mxImageSubViewer;
	m_sag_view->setBackgroundRole(QPalette::Base);
	m_sag_view->set_name(tr("sag"));
	m_sag_view->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	m_sag_view->setPixmap(QPixmap::fromImage(*empty_state));

	m_cor_view = new mxImageSubViewer;
	m_cor_view->setBackgroundRole(QPalette::Base);
	m_cor_view->set_name(tr("cor"));
	m_cor_view->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	m_cor_view->setPixmap(QPixmap::fromImage(*empty_state));

	m_tool_panel = new mxImageQuickToolPanel;

	m_layout = new QGridLayout;

	m_layout->addWidget(m_tra_view, 0, 0);
	m_layout->addWidget(m_sag_view, 0, 1);
	m_layout->addWidget(m_cor_view, 1, 1);
	m_layout->addWidget(m_tool_panel, 1, 0);
	
	for (int i = 0; i < 2; ++i)
	{
		m_layout->setColumnMinimumWidth(i, min_width);
		m_layout->setRowMinimumHeight(i, min_height);
		m_layout->setColumnStretch(i, 1);
		m_layout->setRowStretch(i, 1);
	}
	
	// set central widget and layout
	QWidget* window = new QWidget;
	window->setBackgroundRole(QPalette::Base);
	window->setLayout(m_layout);
	setCentralWidget(window);

	// create menu
	create_actions();
	create_menu();

	initialize_data();
}

void mxImageViewer::create_actions()
{
	m_openAct = new QAction(tr("&Open..."), this);
	m_openAct->setShortcut(tr("Ctrl+O"));
	connect(m_openAct, SIGNAL(triggered()), this, SLOT(open()));

	m_exitAct = new QAction(tr("E&xit"), this);
	m_exitAct->setShortcut(tr("Ctrl+q"));
	connect(m_exitAct, SIGNAL(triggered()), this, SLOT(close()));

	m_openSurfaceAct = new QAction(tr("&Load Surface..."), this);
	connect(m_openSurfaceAct, SIGNAL(triggered()), this, SLOT(openSurface()));

	m_saveSurfaceAct = new QAction(tr("&Save Surface..."), this);
	connect(m_saveSurfaceAct, SIGNAL(triggered()), this, SLOT(saveSurface()));
}

void mxImageViewer::create_menu()
{
	m_file_menu = new QMenu(tr("&File"), this);
	m_file_menu->addAction(m_openAct);
	m_file_menu->addSeparator();
	m_file_menu->addAction(m_exitAct);

	m_surface_menu = new QMenu(tr("&Surface"), this);
	m_surface_menu->addAction(m_openSurfaceAct);
	m_surface_menu->addAction(m_saveSurfaceAct);
		
	menuBar()->addMenu(m_file_menu);
//	menuBar()->addMenu(m_surface_menu);

	m_surface_menu->setEnabled(false);
}

//////////////////////////////////////////////////////////////////////////
// coordinate transform

double mxImageViewer::get_client_spacing() const
{
	return m_display_spacing / m_scale_ratio;
}

vect3<int> mxImageViewer::tra_to_voxel(int x, int y, int width, int height, int slice_idx)
{
	vect3<double> spacing = m_image->GetSpacing();
	double client_spacing = get_client_spacing();

	double world_x = (m_tra_sp.first + x) * client_spacing;
	double world_y = (m_tra_sp.second + y) * client_spacing;

	vect3<int> voxel;
	voxel[0] = static_cast<int>(world_x / spacing[0] + 0.5);
	voxel[1] = static_cast<int>(world_y / spacing[1] + 0.5);
	voxel[2] = slice_idx;

	return voxel;
}

vect3<int> mxImageViewer::sag_to_voxel(int x, int y, int width, int height, int slice_idx)
{
	vect3<double> spacing = m_image->GetSpacing();
	double client_spacing = get_client_spacing();
	vect3<unsigned int> size = m_image->GetImageSize();
	double depth = size[2] * spacing[2];

	double world_x = (m_sag_sp.first + x) * client_spacing;
	double world_y = depth - (m_sag_sp.second + y) * client_spacing;

	vect3<int> voxel;
	voxel[0] = slice_idx;
	voxel[1] = static_cast<int>(world_x / spacing[1] + 0.5);
	voxel[2] = static_cast<int>(world_y / spacing[2] + 0.5);

	return voxel;
}

vect3<int> mxImageViewer::cor_to_voxel(int x, int y, int width, int height, int slice_idx)
{
	vect3<double> spacing = m_image->GetSpacing();
	double client_spacing = get_client_spacing();
	vect3<unsigned int> size = m_image->GetImageSize();
	double depth = size[2] * spacing[2];

	double world_x = (m_cor_sp.first + x) * client_spacing;
	double world_y = depth - (m_cor_sp.second + y) * client_spacing;

	vect3<int> voxel;
	voxel[0] = static_cast<int>(world_x / spacing[0] + 0.5);
	voxel[1] = slice_idx;
	voxel[2] = static_cast<int>(world_y / spacing[2] + 0.5);

	return voxel;
}

std::pair<int,int> mxImageViewer::voxel_to_tra(int x, int y, int z)
{
	vect3<double> spacing = m_image->GetSpacing();
	double client_spacing = get_client_spacing();

	double world_x = x * spacing[0];
	double world_y = y * spacing[1];

	std::pair<int, int> ret;
	ret.first = static_cast<int>(world_x / client_spacing - m_tra_sp.first + 0.5);
	ret.second = static_cast<int>(world_y / client_spacing - m_tra_sp.second + 0.5);

	return ret;
}

std::pair<int,int> mxImageViewer::voxel_to_sag(int x, int y, int z)
{
	vect3<double> spacing = m_image->GetSpacing();
	vect3<unsigned int> size = m_image->GetImageSize();
	double depth = size[2] * spacing[2];

	double client_spacing = get_client_spacing();

	double world_x = y * spacing[0];
	double world_y = depth - z * spacing[2];

	std::pair<int, int> ret;
	ret.first = static_cast<int>(world_x / client_spacing - m_sag_sp.first + 0.5);
	ret.second = static_cast<int>(world_y / client_spacing - m_sag_sp.second + 0.5);

	return ret;
}

std::pair<int,int> mxImageViewer::voxel_to_cor(int x, int y, int z)
{
	vect3<double> spacing = m_image->GetSpacing();
	vect3<unsigned int> size = m_image->GetImageSize();
	double depth = size[2] * spacing[2];

	double client_spacing = get_client_spacing();

	double world_x = x * spacing[0];
	double world_y = depth - z * spacing[2];

	std::pair<int, int> ret;
	ret.first = static_cast<int>(world_x / client_spacing - m_cor_sp.first + 0.5);
	ret.second = static_cast<int>(world_y / client_spacing - m_cor_sp.second + 0.5);

	return ret;
}

/************************************************************************/
/* draw cursor                                                          */
/************************************************************************/

void mxImageViewer::draw_tra_cursor(QImage& image)
{
	std::pair<int, int> cursor_pos = voxel_to_tra(m_cursor_x, m_cursor_y, m_cursor_z);
	QSize size = m_tra_view->size();

	for (int x = 0; x < cursor_pos.first - m_cursor_radius; ++x)
		image.setPixel(x, cursor_pos.second, m_cursor_color);

	for (int x = cursor_pos.first + m_cursor_radius + 1; x < size.width(); ++x)
		image.setPixel(x, cursor_pos.second, m_cursor_color);

	for (int y = 0; y < cursor_pos.second - m_cursor_radius; ++y)
		image.setPixel(cursor_pos.first, y, m_cursor_color);

	for (int y = cursor_pos.second + m_cursor_radius + 1; y < size.height(); ++y)
		image.setPixel(cursor_pos.first, y, m_cursor_color);
}

void mxImageViewer::draw_sag_cursor(QImage& image)
{
	std::pair<int, int> cursor_pos = voxel_to_sag(m_cursor_x, m_cursor_y, m_cursor_z);
	QSize size = m_sag_view->size();

	for (int x = 0; x < cursor_pos.first - m_cursor_radius; ++x)
		image.setPixel(x, cursor_pos.second, m_cursor_color);

	for (int x = cursor_pos.first + m_cursor_radius + 1; x < size.width(); ++x)
		image.setPixel(x, cursor_pos.second, m_cursor_color);

	for (int y = 0; y < cursor_pos.second - m_cursor_radius; ++y)
		image.setPixel(cursor_pos.first, y, m_cursor_color);

	for (int y = cursor_pos.second + m_cursor_radius + 1; y < size.height(); ++y)
		image.setPixel(cursor_pos.first, y, m_cursor_color);
}

void mxImageViewer::draw_cor_cursor(QImage& image)
{
	std::pair<int, int> cursor_pos = voxel_to_cor(m_cursor_x, m_cursor_y, m_cursor_z);
	QSize size = m_cor_view->size();

	for (int x = 0; x < cursor_pos.first - m_cursor_radius; ++x)
		image.setPixel(x, cursor_pos.second, m_cursor_color);

	for (int x = cursor_pos.first + m_cursor_radius + 1; x < size.width(); ++x)
		image.setPixel(x, cursor_pos.second, m_cursor_color);

	for (int y = 0; y < cursor_pos.second - m_cursor_radius; ++y)
		image.setPixel(cursor_pos.first, y, m_cursor_color);

	for (int y = cursor_pos.second + m_cursor_radius + 1; y < size.height(); ++y)
		image.setPixel(cursor_pos.first, y, m_cursor_color);
}

void mxImageViewer::update_sub_views()
{
	std::auto_ptr<QImage> tra_slice (get_tra_slice(m_tra_sp.first, m_tra_sp.second, m_scale_ratio, m_tra_view->size()));
	if (m_tool_panel->m_cursorShow)
		draw_tra_cursor(*tra_slice);
	m_tra_view->setPixmap(QPixmap::fromImage(*tra_slice));

	std::auto_ptr<QImage> sag_slice(get_sag_slice(m_sag_sp.first, m_sag_sp.second, m_scale_ratio, m_sag_view->size()));
	if (m_tool_panel->m_cursorShow)
		draw_sag_cursor(*sag_slice);
	m_sag_view->setPixmap(QPixmap::fromImage(*sag_slice));

	std::auto_ptr<QImage> cor_slice(get_cor_slice(m_cor_sp.first, m_cor_sp.second, m_scale_ratio, m_cor_view->size()));
	if (m_tool_panel->m_cursorShow)
		draw_cor_cursor(*cor_slice);
	m_cor_view->setPixmap(QPixmap::fromImage(*cor_slice));
}

/************************************************************************/
/* Intensity Transform                                                  */
/************************************************************************/

QRgb mxImageViewer::intensity_to_rgb(PixelType val)
{
	int pvalue2 = static_cast<int>((val - m_min_intensity) * 1.0 / (m_max_intensity - m_min_intensity) * 255 + 0.5);
	if (pvalue2 < 0)
		pvalue2 = 0;
	if (pvalue2 > 255)
		pvalue2 = 255;
	return qRgb(pvalue2, pvalue2, pvalue2);
}

/************************************************************************/
/* Compute Display Slices                                               */
/************************************************************************/

QImage* mxImageViewer::get_black_slice(int width, int height)
{
	QImage* empty_img = new QImage(width, height, QImage::Format_RGB32);

	QRgb val = qRgb(0, 0, 0);

	#pragma omp parallel for
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			empty_img->setPixel(x, y, val);
		}
	}

	return empty_img;
}

QImage* mxImageViewer::get_cor_slice(int sp_x, int sp_y, double scale_ratio, const QSize& size)
{
	vect3<unsigned int> volume_size = m_image->GetImageSize();

	if (m_cursor_y < 0 || m_cursor_y >= volume_size[1])
	{
		QMessageBox::critical(this, tr("Internal Error"), tr("slice index out of bound in function get_cor_slice"));
		exit(-1);
	}

	QImage* image = new QImage(size.width(), size.height(), QImage::Format_RGB32);

	if (m_surf == NULL || !m_tool_panel->m_surfaceShow)
	{
		#pragma omp parallel for
		for (int y = 0; y < size.height(); ++y)
		{
			for (int x = 0; x < size.width(); ++x)
			{
				vect3<int> voxel = cor_to_voxel(x, y, size.width(), size.height(), m_cursor_y);

				if (voxel[0] < 0 || voxel[0] >= volume_size[0] || voxel[2] < 0 || voxel[2] >= volume_size[2])
				{
					image->setPixel(x, y, m_background_color);
				}
				else
				{
					PixelType pvalue = (*m_image)(voxel[0], voxel[1], voxel[2]);
					image->setPixel(x, y, intensity_to_rgb(pvalue));
				}
			}
		}
	}
	else
	{
		#pragma omp parallel for
		for (int y = 0; y < size.height(); ++y)
		{
			for (int x = 0; x < size.width(); ++x)
			{
				vect3<int> voxel = cor_to_voxel(x, y, size.width(), size.height(), m_cursor_y);

				if (voxel[0] < 0 || voxel[0] >= volume_size[0] || voxel[2] < 0 || voxel[2] >= volume_size[2])
				{
					image->setPixel(x, y, m_background_color);
				}
				else
				{
					if ((*m_cor_contours)(voxel[0], voxel[1], voxel[2]) > 0)
					{
						image->setPixel(x, y, m_contour_color);
					}
					else
					{
						PixelType pvalue = (*m_image)(voxel[0], voxel[1], voxel[2]);
						image->setPixel(x, y, intensity_to_rgb(pvalue));
					}
				}
			}
		}
	}

	return image;
}

QImage* mxImageViewer::get_sag_slice(int sp_x, int sp_y, double scale_ratio, const QSize& size)
{
	vect3<unsigned int> volume_size = m_image->GetImageSize();

	if (m_cursor_x < 0 || m_cursor_x >= volume_size[0])
	{
		QMessageBox::critical(this, tr("Internal Error"), tr("slice index out of bound in function get_sag_slice"));
		exit(-1);
	}

	QImage* image = new QImage(size.width(), size.height(), QImage::Format_RGB32);

	if (m_surf == NULL || !m_tool_panel->m_surfaceShow)
	{
		#pragma omp parallel for
		for (int y = 0; y < size.height(); ++y)
		{
			for (int x = 0; x < size.width(); ++x)
			{
				vect3<int> voxel = sag_to_voxel(x, y, size.width(), size.height(), m_cursor_x);

				if (voxel[1] < 0 || voxel[1] >= volume_size[1] || voxel[2] < 0 || voxel[2] >= volume_size[2])
				{
					image->setPixel(x, y, m_background_color);
				}
				else
				{
					PixelType pvalue = (*m_image)(voxel[0], voxel[1], voxel[2]);
					image->setPixel(x, y, intensity_to_rgb(pvalue));
				}
			}
		}
	}
	else {
		#pragma omp parallel for
		for (int y = 0; y < size.height(); ++y)
		{
			for (int x = 0; x < size.width(); ++x)
			{
				vect3<int> voxel = sag_to_voxel(x, y, size.width(), size.height(), m_cursor_x);

				if (voxel[1] < 0 || voxel[1] >= volume_size[1] || voxel[2] < 0 || voxel[2] >= volume_size[2])
				{
					image->setPixel(x, y, m_background_color);
				}
				else
				{
					if ((*m_sag_contours)(voxel[0], voxel[1], voxel[2]) > 0)
					{
						image->setPixel(x, y, m_contour_color);
					}
					else
					{
						PixelType pvalue = (*m_image)(voxel[0], voxel[1], voxel[2]);
						image->setPixel(x, y, intensity_to_rgb(pvalue));
					}
				}
			}
		}
	}

	return image;
}

QImage* mxImageViewer::get_tra_slice(int sp_x, int sp_y, double scale_ratio, const QSize& size)
{
	vect3<unsigned int> volume_size = m_image->GetImageSize();

	if (m_cursor_z < 0 || m_cursor_z >= volume_size[2])
	{
		QMessageBox::critical(this, tr("Internal Error"), tr("slice index out of bound in function get_tra_slice"));
		exit(-1);
	}

	QImage* image = new QImage(size.width(), size.height(), QImage::Format_RGB32);

	if (m_surf == NULL || !m_tool_panel->m_surfaceShow)
	{
		#pragma omp parallel for
		for (int y = 0; y < size.height(); ++y)
		{
			for (int x = 0; x < size.width(); ++x)
			{
				vect3<int> voxel = tra_to_voxel(x, y, size.width(), size.height(), m_cursor_z);

				if (voxel[0] < 0 || voxel[0] >= volume_size[0] || voxel[1] < 0 || voxel[1] >= volume_size[1])
				{
					image->setPixel(x, y, m_background_color);
				}
				else
				{
					PixelType pvalue = (*m_image)(voxel[0], voxel[1], voxel[2]);
					image->setPixel(x, y, intensity_to_rgb(pvalue));
				}
			}
		}
	}
	else
	{
	#pragma omp parallel for
		for (int y = 0; y < size.height(); ++y)
		{
			for (int x = 0; x < size.width(); ++x)
			{
				vect3<int> voxel = tra_to_voxel(x, y, size.width(), size.height(), m_cursor_z);

				if (voxel[0] < 0 || voxel[0] >= volume_size[0] || voxel[1] < 0 || voxel[1] >= volume_size[1])
				{
					image->setPixel(x, y, m_background_color);
				}
				else
				{
					if ((*m_tra_contours)(voxel[0], voxel[1], voxel[2]) > 0)
					{
						image->setPixel(x, y, m_contour_color);
					}
					else
					{
						PixelType pvalue = (*m_image)(voxel[0], voxel[1], voxel[2]);
						image->setPixel(x, y, intensity_to_rgb(pvalue));
					}
				}
			}
		}
	}

	return image;
}

void mxImageViewer::get_tra_contour(mxImage<unsigned char>& contour)
{
	mxImage<PixelType> header;
	mxImageUtils::GetTransversalSlice(*m_image, m_cursor_z, header, true);

	if (m_surf == NULL) {
		QMessageBox::critical(this, tr("Internal Error"), tr("surface not loaded yet in get_tra_contour")); exit(-1);
	}

	unsigned char foreground = 255, background = 0;
	mxImage<unsigned char> mask;
	SurfaceUtils::CarveSurface<PixelType, unsigned char>(*m_surf, header, background, foreground, mask);
	mxImageUtils::Find2DContour(mask, contour);
}

void mxImageViewer::get_sag_contour(mxImage<unsigned char>& contour)
{
	mxImage<PixelType> header;
	mxImageUtils::GetSagittalSlice(*m_image, m_cursor_x, header, true);

	if (m_surf == NULL) {
		QMessageBox::critical(this, tr("Internal Error"), tr("surface not loaded yet in get_tra_contour")); exit(-1);
	}

	unsigned char foreground = 255, background = 0;
	mxImage<unsigned char> mask;
	SurfaceUtils::CarveSurface<PixelType, unsigned char>(*m_surf, header, background, foreground, mask);
	mxImageUtils::Find2DContour(mask, contour);
}

void mxImageViewer::get_cor_contour(mxImage<unsigned char>& contour)
{
	mxImage<PixelType> header;
	mxImageUtils::GetCoronalSlice(*m_image, m_cursor_y, header, true);

	if (m_surf == NULL) {
		QMessageBox::critical(this, tr("Internal Error"), tr("surface not loaded yet in get_tra_contour")); exit(-1);
	}

	unsigned char foreground = 255, background = 0;
	mxImage<unsigned char> mask;
	SurfaceUtils::CarveSurface<PixelType, unsigned char>(*m_surf, header, background, foreground, mask);
	mxImageUtils::Find2DContour(mask, contour);
}

/************************************************************************/
/* Setup Initial Image                                                  */
/************************************************************************/

void mxImageViewer::setup_image()
{
	vect3<unsigned int> size = m_image->GetImageSize();

	// enable surface menu
	m_surface_menu->setEnabled(true);

	// setup image size
	m_tool_panel->set_image_info(*m_image);
	m_tool_panel->setEnabled(true);
	m_tool_panel->m_surfaceBox->setEnabled(false);

	// setup display spacing
	vect3<double> spacing = m_image->GetSpacing();
	m_display_spacing = std::min(spacing[0], std::min(spacing[1], spacing[2]));

	// setup cursor
	m_cursor_x = size[0] / 2;
	m_cursor_y = size[1] / 2;
	m_cursor_z = size[2] / 2;

	vect3<int> cursor_voxel(m_cursor_x, m_cursor_y, m_cursor_z);
	vect3<double> cursor_world;
	mxImageUtils::Voxel2World(*m_image, cursor_voxel, cursor_world);
	m_tool_panel->set_cursor(cursor_voxel, cursor_world);

	// setup scale ratio
	QSize tra_size = m_tra_view->size();
	QSize sag_size = m_sag_view->size();
	QSize cor_size = m_cor_view->size();

	vect3<double> world_dim;
	for (int i = 0; i < 3; ++i)
		world_dim[i] = spacing[i] * size[i];

	m_scale_ratio = std::max(world_dim[0] / (tra_size.width() * m_display_spacing), world_dim[1] / (tra_size.height() * m_display_spacing));
	m_scale_ratio = std::max(m_scale_ratio, std::max(world_dim[1] / (sag_size.width() * m_display_spacing), world_dim[2] / (sag_size.height() * m_display_spacing)));
	m_scale_ratio = std::max(m_scale_ratio, std::max(world_dim[0] / (cor_size.width() * m_display_spacing), world_dim[2] / (cor_size.height() * m_display_spacing)));
	m_scale_ratio = 1 / m_scale_ratio;

	// positions of sub-views
	double client_spacing = get_client_spacing();
	vect3<int> display_req_size;
	for (int i = 0; i < 3; ++i)
		display_req_size[i] = static_cast<int>(size[i] * spacing[i] / client_spacing + 0.5);

	m_tra_sp.first = -(tra_size.width() - display_req_size[0]) / 2;
	m_tra_sp.second = -(tra_size.height() - display_req_size[1]) / 2;

	m_sag_sp.first = -(sag_size.width() - display_req_size[1]) / 2;
	m_sag_sp.second = -(tra_size.height() - display_req_size[2]) / 2;

	m_cor_sp.first = -(cor_size.width() - display_req_size[0]) / 2;
	m_cor_sp.second = -(cor_size.height() - display_req_size[2]) / 2;

	// setup intensity range
	m_min_intensity = mxImageUtils::MinIntensity(*m_image);
	m_max_intensity = mxImageUtils::MaxIntensity(*m_image);
	m_tool_panel->init_contrast(m_min_intensity, m_max_intensity);

	// setup different sub-views
	std::auto_ptr<QImage> tra_image (get_tra_slice(m_tra_sp.first, m_tra_sp.second, m_scale_ratio, m_tra_view->size()));
	draw_tra_cursor(*tra_image);
	m_tra_view->setPixmap(QPixmap::fromImage(*tra_image));

	std::auto_ptr<QImage> sag_image (get_sag_slice(m_sag_sp.first, m_sag_sp.second, m_scale_ratio, m_sag_view->size()));
	draw_sag_cursor(*sag_image);
	m_sag_view->setPixmap(QPixmap::fromImage(*sag_image));

	std::auto_ptr<QImage> cor_image(get_cor_slice(m_cor_sp.first, m_cor_sp.second, m_scale_ratio, m_cor_view->size()));
	draw_cor_cursor(*cor_image);
	m_cor_view->setPixmap(QPixmap::fromImage(*cor_image));

}

/************************************************************************/
/* Image Open                                                           */
/************************************************************************/

bool mxImageViewer::load_image(const QString& path)
{
	if (m_image == NULL)
	{
		m_image = new mxImage<PixelType>();
		m_tra_contours = new mxImage<unsigned char>();
		m_sag_contours = new mxImage<unsigned char>();
		m_cor_contours = new mxImage<unsigned char>();
	}
	else {
		m_image->Clear();
		m_tra_contours->Clear();
		m_sag_contours->Clear();
		m_cor_contours->Clear();
	}

	// read image
	if (!ImageHelper::ReadImage<PixelType, PixelType>(path.toStdString().c_str(), *m_image)) {
		QMessageBox::warning(this, tr("Failure"), tr("Could not open image. Please try the other images"));
		return false;
	}
	
	// initialize contours
	m_tra_contours->CopyImageInfo(*m_image);
	m_tra_contours->SetImageSize(m_image->GetImageSize());
	m_tra_contours->Fill(0);

	m_sag_contours->CopyImageInfo(*m_image);
	m_sag_contours->SetImageSize(m_image->GetImageSize());
	m_sag_contours->Fill(0);

	m_cor_contours->CopyImageInfo(*m_image);
	m_cor_contours->SetImageSize(m_image->GetImageSize());
	m_cor_contours->Fill(0);

	// setup image and path
	setup_image();
	setWindowFilePath(path);

	return true;
}

void mxImageViewer::open()
{
	QStringList image_type_filters;
	image_type_filters.append("MetaImage Files (*.mhd)");
	image_type_filters.append("AnalyzeImage Files (*.hdr)");

	QString open_path;
	if (m_last_open_image_path.isEmpty())
	{
		const QStringList home_locations = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
		open_path = home_locations.isEmpty() ? QDir::currentPath() : home_locations.first();
	}
	else
	{
		open_path = m_last_open_image_path;
	}

	QFileDialog dialog(this, tr("Open Image"), open_path);
	dialog.setAcceptMode(QFileDialog::AcceptOpen);
	dialog.setNameFilters(image_type_filters);

	while (dialog.exec() == QDialog::Accepted)
	{
		QFileInfo file_info(dialog.selectedFiles().first());
		m_last_open_image_path = file_info.dir().absolutePath();

		if (!load_image(file_info.absoluteFilePath()))
			continue;
		else
			break;
	}
}

/************************************************************************/
/* Surface operations                                                   */
/************************************************************************/

void mxImageViewer::update_surface()
{
	// generate contour images;
	mxImage<unsigned char> mask;
	vect3<int> sp, ep;
	unsigned char foreground = 255, background = 0;
	SurfaceUtils::SmartCarveSurface(*m_surf, *m_image, background, foreground, mask, sp, ep);

	mxImage<unsigned char> tra_contours, sag_contours, cor_contours;
	mxImageUtils::Find2DContours(mask, TRANSVERSAL, tra_contours);
	mxImageUtils::Find2DContours(mask, SAGITTAL, sag_contours);
	mxImageUtils::Find2DContours(mask, CORONAL, cor_contours);

	m_tra_contours->Fill(0);
	m_sag_contours->Fill(0);
	m_cor_contours->Fill(0);

	// copy back to the whole image
	#pragma omp parallel for
	for (int z = sp[2]; z <= ep[2]; ++z)
	{
		for (int y = sp[1]; y <= ep[1]; ++y)
		{
			for (int x = sp[0]; x <= ep[0]; ++x)
			{
				vect3<int> local_pos(x - sp[0], y - sp[1], z - sp[2]);

				(*m_tra_contours)(x, y, z) = tra_contours(local_pos[0], local_pos[1], local_pos[2]);
				(*m_sag_contours)(x, y, z) = sag_contours(local_pos[0], local_pos[1], local_pos[2]);
				(*m_cor_contours)(x, y, z) = cor_contours(local_pos[0], local_pos[1], local_pos[2]);
			}
		}
	}

	// update sub views
	update_sub_views();
}

void mxImageViewer::setup_surface()
{
	// if loaded surface is outside the image, move it to the center
	vect3<int> sp, ep;
	SurfaceUtils::BoundingBox(*m_surf, *m_image, sp, ep);
	if (sp[0] > ep[0] || sp[1] > ep[1] || sp[2] > ep[2])
	{
		vect3<double> image_center = mxImageUtils::WorldCenter(*m_image);
		m_surf->MoveTo(image_center.to<float>());
	}

	// enable surface panel
	m_tool_panel->m_surfaceBox->setEnabled(true);
	m_tool_panel->m_noDeformButton->setChecked(true);
	m_tool_panel->m_sigmaField->setEnabled(false);

	update_surface();
}

bool mxImageViewer::load_surface(const QString& path)
{
	if (m_surf == NULL)
		m_surf = new Surface;
	else
		m_surf->Clear();

	if (!SurfaceUtils::LoadSurface(path.toStdString().c_str(), *m_surf)) {
		QMessageBox::warning(this, tr("Failure"), tr("Could not open surface. Please try another surface"));
		return false;
	}

	setup_surface();

	return true;
}

void mxImageViewer::openSurface() 
{
	QStringList surface_type_filters;
	surface_type_filters.append("VTK Files (*.vtk)");
	surface_type_filters.append("ISO Files (*.iso)");

	QString open_path;
	if (m_last_open_surface_path.isEmpty())
	{
		const QStringList home_locations = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
		open_path = home_locations.isEmpty() ? QDir::currentPath() : home_locations.first();
	}
	else
	{
		open_path = m_last_open_surface_path;
	}

	QFileDialog dialog(this, tr("Open Surface"), open_path);
	dialog.setAcceptMode(QFileDialog::AcceptOpen);
	dialog.setNameFilters(surface_type_filters);

	while (dialog.exec() == QDialog::Accepted)
	{
		QFileInfo file_info(dialog.selectedFiles().first());
		m_last_open_surface_path = file_info.dir().absolutePath();

		if (!load_surface(file_info.absoluteFilePath()))
			continue;
		else
			break;
	}
}

void mxImageViewer::saveSurface()
{
	QStringList surface_type_filters;
	surface_type_filters.append("VTK Files (*.vtk)");
	surface_type_filters.append("ISO Files (*.iso)");

	QString open_path;
	if (m_last_open_surface_path.isEmpty())
	{
		const QStringList home_locations = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
		open_path = home_locations.isEmpty() ? QDir::currentPath() : home_locations.first();
	}
	else
	{
		open_path = m_last_open_surface_path;
	}

	QFileDialog dialog(this, tr("Save Surface"), open_path);
	dialog.setAcceptMode(QFileDialog::AcceptSave);
	dialog.setNameFilters(surface_type_filters);

	while (dialog.exec() == QDialog::Accepted)
	{
		QFileInfo file_info(dialog.selectedFiles().first());
		m_last_open_surface_path = file_info.dir().absolutePath();

		if (!SurfaceUtils::SaveSurface(*m_surf, file_info.absoluteFilePath().toStdString().c_str()))
			continue;
		else
			break;
	}
}

} } }

