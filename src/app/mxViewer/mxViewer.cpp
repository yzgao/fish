//
//  mxViewer.cpp
//  FISH
//
//  Created by Yaozong Gao on 03/05/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//

#include <QApplication>

#include "mxImageViewer.h"

using namespace BRIC::IDEA::FISH;

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	QGuiApplication::setApplicationDisplayName(mxImageViewer::tr("mxImage Viewer"));

	QDesktopWidget* desktop = QApplication::desktop();
	int screen_width = desktop->width();
	int screen_height = desktop->height();

	mxImageViewer viewer;
	viewer.adjustSize();
	int viewer_width = viewer.width();
	int viewer_height = viewer.height();

	int x = (screen_width - viewer_width) / 2;
	int y = (screen_height - viewer_height) / 2;;
	viewer.move(x, y);

	viewer.show();

	return app.exec();
}
