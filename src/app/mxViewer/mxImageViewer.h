//
//  mxViewer.h
//  FISH
//
//  Created by Yaozong Gao on 03/06/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __mxViewer_h__
#define __mxViewer_h__

#include <QtWidgets>
#include "common/vect.h"

namespace BRIC { namespace IDEA { namespace FISH {

//////////////////////////////////////////////////////////////////////////
// type definitions
typedef unsigned short PixelType;


//////////////////////////////////////////////////////////////////////////
// declaration
template <typename T>
class mxImage;

class Surface;


//////////////////////////////////////////////////////////////////////////
// image sub view declaration

class mxImageSubViewer : public QLabel
{
	Q_OBJECT

public:

	mxImageSubViewer();

	// override event handler
	void resizeEvent(QResizeEvent* event);
	void mousePressEvent(QMouseEvent* event);
	void mouseMoveEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent* event);
	void leaveEvent(QEvent * event);
	void wheelEvent(QWheelEvent* event);

	// attribute getter and setter
	void set_name(const QString& name) { m_name = name; }
	QString get_name() const { return m_name; }

	void update_surface(int start_x, int start_y, int end_x, int end_y);

private:

	void update_cursor_and_views(int x, int y);

private:

	QString m_name;
	
	bool m_LeftClicked;
	int m_leftStart_x;
	int m_leftStart_y;
	double m_fx, m_fy, m_fz;	// start world position

	bool m_middleClicked;
	int m_midStart_x;
	int m_midStart_y;

	bool m_rightClicked;
	int m_rightStart_y;

	std::pair<double,double> m_track_tra;
	std::pair<double, double> m_track_sag;
	std::pair<double, double> m_track_cor;
};


//////////////////////////////////////////////////////////////////////////
// mxImage quick tool panel

class mxImageQuickToolPanel : public QWidget
{
	Q_OBJECT

public:
	mxImageQuickToolPanel();

	void set_image_info(const mxImage<PixelType>& image);
	void set_cursor(const vect3<int>& voxel, const vect3<double>& world);
	void set_contrast(int min_value, int max_value);
	void init_contrast(int min_value, int max_value);

public slots:
	void voxelXChanged(int val);
	void voxelYChanged(int val);
	void voxelZChanged(int val);
	void worldXChanged(double val);
	void worldYChanged(double val);
	void worldZChanged(double val);

	void minFieldChanged(int val);
	void maxFieldChanged(int val);

	void minBarChanged(int val);
	void maxBarChanged(int val);

	void surfaceShowStateChanged(int state);
	void cursorShowStateChanged(int state);
	void centerSurfaceButtonClicked(bool bClicked);
	void deformStateChanged(bool checked);
	void sigmaChanged(double val);

private:
	void create_cursor_box();
	void create_intensity_box();
	void create_contrast_box();
	void create_surface_box();

public:
	// cursor box
	QGroupBox* m_cursorBox;

	QLabel* m_voxelLabel;
	QLabel* m_worldLabel;

	QSpinBox* m_vxField;
	QSpinBox* m_vyField;
	QSpinBox* m_vzField;

	QDoubleSpinBox* m_wxField;
	QDoubleSpinBox* m_wyField;
	QDoubleSpinBox* m_wzField;

	// intensity box
	QGroupBox* m_intensityBox;
	QLabel* m_intensityLabel;
	QLabel* m_labelLabel;
	QLabel* m_intensityField;
	QLabel* m_labelField;

	// contrast box
	QGroupBox* m_contrastBox;
	QSlider* m_minBar;
	QSpinBox* m_minField;
	QSlider* m_maxBar;
	QSpinBox* m_maxField;

	// surface box
	QGroupBox* m_surfaceBox;
	QPushButton* m_centerSurface;
	QCheckBox* m_showSurface;
	QCheckBox* m_showCursor;
	QRadioButton* m_noDeformButton;
	QRadioButton* m_shiftButton;
	QRadioButton* m_affineButton;
	QRadioButton* m_dragButton;
	QDoubleSpinBox* m_sigmaField;

	bool m_surfaceShow;
	bool m_cursorShow;
	int m_deformState;		// 0 for no deform; 1 for shift; 2 for free deform
	double m_deformSigma;
};



//////////////////////////////////////////////////////////////////////////
// mxImage Viewer declaration
class mxImageViewer : public QMainWindow
{
	Q_OBJECT

public:
	mxImageViewer();

private slots:

	void open();
	void openSurface();
	void saveSurface();

private:

	// create GUI elements
	void create_actions();
	void create_menu();

	// helper
	void initialize_data();
	bool load_image(const QString& path);
	bool load_surface(const QString& path);
	void setup_image();
	void setup_surface();
	void update_surface();
	double get_client_spacing() const;

	// draw functions
	void draw_tra_cursor(QImage& image);
	void draw_sag_cursor(QImage& image);
	void draw_cor_cursor(QImage& image);

	void update_sub_views();

	// get sub-view slices
	QImage* get_tra_slice(int sp_x, int sp_y, double scale_ratio, const QSize& size);
	QImage* get_sag_slice(int sp_x, int sp_y, double scale_ratio, const QSize& size);
	QImage* get_cor_slice(int sp_x, int sp_y, double scale_ratio, const QSize& size);
	QImage* get_black_slice(int width, int height);

	void get_tra_contour(mxImage<unsigned char>& contour);
	void get_sag_contour(mxImage<unsigned char>& contour);
	void get_cor_contour(mxImage<unsigned char>& contour);

	// coordinate transform
	vect3<int> tra_to_voxel(int x, int y, int width, int height, int slice_idx);
	vect3<int> sag_to_voxel(int x, int y, int width, int height, int slice_idx);
	vect3<int> cor_to_voxel(int x, int y, int width, int height, int slice_idx);

	std::pair<int,int> voxel_to_tra(int x, int y, int z);
	std::pair<int,int> voxel_to_sag(int x, int y, int z);
	std::pair<int,int> voxel_to_cor(int x, int y, int z);

	// intensity transform
	QRgb intensity_to_rgb(PixelType val);

private:

	// sub view widgets
	mxImageSubViewer* m_tra_view;
	mxImageSubViewer* m_sag_view;
	mxImageSubViewer* m_cor_view;
	mxImageQuickToolPanel* m_tool_panel;
	QGridLayout* m_layout;

	// menu and actions
	QAction* m_openAct;
	QAction* m_exitAct;
	QMenu* m_file_menu;

	QAction* m_openSurfaceAct;
	QAction* m_saveSurfaceAct;
	QMenu* m_surface_menu;

private:

	// program-owned
	QString m_last_open_image_path;
	QString m_last_open_surface_path;
	mxImage<PixelType>* m_image;
	mxImage<unsigned char>* m_mask;

	Surface* m_surf;

	mxImage<unsigned char>* m_tra_contours;
	mxImage<unsigned char>* m_sag_contours;
	mxImage<unsigned char>* m_cor_contours;
	
	// determined by loaded image
	double m_display_spacing;

	// dynamic interaction
	double m_scale_ratio;
	double m_scale_sigma;
	double m_min_scale_ratio;
	double m_max_scale_ratio;

	int m_cursor_x;
	int m_cursor_y;
	int m_cursor_z;

	std::pair<int, int> m_tra_sp;
	std::pair<int, int> m_sag_sp;
	std::pair<int, int> m_cor_sp;

	// could be customized
	int m_min_intensity;
	int m_max_intensity;

	QRgb m_background_color;
	QRgb m_cursor_color;
	QRgb m_contour_color;
	int m_cursor_radius;

public:

	friend class mxImageSubViewer;
	friend class mxImageQuickToolPanel;
};


} } }

#endif