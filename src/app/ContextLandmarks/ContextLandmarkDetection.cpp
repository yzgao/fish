//
//  ContextLandmarkDetection.cpp
//  Fish
//
//  Created by Yaozong Gao on 12/16/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#include "stdafx.h"

#include "forest/Forest.h"
#include "forest/stat/MultiDimensionStatisticsAggregator.h"
#include "forest/wl/MemoryAxisAlignedWeakLearner.h"
#include "extern/ImageHelper.h"
#include "extern/BoostHelper.h"
#include "detector/landmark/ContextLandmarkDetection.h"
#include <iostream>

using namespace BRIC::IDEA::FISH;


void print_usage()
{
	std::cerr << "[ context landmark detection ]" << std::endl;
	std::cerr << "ContextLandmarkDetection --lm [--vote|--jump] --image imagePath --root Folder --names lmNames --level level --maxscale scale1 --minscale scale2 --out lm_path\n" << std::endl;
}

template <typename T>
void DetectLandmarks(const std::string& imagePath, const std::string& root, const std::vector<std::string>& names, int level, int maxscale, int minscale, ContextLandmarkDetectionStrategy option,
					 std::map< std::string,vect3<double> >& worldLMs, std::map< std::string, vect3<int> >& voxelLMs)
{
	boost::timer::auto_cpu_timer timer;

	mxImage<T> image;
	if ( !ImageHelper::ReadImage<T,T>(imagePath.c_str(), image) )
	{
		std::cerr << "fail to read image from " << imagePath << std::endl;
		exit(-1);
	}

	ContextLandmarkDetectionParameters params;
	params.root = root; 
	params.names = names;
	params.contextLevel = level;
	params.maxscale = maxscale;
	params.minscale = minscale;
	params.option = option;

	ContextLandmarkDetection detection;
	detection.SetParameters(params);
	detection.DetectLandmarks(image, worldLMs);

	std::map< std::string, vect3<double> >::const_iterator it = worldLMs.begin();
	while (it != worldLMs.end())
	{
		const std::string& name = it->first;
		const vect3<double>& lm = it->second;

		vect3<int> voxel;
		mxImageUtils::World2Voxel(image, lm, voxel);

		voxelLMs[name] = voxel;
		++ it;
	}
}


int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic Information");
		generic.add_options()("help", "display helper information");

		options_description functions("Functions");
		functions.add_options()("lm", "landmark detection");
		functions.add_options()("dm", "distance map regression");

		options_description commonParams("Common Parameters");
		commonParams.add_options()("image", value<std::string>(), "image path (required)");
		commonParams.add_options()("root", value<std::string>(), "root folder (required)");
		commonParams.add_options()("names", value< std::vector<std::string> >()->multitoken(), "landmark names");
		commonParams.add_options()("level", value<int>(), "context level");

		options_description landmarkParams("Landmark Parameters");
		landmarkParams.add_options()("vote", "landmark detection using voting");
		landmarkParams.add_options()("jump", "landmark detection using jumping");
		landmarkParams.add_options()("maxscale", value<int>()->default_value(4), "coarsest scale");
		landmarkParams.add_options()("minscale", value<int>()->default_value(1), "finest scale");

		options_description mapParams("Distance Image Parameters (all required)");
		mapParams.add_options()("scale", value<int>(), "which scale of distance maps?");
		mapParams.add_options()("voxel", value< std::vector<int> >()->multitoken(), "voxel center in original image");
		mapParams.add_options()("world", value< std::vector<double> >()->multitoken(), "world center in original image");
		mapParams.add_options()("size", value< std::vector<double> >()->multitoken(), "size in world units");
		mapParams.add_options()("out", value< std::string >(), "output path");

		options_description all;
		all.add(generic).add(functions).add(commonParams).add(landmarkParams).add(mapParams);

		variables_map vm;
		store(command_line_parser(argc, argv).options(all).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			std::cerr << all << std::endl;
			print_usage();
			return -1;
		}

		if (vm.count("dm")==0 && vm.count("lm")==0) {
			std::cerr << "only option lm and dm are supported in this application" << std::endl;
			exit(-1);
		}

		if (vm.count("dm")) {

			std::cerr << "under construction" << std::endl; 
			exit(-1);

		}else if(vm.count("lm")) {

			std::string imagePath = BoostCmdHelper::Get<std::string>(vm, "image");
			std::string root = BoostCmdHelper::Get<std::string>(vm, "root");
			std::vector<std::string> names = BoostCmdHelper::Get< std::vector<std::string> >(vm, "names");
			int level = BoostCmdHelper::Get<int>(vm, "level");
			int maxscale = BoostCmdHelper::Get<int>(vm, "maxscale");
			int minscale = BoostCmdHelper::Get<int>(vm, "minscale");

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(imagePath.c_str());

			std::map< std::string, vect3<double> > worldLMs;
			std::map< std::string, vect3<int> > voxelLMs;

			// set the detection strategy
			ContextLandmarkDetectionStrategy option;
			if (vm.count("jump"))
				option = LM_JUMPING;
			else if (vm.count("vote"))
				option = LM_VOTING;
			else 
				option = LM_JUMPING;


			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR:
				DetectLandmarks<unsigned char>(imagePath, root, names, level, maxscale, minscale, option, worldLMs, voxelLMs);
				break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR:
				DetectLandmarks<char>(imagePath, root, names, level, maxscale, minscale, option, worldLMs, voxelLMs);
				break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT:
				DetectLandmarks<unsigned short>(imagePath, root, names, level, maxscale, minscale, option, worldLMs, voxelLMs);
				break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT:
				DetectLandmarks<short>(imagePath, root, names, level, maxscale, minscale, option, worldLMs, voxelLMs);
				break;
			case BRIC::IDEA::FISH::ImageHelper::UINT:
				DetectLandmarks<unsigned int>(imagePath, root, names, level, maxscale, minscale, option, worldLMs, voxelLMs);
				break;
			case BRIC::IDEA::FISH::ImageHelper::INT:
				DetectLandmarks<int>(imagePath, root, names, level, maxscale, minscale, option, worldLMs, voxelLMs);
				break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG:
				DetectLandmarks<unsigned long>(imagePath, root, names, level, maxscale, minscale, option, worldLMs, voxelLMs);
				break;
			case BRIC::IDEA::FISH::ImageHelper::LONG:
				DetectLandmarks<long>(imagePath, root, names, level, maxscale, minscale, option, worldLMs, voxelLMs);
				break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT:
				DetectLandmarks<float>(imagePath, root, names, level, maxscale, minscale, option, worldLMs, voxelLMs);
				break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE:
				DetectLandmarks<double>(imagePath, root, names, level, maxscale, minscale, option, worldLMs, voxelLMs);
				break;
			default:
				std::cerr << "unrecognized pixel type" << std::endl;
				exit(-1);
			}

			std::map< std::string, vect3<double> >::const_iterator it = worldLMs.begin();

			bool isFileOut = (vm.count("out") != 0);
			if (isFileOut) {
				std::string text_out = BoostCmdHelper::Get<std::string>(vm, "out");
				std::ofstream outfile(text_out.c_str());
				if (!outfile) {
					std::cerr << "fails to open file for write: " << text_out << std::endl; return -1;
				}
				while (it != worldLMs.end())
				{
					const std::string& name = it->first;
					const vect3<double>& lm = it->second;
					vect3<int> voxel = voxelLMs[name];

					outfile << name << " " << voxel[0] << " " << voxel[1] << " " << voxel[2] << " " << lm[0] << " " << lm[1] << " " << lm[2] << std::endl;

					++it;
				}
				outfile.close();
			}
			else {
				while (it != worldLMs.end())
				{
					const std::string& name = it->first;
					const vect3<double>& lm = it->second;
					vect3<int> voxel = voxelLMs[name];

					std::cout << name << " " << voxel[0] << " " << voxel[1] << " " << voxel[2] << " " << lm[0] << " " << lm[1] << " " << lm[2] << std::endl;

					++it;
				}
			}

		}else {
			std::cerr << "unexpected error in option selection" << std::endl;
			exit(-1);
		}

	} catch ( boost::program_options::error& exp ) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}
	


	return 0;
}