#include "stdafx.h"
#include "extern/ImageHelper.h"
#include "extern/BoostHelper.h"
#include "common/mxImageUtils.h"
#include "common/stringUtils.h"
#include "common/EvaluateTools.h"
#include "detector/JAT/JAT_annot_struct.h"

using namespace BRIC::IDEA::FISH;

void print_help()
{
	std::cerr << "[ average surface distance ]" << std::endl;
	std::cerr << "EvaluateTools --asd --in1 imageA --in2 imageB\n" << std::endl;

	std::cerr << "[ generalized hausdroff surface distance (by default p = 1) ]" << std::endl;
	std::cerr << "EvaluateTools --hausdroff --in1 imageA --in2 imageB [--p 0.95]\n" << std::endl;

	std::cerr << "[ dice similarity coefficient / sensitivity / ppv ]" << std::endl;
	std::cerr << "EvaluateTools {--dsc | --sen | --ppv} --in1 {test_image | test_surf} --in2 {gt_image | gt_surf}\n" << std::endl;

	std::cerr << "[ anatomy detection accuracy ]" << std::endl;
	std::cerr << "EvaluateTools --anatomy --in1 detected.txt --in2 groundtruth.txt --out result.txt\n" << std::endl;

	std::cerr << "[ landmark detection accuracy in details ]" << std::endl;
	std::cerr << "EvaluateTools --landmark --in1 detected.txt --in2 groundtruth.txt --out result.txt\n" << std::endl;

	std::cerr << "[ dice similarity coefficient for rectum ]" << std::endl;
	std::cerr << "EvaluateTools {--rectumdsc | --rectumasd | --rectumsen | --rectumppv} --in1 {test_image | test_surf} --in2 {gt_image | gt_surf}\n" << std::endl;
}

template <typename T, typename FunctionType>
void ComputeMetrics_Base(std::string path1, std::string path2, FunctionType function)
{
	std::string extension1 = stringUtils::extension(path1);
	std::string extension2 = stringUtils::extension(path2);

	bool is_surface1 = (extension1 == "vtk") || (extension1 == "iso");
	bool is_surface2 = (extension2 == "vtk") || (extension2 == "iso");

	double metric = 0;

	if (is_surface1 && !is_surface2)
	{
		Surface surf1;
		if (!SurfaceUtils::LoadSurface(path1.c_str(), surf1)) {
			std::cerr << "fails to read surface from " << path1 << std::endl; exit(-1);
		}

		mxImage<T> image2;
		if (!ImageHelper::ReadImage<T, T>(path2.c_str(), image2)) {
			std::cerr << "fails to read image from " << path2 << std::endl; exit(-1);
		}

		mxImage<T> image1;
		SurfaceUtils::CarveSurface<T,T>(surf1, image2, static_cast<T>(0), static_cast<T>(255), image1);

		metric = function(image1, static_cast<T>(255), image2, static_cast<T>(255));
	}
	else if (!is_surface1 && is_surface2) {

		Surface surf2;
		if (!SurfaceUtils::LoadSurface(path2.c_str(), surf2)) {
			std::cerr << "fails to read surface from " << path2 << std::endl; exit(-1);
		}

		mxImage<T> image1;
		if (!ImageHelper::ReadImage<T, T>(path1.c_str(), image1)) {
			std::cerr << "fails to read image from " << path1 << std::endl; exit(-1);
		}

		mxImage<T> image2;
		SurfaceUtils::CarveSurface<T,T>(surf2, image1, static_cast<T>(0), static_cast<T>(255), image2);

		metric = function(image1, static_cast<T>(255), image2, static_cast<T>(255));
	}
	else if (!is_surface1 && !is_surface2) {

		mxImage<T> image1, image2;
		if (!ImageHelper::ReadImage<T, T>(path1.c_str(), image1)) {
			std::cerr << "fails to read image from " << path1 << std::endl; exit(-1);
		}

		if (!ImageHelper::ReadImage<T, T>(path2.c_str(), image2)) {
			std::cerr << "fails to read image from " << path2 << std::endl; exit(-1);
		}

		metric = function(image1, static_cast<T>(255), image2, static_cast<T>(255));
	}
	else {
		std::cerr << "two inputs could not be all surfaces" << std::endl;
		exit(-1);
	}

	std::cout << metric << std::endl;
}

void ComputeASD(std::string path1, std::string path2, ImageHelper::PixelType type)
{
	switch (type)
	{
	case BRIC::IDEA::FISH::ImageHelper::UCHAR: ComputeMetrics_Base<unsigned char>(path1, path2, EvaluateTools::AverageSurfaceDistance<unsigned char>); break;
	case BRIC::IDEA::FISH::ImageHelper::CHAR: ComputeMetrics_Base<char>(path1, path2, EvaluateTools::AverageSurfaceDistance<char>); break;
	case BRIC::IDEA::FISH::ImageHelper::USHORT: ComputeMetrics_Base<unsigned short>(path1, path2, EvaluateTools::AverageSurfaceDistance<unsigned short>); break;
	case BRIC::IDEA::FISH::ImageHelper::SHORT: ComputeMetrics_Base<short>(path1, path2, EvaluateTools::AverageSurfaceDistance<short>); break;
	case BRIC::IDEA::FISH::ImageHelper::UINT: ComputeMetrics_Base<unsigned int>(path1, path2, EvaluateTools::AverageSurfaceDistance<unsigned int>); break;
	case BRIC::IDEA::FISH::ImageHelper::INT: ComputeMetrics_Base<int>(path1, path2, EvaluateTools::AverageSurfaceDistance<int>); break;
	case BRIC::IDEA::FISH::ImageHelper::ULONG: ComputeMetrics_Base<unsigned long>(path1, path2, EvaluateTools::AverageSurfaceDistance<unsigned long>); break;
	case BRIC::IDEA::FISH::ImageHelper::LONG: ComputeMetrics_Base<long>(path1, path2, EvaluateTools::AverageSurfaceDistance<long>); break;
	case BRIC::IDEA::FISH::ImageHelper::FLOAT: ComputeMetrics_Base<float>(path1, path2, EvaluateTools::AverageSurfaceDistance<float>); break;
	case BRIC::IDEA::FISH::ImageHelper::DOUBLE: ComputeMetrics_Base<double>(path1, path2, EvaluateTools::AverageSurfaceDistance<double>); break;
	default: std::cerr << "unrecognized pixel type" << std::endl; exit(-1);
	}
}

void ComputeHausdroff(std::string path1, std::string path2, ImageHelper::PixelType type)
{
	switch (type)
	{
	case BRIC::IDEA::FISH::ImageHelper::UCHAR: ComputeMetrics_Base<unsigned char>(path1, path2, EvaluateTools::HausdroffDistance<unsigned char>); break;
	case BRIC::IDEA::FISH::ImageHelper::CHAR: ComputeMetrics_Base<char>(path1, path2, EvaluateTools::HausdroffDistance<char>); break;
	case BRIC::IDEA::FISH::ImageHelper::USHORT: ComputeMetrics_Base<unsigned short>(path1, path2, EvaluateTools::HausdroffDistance<unsigned short>); break;
	case BRIC::IDEA::FISH::ImageHelper::SHORT: ComputeMetrics_Base<short>(path1, path2, EvaluateTools::HausdroffDistance<short>); break;
	case BRIC::IDEA::FISH::ImageHelper::UINT: ComputeMetrics_Base<unsigned int>(path1, path2, EvaluateTools::HausdroffDistance<unsigned int>); break;
	case BRIC::IDEA::FISH::ImageHelper::INT: ComputeMetrics_Base<int>(path1, path2, EvaluateTools::HausdroffDistance<int>); break;
	case BRIC::IDEA::FISH::ImageHelper::ULONG: ComputeMetrics_Base<unsigned long>(path1, path2, EvaluateTools::HausdroffDistance<unsigned long>); break;
	case BRIC::IDEA::FISH::ImageHelper::LONG: ComputeMetrics_Base<long>(path1, path2, EvaluateTools::HausdroffDistance<long>); break;
	case BRIC::IDEA::FISH::ImageHelper::FLOAT: ComputeMetrics_Base<float>(path1, path2, EvaluateTools::HausdroffDistance<float>); break;
	case BRIC::IDEA::FISH::ImageHelper::DOUBLE: ComputeMetrics_Base<double>(path1, path2, EvaluateTools::HausdroffDistance<double>); break;
	default: std::cerr << "unrecognized pixel type" << std::endl; exit(-1);
	}
}

class GHFWrapper {
public:
	GHFWrapper(double percentage = 1.0) : m_percentage(percentage) {}
	
	template <typename T>
	double operator() (const mxImage<T>& image1, T obj_val1, const mxImage<T>& image2, T obj_val2) const {
		return EvaluateTools::GeneralizedHausdroffDistance(image1, obj_val2, image2, obj_val2, m_percentage);
	}

private:
	double m_percentage;
};

void ComputeGeneralizedHausdroff(std::string path1, std::string path2, ImageHelper::PixelType type, double p)
{
	GHFWrapper fun(p);

	switch (type)
	{
	case BRIC::IDEA::FISH::ImageHelper::UCHAR: ComputeMetrics_Base<unsigned char>(path1, path2, fun); break;
	case BRIC::IDEA::FISH::ImageHelper::CHAR: ComputeMetrics_Base<char>(path1, path2, fun); break;
	case BRIC::IDEA::FISH::ImageHelper::USHORT: ComputeMetrics_Base<unsigned short>(path1, path2, fun); break;
	case BRIC::IDEA::FISH::ImageHelper::SHORT: ComputeMetrics_Base<short>(path1, path2, fun); break;
	case BRIC::IDEA::FISH::ImageHelper::UINT: ComputeMetrics_Base<unsigned int>(path1, path2, fun); break;
	case BRIC::IDEA::FISH::ImageHelper::INT: ComputeMetrics_Base<int>(path1, path2, fun); break;
	case BRIC::IDEA::FISH::ImageHelper::ULONG: ComputeMetrics_Base<unsigned long>(path1, path2, fun); break;
	case BRIC::IDEA::FISH::ImageHelper::LONG: ComputeMetrics_Base<long>(path1, path2, fun); break;
	case BRIC::IDEA::FISH::ImageHelper::FLOAT: ComputeMetrics_Base<float>(path1, path2, fun); break;
	case BRIC::IDEA::FISH::ImageHelper::DOUBLE: ComputeMetrics_Base<double>(path1, path2, fun); break;
	default: std::cerr << "unrecognized pixel type" << std::endl; exit(-1);
	}
}

void ComputeDSC(std::string path1, std::string path2, ImageHelper::PixelType type)
{
	switch (type)
	{
	case BRIC::IDEA::FISH::ImageHelper::UCHAR: ComputeMetrics_Base<unsigned char>(path1, path2, EvaluateTools::DSC<unsigned char, unsigned char>); break;
	case BRIC::IDEA::FISH::ImageHelper::CHAR: ComputeMetrics_Base<char>(path1, path2, EvaluateTools::DSC<char, char>); break;
	case BRIC::IDEA::FISH::ImageHelper::USHORT: ComputeMetrics_Base<unsigned short>(path1, path2, EvaluateTools::DSC<unsigned short, unsigned short>); break;
	case BRIC::IDEA::FISH::ImageHelper::SHORT: ComputeMetrics_Base<short>(path1, path2, EvaluateTools::DSC<short, short>); break;
	case BRIC::IDEA::FISH::ImageHelper::UINT:ComputeMetrics_Base<unsigned int>(path1, path2, EvaluateTools::DSC<unsigned int, unsigned int>); break;
	case BRIC::IDEA::FISH::ImageHelper::INT: ComputeMetrics_Base<int>(path1, path2, EvaluateTools::DSC<int, int>); break;
	case BRIC::IDEA::FISH::ImageHelper::ULONG: ComputeMetrics_Base<unsigned long>(path1, path2, EvaluateTools::DSC<unsigned long, unsigned long>); break;
	case BRIC::IDEA::FISH::ImageHelper::LONG: ComputeMetrics_Base<long>(path1, path2, EvaluateTools::DSC<long, long>); break;
	case BRIC::IDEA::FISH::ImageHelper::FLOAT: ComputeMetrics_Base<float>(path1, path2, EvaluateTools::DSC<float, float>); break;
	case BRIC::IDEA::FISH::ImageHelper::DOUBLE: ComputeMetrics_Base<double>(path1, path2, EvaluateTools::DSC<double, double>); break;
	default: std::cerr << "unrecognized pixel type" << std::endl; exit(-1);
	}
}

void ComputeSensitivity(std::string path1, std::string path2, ImageHelper::PixelType type)
{
	switch (type)
	{
	case BRIC::IDEA::FISH::ImageHelper::UCHAR: ComputeMetrics_Base<unsigned char>(path1, path2, EvaluateTools::Sensitivity<unsigned char, unsigned char>); break;
	case BRIC::IDEA::FISH::ImageHelper::CHAR: ComputeMetrics_Base<char>(path1, path2, EvaluateTools::Sensitivity<char, char>); break;
	case BRIC::IDEA::FISH::ImageHelper::USHORT: ComputeMetrics_Base<unsigned short>(path1, path2, EvaluateTools::Sensitivity<unsigned short, unsigned short>); break;
	case BRIC::IDEA::FISH::ImageHelper::SHORT: ComputeMetrics_Base<short>(path1, path2, EvaluateTools::Sensitivity<short, short>); break;
	case BRIC::IDEA::FISH::ImageHelper::UINT:ComputeMetrics_Base<unsigned int>(path1, path2, EvaluateTools::Sensitivity<unsigned int, unsigned int>); break;
	case BRIC::IDEA::FISH::ImageHelper::INT: ComputeMetrics_Base<int>(path1, path2, EvaluateTools::Sensitivity<int, int>); break;
	case BRIC::IDEA::FISH::ImageHelper::ULONG: ComputeMetrics_Base<unsigned long>(path1, path2, EvaluateTools::Sensitivity<unsigned long, unsigned long>); break;
	case BRIC::IDEA::FISH::ImageHelper::LONG: ComputeMetrics_Base<long>(path1, path2, EvaluateTools::Sensitivity<long, long>); break;
	case BRIC::IDEA::FISH::ImageHelper::FLOAT: ComputeMetrics_Base<float>(path1, path2, EvaluateTools::Sensitivity<float, float>); break;
	case BRIC::IDEA::FISH::ImageHelper::DOUBLE: ComputeMetrics_Base<double>(path1, path2, EvaluateTools::Sensitivity<double, double>); break;
	default: std::cerr << "unrecognized pixel type" << std::endl; exit(-1);
	}
}

void ComputePPV(std::string path1, std::string path2, ImageHelper::PixelType type)
{
	switch (type)
	{
	case BRIC::IDEA::FISH::ImageHelper::UCHAR: ComputeMetrics_Base<unsigned char>(path1, path2, EvaluateTools::PPV<unsigned char, unsigned char>); break;
	case BRIC::IDEA::FISH::ImageHelper::CHAR: ComputeMetrics_Base<char>(path1, path2, EvaluateTools::PPV<char, char>); break;
	case BRIC::IDEA::FISH::ImageHelper::USHORT: ComputeMetrics_Base<unsigned short>(path1, path2, EvaluateTools::PPV<unsigned short, unsigned short>); break;
	case BRIC::IDEA::FISH::ImageHelper::SHORT: ComputeMetrics_Base<short>(path1, path2, EvaluateTools::PPV<short, short>); break;
	case BRIC::IDEA::FISH::ImageHelper::UINT:ComputeMetrics_Base<unsigned int>(path1, path2, EvaluateTools::PPV<unsigned int, unsigned int>); break;
	case BRIC::IDEA::FISH::ImageHelper::INT: ComputeMetrics_Base<int>(path1, path2, EvaluateTools::PPV<int, int>); break;
	case BRIC::IDEA::FISH::ImageHelper::ULONG: ComputeMetrics_Base<unsigned long>(path1, path2, EvaluateTools::PPV<unsigned long, unsigned long>); break;
	case BRIC::IDEA::FISH::ImageHelper::LONG: ComputeMetrics_Base<long>(path1, path2, EvaluateTools::PPV<long, long>); break;
	case BRIC::IDEA::FISH::ImageHelper::FLOAT: ComputeMetrics_Base<float>(path1, path2, EvaluateTools::PPV<float, float>); break;
	case BRIC::IDEA::FISH::ImageHelper::DOUBLE: ComputeMetrics_Base<double>(path1, path2, EvaluateTools::PPV<double, double>); break;
	default: std::cerr << "unrecognized pixel type" << std::endl; exit(-1);
	}
}

void ComputeRectumDSC(std::string path1, std::string path2, ImageHelper::PixelType type)
{
	switch (type)
	{
	case BRIC::IDEA::FISH::ImageHelper::UCHAR: ComputeMetrics_Base<unsigned char>(path1, path2, EvaluateTools::RectumDSC<unsigned char, unsigned char>); break;
	case BRIC::IDEA::FISH::ImageHelper::CHAR: ComputeMetrics_Base<char>(path1, path2, EvaluateTools::RectumDSC<char, char>); break;
	case BRIC::IDEA::FISH::ImageHelper::USHORT: ComputeMetrics_Base<unsigned short>(path1, path2, EvaluateTools::RectumDSC<unsigned short, unsigned short>); break;
	case BRIC::IDEA::FISH::ImageHelper::SHORT: ComputeMetrics_Base<short>(path1, path2, EvaluateTools::RectumDSC<short, short>); break;
	case BRIC::IDEA::FISH::ImageHelper::UINT:ComputeMetrics_Base<unsigned int>(path1, path2, EvaluateTools::RectumDSC<unsigned int, unsigned int>); break;
	case BRIC::IDEA::FISH::ImageHelper::INT: ComputeMetrics_Base<int>(path1, path2, EvaluateTools::RectumDSC<int, int>); break;
	case BRIC::IDEA::FISH::ImageHelper::ULONG: ComputeMetrics_Base<unsigned long>(path1, path2, EvaluateTools::RectumDSC<unsigned long, unsigned long>); break;
	case BRIC::IDEA::FISH::ImageHelper::LONG: ComputeMetrics_Base<long>(path1, path2, EvaluateTools::RectumDSC<long, long>); break;
	case BRIC::IDEA::FISH::ImageHelper::FLOAT: ComputeMetrics_Base<float>(path1, path2, EvaluateTools::RectumDSC<float, float>); break;
	case BRIC::IDEA::FISH::ImageHelper::DOUBLE: ComputeMetrics_Base<double>(path1, path2, EvaluateTools::RectumDSC<double, double>); break;
	default: std::cerr << "unrecognized pixel type" << std::endl; exit(-1);
	}
}

void ComputeRectumASD(std::string path1, std::string path2, ImageHelper::PixelType type)
{
	switch (type)
	{
	case BRIC::IDEA::FISH::ImageHelper::UCHAR: ComputeMetrics_Base<unsigned char>(path1, path2, EvaluateTools::RectumASD<unsigned char, unsigned char>); break;
	case BRIC::IDEA::FISH::ImageHelper::CHAR: ComputeMetrics_Base<char>(path1, path2, EvaluateTools::RectumASD<char, char>); break;
	case BRIC::IDEA::FISH::ImageHelper::USHORT: ComputeMetrics_Base<unsigned short>(path1, path2, EvaluateTools::RectumASD<unsigned short, unsigned short>); break;
	case BRIC::IDEA::FISH::ImageHelper::SHORT: ComputeMetrics_Base<short>(path1, path2, EvaluateTools::RectumASD<short, short>); break;
	case BRIC::IDEA::FISH::ImageHelper::UINT:ComputeMetrics_Base<unsigned int>(path1, path2, EvaluateTools::RectumASD<unsigned int, unsigned int>); break;
	case BRIC::IDEA::FISH::ImageHelper::INT: ComputeMetrics_Base<int>(path1, path2, EvaluateTools::RectumASD<int, int>); break;
	case BRIC::IDEA::FISH::ImageHelper::ULONG: ComputeMetrics_Base<unsigned long>(path1, path2, EvaluateTools::RectumASD<unsigned long, unsigned long>); break;
	case BRIC::IDEA::FISH::ImageHelper::LONG: ComputeMetrics_Base<long>(path1, path2, EvaluateTools::RectumASD<long, long>); break;
	case BRIC::IDEA::FISH::ImageHelper::FLOAT: ComputeMetrics_Base<float>(path1, path2, EvaluateTools::RectumASD<float, float>); break;
	case BRIC::IDEA::FISH::ImageHelper::DOUBLE: ComputeMetrics_Base<double>(path1, path2, EvaluateTools::RectumASD<double, double>); break;
	default: std::cerr << "unrecognized pixel type" << std::endl; exit(-1);
	}
}

void ComputeRectumSensitivity(std::string path1, std::string path2, ImageHelper::PixelType type)
{
	switch (type)
	{
	case BRIC::IDEA::FISH::ImageHelper::UCHAR: ComputeMetrics_Base<unsigned char>(path1, path2, EvaluateTools::RectumSensitivity<unsigned char, unsigned char>); break;
	case BRIC::IDEA::FISH::ImageHelper::CHAR: ComputeMetrics_Base<char>(path1, path2, EvaluateTools::RectumSensitivity<char, char>); break;
	case BRIC::IDEA::FISH::ImageHelper::USHORT: ComputeMetrics_Base<unsigned short>(path1, path2, EvaluateTools::RectumSensitivity<unsigned short, unsigned short>); break;
	case BRIC::IDEA::FISH::ImageHelper::SHORT: ComputeMetrics_Base<short>(path1, path2, EvaluateTools::RectumSensitivity<short, short>); break;
	case BRIC::IDEA::FISH::ImageHelper::UINT:ComputeMetrics_Base<unsigned int>(path1, path2, EvaluateTools::RectumSensitivity<unsigned int, unsigned int>); break;
	case BRIC::IDEA::FISH::ImageHelper::INT: ComputeMetrics_Base<int>(path1, path2, EvaluateTools::RectumSensitivity<int, int>); break;
	case BRIC::IDEA::FISH::ImageHelper::ULONG: ComputeMetrics_Base<unsigned long>(path1, path2, EvaluateTools::RectumSensitivity<unsigned long, unsigned long>); break;
	case BRIC::IDEA::FISH::ImageHelper::LONG: ComputeMetrics_Base<long>(path1, path2, EvaluateTools::RectumSensitivity<long, long>); break;
	case BRIC::IDEA::FISH::ImageHelper::FLOAT: ComputeMetrics_Base<float>(path1, path2, EvaluateTools::RectumSensitivity<float, float>); break;
	case BRIC::IDEA::FISH::ImageHelper::DOUBLE: ComputeMetrics_Base<double>(path1, path2, EvaluateTools::RectumSensitivity<double, double>); break;
	default: std::cerr << "unrecognized pixel type" << std::endl; exit(-1);
	}
}


void ComputeRectumPPV(std::string path1, std::string path2, ImageHelper::PixelType type)
{
	switch (type)
	{
	case BRIC::IDEA::FISH::ImageHelper::UCHAR: ComputeMetrics_Base<unsigned char>(path1, path2, EvaluateTools::RectumPPV<unsigned char, unsigned char>); break;
	case BRIC::IDEA::FISH::ImageHelper::CHAR: ComputeMetrics_Base<char>(path1, path2, EvaluateTools::RectumPPV<char, char>); break;
	case BRIC::IDEA::FISH::ImageHelper::USHORT: ComputeMetrics_Base<unsigned short>(path1, path2, EvaluateTools::RectumPPV<unsigned short, unsigned short>); break;
	case BRIC::IDEA::FISH::ImageHelper::SHORT: ComputeMetrics_Base<short>(path1, path2, EvaluateTools::RectumPPV<short, short>); break;
	case BRIC::IDEA::FISH::ImageHelper::UINT:ComputeMetrics_Base<unsigned int>(path1, path2, EvaluateTools::RectumPPV<unsigned int, unsigned int>); break;
	case BRIC::IDEA::FISH::ImageHelper::INT: ComputeMetrics_Base<int>(path1, path2, EvaluateTools::RectumPPV<int, int>); break;
	case BRIC::IDEA::FISH::ImageHelper::ULONG: ComputeMetrics_Base<unsigned long>(path1, path2, EvaluateTools::RectumPPV<unsigned long, unsigned long>); break;
	case BRIC::IDEA::FISH::ImageHelper::LONG: ComputeMetrics_Base<long>(path1, path2, EvaluateTools::RectumPPV<long, long>); break;
	case BRIC::IDEA::FISH::ImageHelper::FLOAT: ComputeMetrics_Base<float>(path1, path2, EvaluateTools::RectumPPV<float, float>); break;
	case BRIC::IDEA::FISH::ImageHelper::DOUBLE: ComputeMetrics_Base<double>(path1, path2, EvaluateTools::RectumPPV<double, double>); break;
	default: std::cerr << "unrecognized pixel type" << std::endl; exit(-1);
	}
}


void ComputeLandmarkAccuracy(std::string detected_path, std::string gt_path, std::string output_path)
{
	JAT_annot_struct detected_annot, gt_annot;

	if (!detected_annot.parse(detected_path.c_str()))
		err_message("fail to load detected annotation");

	if (!gt_annot.parse(gt_path.c_str()))
		err_message("fail to load ground truth annotation");

	assert_message(detected_annot.positions.image_num == gt_annot.positions.image_num, "image number mismatch");
	assert_message(detected_annot.positions.ids.size() == gt_annot.positions.ids.size(), "id number mismatch");
	assert_message(detected_annot.positions.lm_worlds.size() == gt_annot.positions.lm_worlds.size(), "lm_worlds mismatch");

	vnl_matrix<double> landmark_errors[3];
	for (int k = 0; k < 3; ++k)
	{
		landmark_errors[k].set_size(detected_annot.positions.image_num, detected_annot.positions.ids.size());
		landmark_errors[k].fill(0);
	}

	for (int i = 0; i < detected_annot.positions.image_num; ++i)
	{
		for (size_t j = 0; j < detected_annot.positions.ids.size(); ++j)
		{
			vect3<double> detected_lm, gt_lm;
			detected_lm = detected_annot.positions.get_lm_world(i, j);
			gt_lm = gt_annot.positions.get_lm_world(i, j);

			vect3<double> diff = detected_lm - gt_lm;
			for (int k = 0; k < 3; ++k)
				diff[k] = std::abs(diff[k]);

			for (int k = 0; k < 3; ++k)
				landmark_errors[k](i, j) = diff[k];
		}
	}

	// write out the error matrix
	FILE* fp = fopen(output_path.c_str(), "w");

	// write the landmark heading
	if (detected_annot.positions.ids.size() > 0)
	{
		fprintf(fp, " \t");
		for (size_t i = 0; i < detected_annot.positions.ids.size(); ++i)
			fprintf(fp, "%d\t\t\t", detected_annot.positions.ids[i]);
		fprintf(fp, "\n");

		// write the landmark errors
		for (int i = 0; i < detected_annot.positions.image_num; ++i)
		{
			fprintf(fp, "%d\t", i);
			for (size_t j = 0; j < detected_annot.positions.ids.size(); ++j)
			{
				for (int k = 0; k < 3; ++k)
					fprintf(fp, "%.3lf\t", landmark_errors[k](i, j));
			}
			fprintf(fp, "\n");
		}
	}

	fclose(fp);
}

void ComputeDetectionAccuracy(std::string detected_path, std::string gt_path, std::string output_path)
{
	JAT_annot_struct detected_annot, gt_annot;

	if (!detected_annot.parse(detected_path.c_str()))
		err_message("fail to load detected annotation");

	if (!gt_annot.parse(gt_path.c_str()))
		err_message("fail to load ground truth annotation");

	assert_message( detected_annot.positions.image_num == gt_annot.positions.image_num, "image number mismatch" );
	assert_message( detected_annot.positions.ids.size() == gt_annot.positions.ids.size(), "id number mismatch" );
	assert_message( detected_annot.positions.lm_worlds.size() == gt_annot.positions.lm_worlds.size(), "lm_worlds mismatch");

	vnl_matrix<double> landmark_errors;
	landmark_errors.set_size( detected_annot.positions.image_num, detected_annot.positions.ids.size());
	landmark_errors.fill( 0 );

	for (int i = 0; i < detected_annot.positions.image_num; ++i)
	{
		for (size_t j = 0; j < detected_annot.positions.ids.size(); ++j)
		{
			vect3<double> detected_lm, gt_lm;
			detected_lm = detected_annot.positions.get_lm_world(i, j);
			gt_lm = gt_annot.positions.get_lm_world(i, j);

			landmark_errors(i, j) = (detected_lm - gt_lm).l2norm();
		}
	}

	
	// write out the error matrix
	FILE* fp = fopen(output_path.c_str(), "w");
	
	// write the landmark heading
	fprintf(fp, " \t");
	for (size_t i = 0; i < detected_annot.positions.ids.size(); ++i)
		fprintf(fp, "%d\t", detected_annot.positions.ids[i]);
	fprintf(fp, "\n");

	// write the landmark errors
	for (int i = 0; i < detected_annot.positions.image_num; ++i)
	{
		fprintf(fp, "%d\t", i);
		for (size_t j = 0; j < detected_annot.positions.ids.size(); ++j)
			fprintf(fp, "%.3lf\t", landmark_errors(i, j));
		fprintf(fp, "\n");
	}

	fclose(fp);
}


int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display helper information");

		options_description functional("Functional options (must pick one)");
		functional.add_options()("asd", "compute average surface distance");
		functional.add_options()("hausdroff", "compute generalized hausdroff surface distance");
		functional.add_options()("dsc", "compute DSC");
		functional.add_options()("sen", "compute sensitivity");
		functional.add_options()("ppv", "compute ppv");
		functional.add_options()("anatomy", "compute landmark detection error");
		functional.add_options()("landmark", "compute landmark detection error in details");
		functional.add_options()("rectumdsc", "compute DSC for rectum");
		functional.add_options()("rectumasd", "compute ASD for rectum");
		functional.add_options()("rectumsen", "compute SEN for rectum");
		functional.add_options()("rectumppv", "compute PPV for rectum");

		options_description params("Parameter options");
		params.add_options()("in1", value<std::string>(), "input image or surface");
		params.add_options()("in2", value<std::string>(), "input image or surface");
		params.add_options()("out", value<std::string>(), "output image path");
		params.add_options()("p", value<double>()->default_value(1.0), "percentages in Hausdroff distance");

		options_description cmd_options;
		cmd_options.add(generic).add(functional).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);


		if( vm.count("help") || argc == 1 )
		{
			std::cerr << cmd_options << std::endl;
			print_help();
			return -1;
		}

		if ( vm.count("asd") ) {

			std::string path1 = BoostCmdHelper::Get<std::string>(vm, "in1");
			std::string path2 = BoostCmdHelper::Get<std::string>(vm, "in2");

			std::string extension1 = stringUtils::extension(path1);
			std::string extension2 = stringUtils::extension(path2);

			bool is_surface1 = (extension1 == "vtk") || (extension1 == "iso");
			bool is_surface2 = (extension2 == "vtk") || (extension2 == "iso");

			double dsc = 0;

			if (is_surface1 && !is_surface2)
			{
				ImageHelper::PixelType ptype = ImageHelper::ReadPixelType(path2.c_str());
				ComputeASD(path1, path2, ptype);
			}
			else if (!is_surface1 && is_surface2) {
				ImageHelper::PixelType ptype = ImageHelper::ReadPixelType(path1.c_str());
				ComputeASD(path1, path2, ptype);
			}
			else if (!is_surface1 && !is_surface2) {
				ImageHelper::PixelType ptype1 = ImageHelper::ReadPixelType(path1.c_str());
				ImageHelper::PixelType ptype2 = ImageHelper::ReadPixelType(path2.c_str());

				//if (ptype1 != ptype2) {
				//	std::cerr << "input images must have the same pixel type" << std::endl; return -1;
				//}
				ComputeASD(path1, path2, ptype1);
			}
			else {
				std::cerr << "two inputs could not be all surfaces" << std::endl;
				return -1;
			}

		} else if (vm.count("hausdroff")) {

			std::string path1 = BoostCmdHelper::Get<std::string>(vm, "in1");
			std::string path2 = BoostCmdHelper::Get<std::string>(vm, "in2");
			double p = BoostCmdHelper::Get<double>(vm, "p");

			std::string extension1 = stringUtils::extension(path1);
			std::string extension2 = stringUtils::extension(path2);

			bool is_surface1 = (extension1 == "vtk") || (extension1 == "iso");
			bool is_surface2 = (extension2 == "vtk") || (extension2 == "iso");

			double dsc = 0;

			if (is_surface1 && !is_surface2)
			{
				ImageHelper::PixelType ptype = ImageHelper::ReadPixelType(path2.c_str());

				if (std::abs(p - 1) <= VECT_EPSILON)
					ComputeHausdroff(path1, path2, ptype);
				else
					ComputeGeneralizedHausdroff(path1, path2, ptype, p);

			}
			else if (!is_surface1 && is_surface2) {
				ImageHelper::PixelType ptype = ImageHelper::ReadPixelType(path1.c_str());

				if (std::abs(p - 1) <= VECT_EPSILON)
					ComputeHausdroff(path1, path2, ptype);
				else
					ComputeGeneralizedHausdroff(path1, path2, ptype, p);
			}
			else if (!is_surface1 && !is_surface2) {
				ImageHelper::PixelType ptype1 = ImageHelper::ReadPixelType(path1.c_str());
				ImageHelper::PixelType ptype2 = ImageHelper::ReadPixelType(path2.c_str());

				//if (ptype1 != ptype2) {
				//	std::cerr << "input images must have the same pixel type" << std::endl; return -1;
				//}

				if (std::abs(p - 1) <= VECT_EPSILON)
					ComputeHausdroff(path1, path2, ptype1);
				else
					ComputeGeneralizedHausdroff(path1, path2, ptype1, p);
			}
			else {
				std::cerr << "two inputs could not be all surfaces" << std::endl;
				return -1;
			}

		} else if (vm.count("dsc") || vm.count("sen") || vm.count("ppv")) {

			std::string path1 = BoostCmdHelper::Get<std::string>(vm, "in1");
			std::string path2 = BoostCmdHelper::Get<std::string>(vm, "in2");

			std::string extension1 = stringUtils::extension(path1);
			std::string extension2 = stringUtils::extension(path2);

			bool is_surface1 = (extension1 == "vtk") || (extension1 == "iso");
			bool is_surface2 = (extension2 == "vtk") || (extension2 == "iso");

			double dsc = 0;

			if (is_surface1 && !is_surface2)
			{
				ImageHelper::PixelType ptype = ImageHelper::ReadPixelType(path2.c_str());

				if (vm.count("dsc"))
					ComputeDSC(path1, path2, ptype);
				else if (vm.count("sen"))
					ComputeSensitivity(path1, path2, ptype);
				else if (vm.count("ppv"))
					ComputePPV(path1, path2, ptype);
				else
					err_message("unrecognized option");
			}
			else if (!is_surface1 && is_surface2) {
				ImageHelper::PixelType ptype = ImageHelper::ReadPixelType(path1.c_str());

				if (vm.count("dsc"))
					ComputeDSC(path1, path2, ptype);
				else if (vm.count("sen"))
					ComputeSensitivity(path1, path2, ptype);
				else if (vm.count("ppv"))
					ComputePPV(path1, path2, ptype);
				else
					err_message("unrecognized option");
			}
			else if (!is_surface1 && !is_surface2) {
				ImageHelper::PixelType ptype1 = ImageHelper::ReadPixelType(path1.c_str());
				ImageHelper::PixelType ptype2 = ImageHelper::ReadPixelType(path2.c_str());

				if (vm.count("dsc"))
					ComputeDSC(path1, path2, ptype1);
				else if (vm.count("sen"))
					ComputeSensitivity(path1, path2, ptype1);
				else if (vm.count("ppv"))
					ComputePPV(path1, path2, ptype1);
				else
					err_message("unrecognized option");
			}
			else {
				std::cerr << "two inputs could not be all surfaces" << std::endl;
				return -1;
			}

		}
		else if (vm.count("rectumdsc") || vm.count("rectumasd") || vm.count("rectumsen") || vm.count("rectumppv")) {

			std::string path1 = BoostCmdHelper::Get<std::string>(vm, "in1");
			std::string path2 = BoostCmdHelper::Get<std::string>(vm, "in2");

			std::string extension1 = stringUtils::extension(path1);
			std::string extension2 = stringUtils::extension(path2);

			bool is_surface1 = (extension1 == "vtk") || (extension1 == "iso");
			bool is_surface2 = (extension2 == "vtk") || (extension2 == "iso");

			double dsc = 0;

			if (is_surface1 && !is_surface2)
			{
				ImageHelper::PixelType ptype = ImageHelper::ReadPixelType(path2.c_str());
				if (vm.count("rectumdsc"))
					ComputeRectumDSC(path1, path2, ptype);
				else if (vm.count("rectumasd"))
					ComputeRectumASD(path1, path2, ptype);
				else if (vm.count("rectumsen"))
					ComputeRectumSensitivity(path1, path2, ptype);
				else if (vm.count("rectumppv"))
					ComputeRectumPPV(path1, path2, ptype);
				else
					err_message("unrecognized rectum option");
			}
			else if (!is_surface1 && is_surface2) {
				ImageHelper::PixelType ptype = ImageHelper::ReadPixelType(path1.c_str());
				if (vm.count("rectumdsc"))
					ComputeRectumDSC(path1, path2, ptype);
				else if (vm.count("rectumasd"))
					ComputeRectumASD(path1, path2, ptype);
				else if (vm.count("rectumsen"))
					ComputeRectumSensitivity(path1, path2, ptype);
				else if (vm.count("rectumppv"))
					ComputeRectumPPV(path1, path2, ptype);
				else
					err_message("unrecognized rectum option");
			}
			else if (!is_surface1 && !is_surface2) {
				ImageHelper::PixelType ptype1 = ImageHelper::ReadPixelType(path1.c_str());
				ImageHelper::PixelType ptype2 = ImageHelper::ReadPixelType(path2.c_str());

				//if (ptype1 != ptype2) {
				//	std::cerr << "input images must have the same pixel type" << std::endl; return -1;
				//}
				if (vm.count("rectumdsc"))
					ComputeRectumDSC(path1, path2, ptype1);
				else if (vm.count("rectumasd"))
					ComputeRectumASD(path1, path2, ptype1);
				else if (vm.count("rectumsen"))
					ComputeRectumSensitivity(path1, path2, ptype1);
				else if (vm.count("rectumppv"))
					ComputeRectumPPV(path1, path2, ptype1);
				else
					err_message("unrecognized rectum option");
			}
			else {
				std::cerr << "two inputs could not be all surfaces" << std::endl;
				return -1;
			}
		}
		else if (vm.count("anatomy")) {

			std::string path1 = BoostCmdHelper::Get<std::string>(vm, "in1");
			std::string path2 = BoostCmdHelper::Get<std::string>(vm, "in2");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			ComputeDetectionAccuracy(path1, path2, out_path);

		}
		else if (vm.count("landmark")) {

			std::string path1 = BoostCmdHelper::Get<std::string>(vm, "in1");
			std::string path2 = BoostCmdHelper::Get<std::string>(vm, "in2");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			ComputeLandmarkAccuracy(path1, path2, out_path);

		}
		else {
			std::cerr << "option not supported" << std::endl;
			return -1;
		}

	} catch ( boost::program_options::error& exp ) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}