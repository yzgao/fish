//
//  SimpleDM.cpp
//  FISH
//
//  Created by Yaozong Gao on 04/04/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//


#include "stdafx.h"
#include "extern/ImageHelper.h"
#include "extern/BoostHelper.h"
#include "mesh/surface/SurfaceUtils.h"
#include "deform/SimpleDM.h"
#include "deform/deformcontext/ValleyDeformContext.h"
#include "deform/deformcontext/BinaryRegionDeformContext.h"
#include "deform/shapecontext/PCAShapeContext.h"
#include "deform/shapecontext/LinearShapeContext.h"
#include "deform/shapecontext/FreeShapeContext.h"
#include "common/EvaluateTools.h"

using namespace BRIC::IDEA::FISH;

void print_usage()
{
	std::cerr << "[ ASM on Distance Maps ]" << std::endl;
	std::cerr << "SimpleDM --asm_dist --image {path} --pca {path} --init {surface} --patchsize {x y z} --searchrange {value} --iternum {value} [--shape max_var --ratio shape_ratio] --stepsize {value} --tangent {value} --out {path}\n" << std::endl;

	std::cerr << "[ Rigid/Affine/Similarity Deform on Distance Maps ]" << std::endl;
	std::cerr << "SimpleDM {--rigid_dist|--affine_dist|--similarity_dist} --image {path} --init {surf} --patchsize {x y z} --searchrange {value} --iternum {value} --stepsize {value} --tangent {value} --out {path}\n" << std::endl;

	std::cerr << "[ Free Deform on Distance Maps ]" << std::endl;
	std::cerr << "SimpleDM --free_dist --image {path} --init {surf} --patchsize {x y z} --searchrange {value} --iternum {value} --smooth {value} --stepsize {value} --tangent {value} --out {path}\n" << std::endl;

	std::cerr << "[ Fitness of Initialization Using ASD On Distance Maps ]" << std::endl;
	std::cerr << "SimpleDM --eval_dist --image {path} --init {surf}\n" << std::endl;

	std::cerr << "[ Rigid/Affine/Similarity Deform on Binary Maps ]" << std::endl;
	std::cerr << "SimpleDM {--rigid_binary|--affine_binary|--similarity_binary} --image {path} --init {surf} --min {value} --max {value} --searchrange {value} --iternum {value} --stepsize {value} --tangent {value} --stepsize {value} --out {path}\n" << std::endl;

	std::cerr << "[ Free Deform on Binary Maps ]" << std::endl;
	std::cerr << "SimpleDM --free_binary --image {path} --init {surf} --min {value} --max {value} --searchrange {value} --iternum {value} --smooth {value} --stepsize {value} --tangent {value} --out {path}\n" << std::endl;

	std::cerr << "[ find best fitting position that has the maximum probability sum on binary map ]" << std::endl;
	std::cerr << "SimpleDM --bestfit_binary --image {path} --init {surf} --range {10,10,10} --out {surf}\n" << std::endl;
}

double eval_surf_fitness(const Surface& surf, const mxImage<double>& image)
{
	mxImage<unsigned char> carved_image, thresholded_image;
	SurfaceUtils::CarveSurface<double, unsigned char>(surf, image, 0, 255, carved_image);

	vect3<unsigned int> image_size = image.GetImageSize();
	thresholded_image.CopyImageInfo(image);
	thresholded_image.SetImageSize(image_size);

	for (unsigned int z = 0; z < image_size[2]; ++z)
	{
		for (unsigned int y = 0; y < image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < image_size[0]; ++x)
			{
				if (image(x, y, z) < 1)
					thresholded_image(x, y, z) = 255;
				else
					thresholded_image(x, y, z) = 0;
			}
		}
	}

	double fitness = EvaluateTools::DSC<unsigned char, unsigned char>(carved_image, 255, thresholded_image, 255);

	return fitness;
}

template <typename T>
void find_bestfit_binary(const std::string& image_path, const std::string& surf_path, const vect3<float>& range, const std::string& output_path)
{
	// load binary-like map
	mxImage<T> image;
	if (!ImageHelper::ReadImage<T,T>(image_path.c_str(), image))
	{
		std::cerr << "fail to read image from " << image_path << std::endl;
		exit(-1);
	}

	// load initial surface
	Surface surf;
	if (!SurfaceUtils::LoadSurface(surf_path.c_str(), surf))
	{
		std::cerr << "fail to read image from " << surf_path << std::endl;
		exit(-1);
	}

	vect3<double> image_spacing = image.GetSpacing();
	vect3<unsigned int> image_size = image.GetImageSize();

	// compute the bounding box
	vect3<int> range_voxels;
	for (int i = 0; i < 3; ++i)
		range_voxels[i] = static_cast<int>(range[i] / image_spacing[i] + 0.5);

	vect3<int> sp, ep;
	SurfaceUtils::BoundingBox(surf, image, sp, ep);

	for (int i = 0; i < 3; ++i)
	{
		sp[i] -= (range_voxels[i] + 1);
		ep[i] += (range_voxels[i] + 1);

		if (sp[i] < 0)
			sp[i] = 0;
		if (ep[i] >= image_size[i])
			ep[i] = image_size[i] - 1;
	}

	// crop a local image
	mxImage<T> cropImage;
	mxImageUtils::Crop2(image, sp, ep, cropImage, static_cast<T>(0));

	// compute zero-shift mask
	mxImage<unsigned char> maskImage;
	SurfaceUtils::CarveSurface<T,unsigned char>(surf, cropImage, 0, 255, maskImage);

	double max_prob_sum = -std::numeric_limits<double>::max();
	vect3<float> optimal_offset;

	#pragma omp parallel for
	for (int z = -range_voxels[2]; z <= range_voxels[2]; z += 1)
	{
		for (int y = -range_voxels[1]; y <= range_voxels[1]; y += 1)
		{
			for (int x = -range_voxels[0]; x <= range_voxels[0]; x += 1)
			{
				double prob_sum = 0;

				vect3<unsigned int> crop_size = cropImage.GetImageSize();
				for (int pos_z = 0; pos_z < static_cast<int>(crop_size[2]); ++pos_z)
				{
					for (int pos_y = 0; pos_y < static_cast<int>(crop_size[1]); ++pos_y)
					{
						for (int pos_x = 0; pos_x < static_cast<int>(crop_size[0]); ++pos_x)
						{
							vect3<int> from_voxel(pos_x - x, pos_y - y, pos_z - z);

							if (!maskImage.PtInImage(from_voxel.x, from_voxel.y, from_voxel.z))
								continue;

							if (maskImage(from_voxel.x, from_voxel.y, from_voxel.z) > 0)
								prob_sum += cropImage(pos_x, pos_y, pos_z);
						}
					}
				}

				#pragma omp critical
				{
					if (prob_sum > max_prob_sum)
					{
						max_prob_sum = prob_sum;
						optimal_offset = vect3<float>(x, y, z);
					}
				}
			}
		}
	}

	const double* axis = image.GetAxis();
	vect3<float> x_axis(axis[0], axis[1], axis[2]);
	vect3<float> y_axis(axis[3], axis[4], axis[5]);
	vect3<float> z_axis(axis[6], axis[7], axis[8]);

	vect3<float> world_shift = x_axis * optimal_offset[0] * image_spacing[0] + y_axis * optimal_offset[1] * image_spacing[1] + z_axis * optimal_offset[2] * image_spacing[2];

	std::cout << optimal_offset[0] << "," << optimal_offset[1] << "," << optimal_offset[2] << std::endl;
	surf.Translate(world_shift);

	if (!SurfaceUtils::SaveSurface(surf, output_path.c_str()))
	{
		std::cerr << "fail to save surface to file " << output_path << std::endl;
		exit(-1);
	}
}


int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display help information");

		options_description functions("function options");
		functions.add_options()("asm_dist", "active shape model on distance map");
		functions.add_options()("rigid_dist", "rigid deform on distance map");
		functions.add_options()("affine_dist", "affine deform on distance map");
		functions.add_options()("similarity_dist", "similarity deform on distance map");
		functions.add_options()("free_dist", "free deform on distance map");
		functions.add_options()("eval_dist", "evaluate the fitness of one initialization on distance map");

		functions.add_options()("rigid_binary", "rigid deform on binary map");
		functions.add_options()("affine_binary", "affine deform on binary map");
		functions.add_options()("similarity_binary", "similarity deform on binary map");
		functions.add_options()("free_binary", "free deform on binary map");
		functions.add_options()("bestfit_binary", "find the best fit position given shape");

		options_description params("Parameters options");
		params.add_options()("image", value<std::string>(), "image to be segmented");
		params.add_options()("pca", value<std::string>(), "path of PCA shape space");
		params.add_options()("init", value<std::string>(), "path of initial surface");
		params.add_options()("out", value<std::string>(), "output surface path");
		params.add_options()("patchsize", value< std::vector<unsigned int> >()->multitoken(), "patch size");
		params.add_options()("searchrange", value<double>(), "search range" );
		params.add_options()("shape", value<double>()->default_value(3), "max shape variation on each mode");
		params.add_options()("iternum", value<unsigned int>(), "deformation iteration number");
		params.add_options()("stepsize", value<float>()->default_value(0.1), "deformation stepsize");
		params.add_options()("ratio", value<float>()->default_value(1), "ratios for many purposes");
		params.add_options()("smooth", value<unsigned int>(), "smooth iteration (10-15)");
		params.add_options()("min", value<double>(), "minimum boundary intensity");
		params.add_options()("max", value<double>(), "maximum boundary intensity");
		params.add_options()("range", value<std::string>(), "translation range in mm");
		params.add_options()("tangent", value<float>()->default_value(1), "tangent weights (0 to disable)");

		options_description cmd_options;
		cmd_options.add(generic).add(functions).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			std::cerr << cmd_options << std::endl;
			print_usage();
			return -1;
		}

		if (vm.count("asm_dist"))
		{
			std::string image_path = BoostCmdHelper::Get<std::string>(vm, "image");
			std::string pca_path = BoostCmdHelper::Get<std::string>(vm, "pca");
			std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "init");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");
			std::vector<unsigned int> patchsize_ = BoostCmdHelper::Get< std::vector<unsigned int> >(vm, "patchsize");
			vect3<unsigned int> patchsize(patchsize_[0], patchsize_[1], patchsize_[2]);
			double searchrange = BoostCmdHelper::Get<double>(vm, "searchrange");
			double shape_var = BoostCmdHelper::Get<double>(vm, "shape");
			unsigned int iter_num = BoostCmdHelper::Get<unsigned int>(vm, "iternum");
			float stepsize = BoostCmdHelper::Get<float>(vm, "stepsize");
			float tangent = BoostCmdHelper::Get<float>(vm, "tangent");
			float ratio = BoostCmdHelper::Get<float>(vm, "ratio");

			mxImage<double> image;
			if (!ImageHelper::ReadImage<double, double>(image_path.c_str(), image)) {
				std::cerr << "fails to read image from " << image_path << std::endl;
				exit(-1);
			}

			PCAShapeSpace shape_space;
			if (!shape_space.Load(pca_path.c_str())) {
				std::cerr << "fails to load shape space from " << pca_path << std::endl;
				exit(-1);
			}

			Surface surf;
			if (!SurfaceUtils::LoadSurface(surf_path.c_str(), surf)) {
				std::cerr << "fails to load surface from " << surf_path << std::endl;
				exit(-1);
			}

			PCAShapeContext shape_context(shape_space, shape_var, ratio);
			ValleyDeformContext deform_context(image, patchsize, &shape_context);

			SimpleDM_params deform_params;
			deform_params.iter_num = iter_num;
			deform_params.search_range = searchrange;
			deform_params.step_size = stepsize;
			deform_params.tangent_weight = tangent;

			SimpleDM::DeformSurface(&deform_context, deform_params, surf);

			if (!SurfaceUtils::SaveSurface(surf, out_path.c_str())) {
				std::cerr << "fails to save surface to " << out_path << std::endl;
				exit(-1);
			}
		}
		else if (vm.count("rigid_dist") || vm.count("affine_dist") || vm.count("similarity_dist") || vm.count("free_dist"))
		{
			std::string image_path = BoostCmdHelper::Get<std::string>(vm, "image");
			std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "init");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");
			std::vector<unsigned int> patchsize_ = BoostCmdHelper::Get< std::vector<unsigned int> >(vm, "patchsize");
			vect3<unsigned int> patchsize(patchsize_[0], patchsize_[1], patchsize_[2]);
			double searchrange = BoostCmdHelper::Get<double>(vm, "searchrange");
			unsigned int iter_num = BoostCmdHelper::Get<unsigned int>(vm, "iternum");
			float stepsize = BoostCmdHelper::Get<float>(vm, "stepsize");
			float tangent = BoostCmdHelper::Get<float>(vm, "tangent");

			mxImage<double> image;
			if (!ImageHelper::ReadImage<double, double>(image_path.c_str(), image)) {
				std::cerr << "fails to read image from " << image_path << std::endl;
				exit(-1);
			}

			Surface surf;
			if (!SurfaceUtils::LoadSurface(surf_path.c_str(), surf)) {
				std::cerr << "fails to load surface from " << surf_path << std::endl;
				exit(-1);
			}

			SimpleDM_params deform_params;
			deform_params.iter_num = iter_num;
			deform_params.search_range = searchrange;
			deform_params.step_size = stepsize;
			deform_params.tangent_weight = tangent;

			if (vm.count("rigid"))
			{
				LinearShapeContext<PDMRigidTransform> shape_context(surf);
				IDeformContext* deform_context = new ValleyDeformContext(image, patchsize, &shape_context);
				SimpleDM::DeformSurface(deform_context, deform_params, surf);
				delete deform_context;
			}
			else if (vm.count("affine")) 
			{
				LinearShapeContext<PDMAffineTransform> shape_context(surf);
				IDeformContext* deform_context = new ValleyDeformContext(image, patchsize, &shape_context);
				SimpleDM::DeformSurface(deform_context, deform_params, surf);
				delete deform_context;
			}
			else if (vm.count("similarity"))
			{
				LinearShapeContext<PDMSimilarityTransform> shape_context(surf);
				IDeformContext* deform_context = new ValleyDeformContext(image, patchsize, &shape_context);
				SimpleDM::DeformSurface(deform_context, deform_params, surf);
				delete deform_context;
			}
			else if (vm.count("free"))
			{
				unsigned int smooth_iter = BoostCmdHelper::Get<unsigned int>(vm, "smooth");
				FreeShapeContext shape_context(smooth_iter);
				IDeformContext* deform_context = new ValleyDeformContext(image, patchsize, &shape_context);
				SimpleDM::DeformSurface(deform_context, deform_params, surf);
				delete deform_context;
			}
			else {
				std::cerr << "unexpected option" << std::endl;
				exit(-1);
			}

			if (!SurfaceUtils::SaveSurface(surf, out_path.c_str())) {
				std::cerr << "fails to save surface to " << out_path << std::endl;
				exit(-1);
			}
		}
		else if (vm.count("eval_dist")) {

			std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "init");
			std::string img_path = BoostCmdHelper::Get<std::string>(vm, "image");

			mxImage<double> image;
			if (!ImageHelper::ReadImage<double,double>(img_path.c_str(), image)) {
				std::cerr << "fails to read image from " << img_path << std::endl;
				exit(-1);
			}

			Surface surf;
			if (!SurfaceUtils::LoadSurface(surf_path.c_str(), surf)) {
				std::cerr << "fails to load surface from " << surf_path << std::endl;
				exit(-1);
			}

			double fitness = eval_surf_fitness(surf, image);
			std::cout << fitness << std::endl;
		}
		else if (vm.count("rigid_binary") || vm.count("affine_binary") || vm.count("similarity_binary") || vm.count("free_binary")) {

			std::string image_path = BoostCmdHelper::Get<std::string>(vm, "image");
			std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "init");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");
			double min_value = BoostCmdHelper::Get<double>(vm, "min");
			double max_value = BoostCmdHelper::Get<double>(vm, "max");
			double searchrange = BoostCmdHelper::Get<double>(vm, "searchrange");
			unsigned int iter_num = BoostCmdHelper::Get<unsigned int>(vm, "iternum");
			float stepsize = BoostCmdHelper::Get<float>(vm, "stepsize");
			float tangent = BoostCmdHelper::Get<float>(vm, "tangent");

			mxImage<double> image;
			if (!ImageHelper::ReadImage<double, double>(image_path.c_str(), image)) {
				std::cerr << "fails to read image from " << image_path << std::endl;
				exit(-1);
			}

			Surface surf;
			if (!SurfaceUtils::LoadSurface(surf_path.c_str(), surf)) {
				std::cerr << "fails to load surface from " << surf_path << std::endl;
				exit(-1);
			}

			SimpleDM_params deform_params;
			deform_params.iter_num = iter_num;
			deform_params.search_range = searchrange;
			deform_params.step_size = stepsize;
			deform_params.tangent_weight = tangent;
			
			if (vm.count("rigid_binary"))
			{
				LinearShapeContext<PDMRigidTransform> shape_context(surf);
				BinaryRegionDeformContext<double> deform_context(image, &shape_context);
				deform_context.SetBoundaryCriteria(min_value, max_value, -1);
				SimpleDM::DeformSurface(&deform_context, deform_params, surf);
			}
			else if (vm.count("affine_binary"))
			{
				LinearShapeContext<PDMAffineTransform> shape_context(surf);
				BinaryRegionDeformContext<double> deform_context(image, &shape_context);
				deform_context.SetBoundaryCriteria(min_value, max_value, -1);
				SimpleDM::DeformSurface(&deform_context, deform_params, surf);
			}
			else if (vm.count("similarity_binary"))
			{
				LinearShapeContext<PDMSimilarityTransform> shape_context(surf);
				BinaryRegionDeformContext<double> deform_context(image, &shape_context);
				deform_context.SetBoundaryCriteria(min_value, max_value, -1);
				SimpleDM::DeformSurface(&deform_context, deform_params, surf);
			}
			else if (vm.count("free_binary"))
			{
				unsigned int smooth_iter = BoostCmdHelper::Get<unsigned int>(vm, "smooth");
				FreeShapeContext shape_context(smooth_iter);

				BinaryRegionDeformContext<double> deform_context(image, &shape_context);
				deform_context.SetBoundaryCriteria(min_value, max_value, -1);
				SimpleDM::DeformSurface(&deform_context, deform_params, surf);
			}
			else {
				std::cerr << "unexpected option" << std::endl;
				exit(-1);
			}

			if (!SurfaceUtils::SaveSurface(surf, out_path.c_str())) {
				std::cerr << "fails to save surface to " << out_path << std::endl;
				exit(-1);
			}

		} 
		else if (vm.count("bestfit_binary"))
		{
			std::string image_path = BoostCmdHelper::Get<std::string>(vm, "image");
			std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "init");
			std::string range_str = BoostCmdHelper::Get<std::string>(vm, "range");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			std::vector<std::string> tokens;
			stringUtils::Split(range_str, ',', tokens);
			assert_message(tokens.size() >= 3, "range must have three components");

			vect3<float> range;
			for (int i = 0; i < 3; ++i)
				range[i] = stringUtils::str2num<float>(tokens[i]);

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(image_path.c_str());
			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: find_bestfit_binary<unsigned char>(image_path, surf_path, range, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: find_bestfit_binary<char>(image_path, surf_path, range, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: find_bestfit_binary<unsigned short>(image_path, surf_path, range, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: find_bestfit_binary<short>(image_path, surf_path, range, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: find_bestfit_binary<unsigned int>(image_path, surf_path, range, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::INT: find_bestfit_binary<int>(image_path, surf_path, range, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: find_bestfit_binary<unsigned long>(image_path, surf_path, range, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: find_bestfit_binary<long>(image_path, surf_path, range, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: find_bestfit_binary<float>(image_path, surf_path, range, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: find_bestfit_binary<double>(image_path, surf_path, range, output_path); break;
			default: err_message("unrecognized pixel type");  break;
			}
		}
		else {
			std::cerr << "unrecognized options" << std::endl;
			return -1;
		}

	} catch ( boost::program_options::error& exp) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}