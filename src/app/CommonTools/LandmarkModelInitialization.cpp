//
//  LandmarkModelInitialization.cpp
//  FISH
//
//  Created by Yaozong Gao on 9/1/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#include "stdafx.h"

#include <iostream>
#include <omp.h>

#include "mesh/PCAShapeSpace.h"
#include "mesh/PDMUtils.h"
#include "mesh/PDMTransform.h"
#include "common/stringUtils.h"
#include "mesh/LandmarkBasedInitialization.h"
#include "mesh/surface/SurfaceUtils.h"
#include "extern/BoostHelper.h"

using namespace BRIC::IDEA::FISH;

bool LoadLandmarkMap(const char* map_path, const char* lm_path, std::map<int,vect3<float> >& landmarks)
{
	// read landmark id to vertex index mapping
	std::vector< std::string > lines;
	if( !stringUtils::ReadLines(map_path, lines) ) { std::cerr << "fail to read lines" << std::endl; return false; }

	std::map<int,int> idmap;
	for (int i = 0; i < static_cast<int>(lines.size()); ++i)
	{
		if (lines[i].size() < 2)
			continue;
		std::vector<std::string> tokens;
		stringUtils::Split(lines[i], ' ', tokens);
		if (tokens.size() < 2) { std::cerr << "corrupted file" << std::endl; return false; }
		int lm_id = atoi(tokens[0].c_str());
		int vert_id = atoi(tokens[1].c_str());
		idmap[lm_id] = vert_id;
	}

	// read detected landmarks
	lines.clear();
	if( !stringUtils::ReadLines(lm_path, lines) ) { std::cerr << "fail to read lines" << std::endl; return false; }

	for (int i = 0; i < static_cast<int>(lines.size()); ++i)
	{
		if(lines[i].size() < 2)
			continue;
		std::vector< std::string > tokens;
		stringUtils::Split(lines[i], ' ', tokens);
		if(tokens.size() < 7) { std::cerr << "corrupted file" << std::endl; return false; }

		int id = atoi(tokens[0].c_str());
		vect3<float> pt;
		pt[0] = atof(tokens[4].c_str());
		pt[1] = atof(tokens[5].c_str());
		pt[2] = atof(tokens[6].c_str());
		if(idmap.count(id) == 0) { std::cerr << "no corresponding vertex for landmark " << id << std::endl; return false; }

		landmarks[ idmap[id] ] = pt;
	}

	return true;
}

void print_help()
{
	std::cerr << "[ instantiate a statistical shape based on the detected landmarks ]" << std::endl;
	std::cerr << "LandmarkModelInitialization --stat {--rigid|--affine} --pca A --map B --lm C --lambda D --out E" << std::endl;
	std::cerr << "each line of file B is of the following format: vertIdx x y z\n" << std::endl;

	std::cerr << "[ instantiate a mean shape based on the detected landmarks ]" << std::endl;
	std::cerr << "LandmarkModelInitialization --mean {--rigid|--affine} --pca A --map B --lm C --out D" << std::endl;
	std::cerr << "each line of file B is of the following format: vertIdx x y z\n" << std::endl;

	std::cerr << "[ instantiate a shape based on the landmarks ]" << std::endl;
	std::cerr << "LandmarkModelInitialization --align {--rigid|--affine} --srclm A --dstlm B --srcshape C --out D\n" << std::endl;
}

int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display helper information");

		options_description functional("Functional options");
		functional.add_options()("stat", "statistical shape model");
		functional.add_options()("mean", "mean shape model");
		functional.add_options()("align", "algin shape model");

		options_description params("Parameter options");
		params.add_options()("pca", value<std::string>(), "file path for pca shape");
		params.add_options()("map", value<std::string>(), "file that maps landmark id to vertex index");
		params.add_options()("lm", value<std::string>(), "file path for detected landmarks");
		params.add_options()("srclm", value<std::string>(), "source landmark file (complete)");
		params.add_options()("dstlm", value<std::string>(), "destination landmark file (maybe incomplete)");
		params.add_options()("srcshape", value<std::string>(), "source shape model");
		params.add_options()("lambda", value<float>()->default_value(1), "regularization parameter");
		params.add_options()("out", value<std::string>(), "out file path");
		params.add_options()("rigid", "using rigid transformation");
		params.add_options()("affine", "using affine transformation");

		options_description cmd_options;
		cmd_options.add(generic).add(functional).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			std::cerr << cmd_options << std::endl;
			print_help();
			return -1;
		}

		if( vm.count("stat") ) {
			std::string pca_path = BoostCmdHelper::Get<std::string>(vm, "pca");
			std::string map_path = BoostCmdHelper::Get<std::string>(vm, "map");
			std::string lm_path = BoostCmdHelper::Get<std::string>(vm, "lm");
			float lambda = BoostCmdHelper::Get<float>(vm, "lambda");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			std::map<int, vect3<float> > landmarks;
			if( !LoadLandmarkMap(map_path.c_str(), lm_path.c_str(), landmarks) ) {
				std::cerr << "fails to load landmarks from file" << std::endl; return -1;
			}

			PCAShapeSpace ss;
			if( !ss.Load(pca_path.c_str()) ) {
				std::cerr << "fails to read PCA shape from file" << std::endl; return -1;
			}

			PDM init;

			if( vm.count("affine") )
			{
				PDMAffineTransform T;
				LandmarkModelInitialization::Initialize(landmarks, ss, T, lambda, init);
			}else if( vm.count("rigid") ) {
				PDMRigidTransform T;
				LandmarkModelInitialization::Initialize(landmarks, ss, T, lambda, init);
			}else {
				std::cerr << "{--rigid | --affine} option missing" << std::endl; return -1;
			}

			float e = 0;
			Surface initSurf;
			ss.GetSurface(&e, 1, initSurf);
			std::vector< float >& pts = init.GetPoints();
			std::vector< float >& pts_ = initSurf.GetPoints();
			pts_ = pts;

			if( !SurfaceUtils::SaveSurface(initSurf, out_path.c_str()) ) {
				std::cerr << "fail to write surface to file" << std::endl; return -1;
			}
		}else if( vm.count("mean") ) {
			std::string pca_path = BoostCmdHelper::Get<std::string>(vm, "pca");
			std::string map_path = BoostCmdHelper::Get<std::string>(vm, "map");
			std::string lm_path = BoostCmdHelper::Get<std::string>(vm, "lm");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			std::map<int, vect3<float> > landmarks;
			if( !LoadLandmarkMap(map_path.c_str(), lm_path.c_str(), landmarks) ) {
				std::cerr << "fails to load landmarks from file" << std::endl; return -1;
			}

			PCAShapeSpace ss;
			if( !ss.Load(pca_path.c_str()) ) {
				std::cerr << "fails to read PCA shape from file" << std::endl; return -1;
			}

			PDM init, mean, target;
			const std::vector<float>& mean_verts = ss.GetMean();
			std::vector<float>& mean_points = mean.GetPoints();
			std::vector<float>& target_points = target.GetPoints();

			mean_points.reserve(landmarks.size()*3);
			target_points.reserve(landmarks.size()*3);

			for (std::map< int,vect3<float> >::const_iterator it = landmarks.begin(); it != landmarks.end(); ++it) {
				int vert_idx = it->first;

				mean_points.push_back(mean_verts[vert_idx*3]);
				mean_points.push_back(mean_verts[vert_idx*3+1]);
				mean_points.push_back(mean_verts[vert_idx*3+2]);

				target_points.push_back(it->second[0]);
				target_points.push_back(it->second[1]);
				target_points.push_back(it->second[2]);
			}

			if( vm.count("affine") ) {

				float e = 0;
				PDM mean_pdm;
				ss.GetPDM(&e, 1, mean_pdm);

				PDMAffineTransform T;
				T.Estimate(mean, target);
				T.Apply(mean_pdm, init);

			}else if( vm.count("rigid") ) {

				float e = 0;
				PDM mean_pdm;
				ss.GetPDM(&e, 1, mean_pdm);

				PDMRigidTransform T;
				T.Estimate(mean, target);
				T.Apply(mean_pdm, init);

			}else {
				std::cerr << "{--rigid | --affine} option missing" << std::endl; return -1;
			}

			float e = 0;
			Surface initSurf;
			ss.GetSurface(&e, 1, initSurf);
			std::vector< float >& pts = init.GetPoints();
			std::vector< float >& pts_ = initSurf.GetPoints();
			pts_ = pts;

			if( !SurfaceUtils::SaveSurface(initSurf, out_path.c_str()) ) {
				std::cerr << "fail to write surface to file" << std::endl; return -1;
			}

		} else if(vm.count("align")) {

			std::string srclm_path = BoostCmdHelper::Get<std::string>(vm, "srclm");
			std::string dstlm_path = BoostCmdHelper::Get<std::string>(vm, "dstlm");
			std::string srcshape_path = BoostCmdHelper::Get<std::string>(vm, "srcshape");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			std::map< int, vect3<float> > src_landmarks, dst_landmarks;
			if (!PDMUtils::ReadLandmarksFromTxt(srclm_path.c_str(), src_landmarks)) {
				std::cerr << "fails to read landmarks from " << srclm_path << std::endl; return -1;
			}
			if (!PDMUtils::ReadLandmarksFromTxt(dstlm_path.c_str(), dst_landmarks)) {
				std::cerr << "fails to read landmarks from " << dstlm_path << std::endl; return -1;
			}

			std::vector<int> common_lm_ids;
			std::map<int, vect3<float> >::const_iterator it = dst_landmarks.begin();
			while (it != dst_landmarks.end())
			{
				if (src_landmarks.count(it->first) != 0)
					common_lm_ids.push_back(it->first);
				++it;
			}

			PDM src_pdm, dst_pdm;
			std::vector<float>& src_pts = src_pdm.GetPoints();
			std::vector<float>& dst_pts = dst_pdm.GetPoints();
			src_pts.resize(common_lm_ids.size() * 3);
			dst_pts.resize(common_lm_ids.size() * 3);
			for (size_t i = 0; i < common_lm_ids.size(); ++i)
			{
				int lm_id = common_lm_ids[i];
				src_pts[i * 3] = src_landmarks[lm_id][0];
				src_pts[i * 3 + 1] = src_landmarks[lm_id][1];
				src_pts[i * 3 + 2] = src_landmarks[lm_id][2];

				dst_pts[i * 3] = dst_landmarks[lm_id][0];
				dst_pts[i * 3 + 1] = dst_landmarks[lm_id][1];
				dst_pts[i * 3 + 2] = dst_landmarks[lm_id][2];
			}

			Surface src_shape, dst_shape;
			if (!SurfaceUtils::LoadSurface(srcshape_path.c_str(), src_shape)) {
				std::cerr << "fails to load surface from " << srcshape_path << std::endl; return -1;
			}
			dst_shape.CopyFace(src_shape);

			if (vm.count("rigid"))
			{
				PDMRigidTransform T;
				T.Estimate(src_pdm, dst_pdm);
				T.Apply(src_shape, dst_shape);
			}
			else if (vm.count("affine"))
			{
				PDMAffineTransform T;
				T.Estimate(src_pdm, dst_pdm);
				T.Apply(src_shape, dst_shape);
			}
			else {
				std::cerr << "please specify the transformation type" << std::endl; return -1;
			}

			if (!SurfaceUtils::SaveSurface(dst_shape, out_path.c_str())) {
				std::cerr << "fail to write surface to " << out_path << std::endl; return -1;
			}

		} else {
			std::cerr << "option not supported" << std::endl;
			return -1;
		}

	} catch ( boost::program_options::error& exp ) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}