
#include "stdafx.h"
#include "mesh/surface/SurfaceUtils.h"
#include "mesh/PDMTransform.h"
#include "mesh/PDMUtils.h"
#include "mesh/PCAShapeSpace.h"
#include "extern/BoostHelper.h"
#include "common/mxImageUtils.h"
#include "extern/ImageHelper.h"
#include "common/aux_func.h"

#include <fstream>
#include <iostream>

using namespace BRIC::IDEA::FISH;

typedef unsigned short TPixel;

void print_help()
{
	std::cerr << "[ generate surface mesh from image ]" << std::endl;
	std::cerr << "meshTools --isosurface --in {imageA} --spacing {spacingB} --isoval {valC} --smooth {value} --out {meshD.vtk}\n" << std::endl;

	std::cerr << "[ estimate surface transformation ]" << std::endl;
	std::cerr << "meshTools --estimate {--affine|--rigid|--similarity} --in {surfA | txt} --ref {surfB | txt} [--out surfC] [--outT transformD] \n" << std::endl;

	std::cerr << "[ surface reordering ]" << std::endl;
	std::cerr << "meshTools --reorder --in surfA --out surfB\n" << std::endl;

	std::cerr << "[ mean surface estimation ]" << std::endl;
	std::cerr << "meshTools --mean {--affine|--rigid|--similarity} --prefix A --start B --end C --postfix D --out surfE" << std::endl;
	std::cerr << "meshTools --mean {--affine|--rigid|--similarity} --file shapelistA --p B --out C\n" << std::endl;

	std::cerr << "[ mean surface estimation together with mean landmarks ]" << std::endl;
	std::cerr << "meshTools --mean2 {--affine|--rigid|--similarity} --prefix A --start B --end C --postfix D --prefix2 E --postfix2 F --out meanSurf --out2 meanLMs" << std::endl;
	std::cerr << "meshTools --mean2 {--affine|--rigid|--similarity} --file shapelist --file2 lmlist --out meanSurf --out2 meanLMs\n" << std::endl;

	std::cerr << "[ PCA on surfaces ]" << std::endl;
	std::cerr << "meshTools --pca {--affine|--rigid|--similarity} --prefix A --start B --end C --postfix D --p E --out F" << std::endl;
	std::cerr << "meshTools --pca {--affine|--rigid|--similarity} --file shapelistA --p B --out C\n" << std::endl;

	std::cerr << "[ surface type conversion ]" << std::endl;
	std::cerr << "meshTools --type --in surfA --out surfB\n" << std::endl;

	std::cerr << "[ linear surface transformation ]" << std::endl;
	std::cerr << "meshTools --apply --in surfA --init B --out surfC\n" << std::endl;

	std::cerr << "[ landmark extraction ]" << std::endl;
	std::cerr << "meshTools --landmark --id A --prefix B [--prefix2 B2] --start C --end D --postfix E [--prefix2 E2] --out F\n";
	std::cerr << "Parameters:" << std::endl;   
	std::cerr << "    id: 0 center; 1 median-left; 2 median-right; 3 median-up; 4 median-down; 5 base-center; 6 apex-center" << std::endl;
	std::cerr << "    prefix2: prefix for intensity image, specified only if prefix2 != prefix" << std::endl;
	std::cerr << "    postfix2: postfix for intensity image, specified only if postfix2 != postfix\n" << std::endl;

	std::cerr << "[ rectum landmark extraction ]" << std::endl;
	std::cerr << "meshTools --reclm --id A --prefix B [--prefix2 B2] --start C --end D --postfix E [--prefix2 E2] --out F" << std::endl;
	std::cerr << "See documentation above.\n" << std::endl;

	std::cerr << "[ bladder bounding box landmark extraction ]" << std::endl;
	std::cerr << "meshTools --blalm --id A --prefix B [--prefix2 B2] --start C --end D --postfix E [--prefix2 E2] --out F\n" << std::endl;

	std::cerr << "[ closest vertex ]" << std::endl;
	std::cerr << "meshTools --closestvertex --in {surfA} --point worldx worldy worldz\n" << std::endl; 

	std::cerr << "[ get vertex coordinate ]" << std::endl;
	std::cerr << "meshTools --coord --in {surfA} --id {idB}\n" << std::endl;

	std::cerr << "[ carve surface onto image ]" << std::endl;
	std::cerr << "meshTools --carve --in {surfA} --ref {imgB} --out {imgC}\n" << std::endl;

	std::cerr << "[ translate the mesh ]" << std::endl;
	std::cerr << "meshTools --shift --in {surfA} --vector dx dy dz --out {outC}\n" << std::endl;

	std::cerr << "[ map shapes into PCA coefficients ]" << std::endl;
	std::cerr << "meshTools --map {--affine|--rigid|--similarity} --prefix A --start B --end C --postfix D --shapespace shapeSpace --comp number --out txtFile" << std::endl;
	std::cerr << "meshTools --map {--affine|--rigid|--similarity} --file shapeList --shapespace shapeSpace --comp number --out txtFile\n" << std::endl;

	std::cerr << "[ fuse all shapes onto the image using majority voting ]" << std::endl;
	std::cerr << "meshTools --fuse --prefix A --start B --end C --postfix D --ref image --out image\n" << std::endl;

	std::cerr << "[ get a binary mask that covers the 3D mesh with padding ]" << std::endl;
	std::cerr << "meshTools --boundmask --in {surf} --ref {image} --padsize {mm} --out {image}\n" << std::endl;

	std::cerr << "[ dilate/shrink the mesh along normal direction]" << std::endl;
	std::cerr << "meshTools --morph --in {surf} --size {value} --out {surf}\n" << std::endl;

	std::cerr << "[ flip the mesh along x/y/z axis]" << std::endl;
	std::cerr << "meshTools {--flipx | --flipy | --flipz} --in {surf} --out {surf}\n" << std::endl;

	std::cerr << "[ remesh to make vertex uniformly distributed ]" << std::endl;
	std::cerr << "meshTools --remesh --in {surf} [--stepsize 0.5 --iter 300] --out {surf}\n" << std::endl;
}

// forward declaration
void extract_landmark(int id, std::string inten_prefix, std::string inte_postfix, std::string seg_prefix, std::string seg_postfix, int start, int end, std::string out);

void extract_rectum_landmarks(int id, std::string inten_prefix, std::string inten_postfix, std::string seg_prefix, std::string seg_postfix, int start, int end, std::string out);

void extract_bladder_landmarks(int id, std::string inten_prefix, std::string inten_postfix, std::string seg_prefix, std::string seg_postfix, int start, int end, std::string out);

void surface_registration(Surface& mov_surface, Surface& ref_surface, boost::program_options::variables_map& vm)
{
	if( mov_surface.GetPoints().size() != ref_surface.GetPoints().size() )
	{
		std::cerr << "num of points needs to be consistent" << std::endl;
		exit(-1);
	}

	Surface out_surface;
	if( vm.count("affine") )
	{
		PDMAffineTransform T;
		T(mov_surface, ref_surface, out_surface);

		if (vm.count("outT"))
		{
			std::string out_transform_path = vm["outT"].as<std::string>();
			if (!T.Save(out_transform_path.c_str()))
			{
				std::cerr << "fail to output transformation to " << out_transform_path << std::endl;
				exit(-1);
			}
		}

	}else if( vm.count("rigid") )
	{
		PDMRigidTransform T;
		T(mov_surface, ref_surface, out_surface);

		if (vm.count("outT"))
		{
			std::string out_transform_path = vm["outT"].as<std::string>();
			if (!T.Save(out_transform_path.c_str()))
			{
				std::cerr << "fail to output transformation to " << out_transform_path << std::endl;
				exit(-1);
			}
		}
	}
	else if ( vm.count("similarity") )
	{
		PDMSimilarityTransform T;
		T(mov_surface, ref_surface, out_surface);

		if (vm.count("outT"))
		{
			std::string out_transform_path = vm["outT"].as<std::string>();
			if (!T.Save(out_transform_path.c_str()))
			{
				std::cerr << "fail to output transformation to " << out_transform_path << std::endl;
				exit(-1);
			}
		}
	}
	else {
		std::cerr << "undefined transformation " << std::endl; exit(-1);
	}

	std::vector< vect3<int> >& faces = out_surface.GetFaces();
	std::vector< vect3<int> >& refFaces = ref_surface.GetFaces();
	faces = refFaces;

	if (vm.count("out"))
	{
		std::string out_surface_path = vm["out"].as<std::string>();
		if (!SurfaceUtils::SaveSurface(out_surface, out_surface_path.c_str())) { 
			std::cerr << "fail to save surface to " << out_surface_path << std::endl;
			exit(-1);
		}
	}
}

void pdm_registration(PDM& mov_pdm, PDM& ref_pdm, boost::program_options::variables_map& vm)
{
	if( mov_pdm.GetPoints().size() != ref_pdm.GetPoints().size() ) {
		std::cerr << "num of points needs to be consistent" << std::endl;
		exit(-1);
	}

	PDM new_mov, new_ref;
	std::vector<float>& new_mov_pts = new_mov.GetPoints();
	std::vector<float>& new_ref_pts = new_ref.GetPoints();

	unsigned int point_num = mov_pdm.GetPoints().size() / 3;
	const std::vector<float>& mov_pts = mov_pdm.GetPoints();
	const std::vector<float>& ref_pts = ref_pdm.GetPoints();

	for (unsigned int i = 0 ; i < point_num; ++i)
	{
		if ( static_cast<float>(mov_pts[i*3]+1) < 1e-6 && static_cast<float>(mov_pts[i*3+1]+1) < 1e-6 &&
			 static_cast<float>(mov_pts[i*3+2]+1) < 1e-6 )
			continue;
		if ( static_cast<float>(ref_pts[i*3]+1) < 1e-6 && static_cast<float>(ref_pts[i*3+1]+1) < 1e-6 &&
			 static_cast<float>(ref_pts[i*3+2]+1) < 1e-6 )
			continue;

		new_mov_pts.push_back(mov_pts[i*3]);
		new_mov_pts.push_back(mov_pts[i*3+1]);
		new_mov_pts.push_back(mov_pts[i*3+2]);

		new_ref_pts.push_back(ref_pts[i*3]);
		new_ref_pts.push_back(ref_pts[i*3+1]);
		new_ref_pts.push_back(ref_pts[i*3+2]);
	}

	PDM out_pdm;
	if( vm.count("affine") )
	{
		PDMAffineTransform T;
		T(new_mov, new_ref, out_pdm);

		if (vm.count("outT"))
		{
			std::string out_transform_path = vm["outT"].as<std::string>();
			if (!T.Save(out_transform_path.c_str()))
			{
				std::cerr << "fail to output transformation to " << out_transform_path << std::endl;
				exit(-1);
			}
		}

	}else if( vm.count("rigid") )
	{
		PDMRigidTransform T;
		T(new_mov, new_ref, out_pdm);

		if (vm.count("outT"))
		{
			std::string out_transform_path = vm["outT"].as<std::string>();
			if (!T.Save(out_transform_path.c_str()))
			{
				std::cerr << "fail to output transformation to " << out_transform_path << std::endl;
				exit(-1);
			}
		}
	}
	else if (vm.count("similarity"))
	{
		PDMSimilarityTransform T;
		T(new_mov, new_ref, out_pdm);

		if (vm.count("outT"))
		{
			std::string out_transform_path = vm["outT"].as<std::string>();
			if (!T.Save(out_transform_path.c_str()))
			{
				std::cerr << "fail to output transformation to " << out_transform_path << std::endl;
				exit(-1);
			}
		}
	}
	else {
		std::cerr << "undefined transformation" << std::endl; exit(-1);
	}

	if (vm.count("out"))
	{
		std::string out_pdm_path = vm["out"].as<std::string>();
		if (!PDMUtils::WritePDMtoTxt(out_pdm, out_pdm_path.c_str()))
		{
			std::cerr << "fail to output pdm to " << out_pdm_path << std::endl;
			exit(-1);
		}
	}else {
		const std::vector<float>& out_pts = out_pdm.GetPoints();
		unsigned int pt_num = out_pts.size() / 3;

		std::cout << "moving point set: "<< std::endl;
		for (unsigned int i = 0; i < pt_num; ++i) 
			std::cout << new_mov_pts[i*3] << "," << new_mov_pts[i*3+1] << "," << new_mov_pts[i*3+2] << std::endl;
		std::cout << "\nreference point set: " << std::endl;
		for (unsigned int i = 0; i < pt_num; ++i)
			std::cout << new_ref_pts[i*3] << "," << new_ref_pts[i*3+1] << "," << new_ref_pts[i*3+2] << std::endl;
		std::cout << "\naligned point set:" << std::endl;
		for (unsigned int i = 0; i < pt_num; ++i)
			std::cout << out_pts[i*3] << "," << out_pts[i*3+1] << "," << out_pts[i*3+2] << std::endl;
	}
}

void load_pdms(std::string prefix, int start, int end, std::string postfix, std::vector<PDM*>& pdms)
{
	pdms.resize( end - start + 1 );
	for(int i = start; i <= end; ++i)
	{
		Surface* surf = new Surface;
		char surf_path[2048] = {0};
		sprintf(surf_path, "%s%d%s", prefix.c_str(), i, postfix.c_str());
		if( !SurfaceUtils::LoadSurface(surf_path, *surf) ) {
			std::cerr << "cannot load surface from " << surf_path << std::endl; exit(-1);
		}
		pdms[i-start] = surf;
	}
}

void load_surfaces(std::string prefix, int start, int end, std::string postfix, std::vector<Surface*>& surfs) 
{
	for (int i = start; i <= end; ++i)
	{
		Surface* surf = new Surface;
		char surf_path[2048] = { 0 };
		sprintf(surf_path, "%s%d%s", prefix.c_str(), i, postfix.c_str());
		if (!SurfaceUtils::LoadSurface(surf_path, *surf)) {
			continue;
		}
		surfs.push_back(surf);
		std::cout << "Loaded " << surf_path << std::endl;
	}
	std::cout << std::endl;
}

void load_pdms2(std::string path, std::vector<PDM*>& pdms)
{
	std::vector<std::string> lines;
	if(!stringUtils::ReadLines(path.c_str(), lines)) {
		std::cerr << "fails to read string from " << path << std::endl; exit(-1);
	}

	for (unsigned int i = 0; i < lines.size(); ++i)
	{
		std::string line = lines[i];
		if (line.length() < 2)
			continue;
		Surface* surf = new Surface;
		if ( !SurfaceUtils::LoadSurface(line.c_str(), *surf) ) {
			std::cerr << "cannot load surface from " << line << std::endl; exit(-1);
		}
		pdms.push_back(surf);
	}

}

template <typename T>
void isosurface(std::string in_path, double spacing, double smooth_sigma, int isoval, std::string out_path)
{
	mxImage<T> image;
	if (!ImageHelper::ReadImage<T,T>(in_path.c_str(), image)) {
		std::cerr << "cannot read image from " << in_path << std::endl;
		exit(-1);
	}

	ImageLinearInterpolator<T, T> interpolator;
	mxImage<T> isoImage;
	mxImageUtils::ResampleWithInterpolator(image, vect3<double>(spacing, spacing, spacing), interpolator, isoImage);

	mxImage<float> smoothImage;
	if (smooth_sigma > 0)
		mxImageUtils::GaussianSmooth(isoImage, smoothImage, smooth_sigma);
	else
		mxImageUtils::Copy(isoImage, smoothImage);

	// bug fixer to fix the problem of open surface in case the ground-truth segmentation touch the image boundary
	mxImage<float> padImage;
	vect3<unsigned int> padImageSize = smoothImage.GetImageSize();
	for (int i = 0; i < 3; ++i)
		padImageSize[i] += 2;

	vect3<double> padImageSpacing = smoothImage.GetSpacing();
	vect3<double> padImageOrigin = smoothImage.GetOrigin();
	for (int i = 0; i < 3; ++i)
		padImageOrigin[i] -= padImageSpacing[i];

	padImage.CopyImageInfo(smoothImage);
	padImage.SetImageSize(padImageSize);
	padImage.SetOrigin(padImageOrigin);
	padImage.Fill(0);

	vect3<unsigned int> smoothImageSize = smoothImage.GetImageSize();
	for (unsigned int z = 0; z < smoothImageSize[2]; ++z)
	{
		for (unsigned int y = 0; y < smoothImageSize[1]; ++y)
		{
			for (unsigned int x = 0; x < smoothImageSize[0]; ++x)
			{
				padImage(x + 1, y + 1, z + 1) = smoothImage(x, y, z);
			}
		}
	}
	// end bug fixer

	Surface surf;
	SurfaceUtils::IsoSurfaceFromImage(padImage, surf, static_cast<float>(isoval));

	if (!SurfaceUtils::SaveSurface(surf, out_path.c_str())) {
		std::cerr << "cannot write surface to " << out_path << std::endl;
		exit(-1);
	}
}

void map_shapes(std::vector<PDM*>& pdms, std::string shapespace_path, int comp_num, std::string outfile, boost::program_options::variables_map& vm)
{
	// load shape space
	PCAShapeSpace ss;
	if (!ss.Load(shapespace_path.c_str())) {
		std::cerr << "fails to load shape space from disk" << std::endl; exit(-1);
	}

	PDM mean;
	float e = 0;
	ss.GetPDM(&e, 1, mean);

	// align shapes onto the mean shape space
	if (vm.count("affine"))
	{
		PDMAffineTransform T;
		for (size_t i = 0; i < pdms.size(); ++i)
			T(*pdms[i], mean, *pdms[i]);
	}
	else if (vm.count("rigid"))
	{
		PDMRigidTransform T;
		for (size_t i = 0; i < pdms.size(); ++i)
			T(*pdms[i], mean, *pdms[i]);
	}
	else if (vm.count("similarity"))
	{
		PDMSimilarityTransform T;
		for (size_t i = 0; i < pdms.size(); ++i)
			T(*pdms[i], mean, *pdms[i]);
	}
	else {
		std::cerr << "undefined transform" << std::endl; exit(-1);
	}

	std::ofstream out(outfile.c_str());
	if (!out) {
		std::cerr << "fails to open file for write: " << outfile << std::endl; return;
	}

	for (size_t i = 0; i < pdms.size(); ++i)
	{
		std::vector<float> coeffs;
		ss.GetCoefficients(*(pdms[i]), comp_num, coeffs);
		
		for (size_t j = 0; j < coeffs.size(); ++j)
		{
			if (j == 0)
				out << coeffs[j];
			else
				out << " " << coeffs[j];
		}
		out << std::endl;
	}
}

template <typename T>
void fuse_shapes_onto_image(const std::vector<Surface*>& surfaces, std::string refImagePath, mxImage<double>& outImage)
{
	mxImage<T> image;
	if (!ImageHelper::ReadImage<T,T>(refImagePath.c_str(), image)) {
		std::cerr << "fail to read image from " << refImagePath << std::endl;
		exit(-1);
	}

	vect3<unsigned int> image_size = image.GetImageSize();

	mxImage<double> temp;
	for (size_t i = 0; i < surfaces.size(); ++i) 
	{
		SurfaceUtils::CarveSurface<T,double>(*(surfaces[i]), image, 0, 255, temp);

		if (i == 0)
		{
			outImage.CopyImageInfo(image);
			outImage.SetImageSize(image_size);
			outImage.Fill(0);
		}

		for (unsigned int z = 0; z < image_size[2]; ++z) {
			for (unsigned int y = 0; y < image_size[1]; ++y) {
				for (unsigned int x = 0; x < image_size[0]; ++x) {
					outImage(x, y, z) += temp(x, y, z);
				}
			}
		}
	}

	for (unsigned int z = 0; z < image_size[2]; ++z) {
		for (unsigned int y = 0; y < image_size[1]; ++y) {
			for (unsigned int x = 0; x < image_size[0]; ++x) {
				double val = outImage(x,y,z);
				val = val / surfaces.size();
				if (val >= 128) val = 255;
				else val = 0;
				outImage(x, y, z) = val;
			}
		}
	}
}

template <typename T>
void bounding_box_mask(const std::string& surf_path, const std::string& ref_image_path, double padsize, const std::string& out_path)
{
	mxImage<T> ref_image;
	if (!ImageHelper::ReadImage<T,T>(ref_image_path.c_str(), ref_image))
	{
		std::cerr << "fail to read image from file " << ref_image_path << std::endl;
		exit(-1);
	}

	Surface surf;
	if (!SurfaceUtils::LoadSurface(surf_path.c_str(),surf))
	{
		std::cerr << "fail to load surface from file " << surf_path << std::endl;
		exit(-1);
	}

	vect3<int> sp, ep;
	SurfaceUtils::BoundingBox(surf, ref_image, sp, ep);

	vect3<double> spacing = ref_image.GetSpacing();
	vect3<int> padvoxels;
	for (int i = 0; i < 3; ++i)
		padvoxels[i] = static_cast<int>(padsize / spacing[i] + 0.5);

	for (int i = 0; i < 3; ++i)
	{
		sp[i] -= padvoxels[i];
		ep[i] += padvoxels[i];

		if (sp[i] < 0)
			sp[i] = 0;

		if (ep[i] >= ref_image.GetImageSize()[i])
			ep[i] = ref_image.GetImageSize()[i] - 1;
	}

	mxImage<unsigned char> mask_image;
	mask_image.CopyImageInfo(ref_image);
	mask_image.SetImageSize(ref_image.GetImageSize());
	mask_image.Fill(0);

	for (int z = sp[2]; z <= ep[2]; ++z)
		for (int y = sp[1]; y <= ep[1]; ++y)
			for (int x = sp[0]; x <= ep[0]; ++x)
				mask_image(x, y, z) = 255;
	
	if (!ImageHelper::WriteImage<unsigned char,unsigned char>(mask_image, out_path.c_str()))
	{
		std::cerr << "fail to write image to file " << out_path << std::endl;
		exit(-1);
	}
}


int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display helper information");

		options_description functional("Functional options (must pick one)");
		functional.add_options()("isosurface", "generate isosurface from image");
		functional.add_options()("estimate", "estimate transformation");
		functional.add_options()("reorder", "reorder the vertices");
		functional.add_options()("mean", "mean shape using affine registration");
		functional.add_options()("mean2", "mean shape with mean landmarks using affine registration");
		functional.add_options()("pca", "PCA analysis of shapes");
		functional.add_options()("type", "mesh type conversion");
		functional.add_options()("apply", "apply a linear transformation");
		functional.add_options()("landmark", "generate a landmark annotation file");
		functional.add_options()("reclm", "generate rectum landmark annotation file");
		functional.add_options()("blalm", "generate bladder landmark annotation file");
		functional.add_options()("closestvertex", "find the closest vertex on the surface");
		functional.add_options()("carve", "carve a surface onto the image");
		functional.add_options()("coord", "get vertex coordinate");
		functional.add_options()("shift", "translate a mesh");
		functional.add_options()("map", "map shapes into PCA coefficients");
		functional.add_options()("fuse", "fuse shapes onto the image");
		functional.add_options()("boundmask", "compute binary box mask that covers the mesh");
		functional.add_options()("morph", "mesh morphological operations");
		functional.add_options()("flipx", "flip mesh along x direction");
		functional.add_options()("flipy", "flip mesh along y direction");
		functional.add_options()("flipz", "flip mesh along z direction");
		functional.add_options()("remesh", "remesh to improve the uniformity of vertex distribution");

		options_description params("Parameter options");
		params.add_options()("in", value<std::string>(), "input/moving surface/image");
		params.add_options()("ref", value<std::string>(), "reference/fixed surface/image");
		params.add_options()("out", value<std::string>(), "output surface");
		params.add_options()("out2", value<std::string>(), "second output");
		params.add_options()("outT", value<std::string>(), "output transformation");
		params.add_options()("prefix", value<std::string>(), "prefix");
		params.add_options()("postfix", value<std::string>(), "postfix");
		params.add_options()("start", value<int>(), "start index (usually with prefix and postfix)");
		params.add_options()("end", value<int>(), "end index (usually with prefix and postfix)");
		params.add_options()("p", value<float>(), "variations percentage to keep");
		params.add_options()("init", value<std::string>(), "linear transformation txt");
		params.add_options()("id", value<int>(), "landmark/vertex id (0-6)");
		params.add_options()("prefix2", value<std::string>(), "prefix2 (e.g., prefix for intensity image / landmarks)");
		params.add_options()("postfix2", value<std::string>(), "postfix2 (e.g., postfix for intensity image / landmarks)");
		params.add_options()("point", value< std::vector<float> >()->multitoken(), "specify the coordinate ");
		params.add_options()("file", value<std::string>(), "specify a file containing paths of shapes");
		params.add_options()("file2", value<std::string>(), "specify another file containing paths of landmarks");
		params.add_options()("vector", value< std::vector<float> >()->multitoken(), "specify a vector");
		params.add_options()("spacing", value<double>(), "image spacing, scalar value");
		params.add_options()("isoval", value<int>(), "isosurface value");
		params.add_options()("shapespace", value<std::string>(), "path for PCA shape space");
		params.add_options()("comp", value<int>(), "number of components");
		params.add_options()("affine", "affine registration");
		params.add_options()("rigid", "rigid registration");
		params.add_options()("similarity", "similarity registration");
		params.add_options()("padsize", value<double>(), "pad size in mm");
		params.add_options()("size", value<double>(), "size in mm");
		params.add_options()("smooth", value<double>()->default_value(-1), "smooth kernel sigma in voxels");
		params.add_options()("stepsize", value<float>(), "step size");
		params.add_options()("iter", value<int>(), "iteration number");

		options_description cmd_options;
		cmd_options.add(generic).add(functional).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1)
		{
			std::cerr << cmd_options << std::endl;
			print_help();
			return -1;
		}

		if (vm.count("estimate"))
		{
			std::string input_path, ref_path;
			if (vm.count("in"))
			{
				input_path = vm["in"].as<std::string>();
			}
			else {
				std::cerr << "in option missing" << std::endl;
				return -1;
			}

			if (vm.count("ref"))
			{
				ref_path = vm["ref"].as<std::string>();
			}
			else {
				std::cerr << "ref option missing" << std::endl;
				return -1;
			}

			boost::filesystem::path inpath(input_path), refpath(ref_path);
			if (inpath.extension().string() != refpath.extension().string()) {
				std::cerr << "input PDM and reference PDM should have the same extension" << std::endl;
				return -1;
			}

			if (inpath.extension().string() == std::string(".vtk") || inpath.extension().string() == std::string(".iso")) {

				Surface input_surface, ref_surface, out_surface;
				if (!SurfaceUtils::LoadSurface(input_path.c_str(), input_surface)) {
					std::cerr << "fail to load input surface from " << input_path << std::endl;
					return -1;
				}
				if (!SurfaceUtils::LoadSurface(ref_path.c_str(), ref_surface)) {
					std::cerr << "fail to load reference surface from " << ref_path << std::endl;
					return -1;
				}
				surface_registration(input_surface, ref_surface, vm);

			}
			else if (inpath.extension().string() == std::string(".txt")) {

				std::map<int, vect3<float> > input_lms, ref_lms;
				if (!PDMUtils::ReadLandmarksFromTxt(inpath.string().c_str(), input_lms)) {
					std::cerr << "fails to read landmarks from " << inpath.string() << std::endl; return -1;
				}
				if (!PDMUtils::ReadLandmarksFromTxt(refpath.string().c_str(), ref_lms)) {
					std::cerr << "fails to read PDM from " << refpath.string() << std::endl; return -1;
				}

				std::vector<int> keys;
				typedef std::map< int, vect3<float> >::const_iterator iterator_type;
				iterator_type it = ref_lms.begin();
				while (it != ref_lms.end())
				{
					if (input_lms.count(it->first) > 0)
						keys.push_back(it->first);
					++it;
				}

				if (keys.size() <= 3) {
					std::cerr << "number of common landmarks between ref and input is too small (<=3)" << std::endl;
					return -1;
				}

				PDM input_pdm, ref_pdm;
				std::vector<float>& input_pts = input_pdm.GetPoints();
				std::vector<float>& ref_pts = ref_pdm.GetPoints();
				input_pts.resize(keys.size() * 3);
				ref_pts.resize(keys.size() * 3);

				std::cout << "[ ";
				for (size_t i = 0; i < keys.size(); ++i) {
					std::cout << keys[i] << " ";
					for (int j = 0; j < 3; ++j) {
						input_pts[i * 3 + j] = input_lms[keys[i]][j];
						ref_pts[i * 3 + j] = ref_lms[keys[i]][j];
					}
				}
				std::cout << "]" << std::endl;

				pdm_registration(input_pdm, ref_pdm, vm);

			}
			else {
				std::cerr << "file extension not recognized" << std::endl;
				return -1;
			}

		}
		else if (vm.count("reorder"))
		{
			std::string input_surface_path, out_surface_path;
			if (vm.count("in"))
			{
				input_surface_path = vm["in"].as<std::string>();
			}
			else {
				std::cerr << "in option missing" << std::endl;
				return -1;
			}

			if (vm.count("out"))
			{
				out_surface_path = vm["out"].as<std::string>();
			}
			else {
				std::cerr << "out option missing" << std::endl;
				return -1;
			}

			Surface input_surface;
			if (!SurfaceUtils::LoadSurface(input_surface_path.c_str(), input_surface))
			{
				std::cerr << "fail to load input surface from " << input_surface_path << std::endl;
				return -1;
			}

			SurfaceUtils::ReorderVertices(input_surface);

			if (!SurfaceUtils::SaveSurface(input_surface, out_surface_path.c_str()))
			{
				std::cerr << "fail to write surface to " << out_surface_path << std::endl;
				return -1;
			}
		}
		else if (vm.count("mean2")) {

			std::string surface_out = BoostCmdHelper::Get<std::string>(vm, "out");
			std::string landmark_out = BoostCmdHelper::Get<std::string>(vm, "out2");

			std::vector<PDM*> surfacePDMs, landmarkPDMs;
			std::vector<int> lm_ids;

			if (vm.count("prefix"))
			{
				std::string prefix = BoostCmdHelper::Get<std::string>(vm, "prefix");
				std::string postfix = BoostCmdHelper::Get<std::string>(vm, "postfix");
				std::string prefix2 = BoostCmdHelper::Get<std::string>(vm, "prefix2");
				std::string postfix2 = BoostCmdHelper::Get<std::string>(vm, "postfix2");
				int start = BoostCmdHelper::Get<int>(vm, "start");
				int end = BoostCmdHelper::Get<int>(vm, "end");

				// load surfaces
				load_pdms(prefix, start, end, postfix, surfacePDMs);

				// load landmarks
				landmarkPDMs.resize(end - start + 1);
				for (int i = start; i <= end; ++i)
				{
					landmarkPDMs[i - start] = new PDM;
					std::string landmark_path = prefix2 + stringUtils::num2str(i) + postfix2;
					if (!PDMUtils::ReadLandmarksFromTxt(landmark_path.c_str(), *landmarkPDMs[i - start], lm_ids))
						return -1;
				}
			}
			else if (vm.count("file")) {

				std::string shapelist = BoostCmdHelper::Get<std::string>(vm, "file");
				std::string lmlist = BoostCmdHelper::Get<std::string>(vm, "file2");

				// load surfaces
				load_pdms2(shapelist, surfacePDMs);

				// load landmarks
				std::vector< std::string > lines;
				if (!stringUtils::ReadLines(lmlist.c_str(), lines)) {
					std::cerr << "fails to read lines from " << lmlist << std::endl; exit(-1);
				}

				if (lines.size() != surfacePDMs.size()) {
					std::cerr << "number of subjects inconsistent" << std::endl; exit(-1);
				}
				
				landmarkPDMs.resize(lines.size());
				for (size_t i = 0; i < landmarkPDMs.size(); ++i)
				{
					landmarkPDMs[i] = new PDM;
					std::string landmark_path = lines[i];
					if (!PDMUtils::ReadLandmarksFromTxt(landmark_path.c_str(), *landmarkPDMs[i], lm_ids))
						return -1;
				}
			}
			else {
				std::cerr << "please specify file or prefix option" << std::endl; exit(-1);
			}

			// align them into template space
			if (vm.count("affine"))
			{
				PDMAffineTransform T;
				unsigned int median_idx = PDMUtils::CommonPDM(surfacePDMs, T);
				PDMUtils::AlignToTemplateWithLandmarks(surfacePDMs, landmarkPDMs, median_idx, T);
			}
			else if (vm.count("rigid"))
			{
				PDMRigidTransform T;
				unsigned int median_idx = PDMUtils::CommonPDM(surfacePDMs, T);
				PDMUtils::AlignToTemplateWithLandmarks(surfacePDMs, landmarkPDMs, median_idx, T);
			}
			else if (vm.count("similarity"))
			{
				PDMSimilarityTransform T;
				unsigned int median_idx = PDMUtils::CommonPDM(surfacePDMs, T);
				PDMUtils::AlignToTemplateWithLandmarks(surfacePDMs, landmarkPDMs, median_idx, T);
			}
			else {
				std::cerr << "undefined transform" << std::endl; return -1;
			}

			// save mean surface
			PDM mean_surface = PDMUtils::MeanPDM(surfacePDMs);
			std::vector< vect3<int> >& faces = static_cast<Surface*>(surfacePDMs[0])->GetFaces();
			unsigned int num_verts = mean_surface.GetPoints().size() / 3;
			if (!SurfaceUtils::SaveSurface(&(mean_surface.GetPoints()[0]), num_verts, faces, surface_out.c_str())) {
				std::cerr << "fail to write mean surface to " << surface_out << std::endl; return -1;
			}

			// save mean landmarks
			PDM mean_landmarks = PDMUtils::MeanPDM(landmarkPDMs);
			if (!PDMUtils::WriteLandmarksAsTxt(mean_landmarks, lm_ids, landmark_out.c_str())) {
				std::cerr << "fail to write mean landmarks to " << landmark_out << std::endl; return -1;
			}

			for (size_t i = 0; i < surfacePDMs.size(); ++i) {
				if (surfacePDMs[i]) {
					delete surfacePDMs[i];
					surfacePDMs[i] = NULL;
				}
			}

			for (size_t i = 0; i < landmarkPDMs.size(); ++i) {
				if (landmarkPDMs[i]) {
					delete landmarkPDMs[i];
					landmarkPDMs[i] = NULL;
				}
			}

		}
		else if (vm.count("mean") || vm.count("pca")) {

			std::string outpath = BoostCmdHelper::Get<std::string>(vm, "out");

			std::vector<PDM*> pdms;
			if (vm.count("file")) {
				std::string path = BoostCmdHelper::Get<std::string>(vm, "file");
				load_pdms2(path, pdms);
			}
			else {
				std::string prefix = BoostCmdHelper::Get<std::string>(vm, "prefix");
				std::string postfix = BoostCmdHelper::Get<std::string>(vm, "postfix");
				int start = BoostCmdHelper::Get<int>(vm, "start");
				int end = BoostCmdHelper::Get<int>(vm, "end");
				load_pdms(prefix, start, end, postfix, pdms);
			}

			if (vm.count("affine"))
			{
				PDMAffineTransform T;
				unsigned int median_idx = PDMUtils::CommonPDM(pdms, T);
				PDMUtils::AlignToTemplate(pdms, median_idx, T);
			}
			else if (vm.count("rigid"))
			{
				PDMRigidTransform T;
				unsigned int median_idx = PDMUtils::CommonPDM(pdms, T);
				PDMUtils::AlignToTemplate(pdms, median_idx, T);
			}
			else if (vm.count("similarity"))
			{
				PDMSimilarityTransform T;
				unsigned int median_idx = PDMUtils::CommonPDM(pdms, T);
				PDMUtils::AlignToTemplate(pdms, median_idx, T);
			}
			else {
				std::cerr << "undefined transform" << std::endl; return -1;
			}

			//for( unsigned int i = 0; i < pdms.size(); ++i )
			//{
			//	Surface* surf = static_cast<Surface*>(pdms[i]);
			//	char path[2048] = {0};
			//	sprintf(path, "/Users/yzgao/Desktop/temp/%d.vtk", i);
			//	SurfaceUtils::SaveSurface(*surf, path);
			//}

			if (vm.count("mean")) {
				PDM mean = PDMUtils::MeanPDM(pdms);
				std::vector< vect3<int> >& faces = static_cast<Surface*>(pdms[0])->GetFaces();
				unsigned int num_verts = mean.GetPoints().size() / 3;
				if (!SurfaceUtils::SaveSurface(&(mean.GetPoints()[0]), num_verts, faces, outpath.c_str())) {
					std::cerr << "fail to write surface to " << outpath << std::endl; return -1;
				}
			}
			else if (vm.count("pca")) {
				float vp = BoostCmdHelper::Get<float>(vm, "p");
				PCAShapeSpace ss;
				ss.Create(pdms);
				ss.SetFaceConnectivity(static_cast<Surface*>(pdms[0])->GetFaces());
				unsigned int numModes = ss.var2num(vp);
				ss.keep(numModes);
				std::cout << numModes << " modes to keep " << vp << " variations" << std::endl;
				if (!ss.Save(outpath.c_str())) {
					std::cerr << "fail to write space to " << outpath << std::endl; return -1;
				}
			}

			for (unsigned int i = 0; i < pdms.size(); ++i) {
				delete pdms[i];
				pdms[i] = NULL;
			}
		}
		else if (vm.count("type")) {

			std::string input_surface_path, out_surface_path;
			if (vm.count("in"))
			{
				input_surface_path = vm["in"].as<std::string>();
			}
			else {
				std::cerr << "in option missing" << std::endl;
				return -1;
			}

			if (vm.count("out"))
			{
				out_surface_path = vm["out"].as<std::string>();
			}
			else {
				std::cerr << "out option missing" << std::endl;
				return -1;
			}

			Surface input_surface;
			if (!SurfaceUtils::LoadSurface(input_surface_path.c_str(), input_surface))
			{
				std::cerr << "fail to load input surface from " << input_surface_path << std::endl;
				return -1;
			}

			if (!SurfaceUtils::SaveSurface(input_surface, out_surface_path.c_str()))
			{
				std::cerr << "fail to write surface to " << out_surface_path << std::endl;
				return -1;
			}

		}
		else if (vm.count("apply")) {

			std::string input_surface_path, out_surface_path, transform_path;
			if (vm.count("in"))
			{
				input_surface_path = vm["in"].as<std::string>();
			}
			else {
				std::cerr << "in option missing" << std::endl;
				return -1;
			}

			if (vm.count("init"))
			{
				transform_path = vm["init"].as<std::string>();
			}
			else {
				std::cerr << "init option missing" << std::endl;
				return -1;
			}

			if (vm.count("out"))
			{
				out_surface_path = vm["out"].as<std::string>();
			}
			else {
				std::cerr << "out option missing" << std::endl;
				return -1;
			}

			Surface input_surface;
			if (!SurfaceUtils::LoadSurface(input_surface_path.c_str(), input_surface))
			{
				std::cerr << "fail to load input surface from " << input_surface_path << std::endl;
				return -1;
			}

			PDMAffineTransform T;
			if (!T.Load(transform_path.c_str()))
			{
				std::cerr << "fail to load transform txt" << std::endl;
				return -1;
			}

			std::cout << "transformation: " << std::endl;
			std::cout << T.data() << std::endl;

			Surface out_surface;
			T.Apply(input_surface, out_surface);

			std::vector< vect3<int> >& faces = out_surface.GetFaces();
			std::vector< vect3<int> >& refFaces = input_surface.GetFaces();
			faces = refFaces;

			if (!SurfaceUtils::SaveSurface(out_surface, out_surface_path.c_str()))
			{
				std::cerr << "fail to write surface to " << out_surface_path << std::endl;
				return -1;
			}

		}
		else if (vm.count("landmark")) {

			int id = BoostCmdHelper::Get<int>(vm, "id");
			std::string seg_prefix = BoostCmdHelper::Get<std::string>(vm, "prefix");
			std::string seg_postfix = BoostCmdHelper::Get<std::string>(vm, "postfix");
			int start = BoostCmdHelper::Get<int>(vm, "start");
			int end = BoostCmdHelper::Get<int>(vm, "end");
			std::string out = BoostCmdHelper::Get<std::string>(vm, "out");
			std::string inten_prefix, inten_postfix;

			if (id < 0 || id > 8) {
				std::cerr << "landmark id must be within [0,8]" << std::endl;
				return -1;
			}

			if (vm.count("prefix2"))
				inten_prefix = vm["prefix2"].as<std::string>();
			else
				inten_prefix = seg_prefix;

			if (vm.count("postfix2"))
				inten_postfix = vm["postfix2"].as<std::string>();
			else
				inten_postfix = seg_postfix;

			extract_landmark(id, inten_prefix, inten_postfix, seg_prefix, seg_postfix, start, end, out);

		}
		else if (vm.count("reclm")) {

			int id = BoostCmdHelper::Get<int>(vm, "id");
			std::string seg_prefix = BoostCmdHelper::Get<std::string>(vm, "prefix");
			std::string seg_postfix = BoostCmdHelper::Get<std::string>(vm, "postfix");
			int start = BoostCmdHelper::Get<int>(vm, "start");
			int end = BoostCmdHelper::Get<int>(vm, "end");
			std::string out = BoostCmdHelper::Get<std::string>(vm, "out");
			std::string inten_prefix, inten_postfix;

			if (id < 0 || id > 9) {
				std::cerr << "landmark id must be within [0,9]" << std::endl;
				return -1;
			}

			if (vm.count("prefix2"))
				inten_prefix = vm["prefix2"].as<std::string>();
			else
				inten_prefix = seg_prefix;

			if (vm.count("postfix2"))
				inten_postfix = vm["postfix2"].as<std::string>();
			else
				inten_postfix = seg_postfix;

			extract_rectum_landmarks(id, inten_prefix, inten_postfix, seg_prefix, seg_postfix, start, end, out);
		}
		else if (vm.count("blalm")) {

			int id = BoostCmdHelper::Get<int>(vm, "id");
			std::string seg_prefix = BoostCmdHelper::Get<std::string>(vm, "prefix");
			std::string seg_postfix = BoostCmdHelper::Get<std::string>(vm, "postfix");
			int start = BoostCmdHelper::Get<int>(vm, "start");
			int end = BoostCmdHelper::Get<int>(vm, "end");
			std::string out = BoostCmdHelper::Get<std::string>(vm, "out");
			std::string inten_prefix, inten_postfix;

			if (id < 0 || id > 9) {
				std::cerr << "landmark id must be within [0,9]" << std::endl;
				return -1;
			}

			if (vm.count("prefix2"))
				inten_prefix = vm["prefix2"].as<std::string>();
			else
				inten_prefix = seg_prefix;

			if (vm.count("postfix2"))
				inten_postfix = vm["postfix2"].as<std::string>();
			else
				inten_postfix = seg_postfix;

			extract_bladder_landmarks(id, inten_prefix, inten_postfix, seg_prefix, seg_postfix, start, end, out);

		}
		else if (vm.count("closestvertex")) {

			std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::vector<float> point = BoostCmdHelper::Get< std::vector<float> >(vm, "point");

			if (point.size() != 3) {
				std::cerr << "point option should be a triple let." << std::endl; return -1;
			}

			Surface surf;
			if (!SurfaceUtils::LoadSurface(surf_path.c_str(), surf)) {
				std::cerr << "fails to load surface from " << surf_path << std::endl; exit(-1);
			}

			const std::vector<float>& verts = surf.GetPoints();
			unsigned int vert_num = verts.size() / 3;

			vect3<float> pt(point[0], point[1], point[2]);
			double mindist = std::numeric_limits<double>::max();
			int mindist_idx = -1;

			for (unsigned int i = 0; i < vert_num; ++i)
			{
				vect3<float> vert(verts[i * 3], verts[i * 3 + 1], verts[i * 3 + 2]);
				vect3<float> diff = vert - pt;
				double dist = static_cast<double>(sqrt(diff[0] * diff[0] + diff[1] * diff[1] + diff[2] * diff[2]));
				if (dist < mindist) {
					mindist_idx = i;
					mindist = dist;
				}
			}

			std::cout << "[" << mindist_idx << "] " << verts[mindist_idx * 3] << " " << verts[mindist_idx * 3 + 1] << " " << verts[mindist_idx * 3 + 2] << " (" << mindist << ")" << std::endl;

		}
		else if (vm.count("carve")) {

			std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string ref_path = BoostCmdHelper::Get<std::string>(vm, "ref");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			Surface surf;
			if (!SurfaceUtils::LoadSurface(surf_path.c_str(), surf)) {
				std::cerr << "fails to load surface from " << surf_path.c_str() << std::endl; return -1;
			}
			mxImage<TPixel> image;
			if (!ImageHelper::ReadImage<TPixel, TPixel>(ref_path.c_str(), image)) {
				std::cerr << "fails to read image from " << ref_path.c_str() << std::endl; return -1;
			}
			mxImage<TPixel> outImage;
			SurfaceUtils::CarveSurface(surf, image, static_cast<TPixel>(0), static_cast<TPixel>(255), outImage);
			if (!ImageHelper::WriteImage<TPixel, TPixel>(outImage, out_path.c_str())) {
				std::cerr << "fails to write image to " << ref_path.c_str() << std::endl; return -1;
			}

		}
		else if (vm.count("coord")) {

			std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "in");
			int id = BoostCmdHelper::Get<int>(vm, "id");

			Surface surf;
			if (!SurfaceUtils::LoadSurface(surf_path.c_str(), surf)) {
				std::cerr << "fail to read surface from " << surf_path << std::endl; return -1;
			}

			const std::vector<float>& verts = surf.GetPoints();
			unsigned int vert_num = verts.size() / 3;
			if (id < 0 || id >= static_cast<int>(vert_num)) { std::cerr << "vertex id out of index" << std::endl; return -1; }

			std::cout << verts[id * 3] << " " << verts[id * 3 + 1] << " " << verts[id * 3 + 2] << std::endl;

		}
		else if (vm.count("shift")) {

			std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "in");
			if (!vm.count("vector") || vm["vector"].as< std::vector<float> >().size() != 3) {
				std::cerr << "please specify a three-dimensional vector for option vector" << std::endl;
				return -1;
			}
			std::vector<float> shift = vm["vector"].as< std::vector<float> >();
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			// std::cout << shift[0] << "," << shift[1] << "," << shift[2] << std::endl;

			Surface surf;
			if (!SurfaceUtils::LoadSurface(surf_path.c_str(), surf)) {
				std::cerr << "fail to read surface from " << surf_path << std::endl; return -1;
			}

			std::vector<float>& pts = surf.GetPoints();
			int ptnum = pts.size() / 3;
			for (unsigned int i = 0; i < ptnum; ++i) {
				pts[i * 3] += shift[0];
				pts[i * 3 + 1] += shift[1];
				pts[i * 3 + 2] += shift[2];
			}

			if (!SurfaceUtils::SaveSurface(surf, out_path.c_str())) {
				std::cerr << "fail to write surface to " << out_path << std::endl; return -1;
			}

		}
		else if (vm.count("isosurface")) {

			std::string in_path = BoostCmdHelper::Get<std::string>(vm, "in");
			double spacing = BoostCmdHelper::Get<double>(vm, "spacing");
			int isoval = BoostCmdHelper::Get<int>(vm, "isoval");
			double smooth_sigma = BoostCmdHelper::Get<double>(vm, "smooth");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			ImageHelper::PixelType pixeltype = ImageHelper::ReadPixelType(in_path.c_str());
			switch (pixeltype)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: isosurface<unsigned char>(in_path, spacing, smooth_sigma, isoval, out_path); break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: isosurface<char>(in_path, spacing, smooth_sigma, isoval, out_path); break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: isosurface<unsigned short>(in_path, spacing, smooth_sigma, isoval, out_path); break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: isosurface<short>(in_path, spacing, smooth_sigma, isoval, out_path); break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: isosurface<unsigned int>(in_path, spacing, smooth_sigma, isoval, out_path); break;
			case BRIC::IDEA::FISH::ImageHelper::INT: isosurface<int>(in_path, spacing, smooth_sigma, isoval, out_path); break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: isosurface<unsigned long>(in_path, spacing, smooth_sigma, isoval, out_path); break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: isosurface<long>(in_path, spacing, smooth_sigma, isoval, out_path); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: isosurface<float>(in_path, spacing, smooth_sigma, isoval, out_path); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: isosurface<double>(in_path, spacing, smooth_sigma, isoval, out_path); break;
			default:
				std::cerr << "unrecognized pixel type" << std::endl;
				break;
			}

		}
		else if (vm.count("map")) {

			std::string shapespace_path = BoostCmdHelper::Get<std::string>(vm, "shapespace");
			int comp_num = BoostCmdHelper::Get<int>(vm, "comp");
			std::string outfile = BoostCmdHelper::Get<std::string>(vm, "out");

			if (vm.count("prefix"))
			{
				std::string prefix = BoostCmdHelper::Get<std::string>(vm, "prefix");
				int start_idx = BoostCmdHelper::Get<int>(vm, "start");
				int end_idx = BoostCmdHelper::Get<int>(vm, "end");
				std::string postfix = BoostCmdHelper::Get<std::string>(vm, "postfix");

				// load PDMs
				std::vector<PDM*> pdms;
				load_pdms(prefix, start_idx, end_idx, postfix, pdms);
				map_shapes(pdms, shapespace_path, comp_num, outfile, vm);

				// release resources
				for (unsigned int i = 0; i < pdms.size(); ++i) {
					delete pdms[i];
					pdms[i] = NULL;
				}
			}
			else if (vm.count("file"))
			{
				std::string shapeListPath = BoostCmdHelper::Get<std::string>(vm, "file");
				std::vector<PDM*> pdms;
				load_pdms2(shapeListPath, pdms);

				map_shapes(pdms, shapespace_path, comp_num, outfile, vm);

				// release resources
				for (unsigned int i = 0; i < pdms.size(); ++i) {
					delete pdms[i];
					pdms[i] = NULL;
				}
			}
			else {
				std::cerr << "please use options file or prefix to specify the shape list" << std::endl; exit(-1);
			}

		}
		else if (vm.count("fuse")) {
		
			std::string prefix = BoostCmdHelper::Get<std::string>(vm, "prefix");
			int start_idx = BoostCmdHelper::Get<int>(vm, "start");
			int end_idx = BoostCmdHelper::Get<int>(vm, "end");
			std::string postfix = BoostCmdHelper::Get<std::string>(vm, "postfix");
			std::string refImagePath = BoostCmdHelper::Get<std::string>(vm, "ref");
			std::string outPath = BoostCmdHelper::Get<std::string>(vm, "out");

			// load PDMs
			std::vector<Surface*> surfs;
			load_surfaces(prefix, start_idx, end_idx, postfix, surfs);

			// load ref image pixel type
			ImageHelper::PixelType type = ImageHelper::ReadPixelType(refImagePath.c_str());
			mxImage<double> outImage;

			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: 
				fuse_shapes_onto_image<unsigned char>(surfs, refImagePath, outImage);
				if (!ImageHelper::WriteImage<double, unsigned char>(outImage, outPath.c_str())) {
					std::cerr << "fail to load images from " << outPath << std::endl; exit(-1); 
				}
				break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: 
				fuse_shapes_onto_image<char>(surfs, refImagePath, outImage);
				if (!ImageHelper::WriteImage<double, char>(outImage, outPath.c_str())) {
					std::cerr << "fail to load images from " << outPath << std::endl; exit(-1);
				}
				break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: 
				fuse_shapes_onto_image<unsigned short>(surfs, refImagePath, outImage);
				if (!ImageHelper::WriteImage<double, unsigned short>(outImage, outPath.c_str())) {
					std::cerr << "fail to load images from " << outPath << std::endl; exit(-1);
				}
				break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: 
				fuse_shapes_onto_image<short>(surfs, refImagePath, outImage); 
				if (!ImageHelper::WriteImage<double, short>(outImage, outPath.c_str())) {
					std::cerr << "fail to load images from " << outPath << std::endl; exit(-1);
				}
				break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: 
				fuse_shapes_onto_image<unsigned int>(surfs, refImagePath, outImage); 
				if (!ImageHelper::WriteImage<double, unsigned int>(outImage, outPath.c_str())) {
					std::cerr << "fail to load images from " << outPath << std::endl; exit(-1);
				}
				break;
			case BRIC::IDEA::FISH::ImageHelper::INT: 
				fuse_shapes_onto_image<int>(surfs, refImagePath, outImage);
				if (!ImageHelper::WriteImage<double, int>(outImage, outPath.c_str())) {
					std::cerr << "fail to load images from " << outPath << std::endl; exit(-1);
				}
				break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: 
				fuse_shapes_onto_image<unsigned long>(surfs, refImagePath, outImage);
				if (!ImageHelper::WriteImage<double, unsigned long>(outImage, outPath.c_str())) {
					std::cerr << "fail to load images from " << outPath << std::endl; exit(-1);
				}
				break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: 
				fuse_shapes_onto_image<long>(surfs, refImagePath, outImage);
				if (!ImageHelper::WriteImage<double, long>(outImage, outPath.c_str())) {
					std::cerr << "fail to load images from " << outPath << std::endl; exit(-1);
				}
				break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: 
				fuse_shapes_onto_image<float>(surfs, refImagePath, outImage); 
				if (!ImageHelper::WriteImage<double, float>(outImage, outPath.c_str())) {
					std::cerr << "fail to load images from " << outPath << std::endl; exit(-1);
				}
				break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: 
				fuse_shapes_onto_image<double>(surfs, refImagePath, outImage);
				if (!ImageHelper::WriteImage<double, double>(outImage, outPath.c_str())) {
					std::cerr << "fail to load images from " << outPath << std::endl; exit(-1);
				}
				break;
			default: 
				std::cerr << "unrecognized pixel type" << std::endl; exit(-1);
			}

			// release resources
			for (unsigned int i = 0; i < surfs.size(); ++i) {
				delete surfs[i];
				surfs[i] = NULL;
			}
		
		}
		else if (vm.count("boundmask"))
		{
			std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string ref_image_path = BoostCmdHelper::Get<std::string>(vm, "ref");
			double padsize = BoostCmdHelper::Get<double>(vm, "padsize");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(ref_image_path.c_str());

			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: bounding_box_mask<unsigned char>(surf_path, ref_image_path, padsize, out_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: bounding_box_mask<char>(surf_path, ref_image_path, padsize, out_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: bounding_box_mask<unsigned short>(surf_path, ref_image_path, padsize, out_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: bounding_box_mask<short>(surf_path, ref_image_path, padsize, out_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: bounding_box_mask<unsigned int>(surf_path, ref_image_path, padsize, out_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::INT: bounding_box_mask<int>(surf_path, ref_image_path, padsize, out_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: bounding_box_mask<unsigned long>(surf_path, ref_image_path, padsize, out_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: bounding_box_mask<long>(surf_path, ref_image_path, padsize, out_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: bounding_box_mask<float>(surf_path, ref_image_path, padsize, out_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: bounding_box_mask<double>(surf_path, ref_image_path, padsize, out_path);  break;
			default: err_message("unrecognized pixel type");  break;
			}
		}
		else if (vm.count("morph"))
		{
			std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "in");
			double morph_size = BoostCmdHelper::Get<double>(vm, "size");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			Surface surf;
			if (!SurfaceUtils::LoadSurface(surf_path.c_str(), surf))
			{
				std::cerr << "fail to load surface from " << surf_path << std::endl;
				return -1;
			}

			SurfaceUtils::DilateShrinkSurface(surf, morph_size);

			if (!SurfaceUtils::SaveSurface(surf, out_path.c_str()))
			{
				std::cerr << "fail to write surface to " << out_path << std::endl;
				return -1;
			}

		}
		else if (vm.count("flipx") || vm.count("flipy") || vm.count("flipz"))
		{
			std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			Surface surf;
			if (!SurfaceUtils::LoadSurface(surf_path.c_str(), surf))
			{
				std::cerr << "fail to load surface from " << surf_path << std::endl;
				exit(-1);
			}

			if (vm.count("flipx"))
				SurfaceUtils::FlipX(surf);
			
			if (vm.count("flipy"))
				SurfaceUtils::FlipY(surf);

			if (vm.count("flipz"))
				SurfaceUtils::FlipZ(surf);

			if (!SurfaceUtils::SaveSurface(surf, out_path.c_str()))
			{
				std::cerr << "fail to save surface to" << out_path << std::endl;
				exit(-1);
			}
		}
		else if (vm.count("remesh"))
		{
			std::string surf_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			float stepsize = 0.5f;
			if (vm.count("stepsize"))
				stepsize = BoostCmdHelper::Get<float>(vm, "stepsize");

			int iter = 300;
			if (vm.count("iter"))
				iter = BoostCmdHelper::Get<int>(vm, "iter");

			Surface surf;
			if (!SurfaceUtils::LoadSurface(surf_path.c_str(), surf))
			{
				std::cerr << "fail to load surface from " << surf_path << std::endl;
				return -1;
			}

			SurfaceUtils::Remesh(surf, stepsize, iter);

			if (!SurfaceUtils::SaveSurface(surf, out_path.c_str()))
			{
				std::cerr << "fail to save surface to " << out_path << std::endl;
				return -1;
			}
		}
		else {
			std::cerr << "option not supported" << std::endl;
			return -1;
		}

	} catch ( boost::program_options::error& exp )
	{
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}



void extract_landmark(int id, std::string inten_prefix, std::string inten_postfix, std::string seg_prefix, std::string seg_postfix, int start, int end, std::string out)
{
	FILE* fp = fopen(out.c_str(), "w");
	if (!fp)
	{
		std::cout << "could not open file for writing: " << out  << std::endl;
		exit(-1);
	}

	for (int i = start; i <= end; ++i)
	{
		char filename[2048] = {0};
		sprintf(filename, "%s%d%s", inten_prefix.c_str(), i, inten_postfix.c_str());

		char segname[2048] = {0};
		sprintf(segname, "%s%d%s", seg_prefix.c_str(), i, seg_postfix.c_str());

		if (!boost::filesystem::exists(segname)) {
			std::cerr << segname << " does not exist" << std::endl;
			continue;
		}

		typedef unsigned short PixelType;
		mxImage<PixelType> image;
		if( !ImageHelper::ReadImage<PixelType,PixelType>(segname, image) )
		{
			std::cerr << "fail to read image from " << segname << std::endl;
			continue;
		}

		std::cout << "Processing " << segname << std::endl;

		unsigned int slice_start, slice_end;
		mxImageUtils::FindSliceRange(image, slice_start, slice_end);

		if(id == 0) {
			unsigned int slice_middle = (slice_start + slice_end) / 2;
			vect3<double> centerVoxel = mxImageUtils::SliceMassCenter(image, slice_middle);
			vect3<double> centerWorld;
			mxImageUtils::Voxel2World(image, centerVoxel, centerWorld);

			fprintf(fp, "%s\n", filename);
			fprintf(fp, "annotator\t0\t%f\t%f\t%f\t%f\t%f\t%f\n", centerVoxel[0], centerVoxel[1], centerVoxel[2], centerWorld[0], centerWorld[1], centerWorld[2]);

		}else if(id == 5) {
			vect3<double> centerVoxel = mxImageUtils::SliceMassCenter(image, slice_start);
			vect3<double> centerWorld;
			mxImageUtils::Voxel2World(image, centerVoxel, centerWorld);

			fprintf(fp, "%s\n", filename);
			fprintf(fp, "annotator\t0\t%f\t%f\t%f\t%f\t%f\t%f\n", centerVoxel[0], centerVoxel[1], centerVoxel[2], centerWorld[0], centerWorld[1], centerWorld[2]);

		}else if(id == 6) {
			vect3<double> centerVoxel = mxImageUtils::SliceMassCenter(image, slice_end);
			vect3<double> centerWorld;
			mxImageUtils::Voxel2World(image, centerVoxel, centerWorld);

			fprintf(fp, "%s\n", filename);
			fprintf(fp, "annotator\t0\t%f\t%f\t%f\t%f\t%f\t%f\n", centerVoxel[0], centerVoxel[1], centerVoxel[2], centerWorld[0], centerWorld[1], centerWorld[2]);

		}else if(id == 7) {
			unsigned int slice_onefourth = (slice_end - slice_start) / 4 + slice_start;
			vect3<double> centerVoxel = mxImageUtils::SliceMassCenter(image, slice_onefourth);
			vect3<double> centerWorld;
			mxImageUtils::Voxel2World(image, centerVoxel, centerWorld);

			fprintf(fp, "%s\n", filename);
			fprintf(fp, "annotator\t0\t%f\t%f\t%f\t%f\t%f\t%f\n", centerVoxel[0], centerVoxel[1], centerVoxel[2], centerWorld[0], centerWorld[1], centerWorld[2]);

		}else if(id == 8) {
			unsigned int slice_threefourth = (slice_end - slice_start) * 3 / 4 + slice_start;
			vect3<double> centerVoxel = mxImageUtils::SliceMassCenter(image, slice_threefourth);
			vect3<double> centerWorld;
			mxImageUtils::Voxel2World(image, centerVoxel, centerWorld);

			fprintf(fp, "%s\n", filename);
			fprintf(fp, "annotator\t0\t%f\t%f\t%f\t%f\t%f\t%f\n", centerVoxel[0], centerVoxel[1], centerVoxel[2], centerWorld[0], centerWorld[1], centerWorld[2]);

		}else {
			unsigned int slice_middle = (slice_start + slice_end) / 2;
			vect3<double> center = mxImageUtils::SliceMassCenter(image, slice_middle);

			vect3<unsigned int> leftVoxel, rightVoxel, topVoxel, downVoxel;
			mxImageUtils::FindLinePoint(image, vect3<int>(-1, static_cast<int>(center[1]), static_cast<int>(center[2])), leftVoxel, rightVoxel);
			mxImageUtils::FindLinePoint(image, vect3<int>(static_cast<int>(center[0]), -1, static_cast<int>(center[2])), topVoxel, downVoxel);

			vect3<double> leftWorld, rightWorld, topWorld, downWorld;
			mxImageUtils::Voxel2World(image, topVoxel, topWorld);
			mxImageUtils::Voxel2World(image, downVoxel, downWorld);
			mxImageUtils::Voxel2World(image, leftVoxel, leftWorld);
			mxImageUtils::Voxel2World(image, rightVoxel, rightWorld);

			fprintf(fp, "%s\n", filename);
			switch (id)
			{
			case 1:
				fprintf(fp, "annotator\t0\t%d\t%d\t%d\t%f\t%f\t%f\n", leftVoxel[0], leftVoxel[1], leftVoxel[2], leftWorld[0], leftWorld[1], leftWorld[2]); break;
			case 2:
				fprintf(fp, "annotator\t0\t%d\t%d\t%d\t%f\t%f\t%f\n", rightVoxel[0], rightVoxel[1], rightVoxel[2], rightWorld[0], rightWorld[1], rightWorld[2]); break;
			case 3:
				fprintf(fp, "annotator\t0\t%d\t%d\t%d\t%f\t%f\t%f\n", topVoxel[0], topVoxel[1], topVoxel[2], topWorld[0], topWorld[1], topWorld[2]); break;
			case 4:
				fprintf(fp, "annotator\t0\t%d\t%d\t%d\t%f\t%f\t%f\n", downVoxel[0], downVoxel[1], downVoxel[2], downWorld[0], downWorld[1], downWorld[2]); break;
			default:
				std::cerr << "id not recognized" << std::endl; exit(-1);
			}
		}
	}
	fclose(fp);
}

void extract_rectum_landmarks(int id, std::string inten_prefix, std::string inten_postfix, std::string seg_prefix, std::string seg_postfix, int start, int end, std::string out)
{
	FILE* fp = fopen(out.c_str(), "w");
	for (int i = start; i <= end; ++i)
	{
		char filename[2048] = { 0 };
		sprintf(filename, "%s%d%s", inten_prefix.c_str(), i, inten_postfix.c_str());

		char segname[2048] = { 0 };
		sprintf(segname, "%s%d%s", seg_prefix.c_str(), i, seg_postfix.c_str());

		if (!boost::filesystem::exists(segname)) {
			std::cerr << segname << " does not exist" << std::endl;
			continue;
		}

		typedef unsigned short PixelType;
		mxImage<PixelType> image;
		if (!ImageHelper::ReadImage<PixelType, PixelType>(segname, image))
		{
			std::cerr << "fail to read image from " << segname << std::endl;
			continue;
		}

		std::cout << "Processing " << segname << std::endl;

		unsigned int slice_start, slice_end;
		mxImageUtils::FindSliceRange(image, slice_start, slice_end);

		if (id >=1 && id <= 4)
		{
			// landmarks on the first slice
			unsigned int slice_idx = slice_start;
			vect3<double> center = mxImageUtils::SliceMassCenter(image, slice_idx);

			vect3<unsigned int> leftVoxel, rightVoxel, topVoxel, downVoxel;
			mxImageUtils::FindLinePoint(image, vect3<int>(-1, static_cast<int>(center[1]), static_cast<int>(center[2])), leftVoxel, rightVoxel);
			mxImageUtils::FindLinePoint(image, vect3<int>(static_cast<int>(center[0]), -1, static_cast<int>(center[2])), topVoxel, downVoxel);

			vect3<double> leftWorld, rightWorld, topWorld, downWorld;
			mxImageUtils::Voxel2World(image, topVoxel, topWorld);
			mxImageUtils::Voxel2World(image, downVoxel, downWorld);
			mxImageUtils::Voxel2World(image, leftVoxel, leftWorld);
			mxImageUtils::Voxel2World(image, rightVoxel, rightWorld);

			fprintf(fp, "%s\n", filename);
			switch (id)
			{
			case 1:
				fprintf(fp, "annotator\t0\t%d\t%d\t%d\t%f\t%f\t%f\n", leftVoxel[0], leftVoxel[1], leftVoxel[2], leftWorld[0], leftWorld[1], leftWorld[2]); break;
			case 2:
				fprintf(fp, "annotator\t0\t%d\t%d\t%d\t%f\t%f\t%f\n", rightVoxel[0], rightVoxel[1], rightVoxel[2], rightWorld[0], rightWorld[1], rightWorld[2]); break;
			case 3:
				fprintf(fp, "annotator\t0\t%d\t%d\t%d\t%f\t%f\t%f\n", topVoxel[0], topVoxel[1], topVoxel[2], topWorld[0], topWorld[1], topWorld[2]); break;
			case 4:
				fprintf(fp, "annotator\t0\t%d\t%d\t%d\t%f\t%f\t%f\n", downVoxel[0], downVoxel[1], downVoxel[2], downWorld[0], downWorld[1], downWorld[2]); break;
			default:
				std::cerr << "id not recognized" << std::endl; exit(-1);
			}

		}
		else if (id == 5)
		{
			// landmark on the central slice
			unsigned int slice_idx = (slice_start + slice_end) / 2;
			vect3<double> centerVoxel = mxImageUtils::SliceMassCenter(image, slice_idx);
			vect3<double> centerWorld;
			mxImageUtils::Voxel2World(image, centerVoxel, centerWorld);

			fprintf(fp, "%s\n", filename);
			fprintf(fp, "annotator\t0\t%f\t%f\t%f\t%f\t%f\t%f\n", centerVoxel[0], centerVoxel[1], centerVoxel[2], centerWorld[0], centerWorld[1], centerWorld[2]);

		}
		else if (id >= 6 && id <= 9)
		{
			// landmarks on the last slice
			unsigned int slice_idx = slice_end;
			vect3<double> center = mxImageUtils::SliceMassCenter(image, slice_idx);

			vect3<unsigned int> leftVoxel, rightVoxel, topVoxel, downVoxel;
			mxImageUtils::FindLinePoint(image, vect3<int>(-1, static_cast<int>(center[1]), static_cast<int>(center[2])), leftVoxel, rightVoxel);
			mxImageUtils::FindLinePoint(image, vect3<int>(static_cast<int>(center[0]), -1, static_cast<int>(center[2])), topVoxel, downVoxel);

			vect3<double> leftWorld, rightWorld, topWorld, downWorld;
			mxImageUtils::Voxel2World(image, topVoxel, topWorld);
			mxImageUtils::Voxel2World(image, downVoxel, downWorld);
			mxImageUtils::Voxel2World(image, leftVoxel, leftWorld);
			mxImageUtils::Voxel2World(image, rightVoxel, rightWorld);

			fprintf(fp, "%s\n", filename);
			switch (id)
			{
			case 6:
				fprintf(fp, "annotator\t0\t%d\t%d\t%d\t%f\t%f\t%f\n", leftVoxel[0], leftVoxel[1], leftVoxel[2], leftWorld[0], leftWorld[1], leftWorld[2]); break;
			case 7:
				fprintf(fp, "annotator\t0\t%d\t%d\t%d\t%f\t%f\t%f\n", rightVoxel[0], rightVoxel[1], rightVoxel[2], rightWorld[0], rightWorld[1], rightWorld[2]); break;
			case 8:
				fprintf(fp, "annotator\t0\t%d\t%d\t%d\t%f\t%f\t%f\n", topVoxel[0], topVoxel[1], topVoxel[2], topWorld[0], topWorld[1], topWorld[2]); break;
			case 9:
				fprintf(fp, "annotator\t0\t%d\t%d\t%d\t%f\t%f\t%f\n", downVoxel[0], downVoxel[1], downVoxel[2], downWorld[0], downWorld[1], downWorld[2]); break;
			default:
				std::cerr << "id not recognized" << std::endl; exit(-1);
			}

		}
		else {
			err_message("id not recognized");
		}
	}
	fclose(fp);
}

void extract_bladder_landmarks(int id, std::string inten_prefix, std::string inten_postfix, std::string seg_prefix, std::string seg_postfix, int start, int end, std::string out)
{
	FILE* fp = fopen(out.c_str(), "w");
	for (int i = start; i <= end; ++i)
	{
		char filename[2048] = { 0 };
		sprintf(filename, "%s%d%s", inten_prefix.c_str(), i, inten_postfix.c_str());

		char segname[2048] = { 0 };
		sprintf(segname, "%s%d%s", seg_prefix.c_str(), i, seg_postfix.c_str());

		if (!boost::filesystem::exists(segname)) {
			std::cerr << segname << " does not exist" << std::endl;
			continue;
		}

		typedef unsigned short PixelType;
		mxImage<PixelType> image;
		if (!ImageHelper::ReadImage<PixelType, PixelType>(segname, image))
		{
			std::cerr << "fail to read image from " << segname << std::endl;
			continue;
		}

		std::cout << "Processing " << segname << std::endl;

		vect3<int> box_min, box_max;
		mxImageUtils::BoundingBox(image, box_min, box_max);

		vect3<double> box_min_world, box_max_world;
		mxImageUtils::Voxel2World(image, box_min, box_min_world);
		mxImageUtils::Voxel2World(image, box_max, box_max_world);

		if (id == 0)
		{
			fprintf(fp, "%s\n", filename);
			fprintf(fp, "annotator\t0\t%d\t%d\t%d\t%f\t%f\t%f\n", box_min[0], box_min[1], box_min[2], box_min_world[0], box_min_world[1], box_min_world[2]);
		}
		else if (id == 1)
		{
			fprintf(fp, "%s\n", filename);
			fprintf(fp, "annotator\t0\t%d\t%d\t%d\t%f\t%f\t%f\n", box_max[0], box_max[1], box_max[2], box_max_world[0], box_max_world[1], box_max_world[2]);
		}
		else {
			err_message("id not recognized");
		}
	}
	fclose(fp);
}

