//
//  ImageSampling.h
//  FISH
//
//  Created by Yaozong Gao on 01/23/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#include "stdafx.h"
#include "extern/BoostHelper.h"
#include "extern/ImageHelper.h"
#include "sample/ImageSampler.h"
#include "mesh/surface/SurfaceUtils.h"

using namespace BRIC::IDEA::FISH;

void print_help()
{
	std::cerr << "[ near-surface sampling for CFTrain ]" << std::endl;
	std::cerr << "ImageSampling --surf --in imageA --num sampleNum --sigmaIn s1 --sigmaOut s2 --smooth 2 --spacing spacingScalar --out out.txt\n" << std::endl;

	std::cerr << "[ near-surface sampling for rectum ]" << std::endl;
	std::cerr << "ImageSampling --recsurf --in imageA --num sampleNum --sigmaIn s1 --sigmaOut s2 --smooth 2 --spacing spacingScalar --out out.txt\n" << std::endl;

	std::cerr << "[ uniform sampling with equal sample number in all classes for CFTrain ]" << std::endl;
	std::cerr << "ImageSampling --uniform --in imageA --num sampleNum --padsize mm --spacing spacingScalar --labels 0,10,20,30 --weights 1,1,1,1 --out out.txt\n" << std::endl;

	std::cerr << "[ local uniform sampling in binary class for CFTrain ]" << std::endl;
	std::cerr << "ImageSampling --localuniform --in imageA --num sampleNum --inbandwidth mm --outbandwidth mm --spacing spacingScalar --out out.txt\n" << std::endl;
}

template <typename T>
void SurfaceSampling(std::string inputPath, int sampleNum, double sigmaIn, double sigmaOut, double smooth_sigma, double spacingScalar, bool withinSliceOption, std::string outPath)
{
	mxImage<unsigned char> maskImage;
	if ( !ImageHelper::ReadImage<T,unsigned char>(inputPath.c_str(), maskImage) ) {
		std::cerr << "fail to read image from " << inputPath << std::endl;
		exit(-1);
	}

	mxImage<unsigned char> cropMaskImage;
	mxImageUtils::Copy(maskImage, cropMaskImage);

	const vect3<int> pad_size(5, 5, 5);
	mxImageUtils::ShrinkMaskImage(cropMaskImage, pad_size);

	// generate surface
	Surface surf;
	SurfaceUtils::IsoSurfaceFromImage2(cropMaskImage, smooth_sigma, spacingScalar, 128, surf);
	surf.UpdateNormals();

	Random random;
	std::vector< vect3<double> > points;

	if (withinSliceOption)
		ImageSampler::SamplePointsNearSurface_BetweenSlices(surf, maskImage, sampleNum, sigmaIn, sigmaOut, random, points);
	else
		ImageSampler::SamplePointsNearSurface(surf, sampleNum, sigmaIn, sigmaOut, random, points);

	FILE* fp = fopen(outPath.c_str(), "w");
	for (unsigned int i = 0; i < points.size(); ++i)
	{
		int label = 0;
		vect3<int> voxel;
		mxImageUtils::World2Voxel(maskImage, points[i], voxel);

		if ( !maskImage.PtInImage(voxel[0], voxel[1], voxel[2]) ) {
			std::cerr << "found a sample point out of the image" << std::endl;
			exit(-1);
		}

		label = maskImage(voxel[0], voxel[1], voxel[2]) > 0;
		fprintf(fp, "%.2lf %.2lf %.2lf %d\n", points[i][0], points[i][1], points[i][2], label);
	}
	fclose(fp);
}

template <typename T>
void UniformSampling(std::string inputPath, int sampleNum, double padsize, double spacingScalar, const std::vector<int>& labels, const std::vector<double>& weights, std::string outPath)
{
	mxImage<unsigned char> maskImage;
	if (!ImageHelper::ReadImage<T, unsigned char>(inputPath.c_str(), maskImage)) {
		std::cerr << "fail to read image from " << inputPath << std::endl;
		exit(-1);
	}

	vect3<double> spacing(spacingScalar, spacingScalar, spacingScalar);
	mxImage<unsigned char> resampledMaskImage;
	mxImageUtils::Resample(maskImage, spacing, resampledMaskImage, BRIC::IDEA::FISH::IT_NEAREST_NEIGHBOR_INTERPOLATION);

	int pad_voxels = static_cast<int>(padsize / spacingScalar + 0.5);
	vect3<int> box_sp, box_ep;
	mxImageUtils::BoundingBox(resampledMaskImage, box_sp, box_ep);

	for (int i = 0; i < 3; ++i)
	{
		box_sp[i] = box_sp[i] - pad_voxels;
		box_ep[i] = box_ep[i] + pad_voxels;
	}
	mxImageUtils::AdjustVoxel(resampledMaskImage, box_sp);
	mxImageUtils::AdjustVoxel(resampledMaskImage, box_ep);

	// normalize the weights;
	std::vector<double> normalized_weights;
	normalized_weights.resize(weights.size());

	double sum_weight = 0;
	for (size_t i = 0; i < weights.size(); ++i)
		sum_weight += weights[i];
	for (size_t i = 0; i < normalized_weights.size(); ++i)
		normalized_weights[i] = weights[i] / sum_weight;

	// compute sample number
	std::vector<int> class_sample_numbers;
	class_sample_numbers.resize(normalized_weights.size());
	for (size_t i = 0; i < normalized_weights.size(); ++i)
		class_sample_numbers[i] = static_cast<int>(sampleNum * normalized_weights[i] + 0.5);


	std::vector < std::vector< vect3<unsigned int> > > class_voxels;
	std::map<unsigned char, int> label_map; // maps old label to new label

	class_voxels.resize( labels.size() );
	for (size_t i = 0; i < labels.size(); ++i)
		label_map[labels[i]] = i;

	for (int z = box_sp[2]; z < box_ep[2]; ++z)
	{
		for (int y = box_sp[1]; y < box_ep[1]; ++y)
		{
			for (int x = box_sp[0]; x < box_ep[0]; ++x)
			{
				vect3<unsigned int> voxel(x, y, z);
				unsigned char label = resampledMaskImage(x, y, z);
				int new_label_idx = label_map[label];
				class_voxels[new_label_idx].push_back(voxel);
			}
		}
	}

	FILE* fp = fopen(outPath.c_str(), "w");

	Random random;
	int sample_num_per_class = static_cast<int>(sampleNum / label_map.size() + 0.5);
	for (size_t i = 0; i < class_voxels.size(); ++i)
	{
		for (int j = 0; j < class_sample_numbers[i]; ++j)
		{
			size_t random_idx = random.Next<size_t>(0, class_voxels[i].size()-1);

			vect3<double> pt_world;
			mxImageUtils::Voxel2World(resampledMaskImage, class_voxels[i][random_idx], pt_world);

			fprintf(fp, "%.2lf %.2lf %.2lf %d\n", pt_world[0], pt_world[1], pt_world[2], i);
		}
	}

	fclose(fp);

}

template <typename T>
void LocalUniformSampling(std::string inputPath, int sampleNum, double inbandwidth, double outbandwidth, double spacingScalar, std::string outPath)
{
	mxImage<unsigned char> maskImage;
	if (!ImageHelper::ReadImage<T, unsigned char>(inputPath.c_str(), maskImage)) {
		std::cerr << "fail to read image from " << inputPath << std::endl;
		exit(-1);
	}

	// threshold
	unsigned char threshold = 0, inside_value = 255, outside_value = 0;
	mxImageUtils::Threshold(maskImage, threshold, inside_value, outside_value);

	// resample
	vect3<double> spacing(spacingScalar, spacingScalar, spacingScalar);
	mxImage<unsigned char> resampledMaskImage;
	mxImageUtils::Resample(maskImage, spacing, resampledMaskImage, BRIC::IDEA::FISH::IT_NEAREST_NEIGHBOR_INTERPOLATION);

	// dilate and erode
	mxImage<unsigned char> dilatedImage, erodedImage;
	mxImageUtils::Copy(resampledMaskImage, dilatedImage);
	mxImageUtils::Copy(resampledMaskImage, erodedImage);

	int inbandwidth_voxel = static_cast<int>(inbandwidth/spacingScalar + 0.5);
	int outbandwidth_voxel = static_cast<int>(outbandwidth/spacingScalar + 0.5);

	for (int i = 0; i < outbandwidth_voxel; ++i)
		mxImageUtils::BinaryDilate(dilatedImage);
	for (int i = 0; i < inbandwidth_voxel; ++i)
		mxImageUtils::BinaryErode(erodedImage);

	std::vector < vect3<int> > outband_voxels, inband_voxels;
	vect3<unsigned int> image_size = resampledMaskImage.GetImageSize();

	for (unsigned int z = 0; z < image_size[2]; ++z)
	{
		for (unsigned int y = 0; y < image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < image_size[0]; ++x)
			{
				if (dilatedImage(x, y, z) > 0 && resampledMaskImage(x, y, z) == 0)
					outband_voxels.push_back( vect3<int>(x, y, z) );

				if (erodedImage(x, y, z) == 0 && resampledMaskImage(x, y, z) > 0)
					inband_voxels.push_back( vect3<int>(x, y, z) );
			}
		}
	}

	int outband_num = static_cast<int>(sampleNum / 2.0 + 0.5);
	int inband_num = outband_num;

	FILE* fp = fopen(outPath.c_str(), "w");

	Random random;
	for (int j = 0; j < outband_num; ++j)
	{
		size_t random_idx = random.Next<size_t>(0, outband_voxels.size() - 1);

		vect3<double> pt_world;
		mxImageUtils::Voxel2World(resampledMaskImage, outband_voxels[random_idx], pt_world);

		vect3<int> orig_voxel;
		mxImageUtils::World2Voxel(maskImage, pt_world, orig_voxel);

		assert_message( maskImage.PtInImage(orig_voxel.x, orig_voxel.y, orig_voxel.z), "voxel not in the image domain" );
		int label = maskImage(orig_voxel.x, orig_voxel.y, orig_voxel.z) > 0;

		fprintf(fp, "%.2lf %.2lf %.2lf %d\n", pt_world[0], pt_world[1], pt_world[2], label);
	}

	for (int j = 0; j < inband_num; ++j)
	{
		size_t random_idx = random.Next<size_t>(0, inband_voxels.size() - 1);

		vect3<double> pt_world;
		mxImageUtils::Voxel2World(resampledMaskImage, inband_voxels[random_idx], pt_world);

		vect3<int> orig_voxel;
		mxImageUtils::World2Voxel(maskImage, pt_world, orig_voxel);

		assert_message(maskImage.PtInImage(orig_voxel.x, orig_voxel.y, orig_voxel.z), "voxel not in the image domain");
		int label = maskImage(orig_voxel.x, orig_voxel.y, orig_voxel.z) > 0;

		fprintf(fp, "%.2lf %.2lf %.2lf %d\n", pt_world[0], pt_world[1], pt_world[2], label);
	}

	fclose(fp);
}


int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display helper information");

		options_description functional("Functional options (must pick one)");
		functional.add_options()("surf", "near-surface sampling for CFTrain");
		functional.add_options()("recsurf", "near-surface sampling for rectum");
		functional.add_options()("uniform", "uniform sampling for all classes");
		functional.add_options()("localuniform", "local uniform sampling for binary classes");

		options_description params("Parameter options");
		params.add_options()("in", value<std::string>(), "input mask image");
		params.add_options()("num", value<int>(), "sample number");
		params.add_options()("sigmaIn", value<double>(), "sigma value for sampling inside");
		params.add_options()("sigmaOut", value<double>(), "sigma value for sampling outside");
		params.add_options()("spacing", value<double>(), "spacing scalar");
		params.add_options()("out", value<std::string>(), "output path");
		params.add_options()("smooth", value<double>(), "smooth sigma in voxels");
		params.add_options()("padsize", value<double>(), "padsize in millimeters");
		params.add_options()("labels", value<std::string>(), "all labels, separated by comma");
		params.add_options()("weights", value<std::string>(), "all weights for classes, separated by comma");
		params.add_options()("inbandwidth", value<double>(), "bandwidth for inside narrow band");
		params.add_options()("outbandwidth", value<double>(), "bandwidth for outside narrow band");

		options_description cmd_options;
		cmd_options.add(generic).add(functional).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if( vm.count("help") || argc == 1 )
		{
			std::cerr << cmd_options << std::endl;
			print_help();
			return -1;
		}

		if ( vm.count("surf") || vm.count("recsurf") )
		{
			std::string inputPath = BoostCmdHelper::Get<std::string>(vm, "in");
			int num = BoostCmdHelper::Get<int>(vm, "num");
			double sigmaIn = BoostCmdHelper::Get<double>(vm, "sigmaIn");
			double sigmaOut = BoostCmdHelper::Get<double>(vm, "sigmaOut");
			double spacing = BoostCmdHelper::Get<double>(vm, "spacing");
			double smooth_sigma = BoostCmdHelper::Get<double>(vm, "smooth");
			std::string outPath = BoostCmdHelper::Get<std::string>(vm, "out");

			bool withinSliceOption = false;
			if (vm.count("recsurf"))
				withinSliceOption = true;

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(inputPath.c_str());
			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR:
				SurfaceSampling<unsigned char>(inputPath, num, sigmaIn, sigmaOut, smooth_sigma, spacing, withinSliceOption, outPath); break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR:
				SurfaceSampling<char>(inputPath, num, sigmaIn, sigmaOut, smooth_sigma, spacing, withinSliceOption, outPath); break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT:
				SurfaceSampling<unsigned short>(inputPath, num, sigmaIn, sigmaOut, smooth_sigma, spacing, withinSliceOption, outPath); break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT:
				SurfaceSampling<short>(inputPath, num, sigmaIn, sigmaOut, smooth_sigma, spacing, withinSliceOption, outPath); break;
			case BRIC::IDEA::FISH::ImageHelper::UINT:
				SurfaceSampling<unsigned int>(inputPath, num, sigmaIn, sigmaOut, smooth_sigma, spacing, withinSliceOption, outPath); break;
			case BRIC::IDEA::FISH::ImageHelper::INT:
				SurfaceSampling<int>(inputPath, num, sigmaIn, sigmaOut, smooth_sigma, spacing, withinSliceOption, outPath); break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG:
				SurfaceSampling<unsigned long>(inputPath, num, sigmaIn, sigmaOut, smooth_sigma, spacing, withinSliceOption, outPath); break;
			case BRIC::IDEA::FISH::ImageHelper::LONG:
				SurfaceSampling<long>(inputPath, num, sigmaIn, sigmaOut, smooth_sigma, spacing, withinSliceOption, outPath); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT:
				SurfaceSampling<float>(inputPath, num, sigmaIn, sigmaOut, smooth_sigma, spacing, withinSliceOption, outPath); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE:
				SurfaceSampling<double>(inputPath, num, sigmaIn, sigmaOut, smooth_sigma, spacing, withinSliceOption, outPath); break;
			default: err_message("unrecognized pixel type");  break;
			}
		}
		else if ( vm.count("uniform") )
		{
			std::string inputPath = BoostCmdHelper::Get<std::string>(vm, "in");
			int num = BoostCmdHelper::Get<int>(vm, "num");
			double spacing = BoostCmdHelper::Get<double>(vm, "spacing");
			double padsize = BoostCmdHelper::Get<double>(vm, "padsize");
			std::string label_str = BoostCmdHelper::Get<std::string>(vm, "labels");
			std::string weight_str = BoostCmdHelper::Get<std::string>(vm, "weights");
			std::string outPath = BoostCmdHelper::Get<std::string>(vm, "out");

			std::vector<std::string> tokens = stringUtils::Split(label_str, ',');
			std::vector<int> labels;
			for (size_t i = 0; i < tokens.size(); ++i)
				labels.push_back(atoi(tokens[i].c_str()));

			tokens.clear();
			tokens = stringUtils::Split(weight_str, ',');
			std::vector<double> weights;
			for (size_t i = 0; i < tokens.size(); ++i)
				weights.push_back(atof(tokens[i].c_str()));

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(inputPath.c_str());
			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR:
				UniformSampling<unsigned char>(inputPath, num, padsize, spacing, labels, weights, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR:
				UniformSampling<char>(inputPath, num, padsize, spacing, labels, weights, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT:
				UniformSampling<unsigned short>(inputPath, num, padsize, spacing, labels, weights, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT:
				UniformSampling<short>(inputPath, num, padsize, spacing, labels, weights, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::UINT:
				UniformSampling<unsigned int>(inputPath, num, padsize, spacing, labels, weights, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::INT:
				UniformSampling<int>(inputPath, num, padsize, spacing, labels, weights, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG:
				UniformSampling<unsigned long>(inputPath, num, padsize, spacing, labels, weights, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::LONG:
				UniformSampling<long>(inputPath, num, padsize, spacing, labels, weights, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT:
				UniformSampling<float>(inputPath, num, padsize, spacing, labels, weights, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE:
				UniformSampling<double>(inputPath, num, padsize, spacing, labels, weights, outPath);
				break;
			default: err_message("unrecognized pixel type"); break;
			}
		}
		else if ( vm.count("localuniform") )
		{
			std::string inputPath = BoostCmdHelper::Get<std::string>(vm, "in");
			int num = BoostCmdHelper::Get<int>(vm, "num");
			double spacing = BoostCmdHelper::Get<double>(vm, "spacing");
			double inbandwidth = BoostCmdHelper::Get<double>(vm, "inbandwidth");
			double outbandwidth = BoostCmdHelper::Get<double>(vm, "outbandwidth");
			std::string outPath = BoostCmdHelper::Get<std::string>(vm, "out");

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(inputPath.c_str());
			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR:
				LocalUniformSampling<unsigned char>(inputPath, num, inbandwidth, outbandwidth, spacing, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR:
				LocalUniformSampling<char>(inputPath, num, inbandwidth, outbandwidth, spacing, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT:
				LocalUniformSampling<unsigned short>(inputPath, num, inbandwidth, outbandwidth, spacing, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT:
				LocalUniformSampling<short>(inputPath, num, inbandwidth, outbandwidth, spacing, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::UINT:
				LocalUniformSampling<unsigned int>(inputPath, num, inbandwidth, outbandwidth, spacing, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::INT:
				LocalUniformSampling<int>(inputPath, num, inbandwidth, outbandwidth, spacing, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG:
				LocalUniformSampling<unsigned long>(inputPath, num, inbandwidth, outbandwidth, spacing, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::LONG:
				LocalUniformSampling<long>(inputPath, num, inbandwidth, outbandwidth, spacing, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT:
				LocalUniformSampling<float>(inputPath, num, inbandwidth, outbandwidth, spacing, outPath);
				break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE:
				LocalUniformSampling<double>(inputPath, num, inbandwidth, outbandwidth, spacing, outPath);
				break;
			default: err_message("unrecognized pixel type");  break;
			}
		}
		else
		{
			std::cerr << "no such option" << std::endl;
			return -1;
		}

	} catch ( boost::program_options::error& exp ) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}