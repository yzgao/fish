#include "stdafx.h"
#include "extern/ImageHelper.h"
#include "extern/BoostHelper.h"
#include "common/mxImageUtils.h"
#include "common/stringUtils.h"
#include "mesh/PDMUtils.h"
#include "mesh/PDMTransform.h"
#include "common/aux_func.h"
#include "mesh/surface/SurfaceUtils.h"

using namespace BRIC::IDEA::FISH;

void print_help()
{
	std::cerr << "[ linear transformation ]" << std::endl;
	std::cerr << "example: ImageTools --apply --in imageA --ref imageB --init transformC --type dataTypeD {--linear | --nearest} --out imageE [--useWorld]" << std::endl;
	std::cerr << "[ dataTypeD | int, uint, short, ushort, float, double, char, uchar ]\n" << std::endl;

	std::cerr << "[ threshold ]" << std::endl;
	std::cerr << "example: ImageTools --thresholding --in imageA --threshold value --out imageB --outtype type\n" << std::endl;
    
    std::cerr << "[ format/datatype conversion ]" << std::endl;
    std::cerr << "example: ImageTools --format --in imageA --out imageB --intype dataTypeC --outtype dataTypeD\n" << std::endl;

	std::cerr << "[ resample image ]" << std::endl;
	std::cerr << "example: ImageTools --resample {--linear | --nearest} --in imageA --spacing {x y z} --out imageC --intype dataTypeD --outtype dataTypeD\n" << std::endl;

	std::cerr << "[ convert mask image to contour image ]" << std::endl;
	std::cerr << "example: ImageTools --contour --in imageA --out imageB --type typeC\n" << std::endl;

	std::cerr << "[ convert contour data from rtts to mask image ]" << std::endl;
	std::cerr << "example: ImageTools --rtts --in contourA.txt --ref imageB --out maskC --outtype typeD\n" << std::endl;

	std::cerr << "[ merge multiple masks into one ]" << std::endl;
	std::cerr << "example: ImageTools --merge --prefix prefix --tag pro,bla,rec --postfix .nii.gz --out outmask_path\n" << std::endl;

	std::cerr << "[ crop a region from 3D image ]" << std::endl;
	std::cerr << "example: ImageTools --crop --in input_path --coord spx,spy,spz,epx,epy,epz --out output_path\n" << std::endl;

	std::cerr << "[ truncate the rectum mask ]" << std::endl;
	std::cerr << "example: ImageTools --reccrop --rec rec_mask --pro pro_mask --out rec_mask_out\n" << std::endl;

	std::cerr << "[ unsigned distance transform ]" << std::endl;
	std::cerr << "example: ImageTools {--dist | --sdist} --in imageA --out imageB\n" << std::endl;

	std::cerr << "[ apply intensity scaling ]" << std::endl;
	std::cerr << "example: ImageTools --intensityscale --in imageA --scale value1 --shift value2 --outtype dataType --out imageB\n" << std::endl;

	std::cerr << "[ apply Gaussian smoothing ]" << std::endl;
	std::cerr << "example: ImageTools --smooth --in imageA --sigma value --outtype float --out imageB\n" << std::endl;

	std::cerr << "[ compute bounding box mask for binary mask with padding ]" << std::endl;
	std::cerr << "example: ImageTools --boundmask --in imageA --padsize mm --out imageB\n" << std::endl;

	std::cerr << "[ mask out slices that are zero in a reference mask ]" << std::endl;
	std::cerr << "example: ImageTools --slicemaskout --in imageA --ref imageB [--world] --out outImage" << std::endl;
	std::cerr << "example: ImageTools --slicemaskout --in imageA --ref surfB --out outImage\n" << std::endl;

	std::cerr << "[ rotate image along one axis ]" << std::endl;
	std::cerr << "example: ImageTools --rotate --in imageA --center x,y,z --angle x,y,z --out imageB\n" << std::endl;

	std::cerr << "[ rotate image around center ]" << std::endl;
	std::cerr << "example: ImageTools --rotate2 --in imageA --init transform_path {--linear | --nearest} --out imageB\n" << std::endl;

	std::cerr << "[ label difference (label A - label B)]" << std::endl;
	std::cerr << "example: ImageTools --labeldiff --in imageA --ref imageB --outtype type --out outImage\n" << std::endl;

	std::cerr << "[ image masking with thresholding ]" << std::endl;
	std::cerr << "example: ImageTools --thresholdmask --in imageA --threshold 1800 --out out_path\n" << std::endl;

	std::cerr << "[ mask cropping with padding ]" << std::endl;
	std::cerr << "example: ImageTools --maskcrop --in imageA --padsize2 x,y,z --outtype type --out out_path\n" << std::endl;

	std::cerr << "[ image averaging ]" << std::endl;
	std::cerr << "example: ImageTools --average --prefix str --postfix str --start num --end num --out out_path\n" << std::endl;

	std::cerr << "[ output image header info ]" << std::endl;
	std::cerr << "example: ImageTools {--print_spacing | --print_size} --in path\n" << std::endl;

	std::cerr << "[ mask transform ]" << std::endl;
	std::cerr << "example: ImageTools --masktransform --in path --ref path --out path\n" << std::endl;

	std::cerr << "[ extract CT body ]" << std::endl;
	std::cerr << "example: ImageTools --body --in path --out path\n" << std::endl;

	std::cerr << "[ extract one label from multi-label map ]" << std::endl;
	std::cerr << "example: ImageTools --extractLabel --in path --idx val --out path\n" << std::endl;

	std::cerr << "[ flip image along axis ]" << std::endl;
	std::cerr << "example: ImageTools --flip --in path --idx val --out path\n" << std::endl;

	std::cerr << "[ axis correction ]" << std::endl;
	std::cerr << "example: ImageTools --axis_corr --in path --out path\n" << std::endl;

	std::cerr << "[ recursively find files that match the image dimension ]" << std::endl;
	std::cerr << "example: ImageTools --findimage --in folder --ext nii.gz --size x,y,z\n" << std::endl;

	std::cerr << "[ remove small connected components - 27 neighborhood ]" << std::endl;
	std::cerr << "example: ImageTools --remove_small --in path --threshold val --out path\n" << std::endl;

	std::cerr << "[ count voxels based on intensity values ]" << std::endl;
	std::cerr << "example: ImageTools --count --in path --idx val\n" << std::endl;

	std::cerr << "[ joint crop for two images ]" << std::endl;
	std::cerr << "example: ImageTools --jointCrop --in path --ref mask --out path;path\n" << std::endl;

	std::cerr << "[ permute image axises ]" << std::endl;
	std::cerr << "example: ImageTools --permute --in path --order 0,1,2 --out path\n" << std::endl;

	std::cerr << "[ MR global intensity rescale ]" << std::endl;
	std::cerr << "example: ImageTools --mrscale --in path --scale ratio --out path\n" << std::endl;
}

template <typename T>
void LinearTransform(std::string input_path, std::string ref_path, std::string transform_path, std::string output_path, IImageInterpolator<T,T>& interpolator, bool useWorld)
{
	mxImage<T> input, ref, out;
	if (!ImageHelper::ReadImage<T>(input_path.c_str(), input)) {
		std::cerr << "fails to read image from " << input_path << std::endl;
		exit(-1);
	}
	if (!ImageHelper::ReadImage<T>(ref_path.c_str(), ref)) {
		std::cerr << "fails to read image from " << ref_path << std::endl;
		exit(-1);
	}

	PDMAffineTransform transform;
	if (!transform.Load(transform_path.c_str())) {
		std::cerr << "fails to load transformation from " << transform_path << std::endl;
		exit(-1);
	}
	transform.Inverse();

	mxImageUtils::ApplyLinearTransformation(input, ref, transform, out, interpolator, useWorld);

	if (!ImageHelper::WriteImage<T,T>(out, output_path.c_str())) {
		std::cerr << "fails to write image to " << output_path << std::endl;
		exit(-1);
	}
}

template <typename TIn, typename TOut>
std::auto_ptr< IImageInterpolator<TIn,TOut> > CreateImageInterpolator(boost::program_options::variables_map& vm)
{
	if (vm.count("linear"))
		return std::auto_ptr< IImageInterpolator<TIn,TOut> >( new ImageLinearInterpolator<TIn,TOut> );
	else if(vm.count("nearest"))
		return std::auto_ptr< IImageInterpolator<TIn,TOut> >( new ImageNearestNeighborInterpolator<TIn,TOut> );
	else
		return std::auto_ptr< IImageInterpolator<TIn,TOut> >( new ImageNearestNeighborInterpolator<TIn,TOut> );
}

template <typename TIn, typename TOut>
void WriteImage_base(std::string input_path, std::string output_path)
{
    mxImage<TOut> image;
    if(!ImageHelper::ReadImage<TIn>(input_path.c_str(), image)) {
        std::cerr << "fail to read image from " << input_path << std::endl; exit(-1);
    }
    
    if(!ImageHelper::WriteImage<TOut,TOut>(image, output_path.c_str())) {
        std::cerr << "fail to write image from " << output_path << std::endl; exit(-1);
    }
}

template <typename TIn>
void WriteImage(std::string input_path, std::string output_path, std::string outtype)
{
    if(outtype == std::string("short")) {
        WriteImage_base<TIn, short>(input_path, output_path);
    } else if(outtype == std::string("ushort")) {
        WriteImage_base<TIn, unsigned short>(input_path, output_path);
    } else if(outtype == std::string("int")) {
        WriteImage_base<TIn, int>(input_path, output_path);
    } else if(outtype == std::string("uint")) {
        WriteImage_base<TIn, unsigned int>(input_path, output_path);
    } else if(outtype == std::string("float")) {
        WriteImage_base<TIn, float>(input_path, output_path);
    } else if(outtype == std::string("double")) {
        WriteImage_base<TIn, double>(input_path, output_path);
    } else if(outtype == std::string("uchar")) {
        WriteImage_base<TIn, unsigned char>(input_path, output_path);
    } else if(outtype == std::string("char")) {
        WriteImage_base<TIn, char>(input_path, output_path);
    } else {
        std::cerr << "unrecognized data type" << std::endl;
        exit(-1);
    }
}

template <typename TIn, typename TOut>
void Thresholding_base(std::string input_path, TIn threshold, std::string output_path)
{
	mxImage<TIn> image;
	if (!ImageHelper::ReadImage<TIn>(input_path.c_str(), image)) {
		std::cerr << "fail to read image from " << input_path << std::endl; exit(-1);
	}

	mxImage<TOut> outImage;
	mxImageUtils::Threshold(image, outImage, threshold, static_cast<TOut>(255), static_cast<TOut>(0));

	if (!ImageHelper::WriteImage<TOut,TOut>(outImage, output_path.c_str())) {
		std::cerr << "fail to write image to " << output_path << std::endl; exit(-1);
	}
}

template <typename TIn>
void Thresholding(std::string input_path, TIn threshold, std::string output_path, std::string outtype)
{
	if (outtype == std::string("short")) {
		Thresholding_base<TIn, short>(input_path, threshold, output_path);
	}
	else if (outtype == std::string("ushort")) {
		Thresholding_base<TIn, unsigned short>(input_path, threshold, output_path);
	}
	else if (outtype == std::string("int")) {
		Thresholding_base<TIn, int>(input_path, threshold, output_path);
	}
	else if (outtype == std::string("uint")) {
		Thresholding_base<TIn, unsigned int>(input_path, threshold, output_path);
	}
	else if (outtype == std::string("float")) {
		Thresholding_base<TIn, float>(input_path, threshold, output_path);
	}
	else if (outtype == std::string("double")) {
		Thresholding_base<TIn, double>(input_path, threshold, output_path);
	}
	else if (outtype == std::string("uchar")) {
		Thresholding_base<TIn, unsigned char>(input_path, threshold, output_path);
	}
	else if (outtype == std::string("char")) {
		Thresholding_base<TIn, char>(input_path, threshold, output_path);
	}
	else {
		std::cerr << "unrecognized data type" << std::endl;
		exit(-1);
	}
}


template <typename TIn, typename TOut>
void ResampleImage_base(std::string input_path, vect3<double> spacing, std::string output_path, boost::program_options::variables_map& vm)
{
	mxImage<TOut> image;
	if(!ImageHelper::ReadImage<TIn>(input_path.c_str(), image)) {
		std::cerr << "fail to read image from " << input_path << std::endl; exit(-1);
	}

	std::auto_ptr< IImageInterpolator<TIn, TOut> > interpolator = CreateImageInterpolator<TIn, TOut>(vm);

	mxImage<TOut> resampleImage;
	mxImageUtils::ResampleWithInterpolator<TIn,TOut>(image, spacing, *interpolator, resampleImage);

	if(!ImageHelper::WriteImage<TOut,TOut>(resampleImage, output_path.c_str())) {
		std::cerr << "fail to write image from " << output_path << std::endl; exit(-1);
	}
}

template <typename TIn>
void ResampleImage(std::string input_path, vect3<double> spacing, std::string output_path, std::string outtype, boost::program_options::variables_map& vm)
{
	if(outtype == std::string("short")) {
		ResampleImage_base<TIn, short>(input_path, spacing, output_path, vm);
	} else if(outtype == std::string("ushort")) {
		ResampleImage_base<TIn, unsigned short>(input_path, spacing, output_path, vm);
	} else if(outtype == std::string("int")) {
		ResampleImage_base<TIn, int>(input_path, spacing, output_path, vm);
	} else if(outtype == std::string("uint")) {
		ResampleImage_base<TIn, unsigned int>(input_path, spacing, output_path, vm);
	} else if(outtype == std::string("float")) {
		ResampleImage_base<TIn, float>(input_path, spacing, output_path, vm);
	} else if(outtype == std::string("double")) {
		ResampleImage_base<TIn, double>(input_path, spacing, output_path, vm);
	} else if(outtype == std::string("uchar")) {
		ResampleImage_base<TIn, unsigned char>(input_path, spacing, output_path, vm);
	} else if(outtype == std::string("char")) {
		ResampleImage_base<TIn, char>(input_path, spacing, output_path, vm);
	} else {
		std::cerr << "unrecognized data type" << std::endl;
		exit(-1);
	}
}

template <typename T>
void CropImage(std::string input_path, const vect3<int>& sp, const vect3<int>& ep, std::string output_path)
{
	assert_message(ep[0] >= sp[0] && ep[1] >= sp[1] && ep[2] >= sp[2], "ep must >= sp");

	mxImage<T> input_image;
	if (!ImageHelper::ReadImage<T,T>(input_path.c_str(), input_image)) {
		std::cerr << "fails to read image from " << input_path << std::endl;
		exit(-1);
	}

	mxImage<T> crop_image;
	mxImageUtils::Crop2(input_image, sp, ep, crop_image, static_cast<T>(0));

	if (!ImageHelper::WriteImage<T,T>(crop_image, output_path.c_str())) {
		std::cerr << "fails to write image to " << output_path << std::endl;
		exit(-1);
	}
}


template <typename T>
void Mask2Contour(std::string input_path, std::string output_path)
{
	mxImage<T> mask;
	if( !ImageHelper::ReadImage<T>(input_path.c_str(), mask) ) {
		std::cerr << "fail to read image from " << input_path << std::endl; exit(-1);
	}

	std::vector< vect3<float> > boundaryPoints;
	T threshold = 128;
	mxImageUtils::BoundaryPoints(mask, threshold, boundaryPoints, false);

	mask.Fill(0);
	for(unsigned int i = 0; i < boundaryPoints.size(); ++i) {
		int x, y, z;
		x = static_cast<int>(boundaryPoints[i].x);
		y = static_cast<int>(boundaryPoints[i].y);
		z = static_cast<int>(boundaryPoints[i].z);
		mask(x,y,z) = 255;
	}

	if( !ImageHelper::WriteImage<T,T>(mask, output_path.c_str()) ) {
		std::cerr << "fail to write image to " << output_path << std::endl; exit(-1);
	}
}

template <typename T1, typename T2>
void RTTSContourData2Mask_Base(std::string contourDataPath, std::string refImagePath, std::string outputPath)
{
	mxImage<T1> refImage;
	if (!ImageHelper::ReadImage<T1, T1>(refImagePath.c_str(), refImage)) {
		std::cerr << "fail to read ref image from " << refImagePath << std::endl;
		exit(-1);
	}

	mxImage<T2> outImage;
	ImageHelper::RTTSContourData2MaskVolume(contourDataPath.c_str(), refImage, outImage);

	if (!ImageHelper::WriteImage<T2, T2>(outImage, outputPath.c_str())) {
		std::cerr << "fail to write out image to " << outputPath << std::endl;
		exit(-1);
	}
}

template <typename T>
void RTTSContourData2Mask(std::string contourDataPath, std::string refImagePath, std::string outputPath)
{
	ImageHelper::PixelType pixeltype = ImageHelper::ReadPixelType(refImagePath.c_str());

	switch (pixeltype)
	{
	case BRIC::IDEA::FISH::ImageHelper::UCHAR: RTTSContourData2Mask_Base<unsigned char, T>(contourDataPath, refImagePath, outputPath); break;
	case BRIC::IDEA::FISH::ImageHelper::CHAR: RTTSContourData2Mask_Base<char, T>(contourDataPath, refImagePath, outputPath); break;
	case BRIC::IDEA::FISH::ImageHelper::USHORT: RTTSContourData2Mask_Base<unsigned short, T>(contourDataPath, refImagePath, outputPath); break;
	case BRIC::IDEA::FISH::ImageHelper::SHORT: RTTSContourData2Mask_Base<short, T>(contourDataPath, refImagePath, outputPath); break;
	case BRIC::IDEA::FISH::ImageHelper::UINT: RTTSContourData2Mask_Base<unsigned int, T>(contourDataPath, refImagePath, outputPath); break;
	case BRIC::IDEA::FISH::ImageHelper::INT: RTTSContourData2Mask_Base<int, T>(contourDataPath, refImagePath, outputPath); break;
	case BRIC::IDEA::FISH::ImageHelper::ULONG: RTTSContourData2Mask_Base<unsigned long, T>(contourDataPath, refImagePath, outputPath); break;
	case BRIC::IDEA::FISH::ImageHelper::LONG: RTTSContourData2Mask_Base<long, T>(contourDataPath, refImagePath, outputPath); break;
	case BRIC::IDEA::FISH::ImageHelper::FLOAT: RTTSContourData2Mask_Base<float, T>(contourDataPath, refImagePath, outputPath); break;
	case BRIC::IDEA::FISH::ImageHelper::DOUBLE: RTTSContourData2Mask_Base<double, T>(contourDataPath, refImagePath, outputPath); break;
	default: std::cerr << "unrecognized pixel type" << std::endl; break;
	}

}

template <typename T>
void MergeMasks(std::string prefix, const std::vector<std::string>& tags, std::string postfix, std::string out_path)
{
	mxImage<T> out_image;

	for (int i = static_cast<int>(tags.size() - 1); i >= 0; --i)
	{
		std::string mask_path = prefix + tags[i] + postfix;

		mxImage<T> mask_image;
		if (!ImageHelper::ReadImage<T, T>(mask_path.c_str(), mask_image)) {
			std::cerr << "fails to read image from " << mask_path << std::endl; exit(-1);
		}

		vect3<unsigned int> image_size = mask_image.GetImageSize();

		if (i == (tags.size() - 1))
		{
			out_image.CopyImageInfo(mask_image);
			out_image.SetImageSize(image_size);
			out_image.Fill(0);
		}
		else
		{
			assert_message(out_image.GetImageSize() == image_size, "inconsistent image sizes");
		}


		for (unsigned int z = 0; z < image_size[2]; ++z) {
			for (unsigned int y = 0; y < image_size[1]; ++y) {
				for (unsigned int x = 0; x < image_size[0]; ++x) {
					if (mask_image(x, y, z))
						out_image(x, y, z) = i + 1;
				}
			}
		}

		if (!ImageHelper::WriteImage<T, T>(out_image, out_path.c_str())) {
			std::cerr << "fails to write image to " << out_path << std::endl; exit(-1);
		}
	}
}

template <typename T>
void rectum_truncate(const char* rectum_mask_path, const char* pro_mask_path, const char* out_mask_path)
{
	mxImage<T> rec_mask, pro_mask;
	if ( !ImageHelper::ReadImage<T,T>(rectum_mask_path, rec_mask) ) {
		std::cerr << "cannot open file for read: " << rectum_mask_path << std::endl;
		exit(-1);
	}
	if ( !ImageHelper::ReadImage<T,T>(pro_mask_path, pro_mask) ) {
		std::cerr << "cannot open file for read: " << pro_mask_path << std::endl;
		exit(-1);
	}

	vect3<unsigned int> image_size1 = rec_mask.GetImageSize();
	vect3<unsigned int> image_size2 = pro_mask.GetImageSize();

	if (image_size1[0] != image_size2[0] || image_size1[1] != image_size2[1] || image_size1[2] != image_size2[2]) {
		err_message("the sizes of two mask images mismatch");
	}

	std::vector<int> slice_tags;
	slice_tags.resize(image_size1[2]);

	for (unsigned int z = 0; z < image_size1[2]; ++z)
	{
		bool is_empty = true;
		for (unsigned int y = 0; y < image_size1[1]; ++y) {
			for (unsigned int x = 0; x < image_size1[0]; ++x) {
				if (pro_mask(x,y,z) > 0)
				{
					is_empty = false;
					break;
				}
			}
			if (!is_empty) break;
		}
		slice_tags[z] = (is_empty == false);
	}

	for (unsigned int z = 0; z < image_size1[2]; ++z)
	{
		if (!slice_tags[z])
		{
			for (unsigned int y = 0; y < image_size1[1]; ++y) {
				for (unsigned int x = 0; x < image_size1[0]; ++x) {
					rec_mask(x, y, z) = 0;
				}
			}
		}
	}

	if (!ImageHelper::WriteImage<T, T>(rec_mask, out_mask_path)) {
		std::cerr << "cannot open file for write: " << out_mask_path << std::endl;
		exit(-1);
	}
}

template <typename T>
void distance_transform(const char* input_path, const char* output_path, bool isSigned)
{
	mxImage<T> input_image;
	if (!ImageHelper::ReadImage<T,T>(input_path, input_image))
		err_message("fail to load input image");

	mxImage<float> distance_image;
	ImageHelper::DistanceImage(input_image, distance_image);

	if (!isSigned)
	{
		vect3<unsigned int> image_size = distance_image.GetImageSize();
		for (int z = 0; z < image_size[2]; ++z)
			for (int y = 0; y < image_size[1]; ++y)
				for (int x = 0; x < image_size[0]; ++x)
					distance_image(x, y, z) = std::abs(distance_image(x, y, z));
	}

	if (!ImageHelper::WriteImage<float,float>(distance_image, output_path))
		err_message("fail to save output distance image");
}

template <typename T1, typename T2>
void ScaleIntensity(const char* input_path, double scale, double shift, const char* output_path)
{
	mxImage<T1> input_image;
	if (!ImageHelper::ReadImage<T1, T1>(input_path, input_image)) 
		err_message("fail to read image from disk");
	
	mxImage<T2> output_image;
	output_image.CopyImageInfo(input_image);
	
	vect3<unsigned int> image_size = input_image.GetImageSize();
	output_image.SetImageSize(image_size);

	for (unsigned int z = 0; z < image_size[2]; ++z)
	{
		for (unsigned int y = 0; y < image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < image_size[0]; ++x)
			{
				T1 val = input_image(x, y, z);
				double val2 = val * scale + shift;
				T2 val3 = static_cast<T2>(val2);
				output_image(x, y, z) = val3;
			}
		}
	}

	if (!ImageHelper::WriteImage<T2, T2>(output_image, output_path))
		err_message("fail to write image to disk");
}

template <typename T1>
void ScaleIntensityGeneral(const std::string& outtype, const char* input_path, double scale, double shift, const char* output_path)
{
	if (outtype == "uchar")
	{
		ScaleIntensity<T1, unsigned char>(input_path, scale, shift, output_path);
	}
	else if (outtype == "char")
	{
		ScaleIntensity<T1, char>(input_path, scale, shift, output_path);
	}
	else if (outtype == "ushort")
	{
		ScaleIntensity<T1, unsigned short>(input_path, scale, shift, output_path);
	}
	else if (outtype == "short")
	{
		ScaleIntensity<T1, short>(input_path, scale, shift, output_path);
	}
	else if (outtype == "uint")
	{
		ScaleIntensity<T1, unsigned int>(input_path, scale, shift, output_path);
	}
	else if (outtype == "int")
	{
		ScaleIntensity<T1, int>(input_path, scale, shift, output_path);
	}
	else if (outtype == "ulong")
	{
		ScaleIntensity<T1, unsigned long>(input_path, scale, shift, output_path);
	}
	else if (outtype == "long")
	{
		ScaleIntensity<T1, long>(input_path, scale, shift, output_path);
	}
	else if (outtype == "float")
	{
		ScaleIntensity<T1, float>(input_path, scale, shift, output_path);
	}
	else if (outtype == "double")
	{
		ScaleIntensity<T1, double>(input_path, scale, shift, output_path);
	}
	else {
		err_message("unrecognized output pixel type");
	}
}

template <typename T1, typename T2>
void SmoothImage(const char* input_path, double sigma, const char* output_path)
{
	mxImage<T1> image;
	if (!ImageHelper::ReadImage<T1, T1>(input_path, image))
		err_message("fail to read image from disk");

	mxImage<T2> outimage;
	mxImageUtils::GaussianSmooth<T1, T2>(image, outimage, sigma);

	if (!ImageHelper::WriteImage<T2, T2>(outimage, output_path))
		err_message("fail to write image to disk");
}

template  <typename T1>
void SmoothImageGeneral(const char* input_path, double sigma, std::string outtype, const char* output_path)
{
	if (outtype == "uchar")
	{
		SmoothImage<T1, unsigned char>(input_path, sigma, output_path);
	}
	else if (outtype == "char")
	{
		SmoothImage<T1, char>(input_path, sigma, output_path);
	}
	else if (outtype == "ushort")
	{
		SmoothImage<T1, unsigned short>(input_path, sigma, output_path);
	}
	else if (outtype == "short")
	{
		SmoothImage<T1, short>(input_path, sigma, output_path);
	}
	else if (outtype == "uint")
	{
		SmoothImage<T1, unsigned int>(input_path, sigma, output_path);
	}
	else if (outtype == "int")
	{
		SmoothImage<T1, int>(input_path, sigma, output_path);
	}
	else if (outtype == "ulong")
	{
		SmoothImage<T1, unsigned long>(input_path, sigma, output_path);
	}
	else if (outtype == "long")
	{
		SmoothImage<T1, long>(input_path, sigma, output_path);
	}
	else if (outtype == "float")
	{
		SmoothImage<T1, float>(input_path, sigma, output_path);
	}
	else if (outtype == "double")
	{
		SmoothImage<T1, double>(input_path, sigma, output_path);
	}
	else {
		err_message("unrecognized output pixel type");
	}
}

template <typename T>
void BoundingBoxMask(std::string input_path, double padsize, std::string output_path)
{
	mxImage<T> image;
	if (!ImageHelper::ReadImage<T,T>(input_path.c_str(), image))
	{
		std::cerr << "fail to read image from file " << input_path << std::endl;
		exit(-1);
	}

	vect3<int> sp, ep;
	mxImageUtils::BoundingBox(image, sp, ep);

	vect3<int> padvoxels;
	for (int i = 0; i < 3; ++i)
		padvoxels[i] = static_cast<int>(padsize / image.GetSpacing()[i] + 0.5);

	for (int i = 0; i < 3; ++i)
	{
		sp[i] -= padvoxels[i];
		ep[i] += padvoxels[i];

		if (sp[i] < 0)
			sp[i] = 0;
		if (ep[i] >= image.GetImageSize()[i])
			ep[i] = image.GetImageSize()[i] - 1;
	}

	mxImage<unsigned char> maskImage;
	maskImage.CopyImageInfo(image);
	maskImage.SetImageSize(image.GetImageSize());
	maskImage.Fill(0);

	for (int z = sp[2]; z <= ep[2]; ++z)
	{
		for (int y = sp[1]; y <= ep[1]; ++y)
		{
			for (int x = sp[0]; x <= ep[0]; ++x)
			{
				maskImage(x, y, z) = 255;
			}
		}
	}

	if (!ImageHelper::WriteImage<unsigned char, unsigned char>(maskImage, output_path.c_str()))
	{
		std::cerr << "fail to write image to file " << output_path << std::endl;
		exit(-1);
	}
}

template <typename T1, typename T2>
void MaskOutSlices_base(std::string input_path, std::string ref_path, bool isWorld, std::string output_path)
{
	mxImage<T1> input_image;
	if (!ImageHelper::ReadImage<T1,T1>(input_path.c_str(), input_image))
	{
		std::cerr << "fail to read image from " << input_path << std::endl;
		exit(-1);
	}
	vect3<unsigned int> input_image_size = input_image.GetImageSize();

	mxImage<T2> ref_image;
	if (!ImageHelper::ReadImage<T2,T2>(ref_path.c_str(), ref_image))
	{
		std::cerr << "fail to read image from " << ref_path << std::endl;
		exit(-1);
	}
	vect3<unsigned int> ref_image_size = ref_image.GetImageSize();

	if (!isWorld)
		assert_message(input_image_size == ref_image_size, "image size of input and reference image must be the same");

	unsigned int slice_start = 0, slice_end = 0, ref_start = 0, ref_end = 0, in_start = 0, in_end = 0;
	mxImageUtils::FindSliceRange(ref_image, ref_start, ref_end);
	mxImageUtils::FindSliceRange(input_image, in_start, in_end);

	if (isWorld)
	{
		vect3<double> tmp_start(0, 0, ref_start), tmp_end(0, 0, ref_end);
		mxImageUtils::Voxel2World(ref_image, tmp_start);
		mxImageUtils::Voxel2World(ref_image, tmp_end);

		mxImageUtils::World2Voxel(input_image, tmp_start);
		mxImageUtils::World2Voxel(input_image, tmp_end);

		ref_start = static_cast<unsigned int>(tmp_start[2] + 0.5);
		ref_end = static_cast<unsigned int>(tmp_end[2] + 0.5);
	}

	if (ref_start < in_start)
		slice_start = in_start;
	else
		slice_start = ref_start;

	if (ref_end > in_end)
		slice_end = in_end;
	else
		slice_end = ref_end;

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(slice_start); ++z)
	{
		for (unsigned int y = 0; y < input_image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < input_image_size[0]; ++x)
			{
				input_image(x, y, z) = 0;
			}
		}
	}

	#pragma omp parallel for
	for (int z = slice_end; z < static_cast<int>(input_image_size[2]); ++z)
	{
		for (unsigned int y = 0; y < input_image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < input_image_size[0]; ++x)
			{
				input_image(x, y, z) = 0;
			}
		}
	}

	if (!ImageHelper::WriteImage<T1,T1>(input_image, output_path.c_str()))
	{
		std::cerr << "fail to write image to " << output_path << std::endl;
		exit(-1);
	}
}

template <typename T1>
void MaskOutSlices(std::string input_path, std::string ref_path, bool isWorld, std::string output_path)
{
	ImageHelper::PixelType type = ImageHelper::ReadPixelType(ref_path.c_str());

	switch (type)
	{
	case BRIC::IDEA::FISH::ImageHelper::UCHAR: MaskOutSlices_base<T1,unsigned char>(input_path, ref_path, isWorld, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::CHAR: MaskOutSlices_base<T1,char>(input_path, ref_path, isWorld, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::USHORT: MaskOutSlices_base<T1,unsigned short>(input_path, ref_path, isWorld, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::SHORT: MaskOutSlices_base<T1, short>(input_path, ref_path, isWorld, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::UINT: MaskOutSlices_base<T1, unsigned int>(input_path, ref_path, isWorld, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::INT: MaskOutSlices_base<T1, int>(input_path, ref_path, isWorld, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::ULONG: MaskOutSlices_base<T1, unsigned long>(input_path, ref_path, isWorld, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::LONG: MaskOutSlices_base<T1, long>(input_path, ref_path, isWorld, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::FLOAT: MaskOutSlices_base<T1, float>(input_path, ref_path, isWorld, output_path); break;
	case BRIC::IDEA::FISH::ImageHelper::DOUBLE: MaskOutSlices_base<T1, double>(input_path, ref_path, isWorld, output_path); break;
	default: err_message("unrecognized pixel type");  break;
	}
}

template <typename T1>
void MaskOutSlices_Surface(std::string input_path, const Surface& surf, std::string output_path)
{
	mxImage<T1> input_image;
	if (!ImageHelper::ReadImage<T1, T1>(input_path.c_str(), input_image))
	{
		std::cerr << "fail to read image from " << input_path << std::endl;
		exit(-1);
	}
	vect3<unsigned int> input_image_size = input_image.GetImageSize();

	mxImage<unsigned char> binary_image;
	unsigned char outside_val = 0, inside_val = 255;
	SurfaceUtils::CarveSurface(surf, input_image, outside_val, inside_val, binary_image);

	unsigned int ref_start, ref_end;
	mxImageUtils::FindSliceRange(binary_image, ref_start, ref_end);

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(ref_start); ++z)
	{
		for (unsigned int y = 0; y < input_image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < input_image_size[0]; ++x)
			{
				input_image(x, y, z) = 0;
			}
		}
	}

	#pragma omp parallel for
	for (int z = ref_end; z < static_cast<int>(input_image_size[2]); ++z)
	{
		for (unsigned int y = 0; y < input_image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < input_image_size[0]; ++x)
			{
				input_image(x, y, z) = 0;
			}
		}
	}

	if (!ImageHelper::WriteImage<T1, T1>(input_image, output_path.c_str()))
	{
		std::cerr << "fail to write image to " << output_path << std::endl;
		exit(-1);
	}
}

template <typename T>
void RotateImage(std::string input_path, vect3<double>& center, const vect3<double>& angle, std::string output_path)
{
	mxImage<T> image;
	if (!ImageHelper::ReadImage<T,T>(input_path.c_str(), image))
	{
		std::cerr << "fail to read image from " << input_path << std::endl;
		exit(-1);
	}

	mxImage<T> outImage;
	mxImageUtils::RotateImage<T>(image, center, angle, outImage);

	if (!ImageHelper::WriteImage<T,T>(outImage, output_path.c_str()))
	{
		std::cerr << "fail to write image to " << output_path << std::endl;
		exit(-1);
	}
}

template <typename T>
void RotateImage2(std::string input_path, const vnl_matrix<double>& rot_mat, bool nearest_or_linear, std::string output_path)
{
	mxImage<T> image;
	if (!ImageHelper::ReadImage<T,T>(input_path.c_str(), image))
	{
		std::cerr << "fail to read image from " << input_path << std::endl;
		exit(-1);
	}

	vnl_matrix<double> inv_rot = vnl_matrix_inverse<double>(rot_mat).inverse();

	mxImage<T> outImage;
	mxImageUtils::RotateImage(image, inv_rot, nearest_or_linear, outImage);

	if (!ImageHelper::WriteImage<T,T>(outImage, output_path.c_str()))
	{
		std::cerr << "fail to write image to " << output_path << std::endl;;
		exit(-1);
	}
}

template <typename T1, typename T2>
void LabelDiff_base(std::string input_path, std::string ref_path, std::string output_path)
{
	mxImage<unsigned char> image1, image2;
	if (!ImageHelper::ReadImage<T1,unsigned char>(input_path.c_str(), image1))
		err_message("fail to read input image");
	
	if (!ImageHelper::ReadImage<T1, unsigned char>(ref_path.c_str(), image2))
		err_message("fail to read ref image");

	assert_message(image1.GetImageSize() == image2.GetImageSize(), "image size mismatch");

	mxImage<unsigned char> out_image;
	out_image.CopyImageInfo(image1);
	out_image.SetImageSize(image1.GetImageSize());
	out_image.Fill(0);

	vect3<unsigned int> image_size = out_image.GetImageSize();
	
	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		for (unsigned int y = 0; y < image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < image_size[0]; ++x)
			{
				if (image1(x, y, z) != 0 && image2(x, y, z) == 0)
					out_image(x, y, z) = 255;
			}
		}
	}

	if (!ImageHelper::WriteImage<unsigned char, T2>(out_image, output_path.c_str()))
		err_message("fail to write output image");

}

template <typename T>
void LabelDiff(std::string input_path, std::string ref_path, std::string out_type, std::string output_path)
{
	if (out_type == "uchar")
		LabelDiff_base<T, unsigned char>(input_path, ref_path, output_path);
	else if (out_type == "char")
		LabelDiff_base<T, char>(input_path, ref_path, output_path);
	else if (out_type == "uint")
		LabelDiff_base<T, unsigned int>(input_path, ref_path, output_path);
	else if (out_type == "int")
		LabelDiff_base<T, int>(input_path, ref_path, output_path);
	else if (out_type == "ushort")
		LabelDiff_base<T, unsigned short>(input_path, ref_path, output_path);
	else if (out_type == "short")
		LabelDiff_base<T, short>(input_path, ref_path, output_path);
	else if (out_type == "ulong")
		LabelDiff_base<T, unsigned long>(input_path, ref_path, output_path);
	else if (out_type == "long")
		LabelDiff_base<T, long>(input_path, ref_path, output_path);
	else if (out_type == "float")
		LabelDiff_base<T, float>(input_path, ref_path, output_path);
	else if (out_type == "double")
		LabelDiff_base<T, double>(input_path, ref_path, output_path);
	else
		err_message("unrecognized pixel type");
}

template <typename T>
void ThresholdMask(std::string input_path, double threshold, std::string output_path)
{
	mxImage<T> image;
	if (!ImageHelper::ReadImage<T,T>(input_path.c_str(), image))
	{
		std::cerr << "fail to read image from " << input_path << std::endl;
		exit(-1);
	}
	
	vect3<unsigned int> image_size = image.GetImageSize();

	#pragma omp parallel for schedule(dynamic)
	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
			{
				if (static_cast<double>(image(x, y, z)) < threshold)
					image(x, y, z) = 0;
			}
		}
	}

	if (!ImageHelper::WriteImage<T,T>(image, output_path.c_str()))
	{
		std::cerr << "fail to write image to " << output_path << std::endl;
		exit(-1);
	}
}

template <typename T1, typename T2>
void MaskCrop(std::string input_path, const vect3<int>& pad_size,  std::string output_path)
{
	mxImage<T1> mask_image;

	if (!ImageHelper::ReadImage<T1,T1>(input_path.c_str(), mask_image))
	{
		std::cerr << "fails to read image from " << input_path << std::endl;
		exit(-1);
	}

	vect3<int> sp, ep;
	mxImageUtils::BoundingBox(mask_image, sp, ep);

	vect3<unsigned int> image_size = mask_image.GetImageSize();
	for (int i = 0; i < 3; ++i)
	{
		sp[i] -= pad_size[i];
		ep[i] += pad_size[i];
	}

	for (int i = 0; i < 3; ++i)
	{
		if (sp[i] < 0)
			sp[i] = 0;
		if (ep[i] >= image_size[i])
			ep[i] = image_size[i] - 1;
	}

	mxImage<T1> crop_image;
	mxImageUtils::Crop2<T1,T1>(mask_image, sp, ep, crop_image, 0);

	if (!ImageHelper::WriteImage<T1,T2>(crop_image, output_path.c_str()))
	{
		std::cerr << "fails to write image to " << output_path << std::endl;
		exit(-1);
	}
}

template <typename T1>
void MaskCrop(std::string input_path, const vect3<int>& pad_size, std::string outtype, std::string output_path)
{
	if (outtype == "uchar")
	{
		MaskCrop<T1, unsigned char>(input_path, pad_size, output_path);
	}
	else if (outtype == "char")
	{
		MaskCrop<T1, char>(input_path, pad_size, output_path);
	}
	else if (outtype == "ushort")
	{
		MaskCrop<T1, unsigned short>(input_path, pad_size, output_path);
	}
	else if (outtype == "short")
	{
		MaskCrop<T1, short>(input_path, pad_size, output_path);
	}
	else if (outtype == "uint")
	{
		MaskCrop<T1, unsigned int>(input_path, pad_size, output_path);
	}
	else if (outtype == "int")
	{
		MaskCrop<T1, int>(input_path, pad_size, output_path);
	}
	else if (outtype == "ulong")
	{
		MaskCrop<T1, unsigned long>(input_path, pad_size, output_path);
	}
	else if (outtype == "long")
	{
		MaskCrop<T1, long>(input_path, pad_size, output_path);
	}
	else if (outtype == "float")
	{
		MaskCrop<T1, float>(input_path, pad_size, output_path);
	}
	else if (outtype == "double")
	{
		MaskCrop<T1, double>(input_path, pad_size, output_path);
	}
	else
	{
		err_message("unrecognized pixel type");
	}

}

template <typename T>
void ImageAverage(std::string prefix, int start, int end, std::string postfix, std::string out_path)
{
	if (end < start)
		err_message("end < start");

	mxImage < double > image;

	for (int i = start; i <= end; ++i)
	{
		std::string input_path = prefix + stringUtils::num2str(i) + postfix;

		if (i == start)
		{
			if (!ImageHelper::ReadImage<T,double>(input_path.c_str(), image))
			{
				std::cerr << "fails to read image from " << input_path << std::endl;
				exit(-1);
			}
		}
		else
		{
			mxImage<double> tmp_image;
			if (!ImageHelper::ReadImage<T,double>(input_path.c_str(), tmp_image))
			{
				std::cerr << "fails to read image from " << input_path << std::endl;
				exit(-1);
			}

			if (image.GetImageSize() != tmp_image.GetImageSize())
			{
				std::cerr << "image size mismatch: " << input_path << std::endl;
				exit(-1);
			}

			vect3<unsigned int> image_size = image.GetImageSize();

			for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
			{
				for (unsigned int y = 0; y < image_size[1]; ++y)
				{
					for (unsigned int x = 0; x < image_size[0]; ++x)
					{
						image(x, y, z) += tmp_image(x, y, z);
					}
				}
			}
		}
	}

	vect3<unsigned int> image_size = image.GetImageSize();
	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		for (unsigned int y = 0; y < image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < image_size[0]; ++x)
			{
				image(x, y, z) /= (end - start + 1);
			}
		}
	}

	if (!ImageHelper::WriteImage<double,double>(image, out_path.c_str()))
	{
		std::cerr << "fails to write image " << out_path << std::endl;
		exit(-1);
	}
}

template <typename T>
void ImageFlip(std::string input_path, int idx, std::string output_path)
{
	mxImage<T> image;
	if (!ImageHelper::ReadImage<T,T>(input_path.c_str(), image)) 
	{
		std::cerr << "fail to load image from " << input_path << std::endl;
		exit(-1);
	}

	mxImageUtils::FlipImage(image, idx);

	if (!ImageHelper::WriteImage<T,T>(image, output_path.c_str()))
	{
		std::cerr << "fail to write image to " << output_path << std::endl;
		exit(-1);
	}
}

template <typename T>
void AxisCorrection(std::string input_path, std::string output_path)
{
	mxImage<T> image;

	if (!ImageHelper::ReadImage<T,T>(input_path.c_str(), image)) {
		std::cerr << "fails to read image from " << input_path << std::endl;
		exit(-1);
	}

	double axis[9] = { 0 };
	const double* orig_axis = image.GetAxis();
	for (int i = 0; i < 9; ++i)
	{
		if (orig_axis[i] < 0)
			axis[i] = -orig_axis[i];
		else
			axis[i] = orig_axis[i];
	}

	image.SetAxis(axis);

	if (!ImageHelper::WriteImage<T,T>(image, output_path.c_str())) {
		std::cerr << "fails to write image to" << output_path << std::endl;
		exit(-1);
	}
}

void FindImage(std::string folder, std::string size, std::string format)
{
	std::vector<std::string> tokens = stringUtils::Split(size, ',');
	assert_message(tokens.size() == 3, "size must be 3-dimensional");

	vect3<unsigned int> search_imagesize;
	search_imagesize[0] = stringUtils::str2num<unsigned int>(tokens[0]);
	search_imagesize[1] = stringUtils::str2num<unsigned int>(tokens[1]);
	search_imagesize[2] = stringUtils::str2num<unsigned int>(tokens[2]);

	boost::filesystem::path root(folder);
	if (!boost::filesystem::exists(root) || !boost::filesystem::is_directory(root))
		return;

	boost::filesystem::recursive_directory_iterator it(root);
	boost::filesystem::recursive_directory_iterator endit;

	while (it != endit)
	{
		if (!boost::filesystem::is_directory(*it) && it->path().extension() == std::string(".") + format)
		{
			mxImage<unsigned short> header;
			if (!ImageHelper::ReadImageHeader(it->path().string().c_str(), header))
			{
				std::cerr << "fails to read header from " << it->path() << std::endl;
				exit(-1);
			}

			if (header.GetImageSize() == search_imagesize)
				std::cout << it->path() << std::endl;
		}

		++it;
	}
}

template <typename T1, typename T2>
void JointCrop(std::string input_path, std::string mask_path, std::string output_path1, std::string output_path2)
{
	mxImage<T1> input_image;
	mxImage<T2> mask_image;

	if (!ImageHelper::ReadImage<T1,T1>(input_path.c_str(), input_image)) {
		std::cerr << "fails to load image from " << input_path << std::endl;
		return;
	}

	if (!ImageHelper::ReadImage<T2,T2>(mask_path.c_str(), mask_image)) {
		std::cerr << "fails to load image from " << mask_path << std::endl;
		return;
	}

	vect3<int> sp, ep;
	mxImageUtils::BoundingBox(mask_image, sp, ep);

	mxImage<T2> cropped_mask_image;
	mxImageUtils::Crop2(mask_image, sp, ep, cropped_mask_image, static_cast<T2>(0));

	mxImage<T2> cropped_input_image;
	mxImageUtils::Crop2(input_image, sp, ep, cropped_input_image, static_cast<T2>(0));

	if (!ImageHelper::WriteImage<T1,T1>(cropped_input_image, output_path1.c_str())) {
		std::cerr << "fails to output image to " << output_path1 << std::endl;
		return;
	}

	if (!ImageHelper::WriteImage<T2,T2>(cropped_mask_image, output_path2.c_str())) {
		std::cerr << "fails to output image to " << output_path2 << std::endl;
		return;
	}
}

template <typename T>
void PermuteAxis(std::string input_path, const std::vector<unsigned int>& order, std::string output_path)
{
	mxImage<T> image;
	if (!ImageHelper::ReadImage<T, T>(input_path.c_str(), image)) {
		std::cerr << "fails to read image from " << input_path << std::endl;
		exit(-1);
	}

	mxImage<T> outImage;
	ImageHelper::PermuteAxes<T>(image, order, outImage);

	if (!ImageHelper::WriteImage<T, T>(outImage, output_path.c_str())) {
		std::cerr << "fails to write image to " << output_path << std::endl;
		exit(-1);
	}
}

int main(int argc, char** argv)
{
	try {

		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display helper information");

		options_description functional("Functional options (must pick one)");
		functional.add_options()("apply", "apply transformation to an image");
        functional.add_options()("format", "convert image format/change data type");
		functional.add_options()("resample", "resample image into isotropic spacing");
		functional.add_options()("contour", "convert a mask image into a contour image");
		functional.add_options()("rtts", "convert a rtts txt file to mask image");
		functional.add_options()("merge", "merge bladder, prostate, rectum masks");
		functional.add_options()("crop", "crop a sub-region from 3D image");
		functional.add_options()("reccrop", "crop the rectum mask according to prostate mask");
		functional.add_options()("dist", "compute unsigned distance image");
		functional.add_options()("sdist", "compute signed distance image");
		functional.add_options()("intensityscale", "linear scale intensity");
		functional.add_options()("smooth", "Gaussian smooth");
		functional.add_options()("boundmask", "compute bounding box mask");
		functional.add_options()("slicemaskout", "mask out slices that are zero in reference mask"); 
		functional.add_options()("rotate", "rotate image along axises");
		functional.add_options()("rotate2", "rotate image around center");
		functional.add_options()("labeldiff", "label difference");
		functional.add_options()("thresholdmask", "mask the image by thresholding");
		functional.add_options()("thresholding", "thresholding");
		functional.add_options()("maskcrop", "crop mask with padding");
		functional.add_options()("average", "image averaging");
		functional.add_options()("print_size", "print out image size");
		functional.add_options()("print_spacing", "print out image spacing");
		functional.add_options()("masktransform", "transform mask images");
		functional.add_options()("body", "extract CT body");
		functional.add_options()("extractLabel", "extract label map");
		functional.add_options()("flip", "flip image along axis");
		functional.add_options()("axis_corr", "correct axis");
		functional.add_options()("findimage", "find image");
		functional.add_options()("remove_small", "remove small connected component");
		functional.add_options()("count", "count number of voxels");
		functional.add_options()("jointCrop", "joint crop of two images");
		functional.add_options()("triMerge", "special merge function for Tri's CT Prediction");
		functional.add_options()("triMerge2", "special merge function 2 for Tri's CT Prediction");
		functional.add_options()("permute", "permute image axes");
		functional.add_options()("mrscale", "MR intensity maximum for rescaling");

		options_description params("Parameter options");
		params.add_options()("in", value<std::string>(), "input image");
		params.add_options()("ref", value<std::string>(), "reference image");
		params.add_options()("out", value<std::string>(), "output image");
		params.add_options()("init", value<std::string>(), "linear transformation txt");
		params.add_options()("type", value<std::string>(), "data type (e.g., short)");
        params.add_options()("intype", value<std::string>(), "input data type");
        params.add_options()("outtype", value<std::string>(), "output data type");
		params.add_options()("useWorld", "use world coordinate (by default use voxel coordinate)");
		params.add_options()("linear", "use linear interpolation");
		params.add_options()("nearest", "use nearest neighbor interpolation");
		params.add_options()("spacing", value< std::vector<double> >()->multitoken(), "isotropic spacing");
		params.add_options()("pro", value<std::string>(), "prostate mask path");
		params.add_options()("rec", value<std::string>(), "rectum mask path");
		params.add_options()("scale", value<double>(), "scale factor");
		params.add_options()("shift", value<double>(), "shift factor");
		params.add_options()("sigma", value<double>(), "sigma in voxel units");
		params.add_options()("padsize", value<double>(), "padding size in mm");
		params.add_options()("padsize2", value<std::string>(), "padding size in voxel x voxel x voxel");
		params.add_options()("center", value<std::string>(), "center in voxels");
		params.add_options()("angle", value<std::string>(), "angles in degrees");
		params.add_options()("threshold", value<double>(), "threshold value");
		params.add_options()("prefix", value<std::string>(), "prefix string");
		params.add_options()("postfix", value<std::string>(), "postfix string");
		params.add_options()("start", value<int>(), "start index");
		params.add_options()("end", value<int>(), "end index");
		params.add_options()("idx", value<int>(), "value");
		params.add_options()("tag", value<std::string>(), "label tags");
		params.add_options()("size", value<std::string>(), "get a 3D size");
		params.add_options()("ext", value<std::string>(), "image format");
		params.add_options()("coord", value<std::string>(), "coordinate");
		params.add_options()("order", value<std::string>(), "axis order");

		options_description cmd_options;
		cmd_options.add(generic).add(functional).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if( vm.count("help") || argc == 1 )
		{
			std::cerr << cmd_options << std::endl;
			print_help();
			return -1;
		}

		if (vm.count("apply"))
		{
			std::string input_path, ref_path, out_path, transform_path, type;
			
			input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			ref_path = BoostCmdHelper::Get<std::string>(vm, "ref");
			out_path = BoostCmdHelper::Get<std::string>(vm, "out");
			transform_path = BoostCmdHelper::Get<std::string>(vm, "init");
			type = BoostCmdHelper::Get<std::string>(vm, "type");
			bool useWorld = vm.count("useWorld") > 0;
			
			if(type == std::string("short")) {
				std::auto_ptr< IImageInterpolator<short, short> > interpolator = CreateImageInterpolator<short, short>(vm);
				LinearTransform<short>(input_path, ref_path, transform_path, out_path, *interpolator, useWorld);
			} else if(type == std::string("ushort")) {
				std::auto_ptr< IImageInterpolator<unsigned short, unsigned short> > interpolator = CreateImageInterpolator<unsigned short, unsigned short>(vm);
				LinearTransform<unsigned short>(input_path, ref_path, transform_path, out_path, *interpolator, useWorld);
			} else if(type == std::string("int")) {
				std::auto_ptr< IImageInterpolator<int, int> > interpolator = CreateImageInterpolator<int, int>(vm);
				LinearTransform<int>(input_path, ref_path, transform_path, out_path, *interpolator, useWorld);
			} else if(type == std::string("uint")) {
				std::auto_ptr< IImageInterpolator<unsigned int, unsigned int> > interpolator = CreateImageInterpolator<unsigned int, unsigned int>(vm);
				LinearTransform<unsigned int>(input_path, ref_path, transform_path, out_path, *interpolator, useWorld);
			} else if(type == std::string("float")) {
				std::auto_ptr< IImageInterpolator<float, float> > interpolator = CreateImageInterpolator<float, float>(vm);
				LinearTransform<float>(input_path, ref_path, transform_path, out_path, *interpolator, useWorld);
			} else if(type == std::string("double")) {
				std::auto_ptr< IImageInterpolator<double, double> > interpolator = CreateImageInterpolator<double, double>(vm);
				LinearTransform<double>(input_path, ref_path, transform_path, out_path, *interpolator, useWorld);
			} else if(type == std::string("uchar")) {
				std::auto_ptr< IImageInterpolator<unsigned char, unsigned char> > interpolator = CreateImageInterpolator<unsigned char, unsigned char>(vm);
				LinearTransform<unsigned char>(input_path, ref_path, transform_path, out_path, *interpolator, useWorld);
			} else if(type == std::string("char")) {
				std::auto_ptr< IImageInterpolator<char, char> > interpolator = CreateImageInterpolator<char, char>(vm);
				LinearTransform<char>(input_path, ref_path, transform_path, out_path, *interpolator, useWorld);
			} else {
				std::cerr << "unrecognized data type" << std::endl;
				return -1;
			}
		}
		else if(vm.count("format")) {
            
            std::string input_path, output_path, intype, outtype;
            input_path = BoostCmdHelper::Get<std::string>(vm, "in");
            output_path = BoostCmdHelper::Get<std::string>(vm, "out");
            intype = BoostCmdHelper::Get<std::string>(vm, "intype");
            outtype = BoostCmdHelper::Get<std::string>(vm, "outtype");
            
            if(intype == std::string("short")) {
                WriteImage<short>(input_path, output_path, outtype);
            } else if(intype == std::string("ushort")) {
                WriteImage<unsigned short>(input_path, output_path, outtype);
            } else if(intype == std::string("int")) {
                WriteImage<int>(input_path, output_path, outtype);
            } else if(intype == std::string("uint")) {
                WriteImage<unsigned int>(input_path, output_path, outtype);
            } else if(intype == std::string("float")) {
                WriteImage<float>(input_path, output_path, outtype);
            } else if(intype == std::string("double")) {
                WriteImage<double>(input_path, output_path, outtype);
            } else if(intype == std::string("uchar")) {
                WriteImage<unsigned char>(input_path, output_path, outtype);
            } else if(intype == std::string("char")) {
                WriteImage<char>(input_path, output_path, outtype);
            } else {
                std::cerr << "unrecognized data type" << std::endl;
                return -1;
            }

        } 
		else if (vm.count("resample")) {

			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			std::vector<double> spacing_ = BoostCmdHelper::Get< std::vector<double> >(vm, "spacing");
			std::string intype = BoostCmdHelper::Get<std::string>(vm, "intype");
			std::string outtype = BoostCmdHelper::Get<std::string>(vm, "outtype");

			if (spacing_.size() < 3) {
				std::cerr << "please specify x, y, z spacing separately" << std::endl; exit(-1);
			}

			vect3<double> spacing(spacing_[0], spacing_[1], spacing_[2]);

			if(intype == std::string("short")) {
				ResampleImage<short>(input_path, spacing, output_path, outtype, vm);
			} else if(intype == std::string("ushort")) {
				ResampleImage<unsigned short>(input_path, spacing, output_path, outtype, vm);
			} else if(intype == std::string("int")) {
				ResampleImage<int>(input_path, spacing, output_path, outtype, vm);
			} else if(intype == std::string("uint")) {
				ResampleImage<unsigned int>(input_path, spacing, output_path, outtype, vm);
			} else if(intype == std::string("float")) {
				ResampleImage<float>(input_path, spacing, output_path, outtype, vm);
			} else if(intype == std::string("double")) {
				ResampleImage<double>(input_path, spacing, output_path, outtype, vm);
			} else if(intype == std::string("uchar")) {
				ResampleImage<unsigned char>(input_path, spacing, output_path, outtype, vm);
			} else if(intype == std::string("char")) {
				ResampleImage<char>(input_path, spacing, output_path, outtype, vm);
			} else {
				std::cerr << "unrecognized data type" << std::endl;
				return -1;
			}
		} 
		else if (vm.count("thresholding")) {

			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			std::string outtype = BoostCmdHelper::Get<std::string>(vm, "outtype");

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());
			double threshold = BoostCmdHelper::Get<double>(vm, "threshold");

			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR:
				Thresholding(input_path, static_cast<unsigned char>(threshold), output_path, outtype);
				break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR:
				Thresholding(input_path, static_cast<char>(threshold), output_path, outtype);
				break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT:
				Thresholding(input_path, static_cast<unsigned short>(threshold), output_path, outtype);
				break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT:
				Thresholding(input_path, static_cast<short>(threshold), output_path, outtype);
				break;
			case BRIC::IDEA::FISH::ImageHelper::UINT:
				Thresholding(input_path, static_cast<unsigned int>(threshold), output_path, outtype);
				break;
			case BRIC::IDEA::FISH::ImageHelper::INT:
				Thresholding(input_path, static_cast<int>(threshold), output_path, outtype);
				break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG:
				Thresholding(input_path, static_cast<unsigned long>(threshold), output_path, outtype);
				break;
			case BRIC::IDEA::FISH::ImageHelper::LONG:
				Thresholding(input_path, static_cast<long>(threshold), output_path, outtype);
				break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT:
				Thresholding(input_path, static_cast<float>(threshold), output_path, outtype);
				break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE:
				Thresholding(input_path, static_cast<double>(threshold), output_path, outtype);
				break;
			default:
				std::cerr << "unrecognized pixel type" << std::endl; break;
			}

		}
		else if(vm.count("contour")) {
		
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			std::string type = BoostCmdHelper::Get<std::string>(vm, "type");

			if(type == std::string("short")) {
				Mask2Contour<short>(input_path, output_path);
			} else if(type == std::string("ushort")) {
				Mask2Contour<unsigned short>(input_path, output_path);
			} else if(type == std::string("int")) {
				Mask2Contour<int>(input_path, output_path);
			} else if(type == std::string("uint")) {
				Mask2Contour<unsigned int>(input_path, output_path);
			} else if(type == std::string("float")) {
				Mask2Contour<float>(input_path, output_path);
			} else if(type == std::string("double")) {
				Mask2Contour<double>(input_path, output_path);
			} else if(type == std::string("uchar")) {
				Mask2Contour<unsigned char>(input_path, output_path);
			} else if(type == std::string("char")) {
				Mask2Contour<char>(input_path, output_path);
			} else {
				std::cerr << "unrecognized data type" << std::endl;
				return -1;
			}

		}
		else if(vm.count("rtts")) {

			std::string contourDataPath = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			std::string ref_path = BoostCmdHelper::Get<std::string>(vm, "ref");
			std::string outtype = BoostCmdHelper::Get<std::string>(vm, "outtype");

			if (outtype == std::string("short")) {
				RTTSContourData2Mask<short>(contourDataPath, ref_path, output_path);
			} else if (outtype == std::string("ushort")) {
				RTTSContourData2Mask<unsigned short>(contourDataPath, ref_path, output_path);
			} else if (outtype == std::string("int")) {
				RTTSContourData2Mask<int>(contourDataPath, ref_path, output_path);
			} else if (outtype == std::string("uint")) {
				RTTSContourData2Mask<unsigned int>(contourDataPath, ref_path, output_path);
			} else if (outtype == std::string("float")) {
				RTTSContourData2Mask<float>(contourDataPath, ref_path, output_path);
			} else if (outtype == std::string("double")) {
				RTTSContourData2Mask<double>(contourDataPath, ref_path, output_path);
			} else if (outtype == std::string("uchar")) {
				RTTSContourData2Mask<unsigned char>(contourDataPath, ref_path, output_path);
			} else if (outtype == std::string("char")) {
				RTTSContourData2Mask<char>(contourDataPath, ref_path, output_path);
			} else {
				std::cerr << "unrecognized data type" << std::endl;
				return -1;
			}

		} 
		else if (vm.count("crop")) {

			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			std::string coord_str = BoostCmdHelper::Get<std::string>(vm, "coord");

			std::vector<int> coords;
			stringUtils::str2vec<int>(coord_str, coords, ',');
			assert_message(coords.size() == 6, "Six inputs in coord option");

			vect3<int> sp, ep;
			sp[0] = coords[0]; sp[1] = coords[1]; sp[2] = coords[2];
			ep[0] = coords[3]; ep[1] = coords[4]; ep[2] = coords[5];

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());
			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: CropImage<unsigned char>(input_path, sp, ep, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: CropImage<char>(input_path, sp, ep, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: CropImage<unsigned short>(input_path, sp, ep, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: CropImage<short>(input_path, sp, ep, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: CropImage<unsigned int>(input_path, sp, ep, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::INT: CropImage<int>(input_path, sp, ep, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: CropImage<unsigned long>(input_path, sp, ep, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: CropImage<long>(input_path, sp, ep, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: CropImage<float>(input_path, sp, ep, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: CropImage<double>(input_path, sp, ep, output_path); break;
			default: break;
			}
		}
		else if (vm.count("merge")) {

			std::string prefix = BoostCmdHelper::Get<std::string>(vm, "prefix");
			std::string postfix = BoostCmdHelper::Get<std::string>(vm, "postfix");
			std::string tags = BoostCmdHelper::Get<std::string>(vm, "tag");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			std::vector< std::string > all_tags = stringUtils::Split(tags, ',');
			std::string tmp_path = prefix + all_tags[0] + postfix;

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(tmp_path.c_str());

			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: MergeMasks<unsigned char>(prefix, all_tags, postfix, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: MergeMasks<char>(prefix, all_tags, postfix, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: MergeMasks<unsigned short>(prefix, all_tags, postfix, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: MergeMasks<short>(prefix, all_tags, postfix, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: MergeMasks<unsigned int>(prefix, all_tags, postfix, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::INT: MergeMasks<int>(prefix, all_tags, postfix, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: MergeMasks<unsigned long>(prefix, all_tags, postfix, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: MergeMasks<long>(prefix, all_tags, postfix, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: MergeMasks<float>(prefix, all_tags, postfix, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: MergeMasks<double>(prefix, all_tags, postfix, output_path); break;
			default: std::cerr << "unrecognized pixel type" << std::endl; exit(-1);
			}
		} 
		else if (vm.count("reccrop")) {

			std::string rectum_mask_path = BoostCmdHelper::Get<std::string>(vm, "rec");
			std::string pro_mask_path = BoostCmdHelper::Get<std::string>(vm, "pro");
			std::string out_mask_path = BoostCmdHelper::Get<std::string>(vm, "out");

			ImageHelper::PixelType rec_type = ImageHelper::ReadPixelType(rectum_mask_path.c_str());
			ImageHelper::PixelType pro_type = ImageHelper::ReadPixelType(pro_mask_path.c_str());

			if (rec_type != pro_type)
				err_message("pixel types of prostate and rectum masks mismatch");
			
			switch (pro_type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: rectum_truncate<unsigned char>(rectum_mask_path.c_str(), pro_mask_path.c_str(), out_mask_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: rectum_truncate<char>(rectum_mask_path.c_str(), pro_mask_path.c_str(), out_mask_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: rectum_truncate<unsigned short>(rectum_mask_path.c_str(), pro_mask_path.c_str(), out_mask_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: rectum_truncate<short>(rectum_mask_path.c_str(), pro_mask_path.c_str(), out_mask_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: rectum_truncate<unsigned int>(rectum_mask_path.c_str(), pro_mask_path.c_str(), out_mask_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::INT: rectum_truncate<int>(rectum_mask_path.c_str(), pro_mask_path.c_str(), out_mask_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: rectum_truncate<unsigned long>(rectum_mask_path.c_str(), pro_mask_path.c_str(), out_mask_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: rectum_truncate<long>(rectum_mask_path.c_str(), pro_mask_path.c_str(), out_mask_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: rectum_truncate<float>(rectum_mask_path.c_str(), pro_mask_path.c_str(), out_mask_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: rectum_truncate<double>(rectum_mask_path.c_str(), pro_mask_path.c_str(), out_mask_path.c_str()); break;
			default: std::cerr << "unrecognized pixel type" << std::endl; exit(-1);
			}

		}
		else if (vm.count("dist") || vm.count("sdist")) {

			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());

			bool isSigned = false;
			if (vm.count("sdist"))
				isSigned = true;

			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: distance_transform<unsigned char>(input_path.c_str(), output_path.c_str(), isSigned);	break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: distance_transform<char>(input_path.c_str(), output_path.c_str(), isSigned);	break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: distance_transform<unsigned short>(input_path.c_str(), output_path.c_str(), isSigned); break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: distance_transform<short>(input_path.c_str(), output_path.c_str(), isSigned); break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: distance_transform<unsigned int>(input_path.c_str(), output_path.c_str(), isSigned); break;
			case BRIC::IDEA::FISH::ImageHelper::INT: distance_transform<int>(input_path.c_str(), output_path.c_str(), isSigned); break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: distance_transform<unsigned long>(input_path.c_str(), output_path.c_str(), isSigned); break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: distance_transform<long>(input_path.c_str(), output_path.c_str(), isSigned); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: distance_transform<float>(input_path.c_str(), output_path.c_str(), isSigned); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: distance_transform<double>(input_path.c_str(), output_path.c_str(), isSigned); break;
			default: std::cerr << "unrecognized pixel type" << std::endl; exit(-1);
			}

		}
		else if (vm.count("intensityscale")) {

			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			double scale_factor = BoostCmdHelper::Get<double>(vm, "scale");
			double shift_factor = BoostCmdHelper::Get<double>(vm, "shift");
			std::string outtype = BoostCmdHelper::Get<std::string>(vm, "outtype");

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());

			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: 
				ScaleIntensityGeneral<unsigned char>(outtype.c_str(), input_path.c_str(), scale_factor, shift_factor, output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: 
				ScaleIntensityGeneral<char>(outtype.c_str(), input_path.c_str(), scale_factor, shift_factor, output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT:
				ScaleIntensityGeneral<unsigned short>(outtype.c_str(), input_path.c_str(), scale_factor, shift_factor, output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: 
				ScaleIntensityGeneral<short>(outtype.c_str(), input_path.c_str(), scale_factor, shift_factor, output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::UINT:
				ScaleIntensityGeneral<unsigned int>(outtype.c_str(), input_path.c_str(), scale_factor, shift_factor, output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::INT: 
				ScaleIntensityGeneral<int>(outtype.c_str(), input_path.c_str(), scale_factor, shift_factor, output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: 
				ScaleIntensityGeneral<unsigned long>(outtype.c_str(), input_path.c_str(), scale_factor, shift_factor, output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: 
				ScaleIntensityGeneral<long>(outtype.c_str(), input_path.c_str(), scale_factor, shift_factor, output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: 
				ScaleIntensityGeneral<float>(outtype.c_str(), input_path.c_str(), scale_factor, shift_factor, output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: 
				ScaleIntensityGeneral<double>(outtype.c_str(), input_path.c_str(), scale_factor, shift_factor, output_path.c_str()); break;
			default: err_message("unrecognized pixel type"); break;
			}

		}
		else if (vm.count("smooth")) {

			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			double sigma = BoostCmdHelper::Get<double>(vm, "sigma");
			std::string outtype = BoostCmdHelper::Get<std::string>(vm, "outtype");
			
			ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());

			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: SmoothImageGeneral<unsigned char>(input_path.c_str(), sigma, outtype.c_str(), output_path.c_str());  break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: SmoothImageGeneral<char>(input_path.c_str(), sigma, outtype.c_str(), output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: SmoothImageGeneral<unsigned short>(input_path.c_str(), sigma, outtype.c_str(), output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT:  SmoothImageGeneral<short>(input_path.c_str(), sigma, outtype.c_str(), output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::UINT:  SmoothImageGeneral<unsigned int>(input_path.c_str(), sigma, outtype.c_str(), output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::INT:  SmoothImageGeneral<int>(input_path.c_str(), sigma, outtype.c_str(), output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG:  SmoothImageGeneral<unsigned long>(input_path.c_str(), sigma, outtype.c_str(), output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::LONG:  SmoothImageGeneral<long>(input_path.c_str(), sigma, outtype.c_str(), output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT:  SmoothImageGeneral<float>(input_path.c_str(), sigma, outtype.c_str(), output_path.c_str()); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE:  SmoothImageGeneral<double>(input_path.c_str(), sigma, outtype.c_str(), output_path.c_str()); break;
			default: err_message("unrecognized pixel type");  break;
			}

		}
		else if (vm.count("boundmask")) {

			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			double padsize = BoostCmdHelper::Get<double>(vm, "padsize");

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());

			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: BoundingBoxMask<unsigned char>(input_path, padsize, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: BoundingBoxMask<char>(input_path, padsize, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: BoundingBoxMask<unsigned short>(input_path, padsize, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: BoundingBoxMask<short>(input_path, padsize, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: BoundingBoxMask<unsigned int>(input_path, padsize, output_path);	break;
			case BRIC::IDEA::FISH::ImageHelper::INT: BoundingBoxMask<int>(input_path, padsize, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: BoundingBoxMask<unsigned long>(input_path, padsize, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: BoundingBoxMask<long>(input_path, padsize, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: BoundingBoxMask<float>(input_path, padsize, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: BoundingBoxMask<double>(input_path, padsize, output_path); break;
			default: err_message("unrecongized pixel type");  break;
			}

		}
		else if (vm.count("slicemaskout"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string ref_path = BoostCmdHelper::Get<std::string>(vm, "ref");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			std::string ref_ext = stringUtils::extension(ref_path.c_str());
			if (stringUtils::extension(ref_path.c_str()) == "vtk" || stringUtils::extension(ref_path.c_str()) == "iso")
			{
				Surface surf;
				if (!SurfaceUtils::LoadSurface(ref_path.c_str(), surf))
				{
					std::cerr << "fail to load surface from " << ref_path << std::endl;
					return -1;
				}

				ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());
				switch (type)
				{
				case BRIC::IDEA::FISH::ImageHelper::UCHAR: MaskOutSlices_Surface<unsigned char>(input_path, surf, out_path);  break;
				case BRIC::IDEA::FISH::ImageHelper::CHAR: MaskOutSlices_Surface<char>(input_path, surf, out_path);  break;
				case BRIC::IDEA::FISH::ImageHelper::USHORT: MaskOutSlices_Surface<unsigned short>(input_path, surf, out_path); break;
				case BRIC::IDEA::FISH::ImageHelper::SHORT: MaskOutSlices_Surface<short>(input_path, surf, out_path); break;
				case BRIC::IDEA::FISH::ImageHelper::UINT: MaskOutSlices_Surface<unsigned int>(input_path, surf, out_path); break;
				case BRIC::IDEA::FISH::ImageHelper::INT: MaskOutSlices_Surface<int>(input_path, surf, out_path); break;
				case BRIC::IDEA::FISH::ImageHelper::ULONG: MaskOutSlices_Surface<unsigned long>(input_path, surf, out_path); break;
				case BRIC::IDEA::FISH::ImageHelper::LONG: MaskOutSlices_Surface<long>(input_path, surf, out_path); break;
				case BRIC::IDEA::FISH::ImageHelper::FLOAT: MaskOutSlices_Surface<float>(input_path, surf, out_path); break;
				case BRIC::IDEA::FISH::ImageHelper::DOUBLE: MaskOutSlices_Surface<double>(input_path, surf, out_path); break;
				default: err_message("unrecognized pixel type");  break;
				}
			}
			else
			{
				bool isWorld = vm.count("useWorld");
				ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());
				switch (type)
				{
				case BRIC::IDEA::FISH::ImageHelper::UCHAR: MaskOutSlices<unsigned char>(input_path, ref_path, isWorld, out_path);  break;
				case BRIC::IDEA::FISH::ImageHelper::CHAR: MaskOutSlices<char>(input_path, ref_path, isWorld, out_path);  break;
				case BRIC::IDEA::FISH::ImageHelper::USHORT: MaskOutSlices<unsigned short>(input_path, ref_path, isWorld, out_path); break;
				case BRIC::IDEA::FISH::ImageHelper::SHORT: MaskOutSlices<short>(input_path, ref_path, isWorld, out_path); break;
				case BRIC::IDEA::FISH::ImageHelper::UINT: MaskOutSlices<unsigned int>(input_path, ref_path, isWorld, out_path); break;
				case BRIC::IDEA::FISH::ImageHelper::INT: MaskOutSlices<int>(input_path, ref_path, isWorld, out_path); break;
				case BRIC::IDEA::FISH::ImageHelper::ULONG: MaskOutSlices<unsigned long>(input_path, ref_path, isWorld, out_path); break;
				case BRIC::IDEA::FISH::ImageHelper::LONG: MaskOutSlices<long>(input_path, ref_path, isWorld, out_path); break;
				case BRIC::IDEA::FISH::ImageHelper::FLOAT: MaskOutSlices<float>(input_path, ref_path, isWorld, out_path); break;
				case BRIC::IDEA::FISH::ImageHelper::DOUBLE: MaskOutSlices<double>(input_path, ref_path, isWorld, out_path); break;
				default: err_message("unrecognized pixel type");  break;
				}
			}
		}
		else if (vm.count("rotate2")) {

			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			std::string transform_path = BoostCmdHelper::Get<std::string>(vm, "init");

			bool nearest_or_linear;
			if (vm.count("nearest"))
				nearest_or_linear = true;
			else
				nearest_or_linear = false;

			PDMAffineTransform T;
			if (!T.Load(transform_path.c_str()))
			{
				std::cerr << "fails to load transform from " << transform_path << std::endl;
				return -1;
			}

			vnl_float_4x4 mat = T.data();
			vnl_matrix<double> rot(3, 3);
			for (int i = 0; i < 3; ++i)
				for (int j = 0; j < 3; ++j)
					rot(i, j) = mat(i, j);
			
			ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());

			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR:
				RotateImage2<unsigned char>(input_path, rot, nearest_or_linear, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR:
				RotateImage2<char>(input_path, rot, nearest_or_linear, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT:
				RotateImage2<unsigned short>(input_path, rot, nearest_or_linear, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT:
				RotateImage2<short>(input_path, rot, nearest_or_linear, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::UINT:
				RotateImage2<unsigned int>(input_path, rot, nearest_or_linear, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::INT:
				RotateImage2<int>(input_path, rot, nearest_or_linear, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG:
				RotateImage2<unsigned long>(input_path, rot, nearest_or_linear, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::LONG:
				RotateImage2<long>(input_path, rot, nearest_or_linear, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT:
				RotateImage2<float>(input_path, rot, nearest_or_linear, output_path);
				break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE:
				RotateImage2<double>(input_path, rot, nearest_or_linear, output_path);
				break;
			default:
				std::cerr << "unrecognized pixel type" << std::endl;  break;
			}

		}
		else if (vm.count("rotate")) {

			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			vect3<double> voxel_center;
			if (vm.count("center"))
			{
				std::string str_center = BoostCmdHelper::Get<std::string>(vm, "center");
				std::vector<std::string> tokens = stringUtils::Split(str_center, ',');
				assert_message(tokens.size() >= 3, "center must be a triple");

				for (int i = 0; i < 3; ++i)
					voxel_center[i] = stringUtils::str2num<double>(tokens[i]);
			}
			else
			{
				voxel_center[0] = voxel_center[1] = voxel_center[2] = -1;
			}

			std::string str_angles = BoostCmdHelper::Get<std::string>(vm, "angle");
			vect3<double> angles = stringUtils::ParseVect3<double>(str_angles, ',');

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());
			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: RotateImage<unsigned char>(input_path, voxel_center, angles, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: RotateImage<char>(input_path, voxel_center, angles, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: RotateImage<unsigned short>(input_path, voxel_center, angles, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: RotateImage<short>(input_path, voxel_center, angles, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: RotateImage<unsigned int>(input_path, voxel_center, angles, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::INT: RotateImage<int>(input_path, voxel_center, angles, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: RotateImage<unsigned long>(input_path, voxel_center, angles, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: RotateImage<long>(input_path, voxel_center, angles, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: RotateImage<float>(input_path, voxel_center, angles, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: RotateImage<double>(input_path, voxel_center, angles, output_path); break;
			default: err_message("unrecognized pixel type");  break;
			}

		}
		else if (vm.count("labeldiff"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string ref_path = BoostCmdHelper::Get<std::string>(vm, "ref");
			std::string out_type = BoostCmdHelper::Get<std::string>(vm, "outtype");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());
			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: LabelDiff<unsigned char>(input_path, ref_path, out_type, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: LabelDiff<char>(input_path, ref_path, out_type, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: LabelDiff<unsigned short>(input_path, ref_path, out_type, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: LabelDiff<short>(input_path, ref_path, out_type, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: LabelDiff<unsigned int>(input_path, ref_path, out_type, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::INT: LabelDiff<int>(input_path, ref_path, out_type, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: LabelDiff<unsigned long>(input_path, ref_path, out_type, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: LabelDiff<long>(input_path, ref_path, out_type, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: LabelDiff<float>(input_path, ref_path, out_type, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: LabelDiff<double>(input_path, ref_path, out_type, output_path); break;
			default: err_message("unrecognized pixel type");
			}

		}
		else if (vm.count("thresholdmask"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			double threshold = BoostCmdHelper::Get<double>(vm, "threshold");

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());
			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: ThresholdMask<unsigned char>(input_path, threshold, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: ThresholdMask<char>(input_path, threshold, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: ThresholdMask<unsigned short>(input_path, threshold, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: ThresholdMask<short>(input_path, threshold, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: ThresholdMask<unsigned int>(input_path, threshold, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::INT: ThresholdMask<int>(input_path, threshold, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: ThresholdMask<unsigned long>(input_path, threshold, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: ThresholdMask<long>(input_path, threshold, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: ThresholdMask<float>(input_path, threshold, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: ThresholdMask<double>(input_path, threshold, output_path); break;
			default: err_message("unrecognized pixel type");
			}

		}
		else if (vm.count("maskcrop"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			std::string outtype = BoostCmdHelper::Get<std::string>(vm, "outtype");
			vect3<int> pad_size = stringUtils::ParseVect3<int>(BoostCmdHelper::Get<std::string>(vm, "padsize2"), ',');

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());
			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: MaskCrop<unsigned char>(input_path, pad_size, outtype, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: MaskCrop<char>(input_path, pad_size, outtype, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: MaskCrop<unsigned short>(input_path, pad_size, outtype, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: MaskCrop<short>(input_path, pad_size, outtype, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: MaskCrop<unsigned int>(input_path, pad_size, outtype, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::INT: MaskCrop<int>(input_path, pad_size, outtype, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: MaskCrop<unsigned long>(input_path, pad_size, outtype, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: MaskCrop<long>(input_path, pad_size, outtype, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: MaskCrop<float>(input_path, pad_size, outtype, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: MaskCrop<double>(input_path, pad_size, outtype, output_path); break;
			default:  err_message("unrecognized pixel type"); break;
			}

		}
		else if (vm.count("average"))
		{
			std::string prefix = BoostCmdHelper::Get<std::string>(vm, "prefix");
			std::string postfix = BoostCmdHelper::Get<std::string>(vm, "postfix");
			int start = BoostCmdHelper::Get<int>(vm, "start");
			int end = BoostCmdHelper::Get<int>(vm, "end");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			ImageHelper::PixelType type = ImageHelper::ReadPixelType((prefix + stringUtils::num2str(start) + postfix).c_str());
			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: ImageAverage<unsigned char>(prefix, start, end, postfix, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: ImageAverage<char>(prefix, start, end, postfix, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: ImageAverage<unsigned short>(prefix, start, end, postfix, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: ImageAverage<short>(prefix, start, end, postfix, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: ImageAverage<unsigned int>(prefix, start, end, postfix, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::INT: ImageAverage<int>(prefix, start, end, postfix, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: ImageAverage<unsigned long>(prefix, start, end, postfix, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: ImageAverage<long>(prefix, start, end, postfix, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: ImageAverage<float>(prefix, start, end, postfix, output_path); break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: ImageAverage<double>(prefix, start, end, postfix, output_path); break;
			default: err_message("unrecognized pixel type"); break;
			}

		}
		else if (vm.count("print_spacing"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");

			mxImage<unsigned char> image_header;
			if (!ImageHelper::ReadImageHeader(input_path.c_str(), image_header)) 
			{
				std::cerr << "fail to read image header from " << input_path << std::endl;
				exit(-1);
			}

			vect3<double> spacing = image_header.GetSpacing();
			std::cout << spacing[0] << "," << spacing[1] << "," << spacing[2];
		}
		else if (vm.count("print_size"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");

			mxImage<unsigned char> image_header;
			if (!ImageHelper::ReadImageHeader(input_path.c_str(), image_header))
			{
				std::cerr << "fail to read image header from " << input_path << std::endl;
				exit(-1);
			}

			vect3<unsigned int> size = image_header.GetImageSize();
			std::cout << size[0] << "," << size[1] << "," << size[2];
		}
		else if (vm.count("masktransform"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string ref_path = BoostCmdHelper::Get<std::string>(vm, "ref");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			mxImage< unsigned char > input_mask, out_mask;
			mxImage<unsigned char> ref_image;

			if (!ImageHelper::ReadImage<unsigned char, unsigned char>(input_path.c_str(), input_mask)) {
				std::cerr << "fail to read image from " << input_path << std::endl; exit(-1);
			}

			if (!ImageHelper::ReadImageHeader<unsigned char>(ref_path.c_str(), ref_image)) {
				std::cerr << "fail to read ref image from " << ref_path << std::endl; exit(-1);
			}

			mxImageUtils::TransformMaskImage(ref_image, input_mask, out_mask);

			if (!ImageHelper::WriteImage<unsigned char, unsigned char>(out_mask, out_path.c_str())) {
				std::cerr << "fail to write image to " << out_path << std::endl; exit(-1);
			}
		}
		else if (vm.count("body"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			mxImage<unsigned short> image;
			if (!ImageHelper::ReadImage<unsigned short, unsigned short>(input_path.c_str(), image)) {
				std::cerr << "fail to read image from " << input_path << std::endl; exit(-1);
			}

			mxImage<unsigned char> mask;
			mxImageUtils::ExtractBodyFromCT<unsigned short, unsigned short>(image, mask, &image);

			if (!ImageHelper::WriteImage<unsigned char, unsigned char>(mask, out_path.c_str())) {
				std::cerr << "fail to write image to " << out_path << std::endl; exit(-1);
			}
		}
		else if (vm.count("extractLabel"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			int idx = BoostCmdHelper::Get<int>(vm, "idx");

			mxImage<unsigned char> multi_label_image;
			if (!ImageHelper::ReadImage<unsigned char, unsigned char>(input_path.c_str(), multi_label_image)) {
				std::cerr << "fail to read image from " << input_path << std::endl; exit(-1);
			}

			mxImage<unsigned char> single_label_image;
			single_label_image.CopyImageInfo(multi_label_image);
			
			vect3<unsigned int> image_size = multi_label_image.GetImageSize();
			single_label_image.SetImageSize(image_size);

			bool isEmpty = true;

			#pragma omp parallel for
			for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
			{
				for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
				{
					for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
					{
						if (multi_label_image(x, y, z) == idx)
						{
							single_label_image(x, y, z) = 255;
							isEmpty = false;
						}
						else
							single_label_image(x, y, z) = 0;
					}
				}
			}

			if (isEmpty)
				std::cout << "no voxel with label " << idx << std::endl;
			else
			{
				if (!ImageHelper::WriteImage<unsigned char, unsigned char>(single_label_image, output_path.c_str())) {
					std::cerr << "fail to write image to " << output_path << std::endl; exit(-1);
				}
			}
		}
		else if (vm.count("flip"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			int idx = BoostCmdHelper::Get<int>(vm, "idx");

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());
			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: ImageFlip<unsigned char>(input_path, idx, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: ImageFlip<char>(input_path, idx, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: ImageFlip<unsigned short>(input_path, idx, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: ImageFlip<short>(input_path, idx, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: ImageFlip<unsigned int>(input_path, idx, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::INT: ImageFlip<int>(input_path, idx, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: ImageFlip<unsigned long>(input_path, idx, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: ImageFlip<long>(input_path, idx, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: ImageFlip<float>(input_path, idx, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: ImageFlip<double>(input_path, idx, output_path);  break;
			default: err_message("unrecognized pixel type"); break;
			}

		}
		else if (vm.count("axis_corr"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			
			ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());
			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: AxisCorrection<unsigned char>(input_path, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: AxisCorrection<char>(input_path, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: AxisCorrection<unsigned short>(input_path, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: AxisCorrection<short>(input_path, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: AxisCorrection<unsigned int>(input_path, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::INT: AxisCorrection<int>(input_path, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: AxisCorrection<unsigned long>(input_path, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: AxisCorrection<long>(input_path, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: AxisCorrection<float>(input_path, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: AxisCorrection<double>(input_path, output_path);  break;
			default: err_message("unrecognized pixel type"); break;
			}
		}
		else if (vm.count("findimage"))
		{
			std::string search_imagesize = BoostCmdHelper::Get<std::string>(vm, "size");
			std::string image_format = BoostCmdHelper::Get<std::string>(vm, "ext");
			std::string folder = BoostCmdHelper::Get<std::string>(vm, "in");

			FindImage(folder, search_imagesize, image_format);
		}
		else if (vm.count("remove_small"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");
			int threshold = static_cast<int>( BoostCmdHelper::Get<double>(vm, "threshold") );

			mxImage<unsigned char> mask_image;
			if (!ImageHelper::ReadImage<unsigned char, unsigned char>(input_path.c_str(), mask_image))
			{
				std::cerr << "fails to read mask image from " << input_path << std::endl;
				exit(-1);
			}

			mxImageUtils::RemoveSmallComponents(mask_image, threshold);

			if (!ImageHelper::WriteImage<unsigned char, unsigned char>(mask_image, output_path.c_str()))
			{
				std::cerr << "fails to write mask image to " << output_path << std::endl;
				exit(-1);
			}

		}
		else if (vm.count("count"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			int idx = BoostCmdHelper::Get<int>(vm, "idx");

			mxImage<int> image;
			if (!ImageHelper::ReadImage<int,int>(input_path.c_str(), image)) {
				std::cerr << "fails to read image from " << input_path << std::endl;
				exit(-1);
			}

			int count = mxImageUtils::CountVoxels(image, idx);
			std::cout << count << std::endl;

		}
		else if (vm.count("jointCrop"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string ref_path = BoostCmdHelper::Get<std::string>(vm, "ref");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			std::vector<std::string> output_path_vectors = stringUtils::Split(output_path, ';');
			assert_message(output_path_vectors.size() == 2, "you must input two output paths separated by ;");

			JointCrop<unsigned short, unsigned short>(input_path, ref_path, output_path_vectors[0], output_path_vectors[1]);
		}
		else if (vm.count("permute"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string order = BoostCmdHelper::Get<std::string>(vm, "order");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			std::vector<unsigned int> num_order;
			stringUtils::str2vec<unsigned int>(order, num_order, ',');
			assert_message(num_order.size() == 3, "order size must be 3");

			std::set<int> valid_set;
			for (int i = 0; i < num_order.size(); ++i)
			{
				if (num_order[i] <= 2 && num_order[i] >= 0 && valid_set.count(num_order[i]) == 0)
					valid_set.insert(num_order[i]);
				else
					err_message("invalid order");
			}

			ImageHelper::PixelType type = ImageHelper::ReadPixelType(input_path.c_str());
			switch (type)
			{
			case BRIC::IDEA::FISH::ImageHelper::UCHAR: PermuteAxis<unsigned char>(input_path, num_order, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::CHAR: PermuteAxis<char>(input_path, num_order, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::USHORT: PermuteAxis<unsigned short>(input_path, num_order, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::SHORT: PermuteAxis<short>(input_path, num_order, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::UINT: PermuteAxis<unsigned int>(input_path, num_order, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::INT: PermuteAxis<int>(input_path, num_order, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::ULONG: PermuteAxis<unsigned long>(input_path, num_order, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::LONG: PermuteAxis<long>(input_path, num_order, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::FLOAT: PermuteAxis<float>(input_path, num_order, output_path);  break;
			case BRIC::IDEA::FISH::ImageHelper::DOUBLE: PermuteAxis<double>(input_path, num_order, output_path);  break;
			default: err_message("unrecognized pixel type"); break;
			}
		}
		else if (vm.count("mrscale"))
		{
			std::string input_path = BoostCmdHelper::Get<std::string>(vm, "in");
			double ratio = BoostCmdHelper::Get<double>(vm, "scale");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			mxImage<unsigned short> image;
			if (!ImageHelper::ReadImage<unsigned short, unsigned short>(input_path.c_str(), image))
			{
				std::cerr << "fails to read image from " << input_path << std::endl;
				return -1;
			}
			
			mxImage<double> outImage;
			mxImageUtils::MRIntensityNormalization<unsigned short>(image, ratio, outImage);

			if (!ImageHelper::WriteImage<double, double>(outImage, output_path.c_str()))
			{
				std::cerr << "fails to write image to" << output_path << std::endl;
				return -1;
			}
		}
		else if (vm.count("triMerge"))
		{
			std::string synthesize_path = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string ct_path = BoostCmdHelper::Get<std::string>(vm, "ref");
			std::string coord_str = BoostCmdHelper::Get<std::string>(vm, "coord");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			vect3<int> sp = stringUtils::ParseVect3<int>(coord_str, ',');

			mxImage<unsigned short> ct_image, syn_image;
			if (!ImageHelper::ReadImage<unsigned short, unsigned short>(ct_path.c_str(), ct_image)) {
				std::cerr << "fails to read ct image from " << ct_path << std::endl;
				return -1;
			}

			if (!ImageHelper::ReadImage<unsigned short, unsigned short>(synthesize_path.c_str(), syn_image)) {
				std::cerr << "fails to read synthesized image from " << synthesize_path << std::endl;
				return -1;
			}

			vect3<unsigned int> syn_size = syn_image.GetImageSize();

			for (unsigned int z = 0; z < syn_size[2]; ++z)
			{
				for (unsigned int y = 0; y < syn_size[1]; ++y)
				{
					for (unsigned int x = 0; x < syn_size[0]; ++x)
					{
						vect3<int> pt( sp[0] + x , sp[1] + y, sp[2] + z );

						if (syn_image(x, y, z) != 0)
							ct_image(pt[0], pt[1], pt[2]) = syn_image(x, y, z);
					}
				}
			}

			if (!ImageHelper::WriteImage<unsigned short, unsigned short>(ct_image, output_path.c_str())) {
				std::cerr << "fails to write output image to " << output_path << std::endl;
				return -1;
			}

		}
		else if (vm.count("triMerge2"))
		{
			std::string synthesize_paths = BoostCmdHelper::Get<std::string>(vm, "in");
			std::string ct_path = BoostCmdHelper::Get<std::string>(vm, "ref");
			std::string coord_str = BoostCmdHelper::Get<std::string>(vm, "coord");
			std::string output_path = BoostCmdHelper::Get<std::string>(vm, "out");

			std::vector<std::string> tokens;
			tokens = stringUtils::Split(synthesize_paths, ';');
			assert_message(tokens.size() == 2, "in must provide two paths");
			std::string synthesize_path = tokens[0];
			std::string crop_path = tokens[1];

			std::vector<int> coords;
			stringUtils::str2vec(coord_str, coords, ',');
			assert_message(coords.size() == 6, "must have six parameters in coord");

			vect3<int> sp( coords[0], coords[1], coords[2] );
			vect3<int> ep( coords[3], coords[4], coords[5] );

			mxImage<unsigned short> ct_image, syn_image, crop_image;
			if (!ImageHelper::ReadImage<unsigned short, unsigned short>(ct_path.c_str(), ct_image)) {
				std::cerr << "fails to read ct image from " << ct_path << std::endl;
				return -1;
			}

			if (!ImageHelper::ReadImage<unsigned short, unsigned short>(crop_path.c_str(), crop_image)) {
				std::cerr << "fails to read crop image from " << crop_path << std::endl;
				return -1;
			}

			if (!ImageHelper::ReadImage<unsigned short, unsigned short>(synthesize_path.c_str(), syn_image)) {
				std::cerr << "fails to read synthesized image from " << synthesize_path << std::endl;
				return -1;
			}

			vect3<unsigned int> tmp_size(ep[0] - sp[0] + 1, ep[1] - sp[1] + 1, ep[2] - sp[2] + 1);
			assert_message(ct_image.GetImageSize() == syn_image.GetImageSize(), "two image sizes must be same");
			assert_message(crop_image.GetImageSize() == tmp_size, "crop size mismatch");

			for (unsigned int z = sp[2]; z < ep[2]; ++z)
			{
				for (unsigned int y = sp[1]; y < ep[1]; ++y)
				{
					for (unsigned int x = sp[0]; x < ep[0]; ++x)
					{
						vect3<int> pt(x - sp[0], y - sp[1], z - sp[2]);

						if (crop_image(pt[0], pt[1], pt[2]) != 0)
							ct_image(x, y, z) = syn_image(x, y, z);
					}
				}
			}

			if (!ImageHelper::WriteImage<unsigned short, unsigned short>(ct_image, output_path.c_str())) {
				std::cerr << "fails to write output image to " << output_path << std::endl;
				return -1;
			}

		}
		else
		{
			std::cerr << "option not supported" << std::endl;
			return -1;
		}

	} catch ( boost::program_options::error& exp ) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}
