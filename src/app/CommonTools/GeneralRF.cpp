
#include "stdafx.h"
#include <iostream>
#include "forest/RandomForest.h"
#include "extern/BoostHelper.h"
#include "common/ArrayFunctor.h"

using namespace BRIC::IDEA::FISH;

typedef MemoryAxisAlignedWeakLearner W;
typedef SimpleRegressionStatisticsAggregator S;
typedef SimpleRegressionIndexStatisticsAggregator SI;

void print_usage()
{
	std::cerr << "[ General Regression Forest Training ]" << std::endl;
	std::cerr << "GeneralRF --train --data file_path --target file_path --tree num --feature num --threshold num --depth num --leaf num --out forest_path --seed id\n" << std::endl;
	std::cerr << "GeneralRF --train_idx --data file_path --target file_path --tree num --feature num --threshold num --depth num --leaf num --out forest_path --seed id\n" << std::endl;

	std::cerr << "[ General Regression Forest Testing ]" << std::endl;
	std::cerr << "GeneralRF --test --data file_path --forest path --out file_path\n" << std::endl;
	std::cerr << "GeneralRF --test_idx --data file_path --forest path --out file_path\n" << std::endl;
}

void recursive_assign_indices(Node<W, SI>* node, int& current_index)
{
	if (node == NULL)
		return;

	if (node->isLeaf())
	{
		SI& statistics = node->GetStatistics();
		statistics.leaf_index = current_index;
		++current_index;
	}

	if (node->GetLeft() != NULL)
		recursive_assign_indices(node->GetLeft(), current_index);

	if (node->GetRight() != NULL)
		recursive_assign_indices(node->GetRight(), current_index);
}

void assign_leaf_indices(Forest<W,SI>* forest)
{
	for (unsigned int i = 0; i < forest->GetTreeNumber(); ++i)
	{
		Tree<W, SI>* tree = forest->GetTree(i);

		int current_index = 0;
		recursive_assign_indices(tree->Root(), current_index);

		std::cout << "# of leaf nodes in Tree " << i + 1 << ": " << current_index << std::endl;
	}
}


int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display helper information");

		options_description functional("Functional options (must pick one)");
		functional.add_options()("train", "training");
		functional.add_options()("train_idx", "training by assigning leaf indexes");
		functional.add_options()("test", "testing");
		functional.add_options()("test_idx", "testing by outputting leaf indexes");

		options_description params("Parameter options");
		params.add_options()("data", value<std::string>(), "data path");
		params.add_options()("target", value<std::string>(), "target path");
		params.add_options()("tree", value<unsigned int>(), "number of trees");
		params.add_options()("feature", value<unsigned int>(), "number of random features");
		params.add_options()("threshold", value<unsigned int>(), "number of random thresholds");
		params.add_options()("depth", value<unsigned int>(), "tree depth");
		params.add_options()("leaf", value<unsigned int>(), "leaf num");
		params.add_options()("out", value<std::string>(), "output tree path");
		params.add_options()("seed", value<int>(), "random seed");
		params.add_options()("forest", value<std::string>(), "forest folder");

		options_description cmd_options;
		cmd_options.add(generic).add(functional).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1)
		{
			std::cerr << cmd_options << std::endl;
			print_usage();
			return -1;
		}

		if (vm.count("train") || vm.count("train_idx"))
		{
			std::string data_path = BoostCmdHelper::Get<std::string>(vm, "data");
			std::string target_path = BoostCmdHelper::Get<std::string>(vm, "target");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");
			
			ForestTrainingParameters params;
			params.numTrees = BoostCmdHelper::Get<unsigned int>(vm, "tree");
			params.treeParameters.maxTreeDepth = BoostCmdHelper::Get<unsigned int>(vm, "depth");
			params.treeParameters.numOfRandomWeakLearners = BoostCmdHelper::Get<unsigned int>(vm, "feature");
			params.treeParameters.numOfCandidateThresholdsPerWeakLearner = BoostCmdHelper::Get<unsigned int>(vm, "threshold");
			params.treeParameters.minElementsOfLeafNode = BoostCmdHelper::Get<unsigned int>(vm, "leaf");
			params.treeParameters.minInformationGain = 0;

			Random random;
			if (vm.count("seed")) {
				int seed = BoostCmdHelper::Get<int>(vm, "seed");
				random.Seed(seed);
			}
			
			MemoryDataCollection data;
			if (!data.LoadDataFromFile(data_path.c_str())) {
				std::cerr << "fails to load features from " << data_path << std::endl; return -1;
			}

			if (!data.LoadTargetsFromFile(target_path.c_str())) {
				std::cerr << "fails to load targets from " << target_path << std::endl; return -1;
			}

			// print out feature and target information
			std::cout << "Input feature number: " << data.GetFeatureNumber() << std::endl;
			std::cout << "Input sample number: " << data.GetSampleNumber() << std::endl;
			std::cout << "Input target number: " << data.GetTargetDimension() << std::endl;

			if (vm.count("train"))
			{
				SimpleRegressionRandTrainingContext context(data.GetFeatureNumber(), data.GetTargetDimension());
				std::auto_ptr< Forest<W, S> > forest = ForestTrainer<W, S>::TrainForest(context, params, &data, random);

				std::ofstream out(out_path.c_str(), std::ios::binary);
				if (!out) {
					std::cerr << "fails to write tree to " << out_path << std::endl; return -1;
				}
				forest->Serialize(out);
				out.close();
			}
			else if (vm.count("train_idx"))
			{
				SimpleRegressionIndexRandTrainingContext context(data.GetFeatureNumber(), data.GetTargetDimension());
				std::auto_ptr< Forest<W, SI> > forest = ForestTrainer<W, SI>::TrainForest(context, params, &data, random);
				assign_leaf_indices(forest.get());

				std::ofstream out(out_path.c_str(), std::ios::binary);
				if (!out) {
					std::cerr << "fails to write tree to " << out_path << std::endl; return -1;
				}
				forest->Serialize(out);
				out.close();
			}
			else
				err_message("unexpected train option");

		}
		else if (vm.count("test") || vm.count("test_idx")) {

			std::string data_path = BoostCmdHelper::Get<std::string>(vm, "data");
			std::string forest_path = BoostCmdHelper::Get<std::string>(vm, "forest");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");

			unsigned int feature_dimension = 0, sample_number = 0;
			if (!DataCollectionIO::LoadMatrixInfo(data_path.c_str(), feature_dimension, sample_number)) {
				std::cerr << "fail to load matrix info from " << data_path << std::endl; return -1;
			}
			std::cout << "input feature number: " << feature_dimension << std::endl;
			std::cout << "input sample number: " << sample_number << std::endl;
			
			std::vector<double> features;
			try {
				features.resize(static_cast<size_t>(feature_dimension)* sample_number);
			}
			catch ( std::exception& exp ) {
				std::cerr << exp.what() << std::endl; return -1;
			}

			if (!DataCollectionIO::LoadMatrix(data_path.c_str(), &features[0], features.size())) {
				std::cerr << "fail to load matrix from " << data_path << std::endl; return -1;
			}

			if (vm.count("test"))
			{
				std::ifstream infile(forest_path.c_str(), std::ios::binary);
				if (!infile) {
					std::cerr << "fails to open " << forest_path << std::endl; return -1;
				}
				std::auto_ptr< Forest<W, S> > forest = Forest<W, S>::Deserialize(infile);
				if (forest->GetTreeNumber() <= 0) {
					std::cerr << "empty forest loaded " << std::endl; return -1;
				}
				std::cout << "input tree number: " << forest->GetTreeNumber() << std::endl;

				std::vector<double> predictions;
				const S& stat = forest->GetTree(0)->GetAnyLeafStatisticsAggregator();
				unsigned int target_dimension = stat.GetTargetDimension();
				predictions.resize(target_dimension * sample_number);

				#pragma omp parallel for
				for (int i = 0; i < static_cast<int>(sample_number); ++i)
				{
					ArrayFunctor<double> functor;
					functor.SetArray(&features[i*feature_dimension]);
					SimpleRegressor::Regress(forest.get(), functor, &predictions[i * target_dimension]);
				}

				if (!DataCollectionIO::WriteMatrix(predictions, target_dimension, sample_number, out_path.c_str())) {
					std::cerr << "fail to write matrix to " << out_path << std::endl; return -1;
				}
			}
			else if (vm.count("test_idx"))
			{
				std::ifstream infile(forest_path.c_str(), std::ios::binary);
				if (!infile) {
					std::cerr << "fails to open " << forest_path << std::endl; return -1;
				}
				std::auto_ptr< Forest<W, SI> > forest = Forest<W, SI>::Deserialize(infile);
				if (forest->GetTreeNumber() <= 0) {
					std::cerr << "empty forest loaded " << std::endl; return -1;
				}
				std::cout << "input tree number: " << forest->GetTreeNumber() << std::endl;

				std::vector<int> predictions;
				int num_trees = forest->GetTreeNumber();
				predictions.resize(num_trees * sample_number);

				#pragma omp parallel for
				for (int i = 0; i < static_cast<int>(sample_number); ++i)
				{
					ArrayFunctor<double> functor;
					functor.SetArray(&features[i*feature_dimension]);
					SimpleRegressorIndex::FindLeafIndices(forest.get(), functor, &predictions[i * num_trees]);
				}

				if (!DataCollectionIO::WriteMatrix(predictions, num_trees, sample_number, out_path.c_str())) {
					std::cerr << "fail to write matrix to " << out_path << std::endl; return -1;
				}
			}
			else
				err_message("unexpected option");
			
		}
		else {
			std::cerr << "no train or test option" << std::endl;
			return -1;
		}

	}
	catch ( boost::program_options::error& exp) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}