﻿#include "stdafx.h"
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <queue>
#include <climits>
using namespace std;


struct TreeNode {
	int val;
	TreeNode *left;
	TreeNode *right;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
	vector<vector<int>> verticalOrder(TreeNode* root) {

		if (root == nullptr)
			return{};

		int min_col = 0;
		TreeNode* node = root;
		while (node) {
			node = node->left;
			--min_col;
		}

		int max_col = 0;
		node = root;
		while (node) {
			node = node->right;
			++max_col;
		}

		vector<vector<int>> res(max_col - min_col + 1);
		dfs(root, res, 0, min_col, max_col);

		return res;
	}

	void dfs(TreeNode* root, vector<vector<int>>& res, int col, int min_col, int max_col) {

		if (root == nullptr)
			return;

		res[col - min_col].push_back(root->val);

		dfs(root->left, res, col - 1, min_col, max_col);
		dfs(root->right, res, col + 1, min_col, max_col);
	}

};

// To execute C++, please define "int main()"
int main() {

	Solution s;
	TreeNode* root = 

	return 0;
}
