//
//  CFTrain.h
//  FISH
//
//  Created by Yaozong Gao on 10/23/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#include "stdafx.h"
#include "extern/BoostHelper.h"
#include "common/Random.h"
#include "detector/CFTools/MultiSourceCFTrainerX.h"
#include "feature/haar3d/Haar3DFeatureSpace.h"
#include "detector/DetectorIO.h"


using namespace BRIC::IDEA::FISH;

void print_usage()
{
	std::cerr << "[ train a classification forest with Haar features ]" << std::endl;
	std::cerr << "CFTrain --file iniFile\n" << std::endl; 

	std::cerr << "[ create an example ini file ]" << std::endl;
	std::cerr << "CFTrain --example iniFile\n" << std::endl;

	std::cerr << "[ view sample locations ]" << std::endl;
	std::cerr << "CFTrain --sample sample.txt --image imagePath --out sampleImage\n" << std::endl;

	std::cerr << "[ valid normalization type ]" << std::endl;
	std::cerr << "NONE, UNIT_NORM, MAX_ONE, MIN_MAX, ZERO_MEAN_ONE_STD\n" << std::endl;
}

void TrainGeneral(boost::program_options::variables_map& vm)
{
	// load INI file
	std::string iniPath = BoostCmdHelper::Get<std::string>(vm, "file");
	MultiSourceCFTrainerX trainer;
	if( !trainer.LoadIni(iniPath.c_str()) ) {
		std::cerr << "fails to load ini from " << iniPath << std::endl; exit(-1);
	}
	const MultiSourceCFTrainerXParam& params = trainer.GetParams();

	std::cout << "########   Start to train classification forest R" << params.m_scale << "   ########\n" << std::endl;
	Random random;
	if(vm.count("seed"))
		random.Seed(vm["seed"].as<int>());

	std::string outputFolder = params.m_outFolder;
	boost::filesystem::create_directories(outputFolder);

	trainer.WriteInfoIni();

	for (unsigned int treeIdx = 0; treeIdx < params.m_forestParams.numTrees; ++treeIdx)
	{
		boost::timer::auto_cpu_timer timer;

		std::cout << "########   Start to train tree " << treeIdx+1 << "/" << params.m_forestParams.numTrees << "   ########" << std::endl;

		std::auto_ptr<CompositeFeatureSpace> featureSpace = trainer.CreateFeatureSpace();
		std::auto_ptr<MultiSourceCFTrainerX::TreeType> tree = trainer.TrainTree(random, featureSpace.get());

		if (!trainer.WriteTree(tree.get(), featureSpace.get())) {
			std::cerr << "fail to output tree or feature space" << std::endl; exit(-1);
		}
	}
}

void GenerateSampleImage(const std::string& sampleFile, const std::string& imagePath, mxImage<int>& sampleImage)
{
	mxImageWrapper image;
	if( !image.ReadImage(imagePath.c_str()) ) {
		std::cerr << "fail to read image from " << imagePath << std::endl; exit(-1);
	}

	std::vector< vect3<double> > samplePositions;
	std::vector<int> sampleLabels;
	
	MultiSourceCFTrainerX trainer;
	if( !trainer.LoadSampleFile(sampleFile.c_str(), samplePositions, sampleLabels) ) {
		std::cerr << "fail to read sample positions from file " << sampleFile << std::endl; exit(-1);
	}

	if (samplePositions.size() != sampleLabels.size()) {
		std::cerr << "number of samples != number of labels" << std::endl; exit(-1);
	}

	image.CopyImageInfoTo(sampleImage);
	vect3<unsigned int> imageSize = image.GetImageSize();
	sampleImage.SetImageSize(imageSize);
	sampleImage.Fill(0);

	for (unsigned int i = 0; i < samplePositions.size(); ++i)
	{
		vect3<int> voxel;
		mxImageUtils::World2Voxel(sampleImage, samplePositions[i], voxel);

		if (sampleImage.PtInImage(voxel[0], voxel[1], voxel[2])) 
			sampleImage(voxel[0], voxel[1], voxel[2]) = sampleLabels[i]+1;
		else {
			std::cerr << "sample point out of image domain" << std::endl; exit(-1);
		}
	}
}


int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display help information");

		options_description params("Input parameters");
		params.add_options()("file", value<std::string>(), "iniFile");
		params.add_options()("seed", value<int>(), "seed");
		params.add_options()("example", value<std::string>(), "example iniFile");
		params.add_options()("sample", value<std::string>(), "sample file");
		params.add_options()("image", value<std::string>(), "image path");
		params.add_options()("out", value<std::string>(), "output image path");

		options_description cmd_options;
		cmd_options.add(generic).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			std::cerr << cmd_options << std::endl;
			print_usage();
			return -1;
		}

		// output example ini file
		if( vm.count("example") ) {
			std::string path = vm["example"].as<std::string>();
			if(!MultiSourceCFTrainerXParam::WriteDefaultIni(path.c_str())) {
				std::cerr << "fails to output default ini file" << std::endl; return -1;
			}
			return 0;
		}else if( vm.count("sample") ) {
			std::string samplePath = BoostCmdHelper::Get<std::string>(vm, "sample");
			std::string imagePath = BoostCmdHelper::Get<std::string>(vm, "image");
			std::string outPath = BoostCmdHelper::Get<std::string>(vm, "out");

			mxImage<int> sampleImage;
			GenerateSampleImage(samplePath, imagePath, sampleImage);

			if (!ImageHelper::WriteImage<int,int>(sampleImage, outPath.c_str())) {
				std::cerr << "fail to write sample image to " << outPath << std::endl; return -1;
			}
			return 0;
		}

		TrainGeneral(vm);

	} catch ( boost::program_options::error& exp) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}