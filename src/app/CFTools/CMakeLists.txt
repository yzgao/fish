# voxel-wise classification forest with multi-modality and auto-context

option( BUILD_CFTools "Build Multi-Modality Classification Forests" ON )

if( BUILD_CFTools )

	# training module
	add_executable(CFTrain CFTrain.cpp ../../stdafx.cpp)
	target_link_libraries(CFTrain ${ITK_LIBRARIES} ${Boost_LIBRARIES})
	setup_fish_app(CFTrain)

	# testing module
	add_executable(CFTest CFTest.cpp ../../stdafx.cpp)
	target_link_libraries(CFTest ${ITK_LIBRARIES} ${VTK_LIBRARIES} ${Boost_LIBRARIES})
	setup_fish_app(CFTest)

endif()