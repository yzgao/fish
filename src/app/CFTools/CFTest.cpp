//
//  CFTest.cpp
//  FISH
//
//  Created by Yaozong Gao on 10/23/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#include "stdafx.h"
#include "detector/CFTools/MultiSourceCF.h"
#include "extern/ImageHelper.h"
#include "common/stringUtils.h"
#include "extern/BoostHelper.h"
#include "common/mxImageUtils.h"
#include "detector/DetectorIO.h"
#include "feature/haar3d/Haar3DFeatureSpace.h"
#include "mesh/surface/SurfaceUtils.h"

using namespace BRIC::IDEA::FISH;


void print_usage()
{
	std::cerr << "[ pixel-wisely classification with normalization options ]" << std::endl;
	std::cerr << "CFTest --in imagePath1 ... imagePathN [--mask maskImage --threshold 128] --detector detectorFolder --scale index --out outprefix\n" << std::endl;

	std::cerr << "CFTest --in imagePath1 ... imagePathN [--surface path --pad 30,30,30] --detector detectorFolder --scale index --out outprefix\n" << std::endl;
}

void PixelClassification(const std::vector<std::string>& imagePaths, std::string detectorFolder, int scale, std::string outprefix, boost::program_options::variables_map& vm)
{
	// read source types and spacing
	vect3<double> spacing;
	std::vector<NormalizationType> types;
	MultiSourceCF::ReadInfoIni(detectorFolder.c_str(), scale, spacing, types);

	typedef MultiSourceCF::ForestType ForestType;

	// load feature spaces and forest
	FeatureSpaceArray<CompositeFeatureSpace> fss;
	std::auto_ptr<ForestType> forest = DetectorIO::ReadDetector<ForestType>(detectorFolder.c_str(), scale, fss);
	if(fss.size() == 0 || forest->GetTreeNumber() == 0) {
		std::cerr << "empty detector" << std::endl; exit(-1);
	}

	// read and resample source images
	std::vector<mxImageWrapper> images;
	images.resize(imagePaths.size());
	for (unsigned int i = 0; i < imagePaths.size(); ++i)
	{
		if (! images[i].ReadImage(imagePaths[i].c_str()) ) {
			std::cerr << "fail to read image from " << imagePaths[i] << std::endl; exit(-1);
		}
		images[i].ResampleImage(spacing);
	}

	// pixel-wise classification
	mxImage< std::vector<double> > probImage;
	if (vm.count("mask"))
	{
		// load and resample mask image
		std::string maskPath = vm["mask"].as<std::string>();
		double threshold = vm["threshold"].as<double>();

		mxImageWrapper tmpImage;
		if (!tmpImage.ReadImage(maskPath.c_str())) {
			std::cerr << "fails to read mask image from " << maskPath << std::endl;
			exit(-1);
		}
		mxImage<char> maskImage, isoMaskImage;
		tmpImage.Threshold(threshold, static_cast<char>(255), static_cast<char>(0), maskImage);
		tmpImage.Release();

		mxImageUtils::Resample(maskImage, spacing, isoMaskImage);
		MultiSourceCF::CF_mask(images, types, isoMaskImage, fss, forest.get(), probImage);
	}
	else if (vm.count("surface"))
	{
		// load surface
		std::string surface_path = vm["surface"].as<std::string>();
		vect3<double> pad_mm = stringUtils::ParseVect3<double>(vm["pad"].as<std::string>(), ',');
		
		vect3<int> pad_voxel;
		for (int i = 0; i < 3; ++i)
			pad_voxel[i] = pad_mm[i] / spacing[i];

		Surface surf;
		if (!SurfaceUtils::LoadSurface(surface_path.c_str(), surf)) {
			std::cerr << "fails to load surface from " << surface_path << std::endl;
			exit(-1);
		}

		vect3<unsigned int> image_size = images[0].GetImageSize();

		vect3<int> sp, ep;
		SurfaceUtils::BoundingBox(surf, images[0], sp, ep);
		for (int i = 0; i < 3; ++i) {
			sp[i] -= pad_voxel[i];
			if (sp[i] < 0)
				sp[i] = 0;

			ep[i] += pad_voxel[i];
			if (ep[i] >= static_cast<int>(image_size[i]))
				ep[i] = static_cast<int>(image_size[i] - 1);
		}

		mxImage<char> maskImage;
		images[0].CopyImageInfoTo(maskImage);
		maskImage.SetImageSize(image_size);
		maskImage.Fill(0);

		for (int z = sp[2]; z <= ep[2]; ++z)
			for (int y = sp[1]; y <= ep[1]; ++y)
				for (int x = sp[0]; x <= ep[0]; ++x)
					maskImage(x, y, z) = 255;

		MultiSourceCF::CF_mask(images, types, maskImage, fss, forest.get(), probImage);
	}
	else
	{
		MultiSourceCF::CF(images, types, fss, forest.get(), probImage);
	}

	// write out each probability map
	int numClasses = probImage.GetData()[0].size();
	const std::vector<double>* probs = probImage.GetData();
	vect3<unsigned int> imageSize = probImage.GetImageSize();

	for (int i = 0; i < numClasses; ++i) {
		std::string imagePath = outprefix + stringUtils::num2str(i) + ".mha";

		mxImage<double> image;
		image.CopyImageInfo(probImage);
		image.SetImageSize(imageSize);

		double* imageData = image.GetData();
		for (unsigned int j = 0; j < imageSize[0]*imageSize[1]*imageSize[2]; ++j)
			imageData[j] = probs[j][i];

		std::cout << imagePath << std::endl;
		if (!ImageHelper::WriteImage<double,double>(image, imagePath.c_str())) {
			std::cerr << "fail to write prob image to " << imagePath << std::endl; exit(-1);
		}
	}
}

int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;

		options_description generic("Generic options");
		generic.add_options()("help", "display help information");

		options_description params("Input parameters");
		params.add_options()("in", value< std::vector<std::string> >()->multitoken(), "input source images");
		params.add_options()("detector", value<std::string>(), "detector folder");
		params.add_options()("scale", value<int>(), "scale index");
		params.add_options()("out", value<std::string>(), "output path / prefix");
		params.add_options()("mask", value<std::string>(), "mask image (optional)");
		params.add_options()("threshold", value<double>(), "threshold for mask image (optional)");
		params.add_options()("surface", value<std::string>(), "initial surface path");
		params.add_options()("pad", value<std::string>(), "pad size (mm)");

		options_description cmd_options;
		cmd_options.add(generic).add(params);

		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			std::cerr << cmd_options << std::endl;
			print_usage();
			return -1;
		}

		std::vector< std::string > imagePaths;
		if (vm.count("in")) {
			imagePaths = vm["in"].as< std::vector<std::string> >();
		}else {
			std::cerr << "in option missing" << std::endl; exit(-1);
		}

		std::string detectorPath = BoostCmdHelper::Get<std::string>(vm,"detector");
		int scale = BoostCmdHelper::Get<int>(vm, "scale");
		std::string outprefix = BoostCmdHelper::Get<std::string>(vm, "out");

		PixelClassification(imagePaths, detectorPath, scale, outprefix, vm);

	} catch ( boost::program_options::error& exp ) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}

	return 0;
}

