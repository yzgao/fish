function [] = write_samples(samples, is_append, outpath)

featdim = size(samples,1);
sample_num = size(samples,2);

% write training samples to disk (create mode or append mode)
if is_append
    % append mode
    fid = fopen(outpath, 'rb+');
    temp = fread(fid, [1 2], 'uint32');
    file_featdim = temp(1);
    file_samplenum = temp(2);
    
    if file_featdim ~= featdim
       error('feature dimension cannot match!\n'); 
    end
    
    file_samplenum = file_samplenum + sample_num;
    fseek(fid, 0, 'bof');
    fwrite(fid, [file_featdim file_samplenum], 'uint32');
    
    fseek(fid, 0, 'eof');
    fwrite(fid, samples(:), 'double');
    fclose(fid);
    
else
    % create mode
    fid = fopen(outpath, 'wb');
    
    fwrite(fid, [featdim sample_num], 'uint32');
    fwrite(fid, samples(:), 'double');
   
    fclose(fid);
end


end