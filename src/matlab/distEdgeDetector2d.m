function edges = distEdgeDetector2d(dist, si, sigma, lowthres, highthres)
% canny-like edge detector for distance images
% dist: 2d dist image
% si: Gaussian kernel size
% sigma: Gaussian kernel sigma
% lowthres: low distance threshold
% highthres: high distance threshold

%% Antialias the image by convolving with gaussian filter
h=fspecial('gaussian',si,sigma);
I=im2double(dist);
I=imfilter(I,h,'conv');
figure,imagesc(I),impixelinfo,title('Distance Image after Convolving with Gaussian'),colormap('gray'); axis off;

%% find edge direction
hx = [-1 0 1; -2 0 2; -1 0 1];
hy = [-1 -2 -1; 0 0 0; 1 2 1];
gx = imfilter(dist,hx,'conv');
gy = imfilter(dist,hy,'conv');
%figure,imagesc(gx),impixelinfo,title('x-directional gradient map'),colormap('gray');
%figure,imagesc(gy),impixelinfo,title('y-directional gradient map'),colormap('gray')

%% compute gradient direction
angle=atand(gy./gx);

%% non-minima suppression
cands=zeros(size(I,1),size(I,2));
edges=zeros(size(I,1),size(I,2));

for y=2:1:size(I,1)-1
    for x=2:1:size(I,2)-1
        if(((angle(y,x)>=-22.5)&&(angle(y,x)<22.5))||((angle(y,x)<-157.5))||(angle(y,x)>157.5))
            if((I(y,x)<I(y,x+1)) && (I(y,x)<I(y,x-1)))
                if(I(y,x)<=highthres)
                    if(I(y,x)<=lowthres)
                        edges(y,x)=1;
                    else
                        cands(y,x)=1;
                    end
                end
            end
        end
        if(((angle(y,x)>=-112.5)&&(angle(y,x)<-67.5))||((angle(y,x)>=67.5)&&(angle(y,x)<112.5)))
            if((I(y,x)<I(y+1,x)) && (I(y,x)<I(y-1,x)))
                if(I(y,x)<=highthres)
                    if(I(y,x)<=lowthres)
                        edges(y,x)=1;
                    else
                        cands(y,x)=1;
                    end
                end
            end
        end
        if(((angle(y,x)>=-67.5)&&(angle(y,x)<-22.5))||((angle(y,x)>=112.5)&&(angle(y,x)<157.5)))
            if((I(y,x)<I(y-1,x+1)) && (I(y,x)<I(y+1,x-1)))
                if(I(y,x)<=highthres)
                    if(I(y,x)<=lowthres)
                        edges(y,x)=1;
                    else
                        cands(y,x)=1;
                    end
                end
            end
        end
        if(((angle(y,x)>=-157.5)&&(angle(y,x)<-112.5))||((angle(y,x)>=22.5)&&(angle(y,x)<67.5)))
            if((I(y,x)<I(y+1,x+1)) && (I(y,x)<I(y-1,x-1)))
                if(I(y,x)<=highthres)
                    if(I(y,x)<=lowthres)
                        edges(y,x)=1;
                    else
                        cands(y,x)=1;
                    end
                end
            end
        end
    end
end
figure,imagesc(edges),impixelinfo,title('Image after thresholding'),colormap('gray'); axis off;

%% Apply Connectivity Analysis.

updated = true;

while(updated)
    updated = false;
    [row,col]=find(edges>0);
    for t=1:1:size(col)
        if(cands(row(t)+1,col(t))>0)
            edges(row(t)+1,col(t))=1;
            cands(row(t)+1,col(t))=0;
            updated = true;
        end
        if(cands(row(t),col(t)+1)>0)
            edges(row(t),col(t)+1)=1;
            cands(row(t),col(t)+1)=0;
            updated = true;
        end
        if(cands(row(t)+1,col(t)+1)>0)
            edges(row(t)+1,col(t)+1)=1;
            cands(row(t)+1,col(t)+1)=0;
            updated = true;
        end
        if(cands(row(t)-1,col(t)-1)>0)
            edges(row(t)-1,col(t)-1)=1;
            cands(row(t)-1,col(t)-1)=0;
            updated = true;
        end
        if(cands(row(t)+1,col(t)-1)>0)
            edges(row(t)+1,col(t)-1)=1;
            cands(row(t)+1,col(t)-1)=0;
            updated = true;
        end
        if(cands(row(t)-1,col(t)+1)>0)
            edges(row(t)-1,col(t)+1)=1;
            cands(row(t)-1,col(t)+1)=0;
            updated = true;
        end
        if(cands(row(t)-1,col(t))>0)
            edges(row(t)-1,col(t))=1;
            cands(row(t)-1,col(t))=0;
            updated = true;
        end
        if(cands(row(t),col(t)-1)>0)
            edges(row(t),col(t)-1)=1;
            cands(row(t),col(t)-1)=0;
            updated = true;
        end
    end
end

%% Display output Image.    
figure,imagesc(edges),impixelinfo,title('Image after Applying Connectivity Analysis'),colormap('gray'); axis off;

end