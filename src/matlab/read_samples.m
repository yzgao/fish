function samples = read_samples(path)

fid = fopen(path, 'rb');
temp = fread(fid, [1 2], 'uint32');
samples = fread(fid, [temp(1) temp(2)], 'double');
fclose(fid);

end