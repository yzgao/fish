//
//  CurveUtils.h
//  Fish
//
//  Created by Yaozong Gao on 8/14/13.
//

#ifndef __CurveUtils_h__
#define __CurveUtils_h__

#include "mesh/PDM.h"
#include "common/mxImageUtils.h"
#include "extern/ImageHelper.h"
#include "common/stringUtils.h"

namespace BRIC { namespace IDEA { namespace FISH {

class CurveUtils
{
public:
	// Curve IO
	static bool LoadCurve(const char* path, PDM& curve);
	static bool SaveCurve(const PDM& curve, const char* outputPath);

	static void SmoothCurve(const PDM& curve, double sigma, unsigned int radius, PDM& out);

	template <typename T>
	static void ExtractCenterlineFromTube(const mxImage<T>& binaryImage, PDM& curve);

private:
	
	// helper functions for ExtractCenterlineFromTube
	template <typename T>
	static void FindSourceAndSink(const mxImage<T>& binaryImage, const mxImage<float>& distanceImage, vect3<unsigned int>& source, vect3<unsigned int>& sink);
	static void ComputeWeights(const mxImage<float>& distanceImage, const std::vector< vect3<unsigned int> >& pts, const std::vector< std::pair<int,int> >& edges, std::vector<float>& weights);
};


bool CurveUtils::LoadCurve(const char* path, PDM& curve)
{
	std::vector< std::string > lines;
	if( !stringUtils::ReadLines(path, lines) )
	{
		std::cerr << "fail to open file for read: " <<  path << std::endl;
		return false;
	}

	int numPoints = 0;
	sscanf(lines[4].c_str(), "POINTS %d float", &numPoints);
	std::vector< float >& points = curve.GetPoints();
	points.resize( numPoints * 3 );

	const int ptBeginLine = 5;
	for(int i = 0; i < numPoints; ++i)
		sscanf(lines[ptBeginLine+i].c_str(), "%f %f %f", &points[i*3], &points[i*3+1], &points[i*3+2]);

	return true;
}

bool CurveUtils::SaveCurve(const PDM& curve, const char* outputPath)
{
	const std::vector< float >& points = curve.GetPoints();
	if( points.size() % 3 != 0 ) 
	{
		std::cerr << "Incomplete PDM" << std::endl;
		return false;
	}
	int numPoints = static_cast<int>( points.size() / 3 );

	FILE *fp = fopen(outputPath, "w");
	if(fp == NULL)
	{
		std::cerr << "fail to open file for write: " << outputPath << std::endl;
		return false;
	}

	fprintf(fp, "# vtk DataFile Version 3.0\n");
	fprintf(fp, "Automatically Generated Curve\n");
	fprintf(fp, "ASCII\n");
	fprintf(fp, "DATASET UNSTRUCTURED_GRID\n");
	fprintf(fp, "POINTS %d float\n", numPoints);

	for (int i = 0; i < numPoints; ++i)
		fprintf(fp,"%f %f %f\n", points[i*3], points[i*3+1], points[i*3+2]);

	fprintf(fp, "CELLS %d %d\n", 1,numPoints + 1);
	fprintf(fp, "%d ", numPoints);
	for (int i = 0; i < numPoints; ++i)
		fprintf(fp, "%d ", i);
	fprintf(fp, "\n");

	fprintf(fp,"CELL_TYPES %d\n",1);
	fprintf(fp,"4\n");

	fclose(fp);
	return true;
}

void CurveUtils::SmoothCurve(const PDM& curve, double sigma, unsigned int radius, PDM& out)
{
	const std::vector<float>& inPts = curve.GetPoints();
	std::vector<float>& outPts = out.GetPoints();
	outPts.resize(inPts.size());

	int numPoints = static_cast<int>( outPts.size() / 3 ); 

	for (int i = 0; i < numPoints; ++i)
	{
		double weight = 0;
		vect3<float> weight_sum(0,0,0);
		vect3<float> pt( inPts[i*3], inPts[i*3+1], inPts[i*3+2] );

		for (int j = - static_cast<int>(radius); j <= static_cast<int>(radius); ++j)
		{
			int idx = i + j;
			if(idx < 0 || idx >= numPoints)
				continue;

			vect3<float> nbPt( inPts[idx*3], inPts[idx*3+1], inPts[idx*3+2] );

			double w = exp(- (pt - nbPt).square_magnitude() / (2*sigma*sigma) );
			weight += w;
			weight_sum += nbPt * w;
		}

		pt = weight_sum / weight;
		outPts[i*3] = pt[0]; 
		outPts[i*3+1] = pt[1];
		outPts[i*3+2] = pt[2];
	}
}


template <typename T>
void CurveUtils::ExtractCenterlineFromTube(const mxImage<T>& binaryImage, PDM& curve)
{
	mxImage<float> distanceImage;
	ImageHelper::InsideDistanceImageWithOpenEnds(binaryImage, distanceImage);

	vect3<unsigned int> source, sink;
	FindSourceAndSink(binaryImage, distanceImage, source, sink);

	std::vector< vect3<unsigned int> > pts;
	std::vector< std::pair<int, int> > edges;
	std::map<int,int> pt2id;
	
	// construct the connectivity of the graph
	mxImageUtils::Grids2Graph(binaryImage, pts, edges, pt2id);

	// construct the weight
	std::vector<float> weights;
	ComputeWeights(distanceImage, pts, edges, weights);

	vect3<unsigned int> imageSize = binaryImage.GetImageSize();
	int sourceId = pt2id[ source[2]*imageSize[0]*imageSize[1] + source[1]*imageSize[0] + source[0] ];
	int sinkId = pt2id[ sink[2]*imageSize[0]*imageSize[1] + sink[1]*imageSize[0] + sink[0] ];
	assert(sourceId > 0 && sinkId > 0);

	// construct graph
	using namespace boost;
	typedef adjacency_list< listS, vecS, undirectedS, no_property, property<edge_weight_t,float> > GraphType;
	typedef graph_traits<GraphType>::vertex_descriptor vertex_descriptor;
	typedef graph_traits<GraphType>::edge_descriptor edge_descriptor;

	GraphType g(edges.begin(), edges.end(), weights.begin(), pts.size());

	std::vector<vertex_descriptor> p(num_vertices(g));
	std::vector<float> d(num_vertices(g));

	// shortest path
	dijkstra_shortest_paths(g, sourceId, predecessor_map(make_iterator_property_map(p.begin(), get(boost::vertex_index,g))). 
		distance_map(boost::make_iterator_property_map(d.begin(), get(boost::vertex_index,g))) );

	// backtracing
	std::vector< vect3<unsigned int> > curvePts;
	vertex_descriptor target = sinkId;
	do {
		curvePts.push_back(pts[target]);
		target = p[target];
	} while (target != sourceId);

	// voxel to world
	std::vector<float>& curveData = curve.GetPoints();
	curveData.resize( curvePts.size() * 3 );
	for(unsigned int i = 0; i < curvePts.size(); ++i) 
	{
		vect3<float> worldCoord;
		mxImageUtils::Voxel2World(binaryImage, curvePts[i], worldCoord);

		curveData[i*3] = worldCoord[0];
		curveData[i*3+1] = worldCoord[1];
		curveData[i*3+2] = worldCoord[2];
	}

	return;
}

template <typename T>
void CurveUtils::FindSourceAndSink(const mxImage<T>& binaryImage, const mxImage<float>& distanceImage, vect3<unsigned int>& source, vect3<unsigned int>& sink)
{
	unsigned int start = 0, end = 0;
	mxImageUtils::FindSliceRange(binaryImage, start, end);

	vect3<float> tmpsource(0,0,0), tmpsink(0,0,0);
	int count1 = 0, count2 = 0;

	vect3<unsigned int> imageSize = binaryImage.GetImageSize();
	for(int y = 0; y < static_cast<int>(imageSize[1]); ++y)
	{
		for(int x = 0; x < static_cast<int>(imageSize[0]); ++x)
		{
			if(binaryImage(x,y,start))
			{
				tmpsource[0] += x;
				tmpsource[1] += y;
				++ count1;
			}

			if(binaryImage(x,y,end))
			{
				tmpsink[0] += x;
				tmpsink[1] += y;
				++ count2;
			}
		}
	}
	tmpsource[0] /= count1; tmpsource[1] /= count1; tmpsource[2] = start;
	tmpsink[0] /= count2; tmpsink[1] /= count2; tmpsink[2] = end;

	for (int i = 0; i < 3; i++)
	{
		source[i] = static_cast<unsigned int>(tmpsource[i]+0.5);
		sink[i] = static_cast<unsigned int>(tmpsink[i]+0.5);
	}
}

void CurveUtils::ComputeWeights(const mxImage<float>& distanceImage, const std::vector< vect3<unsigned int> >& pts, const std::vector< std::pair<int,int> >& edges, std::vector<float>& weights)
{
	weights.resize(edges.size());

	for(unsigned int i = 0; i < edges.size(); ++i)
	{
		vect3<unsigned int> pt1 = pts[edges[i].first];
		vect3<unsigned int> pt2 = pts[edges[i].second];

		float dist1 = distanceImage(pt1);
		float dist2 = distanceImage(pt2);

		weights[i] = (dist1 + dist2) / 2;
	}

	// reverse the weight
	float max = - std::numeric_limits<float>::max();
	for (unsigned int i = 0; i < weights.size(); ++i)
	{
		if(weights[i] > max)
			max = weights[i];
	}

	for (unsigned int i = 0; i < weights.size(); ++i)
	{
		weights[i] = max - weights[i];
		if( weights[i] < 0 )
			weights[i] = 0;
	}
}


} } }


#endif