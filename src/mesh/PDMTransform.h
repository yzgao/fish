//
//  PDMTransform.h
//  Fish
//
//  Created by Yaozong Gao on 7/28/13.
//
//

#ifndef __PDMTransform_h__
#define __PDMTransform_h__

#include "mesh/PDM.h"
#include <vnl/vnl_float_4x4.h>
#include <vnl/algo/vnl_matrix_inverse.h>
#include <vnl/algo/vnl_determinant.h>
#include "common/stringUtils.h"

namespace BRIC { namespace IDEA { namespace FISH {


class PDMAffineTransform
{
public:
	PDMAffineTransform() {}
	PDMAffineTransform(const PDMAffineTransform& copy) { m_affineMatrix = copy.m_affineMatrix; }

	void Inverse();
	void Estimate(const PDM& srcPDM, const PDM& dstPDM);
	void Apply(const PDM& srcPDM, PDM& dstPDM) const;
	template <typename T> void Apply(const vect3<T>& in, vect3<T>& out) const;
	void operator() (const PDM& srcPDM, const PDM& dstPDM, PDM& outPDM);

	bool Load(const char* path);
	bool Save(const char* path);
	vnl_float_4x4 data() { return m_affineMatrix; };

private:
	vnl_float_4x4 m_affineMatrix;
};


class PDMRigidTransform
{
public:
	PDMRigidTransform() {}
	PDMRigidTransform(const PDMRigidTransform& copy) { m_rigidMatrix = copy.m_rigidMatrix; }

	void Inverse();
	void Estimate(const PDM& srcPDM, const PDM& dstPDM);
	void Apply(const PDM& srcPDM, PDM& dstPDM) const;
	template <typename T> void Apply(const vect3<T>& in, vect3<T>& out) const;
	void operator() (const PDM& srcPDM, const PDM& dstPDM, PDM& outPDM);

	bool Load(const char* path);
	bool Save(const char* path);
	vnl_float_4x4 data() { return m_rigidMatrix; };

private:
	vnl_float_4x4 m_rigidMatrix;
};


class PDMSimilarityTransform
{
public:
	PDMSimilarityTransform() {}
	PDMSimilarityTransform(const PDMSimilarityTransform& copy) { m_similarityMatrix = copy.m_similarityMatrix; }

	void Inverse();
	void Estimate(const PDM& srcPDM, const PDM& dstPDM);
	void Apply(const PDM& srcPDM, PDM& dstPDM) const;
	template <typename T> void Apply(const vect3<T>& in, vect3<T>& out) const;
	void operator() (const PDM& srcPDM, const PDM& dstPDM, PDM& outPDM);

	bool Load(const char* path);
	bool Save(const char* path);
	vnl_float_4x4 data() { return m_similarityMatrix; };

private:
	vnl_float_4x4 m_similarityMatrix;
};

//////////////////////////////////////////////////////////////////////////

bool PDMAffineTransform::Load(const char* path)
{
	std::vector< std::string > lines;
	if(!stringUtils::ReadLines(path, lines))
	{
		std::cerr << "fail to read file" << std::endl;
		return false;
	}

	if(lines.size() < 4) 
	{
		std::cerr << "corrupted file" << std::endl;
		return false;
	}

	for (int i = 0; i < 4; ++i)
	{
		std::vector<std::string> tokens = stringUtils::Split(lines[i], ' ');
		if(tokens.size() < 4) {
			std::cerr << "corrupted file" << std::endl;
			return false;
		}

		for (int j = 0; j < 4; ++j)
			m_affineMatrix(i,j) = static_cast<float>( atof(tokens[j].c_str()) );
	}

	return true;
}

bool PDMAffineTransform::Save(const char* path)
{
	std::ofstream out(path);
	if(!out) return false;

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			if(j == 0)
				out << m_affineMatrix(i,j);
			else
				out << " " << m_affineMatrix(i,j);
		}
		out << std::endl;
	}

	return true;
}

void PDMAffineTransform::Inverse()
{
	m_affineMatrix = vnl_matrix_inverse<float>(m_affineMatrix).inverse();
}

void PDMAffineTransform::Estimate(const PDM& srcPDM, const PDM& dstPDM)
{
	const std::vector<float>& srcVerts = srcPDM.GetPoints();
	const std::vector<float>& dstVerts = dstPDM.GetPoints();

	assert(srcVerts.size() == dstVerts.size());
	if (srcVerts.size() != dstVerts.size()) {
		std::cerr << "vertex number not same (affine transform)" << std::endl;
		assert(false);
		exit(-1);
	}

	unsigned int numVerts = srcVerts.size() / 3;
	vnl_float_4x4& T = m_affineMatrix;

	// Solve the optimal affine transform matrix by least square fit.
	// min_A trace[(D-AS)' (D-AS)]
	// A: 3x4
	// S: 4xN
	// D: 3xN

	// Analytical solution: A = (DS')(SS')^(-1)

	// construct S
	vnl_matrix<double> S;
	S.set_size(4, numVerts);

	for (unsigned int vert_idx = 0; vert_idx < numVerts; ++vert_idx) {
		for (unsigned int dim = 0; dim < 3; ++dim) {
			S[dim][vert_idx] = srcVerts[vert_idx*3+dim];
		}
	}

	// fill last component for S
	for (unsigned int vert_idx = 0; vert_idx < numVerts; ++vert_idx) {
		S[3][vert_idx] = 1;
	}

	// construct D
	vnl_matrix<double> D;
	D.set_size(3, numVerts);

	for (unsigned int vert_idx = 0; vert_idx < numVerts; ++vert_idx) {
		for (unsigned int dim = 0; dim < 3; ++dim) {
			D[dim][vert_idx] = dstVerts[vert_idx*3+dim];
		}
	}

	// solve A
	vnl_matrix<double> A;
	A = S * S.transpose();
	A = D * S.transpose() * vnl_matrix_inverse<double>(A);

	// copy to T
	for (unsigned int i = 0; i < 3; ++i) 
		for(unsigned int j = 0; j < 4; ++j)
			T(i,j) = static_cast<float>( A(i,j) );
	T(3,0) = 0; T(3,1) = 0; T(3,2) = 0; T(3,3) = 1;
}

void PDMAffineTransform::Apply(const PDM& srcPDM, PDM& dstPDM) const 
{
	const std::vector<float>& srcVerts = srcPDM.GetPoints();
	std::vector<float>& dstVerts = dstPDM.GetPoints();
	
	dstVerts.resize(srcVerts.size());
	unsigned int numVerts = srcVerts.size() / 3;

	vnl_vector<float> pt;
	pt.set_size(4);
	for (unsigned int i = 0; i < numVerts; ++i)
	{
		for (unsigned int j = 0; j < 3; ++j)
			pt[j] = srcVerts[i*3+j];
		pt[3] = 1;
		pt = m_affineMatrix * pt;

		for (unsigned int j = 0; j < 3; ++j)
			dstVerts[i*3+j] = pt[j];
	}
}

template <typename T>
void PDMAffineTransform::Apply(const vect3<T>& in, vect3<T>& out) const 
{
	vnl_vector_fixed<float,4> pt;
	for (int i = 0; i < 3; ++i) 
		pt[i] = static_cast<float>(in[i]);
	pt[3] = 1;
	pt = m_affineMatrix * pt;
	for (int i = 0; i < 3; ++i)
		out[i] = static_cast<T>(pt[i]);
}

void PDMAffineTransform::operator() (const PDM& srcPDM, const PDM& dstPDM, PDM& outPDM)
{
	Estimate(srcPDM, dstPDM);
	Apply(srcPDM, outPDM);
}

//////////////////////////////////////////////////////////////////////////

bool PDMRigidTransform::Load(const char* path)
{
	std::vector< std::string > lines;
	if(!stringUtils::ReadLines(path, lines))
	{
		std::cerr << "fail to read file" << std::endl;
		return false;
	}

	if(lines.size() < 4) 
	{
		std::cerr << "corrupted file" << std::endl;
		return false;
	}

	for (int i = 0; i < 4; ++i)
	{
		std::vector<std::string> tokens = stringUtils::Split(lines[i], ' ');
		if(tokens.size() < 4) {
			std::cerr << "corrupted file" << std::endl;
			return false;
		}

		for (int j = 0; j < 4; ++j)
			m_rigidMatrix(i,j) = static_cast<float>( atof(tokens[j].c_str()) );
	}

	return true;
}

bool PDMRigidTransform::Save(const char* path)
{
	std::ofstream out(path);
	if(!out) return false;

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			if(j == 0)
				out << m_rigidMatrix(i,j);
			else
				out << " " << m_rigidMatrix(i,j);
		}
		out << std::endl;
	}

	return true;
}

void PDMRigidTransform::Inverse()
{
	m_rigidMatrix = vnl_matrix_inverse<float>(m_rigidMatrix).inverse();
}

void PDMRigidTransform::Estimate(const PDM& srcPDM, const PDM& dstPDM)
{
	if (srcPDM.GetPoints().size() != dstPDM.GetPoints().size())
	{
		std::cerr << "number of vertex not the same" << std::endl;
		exit(-1);
	}

	PDM src_pdm = srcPDM, dst_pdm = dstPDM;

	vect3<float> src_center = src_pdm.Center();
	vect3<float> dst_center = dst_pdm.Center();

	src_pdm.Translate( src_center * -1);
	dst_pdm.Translate( dst_center * -1);

	vnl_matrix<float> E;
	E.set_size(3,3);
	E.fill(0);

	vnl_vector<float> src_tmp, dst_tmp;
	src_tmp.set_size(3);
	dst_tmp.set_size(3);

	const std::vector<float>& src_pts = src_pdm.GetPoints();
	const std::vector<float>& dst_pts = dst_pdm.GetPoints();
	int numVerts = src_pts.size() / 3;

	for(int i = 0; i < numVerts; ++i)  {

		for (int j = 0; j < 3; ++j) {
			src_tmp[j] = src_pts[i*3+j];
			dst_tmp[j] = dst_pts[i*3+j];
		}

		E += outer_product<float>(src_tmp, dst_tmp);
	}

	vnl_svd<float> svd(E);
	vnl_matrix<float> R = svd.V() * svd.U().transpose();

	// check the determinant to avoid improper rotation (e.g., reflections)
	double det = vnl_determinant<float>(R);
	if (det < 0) {
		vnl_matrix<float> V = svd.V();
		for (unsigned int i = 0; i < V.rows(); ++i)
			V(i, 2) = -V(i, 2);
		R = V * svd.U().transpose();

		det = vnl_determinant<float>(R);
		if (det < 0) {
			std::cerr << "det of rot matrix < 0" << std::endl; exit(-1);
		}
	}

	vnl_float_4x4 C_src, C_dst;

	// C_src
	// 1 0 0  
	// 0 1 0 -src_center
	// 0 0 1 
	// 0 0 0 1
	C_src.set_identity();
	for (unsigned int i = 0; i < 3; ++i)
		C_src(i,3) = - src_center[i];

	// R_ex
	//       0
	//   R   0
	//       0
	// 0 0 0 1
	vnl_float_4x4 R_ex;
	R_ex.fill(0);
	R_ex(3,3) = 1;
	for (unsigned int i = 0; i < 3; ++i)
		for (unsigned int j = 0; j < 3; ++j)
			R_ex(i,j) = R(i,j);

	// C_dst
	// 1 0 0
	// 0 1 0 dst_center
	// 0 0 1
	// 0 0 0 1
	C_dst.set_identity();
	for (unsigned int i = 0; i < 3; ++i)
		C_dst(i,3) = dst_center[i];

	m_rigidMatrix = C_dst * R_ex * C_src;
}

void PDMRigidTransform::Apply(const PDM& srcPDM, PDM& dstPDM) const 
{
	const std::vector<float>& srcVerts = srcPDM.GetPoints();
	std::vector<float>& dstVerts = dstPDM.GetPoints();

	dstVerts.resize(srcVerts.size());
	unsigned int numVerts = srcVerts.size() / 3;

	vnl_vector<float> pt;
	pt.set_size(4);
	for (unsigned int i = 0; i < numVerts; ++i)
	{
		for (unsigned int j = 0; j < 3; ++j)
			pt[j] = srcVerts[i*3+j];
		pt[3] = 1;
		pt = m_rigidMatrix * pt;

		for (unsigned int j = 0; j < 3; ++j)
			dstVerts[i*3+j] = pt[j];
	}
}

template <typename T>
void PDMRigidTransform::Apply(const vect3<T>& in, vect3<T>& out) const 
{
	vnl_vector_fixed<float,4> pt;
	for (int i = 0; i < 3; ++i) 
		pt[i] = static_cast<float>(in[i]);
	pt[3] = 1;
	pt = m_rigidMatrix * pt;
	for (int i = 0; i < 3; ++i)
		out[i] = static_cast<T>(pt[i]);
}

void PDMRigidTransform::operator() (const PDM& srcPDM, const PDM& dstPDM, PDM& outPDM)
{
	Estimate(srcPDM, dstPDM);
	Apply(srcPDM, outPDM);
}

//////////////////////////////////////////////////////////////////////////

bool PDMSimilarityTransform::Load(const char* path)
{
	std::vector< std::string > lines;
	if (!stringUtils::ReadLines(path, lines))
	{
		std::cerr << "fail to read file" << std::endl;
		return false;
	}

	if (lines.size() < 4)
	{
		std::cerr << "corrupted file" << std::endl;
		return false;
	}

	for (int i = 0; i < 4; ++i)
	{
		std::vector<std::string> tokens = stringUtils::Split(lines[i], ' ');
		if (tokens.size() < 4) {
			std::cerr << "corrupted file" << std::endl;
			return false;
		}

		for (int j = 0; j < 4; ++j)
			m_similarityMatrix(i, j) = static_cast<float>(atof(tokens[j].c_str()));
	}

	return true;
}

bool PDMSimilarityTransform::Save(const char* path)
{
	std::ofstream out(path);
	if (!out) return false;

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			if (j == 0)
				out << m_similarityMatrix(i, j);
			else
				out << " " << m_similarityMatrix(i, j);
		}
		out << std::endl;
	}

	return true;
}

void PDMSimilarityTransform::Inverse()
{
	m_similarityMatrix = vnl_matrix_inverse<float>(m_similarityMatrix).inverse();
}

void PDMSimilarityTransform::Estimate(const PDM& srcPDM, const PDM& dstPDM)
{
	if (srcPDM.GetPoints().size() != dstPDM.GetPoints().size())
	{
		std::cerr << "number of vertex not the same" << std::endl;
		exit(-1);
	}

	PDM src_pdm = srcPDM, dst_pdm = dstPDM;

	// compute center of PDM
	vect3<float> src_center = src_pdm.Center();
	vect3<float> dst_center = dst_pdm.Center();

	// zero center alignment
	src_pdm.Translate(src_center * -1);
	dst_pdm.Translate(dst_center * -1);

	const std::vector<float>& src_pts = src_pdm.GetPoints();
	const std::vector<float>& dst_pts = dst_pdm.GetPoints();
	size_t num_verts = src_pts.size() / 3;

	// compute variance of source PDM
	float src_var = 0;
	for (int i = 0; i < 3; ++i)
	{
		float tmp_var = 0;
		for (size_t j = 0; j < num_verts; ++j)
			tmp_var += src_pts[j * 3 + i] * src_pts[j * 3 + i];
		tmp_var /= num_verts;
		src_var += tmp_var;
	}

	vnl_matrix<float> E;
	E.set_size(3, 3);
	E.fill(0);

	vnl_vector<float> src_tmp, dst_tmp;
	src_tmp.set_size(3);
	dst_tmp.set_size(3);

	// compute covariance between source and target points
	for (size_t i = 0; i < num_verts; ++i)  {

		for (int j = 0; j < 3; ++j) {
			src_tmp[j] = src_pts[i * 3 + j];
			dst_tmp[j] = dst_pts[i * 3 + j];
		}

		E += outer_product<float>(src_tmp, dst_tmp);
	}
	E /= static_cast<float>(num_verts);

	// compute SVD and compute rotation R
	vnl_svd<float> svd(E);
	vnl_matrix<float> R = svd.V() * svd.U().transpose();

	// check the determinant to avoid improper rotation (e.g., reflections)
	double det = vnl_determinant<float>(R);
	if (det < 0) {
		vnl_matrix<float> V = svd.V();
		for (unsigned int i = 0; i < V.rows(); ++i)
			V(i, 2) = -V(i, 2);
		R = V * svd.U().transpose();

		det = vnl_determinant<float>(R);
		if (det < 0) {
			std::cerr << "det of rot matrix < 0" << std::endl; exit(-1);
		}
	}

	// compute scaling
	const vnl_diag_matrix<float>& w = svd.W();
	float scale = 0;
	for (unsigned int i = 0; i < w.size(); ++i)
		scale += w[i];
	scale /= src_var;

	// compute translation
	vnl_vector<float> translation;
	vnl_vector<float> src_center_, dst_center_;
	src_center_.set_size(3); 
	dst_center_.set_size(3);

	for (int i = 0; i < 3; ++i)
	{
		src_center_[i] = src_center[i];
		dst_center_[i] = dst_center[i];
	}
	translation = dst_center_ - (R * src_center_) * scale;

	// compose the similarity matrix
	m_similarityMatrix.fill(0);
	m_similarityMatrix(3, 3) = 1;
	for (unsigned int i = 0; i < 3; ++i)
		m_similarityMatrix(i, 3) = translation(i);

	for (unsigned int i = 0; i < 3; ++i)
	{
		for (unsigned int j = 0; j < 3; ++j)
		{
			m_similarityMatrix(i, j) = R(i, j) * scale;
		}
	}
}

void PDMSimilarityTransform::Apply(const PDM& srcPDM, PDM& dstPDM) const
{
	const std::vector<float>& srcVerts = srcPDM.GetPoints();
	std::vector<float>& dstVerts = dstPDM.GetPoints();

	dstVerts.resize(srcVerts.size());
	unsigned int numVerts = srcVerts.size() / 3;

	vnl_vector<float> pt;
	pt.set_size(4);
	for (unsigned int i = 0; i < numVerts; ++i)
	{
		for (unsigned int j = 0; j < 3; ++j)
			pt[j] = srcVerts[i * 3 + j];
		pt[3] = 1;
		pt = m_similarityMatrix * pt;

		for (unsigned int j = 0; j < 3; ++j)
			dstVerts[i * 3 + j] = pt[j];
	}
}

template <typename T>
void PDMSimilarityTransform::Apply(const vect3<T>& in, vect3<T>& out) const
{
	vnl_vector_fixed<float, 4> pt;
	for (int i = 0; i < 3; ++i)
		pt[i] = static_cast<float>(in[i]);
	pt[3] = 1;
	pt = m_similarityMatrix * pt;
	for (int i = 0; i < 3; ++i)
		out[i] = static_cast<T>(pt[i]);
}

void PDMSimilarityTransform::operator() (const PDM& srcPDM, const PDM& dstPDM, PDM& outPDM)
{
	Estimate(srcPDM, dstPDM);
	Apply(srcPDM, outPDM);
}




} } } 

#endif