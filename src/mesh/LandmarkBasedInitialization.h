//
//  LandmarkBasedInitialization.h
//  Fish
//
//  Created by Yaozong Gao on 8/31/13.
//

#ifndef __LandmarkBasedInitialization_h__
#define __LandmarkBasedInitialization_h__

#include "mesh/PDMUtils.h"
#include "mesh/PCAShapeSpace.h"

namespace BRIC { namespace IDEA { namespace FISH {


class PCAShapeCompositionSolver
{
public:
	void SetPCAShapeSpace(const PCAShapeSpace& ss) { m_ss = &ss; }
	void SetLambda(float lambda) { m_lambda = lambda; }
	void Solve(const std::vector<float>& alignLMs, const std::vector<int>& LMIDs, std::vector<float>& alpha);

private:
	double energy_(const std::vector<float>& targetLMs, const std::vector<int>& LMIDs, const std::vector<float>& alpha);

	const PCAShapeSpace* m_ss;
	float m_lambda;
};


class LandmarkModelInitialization
{
public:
	template <class TransformType>
	static void Initialize( const std::map< int,vect3<float> >& landmarks, const PCAShapeSpace& ss, TransformType T, double lambda, PDM& init );

private:
	static void report_diff_(const std::vector<float>& pts1, const std::vector<float>& pts2);
	static void report_lm_fit_accuracy_( const std::map< int,vect3<float> >& landmarks, const PDM& init );
};


double PCAShapeCompositionSolver::energy_(const std::vector<float>& targetLMs, const std::vector<int>& LMIDs, const std::vector<float>& alpha)
{
	PDM inferShape;
	m_ss->GetPDM(&alpha[0], alpha.size(), inferShape);

	std::vector<float> inferLMs;
	PDMUtils::ExtractLandmarksFromPDM(inferShape.GetPoints(), LMIDs, inferLMs);

	double ret = 0;
	for(int i = 0; i < static_cast<int>(inferLMs.size()); ++i)
	{
		double diff = inferLMs[i] - targetLMs[i];
		ret += diff*diff;
	}
	
	const std::vector<float>& eigens = m_ss->GetEigens();
	for (int i = 0; i < static_cast<int>(alpha.size()); ++i)
		ret += m_lambda * static_cast<double>(alpha[i]*alpha[i] / eigens[i]);
	
	return ret;
}

//
// ||l-(E_l*a+m_l)||^2 + lambda * sum_i{ a_i^2 / e_i }
// where l is vector for n detected landmarks, E_l is the matrix that consists of eigen-vectors, m_l is the mean vector,
// lambda is a regularization parameter, a_i is the i-th entry of alpha, e_i is the i-th eigen-value.
// 
void PCAShapeCompositionSolver::Solve(const std::vector<float>& alignLMs, const std::vector<int>& LMIDs, std::vector<float>& alpha)
{
	// a = (E_l^T * E_l + M)^-1 * A^T * (m_l - b)
	// M = diag_matrix, M_ii = lambda / e_0, e_0 is the 1st eigen value.

	const std::vector<float>& mean = m_ss->GetMean();
	const vnl_matrix<float>& comps = m_ss->GetComps();
	const std::vector<float>& eigens = m_ss->GetEigens();

	int numEigens = eigens.size();
	int numLMs = LMIDs.size();

	vnl_diag_matrix<double> M;
	M.set_size(numEigens);
	for (int i = 0; i < numEigens; ++i)
		M(i,i) = m_lambda / eigens[i];

	vnl_matrix<double> E_l;
	E_l.set_size(numLMs * 3, numEigens);
	for (int i = 0; i < numEigens; ++i)
	{
		for (int j = 0; j < numLMs; ++j)
		{
			int k = LMIDs[j];

			E_l(j*3,i) = comps(k*3,i);
			E_l(j*3+1,i) = comps(k*3+1,i);
			E_l(j*3+2,i) = comps(k*3+2,i);
		}
	}

	vnl_vector<double> b;
	b.set_size(alignLMs.size());
	for (unsigned int i = 0; i < alignLMs.size(); ++i)
		b[i] = alignLMs[i];

	vnl_vector<double> m_l;
	m_l.set_size(numLMs * 3);
	for (int i = 0; i < numLMs; ++i)
	{
		int k = LMIDs[i];
		m_l[i*3] = mean[k*3];
		m_l[i*3+1] = mean[k*3+1];
		m_l[i*3+2] = mean[k*3+2];
	}

	// initialize alpha 
	alpha.resize(numEigens);
	for (int i = 0; i < numEigens; ++i)
		alpha[i] = 0;

	//////////////////////////////////////////////////////////////////////////
	std::cout << "energy (mean shape): " << energy_(alignLMs, LMIDs, alpha) << std::endl;
	//////////////////////////////////////////////////////////////////////////

	vnl_vector<double> res = vnl_matrix_inverse<double>(E_l.transpose() * E_l + M).solve(E_l.transpose() * (b - m_l));
	for (int i = 0; i < static_cast<int>(res.size()); ++i)
		alpha[i] = static_cast<float>(res[i]);

	//////////////////////////////////////////////////////////////////////////
	std::cout << "energy (PCA): " << energy_(alignLMs, LMIDs, alpha) << std::endl;
	//////////////////////////////////////////////////////////////////////////
}


void LandmarkModelInitialization::report_diff_(const std::vector<float>& pts1, const std::vector<float>& pts2)
{
	printf("diff: ");

	int numVerts = pts1.size() / 3;
	for (int i = 0; i < numVerts; ++i)
	{
		vect3<double> diff;
		diff[0] = pts1[i*3] - pts2[i*3]; 
		diff[1] = pts1[i*3+1] - pts2[i*3+1]; 
		diff[2] = pts1[i*3+2] - pts2[i*3+2];

		printf("%.2f ", diff.l2norm());
	}

	printf("\n");
}

void LandmarkModelInitialization::report_lm_fit_accuracy_( const std::map< int,vect3<float> >& landmarks, const PDM& init )
{
	printf("fit error: ");

	const std::vector<float>& pts = init.GetPoints();
	std::map<int,vect3<float> >::const_iterator it = landmarks.begin();
	while( it != landmarks.end() )
	{
		vect3<float> fitPt;
		fitPt[0] = pts[it->first*3];
		fitPt[1] = pts[it->first*3+1];
		fitPt[2] = pts[it->first*3+2];

		vect3<float> diff = fitPt - it->second;
		printf("%.2f ", diff.l2norm());

		++ it;
	}

	printf("\n");
}



template <class TransformType>
void LandmarkModelInitialization::Initialize( const std::map< int,vect3<float> >& landmarks, const PCAShapeSpace& ss, TransformType T, double lambda, PDM& init )
{
	PCAShapeCompositionSolver solver;
	solver.SetPCAShapeSpace(ss);
	solver.SetLambda(static_cast<float>(lambda));

	// construct detected and LMIDs
	PDM detLMs;
	std::vector<int> LMIDs;

	std::vector<float>& detPts = detLMs.GetPoints();
	detPts.resize(landmarks.size() * 3);
	LMIDs.resize(landmarks.size());

	std::map<int, vect3<float> >::const_iterator it = landmarks.begin();
	int count = 0;
	while ( it != landmarks.end() )
	{
		detPts[count*3] = it->second[0];
		detPts[count*3+1] = it->second[1];
		detPts[count*3+2] = it->second[2];
		LMIDs[count] = it->first;
		++ count;
		++ it;
	}
	
	// construct mean landmarks
	PDM refLMs;
	const std::vector<float>& meanPts = ss.GetMean();
	PDMUtils::ExtractLandmarksFromPDM(meanPts, LMIDs, refLMs.GetPoints());

	int max_iter = 50, iter = 0;
	std::vector<float> alpha;	// linear coefficients to optimize

	PDM prevRef;

	double threshold = 1e-6;
	while ( iter < max_iter )
	{
		// estimate the initial transform and aligned model
		PDM alignedLMs;
		T(detLMs, refLMs, alignedLMs);

		//////////////////////////////////////////////////////////////////////////
		report_diff_(alignedLMs.GetPoints(), refLMs.GetPoints());
		//////////////////////////////////////////////////////////////////////////

		solver.Solve(alignedLMs.GetPoints(), LMIDs, alpha);
		ss.GetPDM(&alpha[0], alpha.size(), init);
		PDMUtils::ExtractLandmarksFromPDM(init.GetPoints(), LMIDs, refLMs.GetPoints());

		//////////////////////////////////////////////////////////////////////////
		report_diff_(alignedLMs.GetPoints(), refLMs.GetPoints());
		//////////////////////////////////////////////////////////////////////////

		if(iter != 0)
		{
			std::vector<float> tmpdiffs;
			PDMUtils::PDMDiff(prevRef, refLMs, tmpdiffs);

			const std::vector<float>& prev_pts = prevRef.GetPoints();
			const std::vector<float>& ref_pts = refLMs.GetPoints();

			int numVerts = prev_pts.size() / 3;

			float avg_error = 0;
			for(int i = 0; i < numVerts; ++i)
			{
				vect3<float> p1( prev_pts[i*3], prev_pts[i*3+1], prev_pts[i*3+2] );
				vect3<float> p2( ref_pts[i*3], ref_pts[i*3+1], ref_pts[i*3+2] );

				tmpdiffs[i] = static_cast<float>( tmpdiffs[i] / std::max(p1.l2norm(), p2.l2norm()) );
				avg_error += tmpdiffs[i];
			}
			avg_error /= numVerts;

			std::cout << "average relative error: " << avg_error << std::endl;

			if(avg_error < threshold)
				break;
		}

		prevRef = refLMs;

		std::cout << std::endl;
		++ iter;
	}

	T.Inverse();
	T.Apply(init,init);

	report_lm_fit_accuracy_(landmarks, init);
}

} } }

#endif