//
//  PDM.h
//  Fish
//
//  Created by Yaozong Gao on 7/26/13.
//
//

#ifndef __PDM_h__
#define __PDM_h__

    
#include <vector>
#include <set>
#include <queue>
#include <string>
#include <stdexcept>
#include <utility>
#include <algorithm>
#include <iostream>
    
#include "common/vect.h"
    
    
namespace BRIC { namespace IDEA { namespace FISH {
        
        
/** @brief point distribution model */
class PDM {
    
protected:
    // data vector
    std::vector<float> m_verts; // memory, i.e., x1 y1 z1 x2 y2 z2 ...
    
public:
    
    friend class PDMUtils;
	friend class PDMTransform;
    
    /** @brief default construct */
    PDM() {}
    
    /** @brief copy construct */
    PDM(const PDM& rhs) { m_verts = rhs.m_verts; }
    PDM(const float* p, unsigned int size)
    {
        assert(size % 3 == 0);
        m_verts.resize(size);
        for (unsigned int i = 0; i < size; ++i)
            m_verts[i] = p[i];
    }
    
    /** @brief get internal representation */
    std::vector<float>& GetPoints() { return m_verts; }
    const std::vector<float>& GetPoints() const { return m_verts; }
    
    /** @brief clear operations */
    void Clear() { m_verts.clear(); }
    
    // simple operators
    vect3<float> Center() const;
    void Translate(const vect3<float>& offset);
    void MoveTo(const vect3<float>& center_pt);
    void ScaleToUnitRMSD();
    void Fill(float val);
    
    // overide operators
    PDM& operator=(const PDM& rhs);
    PDM& operator+=(const PDM& rhs);
    PDM operator+(const PDM& rhs) const;
    PDM& operator-=(const PDM& rhs);
    PDM operator-(const PDM& rhs) const;
    template <typename T> PDM& operator*=(T scale);
    template <typename T> PDM operator*(T scale) const;
    template <typename T> PDM& operator/=(T scale);
    template <typename T> PDM operator/(T scale);
};

    
vect3<float> PDM::Center() const
{
    vect3<float> center(0,0,0);
    
    unsigned int numVerts = static_cast<unsigned int>( m_verts.size() / 3 );
    for (unsigned int i = 0; i < numVerts; ++i) {
        center[0] += m_verts[i*3];
        center[1] += m_verts[i*3+1];
        center[2] += m_verts[i*3+2];
    }
    center /= numVerts;
    return center;
}
    
void PDM::Translate(const vect3<float>& offset)
{
    unsigned int numVerts = static_cast<unsigned int>( m_verts.size() / 3 );
    
    for (unsigned int i = 0; i < numVerts; ++i) {
        m_verts[i*3] += offset[0];
        m_verts[i*3+1] += offset[1];
        m_verts[i*3+2] += offset[2];
    }
}
    
void PDM::MoveTo(const vect3<float>& centerPt)
{
    vect3<float> center = Center();
    vect3<float> offset = centerPt - center;
    Translate(offset);
}
    
void PDM::ScaleToUnitRMSD()
{
    double distance = 0;
    unsigned int numVerts = static_cast<unsigned int>( m_verts.size() / 3 );
    float x,y,z;
    
    for (unsigned int j = 0; j < numVerts; ++j) {
        
        x = m_verts[j*3];
        y = m_verts[j*3+1];
        z = m_verts[j*3+2];
        
        distance += ( (x*x+y*y+z*z) / static_cast<float>(numVerts) );
    }
    distance = sqrt(distance);
        
    (*this) /= distance;
}
    
void PDM::Fill(float val)
{
    for (unsigned int i = 0; i < m_verts.size(); ++i)
        m_verts[i] = val;
}
    
PDM& PDM::operator=(const PDM& rhs)
{
    if (this == &rhs)
        return *this;
    m_verts = rhs.m_verts;
    return (*this);
}

PDM& PDM::operator+=(const PDM& rhs)
{
    assert(m_verts.size() == rhs.m_verts.size());
    for (unsigned int i = 0; i < m_verts.size(); ++i)
        m_verts[i] += rhs.m_verts[i];
    return *this;
}

PDM PDM::operator+(const PDM& rhs) const
{
    assert(m_verts.size() == rhs.m_verts.size());
    PDM ret = (*this);
    ret += rhs;
    return ret;
}

PDM& PDM::operator-=(const PDM& rhs)
{
    assert(m_verts.size() == rhs.m_verts.size());
    for (unsigned int i = 0; i < m_verts.size(); ++i)
        m_verts[i] -= rhs.m_verts[i];
    return *this;
}

PDM PDM::operator-(const PDM& rhs) const
{
    assert(m_verts.size() == rhs.m_verts.size());
    PDM ret = (*this);
    ret -= rhs;
    return ret;
}

template <typename T>
PDM& PDM::operator*=(T scale)
{
    for (unsigned int i = 0; i < m_verts.size(); ++i)
        m_verts[i] *= scale;
    return *this;
}

template <typename T>
PDM PDM::operator*(T scale) const
{
    PDM ret(*this);
    ret *= scale;
    return ret;
}

template <typename T>
PDM& PDM::operator/=(T scale)
{
    for (unsigned int i = 0; i < m_verts.size(); ++i)
        m_verts[i] = static_cast<float>(m_verts[i] / scale);
    return *this;
}

template <typename T>
PDM PDM::operator/(T scale)
{
    PDM ret(*this);
    ret /= scale;
    return ret;
}
    
    
} } }


#endif
