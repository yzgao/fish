//
//  PDMUtils.h
//  Fish
//
//  Created by Yaozong Gao on 7/26/13.
//
//

#ifndef __PDMUtils_h__
#define __PDMUtils_h__


#include "mesh/PDM.h"
#include "common/stringUtils.h"
#include "common/mathUtils.h"


namespace BRIC { namespace IDEA { namespace FISH {
    
class PDMUtils
{
public:
	static vect3<float> Center( const PDM& pdm );

	static void PDMDiff( const PDM& pdm1, const PDM& pdm2, std::vector<float>& diffs );
	static void ExtractLandmarksFromPDM(const std::vector<float>& shape, const std::vector<int>& LMIDs, std::vector<float>& LMs);

	// PDM alignment, orientation, mean estimation
    static void PDMOrientation( const PDM& pdm, std::vector< vect3<float> >& directions );

	template <class TransformType>
	static double AverageLandmarkDistance( const PDM& pdm1, const PDM& pdm2, TransformType& T );

	static double HausdroffDistance( const PDM& pdm1, const PDM& pdm2 );

	template <class TransformType>
	static unsigned int CommonPDM( const std::vector<PDM*>& pdms, TransformType& T );

	template <class TransformType>
	static void AlignToTemplate( const std::vector<PDM*>& pdms, unsigned int templateIdx, TransformType& T );

	template <class TransformType>
	static void AlignToTemplateWithLandmarks( const std::vector<PDM*>& pdms, const std::vector<PDM*>& landmarkPDMs, unsigned int templateIdx, TransformType& T );

	static PDM MeanPDM( const std::vector<PDM*>& alignedPDMs );

	static void LinearCombine( const PDM& pdm1, float weight1, const PDM& pdm2, float weight2, PDM& outPDM );

	// PDM IO
	static bool SavePoints( const PDM& pdm, const char* outPath);
	static bool WritePDMtoTxt( const PDM& pdm, const char* outPath, char delim = ' ');
	static bool ReadPDMFromTxt( const char* inpath, PDM& pdm, char delim = ' ');
	static bool ReadLandmarksFromTxt( const char* inpath, std::map< int, vect3<float> >& lms );
	static bool ReadLandmarksFromTxt( const char* inpath, PDM& pdm, std::vector<int>& lm_ids );
	static bool WriteLandmarksAsTxt( const PDM& pdm, const std::vector<int>& lm_ids, const char* outpath);
};

//////////////////////////////////////////////////////////////////////////

vect3<float> PDMUtils::Center( const PDM& pdm )
{
	vect3<float> c(0,0,0);
	const std::vector<float>& pt = pdm.GetPoints();
	int numPts = pt.size() / 3;
	for(int i = 0; i < numPts; ++i)
	{
		c[0] += pt[i*3];
		c[1] += pt[i*3+1];
		c[2] += pt[i*3+2];
	}
	c /= numPts;
	return c;
}

void PDMUtils::ExtractLandmarksFromPDM(const std::vector<float>& shape, const std::vector<int>& LMIDs, std::vector<float>& LMs)
{
	int numLMs = LMIDs.size();
	LMs.resize(numLMs * 3);
	for(int i = 0; i < numLMs; ++i)
	{
		int k = LMIDs[i];
		LMs[i*3] = shape[k*3];
		LMs[i*3+1] = shape[k*3+1];
		LMs[i*3+2] = shape[k*3+2];
	}
}

void PDMUtils::PDMDiff( const PDM& pdm1, const PDM& pdm2, std::vector<float>& diffs )
{
	if(pdm1.GetPoints().size() != pdm2.GetPoints().size())
	{
		std::cerr << "pdm1 and pdm2: vertex number not the same" << std::endl;
		return;
	}

	int numVerts = pdm1.GetPoints().size() / 3;

	diffs.resize(numVerts);
	for (int i = 0; i < numVerts; ++i)
	{
		vect3<float> diff;
		diff[0] = pdm1.GetPoints()[i*3] - pdm2.GetPoints()[i*3];
		diff[1] = pdm1.GetPoints()[i*3+1] - pdm2.GetPoints()[i*3+1];
		diff[2] = pdm1.GetPoints()[i*3+2] - pdm2.GetPoints()[i*3+2];

		diffs[i] = static_cast<float>( diff.l2norm() );
	}
}
    
// useful for tubular or planar objects
void PDMUtils::PDMOrientation( const PDM& pdm, std::vector< vect3<float> >& directions )
{
    unsigned int numVerts = pdm.m_verts.size() / 3;
    
    vnl_matrix<float> mat;
    mat.set_size(3,numVerts);
    for (unsigned int i = 0; i < numVerts; ++i)
    {
        mat(0,i) = pdm.m_verts[i*3];
        mat(1,i) = pdm.m_verts[i*3+1];
        mat(2,i) = pdm.m_verts[i*3+2];
    }
    
    vnl_matrix<float> comps;
    std::vector<float> vars;
    mathUtils::PCA(mat, comps, vars, true);
    
    directions.resize(3);
    for (unsigned int i = 0; i < 3; ++i) {
        directions[i][0] = comps(0,i);
        directions[i][1] = comps(1,i);
        directions[i][2] = comps(2,i);
    }
}


template <class TransformType>
double PDMUtils::AverageLandmarkDistance( const PDM& pdm1, const PDM& pdm2, TransformType& T )
{
	PDM alignedPDM;
	T(pdm1, pdm2, alignedPDM);

	const std::vector<float>& verts1 = pdm1.GetPoints();
	const std::vector<float>& verts2 = pdm2.GetPoints();

	double averageDistance = 0;
	unsigned int numVerts = verts1.size() / 3;
	for (unsigned int i = 0; i < numVerts; ++i)
	{
		vect3<float> diff;
		diff[0] = verts1[i*3] - verts2[i*3];
		diff[1] = verts1[i*3+1] - verts2[i*3+1];
		diff[2] = verts1[i*3+2] - verts2[i*3+2];

		averageDistance += sqrt( std::abs(diff[0]*diff[0] + diff[1]*diff[1] + diff[2]*diff[2]) );
	}
	averageDistance /= numVerts;
	return averageDistance;
}


double PDMUtils::HausdroffDistance( const PDM& pdm1, const PDM& pdm2 )
{
	const std::vector<float>& pts1 = pdm1.GetPoints();
	const std::vector<float>& pts2 = pdm2.GetPoints();

	int num1 = pts1.size() / 3, num2 = pts2.size() / 3;

	double d1 = -1;
	for (int i = 0; i < num1; ++i)
	{
		double d = std::numeric_limits<double>::max();
		for(int j = 0; j < num2; ++j)
		{
			vect3<float> pt1, pt2;
			pt1[0] = pts1[i*3]; pt1[1] = pts1[i*3+1]; pt1[2] = pts1[i*3+2];
			pt2[0] = pts2[j*3]; pt2[1] = pts2[j*3+1]; pt2[2] = pts2[j*2+1];

			vect3<float> diff = pt1 - pt2;
			if(diff.l2norm() < d)
				d = diff.l2norm();
		}
		if( d > d1 )
			d1 = d;
	}

	double d2 = -1;
	for (int i = 0; i < num2; ++i)
	{
		double d = std::numeric_limits<double>::max();
		for(int j = 0; j < num1; ++j)
		{
			vect3<float> pt1, pt2;
			pt1[0] = pts1[i*3]; pt1[1] = pts1[i*3+1]; pt1[2] = pts1[i*3+2];
			pt2[0] = pts2[j*3]; pt2[1] = pts2[j*3+1]; pt2[2] = pts2[j*2+1];

			vect3<float> diff = pt1 - pt2;
			if(diff.l2norm() < d)
				d = diff.l2norm();
		}
		if( d > d2 )
			d2 = d;
	}

	return std::max(d1,d2);
}

// pick a PDM that can be considered as mean shape (correspondence required)
template <class TransformType>
unsigned int PDMUtils::CommonPDM( const std::vector<PDM*>& pdms, TransformType& T )
{
	vnl_matrix<double> distMatrix;
	distMatrix.set_size(pdms.size(), pdms.size());

	for (unsigned int i = 0; i < pdms.size(); ++i)
	{
		for (unsigned int j = 0; j < pdms.size(); ++j)
		{
			if(i != j)
				distMatrix(i,j) = AverageLandmarkDistance(*pdms[i], *pdms[j], T);
		}
	}

	std::vector<double> distSum;
	distSum.resize(pdms.size());
	for (unsigned int i = 0; i < pdms.size(); ++i)
	{
		distSum[i] = 0;
		for (unsigned int j = 0; j < pdms.size(); ++j)
		{
			if(i != j)
				distSum[i] += distMatrix(i,j);
		}
		distSum[i] /= pdms.size();
	}

	unsigned int commonIdx = 0;
	double minDist = std::numeric_limits<double>::max();

	for (unsigned int i = 0; i < pdms.size(); ++i)
	{
		if( distSum[i] < minDist )
		{
			commonIdx = i;
			minDist = distSum[i]; 
		}
	}
	return commonIdx;
}


template <class TransformType>
void PDMUtils::AlignToTemplate( const std::vector<PDM*>& pdms, unsigned int templateIdx, TransformType& T )
{
	for (unsigned int i = 0; i < pdms.size(); ++i)
	{
		if (i != templateIdx)
			T(*pdms[i], *pdms[templateIdx], *pdms[i]);
	}
}

template <class TransformType>
void PDMUtils::AlignToTemplateWithLandmarks( const std::vector<PDM*>& pdms, const std::vector<PDM*>& landmarkPDMs, unsigned int templateIdx, TransformType& T )
{
	if(pdms.size() != landmarkPDMs.size()) 
	{
		std::cerr << "number of landmark PDMs != dense PDMs" << std::endl;
		assert(false);
		exit(-1);
	}

	for (unsigned int i = 0; i < pdms.size(); ++i)
	{
		if (i != templateIdx) {
			T(*pdms[i], *pdms[templateIdx], *pdms[i]);
			T.Apply(*landmarkPDMs[i], *landmarkPDMs[i]);
		}
	}
}

PDM PDMUtils::MeanPDM( const std::vector<PDM*>& alignedPDMs )
{
	if(alignedPDMs.size() == 0) 
	{
		std::cerr << "empty PDM array" << std::endl;
		assert(false);
		exit(-1);
	}

	unsigned int numVerts = alignedPDMs[0]->GetPoints().size() / 3;

	PDM mean;
	std::vector<float>& meanVerts = mean.GetPoints();
	meanVerts.resize(numVerts * 3);
	for (unsigned int i = 0; i < meanVerts.size(); ++i)
		meanVerts[i] = 0;
	
	for (unsigned int i = 0; i < alignedPDMs.size(); ++i)
	{
		const std::vector<float>& verts = alignedPDMs[i]->GetPoints();
		for (unsigned int j = 0; j < numVerts * 3; ++j)
			meanVerts[j] += verts[j];
	}
	
	for (unsigned int j = 0; j < numVerts * 3; ++j)
		meanVerts[j] /= alignedPDMs.size();

	return mean;
}

void PDMUtils::LinearCombine( const PDM& pdm1, float weight1, const PDM& pdm2, float weight2, PDM& outPDM )
{
	const std::vector<float>& verts1 = pdm1.GetPoints();
	const std::vector<float>& verts2 = pdm2.GetPoints();
	std::vector<float>& outVerts = outPDM.GetPoints();
	
	if (verts1.size() != verts2.size()) {
		std::cerr << "vert # of pdm1 and pdm2 are not same" << std::endl; exit(-1);
	}

	outVerts.resize(verts1.size());
	for (unsigned int i = 0; i < verts1.size(); ++i)
		outVerts[i] = verts1[i]*weight1 + verts2[i]*weight2;
}

bool PDMUtils::SavePoints( const PDM& pdm, const char* outputPath)
{
	const std::vector< float >& points = pdm.GetPoints();
	if( points.size() % 3 != 0 ) 
	{
		std::cerr << "Incomplete PDM" << std::endl;
		return false;
	}
	int numPoints = static_cast<int>( points.size() / 3 );

	FILE *fp = fopen(outputPath, "w");
	if(fp == NULL)
	{
		std::cerr << "fail to open file for write: " << outputPath << std::endl;
		return false;
	}

	fprintf(fp, "# vtk DataFile Version 3.0\n");
	fprintf(fp, "Automatically Generated Points by fish\n");
	fprintf(fp, "ASCII\n");
	fprintf(fp, "DATASET UNSTRUCTURED_GRID\n");
	fprintf(fp, "POINTS %d float\n", numPoints);

	for (int i = 0; i < numPoints; ++i)
		fprintf(fp,"%f %f %f\n", points[i*3], points[i*3+1], points[i*3+2]);

	fprintf(fp, "CELLS %d %d\n", numPoints, numPoints*2);
	for( int i = 0; i < numPoints; ++i )
		fprintf(fp, "1 %d\n", i);
	fprintf(fp,"CELL_TYPES %d\n", numPoints);
	for( int i = 0; i < numPoints; ++i )
		fprintf(fp,"2\n");

	fclose(fp);
	return true;
}

bool PDMUtils::WritePDMtoTxt( const PDM& pdm, const char* outPath, char delim )
{
	const std::vector<float>& pts = pdm.GetPoints();
	unsigned int point_num = pts.size() / 3;

	std::ofstream out(outPath);
	if(!out) { std::cerr << "fail to open " << outPath << " for write" << std::endl; return false; }

	for (unsigned int i = 0; i < point_num; ++i) 
		out << pts[i*3] << delim << pts[i*3+1] << delim << pts[i*3+2] << std::endl;
	
	out.close();
	return true;
}

bool PDMUtils::ReadPDMFromTxt( const char* inpath, PDM& pdm, char delim )
{
	std::vector<std::string> lines;
	if(!stringUtils::ReadLines(inpath, lines)) {
		std::cerr << "fail to read lines from " << inpath << std::endl; 
		return false;
	}

	std::vector<float>& pts = pdm.GetPoints();
	pts.resize(lines.size()*3);

	for (unsigned int i = 0; i < lines.size(); ++i)
	{
		std::vector<float> vec;
		stringUtils::str2vec(lines[i], vec, delim);

		if(vec.size() < 3) { std::cerr << "txt file format incorrect" << std::endl;  return false; }

		pts[i*3] = vec[0];
		pts[i*3+1] = vec[1];
		pts[i*3+2] = vec[2];
	}

	return true;
}

bool PDMUtils::ReadLandmarksFromTxt(const char* inpath, std::map< int, vect3<float> >& lms)
{
	std::vector<std::string> lines;
	if (!stringUtils::ReadLines(inpath, lines)) {
		std::cerr << "fail to read lines from " << inpath << std::endl;
		return false;
	}

	for (unsigned int i = 0; i < lines.size(); ++i)
	{
		std::vector<float> vec;
		stringUtils::str2vec(lines[i], vec);

		if (vec.size() < 7) { std::cerr << "txt file format incorrect" << std::endl;  return false; }

		int id = static_cast<int>(vec[0]);
		lms[id][0] = vec[4];
		lms[id][1] = vec[5];
		lms[id][2] = vec[6];
	}

	return true;
}

bool PDMUtils::ReadLandmarksFromTxt(const char* inpath, PDM& pdm, std::vector<int>& lm_ids)
{
	std::map<int, vect3<float> > landmarks;
	if (!ReadLandmarksFromTxt(inpath, landmarks))
		return true;

	std::vector<float>& pts = pdm.GetPoints();
	pts.clear();

	pts.resize(landmarks.size() * 3);
	lm_ids.resize(landmarks.size());

	typedef std::map<int, vect3<float> >::const_iterator iterator_type;
	iterator_type it = landmarks.begin();
	int idx = 0;

	while (it != landmarks.end())
	{
		lm_ids[idx / 3] = it->first;

		pts[idx] = it->second[0];
		pts[idx + 1] = it->second[1];
		pts[idx + 2] = it->second[2];

		idx = idx + 3;
		++it;
	}

	return true;
}

bool PDMUtils::WriteLandmarksAsTxt(const PDM& pdm, const std::vector<int>& lm_ids, const char* outpath)
{
	const std::vector<float>& pts = pdm.GetPoints();

	if (pts.size()/3 != lm_ids.size()) {
		std::cerr << "landmark size not consistent between ids and pdm" << std::endl; 
		return false;
	}

	std::ofstream out(outpath);
	if (!out) {
		std::cerr << "cannot open " << outpath << " for write" << std::endl; 
		return false;
	}

	for (size_t i = 0; i < lm_ids.size(); ++i)
		out << lm_ids[i] << " -1 -1 -1 " << pts[i * 3] << " " << pts[i * 3 + 1] << " " << pts[i * 3 + 2] << std::endl;
	
	out.close();
	return true;
}

} } }


#endif
