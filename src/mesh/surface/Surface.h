//
//  Surface.h
//  FISH
//
//  Created by Yaozong Gao on 6/27/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __Surface_h__
#define __Surface_h__

#include "mesh/PDM.h"
#include "common/aux_func.h"
#include "extern/tritri.h"

namespace BRIC { namespace IDEA { namespace FISH {


/** @brief surface class */
class Surface: public PDM {

private:

	std::vector< vect3<int> > m_faces;
	std::vector< vect3<float> > m_normals;
	std::vector< std::vector<int> > m_nbrs;
	std::vector< std::vector<int> > m_faceNbrs;
    
public:
    
    friend class SurfaceUtils;
    
	/** @brief default constructor */
    Surface() {}

	/** @brief copy constructor */
	Surface(const Surface& rhs): PDM(rhs) {
		m_faces = rhs.m_faces;
		m_normals = rhs.m_normals;
		m_nbrs = rhs.m_nbrs;
		m_faceNbrs = rhs.m_faceNbrs;
	}
    
	/** @brief destructor */
    ~Surface() { Clear(); }

    // attribute access
	std::vector< vect3<int> >& GetFaces() { return m_faces; }
	const std::vector< vect3<int> >& GetFaces() const { return m_faces; }
	std::vector< vect3<float> >& GetNormals() { return m_normals; }
	const std::vector< vect3<float> >& GetNormals() const {	return m_normals; }
	std::vector< std::vector<int> >& GetNeighborhood() { return m_nbrs; }
	const std::vector< std::vector<int> >& GetNeighborhood() const { return m_nbrs; }
	std::vector< std::vector<int> >& GetFaceNeighborhood() { return m_faceNbrs;  }
	const std::vector< std::vector<int> >& GetFaceNeighborhood() const { return m_faceNbrs; }
    
	/** @brief Clear the internal data */
    void Clear();

	/** @brief override assignment operator */
	Surface& operator=(const Surface& rhs);

	/** @brief copy face information from another surface */
	void CopyFace(const Surface& rhs);

	/** @brief update normals */
	void UpdateNormals();

	/** @brief update vertex neighbors */
	void UpdateNeighbors();

	/** @brief update face neighbors */
	void UpdateFaceNeighbors();

	/** @brief mean edge length */
	float MeanEdgeLength();

	/** @brief get neighborhood center */
	vect3<float> GetNeighborCenter(unsigned int vertIdx);

	/** @brief get neighborhood face center (area weighted) to encourage uniform distribution of vertices */
	vect3<float> GetNeighborFaceCenter(unsigned int vertIdx);

	/** @brief scale the surface w.r.t. to the centroid */
	void Scale(const vect3<float>& ratio);

	/** @brief surface size in world units */
	vect3<float> Size() const;

	/** @brief face area */
	double FaceArea(int i) const;

	/** @brief face */
	vect3<float> FaceCenter(int i) const;

	/** @brief check collision by deforming one vertex */
	bool VertexCollisonTest(int vertIdx, const float* npos);


	//////////////////////////////////////////////////////////////////////////
	// static functions

	/** @brief neighboring faces */
	static bool isNeighborFace(const vect3<int>& face_a, const vect3<int>& face_b);

};

//////////////////////////////////////////////////////////////////////////
    
void Surface::Clear()
{
    m_faces.clear();
    m_normals.clear();
    m_nbrs.clear();
	m_faceNbrs.clear();
	PDM::Clear();
}
    
Surface& Surface::operator=(const Surface& rhs)
{
	if(this == &rhs)
        return *this;
    PDM::operator=(rhs);
    m_faces = rhs.m_faces;
    m_normals = rhs.m_normals;
    m_nbrs = rhs.m_nbrs;
	m_faceNbrs = rhs.m_faceNbrs;
    return *this;
}

void Surface::CopyFace(const Surface& rhs)
{
	m_faces = rhs.m_faces;
}

void Surface::UpdateNormals()
{
	if (m_verts.size() == 0 || m_faces.size() == 0)
		return;

	m_normals.clear();
	m_normals.resize(m_verts.size()/3);

	for (unsigned int i = 0; i < m_normals.size(); ++i) {
		m_normals[i][0] = 0;
		m_normals[i][1] = 0;
		m_normals[i][2] = 0;
	}

	for (unsigned int i = 0; i < m_faces.size(); ++i) {

		int v1 = m_faces[i][0];
		int v2 = m_faces[i][1];
		int v3 = m_faces[i][2];

		vect3<float> _v1(m_verts[v1*3], m_verts[v1*3+1], m_verts[v1*3+2]);
		vect3<float> _v2(m_verts[v2*3], m_verts[v2*3+1], m_verts[v2*3+2]);
		vect3<float> _v3(m_verts[v3*3], m_verts[v3*3+1], m_verts[v3*3+2]);

		vect3<float> vec1, vec2;
		vec1 = _v2 - _v1;
		vec2 = _v3 - _v1;

		vect3<float> face_normal = vect3<float>::cross_product(vec1, vec2);
		double norm = face_normal.l2norm();
		if (norm <= VECT_EPSILON)
			continue;

		face_normal /= norm;

		m_normals[v1] += face_normal;
		m_normals[v2] += face_normal;
		m_normals[v3] += face_normal;
	}

	for (unsigned int i = 0; i < m_normals.size(); ++i) {
		double norm = m_normals[i].l2norm();
		if (norm < VECT_EPSILON) {
			std::cerr << "cannot compute the normal vector for a vertex!" << std::endl; exit(-1);
		}
		m_normals[i] /= norm;
	}
}

void Surface::UpdateNeighbors()
{
	std::vector< std::set<int> > nbr_sets;
	nbr_sets.resize(m_verts.size() / 3);

	for (unsigned int i = 0; i < m_faces.size(); ++i) {

		vect3<int> face = m_faces[i];

		nbr_sets[face[0]].insert( face[1] );
		nbr_sets[face[0]].insert( face[2] );

		nbr_sets[face[1]].insert( face[0] );
		nbr_sets[face[1]].insert( face[2] );

		nbr_sets[face[2]].insert( face[0] );
		nbr_sets[face[2]].insert( face[1] );
	}

	// copy back nbr_sets info to m_nbrs
	m_nbrs.clear();
	m_nbrs.resize(nbr_sets.size());
	for (unsigned int i = 0; i < nbr_sets.size(); ++i) {
		std::set<int>::iterator it = nbr_sets[i].begin();
		while (it != nbr_sets[i].end()) {
			m_nbrs[i].push_back(*it);
			++ it;
		}
	} 
}

void Surface::UpdateFaceNeighbors()
{
	m_faceNbrs.clear();

	int vert_num = static_cast<int>(m_verts.size() / 3);
	m_faceNbrs.resize(vert_num);

	for (unsigned int i = 0; i < m_faces.size(); ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			int vert_idx = m_faces[i][j];
			m_faceNbrs[vert_idx].push_back(i);
		}
	}
}

vect3<float> Surface::GetNeighborCenter(unsigned int vertIdx)
{
	assert_message(m_nbrs.size() != 0, "empty neighborhood");

	vect3<float> center(0, 0, 0);
	for (size_t i = 0; i < m_nbrs[vertIdx].size(); ++i)
	{
		int nb_idx = m_nbrs[vertIdx][i];
		for (int j = 0; j < 3; ++j)
			center[j] += m_verts[nb_idx * 3 + j];
	}

	center /= m_nbrs[vertIdx].size();
	return center;
}

vect3<float> Surface::GetNeighborFaceCenter(unsigned int vert_idx)
{
	assert_message(m_faceNbrs.size() != 0, "empty face neighborhood");

	vect3<float> area_weighted_center(0, 0, 0);
	double area_sum = 0;

	for (size_t j = 0; j < m_faceNbrs[vert_idx].size(); ++j)
	{
		int face_idx = m_faceNbrs[vert_idx][j];
		float face_area = static_cast<float>(FaceArea(face_idx));
		vect3<float> face_center = FaceCenter(face_idx);

		area_sum += face_area;
		area_weighted_center += face_center * face_area;
	}

	if (area_sum > 0)
		area_weighted_center /= area_sum;
	else
	{
		for (int j = 0; j < 3; ++j)
			area_weighted_center[j] = m_verts[vert_idx * 3 + j];
	}

	return area_weighted_center;
}

float Surface::MeanEdgeLength()
{
	double sum = 0;

	for (size_t i = 0; i < m_faces.size(); ++i)
	{
		vect3<int> face = m_faces[i];
		int idx1 = face[0], idx2 = face[1], idx3 = face[2];

		vect3<float> pt1, pt2, pt3;
		pt1[0] = m_verts[idx1 * 3]; pt1[1] = m_verts[idx1 * 3 + 1]; pt1[2] = m_verts[idx1 * 3 + 2];
		pt2[0] = m_verts[idx2 * 3]; pt2[1] = m_verts[idx2 * 3 + 1]; pt2[2] = m_verts[idx2 * 3 + 2];
		pt3[0] = m_verts[idx3 * 3]; pt3[1] = m_verts[idx3 * 3 + 1]; pt3[2] = m_verts[idx3 * 3 + 2];

		sum += (pt1 - pt2).norm();
		sum += (pt1 - pt3).norm();
		sum += (pt2 - pt3).norm();
	}

	return static_cast<float>( sum / (m_faces.size() * 3) );
}

void Surface::Scale(const vect3<float>& ratio)
{
	vect3<float> center = this->Center();
	size_t pt_num = m_verts.size() / 3;

	for (size_t i = 0; i < pt_num; ++i)
	{
		vect3<float> pt(m_verts[i*3], m_verts[i*3+1], m_verts[i*3+2]);
		pt = (pt - center);
		
		for (int k = 0; k < 3; ++k)
			pt[k] = pt[k] * ratio[k] + center[k];
		
		m_verts[i * 3] = pt[0];
		m_verts[i * 3 + 1] = pt[1];
		m_verts[i * 3 + 2] = pt[2];
	}
}
    
vect3<float> Surface::Size() const
{
	size_t vert_num = m_verts.size() / 3;

	vect3<float> min, max;
	for (int i = 0; i < 3; ++i)
	{
		min[i] = std::numeric_limits<float>::max();
		max[i] = -std::numeric_limits<float>::max();
	}

	for (size_t i = 0; i < vert_num; ++i)
	{
		vect3<float> vert;
		for (int j = 0; j < 3; ++j)
			vert[j] = m_verts[i * 3 + j];

		for (int j = 0; j < 3; ++j)
		{
			if (vert[j] > max[j])
				max[j] = vert[j];
			if (vert[j] < min[j])
				min[j] = vert[j];
		}
	}

	return max - min;
}

double Surface::FaceArea(int i) const
{
	vect3<int> face = m_faces[i];

	vect3<float> vert1, vert2, vert3;
	int idx1 = face[0], idx2 = face[1], idx3 = face[2];

	for (int j = 0; j < 3; ++j)
	{
		vert1[j] = m_verts[idx1 * 3 + j];
		vert2[j] = m_verts[idx2 * 3 + j];
		vert3[j] = m_verts[idx3 * 3 + j];
	}

	double len1 = (vert2 - vert1).l2norm();
	double len2 = (vert3 - vert1).l2norm();
	double len3 = (vert3 - vert2).l2norm();

	double p = (len1 + len2 + len3) / 2.0;
	double area = sqrt(p * (p - len1) * (p - len2) * (p - len3));

	return area;
}

vect3<float> Surface::FaceCenter(int i) const
{
	vect3<float> center(0, 0, 0);

	for (int j = 0; j < 3; ++j)
	{
		int vert_idx = m_faces[i][j];
		
		for (int k = 0; k < 3; ++k)
			center[k] += m_verts[vert_idx * 3 + k];
	}

	center /= 3;
	return center;
}

bool Surface::isNeighborFace(const vect3<int>& face_a, const vect3<int>& face_b) 
{
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			if (face_a[i] == face_b[j])
				return true;
		}
	}
	return false;
}

bool Surface::VertexCollisonTest(int vertIdx, const float* npos)
{
	assert_message(m_faceNbrs.size() > 0, "vertex face-neighborhood not computed");

	// backup the old vertex position and update with new vertex position
	float& vert_x = m_verts[vertIdx * 3];
	float& vert_y = m_verts[vertIdx * 3 + 1];
	float& vert_z = m_verts[vertIdx * 3 + 2];

	float pos_saved[3] = { vert_x, vert_y, vert_z };			// backup
	vert_x = npos[0];  vert_y = npos[1];  vert_z = npos[2];		// update

	bool isCollision = false;
	#pragma omp parallel for
	for (int k = 0; k < m_faceNbrs[vertIdx].size(); ++k)
	{
		#pragma omp flush (isCollision)
		if (!isCollision)
		{
			const vect3<int>& target_face = m_faces[m_faceNbrs[vertIdx][k]];

			const float* a = &(m_verts[target_face[0] * 3]);
			const float* b = &(m_verts[target_face[1] * 3]);
			const float* c = &(m_verts[target_face[2] * 3]);

			for (int j = 0; j < m_faces.size(); ++j)
			{
				const vect3<int>& face = m_faces[j];
				if (isNeighborFace(target_face, face))
					continue;

				const float* d = &(m_verts[face[0] * 3]);
				const float* e = &(m_verts[face[1] * 3]);
				const float* f = &(m_verts[face[2] * 3]);

				int ret = tri_tri_intersect(a, b, c, d, e, f);
				if (ret > 0)
				{
					isCollision = true;
					#pragma omp flush (isCollision)
				}
			}
		}
	}

	// restore
	vert_x = pos_saved[0];  vert_y = pos_saved[1];  vert_z = pos_saved[2];

	return isCollision;
}

} } }

#endif
