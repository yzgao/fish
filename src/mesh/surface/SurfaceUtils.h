//
//  SurfaceUtils.h
//  Fish
//
//  Created by Yaozong Gao on 7/26/13.
//
//

#ifndef __SurfaceUtils_h__
#define __SurfaceUtils_h__

#include "mesh/surface/Surface.h"
#include "extern/ImageHelper.h"
#include "common/mxImageWrapper.h"
#include "common/stringUtils.h"

// ITK headers
#include "itkMesh.h"
#include "itkVTKPolyDataReader.h"
#include "itkVTKPolyDataWriter.h"
#include "itkRGBAPixel.h"
#include "itkTriangleMeshToBinaryImageFilter.h"
#include "itkBinaryMask3DMeshSource.h"

// VTK headers
#include "vtkPolyData.h"
#include "vtkCellArray.h"
#include "extern/itkImageToVTKImageFilter.h"
#include "vtkSmartPointer.h"
#include "vtkMarchingCubes.h"
#include "vtkWindowedSincPolyDataFilter.h"
#include "vtkSmoothPolyDataFilter.h"


namespace BRIC { namespace IDEA { namespace FISH {

    
class SurfaceUtils
{
public:
    
    // itk Mesh Type Declaration
    static const unsigned int PointDimension = 3;   // 3D space
    static const unsigned int MaxCellDimension = 2; // surface
    typedef unsigned char PixelType;                // dummy type, not used
    typedef unsigned char CellDataType;             // dummy type, not used
    typedef float CoordRepType;                     // float coordinates
    typedef double InterpolationWeightType;         // use double to do interpolation
    
    typedef itk::DefaultStaticMeshTraits< PixelType, PointDimension, MaxCellDimension, CoordRepType, InterpolationWeightType, CellDataType > MeshTraits;
    typedef itk::Mesh< PixelType, PointDimension, MeshTraits > MeshType;
    
    
    // Surface IO operators
    static bool LoadSurfaceFromVTK(const char* vtkFilename, Surface& surface);
    static bool LoadSurfaceFromISO(const char* filename, Surface& surface);
    static bool LoadSurface(const char* filename, Surface& surface);
    
    static bool SaveSurfaceAsVTK(const Surface& surface, const char* vtkFilename);
    static bool SaveSurfaceAsISO(const Surface& surface, const char* filename);
	static bool SaveSurface(const Surface& surface, const char* filename);
	static bool SaveSurface(const float* verts, unsigned int numVerts, const std::vector< vect3<int> >& faces, const char* filename);

	static bool Save3DVectorField(const boost::array< mxImage<double>, 3 >& vectorField, vect3<unsigned int> sp, vect3<unsigned int> ep, vect3<unsigned int> stepsizes, const char* filename);

	// Surface auxiliary
	static void SortVerticesByZYX(Surface& surf);
	static void ReorderVertices(Surface& surf);
	static void BoundingBox(const Surface& surf, vect3<float>& sp, vect3<float>& ep);
	template <typename T>
	static void BoundingBox(const Surface& surf, const mxImage<T>& image, vect3<int>& sp, vect3<int>& ep);
	static void BoundingBox(const Surface& surf, const mxImageWrapper& image, vect3<int>& sp, vect3<int>& ep);

	// type conversion functions
	static void vtkPolyData2Surface(vtkPolyData* polyData, Surface& surface);
	static void Surface2vtkPolyData(const Surface& surface, vtkPolyData*& polyData);
	static void Surface2itkMesh(const Surface& surface, MeshType::Pointer mesh);
	static void itkMesh2Surface(MeshType::Pointer mesh, Surface& surface);

	// surface-image functions
	template <typename T>
	static void IsoSurfaceFromImage(const mxImage<T>& image, Surface& surface, T iso_val);

	template <typename T>
	static void IsoSurfaceFromImage2(const mxImage<T>& image, double smooth_sigma, double spacing, int isoval, Surface& surf);

	template <typename T1, typename T2>
	static void CarveSurface(const Surface& surface, const mxImage<T1>& refImage, T2 outside_val, T2 inside_val, mxImage<T2>& output_image);

	template <typename T1, typename T2>
	static void FastCarveSurface(const Surface& surface, const mxImage<T1>& refImage, T2 outside_val, T2 inside_val, const vect3<int>& voxel_pad, mxImage<T2>& output_image);

	template <typename T1, typename T2>
	static void SmartCarveSurface(const Surface& surface, const mxImage<T1>& refImage, T2 outside_val, T2 inside_val, mxImage<T2>& output_image, vect3<int>& sp, vect3<int>& ep);

	// surface operations
	static void SmoothSurface(Surface& surface, unsigned int iterations = 15, double pass_band = 0.1);
	static unsigned int GetVertexNeighborhood(const Surface& surface, unsigned int vertIdx, unsigned int nbSize, std::vector<unsigned int>& nbVertIdxs, std::vector<unsigned int>& nbVertLevels);

	// surface and sub-surface operations
	static void AddSubsurface(Surface& surf, const Surface& sub_surf);

	// morpholocal operations
	static void DilateShrinkSurface(Surface& surf, float size);

	// flip
	static void FlipX(Surface& surf);
	static void FlipY(Surface& surf);
	static void FlipZ(Surface& surf);

	// Re-mesh
	static void Remesh(Surface& surf, float stepsize, int iter_num);
};
    
//////////////////////////////////////////////////////////////////////////
    
bool SurfaceUtils::LoadSurfaceFromVTK(const char* vtkFilename, Surface& surface)
{
    typedef itk::VTKPolyDataReader<MeshType> MeshReaderType;
    MeshReaderType::Pointer meshReader = MeshReaderType::New();
    
    meshReader->SetFileName(vtkFilename);
    
    try {
        meshReader->Update();
    } catch (const itk::ExceptionObject& exp) {
        std::cerr << exp.GetDescription() << std::endl;
        return false;
    }
    
    MeshType::Pointer mesh = meshReader->GetOutput();
    
    // setup vertices
    surface.m_verts.resize(mesh->GetNumberOfPoints() * 3);
    
    // printf("The number of vertices loaded : %ld\n", mesh->GetNumberOfPoints());
    
    typedef MeshType::PointsContainer::Iterator PointIteratorType;
    typedef MeshType::PointType PointType;
    
    PointIteratorType pointIterator = mesh->GetPoints()->Begin();
    PointIteratorType pointEnd = mesh->GetPoints()->End();
    
    unsigned int idx = 0;
    while (pointIterator != pointEnd) {
        
        PointType pt = pointIterator.Value();
        surface.m_verts[idx*3] = pt[0];
        surface.m_verts[idx*3+1] = pt[1];
        surface.m_verts[idx*3+2] = pt[2];
        
        // printf("%f, %f, %f\n", surface.m_verts[idx].x, surface.m_verts[idx].y, surface.m_verts[idx].z);
        
        ++ idx;
        ++ pointIterator;
    }
    
    // setup faces
    typedef itk::TriangleCell<MeshType::CellType> TriangleType;
    typedef MeshType::CellsContainer::Iterator CellIterator;
    CellIterator cellIterator = mesh->GetCells()->Begin();
    CellIterator cellEnd = mesh->GetCells()->End();
    
    idx = 0;
    // setup faces
    surface.m_faces.resize(mesh->GetNumberOfCells());
    
    // printf("The number of cells loaded: %ld\n", mesh->GetNumberOfCells());
    
    while (cellIterator != cellEnd) {
        
        MeshType::CellType* cellptr = cellIterator.Value();
        TriangleType* cell = dynamic_cast<TriangleType *>(cellptr);
        
        assert(cell->GetNumberOfPoints() == 3);
        
        TriangleType::PointIdIterator idIterator = cell->PointIdsBegin();
        TriangleType::PointIdIterator idEnd = cell->PointIdsEnd();
        
        unsigned int subidx = 0;
        while (idIterator != idEnd) {
            
            assert(subidx < 3);
            
            int ptIdx = (*idIterator);
            surface.m_faces[idx][subidx] = ptIdx;
            
            ++ subidx;
            ++ idIterator;
        }
        
        // printf("%d, %d, %d\n", surface.m_faces[idx][0], surface.m_faces[idx][1], surface.m_faces[idx][2]);
        
        ++ idx;
        ++ cellIterator;
    }
    
    return true;
}
    
bool SurfaceUtils::LoadSurfaceFromISO(const char* filename, Surface& surface)
{
    const unsigned int MAX_LINE = 2048;
    
    int i;
    FILE *fp;
    
    fp=fopen(filename,"r");
    if (NULL==fp)
    {
        printf("Can not open file %s for reading.\n",filename);
        return false;
    }
    
    char string[MAX_LINE];
    int verNum,faceNum;
    
    fgets (string , MAX_LINE , fp);
    //puts (string);
    sscanf(string,"%d %d",&verNum,&faceNum);
    printf("verNum=%d triangleNum=%d\n",verNum,faceNum);
    
    // Reading vertices
    surface.m_verts.resize(verNum*3);
    for(i=0;i<verNum;i++)
    {
        fgets (string , MAX_LINE , fp);
        
        ///////////////////////////////////////////////////////
        // Notice: x,y might be flipped
        ///////////////////////////////////////////////////////
        sscanf(string,"%f %f %f",&surface.m_verts[i*3],&surface.m_verts[i*3+1],&surface.m_verts[i*3+2]);
    }
    
    surface.m_faces.resize(faceNum);
    for(i=0;i<faceNum;i++)
    {
        fgets (string , MAX_LINE , fp);
        sscanf(string,"%d %d %d",&surface.m_faces[i][0],&surface.m_faces[i][1],&surface.m_faces[i][2]);
    }
    
    fclose(fp);
    
    return true;
}

bool SurfaceUtils::LoadSurface(const char* filename, Surface& surface)
{
    boost::filesystem::path path(filename);
    std::string extension = path.extension().string();
    
    if (extension == ".vtk")
    {
        return LoadSurfaceFromVTK(filename, surface);
    }else if (extension == ".iso")
    {
        return LoadSurfaceFromISO(filename, surface);
    }else {
        std::cerr << "Unsupported Mesh Type" << std::endl;
        return false;
    }
}
    
bool SurfaceUtils::SaveSurfaceAsVTK(const Surface& surface, const char* vtkFilename)
{
    MeshType::Pointer mesh = MeshType::New();
    
    unsigned int numVerts = surface.m_verts.size() / 3;
    
    // setup vertices
    for (unsigned int i = 0; i < numVerts; ++i) {
        
        MeshType::PointType pt;
        
        for (unsigned int j = 0; j < 3; ++ j)
            pt[j] = surface.m_verts[i*3+j];
        
        mesh->SetPoint(i, pt);
    }
    
    // printf("The number of points in the mesh: %ld \n", mesh->GetNumberOfPoints());
    
    typedef itk::TriangleCell<MeshType::CellType> TriangleType;
    typedef MeshType::CellType::CellAutoPointer CellAutoPointer;
    
    // setup faces
    for (unsigned int i = 0; i < surface.m_faces.size(); ++i) {
        
        CellAutoPointer triangle;
        triangle.TakeNoOwnership(new TriangleType);
        
        triangle->SetPointId(0, surface.m_faces[i][0]);
        triangle->SetPointId(1, surface.m_faces[i][1]);
        triangle->SetPointId(2, surface.m_faces[i][2]);
        
        mesh->SetCell(i, triangle);
    }
    
    // printf("The number of triangles in the mesh: %ld \n", mesh->GetNumberOfCells());
    
    typedef itk::VTKPolyDataWriter<MeshType> MeshWriterType;
    MeshWriterType::Pointer meshWriter = MeshWriterType::New();
    
    meshWriter->SetFileName(vtkFilename);
    meshWriter->SetInput(mesh);
    try {
        meshWriter->Update();
    } catch (const itk::ExceptionObject& exp) {
        std::cerr << exp.GetDescription() << std::endl;
        return false;
    }
    
    return true;
}
    
bool SurfaceUtils::SaveSurfaceAsISO(const Surface& surface, const char* filename) {
    
    int i;
    FILE *fp;
    
    fp=fopen(filename,"w");
    if (NULL==fp)
    {
        printf("Can not open file %s for writing\n",filename);
        return false;
    }
    
    int verNum,faceNum;
    
    verNum=surface.m_verts.size() / 3;
    faceNum=surface.m_faces.size();
    
    fprintf(fp,"%d\t%d\n",verNum,faceNum);
    
    // Write vertices
    for(i=0;i<verNum;i++)
    {
        fprintf(fp,"%f\t%f\t%f\n",surface.m_verts[i*3],surface.m_verts[i*3+1],surface.m_verts[i*3+2]);
    }
    
    // Write m_faces
    for(i=0;i<faceNum;i++)
    {
        fprintf(fp,"%d\t%d\t%d\n",surface.m_faces[i][0],surface.m_faces[i][1],surface.m_faces[i][2]);
    }
    
    fclose(fp);
    return true;
}
    
bool SurfaceUtils::SaveSurface(const Surface& surface, const char* filename) {
    
    boost::filesystem::path path(filename);
    std::string extension = path.extension().string();
    
    if (extension == ".vtk")
    {
        return SaveSurfaceAsVTK(surface, filename);
    }else if (extension == ".iso")
    {
        return SaveSurfaceAsISO(surface, filename);
    }else {
        std::cerr << "Unsupported Output Mesh Type! Use iso and vtk as extension." << std::endl;
        return false;
    }
}
    
bool SurfaceUtils::SaveSurface(const float* vertices, unsigned int numVerts, const std::vector< vect3<int> >& faces, const char* filename) {
    
    Surface surface;
    
    surface.m_verts.resize(numVerts * 3);
    std::copy(vertices, vertices + numVerts * 3, surface.m_verts.begin());
    surface.m_faces = faces;
    
    return SaveSurface(surface, filename);
}

bool SurfaceUtils::Save3DVectorField(const boost::array<mxImage<double>, 3>& vectorField, vect3<unsigned int> sp, vect3<unsigned int> ep, vect3<unsigned int> stepsizes, const char* filename)
{
	vect3<unsigned int> imageSize = vectorField[0].GetImageSize();
	for (int k = 0; k < 3; ++k) {
		sp[k] = std::max(0u, sp[k]);
		ep[k] = std::min(imageSize[k]-1, ep[k]);

		if (ep[k] < sp[k]) {
			std::cerr << "negative ROI?" << std::endl; exit(-1);
		}
	}

	std::ofstream out;
	out.open(filename, std::ios::out);
	if(!out) {
		std::cerr << "fails to open the file for write: " << filename << std::endl;
		return false;
	}

	vect3<unsigned int> new_imageSize;
	for (int k = 0; k < 3; ++k)
		new_imageSize[k] = (ep[k]-sp[k]) / stepsizes[k] + 1;

	out << "# vtk DataFile Version 3.0" << std::endl;
	out << "VTK from Fish Library" << std::endl;
	out << "ASCII" << std::endl << std::endl;
	out << "DATASET STRUCTURED_GRID" << std::endl;
	out << "DIMENSIONS " << new_imageSize[0] << " " << new_imageSize[1] << " " << new_imageSize[2] << std::endl;
	out << "POINTS " << new_imageSize[0] * new_imageSize[1] * new_imageSize[2] << " float" << std::endl;

	// point positions
	for (unsigned int z = 0; z < new_imageSize[2]; ++z) {
		for (unsigned int y = 0; y < new_imageSize[1]; ++y) {
			for (unsigned int x = 0; x < new_imageSize[0]; ++x) {

				vect3<unsigned int> pos(x*stepsizes[0]+sp[0], y*stepsizes[1]+sp[1], z*stepsizes[2]+sp[2]);
				assert( pos[0] >= sp[0] && pos[0] <= ep[0] && pos[1] >= sp[1] && pos[1] <= ep[1] && pos[2] >= sp[2] && pos[2] <= ep[2] );
				out << pos[0] << " " << pos[1] << " " << pos[2] << std::endl;
			}
		}
	}

	// point data
	out << std::endl << "POINT_DATA " << stringUtils::num2str(new_imageSize[0] * new_imageSize[1] * new_imageSize[2]) << std::endl;
	out << "VECTORS vectorField float" << std::endl;

	for (unsigned int z = 0; z < new_imageSize[2]; ++z) {
		for (unsigned int y = 0; y < new_imageSize[1]; ++y) {
			for (unsigned int x = 0; x < new_imageSize[0]; ++x) {

				vect3<unsigned int> pos(x*stepsizes[0]+sp[0], y*stepsizes[1]+sp[1], z*stepsizes[2]+sp[2]);
				assert( pos[0] >= sp[0] && pos[0] <= ep[0] && pos[1] >= sp[1] && pos[1] <= ep[1] && pos[2] >= sp[2] && pos[2] <= ep[2]);

				float u = static_cast<float>( vectorField[0](pos[0], pos[1], pos[2]) );
				float v = static_cast<float>( vectorField[1](pos[0], pos[1], pos[2]) );
				float w = static_cast<float>( vectorField[2](pos[0], pos[1], pos[2]) );

				float mag = sqrt(u*u+v*v+w*w);
				if (static_cast<double>(std::abs(mag)) > VECT_EPSILON) {
					u = u / mag; 
					v = v / mag;
					w = w / mag;
				}

				out << u << " " << v << " " << w << std::endl;
			}
		}
	}

	// scalar data
	out << std::endl << "SCALARS vectorMagnitude float" << std::endl;
	out << "LOOKUP_TABLE default" << std::endl;

	for (unsigned int z = 0; z < new_imageSize[2]; ++z) {
		for (unsigned int y = 0; y < new_imageSize[1]; ++y) {
			for (unsigned int x = 0; x < new_imageSize[0]; ++x) {

				vect3<unsigned int> pos(x*stepsizes[0] + sp[0], y*stepsizes[1] + sp[1], z*stepsizes[2] + sp[2]);
				assert(pos[0] >= sp[0] && pos[0] <= ep[0] && pos[1] >= sp[1] && pos[1] <= ep[1] && pos[2] >= sp[2] && pos[2] <= ep[2]);

				float u = static_cast<float>(vectorField[0](pos[0], pos[1], pos[2]));
				float v = static_cast<float>(vectorField[1](pos[0], pos[1], pos[2]));
				float w = static_cast<float>(vectorField[2](pos[0], pos[1], pos[2]));

				float mag = sqrt(u*u + v*v + w*w);

				out << mag << std::endl;
			}
		}
	}

	out.close();

	return true;
}

void SurfaceUtils::SortVerticesByZYX(Surface& surf) {

	unsigned int num_verts = surf.m_verts.size() / 3;

	typedef std::pair< vect3<float>, int > pair_type;
	std::vector<pair_type> vertices_to_sort;
	vertices_to_sort.resize(num_verts);

	for (unsigned int i = 0; i < num_verts; ++i) {
		vertices_to_sort[i].first[0] = surf.m_verts[i*3];
		vertices_to_sort[i].first[1] = surf.m_verts[i*3+1];
		vertices_to_sort[i].first[2] = surf.m_verts[i*3+2];
		vertices_to_sort[i].second = i;
	}

	std::sort(vertices_to_sort.begin(), vertices_to_sort.end());

	// update vertices and face vertex idxs
	for (unsigned int i = 0; i< num_verts; ++i) {
		surf.m_verts[i*3] = vertices_to_sort[i].first[0];
		surf.m_verts[i*3+1] = vertices_to_sort[i].first[1];
		surf.m_verts[i*3+2] = vertices_to_sort[i].first[2];
	}

	std::vector<int> vert_map;
	vert_map.resize(num_verts);
	for (unsigned int i = 0; i < num_verts; ++i)
		vert_map[vertices_to_sort[i].second] = i;

	for (unsigned int i = 0; i < surf.m_faces.size(); ++i)
		for (unsigned int j = 0; j < 3; ++j)
			surf.m_faces[i][j] = vert_map[surf.m_faces[i][j]];
}

void SurfaceUtils::ReorderVertices(Surface& surf) {

	//
	// sort vertex order according to the z-y-x coordinate
	//
	SortVerticesByZYX(surf);

	//
	// sparsely distribute the vertices
	//
	unsigned int num_verts = surf.m_verts.size() / 3;

	std::vector<int> vert_map;
	vert_map.resize(num_verts);

	std::vector<int> reorder_vert_idxs;
	reorder_vert_idxs.resize(num_verts);

	std::vector<bool> selected;
	selected.resize(num_verts);

	for (unsigned int i = 0; i < selected.size(); ++i) {
		selected[i] = false;
		vert_map[i] = -1;
		reorder_vert_idxs[i] = -1;
	}

	vert_map[0] = 0;						// map the first to the first
	vert_map[vert_map.size() - 1] = 1;		// map the last to the second

	reorder_vert_idxs[0] = 0;
	reorder_vert_idxs[1] = vert_map.size() - 1;

	selected[0] = selected[selected.size() - 1] = true;

	int added_num = 2, current_selected = 0;

	do {
		current_selected = 0;

		// find the front 
		int front = -1;
		for (unsigned int i = 0; i < selected.size(); ++i)
		{
			if (selected[i]) {
				front = i;
				break;
			}
		}

		int rear = front, middle = -1;
		for (unsigned int i = front + 1; i < selected.size(); ++i)
		{
			if (selected[i])
			{
				rear = i;
				
				middle = (front + rear) / 2;
				if (!selected[middle])
				{
					selected[middle] = true;
					reorder_vert_idxs[added_num] = middle;
					vert_map[middle] = added_num;
					++added_num;
					++current_selected;
				}
			}
			front = rear;
		}

	} while (current_selected != 0);
    
    if(static_cast<unsigned int>(added_num) != vert_map.size())
    {
        std::cerr << "vertex reorder wrong!" << std::endl;
        exit(-1);
    }

	// update vertex
	std::vector< float > temp_verts = surf.m_verts;

	for (unsigned int i = 0; i < num_verts; ++i)
	{
		unsigned int map_idx = reorder_vert_idxs[i];
		surf.m_verts[i*3] = temp_verts[map_idx*3];
		surf.m_verts[i*3+1] = temp_verts[map_idx*3+1];
		surf.m_verts[i*3+2] = temp_verts[map_idx*3+2];
	}

	// update faces
	for (unsigned int i = 0; i < surf.m_faces.size(); ++i)
	{
		for (unsigned int j = 0; j < 3; ++j)
		{
			surf.m_faces[i][j] = vert_map[ surf.m_faces[i][j] ];
		}
	}
} 

void SurfaceUtils::BoundingBox(const Surface& surf, vect3<float>& sp, vect3<float>& ep) {

	sp[0] = sp[1] = sp[2] = std::numeric_limits<float>::max();
	ep[0] = ep[1] = ep[2] = -std::numeric_limits<float>::max();

	const std::vector<float>& pts = surf.GetPoints();;
	unsigned int pt_num = pts.size() / 3;
	for (unsigned int i = 0; i < pt_num; ++i)
	{
		for (int k = 0; k < 3; ++k)
		{
			if (pts[i * 3 + k] < sp[k])
				sp[k] = pts[i * 3 + k];
			if (pts[i * 3 + k] > ep[k])
				ep[k] = pts[i * 3 + k];
		}
	}
}

template <typename T>
void SurfaceUtils::BoundingBox(const Surface& surf, const mxImage<T>& image, vect3<int>& sp, vect3<int>& ep)
{
	sp[0] = sp[1] = sp[2] = std::numeric_limits<int>::max();
	ep[0] = ep[1] = ep[2] = -std::numeric_limits<int>::max();

	const std::vector<float>& pts = surf.GetPoints();;
	unsigned int pt_num = pts.size() / 3;
	for (unsigned int i = 0; i < pt_num; ++i)
	{
		vect3<float> pt(pts[i * 3], pts[i * 3 + 1], pts[i * 3 + 2]);
		vect3<int> voxel;
		mxImageUtils::World2Voxel(image, pt, voxel);
		if (!image.PtInImage(voxel[0], voxel[1], voxel[2]))
			continue;

		for (int k = 0; k < 3; ++k)
		{
			if (voxel[k] < sp[k]) sp[k] = voxel[k];
			if (voxel[k] > ep[k]) ep[k] = voxel[k];
		}
	}
}


void SurfaceUtils::BoundingBox(const Surface& surf, const mxImageWrapper& image, vect3<int>& sp, vect3<int>& ep)
{
	sp[0] = sp[1] = sp[2] = std::numeric_limits<int>::max();
	ep[0] = ep[1] = ep[2] = -std::numeric_limits<int>::max();

	const std::vector<float>& pts = surf.GetPoints();;
	unsigned int pt_num = pts.size() / 3;
	for (unsigned int i = 0; i < pt_num; ++i)
	{
		vect3<float> pt(pts[i * 3], pts[i * 3 + 1], pts[i * 3 + 2]);
		vect3<int> voxel;
		image.World2Voxel(pt, voxel);
		if (!image.PtInImage(voxel[0], voxel[1], voxel[2]))
			continue;

		for (int k = 0; k < 3; ++k)
		{
			if (voxel[k] < sp[k]) sp[k] = voxel[k];
			if (voxel[k] > ep[k]) ep[k] = voxel[k];
		}
	}
}

void SurfaceUtils::vtkPolyData2Surface(vtkPolyData* polyData, Surface& surface) 
{
	const unsigned int numberOfPoints = polyData->GetNumberOfPoints();

	std::vector<float>& points = surface.GetPoints();
	points.resize(numberOfPoints * 3);
	for (unsigned int i = 0; i < numberOfPoints; ++i)
	{
		const vtkFloatingPointType* point = polyData->GetPoint(i);
		points[i*3] = point[0];
		points[i*3+1] = point[1];
		points[i*3+2] = point[2];
	}

	//
	// Transfer the cells from the vtkPolyData into the itk::Mesh
	//
	vtkCellArray * triangleStrips = polyData->GetStrips();
	vtkIdType  * cellPoints;
	vtkIdType    numberOfCellPoints;

	// First count the total number of triangles from all the triangle strips.
	unsigned int numberOfTriangles = 0;

	triangleStrips->InitTraversal();
	while( triangleStrips->GetNextCell( numberOfCellPoints, cellPoints ) ) {
		numberOfTriangles += numberOfCellPoints-2;
	}

	// then count the regular triangles
	vtkCellArray * polygons = polyData->GetPolys();
	polygons->InitTraversal();
	while( polygons->GetNextCell( numberOfCellPoints, cellPoints ) ) {
		if( numberOfCellPoints == 3 ) {
			numberOfTriangles ++;
		}
	}

	//
	// Reserve memory in the surface for all those triangles
	//
	std::vector< vect3<int> >& faces = surface.GetFaces();
	faces.resize(numberOfTriangles);

	// 
	// Copy the triangles from vtkPolyData into the surface
	//
	int cellId = 0;

	// first copy the triangle strips
	triangleStrips->InitTraversal();
	while( triangleStrips->GetNextCell( numberOfCellPoints, cellPoints ) )  {

		unsigned int numberOfTrianglesInStrip = numberOfCellPoints - 2;
		unsigned long pointIds[3];
		pointIds[0] = cellPoints[0];
		pointIds[1] = cellPoints[1];
		pointIds[2] = cellPoints[2];

		for( unsigned int t=0; t < numberOfTrianglesInStrip; t++ ) {

			faces[cellId][0] = pointIds[0];
			faces[cellId][1] = pointIds[1];
			faces[cellId][2] = pointIds[2];

			cellId++;
			pointIds[0] = pointIds[1];
			pointIds[1] = pointIds[2];
			pointIds[2] = cellPoints[t+3];
		}
	}

	// then copy the normal triangles
	polygons->InitTraversal();
	while( polygons->GetNextCell( numberOfCellPoints, cellPoints ) ) {
		if( numberOfCellPoints !=3 ) // skip any non-triangle.
		{
			continue;
		}

		faces[cellId][0] = cellPoints[0];
		faces[cellId][1] = cellPoints[1];
		faces[cellId][2] = cellPoints[2];

		cellId++;
	}
	// printf("Number of Points: %d, Number of Faces: %d\n", (int)surface.m_ver.size(), (int)surface.m_faces.size());
}

// invoker should be responsible for deleting the polyData
void SurfaceUtils::Surface2vtkPolyData(const Surface& surface, vtkPolyData*& polyData) 
{
	polyData = vtkPolyData::New();

	// Create vtkPoints for insertion into polyData
	vtkPoints *points = vtkPoints::New();

	const std::vector<float>& verts = surface.GetPoints();
	unsigned int nPoints = verts.size() / 3;
	for (unsigned int i = 0; i < nPoints; ++i)
		points->InsertPoint(i, verts[i*3], verts[i*3+1], verts[i*3+2]);

	polyData->SetPoints(points);
	points->Delete();

	// Copy all cells into the vtkPolyData structure
	// Create vtkCellArray into which the cells are copied
	vtkCellArray* triangles = vtkCellArray::New();

	const std::vector< vect3<int> >& faces = surface.GetFaces();
	for (unsigned int i = 0; i < faces.size(); ++i)
	{
		vtkIdList* pts = vtkIdList::New();
		pts->InsertNextId(faces[i][0]);
		pts->InsertNextId(faces[i][1]);
		pts->InsertNextId(faces[i][2]);

		triangles->InsertNextCell(pts);
	}

	polyData->SetPolys(triangles);
	triangles->Delete();
}

void SurfaceUtils::Surface2itkMesh(const Surface& surface, MeshType::Pointer mesh)
{
	// set vertex
	const std::vector<float>& points = surface.GetPoints();
	unsigned int numberOfPoints = points.size() / 3;
	mesh->GetPoints()->Reserve(numberOfPoints);

	for(unsigned int p =0; p < numberOfPoints; ++p) {
		MeshType::PointType pt;
		pt[0] = points[p*3];	
		pt[1] = points[p*3+1];		
		pt[2] = points[p*3+2];
		mesh->SetPoint( p, pt );
	}

	typedef MeshType::CellType   CellType;
	typedef itk::TriangleCell< CellType > TriangleCellType;

	// set faces
	unsigned int numberOfFaces = surface.m_faces.size();
	mesh->GetCells()->Reserve(numberOfFaces);

	const std::vector< vect3<int> >& faces = surface.GetFaces();
	for (unsigned int p = 0; p < numberOfFaces; ++p) {

		unsigned long cell_points[3] = {0};
		cell_points[0] = static_cast<unsigned long>(faces[p][0]);
		cell_points[1] = static_cast<unsigned long>(faces[p][1]);
		cell_points[2] = static_cast<unsigned long>(faces[p][2]);

		MeshType::CellAutoPointer c;
		TriangleCellType* cell = new TriangleCellType;
		cell->SetPointIds(cell_points);
		c.TakeOwnership(cell);
		mesh->SetCell(p, c);
	}
}

void SurfaceUtils::itkMesh2Surface(MeshType::Pointer mesh, Surface& surface)
{
	// set points
	const MeshType::PointsContainer* points = mesh->GetPoints();
	if(points == NULL || points->size() == 0) {
		std::cerr << "empty itk mesh" << std::endl; exit(-1);
	}

	std::vector<float>& verts = surface.GetPoints();
	verts.resize(points->size() * 3);
	for (unsigned int i = 0; i < points->size(); ++i) {
		verts[i*3] = (*points)[i][0];
		verts[i*3+1] = (*points)[i][1];
		verts[i*3+2] = (*points)[i][2];
	}

	// set faces
	const MeshType::CellsContainer* cells = mesh->GetCells();
	if(cells == NULL || cells->size() == 0) {
		std::cerr << "empty itk mesh" << std::endl; exit(-1);
	}
	std::vector< vect3<int> >& faces = surface.GetFaces();
	faces.resize(cells->size());

	for (unsigned int i = 0; i < cells->size(); ++i)
	{
		const unsigned long* cell_points = (*cells)[i]->GetPointIds();
		faces[i][0] = static_cast<int>(cell_points[0]);
		faces[i][1] = static_cast<int>(cell_points[1]);
		faces[i][2] = static_cast<int>(cell_points[2]);
	}
}

template <typename T>
void SurfaceUtils::IsoSurfaceFromImage(const mxImage<T>& image, Surface& surface, T iso_val)
{
	typedef itk::Image<T,3> ImageType;
	typename ImageType::Pointer target_image = ImageType::New();
	ImageHelper::mx2itk<T,T>(image, target_image);

	typedef itk::ImageToVTKImageFilter<ImageType> ConvertFilterType;
	typename ConvertFilterType::Pointer convertFilter = ConvertFilterType::New();
	convertFilter->SetInput(target_image);
	convertFilter->Update();

	// marching cubes
	vtkSmartPointer<vtkMarchingCubes> mc = vtkSmartPointer<vtkMarchingCubes>::New();
	mc->SetInputData(convertFilter->GetOutput());
	mc->SetValue(0, iso_val);
	mc->Update();

	vtkPolyData* polyData = mc->GetOutput();
	vtkPolyData2Surface(polyData, surface);

	// follow-up codes fix the loss of axis orientation in generating surface meshes
	vect3<double> origin = image.GetOrigin();
	vect3<double> spacing = image.GetSpacing();

	std::vector<float>& pts = surface.GetPoints();
	size_t pt_num = pts.size() / 3;
	for (size_t i = 0; i < pt_num; ++i)
	{
		vect3<float> pt( pts[i*3], pts[i*3+1], pts[i*3+2] );
		for (int k = 0; k < 3; ++k)
			pt[k] = (pt[k] - origin[k]) / spacing[k];
		mxImageUtils::Voxel2World(image, pt);
		for (int k = 0; k < 3; ++k)
			pts[i*3+k] = pt[k];
	}
}


template <typename T>
void SurfaceUtils::IsoSurfaceFromImage2(const mxImage<T>& image, double smooth_sigma, double spacing, int isoval, Surface& surf)
{
	ImageLinearInterpolator<T, T> interpolator;
	mxImage<T> isoImage;
	mxImageUtils::ResampleWithInterpolator(image, vect3<double>(spacing, spacing, spacing), interpolator, isoImage);

	mxImage<float> smoothImage;
	mxImageUtils::GaussianSmooth(isoImage, smoothImage, smooth_sigma);

	// bug fixer to fix the problem of open surface in case the ground-truth segmentation touch the image boundary
	mxImage<float> padImage;
	vect3<unsigned int> padImageSize = smoothImage.GetImageSize();
	for (int i = 0; i < 3; ++i)
		padImageSize[i] += 2;

	vect3<double> padImageSpacing = smoothImage.GetSpacing();
	vect3<double> padImageOrigin = smoothImage.GetOrigin();
	for (int i = 0; i < 3; ++i)
		padImageOrigin[i] -= padImageSpacing[i];

	padImage.CopyImageInfo(smoothImage);
	padImage.SetImageSize(padImageSize);
	padImage.SetOrigin(padImageOrigin);
	padImage.Fill(0);

	vect3<unsigned int> smoothImageSize = smoothImage.GetImageSize();
	for (unsigned int z = 0; z < smoothImageSize[2]; ++z)
	{
		for (unsigned int y = 0; y < smoothImageSize[1]; ++y)
		{
			for (unsigned int x = 0; x < smoothImageSize[0]; ++x)
			{
				padImage(x + 1, y + 1, z + 1) = smoothImage(x, y, z);
			}
		}
	}
	// end bug fixer

	SurfaceUtils::IsoSurfaceFromImage(padImage, surf, static_cast<float>(isoval));
}


void SurfaceUtils::SmoothSurface(Surface& surface, unsigned int iterations, double pass_band) 
{
	if (iterations == 0)
		// no smoothing performed
		return;

	if (pass_band > 2 || pass_band < 0)
	{
		std::cerr << "pass_band should be between 0 and 2" << std::endl;
		exit(-1);
	}

	vtkPolyData* polyData;
	Surface2vtkPolyData(surface, polyData);

	//vtkSmartPointer<vtkSmoothPolyDataFilter> smoothFilter = vtkSmartPointer<vtkSmoothPolyDataFilter>::New();
	//#if VTK_MAJOR_VERSION <= 5
	//    smoothFilter->SetInput(polyData);
	//#else 
	//	smoothFilter->SetInputData(polyData);
	//#endif
	//
	//smoothFilter->SetNumberOfIterations(iterations);
	//smoothFilter->Update();

	vtkSmartPointer<vtkWindowedSincPolyDataFilter> smoothFilter = vtkSmartPointer<vtkWindowedSincPolyDataFilter>::New();
#if VTK_MAJOR_VERSION <= 5
	smoothFilter->SetInput(polyData);
#else 
	smoothFilter->SetInputData(polyData);
#endif
	smoothFilter->SetNumberOfIterations(iterations);
	smoothFilter->BoundarySmoothingOff();
	smoothFilter->FeatureEdgeSmoothingOff();
	smoothFilter->NonManifoldSmoothingOff();
	smoothFilter->NormalizeCoordinatesOn();
	smoothFilter->SetPassBand(pass_band);
	smoothFilter->Update();

	vtkPolyData2Surface(smoothFilter->GetOutput(), surface);
	
	if(polyData) {
		polyData->Delete();
		polyData = NULL;
	}
}

template <typename T1, typename T2>
void SurfaceUtils::SmartCarveSurface(const Surface& surface, const mxImage<T1>& refImage, T2 outside_val, T2 inside_val, mxImage<T2>& output_image, vect3<int>& sp, vect3<int>& ep)
{
	vect3<unsigned int> ref_image_size = refImage.GetImageSize();
	BoundingBox(surface, refImage, sp, ep);

	int pad_size = 5;
	for (int i = 0; i < 3; ++i) {
		sp[i] -= pad_size;
		sp[i] = sp[i] < 0 ? 0 : (sp[i] >= ref_image_size[i] ? ref_image_size[i] - 1: sp[i]);

		ep[i] += pad_size;
		ep[i] = ep[i] < 0 ? 0 : (ep[i] >= ref_image_size[i] ? ref_image_size[i] - 1 : ep[i]);
	}

	vect3<double> roi_origin;
	mxImageUtils::Voxel2World(refImage, sp, roi_origin);

	vect3<unsigned int> roi_size;
	for (int i = 0; i < 3; ++i)
		roi_size[i] = ep[i] - sp[i] + 1;

	mxImage<T1> header;
	header.CopyImageInfo(refImage);
	header.SetOrigin(roi_origin);
	header.SetRawImageSize(roi_size);

	CarveSurface(surface, header, outside_val, inside_val, output_image);
}

template <typename T1, typename T2>
void SurfaceUtils::CarveSurface(const Surface& surface, const mxImage<T1>& refImage, T2 outside_val, T2 inside_val, mxImage<T2>& output_image)
{
	MeshType::Pointer mesh = MeshType::New();
	Surface2itkMesh(surface, mesh);

	typedef itk::Image<T1,3> ImageType;
	typedef itk::Image<T2,3> OutImageType;
	typename ImageType::PointType origin;
	typename ImageType::SpacingType spacing;
	typename ImageType::IndexType idx;
	typename ImageType::SizeType size;
	typename ImageType::DirectionType direction;

	vect3<double> ref_spacing = refImage.GetSpacing();
	vect3<double> ref_origin = refImage.GetOrigin();
	vect3<unsigned int> ref_size = refImage.GetImageSize();

	// add pad size to address the unsolved bug in ITK TriangleMeshToBinaryImageFilter
	// in the situation when the mesh is outside the image.
	/* bug fixer */
	int pad_size = 5;
	vect3<int> tmp_voxel_origin(-pad_size, -pad_size, -pad_size);
	mxImageUtils::Voxel2World(refImage, tmp_voxel_origin, ref_origin);
	for (int i = 0; i < 3; ++i)
		ref_size[i] = ref_size[i] + pad_size * 2;
	/* bug fixer */

	for (int i = 0; i < 3; ++i)
	{
		spacing[i] = ref_spacing[i];
		idx[i] = 0;

		origin[i] = ref_origin[i];
		size[i] = ref_size[i];
	}

	const double* axis = refImage.GetAxis();
	for (unsigned int col = 0; col < 3; ++col) {
		for (unsigned int row = 0; row < 3; ++row) {
			direction[row][col] = axis[col*3+row];
		}
	}

	typedef itk::TriangleMeshToBinaryImageFilter<MeshType, OutImageType> Mesh2ImageFilterType;
	typename Mesh2ImageFilterType::Pointer mesh2ImageFilter = Mesh2ImageFilterType::New();
	mesh2ImageFilter->SetInput(mesh);
	mesh2ImageFilter->SetTolerance(0.01);
	mesh2ImageFilter->SetOrigin(origin);
	mesh2ImageFilter->SetSpacing(spacing);
	mesh2ImageFilter->SetIndex(idx);
	mesh2ImageFilter->SetSize(size);
	mesh2ImageFilter->SetDirection(direction);
	mesh2ImageFilter->SetInsideValue(inside_val);
	mesh2ImageFilter->SetOutsideValue(outside_val);

	try {
		mesh2ImageFilter->Update();
	} catch(itk::ExceptionObject& exp) {
		std::cerr << exp.GetDescription() << std::endl;
		exit(-1);
	}

	mxImage<T2> output_image_padded;
	ImageHelper::itk2mx<T2,T2>(mesh2ImageFilter->GetOutput(), output_image_padded);

	/* bug fixer */
	vect3<int> sp(pad_size, pad_size, pad_size), ep;
	for (int i = 0; i < 3; ++i)
		ep[i] = sp[i] + refImage.GetImageSize()[i] - 1;

	mxImageUtils::Crop2(output_image_padded, sp, ep, output_image, static_cast<T2>(0));
	/* bug fixer */
}


template <typename T1, typename T2>
void SurfaceUtils::FastCarveSurface(const Surface& surface, const mxImage<T1>& refImage, T2 outside_val, T2 inside_val, const vect3<int>& voxel_pad, mxImage<T2>& output_image)
{
	vect3<int> sp, ep;
	BoundingBox(surface, refImage, sp, ep);

	for (int i = 0; i < 3; ++i)
	{
		sp[i] -= voxel_pad[i];
		ep[i] += voxel_pad[i];
	}

	mxImage<T1> cropped_refImage;
	mxImageUtils::GetCropHeader2(refImage, sp, ep, cropped_refImage);

	CarveSurface(surface, cropped_refImage, outside_val, inside_val, output_image);
}

unsigned int SurfaceUtils::GetVertexNeighborhood(const Surface& surface, unsigned int vertIdx, unsigned int nbSize, std::vector<unsigned int>& nbVertIdxs, std::vector<unsigned int>& nbVertLevels)
{
	const std::vector<float>& verts = surface.GetPoints();
	const std::vector< std::vector<int> >& nbrs = surface.GetNeighborhood();
	unsigned int vertnum = verts.size() / 3;

	if(nbrs.size() == 0) { std::cerr << "empty neighborhood, please update the neighborhood first" << std::endl; exit(-1); }
	if(vertIdx > vertnum) {  std::cerr << "input vertex out of index" << std::endl; exit(-1);  }

	nbVertIdxs.clear();
	nbVertLevels.clear();
	std::set<int> tags;

	// tag the central vertex
	nbVertIdxs.push_back(vertIdx);
	nbVertLevels.push_back(0);
	tags.insert(vertIdx);

	unsigned int current_size = 0;
	std::queue<int> vert_queue;
	vert_queue.push(vertIdx);

	// pop one level by one level
	while ( current_size < nbSize && vert_queue.size() > 0 )
	{
		// pop one level and mark its neighbors
		unsigned int current_vert_num = vert_queue.size();
		for (unsigned int i = 0; i < current_vert_num; ++i)
		{
			unsigned int tmp_vert_idx = vert_queue.front();
			vert_queue.pop();
			// push into the next level vertices
			for (unsigned int nb_idx = 0; nb_idx < nbrs[tmp_vert_idx].size(); ++ nb_idx) {
				unsigned int nb_vert_idx = nbrs[tmp_vert_idx][nb_idx];
				if(tags.count(nb_vert_idx) == 0) {
					vert_queue.push(nb_vert_idx);
					tags.insert(nb_vert_idx);
					nbVertIdxs.push_back(nb_vert_idx);
					nbVertLevels.push_back(current_size + 1);
				}
			}
		}
		++ current_size;
	}
	return current_size;
}


void SurfaceUtils::AddSubsurface(Surface& surf, const Surface& sub_surf)
{
	std::vector<float>& verts = surf.GetPoints();
	std::vector< vect3<int> >& faces = surf.GetFaces();

	const std::vector<float>& sub_verts = sub_surf.GetPoints();
	const std::vector< vect3<int> >& sub_faces = sub_surf.GetFaces();

	int old_vert_size = verts.size() / 3;
	int old_face_size = faces.size();

	// copy verts
	verts.resize( verts.size() + sub_verts.size() );
	::memcpy( &(verts[0]) + old_vert_size * 3, &(sub_verts[0]), sizeof(float) * sub_verts.size() );

	// copy faces
	faces.resize( faces.size() + sub_faces.size() );
	for (size_t i = 0; i < sub_faces.size(); ++i)
	{
		vect3<int> face = sub_faces[i];
		for (int j = 0; j < 3; ++j)
			faces[old_face_size + i][j] = face[j] + old_vert_size;
	}
}


void SurfaceUtils::DilateShrinkSurface(Surface& surf, float size)
{
	surf.UpdateNormals();

	std::vector<float>& verts = surf.GetPoints();
	const std::vector< vect3<float> >& normals = surf.GetNormals();

	size_t num_verts = verts.size() / 3;

	for (size_t i = 0; i < num_verts; ++i)
	{
		vect3<float> vert( verts[i*3], verts[i*3+1], verts[i*3+2] );
		vect3<float> normal = normals[i];

		vert = vert + normal * size;

		for (int j = 0; j < 3; ++j)
			verts[i * 3 + j] = vert[j];
	}
}


void SurfaceUtils::FlipX(Surface& surf)
{
	std::vector<float>& verts = surf.GetPoints();
	size_t num_vert = verts.size() / 3;

	for (size_t i = 0; i < num_vert; ++i)
		verts[i * 3] = -verts[i * 3];
}


void SurfaceUtils::FlipY(Surface& surf)
{
	std::vector<float>& verts = surf.GetPoints();
	size_t num_vert = verts.size() / 3;

	for (size_t i = 0; i < num_vert; ++i)
		verts[i * 3 + 1] = -verts[i * 3 + 1];
}


void SurfaceUtils::FlipZ(Surface& surf)
{
	std::vector<float>& verts = surf.GetPoints();
	size_t num_vert = verts.size() / 3;

	for (size_t i = 0; i < num_vert; ++i)
		verts[i * 3 + 2] = -verts[i * 3 + 2];
}


void SurfaceUtils::Remesh(Surface& surf, float stepsize, int iter_num)
{
	for (int i = 0; i < iter_num; ++i)
	{
		surf.UpdateFaceNeighbors();
		surf.UpdateNormals();

		const std::vector< std::vector<int> >& faceNbrs = surf.GetFaceNeighborhood();
		std::vector<float>& pts = surf.GetPoints();
		std::vector< vect3<float> >& normals = surf.GetNormals();
		int vert_num = pts.size() / 3;

		std::vector<float> out_pts(pts.size(), 0);

		#pragma omp parallel for
		for (int vert_idx = 0; vert_idx < vert_num; ++vert_idx)
		{
			vect3<float> area_weighted_center = surf.GetNeighborFaceCenter(vert_idx);

			vect3<float> current_pos(pts[vert_idx * 3], pts[vert_idx * 3 + 1], pts[vert_idx * 3 + 2]);
			vect3<float> force = (area_weighted_center - current_pos) * stepsize;
			float tmp = vect3<float>::dot_product(force, normals[vert_idx]);
			force = force - normals[vert_idx] * tmp;

			for (int j = 0; j < 3; ++j)
				out_pts[vert_idx * 3 + j] = current_pos[j] + force[j];
		}

		// copy back
		::memcpy(&pts[0], &out_pts[0], sizeof(float) * out_pts.size());
	}

}


} } }

#endif


