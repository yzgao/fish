//
//  CompositeFeatureSpace.h
//  FISH
//
//  Created by Yaozong Gao on 9/23/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __CompositeFeatureSpace_h__
#define __CompositeFeatureSpace_h__

#include "feature/IFeatureSpace.h"
#include <iostream>

namespace BRIC { namespace IDEA { namespace FISH {

// forward declaration
class CompositeFeatureSpace;

// composite patch feature functor
class CompositePatchFeatureFunctor: public IPatchFeatureFunctor
{
public:
	typedef CompositeFeatureSpace FeatureSpaceType;

	CompositePatchFeatureFunctor(const CompositeFeatureSpace& compfs);
	~CompositePatchFeatureFunctor();
	double operator() (unsigned int featureIndex) const;

	// overrides
	virtual void SetPatch(const mxImage<double>& patch, int index);
	virtual double GetResponse(unsigned int featureIndex) const;
	virtual void PreprocessInput(mxImage<double>& patch, int index) const;

private:
	const CompositeFeatureSpace* m_compfs;
	std::vector<int> m_featureCounts;
	std::vector<const mxImage<double>*> m_patches;
};

// composite image feature functor
class CompositeImageFeatureFunctor: public IImageFeatureFunctor
{
public:
	typedef CompositeFeatureSpace FeatureSpaceType;

	CompositeImageFeatureFunctor() {}
	CompositeImageFeatureFunctor(const CompositeFeatureSpace& compfs);
	~CompositeImageFeatureFunctor();
	double operator() (unsigned int featureIndex) const;

	// overrides
	virtual void SetImage(const mxImage<double>& image, int index);
	virtual void SetVoxel(const vect3<unsigned int>& pos, int index);
	virtual double GetResponse(unsigned int featureIndex) const;
	virtual void PreprocessInput(mxImage<double>& image, int index) const;

private:
	const CompositeFeatureSpace* m_compfs;
	std::vector<int> m_featureCounts; 
	std::vector<const mxImage<double>*> m_images;
    std::vector< vect3<unsigned int> > m_poss;
};

// composite image feature functor with normalization
class NormalizationCompositeImageFeatureFunctor : public IImageFeatureFunctor
{
public:
	typedef CompositeFeatureSpace FeatureSpaceType;

	NormalizationCompositeImageFeatureFunctor() {}
	NormalizationCompositeImageFeatureFunctor(const CompositeFeatureSpace& compfs);
	~NormalizationCompositeImageFeatureFunctor();
	double operator() (unsigned int featureIndex) const;

	void EnableNormalization(int index);
	void SetNormalizationParameters(int index, double slope, double interp);

	// overrides
	virtual void SetImage(const mxImage<double>& image, int index);
	virtual void SetVoxel(const vect3<unsigned int>& pos, int index);
	virtual double GetResponse(unsigned int featureIndex) const;
	virtual void PreprocessInput(mxImage<double>& image, int index) const;

private:
	const CompositeFeatureSpace* m_compfs;
	std::vector<int> m_featureCounts;
	std::vector<const mxImage<double>*> m_images;
	std::vector< vect3<unsigned int> > m_poss;

	std::vector<bool> m_isNormalizationEnabled;
	std::vector< std::pair<double,double> > m_normalizationParams;
};

// hybrid feature functor 
class HybridImageFeatureFunctor
{
public:
	typedef CompositeFeatureSpace FeatureSpaceType;

	HybridImageFeatureFunctor() {}
	HybridImageFeatureFunctor(const CompositeFeatureSpace& fs, const std::vector<FunctorType>& types);
	~HybridImageFeatureFunctor();

	void SetImage(const mxImage<double>& image, int index);
	void SetVoxel(const vect3<unsigned int>& pos, int index);
	void PreprocessInput(mxImage<double>& image, int index) const;
	double operator() (unsigned int featureIndex);

private:

	std::vector<void *> m_functors;
	std::vector<FunctorType> m_types;	// 0 for patch functor, and 1 for image functor
	std::vector< std::pair<int,int> > m_feature2functor;
};

// composite feature space
class CompositeFeatureSpace: public IFeatureSpace
{
private:
	CompositeFeatureSpace(const CompositeFeatureSpace& copy);
	CompositeFeatureSpace& operator=(const CompositeFeatureSpace& rhs);

public:
	typedef CompositeImageFeatureFunctor ImageFunctorType;
	typedef CompositePatchFeatureFunctor PatchFunctorType;

	CompositeFeatureSpace();
	~CompositeFeatureSpace();

	// used to create CompositeFeatureSpace
	void AddFeatureSpace(std::auto_ptr<IFeatureSpace> fs);
	void SetWeights(const double* weights, unsigned int num);
	IFeatureSpace* GetFeatureSpace(int index) const;
	void DistrubuteFeatures(unsigned int numFeatures, std::vector<unsigned int>& featureBins) const;
	void clear();

	// overrides
	virtual void RandomizeFeatureSpace(Random& random, unsigned int numFeatures);
	virtual CompositePatchFeatureFunctor* CreatePatchFeatureFunctor() const;
	virtual CompositeImageFeatureFunctor* CreateImageFeatureFunctor() const;
	HybridImageFeatureFunctor* CreateHybridFeatureFunctor(const std::vector<FunctorType>& types) const;
	virtual vect3<unsigned int> GetPatchSize(int index) const;
	virtual int GetFeatureNumber() const;
	virtual int GetNumberFeatureSpaces() const;
	virtual void Serialize(std::ostream& out) const;
	static CompositeFeatureSpace* Deserialize(std::istream& in);


	NormalizationCompositeImageFeatureFunctor* CreateNormalizationImageFeatureFunctor() const;

private:
	std::vector<IFeatureSpace*> m_fss;
	std::vector<double> m_weights;

	static const char* m_binaryHeader;
	FS_REGISTER_DEC_TYPE(CompositeFeatureSpace)
};


//////////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////////

// begin the definition of CompositePatchFeatureFunctor
CompositePatchFeatureFunctor::CompositePatchFeatureFunctor(const CompositeFeatureSpace& compfs)
{
	m_compfs = &compfs;

	m_featureCounts.resize(compfs.GetNumberFeatureSpaces());
	m_patches.resize(compfs.GetNumberFeatureSpaces());

	for(unsigned int i = 0; i < m_featureCounts.size(); ++i)
	{
		if(i == 0)
			m_featureCounts[i] = compfs.GetFeatureSpace(i)->GetFeatureNumber();
		else
			m_featureCounts[i] = m_featureCounts[i-1] + compfs.GetFeatureSpace(i)->GetFeatureNumber();

		m_patches[i] = NULL;
	}
}

CompositePatchFeatureFunctor::~CompositePatchFeatureFunctor()
{
	m_compfs = NULL;
	m_featureCounts.clear();
	m_patches.clear();
}

void CompositePatchFeatureFunctor::SetPatch(const mxImage<double>& patch, int index)
{
	if(index < 0 || index >= m_compfs->GetNumberFeatureSpaces()) {
		std::cerr << "index out of boundary" << std::endl; exit(-1);
	}
	m_patches[index] = &patch;
}

double CompositePatchFeatureFunctor::GetResponse(unsigned int featureIndex) const 
{
	int spaceIndex = 0;
	for(int i = 0; i < static_cast<int>(m_featureCounts.size()); ++i)
	{
		if(static_cast<int>(featureIndex) < m_featureCounts[i]) {
			spaceIndex = i;
			break;
		}
	}

	if(spaceIndex > 0)
		featureIndex = static_cast<unsigned int>(featureIndex - m_featureCounts[spaceIndex-1]);

	std::auto_ptr<IPatchFeatureFunctor> functor( m_compfs->GetFeatureSpace(spaceIndex)->CreatePatchFeatureFunctor() );
	functor->SetPatch(*m_patches[spaceIndex],0);
	return functor->GetResponse(featureIndex);
}

void CompositePatchFeatureFunctor::PreprocessInput(mxImage<double>& patch, int index) const
{
	std::auto_ptr<IPatchFeatureFunctor> functor(m_compfs->GetFeatureSpace(index)->CreatePatchFeatureFunctor());
	functor->PreprocessInput(patch,0);
}

double CompositePatchFeatureFunctor::operator () (unsigned int featureIndex) const 
{
	return GetResponse(featureIndex);
}
// end the definition of CompositePatchFeatureFunctor


// begin the definition of CompositeImageFeatureFunctor
CompositeImageFeatureFunctor::CompositeImageFeatureFunctor(const CompositeFeatureSpace& compfs)
{
	m_compfs = &compfs;

	m_featureCounts.resize(compfs.GetNumberFeatureSpaces());
	m_images.resize(compfs.GetNumberFeatureSpaces());
    m_poss.resize(compfs.GetNumberFeatureSpaces());

	for(unsigned int i = 0; i < m_featureCounts.size(); ++i)
	{
		if(i == 0)
			m_featureCounts[i] = compfs.GetFeatureSpace(i)->GetFeatureNumber();
		else
			m_featureCounts[i] = m_featureCounts[i-1] + compfs.GetFeatureSpace(i)->GetFeatureNumber();

		m_images[i] = NULL;
	}
}

CompositeImageFeatureFunctor::~CompositeImageFeatureFunctor()
{
	m_compfs = NULL;
	m_featureCounts.clear();
	m_images.clear();
	m_poss.clear();
}

void CompositeImageFeatureFunctor::SetImage(const mxImage<double>& image, int index)
{
	if(index < 0 || index >= m_compfs->GetNumberFeatureSpaces()) {
		std::cerr << "index out of boundary" << std::endl; exit(-1);
	}
	m_images[index] = &image;
}

void CompositeImageFeatureFunctor::SetVoxel(const vect3<unsigned int>& pos, int index)
{
    if(index < 0 || index >= m_compfs->GetNumberFeatureSpaces()) {
		std::cerr << "index out of boundary" << std::endl; exit(-1);
	}
	m_poss[index] = pos;
}

double CompositeImageFeatureFunctor::GetResponse(unsigned int featureIndex) const 
{
	int spaceIndex = 0;
	for (int i = 0; i < static_cast<int>(m_featureCounts.size()); ++i)
	{
		if (static_cast<int>(featureIndex) < m_featureCounts[i]) {
			spaceIndex = i;
			break;
		}
	}

	int localIndex = featureIndex;
	if (spaceIndex > 0)
		localIndex = static_cast<unsigned int>(featureIndex - m_featureCounts[spaceIndex - 1]);

	std::auto_ptr<IImageFeatureFunctor> functor( m_compfs->GetFeatureSpace(spaceIndex)->CreateImageFeatureFunctor() );
	functor->SetImage(*m_images[spaceIndex],0);
	functor->SetVoxel(m_poss[spaceIndex],0);
	return functor->GetResponse(localIndex);
}

void CompositeImageFeatureFunctor::PreprocessInput(mxImage<double>& image, int index) const
{
	std::auto_ptr<IImageFeatureFunctor> functor( m_compfs->GetFeatureSpace(index)->CreateImageFeatureFunctor() );
	functor->PreprocessInput(image,0);
}

double CompositeImageFeatureFunctor::operator() (unsigned int featureIndex) const 
{
	return GetResponse(featureIndex);
}
// end the definition of CompositeImageFeatureFunctor


// begin the definition of NormalizationCompositeImageFeatureFunctor
NormalizationCompositeImageFeatureFunctor::NormalizationCompositeImageFeatureFunctor(const CompositeFeatureSpace& compfs)
{
	m_compfs = &compfs;

	m_featureCounts.resize(compfs.GetNumberFeatureSpaces());
	m_images.resize(compfs.GetNumberFeatureSpaces());
	m_poss.resize(compfs.GetNumberFeatureSpaces());
	m_isNormalizationEnabled.resize(compfs.GetNumberFeatureSpaces());
	m_normalizationParams.resize(compfs.GetNumberFeatureSpaces());

	for (unsigned int i = 0; i < m_featureCounts.size(); ++i)
	{
		if (i == 0)
			m_featureCounts[i] = compfs.GetFeatureSpace(i)->GetFeatureNumber();
		else
			m_featureCounts[i] = m_featureCounts[i - 1] + compfs.GetFeatureSpace(i)->GetFeatureNumber();

		m_images[i] = NULL;

		m_isNormalizationEnabled[i] = false;
		m_normalizationParams[i] = std::make_pair(1.0, 0.0);
	}
}

NormalizationCompositeImageFeatureFunctor::~NormalizationCompositeImageFeatureFunctor()
{
	m_compfs = NULL;
	m_featureCounts.clear();
	m_images.clear();
	m_poss.clear();
	m_isNormalizationEnabled.clear();
	m_normalizationParams.clear();
}

void NormalizationCompositeImageFeatureFunctor::SetImage(const mxImage<double>& image, int index)
{
	if (index < 0 || index >= m_compfs->GetNumberFeatureSpaces()) {
		std::cerr << "index out of boundary" << std::endl; exit(-1);
	}
	m_images[index] = &image;
}

void NormalizationCompositeImageFeatureFunctor::SetVoxel(const vect3<unsigned int>& pos, int index)
{
	if (index < 0 || index >= m_compfs->GetNumberFeatureSpaces()) {
		std::cerr << "index out of boundary" << std::endl; exit(-1);
	}
	m_poss[index] = pos;
}

double NormalizationCompositeImageFeatureFunctor::GetResponse(unsigned int featureIndex) const
{
	int spaceIndex = 0;
	for (int i = 0; i < static_cast<int>(m_featureCounts.size()); ++i)
	{
		if (static_cast<int>(featureIndex) < m_featureCounts[i]) {
			spaceIndex = i;
			break;
		}
	}

	if (spaceIndex > 0)
		featureIndex = static_cast<unsigned int>(featureIndex - m_featureCounts[spaceIndex - 1]);

	std::auto_ptr<IImageFeatureFunctor> functor(m_compfs->GetFeatureSpace(spaceIndex)->CreateImageFeatureFunctor());
	functor->SetImage(*m_images[spaceIndex], 0);
	functor->SetVoxel(m_poss[spaceIndex], 0);
	double ret = functor->GetResponse(featureIndex);

	if (!m_isNormalizationEnabled[spaceIndex])
		return ret;
	else
	{
		std::pair<double, double> params = m_normalizationParams[spaceIndex];
		return params.first * ret + params.second;
	}
}

void NormalizationCompositeImageFeatureFunctor::PreprocessInput(mxImage<double>& image, int index) const
{
	std::auto_ptr<IImageFeatureFunctor> functor(m_compfs->GetFeatureSpace(index)->CreateImageFeatureFunctor());
	functor->PreprocessInput(image, 0);
}

double NormalizationCompositeImageFeatureFunctor::operator() (unsigned int featureIndex) const
{
	return GetResponse(featureIndex);
}

void NormalizationCompositeImageFeatureFunctor::EnableNormalization(int index)
{
	m_isNormalizationEnabled[index] = true;
}

void NormalizationCompositeImageFeatureFunctor::SetNormalizationParameters(int index, double slope, double interp)
{
	m_normalizationParams[index] = std::make_pair(slope, interp);
}

// end the definition of NormalizationCompositeImageFeatureFunctor



// begin the definition of HybridImageFeatureFunctor
HybridImageFeatureFunctor::HybridImageFeatureFunctor(const CompositeFeatureSpace& fs, const std::vector<FunctorType>& types)
{
	if(fs.GetNumberFeatureSpaces() != static_cast<int>(types.size())) {
		std::cerr << "number of functor types != feature space number" << std::endl;
		exit(-1);
	}

	m_types.resize(types.size());
	for (unsigned int i = 0; i < types.size(); ++i) 
		m_types[i] = types[i];
	
	std::vector<int> featureCounts;
	featureCounts.resize(fs.GetNumberFeatureSpaces());
	int totalFeatureNumber = 0;
	for (unsigned int i = 0; i < featureCounts.size(); ++i) {
		featureCounts[i] = fs.GetFeatureSpace(i)->GetFeatureNumber();
		totalFeatureNumber += featureCounts[i];
	}

	m_feature2functor.resize(totalFeatureNumber);
	int pointer = 0;
	for (unsigned int i = 0; i < featureCounts.size(); ++i)
	{
		for (int j = 0; j < featureCounts[i]; ++j)
			m_feature2functor[pointer+j] = std::make_pair(i,j);
		pointer = pointer + featureCounts[i];
	}

	m_functors.resize(types.size());
	for (unsigned int i = 0; i < m_types.size(); ++i)
	{
		if (types[i] == PatchFunctorType) 
			m_functors[i] = fs.GetFeatureSpace(i)->CreatePatchFeatureFunctor();
		else if(types[i] == ImageFunctorType) 
			m_functors[i] = fs.GetFeatureSpace(i)->CreateImageFeatureFunctor();
		else {
			std::cerr << "fail to recognize the functor type" << std::endl;
			exit(-1);
		}
	}
}

HybridImageFeatureFunctor::~HybridImageFeatureFunctor()
{
	for (unsigned int i = 0; i < m_functors.size(); ++i) {
		switch (m_types[i])
		{
		case ImageFunctorType: delete static_cast<IImageFeatureFunctor*>(m_functors[i]); break;
		case PatchFunctorType: delete static_cast<IPatchFeatureFunctor*>(m_functors[i]); break;
		default:
			break;
		}
		m_functors[i] = NULL;
	}

	m_types.clear();
	m_feature2functor.clear();
}

double HybridImageFeatureFunctor::operator() (unsigned int featureIndex)
{
	int functor_idx = m_feature2functor[featureIndex].first;
	int feature_idx = m_feature2functor[featureIndex].second;

	double ret = 0;
	switch (m_types[functor_idx])
	{
	case PatchFunctorType: ret = (*static_cast<IPatchFeatureFunctor*>(m_functors[functor_idx]))(feature_idx); break;
	case ImageFunctorType: ret = (*static_cast<IImageFeatureFunctor*>(m_functors[functor_idx]))(feature_idx); break;
	default:
		std::cerr << "unknown functor type" << std::endl;
		exit(-1);
		break;
	}

	return ret;
}

void HybridImageFeatureFunctor::SetImage(const mxImage<double>& image, int index)
{
	switch(m_types[index]) {
	case PatchFunctorType: static_cast<IPatchFeatureFunctor*>(m_functors[index])->SetPatch(image,0); break;
	case ImageFunctorType: static_cast<IImageFeatureFunctor*>(m_functors[index])->SetImage(image,0); break;
	default:
		std::cerr << "unrecognized functor type" << std::endl;
		exit(-1);
		break;
	}
}

void HybridImageFeatureFunctor::SetVoxel(const vect3<unsigned int>& pos, int index)
{
	if (m_types[index] != ImageFunctorType) {
		std::cerr << "SetVoxel is only valid for ImageFunctorType" << std::endl; exit(-1);
	}
	static_cast<IImageFeatureFunctor*>(m_functors[index])->SetVoxel(pos,0);
}

void HybridImageFeatureFunctor::PreprocessInput(mxImage<double>& image, int index) const
{
	switch (m_types[index])
	{
	case PatchFunctorType: static_cast<IPatchFeatureFunctor*>(m_functors[index])->PreprocessInput(image,0); break;
	case ImageFunctorType: static_cast<IImageFeatureFunctor*>(m_functors[index])->PreprocessInput(image,0); break;
	default:
		std::cerr << "unrecognized functor type" << std::endl;
		exit(-1);
		break;
	}
}

// end the definition of HybridImageFeatureFunctor


// begin the definition of CompositeFeatureSpace
CompositeFeatureSpace::CompositeFeatureSpace() {}

CompositeFeatureSpace::~CompositeFeatureSpace()
{
	clear();
}

void CompositeFeatureSpace::AddFeatureSpace(std::auto_ptr<IFeatureSpace> fs)
{
	m_fss.push_back(fs.release());
}

void CompositeFeatureSpace::SetWeights(const double* weights, unsigned int num)
{
	double weight_sum = 0;
	for (int i = 0; i < static_cast<int>(num); ++i)
		weight_sum += weights[i];

	if(std::abs(weight_sum) < 1e-6) {
		std::cerr << "weight_sum approximates 0" << std::endl; exit(-1);
	}

	m_weights.resize(num);
	for (unsigned int i = 0; i < num; ++i)
		m_weights[i] = weights[i] / weight_sum;
}

IFeatureSpace* CompositeFeatureSpace::GetFeatureSpace(int index) const
{
	return m_fss[index];
}

int CompositeFeatureSpace::GetNumberFeatureSpaces() const
{
	return static_cast<int>(m_fss.size());
}

void CompositeFeatureSpace::clear()
{
	for (unsigned int i = 0; i < m_fss.size(); ++i) {
		if(m_fss[i] != NULL)
			delete m_fss[i];
	}
	m_fss.clear();
	m_weights.clear();
}

void CompositeFeatureSpace::DistrubuteFeatures(unsigned int numFeatures, std::vector<unsigned int>& num_array) const
{
	if (m_weights.size() != m_fss.size() || m_weights.size() == 0) {
		std::cerr << "unequal size of weights and feature spaces | empty weights" << std::endl;
		exit(-1);
	}
	num_array.resize(m_fss.size());

	unsigned int count = 0;
	for (unsigned int i = 0; i < m_fss.size(); ++i)
	{
		if (i != m_fss.size() - 1)
		{
			num_array[i] = static_cast<unsigned int>(m_weights[i] * numFeatures);
			count += num_array[i];
		}
		else {
			num_array[i] = numFeatures - count;
		}
	}
}

void CompositeFeatureSpace::RandomizeFeatureSpace(Random& random, unsigned int numFeatures)
{
	if(m_weights.size() != m_fss.size() || m_weights.size() == 0) {
		std::cerr << "unequal size of weights and feature spaces | empty weights" << std::endl;
		exit(-1);
	}
	std::vector<unsigned int> num_array;
	num_array.resize(m_fss.size());

	unsigned int count = 0;
	for (unsigned int i = 0; i < m_fss.size(); ++i)
	{
		if(i != m_fss.size() - 1)
		{
			num_array[i] = static_cast<unsigned int>(m_weights[i] * numFeatures);
			count += num_array[i];
		}else {
			num_array[i] = numFeatures - count;
		}
	}

	for (unsigned int i = 0; i < m_fss.size(); ++i)
		m_fss[i]->RandomizeFeatureSpace(random, num_array[i]);
}

CompositePatchFeatureFunctor* CompositeFeatureSpace::CreatePatchFeatureFunctor() const 
{
	return new CompositePatchFeatureFunctor(*this);
}

CompositeImageFeatureFunctor* CompositeFeatureSpace::CreateImageFeatureFunctor() const
{
	return new CompositeImageFeatureFunctor(*this);
}

NormalizationCompositeImageFeatureFunctor* CompositeFeatureSpace::CreateNormalizationImageFeatureFunctor() const
{
	return new NormalizationCompositeImageFeatureFunctor(*this);
}

HybridImageFeatureFunctor* CompositeFeatureSpace::CreateHybridFeatureFunctor(const std::vector<FunctorType>& types) const
{
	return new HybridImageFeatureFunctor(*this, types);
}

vect3<unsigned int> CompositeFeatureSpace::GetPatchSize(int index) const
{
	return m_fss[index]->GetPatchSize(0);
}

int CompositeFeatureSpace::GetFeatureNumber() const
{
	unsigned int total = 0;
	for (unsigned int i = 0; i < m_fss.size(); ++i)
		total += m_fss[i]->GetFeatureNumber();
	return static_cast<int>(total);
}

void CompositeFeatureSpace::Serialize(std::ostream& out) const
{
	char header[BinaryHeaderSize] = {0};
    strncpy(header, m_binaryHeader, strlen(m_binaryHeader)+1);
	out.write(header, BinaryHeaderSize);
	
	unsigned int fss_num = static_cast<unsigned int>(m_fss.size());
	out.write((const char*)(&fss_num), sizeof(unsigned int));
	out.write((const char*)(&m_weights[0]), sizeof(double)*fss_num);
	
	for (unsigned int i = 0; i < fss_num; ++i)
		m_fss[i]->Serialize(out);

	out.flush();
}

CompositeFeatureSpace* CompositeFeatureSpace::Deserialize(std::istream& in)
{
	char header[BinaryHeaderSize] = {0};
	in.read(header, BinaryHeaderSize);
	if (strncmp(header, m_binaryHeader, strlen(m_binaryHeader)) != 0) {
		std::cerr << "binary header incorrect (Haar3DFeatureSpace)" << std::endl; exit(-1);
	}

	CompositeFeatureSpace* ret = new CompositeFeatureSpace;
	unsigned int fss_num = 0;
	in.read((char*)(&fss_num), sizeof(unsigned int));

	ret->m_weights.resize(fss_num);
	in.read((char*)(&ret->m_weights[0]), sizeof(double)*fss_num);

	ret->m_fss.resize(fss_num);
	for (unsigned int i = 0; i < fss_num; ++i) 
		ret->m_fss[i] = FeatureSpaceFactory::Deserialize(in);

	return ret;
}

const char* CompositeFeatureSpace::m_binaryHeader = "BRIC.IDEA.FISH.CompositeFeatureSpace";

FS_REGISTER_DEF_TYPE(CompositeFeatureSpace,BRIC.IDEA.FISH.CompositeFeatureSpace)
// end the definition of CompositeFeatureSpace


} } }


#endif

