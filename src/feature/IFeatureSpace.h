//
//  IFeatureSpace.h
//  FISH
//
//  Created by Yaozong Gao on 9/23/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __IFeatureSpace_h__
#define __IFeatureSpace_h__

#include <iostream>
#include "common/Random.h"
#include "common/mxImage.h"

namespace BRIC { namespace IDEA { namespace FISH {


enum FunctorType {
	PatchFunctorType,
	ImageFunctorType
};

// forward declaration
class IFeatureSpace;

class IFeatureFunctor
{
public:
	virtual double GetResponse(unsigned int featureIndex) const = 0;
};


class IPatchFeatureFunctor: public IFeatureFunctor
{
public:
	typedef IFeatureSpace FeatureSpaceType;

	virtual ~IPatchFeatureFunctor() {}

	virtual void SetPatch(const mxImage<double>& patch, int index) = 0;
	virtual void PreprocessInput(mxImage<double>& patch, int index) const = 0;

	double operator()(unsigned int featureIndex) const { return this->GetResponse(featureIndex); }
};

class IImageFeatureFunctor: public IFeatureFunctor
{
public:
	typedef IFeatureSpace FeatureSpaceType;

	virtual ~IImageFeatureFunctor() {}

	virtual void SetImage(const mxImage<double>& image, int index) = 0;
	virtual void SetVoxel(const vect3<unsigned int>& pos, int index) = 0;
	virtual void PreprocessInput(mxImage<double>& image, int index) const = 0;

	double operator()(unsigned int featureIndex) const { return this->GetResponse(featureIndex); }
};


class IFeatureSpace
{
public:
	typedef IPatchFeatureFunctor PatchFunctorType;
	typedef IImageFeatureFunctor ImageFunctorType;

	virtual ~IFeatureSpace() {}

	virtual void RandomizeFeatureSpace(Random& random, unsigned int numFeatures) = 0;
	virtual IPatchFeatureFunctor* CreatePatchFeatureFunctor() const = 0;	// caller is responsible for memory release
	virtual IImageFeatureFunctor* CreateImageFeatureFunctor() const = 0;	// caller is responsible for memory release
	virtual vect3<unsigned int> GetPatchSize(int index) const = 0;
	virtual int GetFeatureNumber() const = 0;
	virtual int GetNumberFeatureSpaces() const = 0;
	virtual void Serialize(std::ostream& out) const = 0;
	static IFeatureSpace* Deserialize(std::istream& in);

	static const int BinaryHeaderSize = 1024;
};


// factory for loading feature space from disk
class FeatureSpaceFactory
{
public:
	typedef std::map<std::string, boost::function1<IFeatureSpace*,std::istream&> > FunMapType;
	static IFeatureSpace* Deserialize(std::istream& in);		// caller is responsible for memory release
protected:
	static FunMapType deserializeFunMap;
};

template <class T>
class FeatureSpaceRegister: public FeatureSpaceFactory
{
public:
	FeatureSpaceRegister(const std::string& tag);
};

// macro for easy declaration and definition
#define FS_REGISTER_DEC_TYPE(NAME) \
	static FeatureSpaceRegister<NAME> reg;
#define FS_REGISTER_DEF_TYPE(NAME,TAG) \
	FeatureSpaceRegister<NAME> NAME::reg(#TAG);

//////////////////////////////////////////////////////////////////////////

IFeatureSpace* IFeatureSpace::Deserialize(std::istream& in) { return FeatureSpaceFactory::Deserialize(in); }

FeatureSpaceFactory::FunMapType FeatureSpaceFactory::deserializeFunMap;

IFeatureSpace* FeatureSpaceFactory::Deserialize(std::istream& in)
{
	std::streampos pos = in.tellg();
	char header[IFeatureSpace::BinaryHeaderSize] = {0};
	in.read(header, IFeatureSpace::BinaryHeaderSize * sizeof(char));
	in.seekg(pos);

	FunMapType::const_iterator it = deserializeFunMap.find(std::string(header));
	if(it == deserializeFunMap.end()) {
		std::cerr << "no feature space tagged as " << header << std::endl;
		exit(-1);
	}
	return it->second(in);
}

template <class T>
FeatureSpaceRegister<T>::FeatureSpaceRegister(const std::string& tag)
{
	deserializeFunMap[tag] = T::Deserialize;
}



} } }

#endif

