//
//  MultiFeatureFunctors.h
//  FISH
//
//  Created by Yaozong Gao on 12/08/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __MultiFeatureFunctors_h__
#define __MultiFeatureFunctors_h__

#include "feature/IFeatureSpace.h"

namespace BRIC { namespace IDEA { namespace FISH {


class MultiFeatureFunctors: public IFeatureFunctor
{
public:

	void resize(size_t num);
	unsigned int size() const;
	void clear();

	double operator() (unsigned int featureIndex) const;
	double GetResponse(unsigned int featureIndex) const;


	std::vector<IFeatureFunctor*> m_functors;
	std::vector<int> m_range;
};


void MultiFeatureFunctors::resize(size_t num)
{
	m_functors.resize(num);
	m_range.resize(num);
}

unsigned int MultiFeatureFunctors::size() const
{
	return m_functors.size();
}

void MultiFeatureFunctors::clear()
{
	m_functors.clear();
	m_range.clear();
}

double MultiFeatureFunctors::operator() (unsigned int featureIndex) const
{
	return GetResponse(featureIndex);
}

double MultiFeatureFunctors::GetResponse(unsigned int featureIndex) const
{
	size_t idx = m_range.size();
	for (size_t i = 0; i < m_range.size(); ++i)
	{
		if (m_range[i] > featureIndex)
		{
			idx = i;
			break;
		}
	}

	if (idx == m_range.size())
		err_message("MultiFeatureFunctors counting problem");

	if (idx == 0)
		return m_functors[idx]->GetResponse(featureIndex);
	else
		return m_functors[idx]->GetResponse(featureIndex - m_range[idx-1]);
}



} } }


#endif