//
//  FeatureSpaceArray.h
//  FISH
//
//  Created by Yaozong Gao on 9/23/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __FeatureSpaceArray_h__
#define __FeatureSpaceArray_h__

#include "feature/IFeatureSpace.h"

namespace BRIC { namespace IDEA { namespace FISH {

// forward declaration
template <class TFeatureSpace>
class FeatureSpaceArray;

/** @brief patch feature functor array */
// compute features given a patch
template <class TPatchFunctor>
class PatchFeatureFunctorArray
{
public:
	typedef typename TPatchFunctor::FeatureSpaceType FeatureSpaceType;

	PatchFeatureFunctorArray(const FeatureSpaceArray<FeatureSpaceType>& fs_array);
	~PatchFeatureFunctorArray();

	void SetPatch(const mxImage<double>& patch, int index);
	void PreprocessInput(mxImage<double>& patch, int index) const;

	TPatchFunctor& operator[] (unsigned int idx);
	const TPatchFunctor& operator[] (unsigned int idx) const;
	unsigned int size() const;
	void clear();
private:
	std::vector<TPatchFunctor*> m_functors;
};

/** @brief image feature functor array */
// compute features given an image
template <class TImageFunctor>
class ImageFeatureFunctorArray
{
public:
	typedef typename TImageFunctor::FeatureSpaceType FeatureSpaceType;

	ImageFeatureFunctorArray(const FeatureSpaceArray<FeatureSpaceType>& fs_array);
	~ImageFeatureFunctorArray();

	void SetImage(const mxImage<double>& image, int index);
	void SetVoxel(const vect3<unsigned int>& pos, int index);
	void PreprocessInput(mxImage<double>& image, int index) const;

	TImageFunctor& operator[] (unsigned int idx);
	const TImageFunctor& operator[] (unsigned int idx) const;
	unsigned int size() const;
	void clear();
private:
	std::vector<TImageFunctor*> m_functors;
};

/** @brief Feature Space Array */
template <class TFeatureSpace>
class FeatureSpaceArray
{
public:
	FeatureSpaceArray(bool releaseChildren = true);
	~FeatureSpaceArray();

	void AddFeatureSpace( TFeatureSpace* featureSpace );
	const std::vector<TFeatureSpace*>& GetFeatureSpaces() const;
	std::auto_ptr< PatchFeatureFunctorArray<typename TFeatureSpace::PatchFunctorType> > CreatePatchFeatureFunctor() const;
	std::auto_ptr< ImageFeatureFunctorArray<typename TFeatureSpace::ImageFunctorType> > CreateImageFeatureFunctor() const;
	TFeatureSpace& operator[] (unsigned int idx);
	const TFeatureSpace& operator[] (unsigned int idx) const;
	unsigned int size() const;
	void clear();

private:
	std::vector<TFeatureSpace*> m_featureSpaces;
	bool m_releaseChildren;
};


//////////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////////

// begin the definition of PatchFeatureFunctorArray
template <class TPatchFunctor>
PatchFeatureFunctorArray<TPatchFunctor>::PatchFeatureFunctorArray(const FeatureSpaceArray<typename TPatchFunctor::FeatureSpaceType>& fss)
{
	m_functors.reserve(fss.size());
	for(unsigned int i = 0; i < fss.size(); ++i) 
		m_functors.push_back(fss[i].CreatePatchFeatureFunctor());
}

template <class TPatchFunctor>
PatchFeatureFunctorArray<TPatchFunctor>::~PatchFeatureFunctorArray()
{
	for (unsigned int i = 0; i < m_functors.size(); ++i) {
		if (m_functors[i] != NULL) {
			delete m_functors[i];
			m_functors[i] = NULL;
		}
	}
	m_functors.clear();
}

template <class TPatchFunctor>
void PatchFeatureFunctorArray<TPatchFunctor>::SetPatch(const mxImage<double>& patch, int index)
{
	for (unsigned int i = 0; i < m_functors.size(); ++i)
		m_functors[i]->SetPatch(patch, index);
}

template <class TPatchFunctor>
void PatchFeatureFunctorArray<TPatchFunctor>::PreprocessInput(mxImage<double>& patch, int index) const
{
	if(m_functors.size() == 0) {
		std::cerr << "empty functor array" << std::endl; exit(-1);
	}
	m_functors[0]->PreprocessInput(patch,index);
}

template <class TPatchFunctor>
TPatchFunctor& PatchFeatureFunctorArray<TPatchFunctor>::operator[] (unsigned int idx)
{
	assert(idx < static_cast<unsigned int>(m_functors.size()));
	return *m_functors[idx];
}

template <class TPatchFunctor>
const TPatchFunctor& PatchFeatureFunctorArray<TPatchFunctor>::operator[] (unsigned int idx) const 
{
	assert(idx < static_cast<unsigned int>(m_functors.size()));
	return *m_functors[idx];
}

template <class TPatchFunctor>
unsigned int PatchFeatureFunctorArray<TPatchFunctor>::size() const
{
	return static_cast<unsigned int>( m_functors.size() );
}

template <class TPatchFunctor>
void PatchFeatureFunctorArray<TPatchFunctor>::clear()
{
	m_functors.clear();
}
// end of the definition of PatchFeatureFunctorArray



// begin of the definition of ImageFeatureFunctorArray
template <class TImageFunctor>
ImageFeatureFunctorArray<TImageFunctor>::ImageFeatureFunctorArray(const FeatureSpaceArray<typename TImageFunctor::FeatureSpaceType>& fs_array)
{
	m_functors.reserve(fs_array.size());
	for(unsigned int i = 0; i < fs_array.size(); ++i) 
		m_functors.push_back(fs_array[i].CreateImageFeatureFunctor());
}

template <class TImageFunctor>
ImageFeatureFunctorArray<TImageFunctor>::~ImageFeatureFunctorArray()
{
	for (unsigned int i = 0; i < m_functors.size(); ++i)
	{
		if(m_functors[i] != NULL) {
			delete m_functors[i];
			m_functors[i] = NULL;
		}
	}
	m_functors.clear();
}

template <class TImageFunctor>
void ImageFeatureFunctorArray<TImageFunctor>::SetImage(const mxImage<double>& image, int index)
{
	for (unsigned int i = 0; i < m_functors.size(); ++i)
		m_functors[i]->SetImage(image, index);
}

template <class TImageFunctor>
void ImageFeatureFunctorArray<TImageFunctor>::SetVoxel(const vect3<unsigned int>& pos, int index)
{
	for (unsigned int i = 0; i < m_functors.size(); ++i)
		m_functors[i]->SetVoxel(pos, index);
}

template <class TImageFunctor>
void ImageFeatureFunctorArray<TImageFunctor>::PreprocessInput(mxImage<double>& image, int index) const
{
	if(m_functors.size() == 0) {
		std::cerr << "empty functor array" << std::endl; exit(-1);
	}
	m_functors[0]->PreprocessInput(image,index);
}

template <class TImageFunctor>
TImageFunctor& ImageFeatureFunctorArray<TImageFunctor>::operator[] (unsigned int idx)
{
	assert(idx < static_cast<unsigned int>(m_functors.size()));
	return *m_functors[idx];
}

template <class TImageFunctor>
const TImageFunctor& ImageFeatureFunctorArray<TImageFunctor>::operator[] (unsigned int idx) const 
{
	assert(idx < static_cast<unsigned int>(m_functors.size()));
	return *m_functors[idx];
}

template <class TImageFunctor>
unsigned int ImageFeatureFunctorArray<TImageFunctor>::size() const
{
	return static_cast<unsigned int>( m_functors.size() );
}

template <class TImageFunctor>
void ImageFeatureFunctorArray<TImageFunctor>::clear()
{
	m_functors.clear();
}
// end of the definition of ImageFeatureFunctorArray


// begin of the definition of FeatureSpaceArray
template <class TFeatureSpace>
FeatureSpaceArray<TFeatureSpace>::FeatureSpaceArray(bool releaseChildren) 
{
	m_releaseChildren = releaseChildren;
}

template <class TFeatureSpace>
FeatureSpaceArray<TFeatureSpace>::~FeatureSpaceArray() 
{
	if (m_releaseChildren)
		clear();
}

template <class TFeatureSpace>
void FeatureSpaceArray<TFeatureSpace>::AddFeatureSpace( TFeatureSpace* featureSpace ) 
{
	m_featureSpaces.push_back(featureSpace);
}

template <class TFeatureSpace>
const std::vector<TFeatureSpace*>& FeatureSpaceArray<TFeatureSpace>::GetFeatureSpaces() const
{
	return m_featureSpaces;
}

template <class TFeatureSpace>
unsigned int FeatureSpaceArray<TFeatureSpace>::size() const
{
	return static_cast<unsigned int>(m_featureSpaces.size());
}

template <class TFeatureSpace>
std::auto_ptr<PatchFeatureFunctorArray<typename TFeatureSpace::PatchFunctorType> > FeatureSpaceArray<TFeatureSpace>::CreatePatchFeatureFunctor() const
{
	std::auto_ptr<PatchFeatureFunctorArray<typename TFeatureSpace::PatchFunctorType> > ret( new PatchFeatureFunctorArray<typename TFeatureSpace::PatchFunctorType>(*this) );
	return ret;
}

template <class TFeatureSpace>
std::auto_ptr<ImageFeatureFunctorArray<typename TFeatureSpace::ImageFunctorType> > FeatureSpaceArray<TFeatureSpace>::CreateImageFeatureFunctor() const
{
	std::auto_ptr<ImageFeatureFunctorArray<typename TFeatureSpace::ImageFunctorType> > ret( new ImageFeatureFunctorArray<typename TFeatureSpace::ImageFunctorType>(*this) );
	return ret;
}

template <class TFeatureSpace>
TFeatureSpace& FeatureSpaceArray<TFeatureSpace>::operator[] (unsigned int idx)
{
	assert(idx < m_featureSpaces.size());
	return (*m_featureSpaces[idx]);
}

template <class TFeatureSpace>
const TFeatureSpace& FeatureSpaceArray<TFeatureSpace>::operator[] (unsigned int idx) const
{
	assert(idx < m_featureSpaces.size());
	return (*m_featureSpaces[idx]);
}

template <class TFeatureSpace>
void FeatureSpaceArray<TFeatureSpace>::clear() 
{
	for (unsigned int i = 0; i < m_featureSpaces.size(); ++i)
	{
		if (m_featureSpaces[i] != NULL) {
			delete m_featureSpaces[i];
			m_featureSpaces[i] = NULL;
		}
	}
	m_featureSpaces.clear();
}
// end of the definition of FeatureSpaceArray


} } }

#endif

