//
//  Haar3DFeatureSpace.h
//  FISH
//
//  Created by Yaozong Gao on 9/23/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __Haar3DFeatureSpace_h__
#define __Haar3DFeatureSpace_h__

#include "feature/haar3d/Haar3DFeatures.h"
#include "feature/IFeatureSpace.h"

namespace BRIC { namespace IDEA { namespace FISH {

// forward declaration of feature space
class Haar3DFeatureSpace;


/** @brief haar 3d feature functor (compute features given a patch) */
class Haar3DPatchFeatureFunctor: public IPatchFeatureFunctor
{
public:
	typedef Haar3DFeatureSpace FeatureSpaceType;

	Haar3DPatchFeatureFunctor(const Haar3DFeatureSpace& space);
	~Haar3DPatchFeatureFunctor();
	double operator() (unsigned int featureIndex) const;

	// overrides
	virtual void SetPatch(const mxImage<double>& patch, int index);
	virtual double GetResponse(unsigned int featureIndex) const;
	virtual void PreprocessInput(mxImage<double>& patch, int index) const;

private:
	const Haar3DFeatureSpace* m_featureSpace;
	const mxImage<double>* m_integralPatch;
};


/** @brief haar 3d feature functor (compute features given an image) */
class Haar3DImageFeatureFunctor: public IImageFeatureFunctor
{
public:
	typedef Haar3DFeatureSpace FeatureSpaceType;

	Haar3DImageFeatureFunctor(const Haar3DFeatureSpace& space);
	~Haar3DImageFeatureFunctor();
	double operator() (unsigned int featureIndex) const;

	// overrides
	virtual void SetImage(const mxImage<double>& image, int index);
	virtual void SetVoxel(const vect3<unsigned int>& pos, int index);
	virtual double GetResponse(unsigned int featureIndex) const;
	virtual void PreprocessInput(mxImage<double>& image, int index) const;

private:
	const Haar3DFeatureSpace* m_featureSpace;
	vect3<unsigned int> m_patchSize;
	const mxImage<double>* m_integralImage;
	vect3<unsigned int> m_pos;
};


/** @brief Haar 3d feature space */
class Haar3DFeatureSpace: public IFeatureSpace
{
private:
	Haar3DFeatureSpace(const Haar3DFeatureSpace& copy);
	Haar3DFeatureSpace& operator=(const Haar3DFeatureSpace& rhs);

public:
	typedef Haar3DPatchFeatureFunctor PatchFunctorType;
	typedef Haar3DImageFeatureFunctor ImageFunctorType;

	Haar3DFeatureSpace();
	~Haar3DFeatureSpace();

	/** @brief interfaces */
	virtual void RandomizeFeatureSpace(Random& random, unsigned int numFeatures);
	virtual Haar3DPatchFeatureFunctor* CreatePatchFeatureFunctor() const;
	virtual Haar3DImageFeatureFunctor* CreateImageFeatureFunctor() const;
	virtual vect3<unsigned int> GetPatchSize(int index) const;
	virtual int GetFeatureNumber() const;
	virtual int GetNumberFeatureSpaces() const;
	virtual void Serialize(std::ostream& o) const;
	static Haar3DFeatureSpace* Deserialize(std::istream& in);

	/** @brief access */
	void SetPatchSize(const vect3<unsigned int>& patchSize);
	void AddFilterSize(const vect3<unsigned int>& size);
	void SetFilterSizes(const std::vector< vect3<unsigned int> >& filtersizes);
	std::vector< Haar3DFilter >& GetFilters();
	const std::vector< vect3<unsigned int> >& GetFilterSizes() const;
	const std::vector< Haar3DFilter >& GetFilters() const;

	void RandomizeFeatureSpace_oneblock(Random& random, unsigned int numFeatures);
	void RandomizeFeatureSpace_twoblocks(Random& random, unsigned int numFeatures);
	void RandomizeFeatureSpace_onlytwoblocks(Random& random, unsigned int numFeatures);
	void RandomizeFeatureSpace_radiation(Random& random, unsigned int numFeatures);

private:

	std::vector< Haar3DFilter > m_filters;
	vect3<unsigned int> m_patchSize;
	std::vector< vect3<unsigned int> > m_filterSizes;
	static const char* m_binaryHeader;

	FS_REGISTER_DEC_TYPE(Haar3DFeatureSpace)
};

//////////////////////////////////////////////////////////////////////////
// Implementations
//////////////////////////////////////////////////////////////////////////

// begin the definition of Haar3DPatchFeatureFunctor
Haar3DPatchFeatureFunctor::Haar3DPatchFeatureFunctor(const Haar3DFeatureSpace& space)
{
	m_featureSpace = &space;
}

Haar3DPatchFeatureFunctor::~Haar3DPatchFeatureFunctor()
{
	m_featureSpace = NULL;
	m_integralPatch = NULL;
}

void Haar3DPatchFeatureFunctor::SetPatch(const mxImage<double>& patch, int index)
{
	if(index != 0) {
		std::cerr << "functor index out of boundary" << std::endl; exit(-1);
	}
	m_integralPatch = &patch;
}

double Haar3DPatchFeatureFunctor::GetResponse(unsigned int featureIndex) const
{
	assert( featureIndex < m_featureSpace->GetFilters().size() );
	return m_featureSpace->GetFilters()[featureIndex].GetResponseFromPatch(*m_integralPatch);
}

double Haar3DPatchFeatureFunctor::operator() (unsigned int featureIndex) const {
	return GetResponse(featureIndex);
}

void Haar3DPatchFeatureFunctor::PreprocessInput(mxImage<double>& patch, int index) const {
	if(index != 0) {
		std::cerr << "functor index out of boundary" << std::endl; exit(-1);
	}
	Haar3DBasics::IntegralImage(patch);
}
// end the definition of Haar3DPatchFeatureFunctor


// begin the definition of Haar3DImageFeatureFunctor 
Haar3DImageFeatureFunctor::Haar3DImageFeatureFunctor(const Haar3DFeatureSpace& space)
{
	m_featureSpace = &space;
	m_patchSize = space.GetPatchSize(0);
}

Haar3DImageFeatureFunctor::~Haar3DImageFeatureFunctor()
{
	m_featureSpace = NULL;
	m_integralImage = NULL;
}

void Haar3DImageFeatureFunctor::SetImage(const mxImage<double>& image, int index)
{
	if(index != 0) {
		std::cerr << "functor index out of boundary" << std::endl; exit(-1);
	}
	m_integralImage = &image;
}

void Haar3DImageFeatureFunctor::SetVoxel(const vect3<unsigned int>& pos, int index)
{
    if(index != 0) {
		std::cerr << "functor index out of boundary" << std::endl; exit(-1);
	}
	m_pos = pos;
}

double Haar3DImageFeatureFunctor::GetResponse(unsigned int featureIndex) const
{
	assert( featureIndex < m_featureSpace->GetFilters().size() );
	return m_featureSpace->GetFilters()[featureIndex].GetResponseFromImage(*m_integralImage, m_pos, m_patchSize);
}

double Haar3DImageFeatureFunctor::operator() (unsigned int featureIndex) const
{
	return GetResponse(featureIndex);
}

void Haar3DImageFeatureFunctor::PreprocessInput(mxImage<double>& image, int index) const {
	if(index != 0) {
		std::cerr << "functor index out of boundary" << std::endl; exit(-1);
	}
	Haar3DBasics::IntegralImage(image);
}
// end the definition of Haar3DImageFeatureFunctor


// begin the definition of Haar3DFeatureSpace
Haar3DFeatureSpace::Haar3DFeatureSpace(): m_patchSize(0,0,0) {}

Haar3DFeatureSpace::~Haar3DFeatureSpace() {}

void Haar3DFeatureSpace::RandomizeFeatureSpace_onlytwoblocks(Random& random, unsigned int numFeatures)
{
	if (m_filterSizes.size() == 0)
	{
		std::cerr << "Set available filter sizes before randomize feature space" << std::endl;
		exit(-1);
	}

	for (unsigned int i = 0; i < m_filterSizes.size(); ++i)
	{
		if (m_filterSizes[i][0] > m_patchSize[0] || m_filterSizes[i][1] > m_patchSize[1] || m_filterSizes[i][2] > m_patchSize[2])
		{
			std::cerr << "filter size is larger than patch size" << std::endl;
			exit(-1);
		}
	}

	m_filters.clear();
	m_filters.resize(numFeatures);

	for (unsigned int i = 0; i < numFeatures; ++i)
	{
		int numRects = 2;

		for (int j = 0; j < numRects; ++j)
		{
			Haar3DRect rect;

			vect3<unsigned int> filterSize = m_filterSizes[random.Next(0, static_cast<int>(m_filterSizes.size()) - 1)];
			rect.SetSize(filterSize);

			vect3<unsigned int> lowerBound, upperBound;
			for (int k = 0; k < 3; ++k)
			{
				lowerBound[k] = filterSize[k] / 2;
				upperBound[k] = m_patchSize[k] + filterSize[k] / 2 - filterSize[k];
			}

			vect3<unsigned int> center;
			for (int k = 0; k < 3; ++k)
				center[k] = random.Next(lowerBound[k], upperBound[k]);

			rect.SetCenter(center);

			if (j == 0)
				rect.SetFactor(1.0);
			else if (j == 1)
				rect.SetFactor(-1.0);
			else {
				std::cerr << "Maximum number of Rectangles is 2" << std::endl;
				exit(-1);
			}

			m_filters[i].AddRect(rect);
		}
	}
}

void Haar3DFeatureSpace::RandomizeFeatureSpace_twoblocks(Random& random, unsigned int numFeatures)
{
	if (m_filterSizes.size() == 0)
	{
		std::cerr << "Set available filter sizes before randomize feature space" << std::endl;
		exit(-1);
	}

	for (unsigned int i = 0; i < m_filterSizes.size(); ++i)
	{
		if (m_filterSizes[i][0] > m_patchSize[0] || m_filterSizes[i][1] > m_patchSize[1] || m_filterSizes[i][2] > m_patchSize[2])
		{
			std::cerr << "filter size is larger than patch size" << std::endl;
			exit(-1);
		}
	}

	m_filters.clear();
	m_filters.resize(numFeatures);

	for (unsigned int i = 0; i < numFeatures; ++i)
	{
		int numRects = random.Next(1, 2);	// either one block or two blocks

		for (int j = 0; j < numRects; ++j)
		{
			Haar3DRect rect;

			vect3<unsigned int> filterSize = m_filterSizes[random.Next(0, static_cast<int>(m_filterSizes.size()) - 1)];
			rect.SetSize(filterSize);

			vect3<unsigned int> lowerBound, upperBound;
			for (int k = 0; k < 3; ++k)
			{
				lowerBound[k] = filterSize[k] / 2;
				upperBound[k] = m_patchSize[k] + filterSize[k] / 2 - filterSize[k];
			}

			vect3<unsigned int> center;
			for (int k = 0; k < 3; ++k)
				center[k] = random.Next(lowerBound[k], upperBound[k]);

			rect.SetCenter(center);

			if (j == 0)
				rect.SetFactor(1.0);
			else if (j == 1)
				rect.SetFactor(-1.0);
			else {
				std::cerr << "Maximum number of Rectangles is 2" << std::endl;
				exit(-1);
			}

			m_filters[i].AddRect(rect);
		}
	}
}

void Haar3DFeatureSpace::RandomizeFeatureSpace_oneblock(Random& random, unsigned int numFeatures)
{
	if (m_filterSizes.size() == 0)
	{
		std::cerr << "Set available filter sizes before randomize feature space" << std::endl;
		exit(-1);
	}

	for (unsigned int i = 0; i < m_filterSizes.size(); ++i)
	{
		if (m_filterSizes[i][0] > m_patchSize[0] || m_filterSizes[i][1] > m_patchSize[1] || m_filterSizes[i][2] > m_patchSize[2])
		{
			std::cerr << "filter size is larger than patch size" << std::endl;
			exit(-1);
		}
	}

	m_filters.clear();
	m_filters.resize(numFeatures);

	for (unsigned int i = 0; i < numFeatures; ++i)
	{
		Haar3DRect rect;

		vect3<unsigned int> filterSize = m_filterSizes[random.Next(0, static_cast<int>(m_filterSizes.size()) - 1)];
		rect.SetSize(filterSize);

		vect3<unsigned int> lowerBound, upperBound;
		for (int k = 0; k < 3; ++k)
		{
			lowerBound[k] = filterSize[k] / 2;
			upperBound[k] = m_patchSize[k] + filterSize[k] / 2 - filterSize[k];
		}

		vect3<unsigned int> center;
		for (int k = 0; k < 3; ++k)
			center[k] = random.Next(lowerBound[k], upperBound[k]);

		rect.SetCenter(center);
		rect.SetFactor(1.0);
		m_filters[i].AddRect(rect);
	}
}

void Haar3DFeatureSpace::RandomizeFeatureSpace_radiation(Random& random, unsigned int numFeatures)
{
	if (m_filterSizes.size() == 0)
	{
		std::cerr << "Set available filter sizes before randomize feature space" << std::endl;
		exit(-1);
	}

	for (unsigned int i = 0; i < m_filterSizes.size(); ++i)
	{
		if (m_filterSizes[i][0] > m_patchSize[0] || m_filterSizes[i][1] > m_patchSize[1] || m_filterSizes[i][2] > m_patchSize[2])
		{
			std::cerr << "filter size is larger than patch size" << std::endl;
			exit(-1);
		}
	}

	m_filters.clear();
	m_filters.resize(numFeatures);

	// patch center
	vect3<int> patch_center;
	for (int i = 0; i < 3; ++i)
		patch_center[i] = m_patchSize[i] / 2;

	// random features on radiation directions
	for (unsigned int i = 0; i < numFeatures; ++i)
	{
		Haar3DRect rect;
		
		// randomize a filter size
		vect3<unsigned int> filterSize = m_filterSizes[random.Next(0, static_cast<int>(m_filterSizes.size()) - 1)];
		rect.SetSize(filterSize);

		// center selection range
		vect3<unsigned int> lowerBound, upperBound;
		for (int k = 0; k < 3; ++k)
		{
			lowerBound[k] = filterSize[k] / 2;
			upperBound[k] = m_patchSize[k] + filterSize[k] / 2 - filterSize[k];
		}

		// randomize the block center
		vect3<unsigned int> center;

		bool isBreak = false;
		while (!isBreak)
		{
			int line_idx = random.Next(0, 8);
			switch (line_idx)
			{
			case 0: // x axis

				center[0] = random.Next(lowerBound[0], upperBound[0]);
				center[1] = patch_center[1];
				center[2] = patch_center[2];
				break;

			case 1: // y axis

				center[0] = patch_center[0];
				center[1] = random.Next(lowerBound[1], upperBound[1]);
				center[2] = patch_center[2];
				break;

			case 2: // z axis

				center[0] = patch_center[0];
				center[1] = patch_center[1];
				center[2] = random.Next(lowerBound[2], upperBound[2]);
				break;

			case 3: // x-y 45 line

				if (m_patchSize[0] % 2 == 1)
					center[0] = random.Next(lowerBound[0], upperBound[0]);
				else
					center[0] = random.Next(lowerBound[0] + 1, upperBound[0]);

				center[1] = patch_center[1] + (center[0] - patch_center[0]);
				center[2] = patch_center[2];
				break;

			case 4: // x-y 135 line

				if (m_patchSize[0] % 2 == 1)
					center[0] = random.Next(lowerBound[0], upperBound[0]);
				else
					center[0] = random.Next(lowerBound[0] + 1, upperBound[0]);

				center[1] = patch_center[1] - (center[0] - patch_center[0]);
				center[2] = patch_center[2];
				break;

			case 5: // x-z 45 line

				if (m_patchSize[0] % 2 == 1)
					center[0] = random.Next(lowerBound[0], upperBound[0]);
				else
					center[0] = random.Next(lowerBound[0] + 1, upperBound[0]);

				center[1] = patch_center[1];
				center[2] = patch_center[2] + (center[0] - patch_center[0]);
				break;

			case 6: // x-z 135 line

				if (m_patchSize[0] % 2 == 1)
					center[0] = random.Next(lowerBound[0], upperBound[0]);
				else
					center[0] = random.Next(lowerBound[0] + 1, upperBound[0]);

				center[1] = patch_center[1];
				center[2] = patch_center[2] - (center[0] - patch_center[0]);
				break;

			case 7: // y-z 45 line

				center[0] = patch_center[0];

				if (m_patchSize[1] % 2 == 1)
					center[1] = random.Next(lowerBound[1], upperBound[1]);
				else
					center[1] = random.Next(lowerBound[1] + 1, upperBound[1]);

				center[2] = patch_center[2] + (center[1] - patch_center[1]);
				break;

			case 8: // y-z 135 line

				center[0] = patch_center[0];

				if (m_patchSize[1] % 2 == 1)
					center[1] = random.Next(lowerBound[1], upperBound[1]);
				else
					center[1] = random.Next(lowerBound[1] + 1, upperBound[1]);

				center[2] = patch_center[2] - (center[1] - patch_center[1]);
				break;

			default: err_message("unexpected error");
			}

			vect3<int> box_sp, box_ep;
			int count = 0;
			for (int i = 0; i < 3; ++i)
			{
				box_sp[i] = center[i] - filterSize[i] / 2;
				box_ep[i] = box_sp[i] + filterSize[i] - 1;

				if (patch_center[i] >= box_sp[i] && patch_center[i] <= box_ep[i])
					++count;
			}

			if (count == 3)
				isBreak = false;
			else
				isBreak = true;

		}

		// set center
		for (int k = 0; k < 3; ++k)
			assert_message(center[k] >= lowerBound[k] && center[k] <= upperBound[k], "center out of valid region");
		rect.SetCenter(center);

		// add this rect
		m_filters[i].AddRect(rect);
	}
}

void Haar3DFeatureSpace::RandomizeFeatureSpace(Random& random, unsigned int numFeatures) 
{
	RandomizeFeatureSpace_twoblocks(random, numFeatures);
}

Haar3DPatchFeatureFunctor* Haar3DFeatureSpace::CreatePatchFeatureFunctor() const
{
	return new Haar3DPatchFeatureFunctor(*this);
}

Haar3DImageFeatureFunctor* Haar3DFeatureSpace::CreateImageFeatureFunctor() const
{
	return new Haar3DImageFeatureFunctor(*this);
}

vect3<unsigned int> Haar3DFeatureSpace::GetPatchSize(int index) const
{
	if(index != 0) {
		std::cerr << "feature space index out of boundary" << std::endl; exit(-1);
	}
	return m_patchSize;
}

const std::vector< vect3<unsigned int> >& Haar3DFeatureSpace::GetFilterSizes() const
{
	return m_filterSizes;
}

int Haar3DFeatureSpace::GetFeatureNumber() const
{
	return static_cast<int>(m_filters.size());
}

int Haar3DFeatureSpace::GetNumberFeatureSpaces() const 
{
	return 1;
}

void Haar3DFeatureSpace::Serialize(std::ostream& o) const
{
	char header[BinaryHeaderSize] = {0};
    strncpy(header, m_binaryHeader, strlen(m_binaryHeader)+1);
	o.write(header, BinaryHeaderSize);
	m_patchSize.Serialize(o);

	unsigned int size = static_cast<unsigned int>(m_filterSizes.size());
	o.write((const char*)(&size), sizeof(unsigned int));
	for (unsigned int i = 0; i < size; ++i)
		m_filterSizes[i].Serialize(o);

	size = static_cast<unsigned int>(m_filters.size());
	o.write((const char*)(&size), sizeof(unsigned int));
	for (unsigned int i = 0; i < size; ++i)
		m_filters[i].Serialize(o);

	o.flush();
}

Haar3DFeatureSpace* Haar3DFeatureSpace::Deserialize(std::istream& in)
{
	char header[BinaryHeaderSize] = {0};
	in.read(header, BinaryHeaderSize);
	if (strncmp(header, m_binaryHeader, strlen(m_binaryHeader)) != 0) {
		std::cerr << "binary header incorrect (Haar3DFeatureSpace)" << std::endl;
		exit(-1);
	}

	Haar3DFeatureSpace* featureSpace =  new Haar3DFeatureSpace;
	featureSpace->m_patchSize.Deserialize(in);

	unsigned int size = 0;
	in.read((char*)(&size), sizeof(unsigned int));
	featureSpace->m_filterSizes.resize(size);
	for (unsigned int i = 0; i < size; ++i)
		featureSpace->m_filterSizes[i].Deserialize(in);

	in.read((char*)(&size), sizeof(unsigned int));
	featureSpace->m_filters.resize(size);
	for (unsigned int i = 0; i < size; ++i)
		featureSpace->m_filters[i].Deserialize(in);

	return featureSpace;
}


void Haar3DFeatureSpace::SetFilterSizes(const std::vector< vect3<unsigned int> >& filtersizes)
{
	m_filterSizes = filtersizes;
}

void Haar3DFeatureSpace::SetPatchSize(const vect3<unsigned int>& patchSize) 
{
	m_patchSize = patchSize;
}

void Haar3DFeatureSpace::AddFilterSize(const vect3<unsigned int>& size)
{
	m_filterSizes.push_back(size);
}

std::vector< Haar3DFilter >& Haar3DFeatureSpace::GetFilters() 
{
	return m_filters;
}

const std::vector< Haar3DFilter >& Haar3DFeatureSpace::GetFilters() const
{
	return m_filters;
}

const char* Haar3DFeatureSpace::m_binaryHeader = "BRIC.IDEA.FISH.Haar3DFeatureSpace";

FS_REGISTER_DEF_TYPE(Haar3DFeatureSpace,BRIC.IDEA.FISH.Haar3DFeatureSpace)
// end the definition of Haar3DFeatureSpace



} } }

#endif

