//
//  Haar3DFeatures.h
//  FISH
//
//  Created by Yaozong Gao on 7/11/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __Haar3DFeatures_h__
#define __Haar3DFeatures_h__

// fish headers
#include "common/mxImage.h"
#include "common/stringUtils.h"
#include "common/Random.h"
#include <assert.h>


namespace BRIC { namespace IDEA { namespace FISH {


/** @brief basic toolkits for 3D haar feature computation
 *
 * basic helper functions to compute integral image and integral sum.
 *
 */
class Haar3DBasics
{
public:
    
    /** @brief compute integral image
     *
     * @param img mxImage with double type
     * @return void
     */
    static void IntegralImage(mxImage<double>& img) 
	{
		vect3<unsigned int> imageSize = img.GetImageSize();

		// from line to plane to volume
		// x axis
		for (unsigned int x = 1; x < imageSize[0]; ++x)
			img(x,0,0) = img(x,0,0) + img(x-1,0,0);
		// y axis
		for (unsigned int y = 1; y < imageSize[1]; ++y)
			img(0,y,0) = img(0,y,0) + img(0,y-1,0);
		// z axis
		for (unsigned int z = 1; z < imageSize[2]; ++z)
			img(0,0,z) = img(0,0,z) + img(0,0,z-1);

		// x = 0 plane
		for (unsigned int z = 1; z < imageSize[2]; ++z) {
			for (unsigned int y = 1; y < imageSize[1]; ++y) {
				img(0,y,z) = img(0,y,z) + img(0,y-1,z) + img(0,y,z-1) - img(0,y-1,z-1);
				//assert(img(0,y,z)+1e-4 >= img(0,y-1,z));
				//assert(img(0,y,z)+1e-4 >= img(0,y,z-1));
			}
		}
		// y = 0 plane
		for (unsigned int z = 1; z < imageSize[2]; ++z) {
			for (unsigned int x = 1; x < imageSize[0]; ++x) {
				img(x,0,z) = img(x,0,z) + img(x-1,0,z) + img(x,0,z-1) - img(x-1,0,z-1);
				//assert(img(x,0,z)+1e-4 >= img(x,0,z-1));
				//assert(img(x,0,z)+1e-4 >= img(x-1,0,z));
			}
		}
		// z = 0 plane
		for (unsigned int y = 1; y < imageSize[1]; ++y) {
			for (unsigned int x = 1; x < imageSize[0]; ++x) {
				img(x,y,0) = img(x,y,0) + img(x-1,y,0) + img(x,y-1,0) - img(x-1,y-1,0);
				//assert(img(x,y,0)+1e-4 >= img(x,y-1,0));
				//assert(img(x,y,0)+1e-4 >= img(x-1,y,0));
			}
		}

		// rest volume
		for (unsigned int z = 1; z < imageSize[2]; ++z) {
			for (unsigned int y = 1; y < imageSize[1]; ++y) {
				for (unsigned int x = 1; x < imageSize[0]; ++x) {
					img(x,y,z) = img(x,y,z) + img(x-1,y,z) + img(x,y-1,z) + img(x,y,z-1)
						- img(x-1,y-1,z) - img(x-1,y,z-1) - img(x,y-1,z-1) + img(x-1,y-1,z-1);
					//assert(img(x,y,z)+1e-4 >= img(x-1,y,z));
					//assert(img(x,y,z)+1e-4 >= img(x,y-1,z));
					//assert(img(x,y,z)+1e-4 >= img(x,y,z-1));
				}
			}
		}
	}
    

    /** @brief compute integral sum based on integral image
     *
     * compute summation within a rectangle given an integral image
     * use double precision to prevent leakage and rounding problem
     *
     * @param intImg integral image
     * @param sp left-top-inferior point (inclusive)
     * @param ep right-bot-superior point (inclusive)
     * @return rectangle sum of a region
     */
    static double IntegralRectSum(const mxImage<double>& intImg, const vect3<unsigned int>& sp, const vect3<unsigned int>& ep)
	{
		assert( intImg.PtInImage(sp[0], sp[1], sp[2]) && intImg.PtInImage(ep[0], ep[1], ep[2]) );

		double v111 = intImg(ep[0],ep[1],ep[2]);
		double v011 = (sp[0] == 0) ? 0 : intImg(sp[0]-1, ep[1],   ep[2]);
		double v101 = (sp[1] == 0) ? 0 : intImg(ep[0],   sp[1]-1, ep[2]);
		double v110 = (sp[2] == 0) ? 0 : intImg(ep[0],   ep[1],   sp[2]-1);
		double v001 = (sp[0] == 0 || sp[1] == 0) ? 0 : intImg(sp[0]-1, sp[1]-1, ep[2]);
		double v010 = (sp[0] == 0 || sp[2] == 0) ? 0 : intImg(sp[0]-1, ep[1],   sp[2]-1);
		double v100 = (sp[1] == 0 || sp[2] == 0) ? 0 : intImg(ep[0],   sp[1]-1, sp[2]-1);
		double v000 = (sp[0] == 0 || sp[1] == 0 || sp[2] == 0) ? 0: intImg(sp[0]-1, sp[1]-1, sp[2]-1);

		double rect_sum = v111-v011-v101-v110+v001+v010+v100-v000;
		return rect_sum;
	}

    /** @brief compute the region summation using raw image
     * 
     * only used to test the correctness of IntegralRectSum. DON'T USE IT!
     *
     */
    template <typename T, typename TOut>
    static TOut NaiveRectSum (const mxImage<T>& image, const vect3<unsigned int>& sp, const vect3<unsigned int>& ep) {
        
        assert( image.PtInImage(sp[0], sp[1], sp[2]) && image.PtInImage(ep[0], ep[1], ep[2]) );
        
        TOut sum = 0;
        
        for (unsigned int z = sp[2]; z <= ep[2]; ++z) {
            for (unsigned int y = sp[1]; y <= ep[1]; ++y) {
                for (unsigned int x = sp[0]; x <= ep[0]; ++x) {
                    sum = sum + static_cast<TOut>(image(x,y,z));
                }
            }
        }
        
        return sum;
    }  
};


/** @brief 3D Rectangle with factor/sign */
class Haar3DRect
{
public:

	Haar3DRect(): m_center(0,0,0), m_size(0,0,0), m_factor(1) {}
	
	Haar3DRect(const vect3<unsigned int>& center, const vect3<unsigned int>& size, float factor):
		m_center(center), m_size(size), m_factor(factor) {}


	/** @brief get/set access */
	void SetCenter(const vect3<unsigned int>& center) { m_center = center; }
	vect3<unsigned int> GetCenter() const { return m_center; }

	void SetSize(const vect3<unsigned int>& size) { m_size = size; }
	vect3<unsigned int> GetSize() const { return m_size; }

	void SetFactor(float factor) { m_factor = factor; }
	float GetFactor() { return m_factor; }


	//
	// compute response functions
	// 

	// get response from integral patch
	double GetResponseFromPatch(const mxImage<double>& integralPatch) const {

		vect3<int> sp, ep;
		for (int i = 0; i < 3; ++i)
		{
			sp[i] = static_cast<int>(m_center[i]) - static_cast<int>(m_size[i]/2);
			ep[i] = sp[i] + static_cast<int>(m_size[i]) - 1;
		}

		assert( integralPatch.PtInImage(sp[0], sp[1], sp[2]) && integralPatch.PtInImage(ep[0], ep[1], ep[2]) );
		return m_factor * Haar3DBasics::IntegralRectSum(integralPatch, vect3<unsigned int>(sp[0], sp[1], sp[2]), vect3<unsigned int>(ep[0], ep[1], ep[2]));
	}

	// get response from integral image
	double GetResponseFromImage(const mxImage<double>& integralImage, const vect3<unsigned int>& voxel, const vect3<unsigned int>& patchSize) const {

		vect3<int> sp, ep;
		for (int i = 0; i < 3; ++i)
		{
			sp[i] = static_cast<int>(voxel[i]) - static_cast<int>(patchSize[i]/2) + static_cast<int>(m_center[i]) - static_cast<int>(m_size[i]/2);
			ep[i] = sp[i] + static_cast<int>(m_size[i]) - 1;
		}

		assert( integralImage.PtInImage(sp[0], sp[1], sp[2]) && integralImage.PtInImage(ep[0], ep[1], ep[2]) );
		return m_factor * Haar3DBasics::IntegralRectSum(integralImage, vect3<unsigned int>(sp[0], sp[1], sp[2]), vect3<unsigned int>(ep[0], ep[1], ep[2]));
	}

	template <typename T>
	double NaiveGetResponse(const mxImage<T>& patch) const {

		vect3<int> sp, ep;
		for (int i = 0; i < 3; ++i)
		{
			sp[i] = static_cast<int>(m_center[i]) - static_cast<int>(m_size[i]/2);
			ep[i] = sp[i] + static_cast<int>(m_size[i]) - 1;
		}
		
		assert( patch.PtInImage(sp[0], sp[1], sp[2]) && patch.PtInImage(ep[0], ep[1], ep[2]) );
		return m_factor * Haar3DBasics::NaiveRectSum<T,double>(patch, vect3<unsigned int>(sp[0], sp[1], sp[2]), vect3<unsigned int>(ep[0], ep[1], ep[2]));
	}

	//
	// IO and relational operators
	// 

	bool operator==(const Haar3DRect& rect) const {

		if(m_center == rect.m_center && m_size == rect.m_size && fabs(m_factor - rect.m_factor) <= 1e-6) 
			return true;
		else
			return false;
	}

	void Serialize(std::ostream& o) const {
		m_center.Serialize(o);
		m_size.Serialize(o);
		o.write((const char*)(&m_factor), sizeof(float));
	}

	void Deserialize(std::istream& i) {     
		m_center.Deserialize(i);
		m_size.Deserialize(i);
		i.read((char*)(&m_factor), sizeof(float));
	}

	friend std::ostream& operator<<(std::ostream& out, const Haar3DRect& rect);

private:

	vect3<unsigned int> m_center;
	vect3<unsigned int> m_size;
	float m_factor;
};

// overloaded output operator for HaarRectangle
std::ostream& operator<<(std::ostream& out, const Haar3DRect& rect) {

	if (rect.m_factor > 0)
		out << "[+:(" << rect.m_center[0] << "," << rect.m_center[1] << "," << rect.m_center[2] << "),("
		<< rect.m_size[0] << "," << rect.m_size[1] << "," << rect.m_size[2] << ")]";
	else
		out << "[-:(" << rect.m_center[0] << "," << rect.m_center[1] << "," << rect.m_center[2] << "),("
		<< rect.m_size[0] << "," << rect.m_size[1] << "," << rect.m_size[2] << ")]";

	return out;
}



/** @brief Haar3D filter */
class Haar3DFilter 
{
public:

	Haar3DFilter() {}

	Haar3DFilter(const Haar3DFilter& copy)
	{
		m_rects.resize(copy.m_rects.size());
		for (unsigned int i = 0; i < m_rects.size(); ++i)
		{
			m_rects[i] = copy.m_rects[i];
		}
	}

	void AddRect(const Haar3DRect& rect) {
		m_rects.push_back(rect);
	}

	size_t GetRectNumber() const {
		return m_rects.size();
	}

	//
	// compute response function
	// 

	// compute response given an integral patch
	double GetResponseFromPatch(const mxImage<double>& integralPatch) const {
		double response = 0;
		for (unsigned int i = 0; i < m_rects.size(); ++i)
			response += m_rects[i].GetResponseFromPatch(integralPatch);
		return response;
	}

	// compute response given an integral image
	double GetResponseFromImage(const mxImage<double>& integralImage, const vect3<unsigned int>& voxel, const vect3<unsigned int>& patchSize) const {
		double response = 0;
		for (unsigned int i = 0; i < m_rects.size(); ++i)
			response += m_rects[i].GetResponseFromImage(integralImage, voxel, patchSize);
		return response;
	}


	//
	// IO and relational operators
	//
	bool operator==(const Haar3DFilter& flt) const {

		if(m_rects.size() != flt.m_rects.size())
			return false;

		for(unsigned int i = 0; i < m_rects.size(); ++i) {
			if( !(m_rects[i] == flt.m_rects[i]) )
				return false;
		}

		return true;
	}

	bool operator!=(const Haar3DFilter& flt) const {
		return ! this->operator==(flt);
	}

	void Serialize(std::ostream& o) const {
		unsigned int size = static_cast<unsigned int>( m_rects.size() );
		o.write((const char*)(&size), sizeof(unsigned int));
		for (unsigned int i = 0; i < size; ++i)
			m_rects[i].Serialize(o);
	}

	void Deserialize(std::istream& in) {
		unsigned int size = 0;
		in.read((char*)(&size), sizeof(unsigned int));
		if (size != 0) {
			m_rects.resize(size);
			for (unsigned int i = 0; i < size; ++i)
				m_rects[i].Deserialize(in);
		}
	}

	friend std::ostream& operator<<(std::ostream& out, const Haar3DFilter& flt);

	Haar3DFilter& operator=(const Haar3DFilter& rhs) 
	{
		if(this == &rhs)
			return *this;

		m_rects.resize(rhs.m_rects.size());
		for (unsigned int i = 0; i < m_rects.size(); ++i)
			m_rects[i] = rhs.m_rects[i];

		return *this;
	}

public:

	std::vector< Haar3DRect > m_rects;
};

// overloaded output operator for HaarKernel
std::ostream& operator<<(std::ostream& out, const Haar3DFilter& flt)
{
	for(unsigned int i = 0; i < flt.m_rects.size(); ++ i) {
		if (i == 0)
			out << flt.m_rects[i];
		else
			out << "; " << flt.m_rects[i];
	}
	return out;
}

} } }


#endif 

