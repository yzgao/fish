//
//  Forest.h
//  FISH
//
//  Created by Yaozong Gao on 4/8/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __Forest_h__
#define __Forest_h__

#include "forest/Tree.h"

namespace BRIC { namespace IDEA { namespace FISH {
    

/** @brief Forest class
 *
 * Each forest contains a set of trees
 * 
 */ 
template <class W, class S>
class Forest {

public:
    
    typedef Tree<W,S> TreeType;
    
	/** @brief destructor 
	 *
	 * release all trees when deconstruction
	 */
    ~Forest() {
        for (unsigned int i = 0; i < m_trees.size(); ++i) {
            if (m_trees[i] != NULL) {
                delete m_trees[i];
            }
        }
    }
    
	/** @brief get the number of trees */
    unsigned int GetTreeNumber() const {
        return static_cast<unsigned int>( m_trees.size() );
    }
    
	/** @brief get the specified tree */
    const TreeType* GetTree(unsigned int treeIndex) const {
        return m_trees[treeIndex];
    }

	/** @brief get one tree */
	TreeType* GetTree(unsigned int treeIndex) {
		return m_trees[treeIndex];
	}
    
	/** @brief add a tree */
    void AddTree(std::auto_ptr<TreeType> tree) {
        
        tree->CheckValid();
        m_trees.push_back(tree.get());
        tree.release();
    }
    
	/** @brief fast apply without pre-compute all features ahead 
	 *
	 * compute the needed features on the fly. 
	 * Once the features are needed, invoke the functor to get the feature
	 * the functor should be implemented with operator(int), with feature index as input.
	 */
	template <class FunctorType>
	void FastApply(FunctorType& fun, unsigned int* leafIndexes) const {

		for (int i = 0; i < static_cast<int>(m_trees.size()); ++i)
			leafIndexes[i] = m_trees[i]->FastApply(fun);
	}

	/** @brief fast apply (different functors for different trees)*/
	template <class FunctorCollectionType>
	void FastApply2(FunctorCollectionType& funs, unsigned int* leafIndexes) const {
	
		for (int i = 0; i < static_cast<int>(m_trees.size()); ++i)
			leafIndexes[i] = m_trees[i]->FastApply(funs[i]);
	}

    
    // IO operators
	/** @brief serialize this forest */
    void Serialize(std::ostream& o) const;
    
	/** @brief deserialize forest */
    static std::auto_ptr< Forest<W,S> > Deserialize(std::istream& in);
    
private:
    
    std::vector<TreeType*> m_trees;
    
};


//
// Implementations
// 

template <class W, class S>
void Forest<W,S>::Serialize(std::ostream& o) const {

	unsigned int numTrees = static_cast<unsigned int>( m_trees.size() );
	o.write((const char*)(&numTrees), sizeof(unsigned int) );

	for (unsigned int i = 0; i < numTrees; ++i)
		m_trees[i]->Serialize(o);
}

template <class W, class S>
std::auto_ptr< Forest<W,S> > Forest<W,S>::Deserialize(std::istream& in) {

	std::auto_ptr< Forest<W,S> > forest( new Forest<W,S> );

	unsigned int numTrees = 0;
	in.read((char*)(&numTrees), sizeof(unsigned int));

	for (unsigned int i = 0; i < numTrees; ++i) {
		std::auto_ptr< Tree<W,S> > tree = Tree<W,S>::Deserialize(in);
		tree->CheckValid();
		forest->AddTree(tree);
	}

	return forest;
}

    
} } }


#endif
