//
//  CRtester.h
//  FISH
//
//  Created by Yaozong Gao on 2/19/15.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __CRtester_h__
#define __CRtester_h__

#include "forest/wl/MemoryAxisAlignedWeakLearner.h"
#include "forest/stat/CRStatisticsAggregator.h"


namespace BRIC { namespace IDEA { namespace FISH {


/* Joint classification and regression tester */
class CRtester
{
public:

	typedef MemoryAxisAlignedWeakLearner W;
	typedef CRStatisticsAggregator S;
	typedef Tree<W, S> TreeType;
	typedef Forest<W, S> ForestType;

	template <typename FunctorCollectionType>
	static void Predict(const ForestType* forest, FunctorCollectionType& fun, double* labelout, double* dispout);
};


template <typename FunctorCollectionType>
void CRtester::Predict(const ForestType* forest, FunctorCollectionType& funs, double* labelout, double* dispout)
{
	if (forest == NULL || forest->GetTreeNumber() == 0) {
		std::cerr << "cannot evaluate using empty forest!" << std::endl;
		assert(false);
		exit(-1);
	}

	unsigned int treeNumber = forest->GetTreeNumber();
	const CRtester::S& tmp_stat = forest->GetTree(0)->GetAnyLeafStatisticsAggregator();
	unsigned int target_dimension = tmp_stat.GetTargetDimension();
	unsigned int class_num = tmp_stat.GetClassNumber();

	::memset(labelout, 0, sizeof(double) * class_num);
	::memset(dispout, 0, sizeof(double) * target_dimension);

	std::vector<double> mean;
	mean.resize(target_dimension, 0);

	for (unsigned int i = 0; i < treeNumber; ++i)
	{
		const CRtester::S& stat = forest->GetTree(i)->FastApply(funs[i]);

		stat.GetMean(&mean[0]);

		for (unsigned int j = 0; j < target_dimension; ++j)
			dispout[j] += mean[j] / treeNumber;

		for (unsigned int j = 0; j < class_num; ++j)
			labelout[j] += stat.GetProbability(j);
	}

	double sum = 0;
	for (unsigned int j = 0; j < class_num; ++j)
		sum += labelout[j];

	if (std::abs(sum) <= 1e-6) {
		std::cerr << "all zero probabilities?" << std::endl; exit(-1);
	}

	for (unsigned int j = 0; j < class_num; ++j)
		labelout[j] /= sum;
}



} } }


#endif