//
//  SimpleRegressorIndex.h
//  FISH
//
//  Created by Yaozong Gao on 6/9/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __SimpleRegressorIndex_h__
#define __SimpleRegressorIndex_h__

#include "forest/wl/MemoryAxisAlignedWeakLearner.h"
#include "forest/stat/SimpleRegressionIndexStatisticsAggregator.h"
#include "forest/Forest.h"
#include "common/aux_func.h"

namespace BRIC { namespace IDEA { namespace FISH {

/** @brief simple regression */
class SimpleRegressorIndex
{
public:

	typedef MemoryAxisAlignedWeakLearner W;
	typedef SimpleRegressionIndexStatisticsAggregator S;
	typedef Tree<W, S> TreeType;
	typedef Forest<W, S> ForestType;

	template <typename FunctorType>
	static void FindLeafIndices(const ForestType* forest, FunctorType& fun, int* leaf_indices);

};

template <typename FunctorType>
void SimpleRegressorIndex::FindLeafIndices(const ForestType* forest, FunctorType& fun, int* leaf_indices)
{
	if (forest == NULL || forest->GetTreeNumber() == 0) {
		std::cerr << "cannot evaluate using empty forest!" << std::endl;
		assert(false);
		exit(-1);
	}

	unsigned int treeNumber = forest->GetTreeNumber();
	::memset(leaf_indices, 0, sizeof(int) * treeNumber);

	for (unsigned int i = 0; i < treeNumber; ++i)
	{
		const SimpleRegressorIndex::S& stat = forest->GetTree(i)->FastApply(fun);
		leaf_indices[i] = stat.leaf_index;
	}
}


} } }

#endif

