//
//  MultiDimensionRegressor.h
//  FISH
//
//  Created by Yaozong Gao on 6/25/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __MultiDimensionRegressor_h__
#define __MultiDimensionRegressor_h__

#include "forest/wl/MemoryAxisAlignedWeakLearner.h"
#include "forest/stat/MultiDimensionStatisticsAggregator.h"
#include "forest/Forest.h"

namespace BRIC { namespace IDEA { namespace FISH {


/** @brief multi-dimension regression */
template <int Dimension>
class MultiDimensionRegressor
{
public:

	typedef MemoryAxisAlignedWeakLearner W;
	typedef MultiDimensionStatisticsAggregator<Dimension> S;
	typedef Tree<W,S> TreeType;
	typedef Forest<W,S> ForestType;
	typedef boost::array<double,Dimension> ArrayType;

	/** @brief use functor to compute features on the fly
	 *
	 * functor should support operator(int) with the input featureIndex.
	 */
	template <typename FunctorType>
	static void Regress(const ForestType* forest, FunctorType& fun, ArrayType& out);

	template <typename FunctorCollectionType>
	static void Regress2(const ForestType* forest, FunctorCollectionType& funs, ArrayType& out);

	template <typename FunctorCollectionType>
	static void Regress2_weighted(const ForestType* forest, FunctorCollectionType& funs, ArrayType& out);

};


template <int Dimension> template <typename FunctorType>
void MultiDimensionRegressor<Dimension>::Regress(const ForestType* forest, FunctorType& fun, ArrayType& out) 
{
	if (forest == NULL || forest->GetTreeNumber() == 0) {
		std::cerr << "cannot evaluate using empty forest!" << std::endl;
		assert(false);
		exit(-1);
	}

	unsigned int treeNumber = forest->GetTreeNumber();	
	out.assign(0);
	
	//////////////////////////////////////////////////////////////////////////
	// non-weight average
	ArrayType mean;
	for (unsigned int i = 0; i < treeNumber; ++i)
	{
		const S& stat = forest->GetTree(i)->FastApply(fun);
		stat.GetMean(mean);

		for (int j = 0; j < Dimension; ++j)
			out[j] += mean[j] / treeNumber;
	}
}

template <int Dimension> template <typename FunctorCollectionType>
void MultiDimensionRegressor<Dimension>::Regress2(const ForestType* forest, FunctorCollectionType& funs, ArrayType& out) 
{
	if (forest == NULL || forest->GetTreeNumber() == 0) {
		std::cerr << "cannot evaluate using empty forest!" << std::endl;
		assert(false);
		exit(-1);
	}

	unsigned int treeNumber = forest->GetTreeNumber();
	out.assign(0);

	////////////////////////////////////////////////////////////////////////
	// non-weight average
	ArrayType mean;
	for (unsigned int i = 0; i < treeNumber; ++i)
	{
		const S& stat = forest->GetTree(i)->FastApply(funs[i]);
		stat.GetMean(mean);

		for (int j = 0; j < Dimension; ++j)
			out[j] += mean[j] / treeNumber;
	}
}

template <int Dimension> template <typename FunctorCollectionType>
void MultiDimensionRegressor<Dimension>::Regress2_weighted(const ForestType* forest, FunctorCollectionType& funs, ArrayType& out)
{
	if (forest == NULL || forest->GetTreeNumber() == 0) {
		std::cerr << "cannot evaluate using empty forest!" << std::endl;
		assert(false);
		exit(-1);
	}

	unsigned int treeNumber = forest->GetTreeNumber();
	out.assign(0);

	//////////////////////////////////////////////////////////////////////////
	// weighted average (which shows slightly better performances)
	ArrayType var_sum, tmp;
	var_sum.fill(0);
	for (unsigned int i = 0; i < treeNumber; ++i)
	{
		const S& stat = forest->GetTree(i)->FastApply(funs[i]);
		stat.GetVariances(tmp);

		for (int j = 0; j < Dimension; ++j)
			var_sum[j] += tmp[j];
	}

	ArrayType mean;
	for (unsigned int i = 0; i < treeNumber; ++i)
	{
		const S& stat = forest->GetTree(i)->FastApply(funs[i]);
		stat.GetMean(mean);
		stat.GetVariances(tmp);

		// compute weight for each tree
		for (int j = 0; j < Dimension; ++j) {

			if ( std::abs(var_sum[j]) <= VECT_EPSILON )
			{
				tmp[j] = 1.0 / treeNumber;
			}
			else 
			{
				tmp[j] = (var_sum[j] - tmp[j]) / (var_sum[j] * static_cast<double>(treeNumber - 1));
			}
		}

		for (int j = 0; j < Dimension; ++j)
			out[j] += mean[j] * tmp[j];
	}

}


} } }

#endif

