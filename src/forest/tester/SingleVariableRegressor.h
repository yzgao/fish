//
//  SingleVariableRegressor.h
//  FISH
//
//  Created by Yaozong Gao on 6/25/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __SingleVariableRegressor_h__
#define __SingleVariableRegressor_h__


#include "forest/wl/MemoryAxisAlignedWeakLearner.h"
#include "forest/stat/SingleVariableStatisticsAggregator.h"
#include "forest/Forest.h"


namespace BRIC { namespace IDEA { namespace FISH {


/** @brief forest tester for single variable regression
 */
class SingleVariableRegressor
{
public:

	typedef MemoryAxisAlignedWeakLearner W;
	typedef SingleVariableStatisticsAggregator S;
	typedef Forest<W, S> ForestType;
	typedef Tree<W, S> TreeType;

	/** @brief estimate by average 
	 *
	 * @param FunctorType functor that is used to compute features
	 */
	template <class FunctorType>
	static double Regress(const ForestType* forest, const FunctorType& fun) {

		if (forest == NULL || forest->GetTreeNumber() == 0) {
			std::cerr << "cannot evaluate using empty forest!" << std::endl;
			assert(false);
			exit(-1);
		}

		// equally average of the predicted results from each tree
		double mean = 0;
		for (unsigned int i = 0; i < forest->GetTreeNumber(); ++i) {
			const S& statistics = forest->GetTree(i)->FastApply(fun);
			mean += statistics.GetMean();
		}

		mean /= forest->GetTreeNumber();
		return mean;
	}


};

} } }

#endif