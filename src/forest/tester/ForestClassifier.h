//
//  ForestClassifier.h
//  FISH
//
//  Created by Yaozong Gao on 6/25/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __ForestClassifier_h__
#define __ForestClassifier_h__

#include "forest/wl/MemoryAxisAlignedWeakLearner.h"
#include "forest/stat/HistogramStatisticsAggregator.h"
#include "forest/Forest.h"

namespace BRIC { namespace IDEA { namespace FISH {

/** @brief multi-class classification forest tester */
class ForestClassifier
{
public:
    
	typedef MemoryAxisAlignedWeakLearner W;
	typedef HistogramStatisticsAggregator S;
    typedef Forest<W, S> ForestType;
    typedef Tree<W, S> TreeType;
    
	/** @brief estimate probability 
	 *
	 * @param FunctorType functor that is used to retrieve the feature when needed
	 */
	template <class FunctorType>
    static void Classify(const ForestType* forest, FunctorType& fun, std::vector<double>& probs);

	template <class FunctorCollectionType>
	static void Classify2(const ForestType* forest, FunctorCollectionType& funs, std::vector<double>& probs);
};


template <class FunctorType>
void ForestClassifier::Classify(const ForestType* forest, FunctorType& fun, std::vector<double>& probs) {

	if (forest == NULL || forest->GetTreeNumber() == 0) {
		std::cerr << "cannot evaluate using empty forest!" << std::endl;
		assert(false);
		exit(-1);
	}

	// accumulate PDF of each tree
	for (unsigned int i = 0; i < forest->GetTreeNumber(); ++i) {
		const S& stat = forest->GetTree(i)->FastApply(fun);
		if (i == 0) {
			probs.resize(stat.GetClassNumber());
			for (unsigned int j = 0; j < probs.size(); ++j)
				probs[j] = stat.GetProbability(j);
		}else {
			for (unsigned int j = 0; j < probs.size(); ++j)
				probs[j] += stat.GetProbability(j);
		}
	}

	// normalize to a PDF
	double sum = 0;
	for (unsigned int i = 0; i < probs.size(); ++i)
		sum += probs[i];
	if(std::abs(sum) <= 1e-6) {
		std::cerr << "all zero probabilities?" << std::endl; exit(-1);
	}

	for (unsigned int i = 0; i < probs.size(); ++i) 
		probs[i] /= sum;
}  

template <class FunctorCollectionType>
void ForestClassifier::Classify2(const ForestType* forest, FunctorCollectionType& funs, std::vector<double>& probs) {

	if (forest == NULL || forest->GetTreeNumber() == 0) {
		std::cerr << "cannot evaluate using empty forest!" << std::endl;
		assert(false);
		exit(-1);
	}

	// accumulate PDF of each tree
	for (unsigned int i = 0; i < forest->GetTreeNumber(); ++i) {
		const S& stat = forest->GetTree(i)->FastApply(funs[i]);
		if (i == 0) {
			probs.resize(stat.GetClassNumber());
			for (unsigned int j = 0; j < probs.size(); ++j)
				probs[j] = stat.GetProbability(j);
		}else {
			for (unsigned int j = 0; j < probs.size(); ++j)
				probs[j] += stat.GetProbability(j);
		}
	}

	// normalize to a PDF
	double sum = 0;
	for (unsigned int i = 0; i < probs.size(); ++i)
		sum += probs[i];
	if(std::abs(sum) <= 1e-6) {
		std::cerr << "all zero probabilities?" << std::endl; exit(-1);
	}

	for (unsigned int i = 0; i < probs.size(); ++i) 
		probs[i] /= sum;
}

} } }

#endif