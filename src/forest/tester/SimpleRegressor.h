//
//  SimpleRegressor.h
//  FISH
//
//  Created by Yaozong Gao on 5/14/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __SimpleRegressor_h__
#define __SimpleRegressor_h__

#include "forest/wl/MemoryAxisAlignedWeakLearner.h"
#include "forest/stat/SimpleRegressionStatisticsAggregator.h"
#include "forest/Forest.h"
#include "common/aux_func.h"

namespace BRIC { namespace IDEA { namespace FISH {

/** @brief simple regression */
class SimpleRegressor
{
public:

	enum FusionType {
		EqualWeighted,
		SubtractSumWeighted,
		InverseWeighted,
	};

	typedef MemoryAxisAlignedWeakLearner W;
	typedef SimpleRegressionStatisticsAggregator S;
	typedef Tree<W, S> TreeType;
	typedef Forest<W, S> ForestType;

	/** @brief use functor to compute features on the fly
	*
	* functor should support operator(int) with the input featureIndex.
	*/
	template <typename FunctorType>
	static void Regress(const ForestType* forest, FunctorType& fun, double* out);

	template <typename FunctorCollectionType>
	static void Regress2_equal(const ForestType* forest, FunctorCollectionType& funs, double* out);

	template <typename FunctorCollectionType>
	static void Regress2_weighted(const ForestType* forest, FunctorCollectionType& funs, double* out);

	template <typename FunctorCollectionType>
	static void Regress2_weighted2(const ForestType* forest, FunctorCollectionType& funs, double* out);

	template <typename FunctorCollectionType>
	static void Regress2(const ForestType* forest, FunctorCollectionType& funs, SimpleRegressor::FusionType type, double* out);

};

template <typename FunctorType>
void SimpleRegressor::Regress(const ForestType* forest, FunctorType& fun, double* out)
{
	if (forest == NULL || forest->GetTreeNumber() == 0) {
		std::cerr << "cannot evaluate using empty forest!" << std::endl;
		assert(false);
		exit(-1);
	}

	unsigned int treeNumber = forest->GetTreeNumber();
	const SimpleRegressor::S& tmp_stat = forest->GetTree(0)->GetAnyLeafStatisticsAggregator();
	unsigned int target_dimension = tmp_stat.GetTargetDimension();

	::memset(out, 0, sizeof(double) * target_dimension);

	std::vector<double> mean;
	mean.resize(target_dimension);

	for (unsigned int i = 0; i < treeNumber; ++i)
	{
		const SimpleRegressor::S& stat = forest->GetTree(i)->FastApply(fun);
		stat.GetMean(&mean[0]);

		for (unsigned int j = 0; j < target_dimension; ++j)
			out[j] += mean[j] / treeNumber;
	}
}

template <typename FunctorCollectionType>
void SimpleRegressor::Regress2_equal(const ForestType* forest, FunctorCollectionType& funs, double* out)
{
	if (forest == NULL || forest->GetTreeNumber() == 0) {
		std::cerr << "cannot evaluate using empty forest!" << std::endl;
		assert(false);
		exit(-1);
	}

	unsigned int treeNumber = forest->GetTreeNumber();
	typedef SimpleRegressionStatisticsAggregator S;
	const SimpleRegressor::S& tmp_stat = forest->GetTree(0)->GetAnyLeafStatisticsAggregator();
	unsigned int target_dimension = tmp_stat.GetTargetDimension();

	::memset(out, 0, sizeof(double)* target_dimension);

	std::vector<double> mean;
	mean.resize(target_dimension);

	for (unsigned int i = 0; i < treeNumber; ++i)
	{
		const SimpleRegressor::S& stat = forest->GetTree(i)->FastApply(funs[i]);
		stat.GetMean(&mean[0]);

		for (unsigned int j = 0; j < target_dimension; ++j)
			out[j] += mean[j] / treeNumber;
	}
}

template <typename FunctorCollectionType>
void SimpleRegressor::Regress2_weighted(const ForestType* forest, FunctorCollectionType& funs, double* out)
{
	if (forest == NULL || forest->GetTreeNumber() == 0) {
		std::cerr << "cannot evaluate using empty forest!" << std::endl;
		assert(false);
		exit(-1);
	}

	unsigned int treeNumber = forest->GetTreeNumber();
	typedef SimpleRegressionStatisticsAggregator S;
	const SimpleRegressor::S& tmp_stat = forest->GetTree(0)->GetAnyLeafStatisticsAggregator();
	unsigned int target_dimension = tmp_stat.GetTargetDimension();

	::memset(out, 0, sizeof(double)* target_dimension);

	std::vector< std::vector<double> > means;
	std::vector< std::vector<double> > vars;
	means.resize(treeNumber);
	vars.resize(treeNumber);

	for (unsigned int i = 0; i < treeNumber; ++i)
	{
		const SimpleRegressor::S& stat = forest->GetTree(i)->FastApply(funs[i]);

		means[i].resize(target_dimension);
		vars[i].resize(target_dimension);

		stat.GetMean(&(means[i][0]));
		stat.GetVariance(&(vars[i][0]));
	}

	std::vector<double> sum_var;
	sum_var.resize( target_dimension );
	::memset(&sum_var[0], 0, sizeof(double) * target_dimension);

	for (unsigned int i = 0; i < treeNumber; ++i)
	{
		for (unsigned int j = 0; j < target_dimension; ++j)
			sum_var[j] += vars[i][j];
	}

	::memset(out, 0, sizeof(double) * target_dimension);

	std::vector<double> weights;
	weights.resize(target_dimension);

	for (unsigned int i = 0; i < treeNumber; ++i)
	{
		// compute weight for each tree
		for (unsigned int j = 0; j < target_dimension; ++j) 
		{
			if (std::abs(sum_var[j]) <= VECT_EPSILON)
				weights[j] = 1.0 / treeNumber;
			else
				weights[j] = (sum_var[j] - vars[i][j]) / (sum_var[j] * static_cast<double>(treeNumber - 1));
		}

		// weighted average
		for (unsigned int j = 0; j < target_dimension; ++j)
			out[j] += means[i][j] * weights[j];
	}
}


template <typename FunctorCollectionType>
void SimpleRegressor::Regress2_weighted2(const ForestType* forest, FunctorCollectionType& funs, double* out)
{
	if (forest == NULL || forest->GetTreeNumber() == 0) {
		std::cerr << "cannot evaluate using empty forest!" << std::endl;
		assert(false);
		exit(-1);
	}

	unsigned int treeNumber = forest->GetTreeNumber();
	typedef SimpleRegressionStatisticsAggregator S;
	const SimpleRegressor::S& tmp_stat = forest->GetTree(0)->GetAnyLeafStatisticsAggregator();
	unsigned int target_dimension = tmp_stat.GetTargetDimension();

	::memset(out, 0, sizeof(double)* target_dimension);

	std::vector< std::vector<double> > means;
	std::vector< std::vector<double> > vars;
	means.resize(treeNumber);
	vars.resize(treeNumber);

	for (unsigned int i = 0; i < treeNumber; ++i)
	{
		const SimpleRegressor::S& stat = forest->GetTree(i)->FastApply(funs[i]);

		means[i].resize(target_dimension);
		vars[i].resize(target_dimension);

		stat.GetMean(&(means[i][0]));
		stat.GetVariance(&(vars[i][0]));
	}

	std::vector < std::vector<double> > weights;
	std::vector< double > sums;
	weights.resize(treeNumber);

	sums.resize(target_dimension);
	::memset(&sums[0], 0, sizeof(double) * target_dimension);

	for (unsigned int i = 0; i < treeNumber; ++i)
	{
		weights[i].resize(target_dimension);
		for (unsigned int j = 0; j < target_dimension; ++j)
		{
			if (std::abs(vars[i][j]) < 1e-6)
				weights[i][j] = 1e6;
			else
				weights[i][j] = 1.0 / std::abs(vars[i][j]);
			
			sums[j] += weights[i][j];
		}
	}

	for (unsigned int i = 0; i < treeNumber; ++i)
	{
		for (unsigned int j = 0; j < target_dimension; ++j)
			weights[i][j] /= sums[j];
	}

	::memset(out, 0, sizeof(double) * target_dimension);
	for (unsigned int i = 0; i < treeNumber; ++i)
	{
		// weighted average
		for (unsigned int j = 0; j < target_dimension; ++j)
			out[j] += means[i][j] * weights[i][j];
	}
}

template <typename FunctorCollectionType>
void SimpleRegressor::Regress2(const ForestType* forest, FunctorCollectionType& funs, SimpleRegressor::FusionType type, double* out)
{
	switch (type)
	{
	case EqualWeighted: Regress2_equal(forest, funs, out);  break;
	case SubtractSumWeighted: Regress2_weighted(forest, funs, out); break;
	case InverseWeighted: Regress2_weighted2(forest, funs, out); break;
	default: err_message("unknown fusion type");
	}
}

} } }

#endif

