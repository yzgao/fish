//
//  CRStatisticsAggregator.h
//  FISH
//
//  Created by Yaozong Gao on 1/7/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __CRStatisticsAggregator_h__
#define __CRStatisticsAggregator_h__

#include "forest/interfaces.h"
#include "common/vect.h"
#include "common/aux_func.h"
#include <fstream>

namespace BRIC { namespace IDEA { namespace FISH {

class CRStatisticsAggregator
{
public:
	typedef CRStatisticsAggregator Self;

	// constructors
	CRStatisticsAggregator();
	CRStatisticsAggregator(unsigned int dimension, unsigned int classNumber);
	CRStatisticsAggregator(const Self& stat);
	// destructor
	~CRStatisticsAggregator();

	// public interfaces
	inline void Clear();
	inline unsigned int GetSampleNumber() const;
	inline void Aggregate(const IDataCollection* dataCollection, unsigned int dataIdx);
	inline void Aggregate(const Self& aggregator);
	inline Self DeepClone() const;

	// override operators
	inline bool operator==(const Self& stat) const;
	inline Self& operator=(const Self& stat);

	// regression interfaces
	inline double MeanVariance() const;
	inline unsigned int GetTargetDimension() const;
	inline void GetMean(double* mean) const;
	inline void GetVariance(double* vars) const;

	// classification interfaces
	inline double Entropy() const;
	inline unsigned int GetClassNumber() const;
	inline int MAPEstimate() const;
	inline double GetProbability(int label) const;

	// IO Operators
	void Serialize(std::ostream& o) const;
	void Deserialize(std::istream& in);

	static const char* BinaryHeader;

private:

	// sample number
	unsigned int m_num;

	// regression
	unsigned int m_dimension;
	double* m_sum;
	double* m_sqsum;

	// classification
	unsigned int* m_histogram;
	unsigned int m_classNumber;

};

//////////////////////////////////////////////////////////////////////////

const char* CRStatisticsAggregator::BinaryHeader = "BRIC.IDEA.RegressionForest.CRStatisticsAggregator";

CRStatisticsAggregator::CRStatisticsAggregator() : m_num(0), m_dimension(0), m_sum(NULL), m_sqsum(NULL), m_histogram(NULL), m_classNumber(0) {}

CRStatisticsAggregator::CRStatisticsAggregator(unsigned int dimension, unsigned int classNumber) : m_num(0), m_dimension(dimension), m_classNumber(classNumber) {

	assert_message(dimension != 0 && classNumber != 0, "empty target or histograms ?");

	// regression
	unsigned int buffersize = dimension * 2;
	m_sum = (double *)malloc(sizeof(double) * buffersize);
	m_sqsum = m_sum + m_dimension;
	::memset(m_sum, 0, sizeof(double) * buffersize);

	// classification
	m_histogram = (unsigned int*)malloc(sizeof(unsigned int)* m_classNumber);
	::memset(m_histogram, 0, sizeof(unsigned int)* m_classNumber);
}

CRStatisticsAggregator::CRStatisticsAggregator(const CRStatisticsAggregator& stat) {

	m_num = stat.m_num;
	m_dimension = stat.m_dimension;
	m_classNumber = stat.m_classNumber;

	// regression
	if (stat.m_dimension == 0)
		m_sum = m_sqsum = NULL;
	else {
		unsigned int bufferSize = stat.m_dimension * 2;
		m_sum = (double *)malloc(sizeof(double) * bufferSize);
		m_sqsum = m_sum + m_dimension;
		::memcpy(m_sum , stat.m_sum, sizeof(double) * bufferSize);
	}

	// classification
	if (stat.m_classNumber == 0)
		m_histogram = NULL;
	else {
		m_histogram = (unsigned int*)malloc(sizeof(unsigned int) * stat.m_classNumber);
		::memcpy(m_histogram, stat.m_histogram, sizeof(unsigned int) * stat.m_classNumber);
	}
}

CRStatisticsAggregator::~CRStatisticsAggregator() {

	m_num = 0;

	if (m_sum != NULL) {
		free(m_sum);
		m_sum = m_sqsum = NULL;
	}
	m_dimension = 0;

	if (m_histogram != NULL) {
		free(m_histogram);
		m_histogram = NULL;
	}
	m_classNumber = 0;
}

void CRStatisticsAggregator::Clear() {

	m_num = 0;
	::memset(m_sum, 0, sizeof(double) * m_dimension * 2);
	::memset(m_histogram, 0, sizeof(unsigned int) * m_classNumber);
}

unsigned int CRStatisticsAggregator::GetSampleNumber() const {

	return m_num;
}

double CRStatisticsAggregator::Entropy() const {

	if (m_num == 0)
		return 0.0;

	double result = 0.0;
	for (unsigned int i = 0; i < m_classNumber; ++i)
	{
		double p = (double)m_histogram[i] / (double)m_num;
		result -= (p == 0.0 ? 0.0 : p * log(p));
	}

	return result;
}

double CRStatisticsAggregator::MeanVariance() const
{
	double mean_var = 0;

	for (unsigned int i = 0; i < m_dimension; ++i)
	{
		double mean = m_sum[i] / m_num;
		double var = m_sqsum[i] / m_num - mean * mean;
		if (var < 0) var = 0;
		mean_var += sqrt(var);
	}
	mean_var /= m_dimension;

	return mean_var;
}

void CRStatisticsAggregator::Aggregate(const IDataCollection* dataCollection, unsigned int dataIdx) {

	const double* t = dataCollection->GetTarget(dataIdx);
	int label = dataCollection->GetLabel(dataIdx);

	// update sample sum for regression
	for (unsigned int i = 0; i < m_dimension; ++i)
	{
		m_sum[i] += t[i];
		m_sqsum[i] += t[i] * t[i];
	}

	// update histogram for classification
	if (label < 0 || label >= static_cast<int>(m_classNumber)) {
		std::cerr << "label index out of boundary!" << std::endl; exit(-1);
	}
	++m_histogram[label];

	++m_num;
}

void CRStatisticsAggregator::Aggregate(const CRStatisticsAggregator& stat) {

	// update m_sum for regression
	for (unsigned int i = 0; i < m_dimension; ++i) {
		m_sum[i] += stat.m_sum[i];
		m_sqsum[i] += stat.m_sqsum[i];
	}

	// update histogram for classification
	if (m_classNumber != stat.m_classNumber) {
		std::cerr << "numbers of classes are inconsistent" << std::endl; exit(-1);
	}

	for (unsigned int i = 0; i < m_classNumber; ++i)
		m_histogram[i] += stat.m_histogram[i];

	m_num += stat.m_num;
}

CRStatisticsAggregator CRStatisticsAggregator::DeepClone() const {

	CRStatisticsAggregator copy(*this);
	return copy;
}

bool CRStatisticsAggregator::operator==(const CRStatisticsAggregator& stat) const {

	if (stat.m_num != m_num || stat.m_dimension != m_dimension || m_classNumber != stat.m_classNumber)
		return false;

	// for regression
	for (unsigned int i = 0; i < m_dimension; ++i) {
		if (std::abs(m_sum[i] - stat.m_sum[i]) >= VECT_EPSILON || std::abs(m_sqsum[i] - stat.m_sqsum[i]) >= VECT_EPSILON)
			return false;
	}

	// for classification
	for (unsigned int i = 0; i < m_classNumber; ++i) {
		if (m_histogram[i] != stat.m_histogram[i])
			return false;
	}

	return true;
}

CRStatisticsAggregator& CRStatisticsAggregator::operator=(const CRStatisticsAggregator& stat) {

	if (this == &stat)
		return *this;

	m_num = stat.m_num;

	// for regression
	if (stat.m_dimension == 0)
	{
		m_dimension = 0;
		if (m_sum != NULL)
			free(m_sum);
		m_sum = m_sqsum = NULL;
	}
	else {
		unsigned int bufferSize = stat.m_dimension * 2;
		if (m_dimension == stat.m_dimension) {
			::memcpy(m_sum, stat.m_sum, sizeof(double)* bufferSize);
		}
		else {
			m_dimension = stat.m_dimension;
			if (m_sum != NULL)
				free(m_sum);
			m_sum = (double*)malloc(sizeof(double) * bufferSize);
			m_sqsum = m_sum + m_dimension;
			::memcpy(m_sum, stat.m_sum, sizeof(double)* bufferSize);
		}
	}

	// for classification
	if (stat.m_classNumber == 0)
	{
		m_classNumber = 0;
		if (m_histogram != NULL)
			free(m_histogram);
		m_histogram = NULL;
	}
	else {

		if (m_classNumber == stat.m_classNumber)
			::memcpy(m_histogram, stat.m_histogram, sizeof(unsigned int) * stat.m_classNumber);
		else {
			if (m_histogram != NULL)
				free(m_histogram);
			m_histogram = (unsigned int*)malloc(sizeof(unsigned int) * stat.m_classNumber);
			::memcpy(m_histogram, stat.m_histogram, sizeof(unsigned int)* stat.m_classNumber);
			m_classNumber = stat.m_classNumber;
		}
	}

	return *this;
}

unsigned int CRStatisticsAggregator::GetTargetDimension() const {
	return m_dimension;
}

void CRStatisticsAggregator::GetMean(double* mean) const {
	
	for (unsigned int i = 0; i < m_dimension; ++i)
		mean[i] = m_sum[i] / m_num;
}

void CRStatisticsAggregator::GetVariance(double* vars) const
{
	for (unsigned int i = 0; i < m_dimension; ++i)
	{
		double mean = m_sum[i] / m_num;
		vars[i] = m_sqsum[i] / m_num - mean * mean;
	}
}

unsigned int CRStatisticsAggregator::GetClassNumber() const {
	return m_classNumber;
}

int CRStatisticsAggregator::MAPEstimate() const {

	if (m_classNumber == 0 || m_num == 0) {
		std::cerr << "empty aggregator!" << std::endl;	exit(-1);
	}

	int maxLabel = -1;
	unsigned int maxVote = 0;
	for (int i = 0; i < static_cast<int>(m_classNumber); ++i) {

		if (m_histogram[i] > maxVote) {
			maxVote = m_histogram[i];
			maxLabel = i;
		}
	}

	assert(maxLabel != -1);
	return maxLabel;
}

double CRStatisticsAggregator::GetProbability(int label) const {

	if (label < 0 || label >= static_cast<int>(m_classNumber)) {
		std::cerr << "label out of index" << std::endl;	exit(-1);
	}

	return m_histogram[label] * 1.0 / m_num;
}

void CRStatisticsAggregator::Serialize(std::ostream& o) const {

	o.write((const char*)(&m_num), sizeof(unsigned int));

	// for regression
	o.write((const char*)(&m_dimension), sizeof(unsigned int));
	if (m_dimension != 0)
		o.write((const char*)(m_sum), sizeof(double)* m_dimension * 2);

	// for classification
	o.write((const char*)(&m_classNumber), sizeof(unsigned int));
	if (m_classNumber!= 0)
		o.write((const char*)(m_histogram), sizeof(unsigned int)* m_classNumber);
}

std::ostream& operator<<(std::ostream& os, const CRStatisticsAggregator& stat)
{
	os << "(" << stat.Entropy() << "," << stat.MeanVariance() << ")";
	return os;
}

void CRStatisticsAggregator::Deserialize(std::istream& in) {

	in.read((char*)(&m_num), sizeof(unsigned int));

	// for regression
	in.read((char*)(&m_dimension), sizeof(unsigned int));

	if (m_sum != NULL) {
		free(m_sum);
		m_sum = m_sqsum = NULL;
	}

	if (m_dimension != 0)
	{
		unsigned int buffersize = m_dimension * 2;
		m_sum = (double *)malloc(sizeof(double) * buffersize);
		m_sqsum = m_sum + m_dimension;
		in.read((char *)(m_sum), sizeof(double)* buffersize);
	}

	// for classification
	in.read((char*)(&m_classNumber), sizeof(unsigned int));

	if (m_histogram != NULL) {
		free(m_histogram);
		m_histogram = NULL;
	}

	if (m_classNumber != 0) {
		m_histogram = (unsigned int*)malloc(sizeof(unsigned int) * m_classNumber);
		in.read((char*)(m_histogram), sizeof(unsigned int) * m_classNumber);
	}
}

} } }


#endif