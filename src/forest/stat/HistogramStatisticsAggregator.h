//
//  HistogramStatisticsAggregator.h
//  FISH
//
//  Created by Yaozong Gao on 03/29/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __HistogramStatisticsAggregator_h__
#define __HistogramStatisticsAggregator_h__


#include <vector>
#include <fstream>
#include <iostream>
#include "forest/interfaces.h"

namespace BRIC { namespace IDEA { namespace FISH {


class HistogramStatisticsAggregator 
{
public:

	/** @brief constructors and destructor */
	HistogramStatisticsAggregator();
	HistogramStatisticsAggregator(int classNumber);
	HistogramStatisticsAggregator(const HistogramStatisticsAggregator& copy);
	~HistogramStatisticsAggregator();

	/** @brief common interfaces */
	inline void Clear();
	inline HistogramStatisticsAggregator DeepClone() const;
	inline void Aggregate(const IDataCollection* dataCollection, unsigned int dataIdx);
	inline void Aggregate(const HistogramStatisticsAggregator& stat);
	inline double Entropy() const;
	inline unsigned int GetSampleNumber() const;

	/** @brief operators */
	bool operator==(const HistogramStatisticsAggregator& stat) const;
	HistogramStatisticsAggregator& operator=(const HistogramStatisticsAggregator& stat);

	/** @brief IO operators */
	void Serialize(std::ostream& o) const;
	void Deserialize(std::istream& in);

	/** @brief other interfaces */
	inline unsigned int GetClassNumber() const;
	inline int MAPEstimate() const;
	inline double GetProbability(int label) const;

private:

	unsigned int* m_histogram;
	int m_classNumber;
	unsigned int m_sampleNumber;

public:

	static const char* BinaryHeader;
};

//////////////////////////////////////////////////////////////////////////

HistogramStatisticsAggregator::HistogramStatisticsAggregator(): m_histogram(NULL), m_classNumber(0), m_sampleNumber(0) {}

HistogramStatisticsAggregator::HistogramStatisticsAggregator(int classNumber): m_classNumber(classNumber), m_sampleNumber(0) {
	m_histogram = (unsigned int*)malloc(sizeof(unsigned int)* m_classNumber);
	::memset(m_histogram, 0, sizeof(unsigned int)* m_classNumber);
}

HistogramStatisticsAggregator::HistogramStatisticsAggregator(const HistogramStatisticsAggregator& copy): m_classNumber(copy.m_classNumber), m_sampleNumber(copy.m_sampleNumber) {
	m_histogram = (unsigned int*)malloc(sizeof(unsigned int) * m_classNumber);
	::memcpy(m_histogram, copy.m_histogram, sizeof(unsigned int) * m_classNumber);
}

HistogramStatisticsAggregator::~HistogramStatisticsAggregator() {
	m_classNumber = m_sampleNumber = 0;
	if (m_histogram != NULL)
		free(m_histogram);
}

void HistogramStatisticsAggregator::Clear() {
	::memset(m_histogram, 0, sizeof(unsigned int) * m_classNumber);
	m_sampleNumber = 0;
}

HistogramStatisticsAggregator HistogramStatisticsAggregator::DeepClone() const {
	return HistogramStatisticsAggregator(*this);
}

void HistogramStatisticsAggregator::Aggregate(const IDataCollection* dataCollection, unsigned int dataIdx) {
	// assume label is indexed from 0
	int label = dataCollection->GetLabel(dataIdx);

	if (label < 0 || label >= m_classNumber) {
		std::cerr << "label index out of boundary!" << std::endl; exit(-1);
	}

	++m_histogram[label];
	++m_sampleNumber;
}

void HistogramStatisticsAggregator::Aggregate(const HistogramStatisticsAggregator& stat) {

	if (m_classNumber != stat.m_classNumber) {
		std::cerr << "numbers of classes are inconsistent" << std::endl; exit(-1);
	}

	for (int i = 0; i < m_classNumber; ++i)
		m_histogram[i] += stat.m_histogram[i];

	m_sampleNumber += stat.m_sampleNumber;
}


double HistogramStatisticsAggregator::Entropy() const {

	if (m_sampleNumber == 0)
		return 0.0;

	double result = 0.0;
	for (int i = 0; i < m_classNumber; ++i)
	{
		double p = (double)m_histogram[i] / (double)m_sampleNumber;
		result -= (p == 0.0 ? 0.0 : p * log(p));
	}

	return result;
}

unsigned int HistogramStatisticsAggregator::GetSampleNumber() const {
	return m_sampleNumber;
}

bool HistogramStatisticsAggregator::operator==(const HistogramStatisticsAggregator& stat) const {

	if (m_sampleNumber != stat.m_sampleNumber || m_classNumber != stat.m_classNumber)
		return false;

	for (int i = 0; i < m_classNumber; ++i) {
		if (m_histogram[i] != stat.m_histogram[i])
			return false;
	}

	return true;
}

HistogramStatisticsAggregator& HistogramStatisticsAggregator::operator=(const HistogramStatisticsAggregator& stat) {

	if (this == &stat)
		return *this;

	m_sampleNumber = stat.m_sampleNumber;

	if (stat.m_classNumber == 0)
	{
		m_classNumber = 0;
		if (m_histogram != NULL)
			free(m_histogram);
		m_histogram = NULL;
	}
	else {

		if (m_classNumber == stat.m_classNumber)
			::memcpy(m_histogram, stat.m_histogram, sizeof(unsigned int) * stat.m_classNumber);
		else {
			if (m_histogram != NULL)
				free(m_histogram);
			m_histogram = (unsigned int*)malloc(sizeof(unsigned int) * stat.m_classNumber);
			::memcpy(m_histogram, stat.m_histogram, sizeof(unsigned int)* stat.m_classNumber);
			m_classNumber = stat.m_classNumber;
		}
	}

	return *this;
}

void HistogramStatisticsAggregator::Serialize(std::ostream& o) const {

	o.write((const char*)(&m_sampleNumber), sizeof(unsigned int));
	unsigned int numBins = static_cast<unsigned int>(m_classNumber);	// keep backward compatibility
	o.write((const char*)(&numBins), sizeof(unsigned int));
	if (numBins != 0) 
		o.write((const char*)(m_histogram), sizeof(unsigned int)* numBins);
}

void HistogramStatisticsAggregator::Deserialize(std::istream& in) {

	in.read((char*)(&m_sampleNumber), sizeof(unsigned int));
	unsigned int numBins = 0;
	in.read((char*)(&numBins), sizeof(unsigned int));
	m_classNumber = static_cast<int>(numBins);

	if (numBins != 0) {
		m_histogram = (unsigned int*)malloc(sizeof(unsigned int) * numBins);
		in.read((char*)(m_histogram), sizeof(unsigned int) * numBins);
	}
}

std::ostream& operator<<(std::ostream& os, const HistogramStatisticsAggregator& stat)
{
	os << stat.Entropy();
	return os;
}

unsigned int HistogramStatisticsAggregator::GetClassNumber() const {
	return m_classNumber;
}

int HistogramStatisticsAggregator::MAPEstimate() const {

	if (m_classNumber == 0 || m_sampleNumber == 0) {
		std::cerr << "empty aggregator!" << std::endl;	exit(-1);
	}

	int maxLabel = -1;
	unsigned int maxVote = 0;
	for (int i = 0; i < m_classNumber; ++i) {

		if (m_histogram[i] > maxVote) {
			maxVote = m_histogram[i];
			maxLabel = i;
		}
	}

	assert(maxLabel != -1);
	return maxLabel;
}


double HistogramStatisticsAggregator::GetProbability(int label) const {

	if (label < 0 || label >= m_classNumber) {
		std::cerr << "label out of index" << std::endl;	exit(-1);
	}
	return m_histogram[label] * 1.0 / m_sampleNumber;
}

const char* HistogramStatisticsAggregator::BinaryHeader = "BRIC.IDEA.RegressionForest.HistogramStatisticsAggregator";


} } }


#endif