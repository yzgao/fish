//
//  MultiDimensionStatisticsAggregator.h
//  FISH
//
//  Created by Yaozong Gao on 6/25/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __MultiDimensionStatisticsAggregator_h__
#define __MultiDimensionStatisticsAggregator_h__

#include "stdafx.h"

#include "forest/interfaces.h"
#include <fstream>


namespace BRIC { namespace IDEA { namespace FISH {


/** statistics aggregator with fixed dimension
 * 
 * Use matrix trace to measure the information gain in node splitting
 *
 * The main difference of this class with MultiVariableStatisticsAggregator is that:
 *
 * MultiVariableStatisticsAggregator determines target dimension in runtime (more flexible, less efficient)
 * MultiDimensionStatisticsAggregator determines target dimension in compile time (less flexible, more efficient)
 *
 */
template <int Dimension>
class MultiDimensionStatisticsAggregator
{
public:

	typedef MultiDimensionStatisticsAggregator<Dimension> Self;
    static const int RegressionDimension = Dimension;
    

	/** @brief constructor */
	MultiDimensionStatisticsAggregator() {
		m_num = 0;
		m_sum.assign(0);
		m_sumsq.assign(0);
	}

	/** @brief make samples empty and reset all statistics */
	void Clear() {
		m_num = 0;
		m_sum.assign(0);
		m_sumsq.assign(0);
	}

	/** @brief get sample number */
    inline unsigned int GetSampleNumber() const {
		return m_num;
	}
    
    // entropy
    /** @brief use matrix trace to measure entropy */
    double Entropy() const;
    
	/** @brief aggregate a training data */
    void Aggregate(const IDataCollection* dataCollection, unsigned int dataIdx);
    
	/** @brief aggregate another statistics aggregator */
    void Aggregate(const Self& stat);
    
	/** @brief deep clone a statistics aggregator object */
    Self DeepClone() const;
    
	/** @brief equality test
     * for correctness check during training procedure
     * override equal operator
	 */
    bool operator==(const Self& stat) const;
    
    // IO operators
	/** @brief serialize this object to ostream */
    void Serialize(std::ostream& o) const;
    
	/** @brief de-serialize this object from istream */
    void Deserialize(std::istream& in);
    
    //
    // class-specific member functions
    // 
    
	/** @brief get target dimension */
    inline unsigned int GetTargetDimension() const
	{
		return Dimension;
	}
    
	/** @brief get mean estimation */
    void GetMean(boost::array<double,Dimension>& mean) const;
    
	/** @brief get variance estimation */
    void GetVariances(boost::array<double,Dimension>& vars) const;

private:

	boost::array<double, Dimension> m_sum;
	boost::array<double, Dimension> m_sumsq;
	unsigned int m_num;

public:
	
	static const char* BinaryHeader;
};



// 
// Implementations
// 

template <int Dimension>
double MultiDimensionStatisticsAggregator<Dimension>::Entropy() const 
{
	boost::array<double,Dimension> mean;
	for (int i = 0; i < Dimension; ++i)
		mean[i] = m_sum[i] / m_num;

	boost::array<double,Dimension> var;
	for (int i = 0; i < Dimension; ++i) {
		var[i] = m_sumsq[i] / m_num - mean[i] * mean[i];

		if (var[i] < 0) var[i] = 0;
	}

	double stdev = 0;
	for (int i = 0; i < Dimension; ++i)
		stdev += sqrt(var[i]);
	stdev = stdev / Dimension;

	return stdev;
}


template <int Dimension>
void MultiDimensionStatisticsAggregator<Dimension>::Aggregate(const IDataCollection* dataCollection, unsigned int dataIdx) 
{
	const double* target = dataCollection->GetTarget(dataIdx);

	for (int i = 0; i < Dimension; ++i)
	{
		m_sum[i] += target[i];
		m_sumsq[i] += (target[i]*target[i]);
	}

	++ m_num;
}


template <int Dimension>
void MultiDimensionStatisticsAggregator<Dimension>::Aggregate(const Self& stat) 
{
	for (int i = 0; i < Dimension; ++i)
	{
		m_sum[i] += stat.m_sum[i];
		m_sumsq[i] += stat.m_sumsq[i];
	}

	m_num += stat.m_num;
}


template <int Dimension>
MultiDimensionStatisticsAggregator<Dimension> MultiDimensionStatisticsAggregator<Dimension>::DeepClone() const
{
	Self copy;
	copy.m_num = m_num;
	copy.m_sum = m_sum;
	copy.m_sumsq = m_sumsq;
	return copy;
}


template <int Dimension>
bool MultiDimensionStatisticsAggregator<Dimension>::operator==(const Self& stat) const
{
	if(stat.m_num != m_num) 
		return false;

	for (int i = 0; i < Dimension; ++i)
	{
		if( m_sum[i] == 0 ) {
			if( std::abs(stat.m_sum[i] - m_sum[i]) > 1e-6 )
				return false;
		}else {
			if( std::abs((stat.m_sum[i] - m_sum[i]) / m_sum[i]) > 1e-6 )
				return false;
		}

		if( m_sumsq[i] == 0 ) {
			if(std::abs(stat.m_sumsq[i] - m_sumsq[i]) > 1e-6)
				return false;
		}else {
			if(std::abs((stat.m_sumsq[i] - m_sumsq[i]) / m_sumsq[i]) > 1e-6)
				return false;
		}
	}

	return true;
}


template <int Dimension>
void MultiDimensionStatisticsAggregator<Dimension>::Serialize(std::ostream& o) const
{
	o.write((const char*)(&m_num), sizeof(unsigned int));
	int regressionDim = Dimension;
	o.write((const char*)(&regressionDim), sizeof(int));
	o.write((const char*)(&m_sum[0]), sizeof(double) * Dimension);
	o.write((const char*)(&m_sumsq[0]), sizeof(double) * Dimension);
}


template <int Dimension>
void MultiDimensionStatisticsAggregator<Dimension>::Deserialize(std::istream& in)
{
	in.read((char*)(&m_num), sizeof(unsigned int));

	int targetDim = 0;
	in.read((char*)(&targetDim), sizeof(int));

	if(targetDim != Dimension) {
		std::cerr << "loaded wrong stat object (wrong dimension " << targetDim << ")" << std::endl;
		assert(false);
		exit(-1);
	}

	in.read((char*)(&m_sum[0]), sizeof(double) * Dimension);
	in.read((char*)(&m_sumsq[0]), sizeof(double) * Dimension);
}

template <int Dimension>
std::ostream& operator<<(std::ostream& os, const MultiDimensionStatisticsAggregator<Dimension>& stat)
{
	os << stat.Entropy();
	return os;
}


template <int Dimension>
void MultiDimensionStatisticsAggregator<Dimension>::GetMean(boost::array<double,Dimension>& mean) const
{
	for (int i = 0; i < Dimension; ++i)
		mean[i] = m_sum[i] / m_num;
}


template <int Dimension>
void MultiDimensionStatisticsAggregator<Dimension>::GetVariances(boost::array<double,Dimension>& var) const
{
	for (int i = 0; i < Dimension; ++i)
		var[i] = m_sumsq[i] / m_num - (m_sum[i] / m_num) * (m_sum[i] / m_num);
}


template <int Dimension>
const char* MultiDimensionStatisticsAggregator<Dimension>::BinaryHeader = "BRIC.IDEA.RegressionForest.MultiDimensionStatisticsAggregator";


} } }

#endif

