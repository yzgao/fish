//
//  SingleVariableStatisticsAggregator.h
//  FISH
//
//  Created by Yaozong Gao on 6/25/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __SingleVariableStatisticsAggregator_h__
#define __SingleVariableStatisticsAggregator_h__


#include "forest/interfaces.h"
#include <fstream>


namespace BRIC { namespace IDEA { namespace FISH {


/** @brief single variable statistics aggregator    
 *
 * for efficiency if the regression dimension is one
 * use SingleVariableStatisticsAggregator instead of MultiVariableStatisticsAggregator
 */
class SingleVariableStatisticsAggregator
{
public:
    
    SingleVariableStatisticsAggregator(): m_sampleNumber(0), m_sum(0), m_sqsum(0) {}
    
	//
	// common interface for StatisticsAggregator
	// 

    /** @brief make samples empty and reset all statistics */
    void Clear() {
        m_sampleNumber = 0;
        m_sum = m_sqsum = 0;
    }
    
	/** @brief get sample number */
    inline unsigned int GetSampleNumber() const { return m_sampleNumber; }
    
    // entropy
	/** @brief use variance as entropy */
    inline double Entropy() const {
        return GetVariance();
    }
    
	/** @brief aggregate a training data */
    void Aggregate(const IDataCollection* dataCollection, unsigned int dataIdx);
    
	/** @brief aggregate another statistics aggregator */
    void Aggregate(const SingleVariableStatisticsAggregator& aggregator);
    
	/** @brief deep clone a statistics aggregator */
    SingleVariableStatisticsAggregator DeepClone() const;
    
	/** @brief equality test
     *
	 * for correctness check during training procedure
     * override equal operator
	 */
    bool operator==(const SingleVariableStatisticsAggregator& statistics) const;
    
    // IO operators
	/** @brief serialize this object to ostream */
    void Serialize(std::ostream& o) const;
    
	/** @brief deserialize the object from istream */
    void Deserialize(std::istream& in);

	//
	// class-specific member functions
	// 
    /** @brief get target dimension */
	inline unsigned int GetTargetDimension() const { return 1; }

	/** @brief get mean estimation */
	inline double GetMean() const { return m_sum / m_sampleNumber; }

	/** @brief get variance estimation */
	inline double GetVariance() const { 
		double mean = GetMean();
		return (m_sqsum/m_sampleNumber) - mean*mean;
	}
    
private:
    
    unsigned int m_sampleNumber;
    double m_sum;
    double m_sqsum;
    
public:
    
    static const char* BinaryHeader;
    
};


//////////////////////////////////////////////////////////////////////////
// Implementations
//////////////////////////////////////////////////////////////////////////

void SingleVariableStatisticsAggregator::Aggregate(const IDataCollection* dataCollection, unsigned int dataIdx) {

	if (dataCollection->GetTargetDimension() != 1) {
		std::cerr << "Target dimension is not one." << std::endl;
		assert(false);
		exit(-1);
	}

	const double* t = dataCollection->GetTarget(dataIdx);

	m_sum += t[0];
	m_sqsum += t[0]*t[0];

	++ m_sampleNumber;
}


void SingleVariableStatisticsAggregator::Aggregate(const SingleVariableStatisticsAggregator& aggregator) {

	m_sum += aggregator.m_sum;
	m_sqsum += aggregator.m_sqsum;
	m_sampleNumber += aggregator.m_sampleNumber;
}


SingleVariableStatisticsAggregator SingleVariableStatisticsAggregator::DeepClone() const {

	SingleVariableStatisticsAggregator copy;
	copy.m_sampleNumber = m_sampleNumber;
	copy.m_sum = m_sum;
	copy.m_sqsum = m_sqsum;
	return copy;

}


bool SingleVariableStatisticsAggregator::operator==(const SingleVariableStatisticsAggregator& statistics) const
{
	if (m_sampleNumber != statistics.m_sampleNumber)
		return false;

	if (m_sum == 0)
	{
		if (std::abs(statistics.m_sum) > 1e-6)
			return false;
	}else {
		if (std::abs((m_sum-statistics.m_sum) / m_sum) > 1e-6)
			return false;
	}

	if (m_sqsum == 0)
	{
		if(std::abs(statistics.m_sqsum) > 1e-6)
			return false;
	}else {
		if (std::abs((m_sqsum-statistics.m_sqsum) / m_sqsum) > 1e-6)
			return false;
	}

	return true;
}


void SingleVariableStatisticsAggregator::Serialize(std::ostream& o) const {
	o.write((const char*)(&m_sampleNumber), sizeof(unsigned int));
	o.write((const char*)(&m_sum), sizeof(double));
	o.write((const char*)(&m_sqsum), sizeof(double));
}


void SingleVariableStatisticsAggregator::Deserialize(std::istream& in) {
	in.read((char*)(&m_sampleNumber), sizeof(unsigned int));
	in.read((char*)(&m_sum), sizeof(double));
	in.read((char*)(&m_sqsum), sizeof(double));
}

std::ostream& operator<<(std::ostream& os, const SingleVariableStatisticsAggregator& stat)
{
	os << stat.Entropy();
	return os;
}

const char* SingleVariableStatisticsAggregator::BinaryHeader = "BRIC.IDEA.RegressionForest.SingleVariableStatisticsAggregator";


} } }

#endif