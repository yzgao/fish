//
//  SimpleRegressionStatisticsAggregator.h
//  FISH
//
//  Created by Yaozong Gao on 3/25/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __SimpleRegressionStatisticsAggregator_h__
#define __SimpleRegressionStatisticsAggregator_h__

#include "forest/interfaces.h"
#include "common/vect.h"
#include <fstream>

namespace BRIC { namespace IDEA { namespace FISH {

class SimpleRegressionStatisticsAggregator
{
public:
	typedef SimpleRegressionStatisticsAggregator Self;

	// constructors
	SimpleRegressionStatisticsAggregator();
	SimpleRegressionStatisticsAggregator(unsigned int dimension);
	SimpleRegressionStatisticsAggregator(const Self& stat);
	// destructor
	~SimpleRegressionStatisticsAggregator();

	// public interfaces
	inline void Clear();
	inline unsigned int GetSampleNumber() const;
	inline double Entropy() const;
	inline void Aggregate(const IDataCollection* dataCollection, unsigned int dataIdx);
	inline void Aggregate(const Self& aggregator);
	inline Self DeepClone() const;

	// other interfaces
	inline bool operator==(const Self& stat) const;
	inline Self& operator=(const Self& stat);
	inline unsigned int GetTargetDimension() const;

	inline void GetMean(double* mean) const;
	inline void GetVariance(double* vars) const;

	// IO Operators
	void Serialize(std::ostream& o) const;
	void Deserialize(std::istream& in);

	static const char* BinaryHeader;

public:

	unsigned int m_num;
	unsigned int m_dimension;
	double* m_sum;
	double* m_sqsum;

};

//////////////////////////////////////////////////////////////////////////

const char* SimpleRegressionStatisticsAggregator::BinaryHeader = "BRIC.IDEA.RegressionForest.SimpleRegressionStatisticsAggregator";

SimpleRegressionStatisticsAggregator::SimpleRegressionStatisticsAggregator(): m_num(0), m_dimension(0), m_sum(NULL), m_sqsum(NULL) {}

SimpleRegressionStatisticsAggregator::SimpleRegressionStatisticsAggregator(unsigned int dimension): m_num(0), m_dimension(dimension) {

	unsigned int buffersize = dimension * 2;
	m_sum = (double *)malloc(sizeof(double) * buffersize);
	m_sqsum = m_sum + m_dimension;
	::memset(m_sum, 0, sizeof(double) * buffersize);
}

SimpleRegressionStatisticsAggregator::SimpleRegressionStatisticsAggregator(const SimpleRegressionStatisticsAggregator& stat) {

	m_num = stat.m_num;
	m_dimension = stat.m_dimension;

	if (stat.m_dimension == 0)
		m_sum = m_sqsum = NULL;
	else {
		unsigned int bufferSize = stat.m_dimension * 2;
		m_sum = (double *)malloc(sizeof(double) * bufferSize);
		m_sqsum = m_sum + m_dimension;
		::memcpy(m_sum , stat.m_sum, sizeof(double) * bufferSize);
	}
}

SimpleRegressionStatisticsAggregator::~SimpleRegressionStatisticsAggregator() {

	if (m_sum != NULL) {
		free(m_sum);
		m_sum = m_sqsum = NULL;
	}
	m_num = m_dimension = 0;
}

void SimpleRegressionStatisticsAggregator::Clear() {

	m_num = 0;
	::memset(m_sum, 0, sizeof(double) * m_dimension * 2);
}

unsigned int SimpleRegressionStatisticsAggregator::GetSampleNumber() const {

	return m_num;
}

double SimpleRegressionStatisticsAggregator::Entropy() const {

	double mean_var = 0;

	for (unsigned int i = 0; i < m_dimension; ++i)
	{
		double mean = m_sum[i] / m_num;
		double var = m_sqsum[i] / m_num - mean * mean;
		if (var < 0) var = 0;
		mean_var += sqrt(var);
	}
	mean_var /= m_dimension;

	return mean_var;
}

void SimpleRegressionStatisticsAggregator::Aggregate(const IDataCollection* dataCollection, unsigned int dataIdx) {

	const double* t = dataCollection->GetTarget(dataIdx);

	// update sample sum
	for (unsigned int i = 0; i < m_dimension; ++i)
	{
		m_sum[i] += t[i];
		m_sqsum[i] += t[i] * t[i];
	}

	++m_num;
}

void SimpleRegressionStatisticsAggregator::Aggregate(const SimpleRegressionStatisticsAggregator& stat) {

	// update m_sum
	for (unsigned int i = 0; i < m_dimension; ++i) {
		m_sum[i] += stat.m_sum[i];
		m_sqsum[i] += stat.m_sqsum[i];
	}

	m_num += stat.m_num;
}

SimpleRegressionStatisticsAggregator SimpleRegressionStatisticsAggregator::DeepClone() const {

	SimpleRegressionStatisticsAggregator copy;

	unsigned int buffersize = m_dimension * 2;
	copy.m_sum = (double*)malloc(sizeof(double) * buffersize);
	copy.m_sqsum = m_sum + m_dimension;
	copy.m_num = m_num;

	::memcpy(copy.m_sum, m_sum, sizeof(double) * buffersize);

	return copy;
}

bool SimpleRegressionStatisticsAggregator::operator==(const SimpleRegressionStatisticsAggregator& stat) const {

	if (stat.m_num != m_num || stat.m_dimension != m_dimension)
		return false;

	for (unsigned int i = 0; i < m_dimension; ++i) {
		if (std::abs(m_sum[i] - stat.m_sum[i]) >= VECT_EPSILON || std::abs(m_sqsum[i] - stat.m_sqsum[i]) >= VECT_EPSILON)
			return false;
	}
	return true;
}

SimpleRegressionStatisticsAggregator& SimpleRegressionStatisticsAggregator::operator=(const SimpleRegressionStatisticsAggregator& stat) {

	if (this == &stat)
		return *this;

	m_num = stat.m_num;

	if (stat.m_dimension == 0)
	{
		m_dimension = 0;
		if (m_sum != NULL)
			free(m_sum);
		m_sum = m_sqsum = NULL;
	}
	else {
		unsigned int bufferSize = stat.m_dimension * 2;
		if (m_dimension == stat.m_dimension) {
			::memcpy(m_sum, stat.m_sum, sizeof(double)* bufferSize);
		}
		else {
			m_dimension = stat.m_dimension;
			if (m_sum != NULL)
				free(m_sum);
			m_sum = (double*)malloc(sizeof(double) * bufferSize);
			m_sqsum = m_sum + m_dimension;
			::memcpy(m_sum, stat.m_sum, sizeof(double)* bufferSize);
		}
	}

	return *this;
}

unsigned int SimpleRegressionStatisticsAggregator::GetTargetDimension() const {
	return m_dimension;
}

void SimpleRegressionStatisticsAggregator::GetMean(double* mean) const {
	
	for (unsigned int i = 0; i < m_dimension; ++i)
		mean[i] = m_sum[i] / m_num;
}

void SimpleRegressionStatisticsAggregator::GetVariance(double* vars) const
{
	for (unsigned int i = 0; i < m_dimension; ++i)
	{
		double mean = m_sum[i] / m_num;
		vars[i] = m_sqsum[i] / m_num - mean * mean;
	}
}

void SimpleRegressionStatisticsAggregator::Serialize(std::ostream& o) const {

	o.write((const char*)(&m_num), sizeof(unsigned int));
	o.write((const char*)(&m_dimension), sizeof(unsigned int));
	o.write((const char*)(m_sum), sizeof(double)* m_dimension * 2);
}

std::ostream& operator<<(std::ostream& os, const SimpleRegressionStatisticsAggregator& stat)
{
	os << stat.Entropy();
	return os;
}

void SimpleRegressionStatisticsAggregator::Deserialize(std::istream& in) {

	in.read((char*)(&m_num), sizeof(unsigned int));
	in.read((char*)(&m_dimension), sizeof(unsigned int));

	if (m_sum != NULL) {
		free(m_sum);
		m_sum = m_sqsum = NULL;
	}

	if (m_dimension != 0)
	{
		unsigned int buffersize = m_dimension * 2;
		m_sum = (double *)malloc(sizeof(double) * buffersize);
		m_sqsum = m_sum + m_dimension;
		in.read((char *)(m_sum), sizeof(double)* buffersize);
	}
}

} } }


#endif