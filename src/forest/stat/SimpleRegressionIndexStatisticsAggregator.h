//
//  SimpleRegressionIndexStatisticsAggregator.h
//  FISH
//
//  Created by Yaozong Gao on 6/9/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __SimpleRegressionIndexStatisticsAggregator_h__
#define __SimpleRegressionIndexStatisticsAggregator_h__

#include "SimpleRegressionIndexStatisticsAggregator.h"

namespace BRIC { namespace IDEA { namespace FISH {

#include "forest/interfaces.h"
#include "common/vect.h"
#include <fstream>


class SimpleRegressionIndexStatisticsAggregator
{
public:
	typedef SimpleRegressionIndexStatisticsAggregator Self;

	// constructors
	SimpleRegressionIndexStatisticsAggregator();
	SimpleRegressionIndexStatisticsAggregator(unsigned int dimension);
	SimpleRegressionIndexStatisticsAggregator(const Self& stat);
	// destructor
	~SimpleRegressionIndexStatisticsAggregator();

	// public interfaces
	inline void Clear();
	inline unsigned int GetSampleNumber() const;
	inline double Entropy() const;
	inline void Aggregate(const IDataCollection* dataCollection, unsigned int dataIdx);
	inline void Aggregate(const Self& aggregator);
	inline Self DeepClone() const;

	// other interfaces
	inline bool operator==(const Self& stat) const;
	inline Self& operator=(const Self& stat);
	inline unsigned int GetTargetDimension() const;

	inline void GetMean(double* mean) const;
	inline void GetVariance(double* vars) const;

	// IO Operators
	void Serialize(std::ostream& o) const;
	void Deserialize(std::istream& in);

	static const char* BinaryHeader;

public:

	unsigned int m_num;
	unsigned int m_dimension;
	double* m_sum;
	double* m_sqsum;

	int leaf_index;

};

//////////////////////////////////////////////////////////////////////////

const char* SimpleRegressionIndexStatisticsAggregator::BinaryHeader = "BRIC.IDEA.RegressionForest.SimpleRegressionIndexStatisticsAggregator";

SimpleRegressionIndexStatisticsAggregator::SimpleRegressionIndexStatisticsAggregator(): m_num(0), m_dimension(0), m_sum(NULL), m_sqsum(NULL) {}

SimpleRegressionIndexStatisticsAggregator::SimpleRegressionIndexStatisticsAggregator(unsigned int dimension): m_num(0), m_dimension(dimension) {

	unsigned int buffersize = dimension * 2;
	m_sum = (double *)malloc(sizeof(double) * buffersize);
	m_sqsum = m_sum + m_dimension;
	::memset(m_sum, 0, sizeof(double) * buffersize);
}

SimpleRegressionIndexStatisticsAggregator::SimpleRegressionIndexStatisticsAggregator(const SimpleRegressionIndexStatisticsAggregator& stat) {

	m_num = stat.m_num;
	m_dimension = stat.m_dimension;

	if (stat.m_dimension == 0)
		m_sum = m_sqsum = NULL;
	else {
		unsigned int bufferSize = stat.m_dimension * 2;
		m_sum = (double *)malloc(sizeof(double) * bufferSize);
		m_sqsum = m_sum + m_dimension;
		::memcpy(m_sum , stat.m_sum, sizeof(double) * bufferSize);
	}
}

SimpleRegressionIndexStatisticsAggregator::~SimpleRegressionIndexStatisticsAggregator() {

	if (m_sum != NULL) {
		free(m_sum);
		m_sum = m_sqsum = NULL;
	}
	m_num = m_dimension = 0;
}

void SimpleRegressionIndexStatisticsAggregator::Clear() {

	m_num = 0;
	::memset(m_sum, 0, sizeof(double) * m_dimension * 2);
}

unsigned int SimpleRegressionIndexStatisticsAggregator::GetSampleNumber() const {

	return m_num;
}

double SimpleRegressionIndexStatisticsAggregator::Entropy() const {

	double mean_var = 0;

	for (unsigned int i = 0; i < m_dimension; ++i)
	{
		double mean = m_sum[i] / m_num;
		double var = m_sqsum[i] / m_num - mean * mean;
		if (var < 0) var = 0;
		mean_var += sqrt(var);
	}
	mean_var /= m_dimension;

	return mean_var;
}

void SimpleRegressionIndexStatisticsAggregator::Aggregate(const IDataCollection* dataCollection, unsigned int dataIdx) {

	const double* t = dataCollection->GetTarget(dataIdx);

	// update sample sum
	for (unsigned int i = 0; i < m_dimension; ++i)
	{
		m_sum[i] += t[i];
		m_sqsum[i] += t[i] * t[i];
	}

	++m_num;
}

void SimpleRegressionIndexStatisticsAggregator::Aggregate(const SimpleRegressionIndexStatisticsAggregator& stat) {

	// update m_sum
	for (unsigned int i = 0; i < m_dimension; ++i) {
		m_sum[i] += stat.m_sum[i];
		m_sqsum[i] += stat.m_sqsum[i];
	}

	m_num += stat.m_num;
}

SimpleRegressionIndexStatisticsAggregator SimpleRegressionIndexStatisticsAggregator::DeepClone() const {

	SimpleRegressionIndexStatisticsAggregator copy;

	unsigned int buffersize = m_dimension * 2;
	copy.m_sum = (double*)malloc(sizeof(double) * buffersize);
	copy.m_sqsum = m_sum + m_dimension;
	copy.m_num = m_num;

	::memcpy(copy.m_sum, m_sum, sizeof(double) * buffersize);

	return copy;
}

bool SimpleRegressionIndexStatisticsAggregator::operator==(const SimpleRegressionIndexStatisticsAggregator& stat) const {

	if (stat.m_num != m_num || stat.m_dimension != m_dimension)
		return false;

	for (unsigned int i = 0; i < m_dimension; ++i) {
		if (std::abs(m_sum[i] - stat.m_sum[i]) >= VECT_EPSILON || std::abs(m_sqsum[i] - stat.m_sqsum[i]) >= VECT_EPSILON)
			return false;
	}
	return true;
}

SimpleRegressionIndexStatisticsAggregator& SimpleRegressionIndexStatisticsAggregator::operator=(const SimpleRegressionIndexStatisticsAggregator& stat) {

	if (this == &stat)
		return *this;

	m_num = stat.m_num;

	if (stat.m_dimension == 0)
	{
		m_dimension = 0;
		if (m_sum != NULL)
			free(m_sum);
		m_sum = m_sqsum = NULL;
	}
	else {
		unsigned int bufferSize = stat.m_dimension * 2;
		if (m_dimension == stat.m_dimension) {
			::memcpy(m_sum, stat.m_sum, sizeof(double)* bufferSize);
		}
		else {
			m_dimension = stat.m_dimension;
			if (m_sum != NULL)
				free(m_sum);
			m_sum = (double*)malloc(sizeof(double) * bufferSize);
			m_sqsum = m_sum + m_dimension;
			::memcpy(m_sum, stat.m_sum, sizeof(double)* bufferSize);
		}
	}

	return *this;
}

unsigned int SimpleRegressionIndexStatisticsAggregator::GetTargetDimension() const {
	return m_dimension;
}

void SimpleRegressionIndexStatisticsAggregator::GetMean(double* mean) const {
	
	for (unsigned int i = 0; i < m_dimension; ++i)
		mean[i] = m_sum[i] / m_num;
}

void SimpleRegressionIndexStatisticsAggregator::GetVariance(double* vars) const
{
	for (unsigned int i = 0; i < m_dimension; ++i)
	{
		double mean = m_sum[i] / m_num;
		vars[i] = m_sqsum[i] / m_num - mean * mean;
	}
}

void SimpleRegressionIndexStatisticsAggregator::Serialize(std::ostream& o) const {

	o.write((const char*)(&m_num), sizeof(unsigned int));
	o.write((const char*)(&m_dimension), sizeof(unsigned int));
	o.write((const char*)(m_sum), sizeof(double)* m_dimension * 2);
	o.write((const char*)(&leaf_index), sizeof(int));
}

std::ostream& operator<<(std::ostream& os, const SimpleRegressionIndexStatisticsAggregator& stat)
{
	os << stat.Entropy();
	return os;
}

void SimpleRegressionIndexStatisticsAggregator::Deserialize(std::istream& in) {

	in.read((char*)(&m_num), sizeof(unsigned int));
	in.read((char*)(&m_dimension), sizeof(unsigned int));

	if (m_sum != NULL) {
		free(m_sum);
		m_sum = m_sqsum = NULL;
	}

	if (m_dimension != 0)
	{
		unsigned int buffersize = m_dimension * 2;
		m_sum = (double *)malloc(sizeof(double) * buffersize);
		m_sqsum = m_sum + m_dimension;
		in.read((char *)(m_sum), sizeof(double)* buffersize);
	}

	in.read((char *)(&leaf_index), sizeof(int));
}


} } }

#endif