//
//  ForestTrainer.h
//  FISH
//
//  Created by Yaozong Gao on 11/27/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __ForestTrainer_h__
#define __ForestTrainer_h__

#include "forest/interfaces.h"
#include "forest/DataCollection.h"
#include "forest/Forest.h"
#include "common/Random.h"
#include "common/vect.h"

#include <memory>
#include <queue>
#include <assert.h>
#include <vector>
#include <cmath>

#include <omp.h>

namespace BRIC { namespace IDEA { namespace FISH {


/** @brief training parameters for each tree */
struct TreeTrainingParameters {
public:
	unsigned int maxTreeDepth;
	unsigned int numOfCandidateThresholdsPerWeakLearner;
	unsigned int numOfRandomWeakLearners;
	unsigned int minElementsOfLeafNode;
	double minInformationGain;
};


/** @brief training parameters for a forest */
struct ForestTrainingParameters {
public:
	unsigned int numTrees;
	TreeTrainingParameters treeParameters;
};


/** @brief training operations for a single tree
 *  Core algorithms of random forest */
template <class W, class S>
class TreeTrainer
{
public:
    typedef Tree<W,S> TreeType;
    typedef Node<W,S> NodeType;

    /** @brief interface of tree training 
     *
     * @param context training context object
     * @param treeParameters training parameters
     * @param trainingData training data collections
     * @param random random generator
     * @param number of threads used for parallel computing (best if you have one thread per core)
     */
    static std::auto_ptr<TreeType> TrainTree(ITrainingContext<W,S>& context, const TreeTrainingParameters& treeParameters,
                                      const IDataCollection* trainingData, Random& random);

private:

	/** @brief internal data structure for recursive node training */
	struct NodeTracker {
		NodeType* node;
		unsigned int sp;
		unsigned int ep;
		int depth;

		NodeTracker(NodeType* emptyNode, unsigned int dataStartIdx, unsigned int dataEndIdx, int currentDepth): 
			node(emptyNode), sp(dataStartIdx), ep(dataEndIdx), depth(currentDepth) {}
	};

	/** @brief thread local data for parallel computing */
	struct ThreadLocalData {

		double m_maxInformationGain;
		W m_bestWeakLearner;
		double m_bestThreshold;

		S m_leftChildStatistics, m_rightChildStatistics;
		std::vector<S> m_partitionStatistics;

		std::vector<double> m_thresholds;
		std::vector<double> m_responses;

		Random m_random;

		/** @brief constructor */
		ThreadLocalData() {}

		/** @brief explicitly constructor */
		ThreadLocalData(Random& random, const ITrainingContext<W, S>& context, const TreeTrainingParameters& treeParameters, const IDataCollection* trainingData):
			m_random(random.Next<unsigned int>(0, std::numeric_limits<unsigned int>::max()))
		{
			m_maxInformationGain = 0.0;
			m_bestThreshold = 0;

			m_leftChildStatistics = context.GetStatisticsAggregator();
			m_rightChildStatistics = context.GetStatisticsAggregator();

			m_partitionStatistics.resize( treeParameters.numOfCandidateThresholdsPerWeakLearner + 1 );
			for (size_t i = 0; i < m_partitionStatistics.size(); ++i)
				m_partitionStatistics[i] = context.GetStatisticsAggregator();

			m_responses.resize( trainingData->GetSampleNumber() );
		}

		/** @brief clear local data */
		void Clear()
		{
			m_maxInformationGain = 0.0;
			m_bestWeakLearner = W();
			m_bestThreshold = 0;
		}
	};

private: 

	/** @brief check whether the input data contains NaN or INF */
	static void CheckInputData(const IDataCollection* trainingData);

	/** @brief optimize the weak learner and threshold of one node */
	static double TrainNode(ITrainingContext<W,S>& context, const TreeTrainingParameters& treeParameters, const IDataCollection* trainingData, Random& random, 
					int depth, const std::vector<unsigned int>& dataIndices, unsigned int sp, unsigned int ep, 
					const S& nodeStatistics, std::vector<ThreadLocalData>& threadDatas, std::vector<W>& weakLearners,
					W& bestWeakLearner, double& bestThreshold);

	/** @brief partition a collection of training samples into left and right */
	static unsigned int PartitionTrainingData(std::vector<double>& responses, unsigned int sp, unsigned int ep, double bestThreshold, std::vector<unsigned int>& dataIndices);

	/** @brief generate a collection of random thresholds for one weak learner */
	static unsigned int GetRandomCandidateThresholds(double* responses, unsigned int numResponses, unsigned int numThresholds, Random& random, std::vector<double>& thresholds);
};


/** @brief simple wrapper for forest trainer 
 *
 * each tree in the forest is trained using the SAME data.
 * the entire feature pool is the SAME for all trees
 * NOTE: if you want to train trees with different dataset (bagging) and different feature pools, 
 * you should directly use the TreeTrainer.
 */
template <class W, class S>
class ForestTrainer
{
public:
    typedef Forest<W, S> ForestType;
    typedef Tree<W, S> TreeType;
    typedef ITrainingContext<W, S> TrainingContextType;
    
    /** @brief interface for forest training */
    static std::auto_ptr<ForestType> TrainForest(TrainingContextType& context, const ForestTrainingParameters& forestParameters,
                                                 const IDataCollection* trainingData, Random& random);
};


//////////////////////////////////////////////////////////////////////////

template <class W, class S>
std::auto_ptr<typename TreeTrainer<W,S>::TreeType> TreeTrainer<W,S>::TrainTree(ITrainingContext<W,S>& context, const TreeTrainingParameters& treeParameters, const IDataCollection* trainingData, Random& random)
{
	if (trainingData->GetSampleNumber() <= (treeParameters.minElementsOfLeafNode*2)) {
		std::cerr << "training samples must be greater than " << treeParameters.minElementsOfLeafNode*2 << std::endl;
		exit(-1);
	}

	// initialize data indices
	std::vector<unsigned int> dataIndices;
	dataIndices.resize(trainingData->GetSampleNumber());
	for (unsigned int i = 0; i < dataIndices.size(); ++i) 
		dataIndices[i] = i;

	// create a tree with uninitialized root node
	std::auto_ptr< Tree<W,S> > tree( new Tree<W,S>( new NodeType() ) );
	double uniqueID = random.NextDouble(0, std::numeric_limits<double>::max());
	tree->SetUniqueID(uniqueID);

	// create node queue
	std::queue<NodeTracker> nodes;
	unsigned int depth = 1, dataStartIdx = 0, dataEndIdx = static_cast<unsigned int>(dataIndices.size())-1;
	nodes.push( NodeTracker(tree->Root(), dataStartIdx, dataEndIdx, depth) );

	// create response buffer
	std::vector<double> responses;
	responses.resize(trainingData->GetSampleNumber());

	// parallel buffer preparation        
    int numThreads = omp_get_max_threads();
        
    // allocate thread local data for parallel computing in each thread
    std::vector< ThreadLocalData > threadDatas;
    threadDatas.resize( numThreads );
    for (int threadIndex = 0; threadIndex < numThreads; ++threadIndex)
		// Note use of placement new operator to initialize already-allocated memory
		new (&threadDatas[threadIndex]) ThreadLocalData(random, context, treeParameters, trainingData);
        
    // allocate memory for random selected weak learners
    std::vector< W > weakLearners;
    weakLearners.resize( treeParameters.numOfRandomWeakLearners );

	// recursive node training by breath-first order
	while (nodes.size() != 0)
	{
		NodeTracker tracker = nodes.front();
		nodes.pop();

		// output training information
		std::cout << "[D:" << tracker.depth << ", N:" << tracker.ep - tracker.sp + 1 << "] ";
		std::cout.flush();

		// compute sample statistics of this node
		S nodeStatistics = context.GetStatisticsAggregator();
		for (unsigned int i = tracker.sp; i <= tracker.ep; ++i)
			nodeStatistics.Aggregate(trainingData, dataIndices[i]);

		// output the information of this node (e.g., Entropy)
		std::cout << "E: " << nodeStatistics;
		std::cout.flush();

		// check pre-stop conditions
		// 1) maximum tree depth
		if ( tracker.depth >= static_cast<int>(treeParameters.maxTreeDepth) )
		{
			tracker.node->InitializeLeaf(nodeStatistics);
			std::cout << " Terminate (Max depth)." << std::endl;
			continue;
		}
		// 2) minimum sample stopping condition
		unsigned int itemNumber = tracker.ep - tracker.sp + 1;
		if (itemNumber <= treeParameters.minElementsOfLeafNode)
		{
			tracker.node->InitializeLeaf(nodeStatistics);
			std::cout << " Terminate (node # <= " << treeParameters.minElementsOfLeafNode << ")." << std::endl;
			continue;
		}

		// optimize the weak learner and threshold by maximizing the gain
		W bestWeakLearner;
		double bestThreshold = 0, maxGain = 0;

		maxGain = TrainNode(context, treeParameters, trainingData, random, tracker.depth, dataIndices, tracker.sp, tracker.ep, nodeStatistics,
			threadDatas, weakLearners, bestWeakLearner, bestThreshold);

		// check post-stop conditions
		// 1) minimum information gain
		if (maxGain <= treeParameters.minInformationGain)
		{
			tracker.node->InitializeLeaf(nodeStatistics);
			std::cout << " Terminate (I <= " << treeParameters.minInformationGain << ")." << std::endl;
			continue;
		}

		// get responses for the selected weak learner
		bestWeakLearner.GetResponses(trainingData, &dataIndices[tracker.sp], tracker.ep-tracker.sp+1, &responses[tracker.sp]);

		// 2) customized stop condition
		if (context.EnableCustomizedCondition())
		{
			// check customized terminating condition
			S leftChildStatistics = context.GetStatisticsAggregator();
			S rightChildStatistics = context.GetStatisticsAggregator();

			for (unsigned int i = tracker.sp; i <= tracker.ep; ++i)
			{
				if (responses[i] <= bestThreshold)
					leftChildStatistics.Aggregate(trainingData, dataIndices[i]);
				else 
					rightChildStatistics.Aggregate(trainingData, dataIndices[i]);
			}

			// verification
			S verifyStatistics = leftChildStatistics.DeepClone();
			verifyStatistics.Aggregate(rightChildStatistics);
			if (!(verifyStatistics == nodeStatistics)) {
				std::cerr << "Aggregate Inconsistence between children and parent." << std::endl;
				assert(false);
				exit(-1);
			}

			// evaluate customized terminating condition
			if ( context.ShouldTerminate(nodeStatistics, leftChildStatistics, rightChildStatistics, maxGain) )
			{
				tracker.node->InitializeLeaf(nodeStatistics);
				std::cout << " Terminate (customized condition)." << std::endl;
				continue;
			}
		}

		// initialize split training node
		// print out weak learner and learned threshold
		std::cout << ", W: " << bestWeakLearner << ", T: " << bestThreshold << ", I: " << maxGain;
		std::cout << std::endl;

		// if not terminate, initialize a split node.
		tracker.node->InitializeSplit(bestWeakLearner, bestThreshold);

		// partition the training data between sp and ep into left and right.
		unsigned int mid = PartitionTrainingData(responses, tracker.sp, tracker.ep, bestThreshold, dataIndices);

		// recursive training on left and right children
		tracker.node->SetLeft(new NodeType());
		tracker.node->SetRight(new NodeType());

		NodeTracker leftTracker(tracker.node->GetLeft(), tracker.sp, mid, tracker.depth+1);
		NodeTracker rightTracker(tracker.node->GetRight(), mid+1, tracker.ep, tracker.depth+1);

		nodes.push(leftTracker);
		nodes.push(rightTracker);
	}

	//////////////////////////////////////////////////////////////////////////
	// print out statistics of the trained tree for analysis

	// print out how many features in total used in the tree
	std::map<unsigned int, size_t> featureCounts;
	tree->CountFeatures(featureCounts);
	std::cout << "Number of Features used: " << featureCounts.size() << std::endl;

	// print out how many nodes are trained
	size_t totalNodeNumber = tree->GetTotalNodeNumber();
	std::cout << "Number of Nodes used: " << totalNodeNumber << std::endl; 

	// print out the maximum tree depth
	int treeDepth = tree->TreeDepth();
	std::cout << "Tree Depth: " << treeDepth << std::endl;

	return tree;
}

// threadDatas: pre-allocated threadDatas (maximum number of threads)
// weakLearners: pre-allocated weak learner buffer (number of random weak learners)
template <class W, class S>
double TreeTrainer<W,S>::TrainNode(ITrainingContext<W,S>& context, const TreeTrainingParameters& treeParameters, const IDataCollection* trainingData, Random& random, 
						int depth, const std::vector<unsigned int>& dataIndices, unsigned int sp, unsigned int ep, 
						const S& nodeStatistics, std::vector<ThreadLocalData>& threadDatas, std::vector<W>& weakLearners,
						W& bestWeakLearner, double& bestThreshold)
{
	// randomly sample weak learners (features)
    for (int i = 0; i < (int)(treeParameters.numOfRandomWeakLearners); ++i)
        weakLearners[i] = context.GetRandomWeakLearner( random );

	// get number of thresholds available
	int numThreads = static_cast<int>( threadDatas.size() );
            
    // approximate how many weak learners per thread
    int numWeakLearnersPerThread = treeParameters.numOfRandomWeakLearners / numThreads;

    // the total number of training samples in this node
	unsigned int nTotalSamples = ep - sp + 1;
	if (nTotalSamples <= 1)
		return 0.0;
            
    #pragma omp parallel for
    for (int threadIndex = 0; threadIndex < numThreads; ++ threadIndex) {
        
        ThreadLocalData& threadData = threadDatas[threadIndex];
        threadData.Clear();
                
		// compute range of weak learners that each thread is responsible for
        int weakLearnerStartIndex = threadIndex * numWeakLearnersPerThread;
		int weakLearnerEndIndex = 0;
		if (threadIndex != (numThreads - 1)) 
			weakLearnerEndIndex = weakLearnerStartIndex + numWeakLearnersPerThread - 1;
		else
			weakLearnerEndIndex = static_cast<int>(treeParameters.numOfRandomWeakLearners) - 1;


		// iterate all weak learners, find the optimal weak learner and threshold
        for(int weakLearnerIndex = weakLearnerStartIndex; weakLearnerIndex <= weakLearnerEndIndex; ++weakLearnerIndex)
        {
            W& weakLearner = weakLearners[weakLearnerIndex];

            weakLearner.GetResponses(trainingData, &dataIndices[sp], ep-sp+1, &threadData.m_responses[sp]);

            // random sample a set of thresholds based on the sample response distribution
            unsigned int foundThresholds = 0;
            if ( (foundThresholds = GetRandomCandidateThresholds(&threadData.m_responses[sp], ep-sp+1,
                                                                  treeParameters.numOfCandidateThresholdsPerWeakLearner, threadData.m_random,
                                                                  threadData.m_thresholds)) == 0 )
            {
                // all responses are the same, no thresholds found
                // in this case, the information gain is 0
                continue;
            }

            // summarized statistics on all partitions
            for (unsigned int j = 0; j < threadData.m_partitionStatistics.size(); ++j)
                threadData.m_partitionStatistics[j].Clear(); // clear statistics aggregator
       
            for (unsigned int j = sp; j <= ep; ++ j)
            {
                unsigned int partitionIndex = 0;
                while (partitionIndex < foundThresholds && threadData.m_responses[j] > threadData.m_thresholds[partitionIndex] )
                    ++ partitionIndex;
                        
                threadData.m_partitionStatistics[partitionIndex].Aggregate(trainingData, dataIndices[j]);
            }
                  
            // compute the optimal threshold by maximizing gain
            for (unsigned int j = 0; j < foundThresholds; ++ j)
            {
                threadData.m_leftChildStatistics.Clear();
                threadData.m_rightChildStatistics.Clear();
                
				// compute the statistics of left and right child for one threshold
                for (unsigned int k = 0; k < foundThresholds + 1; ++k)
                {
                    if (k <= j)
                        threadData.m_leftChildStatistics.Aggregate( threadData.m_partitionStatistics[k] );
                    else
                        threadData.m_rightChildStatistics.Aggregate( threadData.m_partitionStatistics[k] );
                }
                        
                // discard the threshold if either the sample number of left or right child is insufficient
                if ( threadData.m_leftChildStatistics.GetSampleNumber() < treeParameters.minElementsOfLeafNode
                    || threadData.m_rightChildStatistics.GetSampleNumber() < treeParameters.minElementsOfLeafNode)
                    continue;
                        
                // compute information gain
				double informationGain = context.ComputeInformationGain(nodeStatistics, threadData.m_leftChildStatistics, threadData.m_rightChildStatistics);
                if (informationGain > threadData.m_maxInformationGain)
                {
                    threadData.m_maxInformationGain = informationGain;
                    threadData.m_bestWeakLearner = weakLearner;
                    threadData.m_bestThreshold = threadData.m_thresholds[j];
                }
            }  // end loop over candidate thresholds

        } // end loop over weak learner
    } // end loop over threads
            
    // loop over threads to pick out the best weak learner and threshold
    double maxInformationGain = 0;
            
    for (int threadIndex = 0; threadIndex < numThreads; ++ threadIndex) {
        if ( threadDatas[threadIndex].m_maxInformationGain > maxInformationGain ) {
            maxInformationGain = threadDatas[threadIndex].m_maxInformationGain;
            bestWeakLearner = threadDatas[threadIndex].m_bestWeakLearner;
            bestThreshold = threadDatas[threadIndex].m_bestThreshold;
        }
    }

	return maxInformationGain;
}

template <class W, class S>
unsigned int TreeTrainer<W,S>::PartitionTrainingData(std::vector<double>& responses, unsigned int sp, unsigned int ep, double bestThreshold, std::vector<unsigned int>& dataIndices)
{
	unsigned int p1 = sp, p2 = ep;

	if (p2 < p1) {
		std::cerr << "end index must be >= begin index" << std::endl;
		assert(false);
		exit(-1);
	}

	while (p1 != p2)
	{
		if (responses[p1] > bestThreshold)
		{
			// swap two elements
			double temp_response = responses[p2];
			unsigned int temp_index = dataIndices[p2];

			responses[p2] = responses[p1];
			dataIndices[p2] = dataIndices[p1];

			responses[p1] = temp_response;
			dataIndices[p1] = temp_index;

			--p2;
		}else {
			++p1;
		}
	}

	if(responses[p1] <= bestThreshold)
		return p1;
	else
		return p1-1;
}

template <class W, class S>
unsigned int TreeTrainer<W,S>::GetRandomCandidateThresholds(double* responses, unsigned int numResponses, unsigned int numThresholds, Random& random, std::vector<double>& thresholds) 
{
	thresholds.clear();

	// first generate anchor points from training responses
	// reuse thresholds buffer for efficiency
	if(numThresholds + 1 >= numResponses) {

		thresholds.resize(numResponses);
		std::copy(responses, responses + numResponses, &thresholds[0]);

	} else {

		thresholds.resize(numThresholds + 1);
		for (unsigned int i = 0; i < numThresholds + 1; ++i)
		{
			unsigned int randIndex = random.Next<unsigned int>(0, numResponses-1);
			thresholds[i] = responses[ randIndex ];
		}
	}

	// sort anchor points
	std::sort(thresholds.begin(), thresholds.end());

	// if all anchor points are the same, it means all responses are mostly likely to be the same
	// it may indicate the responses of one weak learner is of no distinguished for different training images
	if (thresholds[0] == thresholds[ thresholds.size() - 1 ])
		return 0;	// return 0 thresholds found

	// random pick thresholds between anchor points
	unsigned int thresholdsFound = static_cast<unsigned int>(thresholds.size()) - 1;

	for (unsigned int i = 0; i < thresholdsFound; ++ i)
	{
		if ( (thresholds[i+1]+VECT_EPSILON) < thresholds[i] ) {
			std::cerr << "thresholds[i+1]: " << thresholds[i+1] << " < thresholds[i]: " << thresholds[i] << ", sort failure" << std::endl;
			exit(-1);
		}

		double rand_val = random.NextDouble();
		thresholds[i] = static_cast<double>( thresholds[i] + (thresholds[i+1] - thresholds[i]) * rand_val );
	}

	return thresholdsFound;
}


template <class W, class S>
void TreeTrainer<W, S>::CheckInputData(const IDataCollection* trainingData)
{
	const MemoryDataCollection* collection = static_cast<const MemoryDataCollection*>(trainingData);
	const std::vector<double>& data_vector = collection->GetDataVector();

	for (size_t i = 0; i < data_vector.size(); ++i)
	{
		if ( !std::isfinite(data_vector[i]) )
		{
			std::cerr << "the input data contains infinite number at index " << i << " : " << data_vector[i] << std::endl;
			exit(-1);
		}
	}
}


template <class W, class S>
std::auto_ptr<typename ForestTrainer<W,S>::ForestType> ForestTrainer<W,S>::TrainForest(TrainingContextType& context, const ForestTrainingParameters& forestParameters,
											 const IDataCollection* trainingData, Random& random)
{
	std::auto_ptr<ForestType> forest ( new ForestType );

	for (unsigned int i = 0; i < forestParameters.numTrees; ++i) {
		std::cout << i+1 << " ";
		std::auto_ptr<TreeType> tree = TreeTrainer<W, S>::TrainTree(context, forestParameters.treeParameters, trainingData, random);
		forest->AddTree(tree);
	}
	std::cout << std::endl;
	return forest;
}


} } } 

#endif