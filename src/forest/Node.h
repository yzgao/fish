//
//  Node.h
//  FISH
//
//  Created by Yaozong Gao on 11/27/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __Node_h__
#define __Node_h__

#include <iostream>
#include <assert.h>
#include <cstdlib>

namespace BRIC { namespace IDEA { namespace FISH {

/** @brief a split/leaf node in decision binary tree */
template < class W, class S >
class Node
{
public:

	/** @brief constructor */
	Node();

	/** @brief destructor */
	~Node();

	/** @brief release */
	void ReleaseChildren();

	/** @brief is a Null node? */
    bool isNull() const;
    
	/** @brief is a split node? */
    bool isSplit() const;
    
	/** @brief is a leaf node? */
    bool isLeaf() const;
    
	/** @brief initialize as a leaf node */
    void InitializeLeaf(const S& statisticsAggregator);
    
	/** @brief initialize as a split node */
    void InitializeSplit(W weakLearner, double threshold);

	/** @brief get weak learner associated with this node*/
	const W& GetWeakLearner() const;

	/** @brief get threshold */
	double GetThreshold() const;

	/** @brief get statistics */
	const S& GetStatistics() const;

	/** @brief get statistics */
	S& GetStatistics();

	/** @brief get left child */
	Node<W,S>* GetLeft();

	/** @brief set left child */
	void SetLeft(Node<W,S>* node);

	/** @brief get right child */
	Node<W,S>* GetRight();

	/** @brief set right child */
	void SetRight(Node<W,S>* node);

	/** @brief fast apply without pre-compute all features ahead 
	 *
	 * compute the needed features on the fly. 
	 * Once the features are needed, invoke the functor to get the feature
	 * the functor should be implemented with operator(int), with feature index as input.
	 */
	template <class FunctorType>
	const S& FastApply(FunctorType& fun) const;
    
    // IO operator
	/** @brief serialize operator */
    void Serialize(std::ostream& o) const;
    
	/** @brief deserialize operator */
    void Deserialize(std::istream& in);

private:
	W m_weakLearner;
	double m_threshold;

	S m_statisticsAggregator;

	bool m_isSplit;
	bool m_isLeaf;

	Node<W,S>* m_leftChild;
	Node<W,S>* m_rightChild;
};


//////////////////////////////////////////////////////////////////////////

template <class W, class S>
Node<W,S>::Node() {
	m_threshold = 0;
	m_isLeaf = false;
	m_isSplit = false;
	m_leftChild = m_rightChild = NULL;
}

template <class W, class S>
Node<W,S>::~Node() {
	m_threshold = 0;
	m_isLeaf = false;
	m_isSplit = false;
	ReleaseChildren();
}

template <class W, class S>
void Node<W,S>::ReleaseChildren() {
	if (m_leftChild) {
		delete m_leftChild;
		m_leftChild = NULL;
	}
	if (m_rightChild) {
		delete m_rightChild;
		m_rightChild = NULL;
	}
}

template <class W, class S>
bool Node<W,S>::isNull() const {
	return !m_isLeaf && !m_isSplit; 
}

template <class W, class S>
bool Node<W,S>::isSplit() const {
	return m_isSplit;
}

template <class W, class S>
bool Node<W,S>::isLeaf() const {
	return m_isLeaf;
}

template <class W, class S>
void Node<W,S>::InitializeLeaf(const S& statisticsAggregator) {
	m_statisticsAggregator = statisticsAggregator;
	m_isLeaf = true;
	m_isSplit = false;
}

template <class W, class S>
void Node<W,S>::InitializeSplit(W weakLearner, double threshold) {
	m_weakLearner = weakLearner;
	m_threshold = threshold;
	m_isSplit = true;
	m_isLeaf = false;
}

template <class W, class S>
const W& Node<W,S>::GetWeakLearner() const
{
	assert( isSplit() );
	return m_weakLearner;
}

template <class W, class S>
double Node<W,S>::GetThreshold() const
{
	assert( isSplit() );
	return m_threshold;
}

template <class W, class S>
const S& Node<W,S>::GetStatistics() const
{
	assert( isLeaf() );
	return m_statisticsAggregator;
}

template <class W, class S>
S& Node<W, S>::GetStatistics()
{
	assert( isLeaf() );
	return m_statisticsAggregator;
}

template <class W, class S>
Node<W,S>* Node<W,S>::GetLeft()
{
	return m_leftChild;
}

template <class W, class S>
void Node<W,S>::SetLeft(Node<W,S>* node)
{
	m_leftChild = node;
}

template <class W, class S>
Node<W,S>* Node<W,S>::GetRight()
{
	return m_rightChild;
}

template <class W, class S>
void Node<W,S>::SetRight(Node<W,S>* node)
{
	m_rightChild = node;
}

template <class W, class S> template <class FunctorType>
const S& Node<W,S>::FastApply(FunctorType& fun) const
{
	if( m_isLeaf && !m_isSplit ) {

		return m_statisticsAggregator;

	}else if ( m_isSplit && !m_isLeaf ) {
		
		double response = m_weakLearner.GetResponse(fun);
		if (response <= m_threshold) {
		
			if (m_leftChild != NULL)
				return m_leftChild->FastApply(fun);
			else {
				std::cerr << "Empty left child" << std::endl;
				assert(false);
				exit(-1);
			}

		} else {

			if (m_rightChild != NULL)
				return m_rightChild->FastApply(fun);
			else {
				std::cerr << "Empty right child" << std::endl;
				assert(false);
				exit(-1);
			}
		}

	}else {
		std::cerr << "invalid node type" << std::endl;
		assert(false);
		exit(-1);
	}
}

template <class W, class S>
void Node<W,S>::Serialize(std::ostream& o) const
{
	o.write((const char*)(&m_isLeaf), sizeof(bool));
	o.write((const char*)(&m_isSplit), sizeof(bool));

	if (m_isLeaf && !m_isSplit)
		m_statisticsAggregator.Serialize(o);
	else if(m_isSplit && !m_isLeaf) {
		o.write((const char*)(&m_threshold), sizeof(double));
		m_weakLearner.Serialize(o);
	}else {
		std::cerr << "cannot save Null tree node" << std::endl;
		assert(false);
		exit(-1);
	}
}

template <class W, class S>
void Node<W,S>::Deserialize(std::istream& in)  {
	in.read((char *)(&m_isLeaf), sizeof(bool));
	in.read((char *)(&m_isSplit), sizeof(bool));

	if (m_isLeaf && !m_isSplit)
		m_statisticsAggregator.Deserialize(in);
	else if(m_isSplit && !m_isLeaf) {
		in.read((char *)(&m_threshold), sizeof(double));
		m_weakLearner.Deserialize(in);
	}else {
		std::cerr << "unrecognized node type" << std::endl;
		assert(false);
		exit(-1);
	}
}
    
} } }


#endif
