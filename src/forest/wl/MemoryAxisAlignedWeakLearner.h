//
//  MemoryAxisAlignedWeakLearner.h
//  FISH
//
//  Created by Yaozong Gao on 6/25/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __MemoryAxisAlignedWeakLearner_h__
#define __MemoryAxisAlignedWeakLearner_h__


#include "forest/interfaces.h"
#include "common/Random.h"
#include "forest/DataCollection.h"

namespace BRIC { namespace IDEA { namespace FISH {


/** @brief extract axis features from memory
 *  
 * works together with MemoryDataCollection
 * @see MemoryDataCollection
 */
class MemoryAxisAlignedWeakLearner {

private:

	unsigned int m_featureIndex;

public:
    
	/** @brief constructor */
    MemoryAxisAlignedWeakLearner(): m_featureIndex(0) {}

	/** @brief constructor */
	MemoryAxisAlignedWeakLearner(unsigned int featureIndex): m_featureIndex(featureIndex) {}

    // designed for common interface
	/** @brief get the features used in this weak learner */
	inline void GetFeatureIndexes(std::vector<unsigned int>& indexes) const {
		indexes.resize(1);
		indexes[0] = m_featureIndex;
	}

	/** @brief get response using a functor object
	 */
	template <typename FunctorType>
	inline double GetResponse(FunctorType& fun) const {
		return fun(m_featureIndex);
	}


	// interface for training
	/** @brief get responses of a set of training samples from dataCollection
	 *
	 * input should be a MemoryDataCollection 
	 */
	virtual void GetResponses(const IDataCollection* dataCollection, const unsigned int* dataIdxs, unsigned int numData, double* responses) const;
    
	/** @brief get a random weaker learner */
    static MemoryAxisAlignedWeakLearner CreateRandomFeature(unsigned int featureNumber, Random& random) {
        
        MemoryAxisAlignedWeakLearner weakLeaner( random.Next<unsigned int>(0, featureNumber-1) );
        return weakLeaner;
    }
    
    // IO operators
	/** @brief serialize operator */
    void Serialize(std::ostream& o) const {
        o.write((const char*)(&m_featureIndex), sizeof(unsigned int));
    }
    
	/** @brief deserialize operator */
    void Deserialize(std::istream& in) const {
        in.read((char*)(&m_featureIndex), sizeof(unsigned int));
    }
    
	/** @brief standard output operator */
    friend std::ostream& operator<< (std::ostream& os, const MemoryAxisAlignedWeakLearner& weakLearner);

	//
	// class-specific member functions
	// 

	/** @brief get selected feature index */
	unsigned int GetSelectedFeatureIndex() const {
		return m_featureIndex;
	}

public:
    
	// share the same type as DiskAxisAlignedWeakLearner
	// even training is done using MemoryAxisAlignedWeakLearner
	// the trained result can still be used for DiskAxisAlignedWeakLearner
    static const char* BinaryHeader;
};


//////////////////////////////////////////////////////////////////////////
// Implementations
//////////////////////////////////////////////////////////////////////////

void MemoryAxisAlignedWeakLearner::GetResponses(const IDataCollection* dataCollection, const unsigned int* dataIdxs, unsigned int numData, double* responses) const {

	const MemoryDataCollection* memoryData = static_cast<const MemoryDataCollection*>(dataCollection);
	assert(m_featureIndex < memoryData->GetFeatureNumber());

	for (unsigned int i = 0; i < numData; ++i)
	{
		assert(dataIdxs[i] < memoryData->GetSampleNumber());
		const double* sample = memoryData->GetData(dataIdxs[i]);
		responses[i] = sample[m_featureIndex];
	}
}

std::ostream& operator<< (std::ostream& os, const MemoryAxisAlignedWeakLearner& weakLearner) 
{
	os << "(" << weakLearner.m_featureIndex << ")";
	return os;
}

const char* MemoryAxisAlignedWeakLearner::BinaryHeader = "BRIC.IDEA.RegressionForest.AxisAlignedWeakLearner";


} } }


#endif

