//
//  ClassificationTrainingContext.h
//  FISH
//
//  Created by Yaozong Gao on 6/25/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __ClassificationTrainingContext_h__
#define __ClassificationTrainingContext_h__

#include "forest/interfaces.h"
#include "forest/stat/HistogramStatisticsAggregator.h"
#include "forest/Forest.h"
#include "common/Random.h"
#include <vector>

namespace BRIC { namespace IDEA { namespace FISH {

/** @brief  training context for classification */
template <class W>
class ClassificationTrainingContext : public ITrainingContext<W, HistogramStatisticsAggregator>
{
public:

	ClassificationTrainingContext(unsigned int featureNumber, unsigned int classNumber) {
		m_featureNumber = featureNumber;
		m_classNumber = classNumber;
	}

	inline HistogramStatisticsAggregator GetStatisticsAggregator() const {
		return HistogramStatisticsAggregator(m_classNumber);
	}

	inline W GetRandomWeakLearner(Random& random) {
		return W::CreateRandomFeature(m_featureNumber, random);
	}

private:

	unsigned int m_featureNumber;
	unsigned int m_classNumber; 
};


} } } 

#endif
