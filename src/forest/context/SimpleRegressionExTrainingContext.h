//
//  SimpleRegressionExTrainingContext.h
//  FISH
//
//  Created by Yaozong Gao on 03/26/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __SimpleRegressionExTrainingContext_h__
#define __SimpleRegressionExTrainingContext_h__

#include "forest/interfaces.h"
#include "forest/wl/MemoryAxisAlignedWeakLearner.h"
#include "forest/stat/SimpleRegressionStatisticsAggregator.h"
#include "forest/Forest.h"
#include "common/Random.h"
#include <vector>

namespace BRIC { namespace IDEA { namespace FISH {

/** @brief simple regression training context for exhaust search of weak learners
*
* features of this training context includes:
* 1) exhaust search over all selected features (pull random feature selection out of training procedure)
* NOTE: For this class to work properly, set treeParameters.numOfRandomWeakLearners = number of features
*/
class SimpleRegressionExTrainingContext : public ITrainingContext<MemoryAxisAlignedWeakLearner, SimpleRegressionStatisticsAggregator>
{
public:

	/** @brief constructor */
	SimpleRegressionExTrainingContext(unsigned int numFeatures, unsigned int dimension) {
		m_dimension = dimension;
		m_numFeatures = numFeatures;
		m_featureIndex = 0;
	}

	/** @brief get statistics aggregator */
	SimpleRegressionStatisticsAggregator GetStatisticsAggregator() const {
		return SimpleRegressionStatisticsAggregator(m_dimension);
	}

	/** @brief exhaust search over all candidate features */
	MemoryAxisAlignedWeakLearner GetRandomWeakLearner(Random& random) {

		MemoryAxisAlignedWeakLearner weakLearner(m_featureIndex);
		m_featureIndex = (m_featureIndex + 1) % m_numFeatures;

		return weakLearner;
	}

private:

	unsigned int m_dimension;								// target regression dimension
	unsigned int m_numFeatures;								// number of features
	unsigned int m_featureIndex;							// next weak learner to be drawn
};

} } }

#endif

