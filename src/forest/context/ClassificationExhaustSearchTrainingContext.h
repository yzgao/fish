//
//  ClassificationExhaustSearchTrainingContext.h
//  FISH
//
//  Created by Yaozong Gao on 7/12/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __ClassificationExhaustSearchTrainingContext_h__
#define __ClassificationExhaustSearchTrainingContext_h__

#include "forest/interfaces.h"
#include "forest/wl/MemoryAxisAlignedWeakLearner.h"
#include "forest/stat/HistogramStatisticsAggregator.h"
#include "forest/Forest.h"
#include "common/Random.h"
#include <vector>

namespace BRIC { namespace IDEA { namespace FISH {

/** @brief classification training context for exhaust search of weak learners
 *
 * features of this training context includes:
 * 1) exhaust search over all selected features (pull random feature selection out of training procedure)
 * NOTE: For this class to work properly, set treeParameters.numOfRandomWeakLearners = number of features
 */
class ClassificationExhaustSearchTrainingContext: public ITrainingContext<MemoryAxisAlignedWeakLearner, HistogramStatisticsAggregator>
{
public:
    
    /** @brief constructor */
    ClassificationExhaustSearchTrainingContext(unsigned int numFeatures, unsigned int numClasses) {
		m_classNumber = numClasses;
        m_numFeatures = numFeatures;
        m_featureIndex = 0;
    }
    
    /** @brief get statistics aggregator */
    HistogramStatisticsAggregator GetStatisticsAggregator() const {
        return HistogramStatisticsAggregator(m_classNumber);
    }
    
    /** @brief exhaust search over all candidate features */
    MemoryAxisAlignedWeakLearner GetRandomWeakLearner(Random& random) {
        
        MemoryAxisAlignedWeakLearner weakLearner(m_featureIndex);
        m_featureIndex = (m_featureIndex + 1) % m_numFeatures;
        
        return weakLearner;
    }
    
private:
    
	unsigned int m_classNumber;								// number of classes
	unsigned int m_numFeatures;								// number of features
    unsigned int m_featureIndex;							// next weak learner to be drawn
};

} } }

#endif

