//
//  SimpleRegressionRandTrainingContext.h
//  FISH
//
//  Created by Yaozong Gao on 05/14/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __SimpleRegressionRandTrainingContext_h__
#define __SimpleRegressionRandTrainingContext_h__

#include "forest/interfaces.h"
#include "forest/wl/MemoryAxisAlignedWeakLearner.h"
#include "forest/stat/SimpleRegressionStatisticsAggregator.h"
#include "forest/Forest.h"
#include "common/Random.h"
#include <vector>

namespace BRIC { namespace IDEA { namespace FISH {

/** @brief simple regression training context for random search of weak learners
*/
class SimpleRegressionRandTrainingContext : public ITrainingContext<MemoryAxisAlignedWeakLearner, SimpleRegressionStatisticsAggregator>
{
public:

	/** @brief constructor */
	SimpleRegressionRandTrainingContext(unsigned int numFeatures, unsigned int dimension) {
		m_dimension = dimension;
		m_numFeatures = numFeatures;
	}

	/** @brief get statistics aggregator */
	SimpleRegressionStatisticsAggregator GetStatisticsAggregator() const {
		return SimpleRegressionStatisticsAggregator(m_dimension);
	}

	/** @brief random search over all candidate features */
	MemoryAxisAlignedWeakLearner GetRandomWeakLearner(Random& random) {
		return MemoryAxisAlignedWeakLearner::CreateRandomFeature(m_numFeatures, random);
	}

private:

	unsigned int m_dimension;								// target regression dimension
	unsigned int m_numFeatures;								// number of features
};

} } }

#endif

