//
//  CRTrainingContext.h
//  FISH
//
//  Created by Yaozong Gao on 1/7/15.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __CRTrainingContext_h__
#define __CRTrainingContext_h__

#include "forest/interfaces.h"
#include "forest/wl/MemoryAxisAlignedWeakLearner.h"
#include "forest/stat/CRStatisticsAggregator.h"
#include "forest/Forest.h"
#include "common/Random.h"
#include <vector>

namespace BRIC { namespace IDEA { namespace FISH {

/** @brief classification and regression training context for exhaust search of weak learners
*
* features of this training context includes:
* 1) exhaust search over all selected features (pull random feature selection out of training procedure)
* NOTE: For this class to work properly, set treeParameters.numOfRandomWeakLearners = number of features
*/
class CRTrainingContext : public ITrainingContext < MemoryAxisAlignedWeakLearner, CRStatisticsAggregator >
{
public:

	/** @brief constructor */
	CRTrainingContext(unsigned int numFeatures, unsigned int dimension, unsigned int classNumber, double rootEntropy, double rootMeanVar, double ratio = 0.5) {

		assert_message(rootEntropy > 0 && rootMeanVar > 0, "root entropy and mean variance must be positive");
		assert_message(ratio >= 0 && ratio <= 1, "ratio must be [0,1]");

		m_dimension = dimension;
		m_classNumber = classNumber;
		m_numFeatures = numFeatures;
		m_featureIndex = 0;

		m_rootEntropy = rootEntropy;
		m_rootMeanVar = rootMeanVar;

		m_ratio = ratio;
	}

	/** @brief get statistics aggregator */
	CRStatisticsAggregator GetStatisticsAggregator() const {
		return CRStatisticsAggregator(m_dimension, m_classNumber);
	}

	/** @brief exhaust search over all candidate features */
	MemoryAxisAlignedWeakLearner GetRandomWeakLearner(Random& random) {

		MemoryAxisAlignedWeakLearner weakLearner(m_featureIndex);
		m_featureIndex = (m_featureIndex + 1) % m_numFeatures;

		return weakLearner;
	}

	/** @brief override the information gain function */
	virtual double ComputeInformationGain(const CRStatisticsAggregator& parentStatistics, const CRStatisticsAggregator& leftChildStatistics, const CRStatisticsAggregator& rightChildStatistics) const
	{
		// compute total number of samples
		unsigned int nTotalSamples = leftChildStatistics.GetSampleNumber() + rightChildStatistics.GetSampleNumber();

		// for classification
		double classificationGain = 0;

		if (m_ratio > 0)
		{
			double entropyBefore = parentStatistics.Entropy();
			double entropyLeft = leftChildStatistics.Entropy();
			double entropyRight = rightChildStatistics.Entropy();
			double entropyAfter = (leftChildStatistics.GetSampleNumber() * 1.0 / nTotalSamples) * entropyLeft
				+ (rightChildStatistics.GetSampleNumber() * 1.0 / nTotalSamples) * entropyRight;

			classificationGain = (entropyBefore - entropyAfter) / m_rootEntropy;
		}

		// for regression
		double regressionGain = 0;

		if (m_ratio < 1)
		{
			double varBefore = parentStatistics.MeanVariance();
			double varLeft = leftChildStatistics.MeanVariance();
			double varRight = rightChildStatistics.MeanVariance();
			double varAfter = (leftChildStatistics.GetSampleNumber() * 1.0 / nTotalSamples) * varLeft
				+ (rightChildStatistics.GetSampleNumber() * 1.0 / nTotalSamples) * varRight;

			regressionGain = (varBefore - varAfter) / m_rootMeanVar;
		}

		// combination
		double informationGain = m_ratio * classificationGain + (1 - m_ratio) * regressionGain;
		return informationGain;
	}

private:

	unsigned int m_dimension;								// target regression dimension
	unsigned int m_classNumber;								// class number
	unsigned int m_numFeatures;								// number of features
	unsigned int m_featureIndex;							// next weak learner to be drawn

	double m_ratio;											// classification and regression balance ratio

	double m_rootEntropy;									// root entropy for normalization
	double m_rootMeanVar;									// root mean variation for normalization
};

} } }

#endif

