//
//  DataCollection.h
//  FISH
//
//  Created by Yaozong Gao on 4/4/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __DataCollection_h__
#define __DataCollection_h__


#include "forest/interfaces.h"
#include <vector>
#include <fstream>
#include <iostream>


namespace BRIC { namespace IDEA { namespace FISH {


/** @brief DataCollectionIO class
 *
 * includes IO operations, e.g., load/save matrix, load/save label
 *
 */
class DataCollectionIO {
  
public:
    
    /** @brief load matrix from file
	 *
     * the file contains two integers in the first 8 bytes
     * first integer indicates rows, second integer indicates cols
	 */
    template <typename T>
    static bool LoadMatrix(const char* path, T* buffer, size_t bufferSize);
    
    /** @ load matrix info (i.e., rows and cols) from file
     * 
	 * read matrix file header information
	 */
    static bool LoadMatrixInfo(const char* path, unsigned int& rows, unsigned int& cols);
    
    /** @brief write matrix to disk
     *
	 * columewise organized
	 */
    template <typename T>
    static bool WriteMatrix(const std::vector<T>& data, unsigned int rows, unsigned int cols, const char* path);
    
    
    /** @brief load label info (i.e., numClasses and numSamples) from file
     *
	 * read label file header information
	 */
    static bool LoadLabelInfo(const char* path, unsigned int& numClasses, unsigned int& numSamples);
    
    
    /** @brief load label from file
	 */
    static bool LoadLabel(const char* path, int* buffer, size_t bufferSize);
    
};


/** @brief DataCollection for pre-loaded training data (double-type)
 * 
 * use to manage the training data loaded into memory.
 */
class MemoryDataCollection: public IDataCollection {
  
public:
    
	/** @brief constructor */
    MemoryDataCollection() {
        m_sampleNumber = m_featureNumber = m_classNumber = m_targetDim = 0;
    }
    
	/** @brief destructor */
    ~MemoryDataCollection() {
        Clear();
    }
    
    /** @brief get sample number */
    unsigned int GetSampleNumber() const {  return m_sampleNumber;  }
	/** @brief get feature dimension */
    unsigned int GetFeatureNumber() const {  return m_featureNumber; }
	/** @brief get class number */
    unsigned int GetClassNumber() const { return m_classNumber; }
	/** @brief get target dimension */
	unsigned int GetTargetDimension() const { return m_targetDim; }
    

	/** @brief load training data from file
	 */
	bool LoadDataFromFile(const char* dataFilePath);

	/** @brief load labels from file (for classification) */
	bool LoadLabelsFromFile(const char* labelFilePath);

	/** @brief load targets from file (for regression) */
	bool LoadTargetsFromFile(const char* targetsFilePath);

	/** @brief clear all training data*/
    void Clear();

    /** @brief get a single data pointed by idx
	 *
     * return a internal address to avoid memory reallocation
	 */
    const double* GetData(unsigned int idx) const {
        return &m_data[ static_cast<size_t>(idx) * m_featureNumber ];
    }

	/** @brief has any classification label? */
	bool HasLabel() const {
		return m_classNumber != 0;
	}
    
    /** @brief get the label for a specific data */
    int GetLabel(unsigned int idx) const {
		return m_labels[idx];
    }

	/** @brief has regression target? */
	bool HasTarget() const {
		return m_targetDim != 0;
	}

	/** @brief get target for a training data */
	const double* GetTarget(unsigned int idx) const {
		return &m_targets[ static_cast<size_t>(idx) * m_targetDim ];
	}
    
    /** @brief get value range for dimension */
    void GetRange(unsigned int dim, double& min, double& max) const;
    
    
    /** @brief check whether the object is loaded correctly */
    bool isValid() const {
        return m_sampleNumber != 0 && m_featureNumber != 0;
    }
    

    // 
	// manually manipulate DataCollection object using the following operations
	// advanced use (be cautious)
	//

	/** @brief get data vector */
	std::vector<double>& GetDataVector() {
		return m_data;
	}

	/** @brief get data vector */
	const std::vector<double>& GetDataVector() const {
		return m_data;
	}

	/** @brief set sample number */
	void SetSampleNumber(unsigned int sampleNumber) {
		m_sampleNumber = sampleNumber;
	}

	/** @brief set feature dimension */
	void SetFeatureNumber(unsigned int featureNumber) {
		m_featureNumber = featureNumber;
	}

	/** @brief get label vector */
	std::vector<int>& GetLabelVector() {
		return m_labels;
	}

	/** @brief get label vector */
	const std::vector<int>& GetLabelVector() const {
		return m_labels;
	}

	/** @brief set class number */
	void SetClassNumber(unsigned int classNumber) {
		m_classNumber = classNumber;
	}

	/** @brief get target vector */
	std::vector<double>& GetTargetVector() {
		return m_targets;
	}

	/** @brief get target vector */
	const std::vector<double>& GetTargetVector() const {
		return m_targets;
	}

	/** @brief get target dimension */
	void SetTargetDim(unsigned int targetDim) {
		m_targetDim = targetDim;
	}


private:
    
    std::vector< double > m_data;
    
    unsigned int m_sampleNumber;
    unsigned int m_featureNumber;

	// class labels (classification)
	std::vector< int > m_labels;
    unsigned int m_classNumber;
    
	// target values (regression)
	std::vector< double > m_targets;
	unsigned int m_targetDim;

};




//
// Implementations
// 

bool DataCollectionIO::LoadMatrixInfo(const char* path, unsigned int& rows, unsigned int& cols) {

	std::ifstream in (path, std::ios::binary);

	if (! in.read((char*)&rows, sizeof(unsigned int)) ) {
		std::cerr << "fails to read matrix information from file: " << path << std::endl;
		return false;
	}

	if (! in.read((char*)&cols, sizeof(unsigned int)) ) {
		std::cerr << "fails to read matrix information from file: " << path << std::endl;
		return false;
	}

	in.close();
	return true;
}

bool DataCollectionIO::LoadLabelInfo(const char* path, unsigned int& numClasses, unsigned int& numSamples) {

	std::ifstream in (path, std::ios::binary);

	if (! in.read((char*)&numClasses, sizeof(unsigned int)) ) {
		std::cerr << "fails to read label information from file: " << path << std::endl;
		return false;
	}

	if (! in.read((char*)&numSamples, sizeof(unsigned int)) ) {
		std::cerr << "fails to read label information from file: " << path << std::endl;
		return false;
	}

	in.close();
	return true;
}

bool DataCollectionIO::LoadLabel(const char* path, int* buffer, size_t bufferSize) {

	std::ifstream in (path, std::ios::binary);

	unsigned int numClasses = 0, numSamples = 0;
	if (! in.read((char*)&numClasses, sizeof(unsigned int)) ) {
		std::cerr << "fails to read label information from file: " << path << std::endl;
		return false;
	}

	if (! in.read((char*)&numSamples, sizeof(unsigned int)) ) {
		std::cerr << "fails to read label information from file: " << path << std::endl;
		return false;
	}

	if (bufferSize != numSamples) {
		std::cerr << "label sample number inconsistent with allocated buffer size, check it!" << std::endl;
		return false;
	}

	if (! in.read((char*)buffer, sizeof(int) * bufferSize) ) {
		std::cerr << "fails to load labels! " << std::endl;
		return false;
	}

	in.close();

	// verify loaded label 
	// whether they are within 0 - numClasses-1
	for (unsigned int i = 0; i < numSamples; ++i) {
		if ( buffer[i] < 0 || buffer[i] >= static_cast<int>(numClasses) ) {
			std::cerr << "label verification fails... label index out of boundary!" << std::endl;
			return false;
		}
	}

	return true;
}

template <typename T>
bool DataCollectionIO::LoadMatrix(const char* path, T* buffer, size_t bufferSize) {

	std::ifstream in (path, std::ios::binary);

	unsigned int rows = 0, cols = 0;
	if (! in.read((char*)&rows, sizeof(unsigned int)) ) {
		std::cerr << "fails to read matrix information from file: " << path << std::endl;
		return false;
	}

	if (! in.read((char*)&cols, sizeof(unsigned int)) ) {
		std::cerr << "fails to read matrix information from file: " << path << std::endl;
		return false;
	}

	if (bufferSize != static_cast<size_t>(rows) * cols) {
		std::cerr << "matrix size inconsistent with allocated buffer size, check it!" << std::endl;
		return false;
	}

	if (! in.read((char*)buffer, sizeof(T) * bufferSize) ) {
		std::cerr << "fails to load matrix! " << std::endl;
		return false;
	}

	in.close();
	return true;
}

template <typename T>
bool DataCollectionIO::WriteMatrix(const std::vector<T>& data, unsigned int rows, unsigned int cols, const char* path) {

	std::ofstream out(path, std::ios::binary);

	if (! out.write((char*)&rows, sizeof(unsigned int)) ) {
		std::cerr << "fails to write matrix information to file: " << path << std::endl;
		return false;
	}

	if (! out.write((char*)&cols, sizeof(unsigned int)) ) {
		std::cerr << "fails to write matrix information to file: " << path << std::endl;
		return false;
	}

	if (! out.write((char*)&data[0], sizeof(T) * data.size()) ) {
		std::cerr << "fails to write matrix to file: " << path << std::endl;
		return false;
	}

	out.close();
	return true;
}



//
// MemoryDataCollection
// 

bool MemoryDataCollection::LoadDataFromFile(const char* dataFilePath) {

	// load data
	unsigned int rows = 0, cols = 0;
	if ( ! DataCollectionIO::LoadMatrixInfo(dataFilePath, rows, cols) ) {
		std::cerr << "fails to load data header!" << std::endl;
		return false;
	}

	m_data.resize(static_cast<size_t>(rows) * cols);
	if ( ! DataCollectionIO::LoadMatrix( dataFilePath, &m_data[0], m_data.size() ) ) {
		std::cerr << "fails to load data!" << std::endl;
		m_data.clear();
		return false;
	}

	m_sampleNumber = cols;
	m_featureNumber = rows;

	return true;
}


bool MemoryDataCollection::LoadLabelsFromFile(const char* labelFilePath) {

	if (m_sampleNumber == 0)
	{
		std::cerr << "Please load data file first." << std::endl;
		return false;
	}

	// load labels
	unsigned int numClasses = 0, numSamples = 0;
	if ( ! DataCollectionIO::LoadLabelInfo(labelFilePath, numClasses, numSamples) ) {
		std::cerr << "fails to load label header!" << std::endl;
		return false;
	}

	// check label number equals data samples.
	if (m_sampleNumber != numSamples)
	{
		std::cerr << "label number is inconsistent with sample number" << std::endl;
		return false;
	}

	m_labels.resize(numSamples);
	if ( ! DataCollectionIO::LoadLabel(labelFilePath, &m_labels[0], numSamples )) {
		std::cerr << "fails to load labels!" << std::endl;
		m_labels.clear();
		return false;
	}

	m_classNumber = numClasses;

	return true;
}


bool MemoryDataCollection::LoadTargetsFromFile(const char* targetsFilePath) {

	if (m_sampleNumber == 0)
	{
		std::cerr << "Please load data file first." << std::endl;
		return false;
	}

	// load data
	unsigned int numTargets = 0, numSamples = 0;
	if ( ! DataCollectionIO::LoadMatrixInfo(targetsFilePath, numTargets, numSamples) ) {
		std::cerr << "fails to load data header!" << std::endl;
		return false;
	}

	// check label number equals data samples.
	if (m_sampleNumber != numSamples)
	{
		std::cerr << "target sample number is inconsistent with sample number" << std::endl;
		return false;
	}

	m_targets.resize( static_cast<size_t>(numTargets) * numSamples );
	if ( ! DataCollectionIO::LoadMatrix( targetsFilePath, &m_targets[0], m_targets.size() ) ) {
		std::cerr << "fails to load data!" << std::endl;
		m_targets.clear();
		return false;
	}

	m_targetDim = numTargets;

	return true;
}


void MemoryDataCollection::Clear() {

	m_data.clear();
	m_labels.clear();
	m_targets.clear();
	m_sampleNumber = m_featureNumber = m_classNumber = m_targetDim = 0;

}


void MemoryDataCollection::GetRange(unsigned int dim, double& min, double& max) const {

	if ( dim >= m_featureNumber || !isValid() ) {
		std::cerr << "dimension requested exceeded!" << std::endl;
		assert(false);
		exit(-1);
	}else {

		min = std::numeric_limits<double>::max();
		max = -std::numeric_limits<double>::max();

		size_t maxsize = static_cast<size_t>(m_sampleNumber)* m_featureNumber;

		for (size_t i = dim; i < maxsize; i = i + m_featureNumber) {
			if (m_data[i] < min)
				min = m_data[i];
			if (m_data[i] > max) 
				max = m_data[i];
		}

	}
}

} } } // end namespace
    
#endif
