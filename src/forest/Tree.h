//
//  Tree.h
//  FISH
//
//  Created by Yaozong Gao on 4/4/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __Tree_h__
#define __Tree_h__

#include "Node.h"
#include <vector>
#include <map>
#include <queue>
#include <iostream>
#include <assert.h>
#include <memory>
#include <string>
#include <cstring>

namespace BRIC { namespace IDEA { namespace FISH {


/** @brief binary decision tree
 *
 */
template <class W, class S>
class Tree
{
public:
	typedef Node<W,S> NodeType;
	
	/** @brief constructor and destructor */
	Tree(NodeType* root);
	~Tree();

	/** @brief set and get unique id */
	void SetUniqueID(double ID);
	double GetUniqueID() const;

	/** @brief Get the number of valid nodes*/
	size_t GetTotalNodeNumber() const;

	/** @brief Get root node */
	NodeType* Root();

	/** @brief Get any statistical distribution */
	const S& GetAnyLeafStatisticsAggregator() const;

	/** @brief Count the times of features appearing in the tree nodes */
	void CountFeatures(std::map<unsigned int, size_t>& featureCounts) const;

	/** @brief get tree depth */
	int TreeDepth() const;

	/** @brief retrieve the statistics aggregator in the final leaf node */
	template <class FunctorType>
	const S& FastApply(FunctorType& fun) const;

	/** @brief check validity of tree */
	void CheckValid() const;

	/** @brief serialize operator */
	void Serialize(std::ostream& o) const;
    
	/** @brief deserialize operator */
    static std::auto_ptr< Tree<W,S> > Deserialize(std::istream& in);

private:

	static NodeType* ReadNode(std::istream& in);
	int TreeDepth_(NodeType* root) const;

	NodeType* m_root;
	double m_uniqueID;

	static const char* BinaryHeader; 
};

//////////////////////////////////////////////////////////////////////////

template <class W, class S>
Tree<W,S>::Tree(NodeType* root)
{
	m_root = root;
}

template <class W, class S>
Tree<W,S>::~Tree() 
{
	if (m_root)
	{
		delete m_root;
		m_root = NULL;
	}
	m_uniqueID = 0;
}

template <class W, class S>
void Tree<W,S>::SetUniqueID(double ID)
{
	m_uniqueID = ID;
}

template <class W, class S>
double Tree<W,S>::GetUniqueID() const
{
	return m_uniqueID;
}

template <class W, class S>
size_t Tree<W,S>::GetTotalNodeNumber() const
{
	if (m_root == NULL)
		return 0;

	size_t count = 0;

	std::queue<NodeType*> nodes;
	count = 1;

	NodeType* child = m_root->GetLeft();
	if (child)
		nodes.push(child);
	child = m_root->GetRight();
	if (child)
		nodes.push(child);

	while (nodes.size())
	{
		NodeType* tmp = nodes.front();
		nodes.pop();
		++ count;

		child = tmp->GetLeft();
		if (child)
			nodes.push(child);
		child = tmp->GetRight();
		if (child)
			nodes.push(child);
	}

	return count;
}

template <class W, class S>
typename Tree<W,S>::NodeType* Tree<W,S>::Root()
{
	return m_root;
}

template <class W, class S>
const S& Tree<W,S>::GetAnyLeafStatisticsAggregator() const
{
	if (m_root == NULL) {
		std::cerr << "empty tree for retrieves statistics aggregator?" << std::endl;
		exit(-1);
	}

	NodeType* tmp = m_root;
	while (tmp->isSplit()) {
		if (tmp->GetLeft() == NULL) {
			std::cerr << "children of split nodes cannot be NULL" << std::endl;
			exit(-1);
		}
		tmp = tmp->GetLeft();
	}

	if (!tmp->isLeaf()) {
		std::cerr << "unexpectedly fall into a NULL node" << std::endl;
		exit(-1);
	}

	return tmp->GetStatistics();
}

template <class W, class S>
void Tree<W,S>::CountFeatures(std::map<unsigned int, size_t>& featureCounts) const
{
	if (m_root == NULL || !m_root->isSplit())
		return;

	std::queue<NodeType*> nodes;
	nodes.push(m_root);

	while (nodes.size() != 0)
	{
		NodeType* tmp = nodes.front();
		nodes.pop();

		if (tmp->isSplit())
		{
			std::vector<unsigned int> selectedFeatureIndices;
			const W& weakLearner = tmp->GetWeakLearner();
			weakLearner.GetFeatureIndexes(selectedFeatureIndices);

			for(unsigned int j = 0; j < selectedFeatureIndices.size(); ++j) {
				unsigned int featureIndex = selectedFeatureIndices[j];
				if (featureCounts.count(featureIndex) > 0)
					featureCounts[featureIndex] += 1;
				else
					featureCounts[featureIndex] = 1;
			}

			NodeType* child = NULL;
			if ( (child = tmp->GetLeft()) ) nodes.push(child);
			if ( (child = tmp->GetRight()) ) nodes.push(child);
		}
	}
}

template <class W, class S>
int Tree<W,S>::TreeDepth_(NodeType* root) const
{
	if (root == NULL)
		return 0;
	return std::max(TreeDepth_(root->GetLeft())+1, TreeDepth_(root->GetRight())+1);
}

template <class W, class S>
int Tree<W,S>::TreeDepth() const 
{
	return TreeDepth_(m_root);
}

template <class W, class S> template <class FunctorType>
const S& Tree<W,S>::FastApply(FunctorType& fun) const
{
	if (m_root == NULL) {
		std::cerr << "cannot apply with empty tree" << std::endl;
		exit(-1);
	}

	return m_root->FastApply(fun);
}

template <class W, class S>
void Tree<W,S>::CheckValid() const
{
	if (m_root == NULL) {
		std::cerr << "empty tree" << std::endl;
		exit(-1);
	}

	std::queue<NodeType*> nodes;
	nodes.push(m_root);

	while (nodes.size() != 0)
	{
		NodeType* tmp = nodes.front();
		nodes.pop();

		if (tmp->isSplit())
		{
			if (tmp->GetLeft() == NULL || tmp->GetRight() == NULL)
			{
				std::cerr << "split node must have two non-empty children" << std::endl;
				exit(-1);
			}

			NodeType* child = NULL;
			if ( (child = tmp->GetLeft()) ) nodes.push(child);
			if ( (child = tmp->GetRight()) ) nodes.push(child);
		}

		if (tmp->isLeaf()) 
		{
			if(tmp->GetLeft() != NULL || tmp->GetRight() != NULL) 
			{
				std::cerr << "leaf node must have two empty children" << std::endl;
				exit(-1);
			}
		}
	}
}

template <class W, class S>
void Tree<W,S>::Serialize(std::ostream& o) const
{
	std::string header( Tree<W,S>::BinaryHeader );
	header = header + "|" + W::BinaryHeader + "|" + S::BinaryHeader;
	o.write(header.c_str(), header.length());

	// save tree using breath-first order
	std::queue<NodeType*> nodes;
	nodes.push(m_root);

	const char nulltag = 0;
	const char normaltag = 1;

	while (nodes.size() != 0)
	{
		NodeType* tmp = nodes.front();
		nodes.pop();

		if (tmp == NULL) {
			o.write(&nulltag, sizeof(char));
		}else {
			o.write(&normaltag, sizeof(char));
			tmp->Serialize(o);

			nodes.push(tmp->GetLeft());
			nodes.push(tmp->GetRight());
		}
	}
}

template <class W, class S>
typename Tree<W,S>::NodeType* Tree<W,S>::ReadNode(std::istream& in)
{
	const char nulltag = 0;
	const char normaltag = 1;
	char tag = -1;

	in.read(&tag, sizeof(char));

	if (tag != 0 && tag != 1) {
		std::cerr << "unrecognized node tag during read" << std::endl;
		assert(false);
		exit(-1);
	}

	if (tag == 0)
		return NULL;
	else { // tag == 1
		NodeType* node = new NodeType;
		node->Deserialize(in);
		return node;
	}
}

template <class W, class S>
std::auto_ptr< Tree<W,S> > Tree<W,S>::Deserialize(std::istream& in)
{
	std::string header( Tree<W,S>::BinaryHeader );
	header = header + "|" + W::BinaryHeader + "|" + S::BinaryHeader;

	unsigned int headerLen = static_cast<unsigned int>(header.length());
	std::auto_ptr<char> loadedHeader(new char[headerLen]);
	in.read(loadedHeader.get(), headerLen);

	if (strncmp(header.c_str(), loadedHeader.get(), headerLen) == 0) {

		NodeType* root = ReadNode(in);
		if (root == NULL)
		{
			std::auto_ptr< Tree<W,S> > tree ( new Tree<W,S>(NULL) );
			return tree;
		}
	
		std::queue<NodeType*> nodes;
		nodes.push(root);

		while(nodes.size() != 0)
		{
			NodeType* tmp = nodes.front();
			nodes.pop();

			NodeType* left = ReadNode(in);
			NodeType* right = ReadNode(in);

			tmp->SetLeft(left);
			tmp->SetRight(right);

			if (left) nodes.push(left);
			if (right) nodes.push(right);
		}

		std::auto_ptr< Tree<W,S> > tree ( new Tree<W,S>(root) );
		return tree;

	}else {
		std::cerr << "Loading from wrong file type" << std::endl;
		std::cerr << "Expected Header: " << header << std::endl;
		std::cerr << "Loaded Header: " << loadedHeader.get() << std::endl; 
		assert(false);
		exit(-1);
	}   
}

template <typename W, typename S>
const char* Tree<W,S>::BinaryHeader = "BRIC.IDEA.RandomForest.Tree";
    
    
} } }


#endif
