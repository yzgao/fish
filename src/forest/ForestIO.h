//
//  ForestIO.h
//  Fish
//
//  Created by Yaozong Gao on 6/6/13.
//
//

#ifndef __ForestIO_h__
#define __ForestIO_h__

#include "stdafx.h"

#include "forest/Tree.h"
#include "forest/Forest.h"

//#include <boost/filesystem/path.hpp>
//#include <boost/filesystem/operations.hpp>


namespace BRIC { namespace IDEA { namespace FISH {

/** @brief provide IO to save and load forest from folder */
template <class W, class S>
class ForestIO {
public:
    
    typedef Tree<W,S> TreeType;
    typedef Forest<W,S> ForestType;
    
    /** @brief save forest as individual trees into a folder */
    static bool SaveForestToFolder(const ForestType* forest, unsigned int resolutionIndex, const char* folder);
    
    /** @brief load forest as individual trees from a folder */
    static std::auto_ptr<ForestType> LoadForestFromFolder(const char* folder, unsigned int resolutionIndex);

	/** @brief load forest as individual trees from a folder */
	static std::auto_ptr<ForestType> LoadForestFromFolder(const char* folder);
    
private:
    
    /** @brief double to hex string */
    static std::string DoubleToHexString(double x);
    
    /** @brief return a slash character depending on the system */
    static std::string Slash() {
#ifdef _WIN32   // Windows
        return std::string("\\");
#else  // other OS
        return std::string("/");
#endif
    }
    
};


//
// Implementations
// 

template <class W, class S>
bool ForestIO<W,S>::SaveForestToFolder(const ForestType* forest, unsigned int resolutionIndex, const char* folder) {

	// check path
	boost::filesystem::path folderPath(folder);
	if( !boost::filesystem::is_directory(folderPath) )
		boost::filesystem::create_directory(folderPath);

	// save trees individually for the ease of parallel computing
	for (unsigned int i = 0; i < forest->GetTreeNumber(); ++i) {

		const TreeType* tree = forest->GetTree(i);
		std::string uniqueID = DoubleToHexString(tree->GetUniqueID());

		char treeOutPath[2048] = {0};
		sprintf(treeOutPath, "%s%sTree_R%d_%s.bin", folder, Slash().c_str(), resolutionIndex, uniqueID.c_str());

		std::ofstream out(treeOutPath, std::ios::binary | std::ios::out );
		if(!out) {
			std::cerr << "couldn\'t open " << treeOutPath << std::endl;
			return false;
		}
		forest->GetTree(i)->Serialize(out);
		out.close();
	}

	return true;
}

template <class W, class S>
std::auto_ptr< Forest<W, S> > ForestIO<W, S>::LoadForestFromFolder(const char* folder) {

	// check path
	if (!boost::filesystem::is_directory(folder)) {
		std::cerr << "the detector folder doesn\'t exist (" << folder << ")" << std::endl;
		assert(false);
		exit(-1);
	}

	char prefixBuffer[100] = { "Tree" };

	std::auto_ptr<ForestType> forest(new ForestType());

	using boost::filesystem::directory_iterator;

	directory_iterator end_iter;
	for (directory_iterator iter(folder); iter != end_iter; ++iter) {

		// not a directory
		if (!boost::filesystem::is_directory(iter->status())) {
			std::string filename = iter->path().filename().string();
			if (filename.find(prefixBuffer) != std::string::npos) {

				std::ifstream in(iter->path().string().c_str(), std::ios::binary);
				if (!in) {
					std::cerr << "couldn\'t read tree from " << iter->path().string() << std::endl;
					continue;
				}
				std::auto_ptr<TreeType> tree = TreeType::Deserialize(in);
				in.close();

				forest->AddTree(tree);
			}
		}
	}
	return forest;
}


template <class W, class S>
std::auto_ptr< Forest<W,S> > ForestIO<W,S>::LoadForestFromFolder(const char* folder, unsigned int resolutionIndex) {

	// check path
	if( !boost::filesystem::is_directory(folder) ) {
		std::cerr << "the detector folder doesn\'t exist (" << folder << ")" << std::endl;
		assert(false);
		exit(-1);
	}

	char prefixBuffer[100] = {0};
	sprintf(prefixBuffer, "Tree_R%d", resolutionIndex);

	std::auto_ptr<ForestType> forest( new ForestType() );

	using boost::filesystem::directory_iterator;

	directory_iterator end_iter;
	for (directory_iterator iter(folder); iter != end_iter; ++iter) {

		// not a directory
		if( !boost::filesystem::is_directory(iter->status()) ) {
			std::string filename = iter->path().filename().string();
			if ( filename.find(prefixBuffer) != std::string::npos ) {

				std::ifstream in(iter->path().string().c_str(), std::ios::binary);
				if(!in) {
					std::cerr << "couldn\'t read tree from " << iter->path().string() << std::endl;
					continue;
				}
				std::auto_ptr<TreeType> tree = TreeType::Deserialize(in);
				in.close();

				forest->AddTree(tree);
			}
		}
	}
	return forest;
}

template <class W, class S>
std::string ForestIO<W,S>::DoubleToHexString(double x) {

	const size_t bytesInDouble = sizeof(double);
	union {
		double value;
		unsigned char bytes[bytesInDouble];
	} u;

	u.value = x;

	char *buffer = new char[bytesInDouble * 2 + 1];
	unsigned char *input = u.bytes;
	char *output = buffer;

	for(unsigned int i = 0; i < bytesInDouble; ++i) {
		sprintf(output, "%02X", *input);
		++input;
		output += 2;
	}

	return std::string(buffer);
}


    
} } } // end namespace


#endif
