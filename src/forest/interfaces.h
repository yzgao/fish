//
//  interfaces.h
//  FISH
//
//  Created by Yaozong Gao on 4/4/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __interfaces_h__
#define __interfaces_h__


#include "common/Random.h"
#include <vector>


namespace BRIC { namespace IDEA { namespace FISH {

// interfaces for random forests
// these interfaces only define the contract in template-based pattern
// users don't need to derive from these interfaces, except for IDataCollection.

/** @brief interface for training data collection
 *
 * define interfaces for a training data collection
 */
class IDataCollection {
    
public:
    
	virtual ~IDataCollection() {}

    virtual unsigned int GetSampleNumber() const = 0;
    virtual unsigned int GetFeatureNumber() const = 0;
    virtual unsigned int GetClassNumber() const = 0;    // number of classes
    virtual unsigned int GetTargetDimension() const = 0;   // number of targets to regress
    
    virtual bool HasLabel() const = 0;
    virtual bool HasTarget() const = 0;
    
    virtual int GetLabel(unsigned int idx) const = 0;
    virtual const double* GetTarget(unsigned int idx) const = 0;
    
};


/** @brief interface for weak learner function (i.e., axis, linear)
 *
 * IWeakLearner should be a light object (easy to copy)
 *
 * Each WeakLearner class should implement a static member function for creating random feature
 * static IWeakLearner CreateRandomFeature(unsigned int featureNumber, Random& random);
 *
 * Each WeakLearner class should define a BinaryHeader static const char * variable to describe the type
 */
class IWeakLearner {
  
public:

	virtual ~IWeakLearner() {}

	/** @brief get selected feature indexes of this weak learner
	 * each weak learner can choose multiple features.
	 * for axis weak learner, only one feature is picked.
	 * for linear weak learner, multiple features can be picked (typically two for efficiency)
     */
	virtual void GetFeatureIndexes(std::vector<unsigned int>& indexes) const = 0;
    
	// separate testing interface from training for efficiency
	// In testing, classification / regression one by one is both computational and memory efficient with the support of OMP.
	// In training, training data may be not loaded. One by one fetch may cause multiple disk reads thus slowing the speed.


	// [interface for testing]
	/** @brief get response using a functor object
	 */
	template <typename FunctorType>
	double GetResponse(const FunctorType& fun) const;

    // [interface for training]
	/** @brief get responses for multiple data
     * get multiple responses together can potentially benefit from the speed, especially when data is stored in the disk
     */
    virtual void GetResponses(const IDataCollection* dataCollection, const unsigned int* dataIdxs, unsigned int numData, double* responses) const = 0;
    
};


/** @brief interface for statistics aggregator (on both split node and inner node)
 *
 * IStatisticsAggregator should be a light object
 * each class should define a BinaryHeader static const char * variable to describe the type.
 */
template <class S>
class IStatisticsAggregator {

public:

	virtual ~IStatisticsAggregator() {}

	/** @brief clear the statistics */
	virtual void Clear() = 0;
    
    /** @brief get sample number */
    virtual unsigned int GetSampleNumber() const = 0;
    
	/** @brief aggregate statistics information for additional data point */
    virtual void Aggregate(const IDataCollection* dataCollection, unsigned int dataIdx) = 0;

	/** @brief compute entropy */
	double Entropy() const;

	/** @brief aggregate statistics with another aggregator */
    virtual void Aggregate(const S& aggregator) = 0;
    
	/** @brief deep clone of statistics aggregator */
	virtual S DeepClone() const = 0;
    
    // (required)
    // define a BinaryHeader static const char * variable to describe the type
};


/** @brief interface for training context (providing information for training operator, e.g., information gain)
 */
template <class W, class S>
class ITrainingContext {

public:

	virtual ~ITrainingContext() {}

    /** @brief return an initialized statisticsAggregator */
    virtual S GetStatisticsAggregator() const = 0;
    
    /** @brief Get a random weak learner function */
    virtual W GetRandomWeakLearner(Random& random) = 0;
    
    /** @brief check whether to split or not */
    virtual bool ShouldTerminate(const S& parentStatistics, const S& leftChildStatistics, const S& rightChildStatistics, double infoGain) const {
		return false;
	}

	/** @brief whether to enable customized termination condition */
	virtual bool EnableCustomizedCondition() const {
		return false;
	}

	/** @brief compute information gain */
	virtual double ComputeInformationGain(const S& parentStatistics, const S& leftChildStatistics, const S& rightChildStatistics) const
	{
		double entropyBefore = parentStatistics.Entropy();
		unsigned int nTotalSamples = leftChildStatistics.GetSampleNumber() + rightChildStatistics.GetSampleNumber();

		double entropyLeft = leftChildStatistics.Entropy();
		double entropyRight = rightChildStatistics.Entropy();
		double entropyAfter = (leftChildStatistics.GetSampleNumber() * 1.0 / nTotalSamples) * entropyLeft 
			+ (rightChildStatistics.GetSampleNumber() * 1.0 / nTotalSamples) * entropyRight;

		double informationGain = entropyBefore - entropyAfter;
		return informationGain;
	}

};
    

} } }
    
#endif
