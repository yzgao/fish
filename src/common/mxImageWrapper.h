//
//  mxImageWrapper.h
//  FISH
//
//  Created by Yaozong Gao on 11/19/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __mxImageWrapper_h__
#define __mxImageWrapper_h__

#include "extern/ImageHelper.h"

namespace BRIC { namespace IDEA { namespace FISH {

class mxImageWrapper
{
public:
	mxImageWrapper() { m_pointer = NULL; m_type = ImageHelper::UNKNOWN; }
	~mxImageWrapper() { Release(); }

	mxImageWrapper& operator=(const mxImageWrapper& rhs);
	mxImageWrapper(const mxImageWrapper& copy);

	void Release();
	bool ReadImage(const char* path);
	void ResampleImage(const vect3<double>& spacing);
	template <typename T> void CropPatch(const vect3<unsigned int>& voxel, const vect3<unsigned int>& patchSize, mxImage<T>& patch, T default_val) const;
	template <typename T1, typename T2> void World2Voxel(const vect3<T1>& world, vect3<T2>& voxel) const;
	vect3<unsigned int> GetImageSize() const;
	vect3<double> GetSpacing() const;
	bool PtInImage(const vect3<unsigned int>& pt) const;
	bool PtInImage(int x, int y, int z) const;
	template <typename T> void CopyImageInfoTo(mxImage<T>& image) const;
	template <typename T> void PadImage(const vect3<unsigned int>& patchSize, T default_val);
	template <typename T> mxImage<T>* as();
	void ConvertToBinaryImage(mxImage<char>& image);
	template <typename T> void Threshold(double threshold, T objOutputValue, T bkOutputValue, mxImage<T>& image);

private:
	void MakeCopy(const mxImageWrapper& rhs);

	void* m_pointer;
	ImageHelper::PixelType m_type;
};


//////////////////////////////////////////////////////////////////////////

void mxImageWrapper::MakeCopy(const mxImageWrapper& rhs)
{
	switch(rhs.m_type) {
	case ImageHelper::CHAR: 
		m_pointer = new mxImage<char>();
		*static_cast<mxImage<char>*>(m_pointer) = *static_cast<mxImage<char>*>(rhs.m_pointer);
		break;
	case ImageHelper::UCHAR:
		m_pointer = new mxImage<unsigned char>();
		*static_cast<mxImage<unsigned char>*>(m_pointer) = *static_cast<mxImage<unsigned char>*>(rhs.m_pointer);
		break;
	case ImageHelper::SHORT: 
		m_pointer = new mxImage<short>();
		*static_cast<mxImage<short>*>(m_pointer) = *static_cast<mxImage<short>*>(rhs.m_pointer);
		break;
	case ImageHelper::USHORT:
		m_pointer = new mxImage<unsigned short>();
		*static_cast<mxImage<unsigned short>*>(m_pointer) = *static_cast<mxImage<unsigned short>*>(rhs.m_pointer);
		break;
	case ImageHelper::INT:
		m_pointer = new mxImage<int>();
		*static_cast<mxImage<int>*>(m_pointer) = *static_cast<mxImage<int>*>(rhs.m_pointer);
		break;
	case ImageHelper::UINT:
		m_pointer = new mxImage<unsigned int>();
		*static_cast<mxImage<unsigned int>*>(m_pointer) = *static_cast<mxImage<unsigned int>*>(rhs.m_pointer);
		break;
	case ImageHelper::LONG:
		m_pointer = new mxImage<long>();
		*static_cast<mxImage<long>*>(m_pointer) = *static_cast<mxImage<long>*>(rhs.m_pointer);
		break;
	case ImageHelper::ULONG:
		m_pointer = new mxImage<unsigned long>();
		*static_cast<mxImage<unsigned long>*>(m_pointer) = *static_cast<mxImage<unsigned long>*>(rhs.m_pointer);
		break;
	case ImageHelper::FLOAT:
		m_pointer = new mxImage<float>();
		*static_cast<mxImage<float>*>(m_pointer) = *static_cast<mxImage<float>*>(rhs.m_pointer);
		break;
	case ImageHelper::DOUBLE:
		m_pointer = new mxImage<double>();
		*static_cast<mxImage<double>*>(m_pointer) = *static_cast<mxImage<double>*>(rhs.m_pointer);
		break;
	default:
		m_pointer = NULL;
		break;
	}
	m_type = rhs.m_type;
}

mxImageWrapper& mxImageWrapper::operator=(const mxImageWrapper& rhs)
{
	if(this == &rhs)
		return *this;
	Release();
	MakeCopy(rhs);
	return *this;
}

mxImageWrapper::mxImageWrapper(const mxImageWrapper& copy)
{
	MakeCopy(copy);
}

/** @brief read image */
bool mxImageWrapper::ReadImage( const char* path ) {

	m_type = ImageHelper::ReadPixelType(path);

	switch (m_type)
	{
	case ImageHelper::UCHAR:
		m_pointer = new mxImage<unsigned char>();
		if (!ImageHelper::ReadImage<unsigned char, unsigned char>(path, *static_cast<mxImage<unsigned char>*>(m_pointer))) 
			return false;
		break;
	case ImageHelper::CHAR:
		m_pointer = new mxImage<char>();
		if (!ImageHelper::ReadImage<char, char>(path, *static_cast<mxImage<char>*>(m_pointer))) 
			return false;
		break;
	case ImageHelper::SHORT:
		m_pointer = new mxImage<short>();
		if (!ImageHelper::ReadImage<short, short>(path, *static_cast<mxImage<short>*>(m_pointer)))
			return false;
		break;
	case ImageHelper::USHORT:
		m_pointer = new mxImage<unsigned short>();
		if (!ImageHelper::ReadImage<unsigned short, unsigned short>(path, *static_cast<mxImage<unsigned short>*>(m_pointer)))
			return false;
		break;
	case ImageHelper::INT:
		m_pointer = new mxImage<int>();
		if (!ImageHelper::ReadImage<int, int>(path, *static_cast<mxImage<int>*>(m_pointer))) 
			return false;
		break;
	case ImageHelper::UINT:
		m_pointer = new mxImage<unsigned int>();
		if (!ImageHelper::ReadImage<unsigned int, unsigned int>(path, *static_cast<mxImage<unsigned int>*>(m_pointer))) 
			return false;
		break;
	case ImageHelper::LONG:
		m_pointer = new mxImage<long>();
		if (!ImageHelper::ReadImage<long, long>(path, *static_cast<mxImage<long>*>(m_pointer)))
			return false;
		break;
	case ImageHelper::ULONG:
		m_pointer = new mxImage<unsigned long>();
		if (!ImageHelper::ReadImage<unsigned long, unsigned long>(path, *static_cast<mxImage<unsigned long>*>(m_pointer)))
			return false;
		break;
	case ImageHelper::FLOAT:
		m_pointer = new mxImage<float>();
		if (!ImageHelper::ReadImage<float,float>(path, *static_cast<mxImage<float>*>(m_pointer))) 
			return false;
		break;
	case ImageHelper::DOUBLE:
		m_pointer = new mxImage<double>();
		if (!ImageHelper::ReadImage<double,double>(path, *static_cast<mxImage<double>*>(m_pointer)))
			return false;
		break;
	default:
		m_pointer = NULL;
		break;
	}
	return true;
}

void mxImageWrapper::Release()
{
	switch (m_type)
	{
	case ImageHelper::CHAR: delete static_cast<mxImage<char>*>(m_pointer); break;
	case ImageHelper::UCHAR: delete static_cast<mxImage<unsigned char>*>(m_pointer); break;
	case ImageHelper::SHORT: delete static_cast<mxImage<short>*>(m_pointer); break;
	case ImageHelper::USHORT: delete static_cast<mxImage<unsigned short>*>(m_pointer); break;
	case ImageHelper::INT: delete static_cast<mxImage<int>*>(m_pointer); break;
	case ImageHelper::UINT: delete static_cast<mxImage<unsigned int>*>(m_pointer); break;
	case ImageHelper::LONG: delete static_cast<mxImage<long>*>(m_pointer); break;
	case ImageHelper::ULONG: delete static_cast<mxImage<unsigned long>*>(m_pointer); break;
	case ImageHelper::FLOAT: delete static_cast<mxImage<float>*>(m_pointer); break;
	case ImageHelper::DOUBLE: delete static_cast<mxImage<double>*>(m_pointer); break;
	default:
		break;
	}
	m_pointer = NULL;
	m_type = ImageHelper::UNKNOWN;
}

void mxImageWrapper::ResampleImage(const vect3<double>& spacing)
{
	void* resample_pointer = NULL;
	switch (m_type)
	{
	case ImageHelper::CHAR: 
		resample_pointer = new mxImage<char>();
		mxImageUtils::Resample(*static_cast<mxImage<char>*>(m_pointer), spacing, *static_cast<mxImage<char>*>(resample_pointer));
		break;
	case ImageHelper::UCHAR:
		resample_pointer = new mxImage<unsigned char>();
		mxImageUtils::Resample(*static_cast<mxImage<unsigned char>*>(m_pointer), spacing, *static_cast<mxImage<unsigned char>*>(resample_pointer));
		break;
	case ImageHelper::SHORT:
		resample_pointer = new mxImage<short>();
		mxImageUtils::Resample(*static_cast<mxImage<short>*>(m_pointer), spacing, *static_cast<mxImage<short>*>(resample_pointer));
		break;
	case ImageHelper::USHORT:
		resample_pointer = new mxImage<unsigned short>();
		mxImageUtils::Resample(*static_cast<mxImage<unsigned short>*>(m_pointer), spacing, *static_cast<mxImage<unsigned short>*>(resample_pointer));
		break;
	case ImageHelper::INT:
		resample_pointer = new mxImage<int>();
		mxImageUtils::Resample(*static_cast<mxImage<int>*>(m_pointer), spacing, *static_cast<mxImage<int>*>(resample_pointer));
		break;
	case ImageHelper::UINT:
		resample_pointer = new mxImage<unsigned int>();
		mxImageUtils::Resample(*static_cast<mxImage<unsigned int>*>(m_pointer), spacing, *static_cast<mxImage<unsigned int>*>(resample_pointer));
		break;
	case ImageHelper::LONG:
		resample_pointer = new mxImage<long>();
		mxImageUtils::Resample(*static_cast<mxImage<long>*>(m_pointer), spacing, *static_cast<mxImage<long>*>(resample_pointer));
		break;
	case ImageHelper::ULONG:
		resample_pointer = new mxImage<unsigned long>();
		mxImageUtils::Resample(*static_cast<mxImage<unsigned long>*>(m_pointer), spacing, *static_cast<mxImage<unsigned long>*>(resample_pointer));
		break;
	case ImageHelper::FLOAT:
		resample_pointer = new mxImage<float>();
		mxImageUtils::Resample(*static_cast<mxImage<float>*>(m_pointer), spacing, *static_cast<mxImage<float>*>(resample_pointer));
		break;
	case ImageHelper::DOUBLE:
		resample_pointer = new mxImage<double>();
		mxImageUtils::Resample(*static_cast<mxImage<double>*>(m_pointer), spacing, *static_cast<mxImage<double>*>(resample_pointer));
		break;
	default:
		break;
	}

	if(m_pointer) {
		ImageHelper::PixelType old_type = m_type;
		Release();
		m_type = old_type;
	}
	m_pointer = resample_pointer;
}

template <typename T>
void mxImageWrapper::CropPatch(const vect3<unsigned int>& voxel, const vect3<unsigned int>& patchSize, mxImage<T>& patch, T default_val) const 
{
	switch (m_type)
	{
	case ImageHelper::CHAR:
		mxImageUtils::Crop3(*(static_cast<mxImage<char>*>(m_pointer)), voxel, patchSize, patch, default_val); break;
	case ImageHelper::UCHAR:
		mxImageUtils::Crop3(*(static_cast<mxImage<unsigned char>*>(m_pointer)), voxel, patchSize, patch, default_val); break;
	case ImageHelper::SHORT:
		mxImageUtils::Crop3(*(static_cast<mxImage<short>*>(m_pointer)), voxel, patchSize, patch, default_val); break;
	case ImageHelper::USHORT:
		mxImageUtils::Crop3(*(static_cast<mxImage<unsigned short>*>(m_pointer)), voxel, patchSize, patch, default_val); break;
	case ImageHelper::INT:
		mxImageUtils::Crop3(*(static_cast<mxImage<int>*>(m_pointer)), voxel, patchSize, patch, default_val); break;
	case ImageHelper::UINT:
		mxImageUtils::Crop3(*(static_cast<mxImage<unsigned int>*>(m_pointer)), voxel, patchSize, patch, default_val); break;
	case ImageHelper::LONG:
		mxImageUtils::Crop3(*(static_cast<mxImage<long>*>(m_pointer)), voxel, patchSize, patch, default_val); break;
	case ImageHelper::ULONG:
		mxImageUtils::Crop3(*(static_cast<mxImage<unsigned long>*>(m_pointer)), voxel, patchSize, patch, default_val); break;
	case ImageHelper::FLOAT:
		mxImageUtils::Crop3(*(static_cast<mxImage<float>*>(m_pointer)), voxel, patchSize, patch, default_val); break;
	case ImageHelper::DOUBLE:
		mxImageUtils::Crop3(*(static_cast<mxImage<double>*>(m_pointer)), voxel, patchSize, patch, default_val); break;
	default:
		break;
	}
}

template <typename T1, typename T2>
void mxImageWrapper::World2Voxel(const vect3<T1>& world, vect3<T2>& voxel) const 
{
	switch (m_type)
	{
	case ImageHelper::CHAR: mxImageUtils::World2Voxel(*(static_cast<mxImage<char>*>(m_pointer)), world, voxel); break;
	case ImageHelper::UCHAR: mxImageUtils::World2Voxel(*(static_cast<mxImage<unsigned char>*>(m_pointer)), world, voxel); break;
	case ImageHelper::SHORT: mxImageUtils::World2Voxel(*(static_cast<mxImage<short>*>(m_pointer)), world, voxel); break;
	case ImageHelper::USHORT: mxImageUtils::World2Voxel(*(static_cast<mxImage<unsigned short>*>(m_pointer)), world, voxel); break;
	case ImageHelper::INT: mxImageUtils::World2Voxel(*(static_cast<mxImage<int>*>(m_pointer)), world, voxel); break;
	case ImageHelper::UINT: mxImageUtils::World2Voxel(*(static_cast<mxImage<unsigned int>*>(m_pointer)), world, voxel); break;
	case ImageHelper::LONG: mxImageUtils::World2Voxel(*(static_cast<mxImage<long>*>(m_pointer)), world, voxel); break;
	case ImageHelper::ULONG: mxImageUtils::World2Voxel(*(static_cast<mxImage<unsigned long>*>(m_pointer)), world, voxel); break;
	case ImageHelper::FLOAT: mxImageUtils::World2Voxel(*(static_cast<mxImage<float>*>(m_pointer)), world, voxel); break;
	case ImageHelper::DOUBLE: mxImageUtils::World2Voxel(*(static_cast<mxImage<double>*>(m_pointer)), world, voxel); break;
	default:
		std::cerr << "unknown image type" << std::endl; exit(-1);
		break;
	}
}

vect3<unsigned int> mxImageWrapper::GetImageSize() const
{
	switch (m_type)
	{
	case ImageHelper::CHAR: return static_cast<mxImage<char>*>(m_pointer)->GetImageSize();
	case ImageHelper::UCHAR: return static_cast<mxImage<unsigned char>*>(m_pointer)->GetImageSize();
	case ImageHelper::SHORT: return static_cast<mxImage<short>*>(m_pointer)->GetImageSize();
	case ImageHelper::USHORT: return static_cast<mxImage<unsigned short>*>(m_pointer)->GetImageSize();
	case ImageHelper::INT: return static_cast<mxImage<int>*>(m_pointer)->GetImageSize();
	case ImageHelper::UINT: return static_cast<mxImage<unsigned int>*>(m_pointer)->GetImageSize();
	case ImageHelper::LONG: return static_cast<mxImage<long>*>(m_pointer)->GetImageSize();
	case ImageHelper::ULONG: return static_cast<mxImage<unsigned long>*>(m_pointer)->GetImageSize();
	case ImageHelper::FLOAT: return static_cast<mxImage<float>*>(m_pointer)->GetImageSize();
	case ImageHelper::DOUBLE: return static_cast<mxImage<double>*>(m_pointer)->GetImageSize();
	default:
		std::cerr << "unknown image type" << std::endl; exit(-1);
		break;
	}
}

bool mxImageWrapper::PtInImage(const vect3<unsigned int>& pt) const 
{
	switch (m_type)
	{
	case ImageHelper::CHAR: return static_cast<mxImage<char>*>(m_pointer)->PtInImage(pt);
	case ImageHelper::UCHAR: return static_cast<mxImage<unsigned char>*>(m_pointer)->PtInImage(pt);
	case ImageHelper::SHORT: return static_cast<mxImage<short>*>(m_pointer)->PtInImage(pt);
	case ImageHelper::USHORT: return static_cast<mxImage<unsigned short>*>(m_pointer)->PtInImage(pt);
	case ImageHelper::INT: return static_cast<mxImage<int>*>(m_pointer)->PtInImage(pt);
	case ImageHelper::UINT: return static_cast<mxImage<unsigned int>*>(m_pointer)->PtInImage(pt);
	case ImageHelper::LONG: return static_cast<mxImage<long>*>(m_pointer)->PtInImage(pt);
	case ImageHelper::ULONG: return static_cast<mxImage<unsigned long>*>(m_pointer)->PtInImage(pt);
	case ImageHelper::FLOAT: return static_cast<mxImage<float>*>(m_pointer)->PtInImage(pt);
	case ImageHelper::DOUBLE: return static_cast<mxImage<double>*>(m_pointer)->PtInImage(pt);
	default:
		std::cerr << "unknown image type" << std::endl; exit(-1);
		break;
	}
}

bool mxImageWrapper::PtInImage(int x, int y, int z) const
{
	switch (m_type)
	{
	case ImageHelper::CHAR: return static_cast<mxImage<char>*>(m_pointer)->PtInImage(x, y, z);
	case ImageHelper::UCHAR: return static_cast<mxImage<unsigned char>*>(m_pointer)->PtInImage(x, y, z);
	case ImageHelper::SHORT: return static_cast<mxImage<short>*>(m_pointer)->PtInImage(x, y, z);
	case ImageHelper::USHORT: return static_cast<mxImage<unsigned short>*>(m_pointer)->PtInImage(x, y, z);
	case ImageHelper::INT: return static_cast<mxImage<int>*>(m_pointer)->PtInImage(x, y, z);
	case ImageHelper::UINT: return static_cast<mxImage<unsigned int>*>(m_pointer)->PtInImage(x, y, z);
	case ImageHelper::LONG: return static_cast<mxImage<long>*>(m_pointer)->PtInImage(x, y, z);
	case ImageHelper::ULONG: return static_cast<mxImage<unsigned long>*>(m_pointer)->PtInImage(x, y, z);
	case ImageHelper::FLOAT: return static_cast<mxImage<float>*>(m_pointer)->PtInImage(x, y, z);
	case ImageHelper::DOUBLE: return static_cast<mxImage<double>*>(m_pointer)->PtInImage(x, y, z);
	default:
		std::cerr << "unknown image type" << std::endl; exit(-1);
		break;
	}
}

vect3<double> mxImageWrapper::GetSpacing() const
{
	switch (m_type)
	{
	case ImageHelper::CHAR: return static_cast<mxImage<char>*>(m_pointer)->GetSpacing();
	case ImageHelper::UCHAR: return static_cast<mxImage<unsigned char>*>(m_pointer)->GetSpacing();
	case ImageHelper::SHORT: return static_cast<mxImage<short>*>(m_pointer)->GetSpacing();
	case ImageHelper::USHORT: return static_cast<mxImage<unsigned short>*>(m_pointer)->GetSpacing();
	case ImageHelper::INT: return static_cast<mxImage<int>*>(m_pointer)->GetSpacing();
	case ImageHelper::UINT: return static_cast<mxImage<unsigned int>*>(m_pointer)->GetSpacing();
	case ImageHelper::LONG: return static_cast<mxImage<long>*>(m_pointer)->GetSpacing();
	case ImageHelper::ULONG: return static_cast<mxImage<unsigned long>*>(m_pointer)->GetSpacing();
	case ImageHelper::FLOAT: return static_cast<mxImage<float>*>(m_pointer)->GetSpacing();
	case ImageHelper::DOUBLE: return static_cast<mxImage<double>*>(m_pointer)->GetSpacing();
	default:
		std::cerr << "unknown image type" << std::endl; exit(-1);
		break;
	}
}

template <typename T> 
void mxImageWrapper::CopyImageInfoTo(mxImage<T>& image) const
{
	switch (m_type)
	{
	case ImageHelper::CHAR: image.CopyImageInfo(*static_cast<mxImage<char>*>(m_pointer)); break;
	case ImageHelper::UCHAR: image.CopyImageInfo(*static_cast<mxImage<unsigned char>*>(m_pointer)); break;
	case ImageHelper::SHORT: image.CopyImageInfo(*static_cast<mxImage<short>*>(m_pointer)); break;
	case ImageHelper::USHORT: image.CopyImageInfo(*static_cast<mxImage<unsigned short>*>(m_pointer)); break;
	case ImageHelper::INT: image.CopyImageInfo(*static_cast<mxImage<int>*>(m_pointer)); break;
	case ImageHelper::UINT: image.CopyImageInfo(*static_cast<mxImage<unsigned int>*>(m_pointer)); break;
	case ImageHelper::LONG: image.CopyImageInfo(*static_cast<mxImage<long>*>(m_pointer)); break;
	case ImageHelper::ULONG: image.CopyImageInfo(*static_cast<mxImage<unsigned long>*>(m_pointer)); break;
	case ImageHelper::FLOAT: image.CopyImageInfo(*static_cast<mxImage<float>*>(m_pointer)); break;
	case ImageHelper::DOUBLE: image.CopyImageInfo(*static_cast<mxImage<double>*>(m_pointer)); break;
	default:
		std::cerr << "unknown image type" << std::endl; exit(-1);
		break;
	}
}

template <typename T> 
void mxImageWrapper::PadImage(const vect3<unsigned int>& patchSize, T default_val)
{
	mxImage<T>* tmp = new mxImage<T>();
	switch (m_type)
	{
	case ImageHelper::CHAR: mxImageUtils::ImagePad(*static_cast<mxImage<char>*>(m_pointer), patchSize, *tmp, default_val); break;
	case ImageHelper::UCHAR: mxImageUtils::ImagePad(*static_cast<mxImage<unsigned char>*>(m_pointer), patchSize, *tmp, default_val); break;
	case ImageHelper::SHORT: mxImageUtils::ImagePad(*static_cast<mxImage<short>*>(m_pointer), patchSize, *tmp, default_val); break;
	case ImageHelper::USHORT: mxImageUtils::ImagePad(*static_cast<mxImage<unsigned short>*>(m_pointer), patchSize, *tmp, default_val); break;
	case ImageHelper::INT: mxImageUtils::ImagePad(*static_cast<mxImage<int>*>(m_pointer), patchSize, *tmp, default_val); break;
	case ImageHelper::UINT: mxImageUtils::ImagePad(*static_cast<mxImage<unsigned int>*>(m_pointer), patchSize, *tmp, default_val); break;
	case ImageHelper::LONG: mxImageUtils::ImagePad(*static_cast<mxImage<long>*>(m_pointer), patchSize, *tmp, default_val); break;
	case ImageHelper::ULONG: mxImageUtils::ImagePad(*static_cast<mxImage<unsigned long>*>(m_pointer), patchSize, *tmp, default_val); break;
	case ImageHelper::FLOAT: mxImageUtils::ImagePad(*static_cast<mxImage<float>*>(m_pointer), patchSize, *tmp, default_val); break;
	case ImageHelper::DOUBLE: mxImageUtils::ImagePad(*static_cast<mxImage<double>*>(m_pointer), patchSize, *tmp, default_val); break;
	default:
		std::cerr << "unknown image type" << std::endl; exit(-1);
		break;
	}

	ImageHelper::PixelType old_type = m_type;
	Release();
	m_pointer = tmp;
	m_type = old_type;
}

template <typename T> 
mxImage<T>* mxImageWrapper::as()
{
	return static_cast<mxImage<T>*>(m_pointer);
}

void mxImageWrapper::ConvertToBinaryImage(mxImage<char>& image)
{
	switch (m_type)
	{
	case ImageHelper::UCHAR:
		mxImageUtils::ConvertToBinaryImage(*static_cast<mxImage<unsigned char>*>(m_pointer), image);
		break;
	case ImageHelper::CHAR:
		mxImageUtils::ConvertToBinaryImage(*static_cast<mxImage<char>*>(m_pointer), image);
		break;
	case ImageHelper::USHORT:
		mxImageUtils::ConvertToBinaryImage(*static_cast<mxImage<unsigned short>*>(m_pointer), image);
		break;
	case ImageHelper::SHORT:
		mxImageUtils::ConvertToBinaryImage(*static_cast<mxImage<short>*>(m_pointer), image);
		break;
	case ImageHelper::UINT:
		mxImageUtils::ConvertToBinaryImage(*static_cast<mxImage<unsigned int>*>(m_pointer), image);
		break;
	case ImageHelper::INT:
		mxImageUtils::ConvertToBinaryImage(*static_cast<mxImage<int>*>(m_pointer), image);
		break;
	case ImageHelper::ULONG:
		mxImageUtils::ConvertToBinaryImage(*static_cast<mxImage<unsigned long>*>(m_pointer), image);
		break;
	case ImageHelper::LONG:
		mxImageUtils::ConvertToBinaryImage(*static_cast<mxImage<long>*>(m_pointer), image);
		break;
	case ImageHelper::FLOAT:
		mxImageUtils::ConvertToBinaryImage(*static_cast<mxImage<float>*>(m_pointer), image);
		break;
	case ImageHelper::DOUBLE:
		mxImageUtils::ConvertToBinaryImage(*static_cast<mxImage<double>*>(m_pointer), image);
		break;
	default:
		std::cerr << "unrecognized pixel type" << std::endl;
		exit(-1);
		break;
	}
}

template <typename T> 
void mxImageWrapper::Threshold(double threshold, T objOutputValue, T bkOutputValue, mxImage<T>& image)
{
	switch (m_type)
	{
	case ImageHelper::UCHAR:
		mxImageUtils::Threshold(*static_cast<mxImage<unsigned char>*>(m_pointer), image, static_cast<unsigned char>(threshold), objOutputValue, bkOutputValue);
		break;
	case ImageHelper::CHAR:
		mxImageUtils::Threshold(*static_cast<mxImage<char>*>(m_pointer), image, static_cast<char>(threshold), objOutputValue, bkOutputValue);
		break;
	case ImageHelper::USHORT:
		mxImageUtils::Threshold(*static_cast<mxImage<unsigned short>*>(m_pointer), image, static_cast<unsigned short>(threshold), objOutputValue, bkOutputValue);
		break;
	case ImageHelper::SHORT:
		mxImageUtils::Threshold(*static_cast<mxImage<short>*>(m_pointer), image, static_cast<short>(threshold), objOutputValue, bkOutputValue);
		break;
	case ImageHelper::UINT:
		mxImageUtils::Threshold(*static_cast<mxImage<unsigned int>*>(m_pointer), image, static_cast<unsigned int>(threshold), objOutputValue, bkOutputValue);
		break;
	case ImageHelper::INT:
		mxImageUtils::Threshold(*static_cast<mxImage<int>*>(m_pointer), image, static_cast<int>(threshold), objOutputValue, bkOutputValue);
		break;
	case ImageHelper::ULONG:
		mxImageUtils::Threshold(*static_cast<mxImage<unsigned long>*>(m_pointer), image, static_cast<unsigned long>(threshold), objOutputValue, bkOutputValue);
		break;
	case ImageHelper::LONG:
		mxImageUtils::Threshold(*static_cast<mxImage<long>*>(m_pointer), image, static_cast<long>(threshold), objOutputValue, bkOutputValue);
		break;
	case ImageHelper::FLOAT:
		mxImageUtils::Threshold(*static_cast<mxImage<float>*>(m_pointer), image, static_cast<float>(threshold), objOutputValue, bkOutputValue);
		break;
	case ImageHelper::DOUBLE:
		mxImageUtils::Threshold(*static_cast<mxImage<double>*>(m_pointer), image, static_cast<double>(threshold), objOutputValue, bkOutputValue);
		break;
	default:
		std::cerr << "unrecognized pixel type" << std::endl;
		exit(-1);
		break;
	}
}

} } }

#endif
