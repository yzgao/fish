//
//  vect.h
//  FISH
//
//  Created by Yaozong Gao on 11/06/12.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __vect_h__
#define __vect_h__

#include <math.h>
#include <assert.h>
#include <iostream>


namespace BRIC { namespace IDEA { namespace FISH {


#define VECT_EPSILON 1e-16

//
// 3 dimensional vector class
// 

template <typename T>
class vect3
{
public:

	typedef T ElementType;
    
    T x;
    T y;
    T z;
    
    vect3(): x(), y(), z() {}
    
    vect3(T arg_x, T arg_y, T arg_z): x(arg_x), y(arg_y), z(arg_z) {}
    
    vect3(const vect3<T>& rhs): x(rhs.x), y(rhs.y), z(rhs.z) {}

	std::string to_string() const {

		static const int Dimension = 3;

		std::ostringstream convert;
		convert << "[";
		for (unsigned int i = 0; i < Dimension; ++i) {
			convert << (*this)[i];
			if (i != Dimension - 1) {
				convert << ",";
			}
		}
		convert << "]";

		return convert.str();
	}
    
    //
    // overrided operators
    //
    
    T& operator[] (unsigned int idx) {
        assert(idx < 3);
        if (idx == 0) {
            return x;
        }else if(idx == 1) {
            return y;
        }else if(idx == 2) {
            return z;
        }else 
            return z;
    }
    
    const T& operator[] (unsigned int idx) const {
        assert(idx < 3);
        if (idx == 0) {
            return x;
        }else if(idx == 1) {
            return y;
        }else if(idx == 2) {
            return z;
        }else
            return z;
    }
    
    vect3<T>& operator=(const vect3<T>& rhs) {
        
        if(this == &rhs)
            return *this;
        
        x = rhs.x;
        y = rhs.y;
        z = rhs.z;
        
        return *this;
    }
    
    vect3<T> operator+(const vect3<T>& rhs) const {
        
        vect3<T> ret(*this);
        ret += rhs;
        return ret;
    }
    
    vect3<T> operator-(const vect3<T>& rhs) const {
        
        vect3<T> ret(*this);
        ret -= rhs;
        return ret;
    }
    
    template <typename T2>
    vect3<T> operator*(T2 scale) const {
        
        vect3<T> ret(*this);
        ret *= scale;
        return ret;
    }
    
    template <typename T2>
    vect3<T> operator/(T2 scale) const {
        
        vect3<T> ret(*this);
        ret /= scale;
        return ret;
    }
    
    vect3<T>& operator+=(const vect3<T>& rhs) {
        
        x += rhs.x;
        y += rhs.y;
        z += rhs.z;
        return *this;
    }
    
    vect3<T>& operator-=(const vect3<T>& rhs) {
        
        x -= rhs.x;
        y -= rhs.y;
        z -= rhs.z;
        return *this;
    }
    
    template <typename T2>
    vect3<T>& operator/=(T2 scale) {
        
        x = static_cast<T>(static_cast<double>(x) / scale);
        y = static_cast<T>(static_cast<double>(y) / scale);
        z = static_cast<T>(static_cast<double>(z) / scale);
        return *this;
    }
    
    template <typename T2>
    vect3<T>& operator*=(T2 scale) {
        
        x = static_cast<T>(static_cast<double>(x) * scale);
        y = static_cast<T>(static_cast<double>(y) * scale);
        z = static_cast<T>(static_cast<double>(z) * scale);
        return *this;
    }
    
    static T dot_product(const vect3<T>& lhs, const vect3<T>& rhs);
    static vect3<T> cross_product(const vect3<T>& lhs, const vect3<T>& rhs);

	// sorted by the order of z, y, x
	bool operator< (const vect3<T>& rhs) const 
	{
		if (z < rhs.z)
			return true;
		else if (z > rhs.z)
			return false;
		else {
			if (y < rhs.y)
				return true;
			else if (y > rhs.y)
				return false;
			else {
				if (x < rhs.x)
					return true;
				else if (x > rhs.x)
					return false;
				else
					return false;
			}
		}
	}

	bool operator<= (const vect3<T>& rhs) const 
	{
		return this->operator<(rhs) || this->operator==(rhs);
	}

	bool operator> (const vect3<T>& rhs) const 
	{
		return !this->operator<=(rhs);
	}

	bool operator>= (const vect3<T>& rhs) const
	{
		return !this->operator<(rhs);
	}

	bool operator== (const vect3<T>& rhs) const
	{
		if (x == rhs.x && y == rhs.y && z == rhs.z)
			return true;
		else
			return false;
	}

	bool operator!= (const vect3<T>& rhs) const 
	{
		return !this->operator==(rhs);
	}

	template <typename T2>
	vect3<T2> to() const {

		vect3<T2> ret;
		ret[0] = static_cast<T2>(x);
		ret[1] = static_cast<T2>(y);
		ret[2] = static_cast<T2>(z);

		return ret;
	}


    //
    // norm related operators
    //
    
    double square_magnitude() const {
        return static_cast<double>(x * x + y * y + z * z);
    }
    
    double norm() const {
        return l2norm();
    }
    
    double l2norm() const {
        return sqrt(static_cast<double>(x * x + y * y + z * z));
    }
    
    double l1norm() const {
        return abs(static_cast<double>(x)) + abs(static_cast<double>(y)) + abs(static_cast<double>(z));
    }
    
    vect3<T> normalized() const {
        
        double norm_val = norm();
        if (norm_val < VECT_EPSILON) {
            return vect3<T>();
        }
        
        vect3<T> ret(*this);
        ret.x = static_cast<T>(ret.x / norm_val);
        ret.y = static_cast<T>(ret.y / norm_val);
        ret.z = static_cast<T>(ret.z / norm_val);
        
        return ret;
	}
    
    // IO
    void Serialize(std::ostream& o) const {
        
        o.write((const char*)(&x), sizeof(T));
        o.write((const char*)(&y), sizeof(T));
        o.write((const char*)(&z), sizeof(T));
        
    }
    
    void Deserialize(std::istream& in) {
        
        in.read((char*)(&x), sizeof(T));
        in.read((char*)(&y), sizeof(T));
        in.read((char*)(&z), sizeof(T));
        
    }
    
    
};


template <typename T>
T vect3<T>::dot_product(const vect3<T>& lhs, const vect3<T>& rhs)
{
    return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
}


template <typename T>
vect3<T> vect3<T>::cross_product(const vect3<T>& lhs, const vect3<T>& rhs)
{
    vect3<T> ret;
    ret.x = lhs.y * rhs.z - lhs.z * rhs.y;
    ret.y = lhs.z * rhs.x - lhs.x * rhs.z;
    ret.z = lhs.x * rhs.y - lhs.y * rhs.x;
    
    return ret;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const vect3<T>& vec)
{
    unsigned int Dimension = 3;
    
    os << "[";
    for (unsigned int i = 0; i < Dimension; ++i) {
        os << vec[i];
        if (i != Dimension - 1) {
            os << ",";
        }
    }
    os << "]";
    return os;
}

//
// float member function specialization 
//
template <>
bool vect3<float>::operator== (const vect3<float>& rhs) const
{
	// std::cout << "float-version..." << std::endl; 

	if ( std::abs(x-rhs.x) <= VECT_EPSILON && std::abs(y-rhs.y) <= VECT_EPSILON && std::abs(z-rhs.z) <= VECT_EPSILON )
		return true;
	else
		return false;
}

//
// double member function specialization
//
template <>
bool vect3<double>::operator== (const vect3<double>& rhs) const
{
	// std::cout << "double-version..." << std::endl;

	if ( std::abs(x-rhs.x) <= VECT_EPSILON && std::abs(y-rhs.y) <= VECT_EPSILON && std::abs(z-rhs.z) <= VECT_EPSILON )
		return true;
	else
		return false;
}
    
    
} } }   // end namespace


#endif

