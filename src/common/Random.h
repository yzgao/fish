//
//  Random.h
//  FISH
//
//  Created by Yaozong Gao on 4/4/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __Random_h__
#define __Random_h__

#include "stdafx.h"
#include <time.h>

//#include <boost/random/mersenne_twister.hpp>
//#include <boost/random/uniform_real_distribution.hpp>
//#include <boost/random/uniform_int_distribution.hpp>

namespace BRIC { namespace IDEA { namespace FISH {
    
    
/** @brief Random number generator wrapper
 *
 * use boost random number generator inside
 */
class Random {
    
public:
    
	/** @brief constructor */
    Random();
    
	/** @brief constructor */
    Random(unsigned int seed);
    
    void Seed(const unsigned int seed);
    
	/** @brief random a double in [0,1] */
    double NextDouble();
    
	/** @brief random a double within a range */
    double NextDouble(double minValue, double maxValue);
    
	/** @brief random a integer [minValue,maxValue] */
	template <typename T>
    T Next(T minValue, T maxValue);

	/** @brief random a double from a Gaussian distribution */
	double GaussianNextDouble(double mean, double sigma);
    
private:
    
    boost::random::mt19937 m_gen;
    
};

/** @brief constructor */
Random::Random(): m_gen( (unsigned int)(time(NULL)) ) {}

/** @brief constructor */
Random::Random(unsigned int seed): m_gen(seed) {}

void Random::Seed(const unsigned int seed) {
	m_gen.seed(seed);
}

/** @brief random a double in [0,1] */
double Random::NextDouble() 
{
	boost::random::uniform_real_distribution<double> dist(0,1);
	return dist(m_gen);
}

/** @brief random a double within a range */
double Random::NextDouble(double minValue, double maxValue) 
{
	boost::random::uniform_real_distribution<double> dist(minValue, maxValue);
	return dist(m_gen);
}

/** @brief random a integer [minValue,maxValue] */
template <typename T>
T Random::Next(T minValue, T maxValue) 
{
	boost::random::uniform_int_distribution<T> dist(minValue, maxValue);
	return dist(m_gen);
}

/** @brief generate a double number from a Gaussian distribution */
double Random::GaussianNextDouble(double mean, double sigma)
{
	boost::random::normal_distribution<double> dist(mean, sigma);
	return dist(m_gen);
}
    
} } }

#endif
