//
//  mxImage.h
//  FISH
//
//  Created by Yaozong Gao on 12/28/12.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __mxImage_h__
#define __mxImage_h__


#include <algorithm>
#include <vector>
#include <assert.h>
#include "vect.h"
#include "aux_func.h"


namespace BRIC { namespace IDEA { namespace FISH {


/** @brief light-weight image class
 *
 * used to represent 3D image and patch
 *
 */
template <typename T>
class mxImage
{
public:
    
    template <typename U> friend class mxImage;
    
    typedef T PixelType;
    
    /** @brief default constructor */
    mxImage() {
        initializeImageInfo();
    }
    
    /** @brief constructor for creating an image from scratch */
    mxImage(unsigned int width, unsigned int height, unsigned int depth);
    
    /** @brief copy constructor */
    mxImage(const mxImage& rhs);
    
    /** @brief copy constructor for other type of images 
     *
     * easy for type conversion
     */
    template <typename T1>
    mxImage(const mxImage<T1>& rhs);
    
    /** @brief deconstructor */
    ~mxImage() {
        Clear();
    }
    
    /** @brief Clear the memory */
    void Clear() {
        m_data.clear();
        initializeImageInfo();
    }
    
    /** @brief fill all entries with a value */
	void Fill(T val)
	{
		for (unsigned int i = 0; i < m_data.size(); ++i)
			m_data[i] = val;
	}

	/** @brief allocate buffer */
	void Allocate()
	{
		m_data.resize(m_size[0] * m_size[1] * m_size[2]);
	}

    /** @brief set default image info (e.g., spacing, origin and axis) */
	void SetDefaultImageInfo()
	{
		for (unsigned int i = 0; i < 3; ++i) {
			m_spacing[i] = 1.0;
			m_origin[i] = 0.0;
		}
		
		m_axis[0] = 1.0; m_axis[1] = 0.0; m_axis[2] = 0.0;
		m_axis[3] = 0.0; m_axis[4] = 1.0; m_axis[5] = 0.0;
		m_axis[6] = 0.0; m_axis[7] = 0.0; m_axis[8] = 1.0;
	}
    
    /** @brief copy image information from another image */
    template <typename T2>
    void CopyImageInfo(const mxImage<T2>& img) 
    {
        SetSpacing(img.GetSpacing());
        SetOrigin(img.GetOrigin());
		SetAxis(img.GetAxis());
    }

    /** @brief check whether a point is inside the image region */
	inline bool PtInImage(int x, int y, int z) const
	{
		if(x < 0 || y < 0 || z < 0 || 
           x >= static_cast<int>(m_size[0]) || y >= static_cast<int>(m_size[1]) || z >= static_cast<int>(m_size[2])) 
			return false;
		else
			return true;
	}
    
    /** @brief check whether one point is in the image region */
    inline bool PtInImage(const vect3<unsigned int>& pt) const 
    {
        return PtInImage(static_cast<int>(pt[0]), static_cast<int>(pt[1]), static_cast<int>(pt[2]));
    }
    
    
    /** @brief overrided operators, extract value at (x,y,z) */
    inline const T& operator() (unsigned int x, unsigned int y, unsigned int z) const {
        assert_message(x < m_size[0] && y < m_size[1] && z < m_size[2], "image index out of bound");
        return m_data[z * m_size[0] * m_size[1] + y * m_size[0] + x];
    }
    
    /** @brief extract value at idx */
    inline const T& operator() (const vect3<unsigned int>& idx) const {
        assert_message(idx[0] < m_size[0] && idx[1] < m_size[1] && idx[2] < m_size[2], "image index out of bound");
        return m_data[idx[2] * m_size[0] * m_size[1] + idx[1] * m_size[0] + idx[0]];
    }
    
     /** @brief overrided operators, extract value at (x,y,z) */
    inline T& operator() (unsigned int x, unsigned int y, unsigned int z) {
        assert_message(x < m_size[0] && y < m_size[1] && z < m_size[2], "image index out of bound");
        return m_data[z * m_size[0] * m_size[1] + y * m_size[0] + x];
    }
    
    /** @brief extract value at idx */
    inline T& operator() (const vect3<unsigned int>& idx) {
        assert_message(idx[0] < m_size[0] && idx[1] < m_size[1] && idx[2] < m_size[2], "image index out of bound");
        return m_data[idx[2] * m_size[0] * m_size[1] + idx[1] * m_size[0] + idx[0]];
    }
    
    // assignment operator (allowing assigned from image of other types)
    template <typename T1>
    mxImage<T>& operator= (const mxImage<T1>& rhs);
    
    // attribute get operators
    
    /** @brief get the header pointer */
    T* GetData() {
        if (m_data.size() == 0) 
            return NULL;
        else
            return &m_data[0];
    }
    
    /** @brief get the header pointer */
    const T* GetData() const {
        if (m_data.size() == 0)
            return NULL;
        else
            return &m_data[0];
    }
    
    /** @brief get the image size */
    const vect3<unsigned int>& GetImageSize() const {
        return m_size;
    }
    
    /** @brief get the image spacing */
    const vect3<double>& GetSpacing() const {
        return m_spacing;
    }
    
    /** @brief get origin */
    const vect3<double>& GetOrigin() const {
        return m_origin;
    }
    
    /** @brief axis direction matrix */
    const double* GetAxis() const {
        return m_axis;
    }
    
    // attribute setter operator
    /** @brief set image size */
    void SetImageSize(const vect3<unsigned int>& size) {
        m_data.resize( size[0] * size[1] * size[2] );
        m_size = size;
    }
    
	void SetRawImageSize(const vect3<unsigned int>& size) {
		m_size = size;
	}

    /** @brief set image spacing */
    void SetSpacing(const vect3<double>& spacing) {
        m_spacing = spacing;
    }
    
    /** @brief set image origin */
    void SetOrigin(const vect3<double>& origin) {
        m_origin = origin;
    }
    
    /** @brief set image axis */
    void SetAxis(const double* axis) {
        for (unsigned int i = 0; i < 9; ++i) 
            m_axis[i] = axis[i];
    }
 
    
private:
    
    // initialize image information
    inline void initializeImageInfo() {
        
        m_size[0] = m_size[1] = m_size[2] = 0;
        m_spacing[0] = m_spacing[1] = m_spacing[2] = 1;
        m_origin[0] = m_origin[1] = m_origin[2] = 0;
        
        // orthogonal basises
        m_axis[0] = 1; m_axis[1] = 0; m_axis[2] = 0;      // x axis
        m_axis[3] = 0; m_axis[4] = 1; m_axis[5] = 0;      // y axis
        m_axis[6] = 0; m_axis[7] = 0; m_axis[8] = 1;      // z axis
    }
    
    
private:
    
    // data
    std::vector<T> m_data;
    
    vect3<unsigned int> m_size;                   // image size
    vect3<double> m_spacing;                      // image spacing
    vect3<double> m_origin;                       // image origin
    double m_axis[9];                             // image axis directions
                                                  // each axis direction is stored in a row, starting with x axis
    
};
    
    
    
// Implementations
// constructor for creating an image from scratch
template <typename T>
mxImage<T>::mxImage(unsigned int width, unsigned int height, unsigned int depth)
{
    m_size[0] = width; m_size[1] = height; m_size[2] = depth;
    for (unsigned int i = 0; i < 3; ++i) {
        m_spacing[i] = 1;
        m_origin[i] = 0;
    }
    
    m_axis[0] = 1; m_axis[1] = 0; m_axis[2] = 0;      // x axis
    m_axis[3] = 0; m_axis[4] = 1; m_axis[5] = 0;      // y axis
    m_axis[6] = 0; m_axis[7] = 0; m_axis[8] = 1;      // z axis
    
    m_data.resize(width * height * depth);
}

// copy constructor
template <typename T>
mxImage<T>::mxImage(const mxImage<T>& rhs)
{
    if (rhs.m_data.size() == 0) {
		m_data.clear();
		m_spacing = rhs.m_spacing;
		m_origin = rhs.m_origin;
		memcpy(m_axis, rhs.m_axis, sizeof(double) * 9);
		m_size = rhs.m_size;

    }else {
		if (m_size == rhs.m_size) {
			memcpy(&m_data[0], &(rhs.m_data[0]), sizeof(T) * rhs.m_data.size());
			m_spacing = rhs.m_spacing;
			m_origin = rhs.m_origin;
			memcpy(m_axis, rhs.m_axis, sizeof(double) * 9);
		} else {
			m_size = rhs.m_size;
			m_spacing = rhs.m_spacing;
			m_origin = rhs.m_origin;
			memcpy(m_axis, rhs.m_axis, sizeof(double) * 9);

			m_data.resize(m_size[0] * m_size[1] * m_size[2]);
			memcpy(&m_data[0], &(rhs.m_data[0]), sizeof(T) * rhs.m_data.size());
		}
	}
}

// copy constructor for other type of images
// easy for type conversion
template <typename T> template <typename T1>
mxImage<T>::mxImage(const mxImage<T1>& rhs) {
    
    m_size = rhs.m_size;
    m_spacing = rhs.m_spacing;
    m_origin = rhs.m_origin;
    memcpy(m_axis, rhs.m_axis, sizeof(double) * 9);
    
    m_data.resize(m_size[0] * m_size[1] * m_size[2]);
    for (unsigned int i = 0; i < m_data.size(); ++i)
        m_data[i] = static_cast<T>(rhs.m_data[i]);
}

    
template <typename T> template <typename T1>
mxImage<T>& mxImage<T>::operator= (const mxImage<T1>& rhs) {

    if (this == &rhs)
        return *this;

    m_data.resize(rhs.m_data.size());
    for (unsigned int i = 0; i < m_data.size(); ++i)
        m_data[i] = static_cast<T>( rhs.m_data[i] );

    m_size = rhs.m_size;
    m_spacing = rhs.m_spacing;
    m_origin = rhs.m_origin;
    memcpy(m_axis, rhs.m_axis, sizeof(double) * 9);

    return *this;
}
    

// short names
typedef mxImage<unsigned char> MaskImage;

} } }    // end namespace


#endif



