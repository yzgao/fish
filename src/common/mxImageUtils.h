//
//  mxImageUtils.h
//  FISH
//
//  Created by Yaozong Gao on 12/18/12.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __mxImageUtils_h__
#define __mxImageUtils_h__

#include "mxImage.h"
#include "SmartCast.h"
#include <queue>
#include "aux_func.h"
#include "common/mathUtils.h"
#include "common/vect.h"
#include "extern/NearestPointIndexer.h"

namespace BRIC { namespace IDEA { namespace FISH {

// interpolators
template <typename TIn, typename TOut>
class IImageInterpolator {
public:
	virtual TOut Get(const mxImage<TIn>& image, const vect3<double>& voxel) const = 0;
};

template <typename TIn, typename TOut>
class ImageLinearInterpolator: public IImageInterpolator<TIn, TOut> {
public:
	virtual TOut Get(const mxImage<TIn>& image, const vect3<double>& voxel) const;
};

template <typename TIn, typename TOut>
class ImageNearestNeighborInterpolator: public IImageInterpolator<TIn, TOut> {
public:
	virtual TOut Get(const mxImage<TIn>& image, const vect3<double>& voxel) const;
};


enum InterpolationType {
	IT_LINEAR_INTERPOLATION,
	IT_NEAREST_NEIGHBOR_INTERPOLATION
};


// normalization types
enum NormalizationType {
	NONE,
	UNIT_NORM,
	MAX_ONE,
	MIN_MAX,
	ZERO_MEAN_ONE_STD
};

enum SliceType {
	TRANSVERSAL,
	SAGITTAL,
	CORONAL
};


// utils for mxImage class
class mxImageUtils {
    
public:

	/** @brief copy image */
	template <typename TIn, typename TOut>
	static void Copy(const mxImage<TIn>& orig, mxImage<TOut>& copy);

    /** @brief convert world coordinate to voxel coordinate */
    template <typename T, typename CoordType>
    static void World2Voxel(const mxImage<T>& img, CoordType& x, CoordType& y, CoordType& z);
    
    /** @brief convert world coordinate to voxel coordinate */
    template <typename T, typename PointType>
    static void World2Voxel(const mxImage<T>& img, PointType& pt);
    
    /** @brief convert world coordinate to voxel coordinate */
    template <typename T, typename T1, typename T2>
    static void World2Voxel(const mxImage<T>& img, const vect3<T1>& worldCoord, vect3<T2>& voxelCoord);
    
    /** @brief convert voxel coordinate to world coordinate */
    template <typename T, typename CoordType>
    static void Voxel2World(const mxImage<T>& img, CoordType& x, CoordType& y, CoordType& z);
    
    /** @brief convert voxel coordinate to world coordinate */
    template <typename T, typename PointType>
    static void Voxel2World(const mxImage<T>& img, PointType& pt);
    
    /** @brief convert voxel coordinate to world coordinate */
    template <typename T, typename T1, typename T2>
    static void Voxel2World(const mxImage<T>& img, const vect3<T1>& voxelCoord, vect3<T2>& worldCoord);

    /** @brief batch convert world coordinate to voxel coordinate */
	template <typename T, typename T1, typename T2>
	static void World2Voxel(const mxImage<T>& img, const std::vector< vect3<T1> >& pts, std::vector< vect3<T2> >& voxels);

    /** @brief batch convert voxel coordinate to world coordinate */
	template <typename T, typename T1, typename T2>
	static void Voxel2World(const mxImage<T>& img, const std::vector< vect3<T1> >& voxels, std::vector< vect3<T2> >& pts);
    
    /** @brief adjust out-of-bounding-box points to nearest points on the bounding box */
    template <typename T, typename T1>
    static void AdjustVoxel(const mxImage<T>& img, vect3<T1>& voxel);
    
    /** @brief crop by center and radius */
	template <typename InputType, typename OutputType>
    static void Crop(const mxImage<InputType>& img, const vect3<unsigned int>& idx, const vect3<unsigned int>& radius, mxImage<OutputType>& patch, OutputType default_val = OutputType());
    
    /** @brief crop by rect, pt1: leftTopInferior, pt2: rightBottomSuperior */
	template <typename InputType, typename OutputType>
    static void Crop2(const mxImage<InputType>& img, const vect3<int>& sp, const vect3<int>& ep, mxImage<OutputType>& patch, OutputType default_val = OutputType());
    
	/** @brief crop by rect with patch padding */
	template <typename InputType, typename OutputType>
	static void Crop2_pad(const mxImage<InputType>& img, const vect3<int>& sp, const vect3<int>& ep, const vect3<unsigned int>& pad_patchsize, mxImage<OutputType>& patch, OutputType default_val = OutputType());

    /** @brief crop by center and size */
    template <typename InputType, typename OutputType>
    static void Crop3(const mxImage<InputType>& img, const vect3<unsigned int>& center, const vect3<unsigned int>& size, mxImage<OutputType>& patch, OutputType default_val = OutputType());

	/** @brief get crop header */
	template <typename InputType, typename OutputType>
	static void GetCropHeader2(const mxImage<InputType>& img, const vect3<int>& sp, const vect3<int>& ep, mxImage<OutputType>& outHeader);

	/** @brief finding the bounding box for a binary image */
	template <typename T>
	static void BoundingBox(const mxImage<T>& img, vect3<int>& min, vect3<int>& max);

	/** @brief finding 2D bounding box for a binary image */
	template <typename T>
	static void BoundingBox2D(const mxImage<T>& image, int idx, vect3<int>& min, vect3<int>& max);

	/** @brief shrink mask image to the content */
	template <typename T>
	static void ShrinkMaskImage(mxImage<T>& image, const vect3<int>& pad_size);

	/** @brief get boundary points in world coordinates */
	template <typename T>
	static void BoundaryPoints(const mxImage<T>& image, T threshold, std::vector< vect3<float> >& points, bool isWorldCoordinate = true);
    
    /** @brief threshold */
    template <typename T>
    static void Threshold(mxImage<T>& image, T threshold, T greaterValue = 255, T eqLessValue = 0);

	/** @brief threshold */
	template <typename T, typename TOut>
	static void Threshold(const mxImage<T>& image, mxImage<TOut>& outImage, T threshold, TOut greaterValue = 255, TOut eqLessValue = 0);

	/** @brief pick largest connected component */
	template <typename T>
	static void PickLargestComponent(mxImage<T>& image);

	/** @brief remove small connected component */
	template <typename T>
	static void RemoveSmallComponents(mxImage<T>& image, int volumesize);

	/** @brief binary fill holes */
	template <typename T>
	static void BinaryFillHoles(mxImage<T>& image, T foreground = 255);

	/** @brief binary fill holes 2D */
	template <typename T>
	static void BinaryFillHoles2D(mxImage<T>& image, unsigned int sliceIdx, T foreground = 255);

	/** @brief binary erode */
	template <typename T>
	static void BinaryErode(mxImage<T>& image, T foreground = 255);

	/** @brief binary dilate */
	template <typename T>
	static void BinaryDilate(mxImage<T>& image, T foreground = 255);

	/** @brief binary erode 2D */
	template <typename T>
	static void BinaryErode2D(mxImage<T>& image, int slice_idx, T foreground = 255);

	/** @brief binary dilate 2D */
	template <typename T>
	static void BinaryDilate2D(mxImage<T>& image, int slice_idx, T foreground = 255);

	/** @brief get binary narrow band */
	template <typename T>
	static void BinaryNarrowBand(const mxImage<T>& image, int band_len, mxImage<T>& outImage, T foreground = 255);

	/** @brief get binary narrow band */
	template <typename T>
	static void BinaryNarrowBand2D(const mxImage<T>& image, int band_len, mxImage<T>& outImage, T foreground = 255);

	/** @brief Gaussian smooth */
	template <typename TIn, typename TOut>
	static void GaussianSmooth(const mxImage<TIn>& image, mxImage<TOut>& outImage, double sigma, int kernelRadius = -1);

	/** @brief 1D Gaussian smooth */
	template <typename TIn, typename TOut>
	static void GaussianSmooth1D(const std::vector<TIn>& line, std::vector<TOut>& outLine, double sigma, int kernelRadius = -1);

	/** @brief max intensity */
	template <typename T>
	static T MaxIntensity(const mxImage<T>& image);

	/** @brief min intensity */
	template <typename T>
	static T MinIntensity(const mxImage<T>& image);

	/** @brief mass center */
	template <typename T>
	static vect3<double> MassCenter(const mxImage<T>& image);
    
    /** @brief lower and upper bound */
    static void MarginalBound(const vect3<unsigned int>& boxSize, const vect3<unsigned int>& patchSize, vect3<unsigned int>& lowerBound, vect3<unsigned int>& upperBound);
    
    /** @brief bounding box */
    template <typename T>
    static void BoxWorld2Voxel(const mxImage<T>& image, const vect3<double>& center, const vect3<double>& size, vect3<unsigned int>& sp, vect3<unsigned int>& ep, const vect3<unsigned int>& patchSize = vect3<unsigned int>(0,0,0));

	/** @brief get resample image header */
	template <typename T, typename TOut>
	static void GetResampleHeader(const mxImage<T>& image, const vect3<double>& spacing, mxImage<TOut>& outImage);

    /** @brief resample image with nearest neighbors */
	template <typename T, typename TOut>
 	static void Resample(const mxImage<T>& image, const vect3<double>& spacing, mxImage<TOut>& outImage, InterpolationType type = IT_NEAREST_NEIGHBOR_INTERPOLATION);

	/** @brief resample image with customized interpolation */
	template <typename T, typename TOut>
	static void ResampleWithInterpolator(const mxImage<T>& image, const vect3<double>& spacing, IImageInterpolator<T,TOut>& interpolator, mxImage<TOut>& outImage);

	/** @brief resample image with padding */
	template <typename T, typename TOut>
	static void ResampleWithPadding(const mxImage<T>& image, const vect3<unsigned int>& patchSize, const vect3<double>& spacing, mxImage<TOut>& outImage);

	/** @brief image padding */
	template <typename T, typename TOut>
	static void ImagePad(const mxImage<T>& image, const vect3<unsigned int>& patchSize, mxImage<TOut>& outImage, TOut defaultVal = TOut());

    /** @brief extract and resample a block from image */
	template <typename T, typename TOut>
	static void ExtractLargestBlock(const mxImage<T>& image, const vect3<double>& ptWorld, const vect3<double>& radius, const vect3<double>& spacing, mxImage<TOut>& block);

	/** @brief extract block from image */
	template <typename T, typename TOut>
	static void ExtractBlockWithPadding(const mxImage<T>& image, const vect3<double>& ptWorld, const vect3<double>& radius, const vect3<unsigned int>& patchSize, const vect3<double>& spacing, mxImage<TOut>& block);

    /** @brief world center of an image */
	template <typename T>
	static vect3<double> WorldCenter(const mxImage<T>& image);
    
    /** @brief z axis slice range for binary image */
    template <typename T>
    static void FindSliceRange(const mxImage<T>& image, unsigned int& start, unsigned int& end);
    
    /** @brief get four extreme points of a binary slice */
    template <typename T>
    static void FindSliceExtremePoints(const mxImage<T>& image, unsigned int sliceIdx, vect3<unsigned int>& top, vect3<unsigned int>& bot, vect3<unsigned int>& left, vect3<unsigned int>& right);
    
	/** @brief get a characterized point of a binary slice (specify x or y line in voxel) */
	template <typename T>
	static void FindLinePoint(const mxImage<T>& image, const vect3<int>& line, vect3<unsigned int>& pt1, vect3<unsigned int>& pt2);

	/** @brief get mass center for one binary slice */
	template <typename T>
	static vect3<double> SliceMassCenter(const mxImage<T>& image, unsigned int sliceIdx);

	/** @brief convert grids into graph */
	template <typename T>
	static void Grids2Graph(const mxImage<T>& binaryImage, std::vector< vect3<unsigned int> >& pts, std::vector< std::pair<int,int> >& edges, std::map<int,int>& pt2id);

	/** @brief extract boundary object points from a binary image */
	template <typename T>
	static void ExtractBoundaryPoints(const mxImage<T>& segImage, T threshold, std::vector< vect3<double> >& voxels);

	/** @brief extract boundary object points from a binary image (eight neighborhood) */
	template <typename T>
	static void ExtractBoundaryPoints_8(const mxImage<T>& segImage, T threshold, std::vector< vect3<double> >& voxels);

	/** @brief extract boundary background points from a binary image */
	template <typename T>
	static void ExtractBoundaryPoints2(const mxImage<T>& segImage, T threshold, std::vector< vect3<double> >& voxels);

	/** @brief apply a linear transformation to an image */
	template <typename T1, typename T2, typename T3, class TTransform>
	static void ApplyLinearTransformation(const mxImage<T1>& movImage, const mxImage<T2>& refImage, const TTransform& T, mxImage<T3>& outImage, IImageInterpolator<T3,T3>& interpolator, bool useWorld = false);

	/** @brief compute gradient at voxel */
	template <typename T>
	static vect3<double> Gradient(const mxImage<T>& image, int x, int y, int z);

	/** @brief gradient magnitude map */
	template <typename T>
	static void GradientMap(const mxImage<T>& image, mxImage<double>& gradMap);

	/** @brief gradient component map */
	template <typename T>
	static void GradientMaps(const mxImage<T>& image, mxImage<double>& xgradmap, mxImage<double>& ygradmap, mxImage<double>& zgradmap);

	/** @brief 2nd derivative */
	template <typename T>
	static void LaplacianMap(const mxImage<T>& image, mxImage<double>& lmap);

	/** @brief normalize an image to norm 1, only for float and double pixel type */
	template <typename T>
	static void NormalizeWithUnitNorm(mxImage<T>& image);

	/** @brief normalize an image by scaling to [0,1] with the maximum value */
	template <typename T>
	static void NormalizeWithMax(mxImage<T>& image);

	/** @brief normalize an image by scaling to [0,1] with minimum and maximum */
	template <typename T>
	static void NormalizeWithMinMax(mxImage<T>& image);

	/** @brief normalize an image by 0 mean and 1 standard deviation */
	template <typename T>
	static void NormalizeWithZeroMeanOneStd(mxImage<T>& image);

	/** @brief normalize an image (float or double type) */
	template <typename T>
	static void Normalize(mxImage<T>& image, NormalizationType type);

	/** @brief get normalization type by string */
	static NormalizationType GetNormalizationByString(std::string str);

	/** @brief get string from normalization type */
	static std::string GetNormalizationString(NormalizationType type);

	/** @brief vector image to norm image */
	template <typename T, int Dimension>
	static void Vector2Norm(const mxImage< boost::array<T,Dimension> >& vectorImage, mxImage<double>& normImage);

	/** @brief vector image to norm image */
	template <typename T>
	static void Vector2Norm(const std::vector< mxImage<T> >& vector_image, mxImage<double>& norm_image);

	/** @brief extract one dimensional image from vector image */
	template <typename T1, typename T2, int Dimension>
	static void ExtractFromVectorImage(const mxImage< boost::array<T1,Dimension> >& vectorImage, int dim, mxImage<T2>& compImage);

	/** @brief convert any image into standard binary image */
	template <typename T1, typename T2>
	static void ConvertToBinaryImage(const mxImage<T1>& input, mxImage<T2>& output);

	/** @brief pad box with patch size */
	static void PadBox(vect3<int>& sp, vect3<int>& ep, const vect3<unsigned int>& patchSize);

	/** @brief count times of points appearing in the voxel array */
	template <typename T1, typename T2>
	static void CountCumulativeImage(const mxImage<T1>& refImage, const std::vector< vect3<unsigned int> >& voxels, mxImage<T2>& countImage);

	/** @brief subtract two images, image1 - image2 */
	template <typename T>
	static void SubtractImage(const mxImage<T>& image1, const mxImage<T>& image2, mxImage<T>& image3);

	/** @brief CT body extraction */
	template <typename T1, typename T2>
	static void ExtractBodyFromCT(const mxImage<T1>& image, mxImage<unsigned char>& mask, const mxImage<T2>* ref_image);

	/** @brief add distance images */
	static void AddDistanceImages(const std::vector< mxImage<double> >& displacementImages, std::vector< mxImage<double> >& distanceImages);

	/** @brief rotate image */
	template <typename T>
	static void RotateImage(const mxImage<T>& image, vect3<double>& voxel_center, const vect3<double>& degrees, mxImage<T>& outImage);

	/** @breif rotate image */
	template <typename T>
	static void RotateImage(const mxImage<T>& image, const vnl_matrix<double>& inv_rot, bool nearest_or_linear, mxImage<T>& outImage);

	/** @brief square image, better use float or double */
	template <typename T>
	static void SquareImage(mxImage<T>& image);

	/** @brief transform mask image between two image spaces */
	template <typename T>
	static void TransformMaskImage(const mxImage<T>& ref_image, const mxImage<unsigned char>& src_image, mxImage<unsigned char>& transformed_image);

	/** @brief get one transversal slice */
	template <typename T>
	static void GetTransversalSlice(const mxImage<T>& image, int sliceIdx, mxImage<T>& slice, bool empty = false);

	/** @brief get one sagittal slice */
	template <typename T>
	static void GetSagittalSlice(const mxImage<T>& image, int sliceIdx, mxImage<T>& slice, bool empty = false);

	/** @brief get one coronal slice */
	template <typename T>
	static void GetCoronalSlice(const mxImage<T>& image, int sliceIdx, mxImage<T>& slice, bool empty = false);

	/** @brief find 2D contour */
	template <typename T>
	static void Find2DContour(const mxImage<T>& image, mxImage<unsigned char>& contour);

	/** @brief find 2D contours */
	template <typename T>
	static void Find2DContours(const mxImage<T>& image, SliceType type, mxImage<unsigned char>& contours);

	/** @brief mask OR */
	template <typename TIn, typename TOut>
	static void MaskOr(const mxImage<TIn>& image1, const mxImage<TIn>& image2, mxImage<TOut>& out_image);

	/** @brief mask image */
	static void ImageMask(mxImage<unsigned char>& image, const vect3<int>& box_sp, const vect3<int>& box_ep, const mxImage<unsigned char>& mask);

	/** @brief fill image data */
	template <typename T1, typename T2, typename TInterp>
	static void InterpolateImage(const mxImage<T1>& src_image, mxImage<T2>& out_image);

	/** @brief flip image */
	template <typename T>
	static void FlipImage(mxImage<T>& image, int axis);

	/** @brief count voxels */
	template <typename T>
	static int CountVoxels(const mxImage<T>& image, T value);

	/** @brief MR global intensity normalization */
	template <typename T>
	static void MRIntensityNormalization(const mxImage<T>& image, double ratio, mxImage<double>& outImage);

	/** @brief convert likelihood map to 3D displacement field */
	static void Prob2Disp(const mxImage<double>& prob_map, double threshold, mxImage<double>* disp_maps, const mxImage<unsigned char>* mask);

}; // end class definition
    
    
//////////////////////////////////////////////////////////////////////////
// Implementations

template <typename TIn, typename TOut>
TOut ImageLinearInterpolator<TIn,TOut>::Get(const mxImage<TIn>& image, const vect3<double>& voxel) const
{
	vect3<unsigned int> imageSize = image.GetImageSize();

	if (voxel[0] < 0 || voxel[1] < 0 || voxel[2] < 0 || voxel[0] > static_cast<double>(imageSize[0]-1) ||
		voxel[1] > static_cast<double>(imageSize[1]-1) || voxel[2] > static_cast<double>(imageSize[2]-1) )
		return TOut();

	vect3<int> base(static_cast<int>(voxel[0]), static_cast<int>(voxel[1]), static_cast<int>(voxel[2]));
	double p000, p100, p010, p001, p110, p101, p011, p111;

	vect3<int> pt = base;
	if (image.PtInImage(pt[0],pt[1],pt[2])) p000 = image(pt[0],pt[1],pt[2]);
	else p000 = 0;

	pt[0] = base[0]+1; pt[1] = base[1];    pt[2] = base[2];
	if (image.PtInImage(pt[0],pt[1],pt[2])) p100 = image(pt[0],pt[1],pt[2]);
	else p100 = 0;

	pt[0] = base[0];   pt[1] = base[1]+1;  pt[2] = base[2];
	if (image.PtInImage(pt[0],pt[1],pt[2])) p010 = image(pt[0],pt[1],pt[2]);
	else p010 = 0;

	pt[0] = base[0];   pt[1] = base[1];    pt[2] = base[2]+1;
	if (image.PtInImage(pt[0],pt[1],pt[2])) p001 = image(pt[0],pt[1],pt[2]);
	else p001 = 0;

	pt[0] = base[0]+1; pt[1] = base[1]+1;  pt[2] = base[2];
	if (image.PtInImage(pt[0],pt[1],pt[2])) p110 = image(pt[0],pt[1],pt[2]);
	else p110 = 0;

	pt[0] = base[0]+1; pt[1] = base[1];    pt[2] = base[2]+1;
	if (image.PtInImage(pt[0],pt[1],pt[2])) p101 = image(pt[0],pt[1],pt[2]);
	else p101 = 0;

	pt[0] = base[0];   pt[1] = base[1]+1;  pt[2] = base[2]+1;
	if (image.PtInImage(pt[0],pt[1],pt[2])) p011 = image(pt[0],pt[1],pt[2]);
	else p011 = 0;

	pt[0] = base[0]+1; pt[1] = base[1]+1;  pt[2] = base[2]+1;
	if (image.PtInImage(pt[0],pt[1],pt[2])) p111 = image(pt[0],pt[1],pt[2]);
	else p111 = 0;

	double px00 = (voxel[0]-base[0])*(p100-p000)+p000;
	double px10 = (voxel[0]-base[0])*(p110-p010)+p010;
	double px01 = (voxel[0]-base[0])*(p101-p001)+p001;
	double px11 = (voxel[0]-base[0])*(p111-p011)+p011;

	double pxy0 = (voxel[1]-base[1])*(px10-px00)+px00;
	double pxy1 = (voxel[1]-base[1])*(px11-px01)+px01;

	double pxyz = (voxel[2]-base[2])*(pxy1-pxy0)+pxy0;

	return static_cast<TOut>(pxyz);
}

template <typename TIn, typename TOut>
TOut ImageNearestNeighborInterpolator<TIn,TOut>::Get(const mxImage<TIn>& image, const vect3<double>& voxel) const
{
	vect3<int> base;
	for (int i = 0; i < 3; ++i)
		base[i] = static_cast<int>(voxel[i]+0.5);

	if (!image.PtInImage(base[0], base[1], base[2]))
		return 0;
	else
		return static_cast<TOut>(image(base[0],base[1],base[2]));
}

template <typename TIn, typename TOut>
void mxImageUtils::Copy(const mxImage<TIn>& orig, mxImage<TOut>& copy)
{
	vect3<unsigned int> imageSize = orig.GetImageSize();
	copy.CopyImageInfo(orig);
	copy.SetImageSize(imageSize);

	size_t length = static_cast<size_t>(imageSize[0]) * imageSize[1] * imageSize[2];
	TOut* copydata = copy.GetData();
	const TIn* origdata = orig.GetData();

	for (size_t i = 0; i < length; ++i) 
		copydata[i] = static_cast<TOut>(origdata[i]);
}

template <typename T, typename CoordType>
void mxImageUtils::World2Voxel(const mxImage<T>& img, CoordType& x, CoordType& y, CoordType& z)
{
    const vect3<double>& origin = img.GetOrigin();
    const double* axis = img.GetAxis();
    const vect3<double>& spacing = img.GetSpacing();
    
    vect3<double> coord;
    coord[0] = x - origin[0]; coord[1] = y - origin[1]; coord[2] = z - origin[2];
    
    vect3<double> projs;
    projs[0] = coord[0] * axis[0] + coord[1] * axis[1] + coord[2] * axis[2];
    projs[1] = coord[0] * axis[3] + coord[1] * axis[4] + coord[2] * axis[5];
    projs[2] = coord[0] * axis[6] + coord[1] * axis[7] + coord[2] * axis[8];
    
    x = SmartCast<CoordType>::Cast(projs[0] / spacing[0]);
    y = SmartCast<CoordType>::Cast(projs[1] / spacing[1]);
    z = SmartCast<CoordType>::Cast(projs[2] / spacing[2]);
}

template <typename T, typename PointType>
void mxImageUtils::World2Voxel(const mxImage<T>& img, PointType& pt)
{
    World2Voxel(img, pt[0], pt[1], pt[2]);
}
    
template <typename T, typename T1, typename T2>
void mxImageUtils::World2Voxel(const mxImage<T>& img, const vect3<T1>& worldCoord, vect3<T2>& voxelCoord)
{
    const vect3<double>& origin = img.GetOrigin();
    const double* axis = img.GetAxis();
    const vect3<double>& spacing = img.GetSpacing();
    
    vect3<double> coord;
    for(int i = 0; i < 3; ++i)
        coord[i] = worldCoord[i] - origin[i];
    
    vect3<double> projs;
    projs[0] = coord[0] * axis[0] + coord[1] * axis[1] + coord[2] * axis[2];
    projs[1] = coord[0] * axis[3] + coord[1] * axis[4] + coord[2] * axis[5];
    projs[2] = coord[0] * axis[6] + coord[1] * axis[7] + coord[2] * axis[8];
    
    for(int i = 0; i < 3; ++i)
        voxelCoord[i] = SmartCast<T2>::Cast(projs[i] / spacing[i]);
}
       
template <typename T, typename CoordType>
void mxImageUtils::Voxel2World(const mxImage<T>& img, CoordType& x, CoordType& y, CoordType& z)
{
    const vect3<double>& origin = img.GetOrigin();
    const double* axis = img.GetAxis();
    const vect3<double>& spacing = img.GetSpacing();
    
    vect3<double> coord;
    coord[0] = x * spacing[0]; coord[1] = y * spacing[1]; coord[2] = z * spacing[2];
    
    vect3<double> x_vec, y_vec, z_vec;
    x_vec[0] = axis[0] * coord[0]; x_vec[1] = axis[1] * coord[0]; x_vec[2] = axis[2] * coord[0];
    y_vec[0] = axis[3] * coord[1]; y_vec[1] = axis[4] * coord[1]; y_vec[2] = axis[5] * coord[1];
    z_vec[0] = axis[6] * coord[2]; z_vec[1] = axis[7] * coord[2]; z_vec[2] = axis[8] * coord[2];
    
    x = SmartCast<CoordType>::Cast(origin[0] + x_vec[0] + y_vec[0] + z_vec[0]);
    y = SmartCast<CoordType>::Cast(origin[1] + x_vec[1] + y_vec[1] + z_vec[1]);
    z = SmartCast<CoordType>::Cast(origin[2] + x_vec[2] + y_vec[2] + z_vec[2]);
}

template <typename T, typename PointType>
void mxImageUtils::Voxel2World(const mxImage<T>& img, PointType& pt)
{
    Voxel2World(img, pt[0], pt[1], pt[2]);
}
    
template <typename T, typename T1, typename T2>
void mxImageUtils::Voxel2World(const mxImage<T>& img, const vect3<T1>& voxelCoord, vect3<T2>& worldCoord)
{
    const vect3<double>& origin = img.GetOrigin();
    const double* axis = img.GetAxis();
    const vect3<double>& spacing = img.GetSpacing();
    
    vect3<double> coord;
    for(int i = 0; i < 3; ++i)
        coord[i] = voxelCoord[i] * spacing[i];
    
    vect3<double> x_vec, y_vec, z_vec;
    x_vec[0] = axis[0] * coord[0]; x_vec[1] = axis[1] * coord[0]; x_vec[2] = axis[2] * coord[0];
    y_vec[0] = axis[3] * coord[1]; y_vec[1] = axis[4] * coord[1]; y_vec[2] = axis[5] * coord[1];
    z_vec[0] = axis[6] * coord[2]; z_vec[1] = axis[7] * coord[2]; z_vec[2] = axis[8] * coord[2];
    
    for (int i = 0; i < 3; ++i)
        worldCoord[i] = SmartCast<T2>::Cast( origin[i] + x_vec[i] + y_vec[i] + z_vec[i] );
}

// batch conversion between world and voxel coordinates
template <typename T, typename T1, typename T2>
void mxImageUtils::World2Voxel(const mxImage<T>& img, const std::vector< vect3<T1> >& pts, std::vector< vect3<T2> >& voxels) {
    
    voxels.resize(pts.size());
    for (unsigned int i = 0; i < pts.size(); ++i)
        World2Voxel(img, pts[i], voxels[i]);
}

// adjust voxels that are out of boundary
template <typename T, typename T1>
void mxImageUtils::AdjustVoxel(const mxImage<T>& img, vect3<T1>& voxel)
{
    const vect3<unsigned int>& imageSize = img.GetImageSize();
    
    for (int i = 0; i < 3; ++i) {
        if(voxel[i] < 0)
            voxel[i] = 0;
        if(static_cast<double>(voxel[i]) > static_cast<double>(imageSize[i]) - 1)
            voxel[i] = static_cast<T1>(imageSize[i] - 1);
    }
}
        
template <typename T, typename T1, typename T2>
void mxImageUtils::Voxel2World(const mxImage<T>& img, const std::vector< vect3<T1> >& voxels, std::vector< vect3<T2> >& pts) {
    
    pts.resize(voxels.size());
    for (unsigned int i = 0; i < pts.size(); ++i)
        Voxel2World(img,voxels[i],pts[i]);
    
}

// crop by center and radius
template <typename InputType, typename OutputType>
void mxImageUtils::Crop(const mxImage<InputType>& img, const vect3<unsigned int>& idx, const vect3<unsigned int>& radius, mxImage<OutputType>& patch, OutputType default_val) {
    
    vect3<int> leftTop(idx[0], idx[1], idx[2]);
    leftTop[0] -= static_cast<int>(radius[0]); leftTop[1] -= static_cast<int>(radius[1]); leftTop[2] -= static_cast<int>(radius[2]);
    
    // set patch image information
    patch.SetSpacing( img.GetSpacing() );
    patch.SetAxis( img.GetAxis() );
    
    vect3<double> patchOrigin( leftTop[0], leftTop[1], leftTop[2] );
    Voxel2World( img, patchOrigin );
    patch.SetOrigin( patchOrigin );
    
    vect3<unsigned int> patchSize;
    patchSize[0] = radius[0]*2+1; patchSize[1] = radius[1]*2+1; patchSize[2] = radius[2]*2+1;
    patch.SetImageSize(patchSize);  // allocate memory
    
    for (unsigned int z = 0; z < patchSize[2]; ++z) {
        for (unsigned int y = 0; y < patchSize[1]; ++y) {
            for (unsigned int x = 0; x < patchSize[0]; ++x) {
                
                vect3<int> imgCoord(leftTop[0]+static_cast<int>(x), leftTop[1]+static_cast<int>(y), leftTop[2]+static_cast<int>(z));
                
                if (img.PtInImage(imgCoord[0], imgCoord[1], imgCoord[2]))
                    patch(x, y, z) = static_cast<OutputType>( img(imgCoord[0], imgCoord[1], imgCoord[2]) );
                else
                    patch(x, y, z) = default_val;
                
            } // end x
        } // end y
    }// end z
    
}

// crop by rect, pt1: leftTopInferior, pt2: rightBottomSuperior
template <typename InputType, typename OutputType>
void mxImageUtils::Crop2(const mxImage<InputType>& img, const vect3<int>& sp, const vect3<int>& ep, mxImage<OutputType>& patch, OutputType default_val) {
    
    assert(ep[0]>=sp[0] && ep[1]>=sp[1] && ep[2]>=sp[2]);
    
    // set patch image information
    patch.SetSpacing( img.GetSpacing() );
    patch.SetAxis( img.GetAxis() );
    
    vect3<double> patchOrigin( sp[0], sp[1], sp[2] );
    Voxel2World(img, patchOrigin);
    patch.SetOrigin( patchOrigin );
    
    vect3<unsigned int> patchSize;
    for (unsigned int i = 0; i < 3; ++i) {
        patchSize[i] = ep[i] - sp[i] + 1;
    }
    patch.SetImageSize(patchSize);
    
    for (unsigned int z = 0; z < patchSize[2]; ++z) {
        for (unsigned int y = 0; y < patchSize[1]; ++y) {
            for (unsigned int x = 0; x < patchSize[0]; ++x) {
                
                vect3<int> imgCoord(sp[0] + static_cast<int>(x), sp[1] + static_cast<int>(y), sp[2] + static_cast<int>(z));
                
                if (img.PtInImage(imgCoord[0], imgCoord[1], imgCoord[2]))
                    patch(x,y,z) = static_cast<OutputType>( img(imgCoord[0], imgCoord[1], imgCoord[2]) );
                else 
                    patch(x,y,z) = default_val;
                
            } // end x
        } // end y
    } // end z
}

template <typename InputType, typename OutputType>
void mxImageUtils::GetCropHeader2(const mxImage<InputType>& img, const vect3<int>& sp, const vect3<int>& ep, mxImage<OutputType>& outHeader)
{
	assert(ep[0] >= sp[0] && ep[1] >= sp[1] && ep[2] >= sp[2]);

	// set output image information
	outHeader.SetSpacing(img.GetSpacing());
	outHeader.SetAxis(img.GetAxis());

	vect3<double> outOrigin(sp[0], sp[1], sp[2]);
	Voxel2World(img, outOrigin);
	outHeader.SetOrigin(outOrigin);

	vect3<unsigned int> outSize;
	for (unsigned int i = 0; i < 3; ++i) 
		outSize[i] = ep[i] - sp[i] + 1;
	outHeader.SetRawImageSize(outSize);
}

template <typename InputType, typename OutputType>
void mxImageUtils::Crop2_pad(const mxImage<InputType>& image, const vect3<int>& sp, const vect3<int>& ep, const vect3<unsigned int>& pad_patchsize, mxImage<OutputType>& patch, OutputType default_val)
{
	vect3<int> req_sp = sp, req_ep = ep;
	mxImageUtils::PadBox(req_sp, req_ep, pad_patchsize);
	mxImageUtils::Crop2<InputType, OutputType>(image, req_sp, req_ep, patch, default_val);
}

// crop by center and size
template <typename InputType, typename OutputType>
void mxImageUtils::Crop3(const mxImage<InputType>& img, const vect3<unsigned int>& center, const vect3<unsigned int>& size, mxImage<OutputType>& patch, OutputType default_val)
{
    vect3<int> sp, ep;
    
    for (unsigned int i = 0; i < 3; ++i) {
        sp[i] = static_cast<int>(center[i]) - static_cast<int>(size[i] / 2);
        ep[i] = sp[i] + static_cast<int>(size[i]) - 1;
    }
    
    Crop2<InputType, OutputType>(img, sp, ep, patch, default_val);
}

// finding the bounding box for a binary image
template <typename T>
void mxImageUtils::BoundingBox(const mxImage<T>& img, vect3<int>& min, vect3<int>& max) {
    
    vect3<unsigned int> imageSize = img.GetImageSize();
    
    bool is_found = false;
    for (int z = 0; z < static_cast<int>(imageSize[2]); ++z) {
        for (int y = 0; y < static_cast<int>(imageSize[1]); ++y) {
            for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
                if (img(x,y,z) != 0) { is_found = true; min[2] = z; break; }
            if (is_found) break;
        }
        if(is_found) break;
    }
    
    is_found = false;
    for (int z = static_cast<int>(imageSize[2])-1; z >= 0; --z) {
        for (int y = 0; y < static_cast<int>(imageSize[1]); ++y) {
            for (int x = 0; x < static_cast<int>(imageSize[0]); ++x) {
                if (img(x,y,z) != 0) { is_found = true; max[2] = z; break; }
            }
            if (is_found) break;
        }
        if (is_found) break;
    }
    
    is_found = false;
    for (int y = 0; y < static_cast<int>(imageSize[1]); ++y) {
        for (int z = 0; z < static_cast<int>(imageSize[2]); ++z) {
            for (int x = 0; x < static_cast<int>(imageSize[0]); ++x) {
                if (img(x,y,z) != 0) { is_found = true; min[1] = y; break; }
            }
            if (is_found) break;
        }
        if (is_found) break;
    }
    
    is_found = false;
    for (int y = static_cast<int>(imageSize[1])-1; y >= 0; --y) {
        for (int z = 0; z < static_cast<int>(imageSize[2]); ++z) {
            for (int x = 0; x < static_cast<int>(imageSize[0]); ++x) {
                if (img(x,y,z) != 0) { is_found = true; max[1] = y; break; }
            }
            if (is_found) break;
        }
        if (is_found) break;
    }
    
    is_found = false;
    for (int x = 0; x < static_cast<int>(imageSize[0]); ++x) {
        for (int z = 0; z < static_cast<int>(imageSize[2]); ++z) {
            for (int y = 0; y < static_cast<int>(imageSize[1]); ++y) {
                if (img(x,y,z) != 0) { is_found = true; min[0] = x; break; }
            }
            if (is_found) break;
        }
        if (is_found) break;
    }
    
    is_found = false;
    for (int x = static_cast<int>(imageSize[0])-1; x >= 0; --x) {
        for (int z = 0; z < static_cast<int>(imageSize[2]); ++z) {
            for (int y = 0; y < static_cast<int>(imageSize[1]); ++y) {
                if (img(x,y,z) != 0) { is_found = true; max[0] = x; break; }
            }
            if (is_found) break;
        }
        if (is_found) break;
    }
}

template <typename T>
void mxImageUtils::BoundingBox2D(const mxImage<T>& image, int idx, vect3<int>& min, vect3<int>& max)
{
	vect3<unsigned int> imageSize = image.GetImageSize();

	min[2] = max[2] = idx;

	bool is_found = false;
	for (int y = 0; y < static_cast<int>(imageSize[1]); ++y) 
	{
		for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
			if (image(x, y, idx) != 0) { is_found = true; min[1] = y; break; }
		if (is_found) break;
	}

	is_found = false;
	for (int y = imageSize[1] - 1; y >= 0; --y)
	{
		for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
			if (image(x, y, idx) != 0) { is_found = true; max[1] = y; break; }
		if (is_found) break;
	}

	is_found = false;
	for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
	{
		for (int y = 0; y < static_cast<int>(imageSize[1]); ++y)
			if (image(x, y, idx) != 0) { is_found = true; min[0] = x; break; }
		if (is_found) break;
	}

	is_found = false;
	for (int x = imageSize[0] - 1; x >= 0; --x)
	{
		for (int y = 0; y < static_cast<int>(imageSize[1]); ++y)
			if (image(x, y, idx) != 0) { is_found = true; max[0] = x; break; }
		if (is_found) break;
	}
}

// get boundary points in world coordinates
template <typename T>
void mxImageUtils::BoundaryPoints(const mxImage<T>& image, T threshold, std::vector< vect3<float> >& points, bool isWorldCoordinate)
{
    vect3<unsigned int> imageSize = image.GetImageSize();
    
    for (unsigned int z = 0; z < imageSize[2]; ++z) {
        for (unsigned int y = 0; y < imageSize[1]; ++y) {
            for (unsigned int x = 0; x < imageSize[0]; ++x) {
                
                bool isOnObject = image(x,y,z) >= threshold;
                if (!isOnObject)
                    continue;
                
                bool isOnBoundary = false;
                for (int dz = -1; dz <= 1; ++dz) {
                    for (int dy = -1; dy <= 1; ++dy) {
                        for (int dx = -1; dx <= 1; ++dx) {
                            
                            vect3<int> coord;
                            coord[0] = static_cast<int>(x) + dx;
                            coord[1] = static_cast<int>(y) + dy;
                            coord[2] = static_cast<int>(z) + dz;
                            
                            if (!image.PtInImage(coord[0], coord[1], coord[2]) || image(coord[0],coord[1],coord[2]) < threshold)
                            {
                                isOnBoundary = true;
                                break;
                            }
                        }
                        if (isOnBoundary) break;
                    }
                    if(isOnBoundary) break;
                }
                
                if (isOnObject && isOnBoundary) {
                    vect3<float> point( static_cast<float>(x), static_cast<float>(y), static_cast<float>(z) );
                    if( isWorldCoordinate)
                        mxImageUtils::Voxel2World(image, point);
                    points.push_back(point);
                }
            }
        }
    }
}
    
// Threshold value
template <typename T>
void mxImageUtils::Threshold(mxImage<T>& image, T threshold, T greaterValue, T eqLessValue)
{
    vect3<unsigned int> imageSize = image.GetImageSize();
    T* data = image.GetData();
    unsigned int length = imageSize[0] * imageSize[1] * imageSize[2];
    
    #pragma omp parallel for
    for (int i = 0; i < static_cast<int>(length); ++i) {
        if (data[i] > threshold)
            data[i] = greaterValue;
        else
            data[i] = eqLessValue;
    }
}

template <typename T, typename TOut>
void mxImageUtils::Threshold(const mxImage<T>& image, mxImage<TOut>& outImage, T threshold, TOut greaterValue, TOut eqLessValue)
{
	outImage.CopyImageInfo(image);
	outImage.SetImageSize(image.GetImageSize());
	
	vect3<unsigned int> imageSize = image.GetImageSize();
	const T* data = image.GetData();
	TOut* outData = outImage.GetData();
	unsigned int length = imageSize[0] * imageSize[1] * imageSize[2];

	#pragma omp parallel for
	for (int i = 0; i < static_cast<int>(length); ++i) {
		if (data[i] > threshold)
			outData[i] = greaterValue;
		else
			outData[i] = eqLessValue;
	}
}

/** @brief pick largest connected component */
template <typename T>
void mxImageUtils::PickLargestComponent(mxImage<T>& image)
{
	mxImage<unsigned char> tagImage;
	vect3<unsigned int> imageSize = image.GetImageSize();

	tagImage.SetImageSize(imageSize);
	tagImage.Fill(0);

	int tagIndex = 0;

	T* imageData = image.GetData();
	unsigned char* tagData = tagImage.GetData();
	std::queue< vect3<int> > q;

	std::vector<int> tagSizes;

	// one pass loop
	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z) 
	{
		// std::cout << z << std::endl;

		for (int y = 0; y < static_cast<int>(imageSize[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
			{
				unsigned int index = z*imageSize[0]*imageSize[1]+y*imageSize[0]+x;

				if ( imageData[index] > 0 && tagData[index] == 0 )
				{
					++ tagIndex;
					tagData[index] = tagIndex;
					q.push( vect3<int>(x,y,z) );

					int tagSize = 1;
					
					while (!q.empty())
					{
						vect3<int> p = q.front();
						q.pop();

						for (int dz = -1; dz <= 1; ++dz)
						{
							for (int dy = -1; dy <= 1; ++dy)
							{
								for (int dx = -1; dx <= 1; ++dx)
								{
									vect3<int> nb(p[0]+dx, p[1]+dy, p[2]+dz);
									if (nb[0] < 0 || nb[1] < 0 || nb[2] < 0 || 
										nb[0] >= static_cast<int>(imageSize[0]) ||
										nb[1] >= static_cast<int>(imageSize[1]) || 
										nb[2] >= static_cast<int>(imageSize[2]) )
										continue;

									unsigned int nbIndex = nb[2]*imageSize[0]*imageSize[1] + nb[1]*imageSize[0] + nb[0];
									if(tagData[nbIndex] == 0 && imageData[nbIndex] > 0 ) 
									{
										tagData[nbIndex] = tagIndex;
										++ tagSize;
										q.push(nb);
									}
								}
							}
						} // end nb loop
					} // end while

					tagSizes.push_back(tagSize);
				} // end one tag region
			}
		}
	}

	int largestTagIndex = -1;
	int largestSize = -1;
	for (int i = 0; i < static_cast<int>(tagSizes.size()); ++i)
	{
		if(tagSizes[i] > largestSize) 
		{
			largestSize = tagSizes[i];
			largestTagIndex = i + 1;
		}
	}

	for (unsigned int i = 0; i < imageSize[0] * imageSize[1] * imageSize[2]; ++i)
	{
		if(tagData[i] != largestTagIndex)
			imageData[i] = 0;
	}
}

template <typename T>
void mxImageUtils::RemoveSmallComponents(mxImage<T>& image, int volumesize)
{
	mxImage<unsigned char> tagImage;
	vect3<unsigned int> imageSize = image.GetImageSize();

	tagImage.SetImageSize(imageSize);
	tagImage.Fill(0);

	int tagIndex = 0;

	T* imageData = image.GetData();
	unsigned char* tagData = tagImage.GetData();
	std::queue< vect3<int> > q;

	std::vector<int> tagSizes;

	// one pass loop
	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z)
	{
		// std::cout << z << std::endl;

		for (int y = 0; y < static_cast<int>(imageSize[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
			{
				unsigned int index = z*imageSize[0] * imageSize[1] + y*imageSize[0] + x;

				if (imageData[index] > 0 && tagData[index] == 0)
				{
					++tagIndex;
					tagData[index] = tagIndex;
					q.push(vect3<int>(x, y, z));

					int tagSize = 1;

					while (!q.empty())
					{
						vect3<int> p = q.front();
						q.pop();

						for (int dz = -1; dz <= 1; ++dz)
						{
							for (int dy = -1; dy <= 1; ++dy)
							{
								for (int dx = -1; dx <= 1; ++dx)
								{
									vect3<int> nb(p[0] + dx, p[1] + dy, p[2] + dz);
									if (nb[0] < 0 || nb[1] < 0 || nb[2] < 0 ||
										nb[0] >= static_cast<int>(imageSize[0]) ||
										nb[1] >= static_cast<int>(imageSize[1]) ||
										nb[2] >= static_cast<int>(imageSize[2]))
										continue;

									unsigned int nbIndex = nb[2] * imageSize[0] * imageSize[1] + nb[1] * imageSize[0] + nb[0];
									if (tagData[nbIndex] == 0 && imageData[nbIndex] > 0)
									{
										tagData[nbIndex] = tagIndex;
										++tagSize;
										q.push(nb);
									}
								}
							}
						} // end nb loop
					} // end while

					tagSizes.push_back(tagSize);
				} // end one tag region
			}
		}
	}

	std::set<int> remove_idxes;
	for (int i = 0; i < static_cast<int>(tagSizes.size()); ++i)
	{
		if (tagSizes[i] <= volumesize)
			remove_idxes.insert(i + 1);
	}

	for (unsigned int i = 0; i < imageSize[0] * imageSize[1] * imageSize[2]; ++i)
	{
		if (remove_idxes.count(tagData[i]) != 0)
			imageData[i] = 0;
	}
}

template <typename T>
void mxImageUtils::BinaryFillHoles(mxImage<T>& image, T foreground)
{
	mxImage<unsigned char> tagImage;
	vect3<unsigned int> imageSize = image.GetImageSize();
	tagImage.SetImageSize(imageSize);
	tagImage.Fill(0);

	std::queue< vect3<int> > q;
	
	for (int z = 0; z <= 1; ++z)
	{
		for (int y = 0; y <= 1; ++y)
		{
			for (int x = 0; x <= 1; ++x)
			{
				vect3<int> seed;
				if (x == 0)
					seed[0] = 0;
				else
					seed[0] = static_cast<int>(imageSize[0])-1;

				if (y == 0)
					seed[1] = 0;
				else
					seed[1] = static_cast<int>(imageSize[1])-1;

				if (z == 0)
					seed[2] = 0;
				else
					seed[2] = static_cast<int>(imageSize[2])-1;

				if( image(seed[0], seed[1], seed[2]) != foreground ) {
					tagImage(seed[0], seed[1], seed[2]) = 1;
					q.push(seed);
				}
			}
		}
	}

	// region growing
	while ( !q.empty() )
	{
		vect3<int> p = q.front();
		q.pop();

		for (int dz = -1; dz <= 1; ++dz)
		{
			for (int dy = -1; dy <= 1; ++dy)
			{
				for (int dx = -1; dx <= 1; ++dx)
				{
					if( std::abs(dz) + std::abs(dy) + std::abs(dx) != 1 )
						continue;

					vect3<int> nb( p[0]+dx, p[1]+dy, p[2]+dz );
					
					if(nb[0] < 0 || nb[1] < 0 || nb[2] < 0 ||
						nb[0] >= static_cast<int>(imageSize[0]) || 
						nb[1] >= static_cast<int>(imageSize[1]) || 
						nb[2] >= static_cast<int>(imageSize[2]))
						continue;

					if ( tagImage(nb[0], nb[1], nb[2]) == 0 && image(nb[0], nb[1], nb[2]) != foreground )
					{
						tagImage(nb[0], nb[1], nb[2]) = 1;
						q.push(nb);
					}

				}
			}
		}
	}

	const unsigned char* tagData = tagImage.GetData();
	T* imageData = image.GetData();

	for (unsigned int i = 0; i < imageSize[0]*imageSize[1]*imageSize[2]; ++i)
	{
		if (tagData[i] == 0)
			imageData[i] = foreground;
	}

}

template <typename T>
void mxImageUtils::BinaryFillHoles2D(mxImage<T>& image, unsigned int sliceIdx, T foreground)
{
	mxImage<unsigned char> tagSlice;
	vect3<unsigned int> imageSize = image.GetImageSize();
	vect3<unsigned int> sliceSize( imageSize[0], imageSize[1], 1 );

	tagSlice.SetImageSize(sliceSize);
	tagSlice.Fill(0);

	std::queue< vect3<int> > q;

	for (int y = 0; y <= 1; ++y) {
		for (int x = 0; x <= 1; ++x) {
			vect3<int> seed;
			if (x == 0)
				seed[0] = 0;
			else
				seed[0] = static_cast<int>(imageSize[0]) - 1;

			if (y == 0)
				seed[1] = 0;
			else
				seed[1] = static_cast<int>(imageSize[1]) - 1;

			seed[2] = sliceIdx;

			if (image(seed[0], seed[1], seed[2]) != foreground) {
				tagSlice(seed[0], seed[1], 0) = 1;
				q.push(seed);
			}
		}
	}

	// region growing
	while (!q.empty())
	{
		vect3<int> p = q.front();
		q.pop();

		for (int dy = -1; dy <= 1; ++dy)
		{
			for (int dx = -1; dx <= 1; ++dx)
			{
				if (std::abs(dy) + std::abs(dx) != 1)
					continue;

				vect3<int> nb(p[0] + dx, p[1] + dy, p[2]);

				if ( nb[0] < 0 || nb[1] < 0 || nb[0] >= static_cast<int>(imageSize[0]) || nb[1] >= static_cast<int>(imageSize[1]) )
					continue;

				if (tagSlice(nb[0], nb[1], 0) == 0 && image(nb[0], nb[1], nb[2]) != foreground)
				{
					tagSlice(nb[0], nb[1], 0) = 1;
					q.push(nb);
				}

			}
		}
	}

	for (unsigned int y = 0; y < imageSize[1]; ++y)
	{
		for (unsigned int x = 0; x < imageSize[0]; ++x)
		{
			if (tagSlice(x, y, 0) == 0)
				image(x, y, sliceIdx) = foreground;
		}
	}
}


template <typename T>
void mxImageUtils::BinaryErode2D(mxImage<T>& image, int slice_idx, T foreground)
{
	vect3<unsigned int> imageSize = image.GetImageSize();
	std::vector< vect3<int> > erodeVoxels;

	// loop
	for (int y = 0; y < static_cast<int>(imageSize[1]); ++y)
	{
		for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
		{
			if (image(x, y, slice_idx) != foreground)
				continue;

			bool isErode = false;
			for (int dy = -1; dy <= 1; ++dy)
			{
				for (int dx = -1; dx <= 1; ++dx)
				{
					vect3<int> nb(x + dx, y + dy, slice_idx);
					if (nb[0] < 0 || nb[1] < 0 || nb[0] >= static_cast<int>(imageSize[0]) || nb[1] >= static_cast<int>(imageSize[1]))
						continue;

					if (image(nb[0], nb[1], nb[2]) != foreground) {
						isErode = true;
						break;
					}
				}
				if (isErode) break;
			}

			if (isErode)
				erodeVoxels.push_back(vect3<int>(x, y, slice_idx));
		}
	}

	for (unsigned int i = 0; i < erodeVoxels.size(); ++i) 
		image(erodeVoxels[i][0], erodeVoxels[i][1], erodeVoxels[i][2]) = 0;
}


template <typename T>
void mxImageUtils::BinaryErode(mxImage<T>& image, T foreground)
{
	vect3<unsigned int> imageSize = image.GetImageSize();
	std::vector< vect3<int> > erodeVoxels;

	// loop
	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(imageSize[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
			{
				if(image(x,y,z) != foreground)
					continue;

				bool isErode = false;
				for (int dz = -1; dz <= 1; ++dz)
				{
					for (int dy = -1; dy <= 1; ++ dy)
					{
						for (int dx = -1; dx <= 1; ++dx)
						{
							vect3<int> nb(x+dx, y+dy, z+dz);
							if(nb[0] < 0 || nb[1] < 0 || nb[2] < 0 ||
								nb[0] >= static_cast<int>(imageSize[0]) ||
								nb[1] >= static_cast<int>(imageSize[1]) ||
								nb[2] >= static_cast<int>(imageSize[2]) )
								continue;

							if( image(nb[0], nb[1], nb[2]) != foreground ) {
								isErode = true;
								break;
							}
						}
						if(isErode) break;
					}
					if(isErode) break;
				}
				if(isErode)
					erodeVoxels.push_back(vect3<int>(x,y,z));
			}
		}
	}

	for (unsigned int i = 0; i < erodeVoxels.size(); ++i) {
		image(erodeVoxels[i][0], erodeVoxels[i][1], erodeVoxels[i][2]) = 0;
	}
}


template <typename T>
void mxImageUtils::BinaryDilate2D(mxImage<T>& image, int slice_idx, T foreground)
{
	vect3<unsigned int> imageSize = image.GetImageSize();
	std::vector< vect3<int> > dilateVoxels;

	for (int y = 0; y < static_cast<int>(imageSize[1]); ++y)
	{
		for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
		{
			if (image(x, y, slice_idx) == foreground)
				continue;

			bool isDilate = false;
			for (int dy = -1; dy <= 1; ++dy)
			{
				for (int dx = -1; dx <= 1; ++dx)
				{
					vect3<int> nb(x + dx, y + dy, slice_idx);
					if (nb[0] < 0 || nb[1] < 0 || nb[0] >= static_cast<int>(imageSize[0]) || nb[1] >= static_cast<int>(imageSize[1]))
						continue;

					if (image(nb[0], nb[1], nb[2]) == foreground) {
						isDilate = true;
						break;
					}
				}
				if (isDilate) break;
			}

			if (isDilate)
				dilateVoxels.push_back(vect3<int>(x, y, slice_idx));
		}
	}

	for (unsigned int i = 0; i < dilateVoxels.size(); ++i)
		image(dilateVoxels[i][0], dilateVoxels[i][1], dilateVoxels[i][2]) = foreground;
}


/** @brief binary dilate */
template <typename T>
void mxImageUtils::BinaryDilate(mxImage<T>& image, T foreground)
{
	vect3<unsigned int> imageSize = image.GetImageSize();
	std::vector< vect3<int> > dilateVoxels;

	// loop
	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(imageSize[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
			{
				if(image(x,y,z) == foreground)
					continue;

				bool isDilate = false;
				for (int dz = -1; dz <= 1; ++dz)
				{
					for (int dy = -1; dy <= 1; ++ dy)
					{
						for (int dx = -1; dx <= 1; ++dx)
						{
							vect3<int> nb(x+dx, y+dy, z+dz);
							if(nb[0] < 0 || nb[1] < 0 || nb[2] < 0 ||
								nb[0] >= static_cast<int>(imageSize[0]) ||
								nb[1] >= static_cast<int>(imageSize[1]) ||
								nb[2] >= static_cast<int>(imageSize[2]) )
								continue;

							if( image(nb[0], nb[1], nb[2]) == foreground ) {
								isDilate = true;
								break;
							}
						}
						if(isDilate) break;
					}
					if(isDilate) break;
				}
				if(isDilate)
					dilateVoxels.push_back(vect3<int>(x,y,z));
			}
		}
	}

	for (unsigned int i = 0; i < dilateVoxels.size(); ++i)
		image(dilateVoxels[i][0], dilateVoxels[i][1], dilateVoxels[i][2]) = foreground;
}


template <typename T>
void mxImageUtils::BinaryNarrowBand(const mxImage<T>& image, int band_len, mxImage<T>& outImage, T foreground)
{
	mxImage<T> dilated_image, eroded_image;

	mxImageUtils::Copy(image, dilated_image);
	mxImageUtils::Copy(image, eroded_image);

	for (int i = 0; i < band_len; ++i)
	{
		mxImageUtils::BinaryDilate(dilated_image, foreground);
		mxImageUtils::BinaryErode(eroded_image, foreground);
	}

	outImage.CopyImageInfo(image);
	outImage.SetImageSize(image.GetImageSize());
	outImage.Fill(0);

	vect3<unsigned int> image_size = outImage.GetImageSize();

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
			{
				if (dilated_image(x, y, z) != 0 && eroded_image(x, y, z) == 0)
					outImage(x, y, z) = foreground;
				else
					outImage(x, y, z) = 0;
			}
		}
	}
}


template <typename T>
void mxImageUtils::BinaryNarrowBand2D(const mxImage<T>& image, int band_len, mxImage<T>& outImage, T foreground)
{
	mxImage<T> dilated_image, eroded_image;

	mxImageUtils::Copy(image, dilated_image);
	mxImageUtils::Copy(image, eroded_image);

	vect3<unsigned int> image_size = image.GetImageSize();
	for (int i = 0; i < band_len; ++i)
	{
		for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
		{
			mxImageUtils::BinaryDilate2D(dilated_image, z, foreground);
			mxImageUtils::BinaryErode2D(eroded_image, z, foreground);
		}
	}

	outImage.CopyImageInfo(image);
	outImage.SetImageSize(image.GetImageSize());
	outImage.Fill(0);

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
			{
				if (dilated_image(x, y, z) != 0 && eroded_image(x, y, z) == 0)
					outImage(x, y, z) = foreground;
				else
					outImage(x, y, z) = 0;
			}
		}
	}
}


template <typename TIn, typename TOut>
void mxImageUtils::GaussianSmooth(const mxImage<TIn>& image, mxImage<TOut>& outImage, double sigma, int kernelRadius)
{
	if (kernelRadius < 0)
		kernelRadius = static_cast<int>(sigma * 3 + 0.5);

	// setup kernel
	std::vector<double> kernel;
	kernel.resize(2*kernelRadius+1);

	for (int i = 0; i < static_cast<int>(kernel.size()); ++i)
		kernel[i] = 1/(sqrt(2*M_PI)*sigma) * exp(- (i-kernelRadius)*(i-kernelRadius) / (2*sigma*sigma) );

	vect3<unsigned int> imageSize = image.GetImageSize();
	outImage.CopyImageInfo(image);
	outImage.SetImageSize(imageSize);

	mxImage<TOut> tmpOutImage;
	tmpOutImage.CopyImageInfo(image);
	tmpOutImage.SetImageSize(imageSize);


	// x filtering
	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(imageSize[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
			{
				double sum = 0, count = 0;

				for (int k = -kernelRadius; k <= kernelRadius; ++k)
				{
					int nx = x + k;
					if(nx < 0 || nx >= static_cast<int>(imageSize[0]))
						continue;
					count += kernel[k+kernelRadius];
					sum += image(nx,y,z) * kernel[k+kernelRadius];
				}

				outImage(x,y,z) = static_cast<TOut>(sum / count);
			}
		}
	}

	// y filtering
	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(imageSize[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
			{
				double sum = 0, count = 0;

				for (int k = -kernelRadius; k <= kernelRadius; ++k)
				{
					int ny = y + k;
					if(ny < 0 || ny >= static_cast<int>(imageSize[1]))
						continue;
					count += kernel[k+kernelRadius];
					sum += outImage(x,ny,z) * kernel[k+kernelRadius];
				}

				tmpOutImage(x,y,z) = static_cast<TOut>(sum / count);
			}
		}
	}

	// z filtering
	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(imageSize[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
			{
				double sum = 0, count = 0;

				for (int k = -kernelRadius; k <= kernelRadius; ++k)
				{
					int nz = z + k;
					if(nz < 0 || nz >= static_cast<int>(imageSize[2]))
						continue;
					count += kernel[k+kernelRadius];
					sum += tmpOutImage(x,y,nz) * kernel[k+kernelRadius];
				}

				outImage(x,y,z) = static_cast<TOut>(sum / count);
			}
		}
	}
}

template <typename TIn, typename TOut>
void mxImageUtils::GaussianSmooth1D(const std::vector<TIn>& line, std::vector<TOut>& outLine, double sigma, int kernelRadius)
{
	if (kernelRadius < 0)
		kernelRadius = static_cast<int>(sigma * 3 + 0.5);

	// setup kernel
	std::vector<double> kernel;
	kernel.resize(2 * kernelRadius + 1);

	for (int i = 0; i < static_cast<int>(kernel.size()); ++i)
		kernel[i] = 1 / (sqrt(2 * M_PI)*sigma) * exp(-(i - kernelRadius)*(i - kernelRadius) / (2 * sigma*sigma));

	outLine.resize(line.size());

	#pragma omp parallel for
	for (int x = 0; x < static_cast<int>(line.size()); ++x)
	{
		double sum = 0, count = 0;

		for (int k = -kernelRadius; k <= kernelRadius; ++k)
		{
			int nx = x + k;
			if (nx < 0 || nx >= static_cast<int>(line.size()))
				continue;
			count += kernel[k + kernelRadius];
			sum += line[x] * kernel[k + kernelRadius];
		}

		outLine[x] = static_cast<TOut>(sum / count);
	}
}

template <typename T>
T mxImageUtils::MaxIntensity(const mxImage<T>& image)
{
	vect3<unsigned int> imageSize = image.GetImageSize();

	T max = std::numeric_limits<T>::min();
	const T* data = image.GetData();

	for (unsigned int i = 0; i < imageSize[0]*imageSize[1]*imageSize[2]; ++i) 
	{
		if(data[i] > max)
			max = data[i];
	}

	return max;
}

template <typename T>
T mxImageUtils::MinIntensity(const mxImage<T>& image)
{
	vect3<unsigned int> imageSize = image.GetImageSize();

	T min = std::numeric_limits<T>::max();
	const T* data = image.GetData();

	for (unsigned int i = 0; i < imageSize[0] * imageSize[1] * imageSize[2]; ++i)
	{
		if (data[i] < min)
			min = data[i];
	}

	return min;
}


template <typename T>
vect3<double> mxImageUtils::MassCenter(const mxImage<T>& image)
{
	vect3<double> center(0,0,0);
	unsigned int count = 0;
	vect3<unsigned int> imageSize = image.GetImageSize();

	for (unsigned int z = 0; z < imageSize[2]; ++z)
	{
		for (unsigned int y = 0; y < imageSize[1]; ++y)
		{
			for (unsigned int x = 0; x < imageSize[0]; ++x)
			{
				if(image(x,y,z) > 0)
				{
					center[0] += x;
					center[1] += y;
					center[2] += z;
					++ count;
				}
			}
		}
	}

	for (unsigned int i = 0; i < 3; ++i)
		center[i] = center[i] / count;

	return center;
}
     
void mxImageUtils::MarginalBound(const vect3<unsigned int>& boxSize, const vect3<unsigned int>& patchSize, vect3<unsigned int>& lowerBound, vect3<unsigned int>& upperBound)
{
    for(int i = 0; i < 3; ++i) {
        lowerBound[i] = patchSize[i] / 2;
        upperBound[i] = boxSize[i] + patchSize[i]/2 - patchSize[i];
    }
}
  
template <typename T>
void mxImageUtils::BoxWorld2Voxel(const mxImage<T>& image, const vect3<double>& center, const vect3<double>& size, vect3<unsigned int>& sp, vect3<unsigned int>& ep, const vect3<unsigned int>& patchSize)
{
    vect3<double> spWorld, epWorld;
    for (int i = 0; i < 3; ++i)
    {
        spWorld[i] = center[i] - size[i] / 2;
        epWorld[i] = center[i] + size[i] / 2;
    }
    
    vect3<int> spVoxel, epVoxel;
    mxImageUtils::World2Voxel(image, spWorld, spVoxel);
    mxImageUtils::World2Voxel(image, epWorld, epVoxel);
    
    for (int i = 0; i < 3; ++i) {
        if(spVoxel[i] > epVoxel[i])
            std::swap(spVoxel[i], epVoxel[i]);
    }
    
    vect3<int> lowerBound, upperBound;
    vect3<unsigned int> imageSize = image.GetImageSize();
    for (int i = 0; i < 3; ++i) {
        lowerBound[i] = patchSize[i] / 2;
        upperBound[i] = static_cast<int>(imageSize[i]) + static_cast<int>(patchSize[i]/2) - static_cast<int>(patchSize[i]);
    }
    
    // adjust box to the lower and upper bound
    for (int i = 0; i < 3; ++i) {
        if(spVoxel[i] < lowerBound[i])
            spVoxel[i] = lowerBound[i];
        if(epVoxel[i] > upperBound[i])
            epVoxel[i] = upperBound[i];
    }
    
    for (int i = 0; i < 3; ++i) {
        if(spVoxel[i] > epVoxel[i]) {
            std::cerr << "invalid image region (check image size and patch size)" << std::endl;
            exit(-1);
        }
    }
    
    for (int i = 0; i < 3; ++i) {
        sp[i] = static_cast<unsigned int>(spVoxel[i]);
        ep[i] = static_cast<unsigned int>(epVoxel[i]);
    }
}


template <typename T, typename TOut>
void mxImageUtils::GetResampleHeader(const mxImage<T>& image, const vect3<double>& spacing, mxImage<TOut>& outImage)
{
	vect3<unsigned int> imageSize = image.GetImageSize();
	vect3<double> imageSpacing = image.GetSpacing();

	vect3<double> physicalSize;
	for (unsigned int i = 0; i < 3; ++i)
		physicalSize[i] = imageSize[i] * imageSpacing[i];

	vect3<unsigned int> resampleSize;
	for (unsigned int i = 0; i < 3; ++i)
		resampleSize[i] = static_cast<unsigned int>(physicalSize[i] / spacing[i] + 0.5);

	outImage.CopyImageInfo(image);
	outImage.SetRawImageSize(resampleSize);
	outImage.SetSpacing(spacing);
}

    
template <typename T, typename TOut>
void mxImageUtils::Resample(const mxImage<T>& image, const vect3<double>& spacing, mxImage<TOut>& outImage, InterpolationType type)
{
	if (type == IT_NEAREST_NEIGHBOR_INTERPOLATION)
	{
		vect3<unsigned int> imageSize = image.GetImageSize();
		vect3<double> imageSpacing = image.GetSpacing();

		vect3<double> physicalSize;
		for (unsigned int i = 0; i < 3; ++i)
			physicalSize[i] = imageSize[i] * imageSpacing[i];

		vect3<unsigned int> resampleSize;
		for (unsigned int i = 0; i < 3; ++i)
			resampleSize[i] = static_cast<unsigned int>(physicalSize[i] / spacing[i] + 0.5);

		outImage.CopyImageInfo(image);
		outImage.SetImageSize(resampleSize);
		outImage.SetSpacing(spacing);

		#pragma omp parallel for
		for (int z = 0; z < static_cast<int>(resampleSize[2]); ++z)
		{
			for (unsigned int y = 0; y < resampleSize[1]; ++y)
			{
				for (unsigned int x = 0; x < resampleSize[0]; ++x)
				{
					vect3<double> coord(x, y, z);
					mxImageUtils::Voxel2World(outImage, coord);
					mxImageUtils::World2Voxel(image, coord);

					vect3<int> pt(static_cast<int>(coord[0] + 0.5), static_cast<int>(coord[1] + 0.5), static_cast<int>(coord[2] + 0.5));
					if (image.PtInImage(pt[0], pt[1], pt[2]))
						outImage(x, y, z) = static_cast<TOut>(image(pt[0], pt[1], pt[2]));
					else
						outImage(x, y, z) = 0;
				}
			}
		}
	}
	else if (type == IT_LINEAR_INTERPOLATION)
	{
		ImageLinearInterpolator<T, TOut> interpolator;
		ResampleWithInterpolator(image, spacing, interpolator, outImage);
	}
	else
	{
		err_message("unrecognized interpolation type");
	}
}

template <typename T, typename TOut>
void mxImageUtils::ResampleWithInterpolator(const mxImage<T>& image, const vect3<double>& spacing, IImageInterpolator<T,TOut>& interpolator, mxImage<TOut>& outImage)
{
	vect3<unsigned int> imageSize = image.GetImageSize();
	vect3<double> imageSpacing = image.GetSpacing();

	vect3<double> physicalSize;
	for (unsigned int i = 0; i < 3; ++i)
		physicalSize[i] = imageSize[i] * imageSpacing[i];

	vect3<unsigned int> resampleSize;
	for (unsigned int i = 0; i < 3; ++i)
		resampleSize[i] = static_cast<unsigned int>(physicalSize[i] / spacing[i] + 0.5);

	outImage.CopyImageInfo(image);
	outImage.SetImageSize(resampleSize);
	outImage.SetSpacing(spacing);

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(resampleSize[2]); ++z)
	{
		for (unsigned int y = 0; y < resampleSize[1]; ++y)
		{
			for (unsigned int x = 0; x < resampleSize[0]; ++x)
			{
				vect3<double> coord(x, y, z);
				mxImageUtils::Voxel2World(outImage, coord);
				mxImageUtils::World2Voxel(image, coord);

				outImage(x,y,z) = interpolator.Get(image, coord);
			}
		}
	}
}

template <typename T, typename TOut>
void mxImageUtils::ResampleWithPadding(const mxImage<T>& image, const vect3<unsigned int>& patchSize, const vect3<double>& spacing, mxImage<TOut>& outImage)
{
	vect3<unsigned int> imageSize = image.GetImageSize();
	vect3<double> imageSpacing = image.GetSpacing();
	vect3<double> imageOrigin = image.GetOrigin();
	const double* axis = image.GetAxis();

	vect3< vect3<double> > directions;
	directions[0][0] = axis[0]; directions[0][1] = axis[1]; directions[0][2] = axis[2];
	directions[1][0] = axis[3]; directions[1][1] = axis[4]; directions[1][2] = axis[5];
	directions[2][0] = axis[6]; directions[2][1] = axis[7]; directions[2][2] = axis[8];

	vect3<double> physicalSize;
	for( unsigned int i = 0; i < 3; ++i )
		physicalSize[i] = imageSize[i] * imageSpacing[i];
	
	vect3<unsigned int> resampleSize;
	for ( unsigned int i = 0; i < 3; ++i ) 
		resampleSize[i] = static_cast<unsigned int>(physicalSize[i] / spacing[i] + 0.5) + patchSize[i] - 1;
	
	vect3<double> halfPatchSize( patchSize[0]*spacing[0]/2, patchSize[1]*spacing[1]/2, patchSize[2]*spacing[2]/2 );
	vect3<double> origin = imageOrigin - directions[0]*halfPatchSize[0] - directions[1]*halfPatchSize[1] - directions[2]*halfPatchSize[2];

	outImage.SetAxis( axis );
	outImage.SetOrigin( origin );
	outImage.SetImageSize( resampleSize );
	outImage.SetSpacing( spacing );
	outImage.Fill(0);

	vect3<unsigned int> sp, ep;
	MarginalBound(resampleSize, patchSize, sp, ep);

	#pragma omp parallel for
	for (int z = static_cast<int>(sp[2]); z <= static_cast<int>(ep[2]); ++z)
	{
		for (int y = static_cast<int>(sp[1]); y <= static_cast<int>(ep[1]); ++y)
		{
			for (int x = static_cast<int>(sp[0]); x <= static_cast<int>(ep[0]); ++x)
			{
				vect3<double> coord(x,y,z);
				mxImageUtils::Voxel2World(outImage,coord);
				mxImageUtils::World2Voxel(image,coord);

				vect3<int> pt( static_cast<int>(coord[0]+0.5), static_cast<int>(coord[1]+0.5), static_cast<int>(coord[2]+0.5) );
				if (image.PtInImage(pt[0], pt[1], pt[2]))
					outImage(x,y,z) = static_cast<TOut>( image(pt[0], pt[1], pt[2]) );
				else
					outImage(x,y,z) = 0;
			}
		}
	}
}

template <typename T, typename TOut>
void mxImageUtils::ImagePad(const mxImage<T>& image, const vect3<unsigned int>& patchSize, mxImage<TOut>& outImage, TOut defaultVal)
{
	vect3<double> origin = image.GetOrigin();
	vect3<unsigned int> imageSize = image.GetImageSize();
	vect3<double> spacing = image.GetSpacing();
	const double* axis = image.GetAxis();

	vect3< vect3<double> > directions;
	directions[0][0] = axis[0]; directions[0][1] = axis[1]; directions[0][2] = axis[2];
	directions[1][0] = axis[3]; directions[1][1] = axis[4]; directions[1][2] = axis[5];
	directions[2][0] = axis[6]; directions[2][1] = axis[7]; directions[2][2] = axis[8];

	vect3<unsigned int> halfPatchSize(patchSize[0]/2, patchSize[1]/2, patchSize[2]/2);
	vect3<double> origin_new = origin;
	for(int i = 0; i < 3; ++i)
		origin_new = origin_new - directions[i]*halfPatchSize[i]*spacing[i];

	vect3<unsigned int> imageSize_new;
	for(int i = 0; i < 3; ++i)
		imageSize_new[i] = imageSize[i] + patchSize[i] - 1;

	outImage.CopyImageInfo(image);
	outImage.SetOrigin(origin_new);
	outImage.SetImageSize(imageSize_new);
	outImage.Fill(defaultVal);
	
	vect3<unsigned int> sp(patchSize[0]/2,patchSize[1]/2,patchSize[2]/2);
	vect3<unsigned int> ep;
	for(int i = 0; i < 3; ++i)
		ep[i] = imageSize_new[i] + patchSize[i]/2 - patchSize[i];

	#pragma omp parallel for
	for(int z = static_cast<int>(sp[2]); z <= static_cast<int>(ep[2]); ++z)
	{
		for(int y = static_cast<int>(sp[1]); y <= static_cast<int>(ep[1]); ++y)
		{
			for(int x = static_cast<int>(sp[0]); x <= static_cast<int>(ep[0]); ++x)
			{
				vect3<unsigned int> corres_voxel(x-patchSize[0]/2, y-patchSize[1]/2, z-patchSize[2]/2);
				outImage(x,y,z) = static_cast<TOut>(image(corres_voxel[0], corres_voxel[1], corres_voxel[2]));
			}
		}
	}
}

template <typename T, typename TOut>
void mxImageUtils::ExtractLargestBlock(const mxImage<T>& image, const vect3<double>& ptWorld, const vect3<double>& radius, const vect3<double>& spacing, mxImage<TOut>& block)
{
	vect3<double> spWorld, epWorld;
	for (unsigned int i = 0; i < 3; ++i)
	{
		spWorld[i] = ptWorld[i] - radius[i];
		epWorld[i] = ptWorld[i] + radius[i];
	}

	vect3<int> spVoxel, epVoxel;
	mxImageUtils::World2Voxel(image, spWorld, spVoxel);
	mxImageUtils::World2Voxel(image, epWorld, epVoxel);
	for (unsigned int i = 0; i < 3; ++i)
	{
		if(spVoxel[i] > epVoxel[i])
			std::swap(spVoxel[i], epVoxel[i]);
	}

	vect3<unsigned int> imageSize = image.GetImageSize();
	for (unsigned int i = 0; i < 3; ++i)
	{
		if (spVoxel[i] < 0)
			spVoxel[i] = 0;
		if (epVoxel[i] >= static_cast<int>(imageSize[i]))
			epVoxel[i] = static_cast<int>(imageSize[i])-1;

		if(spVoxel[i] > epVoxel[i]) 
		{
			std::cerr << "exception in ExtractLargestBlock" << std::endl;
			assert(false);
			exit(-1);
		}
	}

	vect3<double> imageSpacing = image.GetSpacing();
	vect3<unsigned int> blockSize;
	for (unsigned int i = 0; i < 3; ++i)
		blockSize[i] = static_cast<unsigned int>( (epVoxel[i] - spVoxel[i] + 1) * imageSpacing[i] / spacing[i] );

	vect3<double> origin;
	mxImageUtils::Voxel2World(image, spVoxel, origin);

	block.CopyImageInfo(image);
	block.SetOrigin(origin);
	block.SetSpacing(spacing);
	block.SetImageSize(blockSize);

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(blockSize[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(blockSize[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(blockSize[0]); ++x)
			{
				vect3<double> coord(x,y,z);
				mxImageUtils::Voxel2World(block,coord);
				mxImageUtils::World2Voxel(image,coord);

				vect3<int> pt( static_cast<int>(coord[0]+0.5), static_cast<int>(coord[1]+0.5), static_cast<int>(coord[2]+0.5) );
				if (image.PtInImage(pt[0], pt[1], pt[2]))
					block(x,y,z) = static_cast<TOut>(image(pt[0], pt[1], pt[2]));
				else
					block(x,y,z) = 0;
			}
		}
	}
}

template <typename T, typename TOut>
void mxImageUtils::ExtractBlockWithPadding(const mxImage<T>& image, const vect3<double>& ptWorld, const vect3<double>& radius, const vect3<unsigned int>& patchSize, const vect3<double>& spacing, mxImage<TOut>& block)
{
	const double* axis = image.GetAxis();

	vect3< vect3<double> > directions;
	directions[0][0] = axis[0]; directions[0][1] = axis[1]; directions[0][2] = axis[2];
	directions[1][0] = axis[3]; directions[1][1] = axis[4]; directions[1][2] = axis[5];
	directions[2][0] = axis[6]; directions[2][1] = axis[7]; directions[2][2] = axis[8];

	vect3<unsigned int> imageSize = image.GetImageSize();

	vect3< double > min, max(imageSize[0], imageSize[1], imageSize[2]);
	min = image.GetOrigin();
	mxImageUtils::Voxel2World(image, max);

	vect3<double> spWorld, epWorld, offset;
	offset = directions[0]*radius[0] + directions[1]*radius[1] + directions[2]*radius[2];
	spWorld = ptWorld - offset;
	epWorld = ptWorld + offset;

	for (int i = 0; i < 3; ++i)
	{
		double minVal = std::min( min[i], max[i] ), maxVal = std::max( min[i], max[i] );
		if( spWorld[i] < minVal )
			spWorld[i] = minVal;
		if ( spWorld[i] > maxVal )
			spWorld[i] = maxVal;

		if ( epWorld[i] < minVal )
			epWorld[i] = minVal;
		if ( epWorld[i] > maxVal )
			epWorld[i] = maxVal;
	}

	vect3<double> halfPatchSize;
	halfPatchSize[0] = patchSize[0] * spacing[0] / 2;
	halfPatchSize[1] = patchSize[1] * spacing[1] / 2;
	halfPatchSize[2] = patchSize[2] * spacing[2] / 2;

	vect3<double> padding = directions[0]*halfPatchSize[0] + directions[1]*halfPatchSize[1] + directions[2]*halfPatchSize[2];
	spWorld = spWorld - padding;
	epWorld = epWorld + padding;

	vect3<unsigned int> blockSize;
	for (int i = 0; i < 3; ++i)
		blockSize[i] = static_cast<unsigned int>( std::abs(epWorld[i]-spWorld[i]) / spacing[i] + 0.5 );

	block.SetOrigin(spWorld);
	block.SetAxis(axis);
	block.SetSpacing(spacing);
	block.SetImageSize(blockSize);

    #pragma omp parallel for
	for (int z = 0; z < static_cast<int>(blockSize[2]); ++z)
	{
		for(int y = 0; y < static_cast<int>(blockSize[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(blockSize[0]); ++x)
			{
				vect3<double> ptWorld(x,y,z);
				mxImageUtils::Voxel2World(block, ptWorld);

                vect3<int> ptVoxel;
				mxImageUtils::World2Voxel(image, ptWorld, ptVoxel);

				if (ptVoxel[0] < 0 || ptVoxel[1] < 0 || ptVoxel[2] < 0 || ptVoxel[0] >= static_cast<int>(imageSize[0]) || 
					ptVoxel[1] >= static_cast<int>(imageSize[1]) || ptVoxel[2] >= static_cast<int>(imageSize[2]) )
					block(x,y,z) = 0;
				else
					block(x,y,z) = image( ptVoxel[0], ptVoxel[1], ptVoxel[2] );

			}
		}
	}
}

template <typename T>
vect3<double> mxImageUtils::WorldCenter(const mxImage<T>& image)
{
	vect3<unsigned int> centerVoxel;
	vect3<unsigned int> imageSize = image.GetImageSize();

	for (unsigned int i = 0; i < 3; ++i)
		centerVoxel[i] = imageSize[i] / 2;

	vect3<double> centerWorld;
	Voxel2World(image, centerVoxel, centerWorld);

	return centerWorld;
}
    
template <typename T>
void mxImageUtils::FindSliceRange(const mxImage<T>& image, unsigned int& start, unsigned int& end)
{
    vect3<unsigned int> imagesize = image.GetImageSize();
    const T* data = image.GetData();

	// initialization
	start = 1; end = 0;
    
    for (unsigned int i = 0; i < imagesize[2]; ++i) {
        
        bool isFirst = false;
        const T* sliceData = data + i * imagesize[0] * imagesize[1];
        
        for (unsigned int j = 0; j < imagesize[0] * imagesize[1]; ++j) {
            if (sliceData[j] != 0) {
                isFirst = true;
                break;
            }
        }
        
        if (isFirst) {
            start = i;
            break;
        }
    }
    
    for (unsigned int i = imagesize[2] - 1; i >= 0 && i < imagesize[2]; --i) {
        
        bool isEnd = false;
        const T* sliceData = data + i * imagesize[0] * imagesize[1];
        
        for (unsigned int j = 0; j < imagesize[0] * imagesize[1]; ++j) {
            if (sliceData[j] != 0) {
                isEnd = true;
                break;
            }
        }
        
        if (isEnd) {
            end = i;
            break;
        }
    }
}
    
template <typename T>
vect3<double> mxImageUtils::SliceMassCenter(const mxImage<T>& image, unsigned int sliceIdx)
{
	vect3<unsigned int> imageSize = image.GetImageSize();
	vect3<double> center(0,0,0);
	unsigned int count = 0;

	for (unsigned int y = 0; y < imageSize[1]; ++y)
	{
		for (unsigned int x = 0; x < imageSize[0]; ++x)
		{
			if(image(x,y,sliceIdx) > 0)
			{
				center += vect3<double>(x,y,0);
				++ count;
			}
		}
	}

	center /= count;
	center[2] = sliceIdx;
	return center;
}

template <typename T>
void mxImageUtils::FindSliceExtremePoints(const mxImage<T>& image, unsigned int sliceIdx, vect3<unsigned int>& top, vect3<unsigned int>& bot, vect3<unsigned int>& left, vect3<unsigned int>& right)
{
    vect3<unsigned int> imagesize = image.GetImageSize();
    const T* sliceData = image.GetData() + sliceIdx * imagesize[0] * imagesize[1];
    
    bool isBreak = false;
    
    for(unsigned int y = 0; y < imagesize[1]; ++y)
    {
        vect3<unsigned int> sp, ep;
        for(unsigned int x = 0; x < imagesize[0]; ++x)
        {
            if( sliceData[y*imagesize[0]+x] != 0 && isBreak ) {
                ep[0] = x; ep[1] = y; ep[2] = sliceIdx;
            } else if ( sliceData[y*imagesize[0]+x] != 0 && !isBreak )
            {
                sp[0] = ep[0] = x;
                sp[1] = ep[1] = y;
                sp[2] = ep[2] = sliceIdx;
                isBreak = true;
            }
        }
        if( isBreak )
        {
            bot[0] = (sp[0] + ep[0]) / 2;
            bot[1] = (sp[1] + ep[1]) / 2;
            bot[2] = sliceIdx;
            break;
        }
    }
    
    isBreak = false;
    for(unsigned int y = imagesize[1]-1; y >= 0 && y < imagesize[1]; --y)
    {
        vect3<unsigned int> sp, ep;
        for(unsigned int x = 0; x < imagesize[0]; ++x)
        {
            if( sliceData[y*imagesize[0]+x] != 0 && isBreak ) {
                ep[0] = x; ep[1] = y; ep[2] = sliceIdx;
            } else if ( sliceData[y*imagesize[0]+x] != 0 && !isBreak )
            {
                sp[0] = ep[0] = x;
                sp[1] = ep[1] = y;
                sp[2] = ep[2] = sliceIdx;
                isBreak = true;
            }
        }
        if( isBreak )
        {
            top[0] = (sp[0] + ep[0]) / 2;
            top[1] = (sp[1] + ep[1]) / 2;
            top[2] = sliceIdx;
            break;
        }
    }
    
    isBreak = false;
    for(unsigned int x = 0; x < imagesize[0]; ++x)
    {
        vect3<unsigned int> sp, ep;
        for(unsigned int y = 0; y < imagesize[1]; ++y)
        {
            if( sliceData[y*imagesize[0]+x] != 0 && isBreak ) {
                ep[0] = x; ep[1] = y; ep[2] = sliceIdx;
            } else if ( sliceData[y*imagesize[0]+x] != 0 && !isBreak )
            {
                sp[0] = ep[0] = x;
                sp[1] = ep[1] = y;
                sp[2] = ep[2] = sliceIdx;
                isBreak = true;
            }
        }
        if( isBreak )
        {
            left[0] = (sp[0] + ep[0]) / 2;
            left[1] = (sp[1] + ep[1]) / 2;
            left[2] = sliceIdx;
            break;
        }
    }
    
    isBreak = false;
    for(unsigned int x = imagesize[0]-1; x >= 0 && x < imagesize[0]; --x)
    {
        vect3<unsigned int> sp, ep;
        for(unsigned int y = 0; y < imagesize[1]; ++y)
        {
            if( sliceData[y*imagesize[0]+x] != 0 && isBreak ) {
                ep[0] = x; ep[1] = y; ep[2] = sliceIdx;
            } else if ( sliceData[y*imagesize[0]+x] != 0 && !isBreak )
            {
                sp[0] = ep[0] = x;
                sp[1] = ep[1] = y;
                sp[2] = ep[2] = sliceIdx;
                isBreak = true;
            }
        }
        if( isBreak )
        {
            right[0] = (sp[0] + ep[0]) / 2;
            right[1] = (sp[1] + ep[1]) / 2;
            right[2] = sliceIdx;
            break;
        }
    }
}

template <typename T>
void mxImageUtils::FindLinePoint(const mxImage<T>& image, const vect3<int>& line, vect3<unsigned int>& pt1, vect3<unsigned int>& pt2)
{
	vect3<unsigned int> imageSize = image.GetImageSize();

	if(line[0] >= 0 && line[1] >= 0 && line[2] < 0)
	{
		if(line[0] >= static_cast<int>(imageSize[0]) || line[1] >= static_cast<int>(imageSize[1]))
		{
			std::cerr << "line out of boundary" << std::endl;
			exit(-1);
		}

		pt1[0] = line[0];	pt1[1] = line[1];
		for (unsigned int z = 0; z < imageSize[2]; ++z)
		{
			if(image(line[0], line[1], z) > 0)
			{
				pt1[2] = z;
				break;
			}
		}

		pt2[0] = line[0];	pt2[1] = line[1];
		for (unsigned int z = imageSize[2]; z >= 0 && z < imageSize[2]; --z)
		{
			if(image(line[0], line[1], z) > 0)
			{
				pt2[2] = z;
				break;
			}
		}

		return;
	}

	if(line[0] >= 0 && line[1] < 0 && line[2] >= 0)
	{
		if(line[0] >= static_cast<int>(imageSize[0]) || line[2] >= static_cast<int>(imageSize[2]))
		{
			std::cerr << "line out of boundary" << std::endl;
			exit(-1);
		}

		pt1[0] = line[0];	pt1[2] = line[2];
		for (unsigned int y = 0; y < imageSize[1]; ++y)
		{
			if(image(line[0], y, line[2]) > 0)
			{
				pt1[1] = y;
				break;
			}
		}

		pt2[0] = line[0];	pt2[2] = line[2];
		for (unsigned int y = imageSize[1]-1; y >= 0 && y < imageSize[1]; --y)
		{
			if(image(line[0], y, line[2]) > 0)
			{
				pt2[1] = y;
				break;
			}
		}

		return;
	}

	if(line[0] < 0 && line[1] >= 0 && line[2] >= 0)
	{
		if(line[1] >= static_cast<int>(imageSize[1]) || line[2] >= static_cast<int>(imageSize[2]))
		{
			std::cerr << "line out of boundary" << std::endl;
			exit(-1);
		}

		pt1[1] = line[1];	pt1[2] = line[2];
		for (unsigned int x = 0; x < imageSize[0]; ++x)
		{
			if(image(x, line[1], line[2]) > 0)
			{
				pt1[0] = x;
				break;
			}
		}

		pt2[1] = line[1];	pt2[2] = line[2];
		for (unsigned int x = imageSize[0]-1; x >= 0 && x < imageSize[0]; --x)
		{
			if(image(x, line[1], line[2]) > 0)
			{
				pt2[0] = x;
				break;
			}
		}

		return;
	}

	std::cerr << "incorrect line specification" << std::endl;
	exit(-1);
}
    
template <typename T>
void mxImageUtils::Grids2Graph(const mxImage<T>& binaryImage, std::vector< vect3<unsigned int> >& pts, std::vector< std::pair<int,int> >& edges, std::map<int,int>& pt2id)
{
	vect3<unsigned int> imageSize = binaryImage.GetImageSize();

	std::map<int, int> idxmap;

	// add points, build pt2id, id2pt
	for(unsigned int z = 0; z < imageSize[2]; ++z)
	{
		for(unsigned int y = 0; y < imageSize[1]; ++y)
		{
			for(unsigned int x = 0; x < imageSize[0]; ++x)
			{
				vect3<unsigned int> voxel(x,y,z);
				unsigned int index = z*imageSize[0]*imageSize[1] + y*imageSize[0] + x; 

				if(binaryImage(voxel))
				{
					int id = static_cast<int>(pts.size());
					pts.push_back(voxel);
					idxmap[index] = id;
					pt2id[index] = id;
				}
			}
		}
	}

	// add edges
	for(unsigned int i = 0; i < pts.size(); ++i) 
	{
		unsigned int idx1 = pts[i][2] * imageSize[0] * imageSize[1] + pts[i][1] * imageSize[0] + pts[i][0];
		assert( idxmap.count(idx1) );

		for(int dz = -1; dz <= 1; ++dz)
		{
			for(int dy = -1; dy <= 1; ++dy)
			{
				for(int dx = -1; dx <= 1; ++dx) 
				{
					if( std::abs(dx) + std::abs(dy) + std::abs(dz) == 0 )
						continue;

					vect3<int> nbVoxel;
					nbVoxel[0] = static_cast<int>(pts[i][0]) + dx;
					nbVoxel[1] = static_cast<int>(pts[i][1]) + dy;
					nbVoxel[2] = static_cast<int>(pts[i][2]) + dz;

					if(nbVoxel[0] < 0 || nbVoxel[1] < 0 || nbVoxel[2] < 0 || nbVoxel[0] >= static_cast<int>(imageSize[0]) || 
						nbVoxel[1] >= static_cast<int>(imageSize[1]) || nbVoxel[2] >= static_cast<int>(imageSize[2]) )
						continue;

					unsigned int idx2 = nbVoxel[2] * imageSize[0] * imageSize[1] + nbVoxel[1] * imageSize[0] + nbVoxel[0];
					if( idxmap.count(idx2) )
						edges.push_back( std::make_pair(idxmap[idx1], idxmap[idx2]) );
				}
			}
		}
		idxmap.erase(idx1);
	}
}

template <typename T>
void mxImageUtils::ExtractBoundaryPoints(const mxImage<T>& segImage, T threshold, std::vector< vect3<double> >& voxels) {

	const vect3<unsigned int>& imageSize = segImage.GetImageSize();

	for(int z = 0; z < static_cast<int>(imageSize[2]); ++z) {
		for(int y = 0; y < static_cast<int>(imageSize[1]); ++y) {
			for(int x = 0; x < static_cast<int>(imageSize[0]); ++x) {

				if( segImage(x,y,z) < threshold )
					continue;

				// on image boundary
				if( x == 0 || x == static_cast<int>(imageSize[0])-1 ||
					y == 0 || y == static_cast<int>(imageSize[1])-1 ||
					z == 0 || z == static_cast<int>(imageSize[2])-1 ) {

					voxels.push_back( vect3<double>(x,y,z) );
				}

				// neighborhood has background voxels
				bool isNeighborBackground = false;
				for (int nz = z-1; nz <= z+1; ++nz) {
					for(int ny = y-1; ny <= y+1; ++ny) {
						for(int nx = x-1; nx <= x+1; ++nx) {
							if( segImage(nx,ny,nz) < threshold ) {
								isNeighborBackground = true;
								break;
							}
						}
						if ( isNeighborBackground ) break;
					}
					if ( isNeighborBackground ) break;
				}

				if( isNeighborBackground )
				{
					voxels.push_back( vect3<double>(x,y,z) );
				}

			} // end x
		} // end y
	} // end z
}

template <typename T>
void mxImageUtils::ExtractBoundaryPoints_8(const mxImage<T>& segImage, T threshold, std::vector< vect3<double> >& voxels) {

	const vect3<unsigned int>& imageSize = segImage.GetImageSize();

	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z) {
		for (int y = 0; y < static_cast<int>(imageSize[1]); ++y) {
			for (int x = 0; x < static_cast<int>(imageSize[0]); ++x) {

				if (segImage(x, y, z) < threshold)
					continue;

				// on image boundary
				if (x == 0 || x == static_cast<int>(imageSize[0]) - 1 ||
					y == 0 || y == static_cast<int>(imageSize[1]) - 1 ||
					z == 0 || z == static_cast<int>(imageSize[2]) - 1) {

					voxels.push_back(vect3<double>(x, y, z));
				}

				// neighborhood has background voxels
				bool isNeighborBackground = false;
				for (int nz = z - 1; nz <= z + 1; ++nz) {
					for (int ny = y - 1; ny <= y + 1; ++ny) {
						for (int nx = x - 1; nx <= x + 1; ++nx) {

							if (std::abs(nx-x) + std::abs(ny-y) + std::abs(nz-z) != 1)
								continue;

							if (segImage(nx, ny, nz) < threshold) {
								isNeighborBackground = true;
								break;
							}
						}
						if (isNeighborBackground) break;
					}
					if (isNeighborBackground) break;
				}

				if (isNeighborBackground)
				{
					voxels.push_back(vect3<double>(x, y, z));
				}

			} // end x
		} // end y
	} // end z

}

template <typename T>
void mxImageUtils::ExtractBoundaryPoints2(const mxImage<T>& segImage, T threshold, std::vector< vect3<double> >& voxels)
{
	const vect3<unsigned int>& imageSize = segImage.GetImageSize();

	for(int z = 0; z < static_cast<int>(imageSize[2]); ++z) {
		for(int y = 0; y < static_cast<int>(imageSize[1]); ++y) {
			for(int x = 0; x < static_cast<int>(imageSize[0]); ++x) {

				if( segImage(x,y,z) >= threshold )
					continue;

				// neighborhood has background voxels
				bool isNeighborObject = false;
				for (int nz = z-1; nz <= z+1; ++nz) {
					for(int ny = y-1; ny <= y+1; ++ny) {
						for(int nx = x-1; nx <= x+1; ++nx) {
							if (!segImage.PtInImage(nx,ny,nz))
								continue;

							if( segImage(nx,ny,nz) >= threshold ) {
								isNeighborObject = true;
								break;
							}
						}
						if ( isNeighborObject ) break;
					}
					if ( isNeighborObject ) break;
				}

				if( isNeighborObject )
				{
					voxels.push_back( vect3<double>(x,y,z) );
				}

			} // end x
		} // end y
	} // end z
}
    
template <typename T1, typename T2, typename T3, class TTransform>
void mxImageUtils::ApplyLinearTransformation(const mxImage<T1>& movImage, const mxImage<T2>& refImage, const TTransform& T, mxImage<T3>& outImage, IImageInterpolator<T3,T3>& interpolator, bool useWorld)
{
	outImage.CopyImageInfo(refImage);
	outImage.SetImageSize(refImage.GetImageSize());
	outImage.Fill(0);

	vect3<unsigned int> imageSize = outImage.GetImageSize();
	vect3<unsigned int> moveImageSize = movImage.GetImageSize();

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z)
	{
		for (unsigned int y = 0; y < imageSize[1]; ++y)
		{
			for (unsigned int x = 0; x < imageSize[0]; ++x)
			{
				vect3<double> voxel;
				if (useWorld)
				{
					vect3<double> pt1(x,y,z), pt2;
					mxImageUtils::Voxel2World(outImage,pt1);
					T.Apply(pt1, pt2);
					mxImageUtils::World2Voxel(movImage,pt2,voxel);
				}else {
					vect3<double> pt1(x,y,z);
					T.Apply(pt1,voxel);
				}
				outImage(x,y,z) = interpolator.Get(movImage, voxel);
			}
		}
	}
}

template <typename T>
vect3<double> mxImageUtils::Gradient(const mxImage<T>& image, int x, int y, int z)
{
	if (!image.PtInImage(x,y,z)) {
		std::cerr << "(x,y,z) outside image region" << std::endl; exit(-1);
	}

	vect3<double> grad;
	const vect3<unsigned int> imageSize = image.GetImageSize();

	if (x != 0 && x != static_cast<int>(imageSize[0])-1) 
		grad[0] = ( static_cast<double>(image(x+1,y,z)) - static_cast<double>(image(x-1,y,z)) ) / 2;
	else if (x == 0) 
		grad[0] = static_cast<double>(image(x+1,y,z)) - static_cast<double>(image(x,y,z));
	else if (x == static_cast<int>(imageSize[0])-1)
		grad[0] = static_cast<double>(image(x,y,z)) - static_cast<double>(image(x-1,y,z));
	else {
		std::cerr << "unexpected error in gradient computation" << std::endl;
		exit(-1);
	}

	if (y != 0 && y != static_cast<int>(imageSize[1])-1)
		grad[1] = ( static_cast<double>(image(x,y+1,z)) - static_cast<double>(image(x,y-1,z)) ) / 2;
	else if (y == 0)
		grad[1] = static_cast<double>(image(x,y+1,z)) - static_cast<double>(image(x,y,z));
	else if (y == static_cast<int>(imageSize[1])-1) 
		grad[1] = static_cast<double>(image(x,y,z)) - static_cast<double>(image(x,y-1,z));
	else {
		std::cerr << "unexpected error in gradient computation" << std::endl;
		exit(-1);
	}

	if (z != 0 && z != static_cast<int>(imageSize[2])-1)
		grad[2] = ( static_cast<double>(image(x,y,z+1)) - static_cast<double>(image(x,y,z-1)) ) / 2;
	else if (z == 0)
		grad[2] = static_cast<double>(image(x,y,z+1)) - static_cast<double>(image(x,y,z));
	else if (z == static_cast<int>(imageSize[2])-1)
		grad[2] = static_cast<double>(image(x,y,z)) - static_cast<double>(image(x,y,z-1));
	else {
		std::cerr << "unexpected error in gradient computation" << std::endl;
		exit(-1);
	}

	return grad;
}


template <typename T>
void mxImageUtils::GradientMap(const mxImage<T>& image, mxImage<double>& gradMap)
{
	vect3<unsigned int> imageSize = image.GetImageSize();
	gradMap.CopyImageInfo(image);
	gradMap.SetImageSize(image.GetImageSize());

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(imageSize[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
			{
				double gradx = 0, grady = 0, gradz = 0;

				if (x != 0 && x != static_cast<int>(imageSize[0]) - 1)
					gradx = (static_cast<double>(image(x + 1, y, z)) - static_cast<double>(image(x - 1, y, z))) / 2;
				else if (x == 0)
					gradx = static_cast<double>(image(x + 1, y, z)) - static_cast<double>(image(x, y, z));
				else if (x == static_cast<int>(imageSize[0]) - 1)
					gradx = static_cast<double>(image(x, y, z)) - static_cast<double>(image(x - 1, y, z));
				else {
					std::cerr << "unexpected error in gradient computation" << std::endl;
					exit(-1);
				}

				if (y != 0 && y != static_cast<int>(imageSize[1]) - 1)
					grady = (static_cast<double>(image(x, y + 1, z)) - static_cast<double>(image(x, y - 1, z))) / 2;
				else if (y == 0)
					grady = static_cast<double>(image(x, y + 1, z)) - static_cast<double>(image(x, y, z));
				else if (y == static_cast<int>(imageSize[1]) - 1)
					grady = static_cast<double>(image(x, y, z)) - static_cast<double>(image(x, y - 1, z));
				else {
					std::cerr << "unexpected error in gradient computation" << std::endl;
					exit(-1);
				}

				if (z != 0 && z != static_cast<int>(imageSize[2]) - 1)
					gradz = (static_cast<double>(image(x, y, z + 1)) - static_cast<double>(image(x, y, z - 1))) / 2;
				else if (z == 0)
					gradz = static_cast<double>(image(x, y, z + 1)) - static_cast<double>(image(x, y, z));
				else if (z == static_cast<int>(imageSize[2]) - 1)
					gradz = static_cast<double>(image(x, y, z)) - static_cast<double>(image(x, y, z - 1));
				else {
					std::cerr << "unexpected error in gradient computation" << std::endl;
					exit(-1);
				}

				double mag = sqrt(gradx*gradx + grady*grady + gradz*gradz);
				gradMap(x, y, z) = mag;
			}
		}
	}
}

template <typename T>
void mxImageUtils::GradientMaps(const mxImage<T>& image, mxImage<double>& xgradmap, mxImage<double>& ygradmap, mxImage<double>& zgradmap)
{
	// use mirror boundary condition
	vect3<unsigned int> imageSize = image.GetImageSize();
	xgradmap.CopyImageInfo(image);
	xgradmap.SetImageSize(imageSize);

	ygradmap.CopyImageInfo(image);
	ygradmap.SetImageSize(imageSize);

	zgradmap.CopyImageInfo(image);
	zgradmap.SetImageSize(imageSize);

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(imageSize[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
			{
				if (x != 0 && x != static_cast<int>(imageSize[0]) - 1)
					xgradmap(x,y,z) = (static_cast<double>(image(x + 1, y, z)) - static_cast<double>(image(x - 1, y, z))) / 2;
				else if (x == 0)
					xgradmap(x,y,z) = static_cast<double>(image(x + 1, y, z)) - static_cast<double>(image(x, y, z));
				else if (x == static_cast<int>(imageSize[0]) - 1)
					xgradmap(x,y,z) = static_cast<double>(image(x, y, z)) - static_cast<double>(image(x - 1, y, z));
				else {
					std::cerr << "unexpected error in gradient computation" << std::endl;
					exit(-1);
				}

				if (y != 0 && y != static_cast<int>(imageSize[1]) - 1)
					ygradmap(x,y,z) = (static_cast<double>(image(x, y + 1, z)) - static_cast<double>(image(x, y - 1, z))) / 2;
				else if (y == 0)
					ygradmap(x,y,z) = static_cast<double>(image(x, y + 1, z)) - static_cast<double>(image(x, y, z));
				else if (y == static_cast<int>(imageSize[1]) - 1)
					ygradmap(x,y,z) = static_cast<double>(image(x, y, z)) - static_cast<double>(image(x, y - 1, z));
				else {
					std::cerr << "unexpected error in gradient computation" << std::endl;
					exit(-1);
				}

				if (z != 0 && z != static_cast<int>(imageSize[2]) - 1)
					zgradmap(x,y,z) = (static_cast<double>(image(x, y, z + 1)) - static_cast<double>(image(x, y, z - 1))) / 2;
				else if (z == 0)
					zgradmap(x,y,z) = static_cast<double>(image(x, y, z + 1)) - static_cast<double>(image(x, y, z));
				else if (z == static_cast<int>(imageSize[2]) - 1)
					zgradmap(x,y,z) = static_cast<double>(image(x, y, z)) - static_cast<double>(image(x, y, z - 1));
				else {
					std::cerr << "unexpected error in gradient computation" << std::endl;
					exit(-1);
				}
			}
		}
	}
}

template <typename T>
void mxImageUtils::LaplacianMap(const mxImage<T>& image, mxImage<double>& lmap)
{
	// use mirror boundary condition
	vect3<unsigned int> imageSize = image.GetImageSize();
	lmap.CopyImageInfo(image);
	lmap.SetImageSize(imageSize);

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(imageSize[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(imageSize[0]); ++x)
			{
				double secx = 0, secy = 0, secz = 0;

				if (x != 0 && x != static_cast<int>(imageSize[0]) - 1)
					secx = static_cast<double>(image(x + 1, y, z)) + static_cast<double>(image(x - 1, y, z)) - 2 * static_cast<double>(image(x,y,z));
				else if (x == 0)
					secx = 2 * static_cast<double>(image(x + 1, y, z)) - 2 * static_cast<double>(image(x, y, z));
				else if (x == static_cast<int>(imageSize[0]) - 1)
					secx = 2 * static_cast<double>(image(x - 1, y, z)) - 2 * static_cast<double>(image(x, y, z));
				else {
					std::cerr << "unexpected error in Laplacian computation" << std::endl;
					exit(-1);
				}

				if (y != 0 && y != static_cast<int>(imageSize[1]) - 1)
					secy = static_cast<double>(image(x, y + 1, z)) + static_cast<double>(image(x, y - 1, z)) - 2 * static_cast<double>(image(x, y, z));
				else if (y == 0)
					secy = 2 * static_cast<double>(image(x, y + 1, z)) - 2 * static_cast<double>(image(x, y, z));
				else if (y == static_cast<int>(imageSize[1]) - 1)
					secy = 2 * static_cast<double>(image(x, y - 1, z)) - 2 * static_cast<double>(image(x, y, z));
				else {
					std::cerr << "unexpected error in Laplacian computation" << std::endl;
					exit(-1);
				}

				if (z != 0 && z != static_cast<int>(imageSize[2]) - 1)
					secz = static_cast<double>(image(x, y, z + 1)) + static_cast<double>(image(x, y, z - 1)) - 2 * static_cast<double>(image(x, y, z));
				else if (z == 0)
					secz = 2 * static_cast<double>(image(x, y, z + 1)) - 2 * static_cast<double>(image(x, y, z));
				else if (z == static_cast<int>(imageSize[2]) - 1)
					secz = 2 * static_cast<double>(image(x, y, z - 1)) - 2 * static_cast<double>(image(x, y, z));
				else {
					std::cerr << "unexpected error in Laplacian computation" << std::endl;
					exit(-1);
				}

				lmap(x, y, z) = secx + secy + secz;
			}
		}
	}
}

template <typename T>
void mxImageUtils::NormalizeWithUnitNorm(mxImage<T>& image)
{
	vect3<unsigned int> imageSize = image.GetImageSize();
	unsigned int len = imageSize[0] * imageSize[1] * imageSize[2];
	T* data = image.GetData();

	double norm = 0;
	for (unsigned int i = 0; i < len; ++i)
		norm += data[i]*data[i];
	if (norm < VECT_EPSILON)
		return;

	norm = sqrt(norm);
	for (unsigned int i = 0; i < len; ++i)
		data[i] = static_cast<T>(data[i] / norm);
}

/** @brief normalize an image by scaling to [0,1] with the maximum value */
template <typename T>
void mxImageUtils::NormalizeWithMax(mxImage<T>& image)
{
	vect3<unsigned int> imageSize = image.GetImageSize();
	unsigned int len = imageSize[0] * imageSize[1] * imageSize[2];
	T* data = image.GetData();

	double maxVal = -std::numeric_limits<double>::max();
	for (unsigned int i = 0; i < len; ++i) {
		if (data[i] > maxVal)
			maxVal = data[i];
	}
	if (maxVal < VECT_EPSILON)
		return;

	for (unsigned int i = 0; i < len; ++i)
		data[i] = static_cast<T>(data[i] / maxVal);
}

/** @brief normalize an image by scaling to [0,1] with minimum and maximum */
template <typename T>
void mxImageUtils::NormalizeWithMinMax(mxImage<T>& image)
{
	vect3<unsigned int> imageSize = image.GetImageSize();
	unsigned int len = imageSize[0] * imageSize[1] * imageSize[2];
	T* data = image.GetData();

	double maxVal = -std::numeric_limits<double>::max();
	double minVal = std::numeric_limits<double>::max();

	for (unsigned int i = 0; i < len; ++i) {
		if (data[i] > maxVal)
			maxVal = data[i];
		if (data[i] < minVal)
			minVal = data[i];
	}
	if (std::abs(maxVal-minVal) < VECT_EPSILON)
		return;

	for (unsigned int i = 0; i < len; ++i)
		data[i] = static_cast<T>( (data[i]-minVal) / (maxVal-minVal) );
}

/** @brief normalize an image by 0 mean and 1 standard deviation */
template <typename T>
void mxImageUtils::NormalizeWithZeroMeanOneStd(mxImage<T>& image)
{
	vect3<unsigned int> imageSize = image.GetImageSize();
	unsigned int len = imageSize[0] * imageSize[1] * imageSize[2];
	T* data = image.GetData();

	double mean = 0;
	for (unsigned int i = 0; i < len; ++i) 
		mean += data[i];
	mean /= len;

	double stdev = 0;
	for (unsigned int i = 0; i < len; ++i)
		stdev += (data[i]-mean)*(data[i]-mean);
	stdev = sqrt(stdev / len);
	if (stdev < VECT_EPSILON)
		return;

	for (unsigned int i = 0; i < len; ++i)
		data[i] = static_cast<T>( (data[i] - mean) / stdev );
}

template <typename T>
void mxImageUtils::Normalize(mxImage<T>& image, NormalizationType type)
{
	switch (type)
	{
	case UNIT_NORM:				mxImageUtils::NormalizeWithUnitNorm(image); break;
	case MAX_ONE:				mxImageUtils::NormalizeWithMax(image); break;
	case MIN_MAX:				mxImageUtils::NormalizeWithMinMax(image); break;
	case ZERO_MEAN_ONE_STD:		mxImageUtils::NormalizeWithZeroMeanOneStd(image); break;
	default:					break;
	}
}

NormalizationType mxImageUtils::GetNormalizationByString(std::string str)
{
	if (str == "NONE")
		return NONE;
	else if (str == "UNIT_NORM")
		return UNIT_NORM;
	else if (str == "MAX_ONE")
		return MAX_ONE;
	else if (str == "MIN_MAX")
		return MIN_MAX;
	else if (str == "ZERO_MEAN_ONE_STD")
		return ZERO_MEAN_ONE_STD;
	else {
		std::cerr << "unrecognized normalization type" << std::endl;
		exit(-1);
	}
}

std::string mxImageUtils::GetNormalizationString(NormalizationType type)
{
	switch (type)
	{
	case NONE: return std::string("NONE"); break;
	case UNIT_NORM: return std::string("UNIT_NORM"); break;
	case MAX_ONE: return std::string("MAX_ONE"); break;
	case MIN_MAX: return std::string("MIN_MAX"); break;
	case ZERO_MEAN_ONE_STD: return std::string("ZERO_MEAN_ONE_STD"); break;
	default:
		std::cerr << "unrecognized normalization type" << std::endl;
		exit(-1);
	}
}


template <typename T, int Dimension>
void mxImageUtils::Vector2Norm(const mxImage< boost::array<T,Dimension> >& vectorImage, mxImage<double>& normImage)
{
	normImage.SetImageSize(vectorImage.GetImageSize());
	normImage.CopyImageInfo(vectorImage);

	vect3<unsigned int> imageSize = normImage.GetImageSize();
	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z) {
		for(int y = 0; y < static_cast<int>(imageSize[1]); ++y) {
			for(int x = 0; x < static_cast<int>(imageSize[0]); ++x) {
				const boost::array<T,Dimension>& val = vectorImage(x,y,z);
				normImage(x,y,z) = 0;
				for (int k = 0; k < Dimension; ++k) 
					normImage(x,y,z) += val[k] * val[k];
				normImage(x,y,z) = sqrt( static_cast<double>(normImage(x,y,z)) );
			}
		}
	}
}


template <typename T>
void mxImageUtils::Vector2Norm(const std::vector< mxImage<T> >& vector_image, mxImage<double>& norm_image)
{
	assert_message(vector_image.size() > 0, "empty vector image");

	norm_image.SetImageSize(vector_image[0].GetImageSize());
	norm_image.CopyImageInfo(vector_image[0]);

	vect3<unsigned int> image_size = vector_image[0].GetImageSize();

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
			{
				double norm = 0;
				for (int i = 0; i < vector_image.size(); ++i)
					norm += (vector_image[i](x, y, z) * vector_image[i](x, y, z));
				norm = sqrt(norm);

				norm_image(x, y, z) = norm;
			}
		}
	}
}


template <typename T1, typename T2, int Dimension>
void mxImageUtils::ExtractFromVectorImage(const mxImage< boost::array<T1, Dimension> >& vectorImage, int dim, mxImage<T2>& compImage)
{
	if (dim >= Dimension) {
		std::cerr << "dimension index exceeds" << std::endl; exit(-1);
	}

	vect3<unsigned int> image_size = vectorImage.GetImageSize();

	compImage.CopyImageInfo(vectorImage);
	compImage.SetImageSize(image_size);

	for (unsigned int z = 0; z < image_size[2]; ++z) {
		for (unsigned int y = 0; y < image_size[1]; ++y) {
			for (unsigned int x = 0; x < image_size[0]; ++x) {
				compImage(x, y, z) = static_cast<T2>(vectorImage(x, y, z)[dim]);
			}
		}
	}
}

template <typename T1, typename T2>
void mxImageUtils::ConvertToBinaryImage(const mxImage<T1>& input, mxImage<T2>& output)
{
	output.CopyImageInfo(input);
	output.SetImageSize(input.GetImageSize());

	vect3<unsigned int> imageSize = input.GetImageSize();
	for (unsigned int z = 0; z < imageSize[2]; ++z) {
		for (unsigned int y = 0; y < imageSize[1]; ++y) {
			for (unsigned int x = 0; x < imageSize[0]; ++x) {
				if (input(x,y,z) == 0)
					output(x,y,z) = 0;
				else
					output(x,y,z) = 1;
			}
		}
	}
}

void mxImageUtils::PadBox(vect3<int>& sp, vect3<int>& ep, const vect3<unsigned int>& patchSize)
{
	for (unsigned int i = 0; i < 3; ++i)
	{
		sp[i] = sp[i] - patchSize[i]/2;
		ep[i] = ep[i] - patchSize[i]/2 + patchSize[i] - 1;
	}
}

template <typename T1, typename T2>
void mxImageUtils::CountCumulativeImage(const mxImage<T1>& refImage, const std::vector< vect3<unsigned int> >& voxels, mxImage<T2>& countImage)
{
	countImage.CopyImageInfo(refImage);
	countImage.SetImageSize(refImage.GetImageSize());
	countImage.Fill(0);

	for (unsigned int i = 0; i < voxels.size(); ++i)
	{
		if (countImage.PtInImage(voxels[i]))
			countImage(voxels[i]) += 1;
		else {
			std::cerr << "voxel to count is outside the image domain" << std::endl;
			exit(-1);
		}
	}
}

template <typename T>
void mxImageUtils::SubtractImage(const mxImage<T>& image1, const mxImage<T>& image2, mxImage<T>& image3)
{
	vect3<unsigned int> size1 = image1.GetImageSize();
	vect3<unsigned int> size2 = image2.GetImageSize();

	if (size1 != size2) {
		std::cerr << "image size must be the same for image subtraction" << std::endl; exit(-1);
	}

	image3.CopyImageInfo(image1);
	image3.SetImageSize(size1);

	for (unsigned int z = 0; z < size1[2]; ++z) {
		for (unsigned int y = 0; y < size1[1]; ++y) {
			for (unsigned int x = 0; x < size1[0]; ++x) {
				image3(x, y, z) = image1(x, y, z) - image2(x, y, z);
			}
		}
	}
}


template <typename T1, typename T2>
void mxImageUtils::ExtractBodyFromCT(const mxImage<T1>& image, mxImage<unsigned char>& mask, const mxImage<T2>* ref_image)
{
	//boost::timer::auto_cpu_timer timer;

	vect3<double> spacing(8, 8, 8);
	mxImage<T1> resampled_image;
	mxImage<unsigned char> resampled_mask;

	// use NN for efficiency
	Resample(image, spacing, resampled_image, IT_NEAREST_NEIGHBOR_INTERPOLATION);

	int threshold = 800;
	unsigned char obj_val = 255, bak_val = 0;
	Threshold(resampled_image, resampled_mask, static_cast<T1>(threshold), obj_val, bak_val);
	BinaryFillHoles(resampled_mask);
	PickLargestComponent(resampled_mask);

	if (ref_image == NULL)
		Copy(resampled_mask, mask);
	else
		TransformMaskImage(*ref_image, resampled_mask, mask);
}


void mxImageUtils::AddDistanceImages(const std::vector< mxImage<double> >& displacementImages, std::vector< mxImage<double> >& distanceImages)
{
	assert_message(displacementImages.size() % 3 == 0, "displacement dimension should be divisible by 3");

	int num_distance_images = static_cast<int>(displacementImages.size() / 3);
	int num_old_images = static_cast<int>(distanceImages.size());

	// allocate buffer for distance images
	distanceImages.resize(num_old_images + num_distance_images);
	for (int i = num_old_images; i < num_old_images + num_distance_images; ++i)
	{
		int disp_sp_index = (i - num_old_images) * 3;
		distanceImages[i].CopyImageInfo(displacementImages[disp_sp_index]);
		distanceImages[i].SetImageSize(displacementImages[disp_sp_index].GetImageSize());
	}


	// compute distance images from displacement maps
	for (int i = num_old_images; i < num_old_images + num_distance_images; ++i)
	{
		int disp_sp_index = (i - num_old_images) * 3;

		vect3 < unsigned int > image_size = displacementImages[disp_sp_index].GetImageSize();
		for (unsigned int z = 0; z < image_size[2]; ++z)
		{
			for (unsigned int y = 0; y < image_size[1]; ++y)
			{
				for (unsigned int x = 0; x < image_size[0]; ++x)
				{
					double val_x = displacementImages[disp_sp_index](x, y, z);
					double val_y = displacementImages[disp_sp_index + 1](x, y, z);
					double val_z = displacementImages[disp_sp_index + 2](x, y, z);

					double norm = val_x * val_x + val_y * val_y + val_z * val_z;
					if (norm < 0)
						norm = 0;
					else
						norm = sqrt(norm);

					distanceImages[i](x, y, z) = norm;
				}
			}
		}
	}
}


template <typename T>
void mxImageUtils::RotateImage(const mxImage<T>& image, vect3<double>& voxel_center, const vect3<double>& degrees, mxImage<T>& outImage)
{
	vect3<unsigned int> image_size = image.GetImageSize();
	vect3<double> spacing = image.GetSpacing();

	if (voxel_center[0] < 0 || voxel_center[1] < 0 || voxel_center[2] < 0)
	{
		for (int i = 0; i < 3; ++i)
			voxel_center[i] = image_size[i] / 2;
	}

	vnl_matrix<double> rot, rot_x, rot_y, rot_z;
	rot_x.set_size(3, 3);
	rot_y.set_size(3, 3);
	rot_z.set_size(3, 3);

	double a = - degrees[0] / 180 * M_PI;
	double b = - degrees[1] / 180 * M_PI;
	double c = - degrees[2] / 180 * M_PI;

	mathUtils::GetRotationMatrix_X(a, rot_x);
	mathUtils::GetRotationMatrix_Y(b, rot_y);
	mathUtils::GetRotationMatrix_Z(c, rot_z);

	rot = rot_x * rot_y * rot_z;

	outImage.CopyImageInfo(image);
	outImage.SetImageSize(image_size);

	ImageLinearInterpolator<T,T> interp;

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		vnl_matrix<double> offset;
		offset.set_size(3, 1);

		for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
			{
				offset(0,0) = (x - voxel_center[0]) * spacing[0];
				offset(1,0) = (y - voxel_center[1]) * spacing[1];
				offset(2,0) = (z - voxel_center[2]) * spacing[2];

				offset = rot * offset;

				for (int i = 0; i < 3; ++i)
					offset(i, 0) = offset(i, 0) / spacing[i];

				vect3<double> voxel;
				for (int i = 0; i < 3; ++i)
					voxel[i] = voxel_center[i] + offset(i, 0);

				T val = interp.Get(image, voxel);
				outImage(x, y, z) = val;
			}
		}
	}
}


template <typename T>
void mxImageUtils::RotateImage(const mxImage<T>& image, const vnl_matrix<double>& inv_rot, bool nearest_or_linear, mxImage<T>& outImage)
{
	vect3<unsigned int> image_size = image.GetImageSize();
	vect3<double> spacing = image.GetSpacing();

	outImage.CopyImageInfo(image);
	outImage.SetImageSize(image_size);
	outImage.Fill(0);

	vect3<int> voxel_center(image_size[0] / 2, image_size[1] / 2, image_size[2] / 2);

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		vnl_matrix<double> offset;
		offset.set_size(3, 1);

		std::auto_ptr< IImageInterpolator<T, T> > interp;
		if (nearest_or_linear)
			interp.reset(new ImageNearestNeighborInterpolator<T,T>());
		else
			interp.reset(new ImageLinearInterpolator<T, T>());

		for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
			{
				offset(0, 0) = (x - voxel_center[0]) * spacing[0];
				offset(1, 0) = (y - voxel_center[1]) * spacing[1];
				offset(2, 0) = (z - voxel_center[2]) * spacing[2];

				offset = inv_rot * offset;

				for (int i = 0; i < 3; ++i)
					offset(i, 0) = offset(i, 0) / spacing[i];

				vect3<double> voxel;
				for (int i = 0; i < 3; ++i)
					voxel[i] = voxel_center[i] + offset(i, 0);

				T val = interp->Get(image, voxel);
				outImage(x, y, z) = val;
			}
		}
	}
}


template <typename T>
void mxImageUtils::ShrinkMaskImage(mxImage<T>& image, const vect3<int>& pad_size)
{
	vect3<int> sp, ep;
	BoundingBox(image, sp, ep);

	for (int i = 0; i < 3; ++i)
	{
		sp[i] -= pad_size[i];
		ep[i] += pad_size[i];
	}

	mxImage<T> cropped_image;
	Crop2<T,T>(image, sp, ep, cropped_image, 0);

	image.Clear();
	Copy(cropped_image, image);
}


template <typename T>
void mxImageUtils::SquareImage(mxImage<T>& image)
{
  //vect3<int> image_size = image.GetImageSize().to<int>();
  vect3<unsigned int> image_size = image.GetImageSize();

	#pragma omp parallel for
  for (int z = 0; z < static_cast<int>(image_size[2]); ++z) {
    for (int y = 0; y < static_cast<int>(image_size[1]); ++y) {
      for (int x = 0; x < static_cast<int>(image_size[0]); ++x) {
	T& val = image(x, y, z);
	val = val * val;
      }
    }
  }
}


template <typename T>
void mxImageUtils::TransformMaskImage(const mxImage<T>& ref_image, const mxImage<unsigned char>& src_image, mxImage<unsigned char>& transformed_image)
{
	transformed_image.CopyImageInfo(ref_image);
	transformed_image.SetImageSize(ref_image.GetImageSize());
	transformed_image.Fill(0);

	vect3<unsigned int> image_size = transformed_image.GetImageSize();

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
			{
				vect3<int> vcoord(x, y, z);
				vect3<double> world_coord;
				mxImageUtils::Voxel2World(transformed_image, vcoord, world_coord);
				mxImageUtils::World2Voxel(src_image, world_coord, vcoord);

				if (!src_image.PtInImage(vcoord[0], vcoord[1], vcoord[2]))
					continue;

				transformed_image(x, y, z) = src_image(vcoord[0], vcoord[1], vcoord[2]);
			}
		}
	}
}

template <typename T>
void mxImageUtils::GetTransversalSlice(const mxImage<T>& image, int sliceIdx, mxImage<T>& slice, bool empty)
{
	vect3<unsigned int> image_size = image.GetImageSize();
	assert_message(sliceIdx >= 0 && sliceIdx < image_size[2], "transversal slice index out of bound");

	vect3<double> origin(0, 0, sliceIdx);
	mxImageUtils::Voxel2World(image, origin);

	slice.CopyImageInfo(image);
	slice.SetOrigin(origin);

	if (empty)
	{
		slice.SetRawImageSize( vect3<unsigned int>(image_size[0], image_size[1], 1) );
	}
	else
	{
		slice.SetImageSize(vect3<unsigned int>(image_size[0], image_size[1], 1));

		#pragma omp parallel for
		for (int y = 0; y < static_cast<int>(image_size[1]); ++y) {
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x) {
				slice(x, y, 0) = image(x, y, sliceIdx);
			}
		}
	}
}

template <typename T>
void mxImageUtils::GetSagittalSlice(const mxImage<T>& image, int sliceIdx, mxImage<T>& slice, bool empty)
{
	vect3<unsigned int> image_size = image.GetImageSize();
	assert_message(sliceIdx >= 0 && sliceIdx < image_size[0], "sagittal slice index out of bound");

	vect3<double> origin(sliceIdx, 0, 0);
	mxImageUtils::Voxel2World(image, origin);

	slice.CopyImageInfo(image);
	slice.SetOrigin(origin);

	if (empty)
	{
		slice.SetRawImageSize( vect3<unsigned int>(1, image_size[1], image_size[2]) );
	}
	else
	{
		slice.SetImageSize(vect3<unsigned int>(1, image_size[1], image_size[2]));

		#pragma omp parallel for
		for (int z = 0; z < static_cast<int>(image_size[2]); ++z) {
			for (int y = 0; y < static_cast<int>(image_size[1]); ++y) {
				slice(0, y, z) = image(sliceIdx, y, z);
			}
		}
	}
}

template <typename T>
void mxImageUtils::GetCoronalSlice(const mxImage<T>& image, int sliceIdx, mxImage<T>& slice, bool empty)
{
	vect3<unsigned int> image_size = image.GetImageSize();
	assert_message(sliceIdx >= 0 && sliceIdx < image_size[1], "coronal slice index out of bound");

	vect3<double> origin(0, sliceIdx, 0);
	mxImageUtils::Voxel2World(image, origin);

	slice.CopyImageInfo(image);
	slice.SetOrigin(origin);

	if (empty)
	{
		slice.SetRawImageSize(vect3<unsigned int>(image_size[0], 1, image_size[2]));
	}
	else
	{
		slice.SetImageSize(vect3<unsigned int>(image_size[0], 1, image_size[2]));

		#pragma omp parallel for
		for (int z = 0; z < static_cast<int>(image_size[2]); ++z) {
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x) {
				slice(x, 0, z) = image(x, sliceIdx, z);
			}
		}
	}
}

template <typename T>
void mxImageUtils::Find2DContour(const mxImage<T>& image, mxImage<unsigned char>& contour)
{
	vect3<unsigned int> image_size = image.GetImageSize();

	if (image_size[2] == 1) // transversal slice
	{
		contour.CopyImageInfo(image);
		contour.SetImageSize(image_size);
		contour.Fill(0);

		#pragma omp parallel for 
		for (int y = 0; y < static_cast<int>(image_size[1]); ++y) 
		{
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x) 
			{	
				if (image(x, y, 0) == 0)
					continue;

				bool is_edge = false;
				for (int dy = -1; dy <= 1; ++dy) 
				{
					for (int dx = -1; dx <= 1; ++dx) 
					{
						int cx = x + dx, cy = y + dy;
						if (cx < 0 || cy < 0 || cx >= image_size[0] || cy >= image_size[1]) {
							is_edge = true;
							break;
						}

						if (image(cx, cy, 0) == 0) {
							is_edge = true;
							break;
						}
					}
					if (is_edge) break;
				}

				if (is_edge)
					contour(x, y, 0) = 255;
			}
		}
	}
	else if (image_size[0] == 1) // sagittal slice
	{
		contour.CopyImageInfo(image);
		contour.SetImageSize(image_size);
		contour.Fill(0);

		#pragma omp parallel for
		for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
		{
			for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
			{
				if (image(0, y, z) == 0)
					continue;

				bool is_edge = false;
				for (int dz = -1; dz <= 1; ++dz)
				{
					for (int dy = -1; dy <= 1; ++dy)
					{
						int cy = y + dy, cz = z + dy;
						if (cy < 0 || cy >= image_size[1] || cz < 0 || cz >= image_size[2]) {
							is_edge = true;
							break;
						}

						if (image(0, cy, cz) == 0) {
							is_edge = true;
							break;
						}
					}
					if (is_edge) break;
				}

				if (is_edge)
					contour(0, y, z) = 255;
			}
		}
	}
	else if (image_size[1] == 1) // coronal slice
	{
		contour.CopyImageInfo(image);
		contour.SetImageSize(image_size);
		contour.Fill(0);

		#pragma omp parallel for
		for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
		{
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
			{
				if (image(x, 0, z) == 0)
					continue;

				bool is_edge = false;
				for (int dz = -1; dz <= 1; ++dz)
				{
					for (int dx = -1; dx <= 1; ++dx)
					{
						int cx = x + dx, cz = z + dz;
						if (cx < 0 || cx >= image_size[0] || cz < 0 || cz >= image_size[2]) {
							is_edge = true;
							break;
						}

						if (image(cx, 0, cz) == 0) {
							is_edge = true;
							break;
						}
					}
					if (is_edge) break;
				}

				if (is_edge)
					contour(x, 0, z) = 255;
			}
		}
	}
	else {
		err_message("not a 2D slice");
	}
}


template <typename T>
void mxImageUtils::Find2DContours(const mxImage<T>& image, SliceType type, mxImage<unsigned char>& contours)
{
	vect3<unsigned int> image_size = image.GetImageSize();

	if (type == TRANSVERSAL) // transversal slice
	{
		contours.CopyImageInfo(image);
		contours.SetImageSize(image_size);
		contours.Fill(0);

		#pragma omp parallel for 
		for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
		{
			for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
			{
				for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
				{
					if (image(x, y, z) == 0)
						continue;

					bool is_edge = false;
					for (int dy = -1; dy <= 1; ++dy)
					{
						for (int dx = -1; dx <= 1; ++dx)
						{
							int cx = x + dx, cy = y + dy;
							if (cx < 0 || cy < 0 || cx >= image_size[0] || cy >= image_size[1]) {
								is_edge = true;
								break;
							}

							if (image(cx, cy, z) == 0) {
								is_edge = true;
								break;
							}
						}
						if (is_edge) break;
					}

					if (is_edge)
						contours(x, y, z) = 255;
				}
			}
		}
	}
	else if (type == SAGITTAL) // sagittal slice
	{
		contours.CopyImageInfo(image);
		contours.SetImageSize(image_size);
		contours.Fill(0);

		#pragma omp parallel for
		for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
		{
			for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
			{
				for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
				{
					if (image(x, y, z) == 0)
						continue;

					bool is_edge = false;
					for (int dz = -1; dz <= 1; ++dz)
					{
						for (int dy = -1; dy <= 1; ++dy)
						{
							int cy = y + dy, cz = z + dz;
							if (cy < 0 || cy >= image_size[1] || cz < 0 || cz >= image_size[2]) {
								is_edge = true;
								break;
							}

							if (image(x, cy, cz) == 0) {
								is_edge = true;
								break;
							}
						}
						if (is_edge) break;
					}

					if (is_edge)
						contours(x, y, z) = 255;
				}
			}
		}
	}
	else if (type == CORONAL) // coronal slice
	{
		contours.CopyImageInfo(image);
		contours.SetImageSize(image_size);
		contours.Fill(0);

		#pragma omp parallel for
		for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
		{
			for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
			{
				for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
				{
					if (image(x, y, z) == 0)
						continue;

					bool is_edge = false;
					for (int dz = -1; dz <= 1; ++dz)
					{
						for (int dx = -1; dx <= 1; ++dx)
						{
							int cx = x + dx, cz = z + dz;
							if (cx < 0 || cx >= image_size[0] || cz < 0 || cz >= image_size[2]) {
								is_edge = true;
								break;
							}

							if (image(cx, y, cz) == 0) {
								is_edge = true;
								break;
							}
						}
						if (is_edge) break;
					}

					if (is_edge)
						contours(x, y, z) = 255;
				}
			}
		}
	}
	else {
		err_message("not a 2D slice");
	}
}


template <typename TIn, typename TOut>
void mxImageUtils::MaskOr(const mxImage<TIn>& image1, const mxImage<TIn>& image2, mxImage<TOut>& out_image)
{
	vect3<unsigned int> image_size1 = image1.GetImageSize();
	vect3<unsigned int> image_size2 = image2.GetImageSize();

	assert_message(image_size1 == image_size2, "image sizes not same in MaskOr");

	out_image.CopyImageInfo(image1);
	out_image.SetImageSize(image_size1);

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(image_size1[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(image_size1[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(image_size1[0]); ++x)
			{
				if (image1(x, y, z) > 0 || image2(x, y, z) > 0)
					out_image(x, y, z) = static_cast<TOut>(255);
			}
		}
	}
}


void mxImageUtils::ImageMask(mxImage<unsigned char>& image, const vect3<int>& box_sp, const vect3<int>& box_ep, const mxImage<unsigned char>& mask)
{
	vect3<unsigned int> image_size = image.GetImageSize();

	#pragma omp parallel for schedule(dynamic)
	for (int z = box_sp[2]; z < box_ep[2]; ++z)
	{
		for (int y = box_sp[1]; y < box_ep[1]; ++y)
		{
			for (int x = box_sp[0]; x < box_ep[0]; ++x)
			{
				if (image(x, y, z) == 0)
					continue;

				vect3<double> world(x, y, z);
				mxImageUtils::Voxel2World(image, world);

				vect3<int> voxel;
				mxImageUtils::World2Voxel(mask, world, voxel);

				if (!mask.PtInImage(voxel[0], voxel[1], voxel[2]))
					continue;

				if (mask(voxel[0], voxel[1], voxel[2]) == 0)
					image(x, y, z) = 0;
			}
		}
	}
}

template <typename T1, typename T2, typename TInterp>
void mxImageUtils::InterpolateImage(const mxImage<T1>& src_image, mxImage<T2>& out_image)
{
	vect3<unsigned int> out_image_size = out_image.GetImageSize();
	vect3<unsigned int> src_image_size = src_image.GetImageSize();

	out_image.Allocate();

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(out_image_size[2]); ++z)
	{
		TInterp interp;

		for (int y = 0; y < static_cast<int>(out_image_size[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(out_image_size[0]); ++x)
			{
				vect3<double> pos(x, y, z);
				vect3<int> voxel;

				mxImageUtils::Voxel2World(out_image, pos);
				mxImageUtils::World2Voxel(src_image, pos, voxel);
				mxImageUtils::World2Voxel(src_image, pos);

				if (!src_image.PtInImage(voxel[0], voxel[1], voxel[2]))
					out_image(x, y, z) = 0;
				else
					out_image(x, y, z) = interp.Get(src_image, pos);
			}
		}
	}
}


template <typename T>
void mxImageUtils::FlipImage(mxImage<T>& image, int axis)
{
	assert_message(axis >= 0 && axis <= 2, "axis [0-2]");

	vect3<unsigned int> image_size = image.GetImageSize();

	mxImage<T> out_image;
	out_image.CopyImageInfo(image);
	out_image.SetImageSize(image_size);

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		for (int y = 0; y < static_cast<int>(image_size[1]); ++y)
		{
			for (int x = 0; x < static_cast<int>(image_size[0]); ++x)
			{
				if (axis == 0)
					out_image(image_size[0] - 1 - x, y, z) = image(x, y, z);
				else if (axis == 1)
					out_image(x, image_size[1] - 1 - y, z) = image(x, y, z);
				else if (axis == 2)
					out_image(x, y, image_size[2] - 1 - z) = image(x, y, z);
				else
					err_message("no such axis");
			}
		}
	}

	image.Clear();
	mxImageUtils::Copy(out_image, image);
}


template <typename T>
int mxImageUtils::CountVoxels(const mxImage<T>& image, T value)
{
	vect3<unsigned int> image_size = image.GetImageSize();

	int count = 0;

	#pragma omp parallel for reduction(+ : count)
	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		for (unsigned int y = 0; y < image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < image_size[0]; ++x)
			{
				if (image(x, y, z) == value)
					count += 1;
			}
		}
	}

	return count;
}


template <typename T>
void mxImageUtils::MRIntensityNormalization(const mxImage<T>& image, double ratio, mxImage<double>& outImage)
{
	vect3<unsigned int> image_size = image.GetImageSize();
	size_t total_num = image_size[0] * image_size[1] * image_size[2];
	const T* data = image.GetData();
	std::vector<T> data_copy(total_num);
	::memcpy(&data_copy[0], data, sizeof(T)*total_num);

	std::sort(data_copy.begin(), data_copy.end());

	T* p = std::lower_bound(&data_copy[0], &data_copy[0] + total_num, static_cast<T>(5));
	size_t left = p - &data_copy[0];
	size_t new_total = total_num - left;

	T val = p[static_cast<size_t>((new_total - 1) * ratio)];

	outImage.CopyImageInfo(image);
	outImage.SetImageSize(image_size);
	
	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		for (unsigned int y = 0; y < image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < image_size[0]; ++x)
			{
				//if (image(x, y, z) >= val)
				//	outImage(x, y, z) = 1;
				//else
				
				outImage(x, y, z) = image(x, y, z) / static_cast<double>(val);
			}
		}
	}
}


void mxImageUtils::Prob2Disp(const mxImage<double>& prob_map, double threshold, mxImage<double>* disp_maps, const mxImage<unsigned char>* mask)
{
	std::vector< vect3<double> > bk_boundary_voxels;
	ExtractBoundaryPoints2<double>(prob_map, threshold, bk_boundary_voxels);

	// convert coordinate system to the current one
	for (size_t j = 0; j < bk_boundary_voxels.size(); ++j)
		mxImageUtils::Voxel2World(prob_map, bk_boundary_voxels[j]);

	NearestPointIndexer<double> nn;
	nn.Initialize(bk_boundary_voxels);

	vect3<unsigned int> image_size = disp_maps[0].GetImageSize();

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(image_size[2]); ++z)
	{
		for (unsigned int y = 0; y < image_size[1]; ++y)
		{
			for (unsigned int x = 0; x < image_size[0]; ++x)
			{
				if (mask) {
					vect3<double> pt(x, y, z);   vect3<int> voxel;
					mxImageUtils::Voxel2World(disp_maps[0], pt);
					mxImageUtils::World2Voxel(*mask, pt, voxel);
					if (!mask->PtInImage(voxel[0], voxel[1], voxel[2]) || (*mask)(voxel[0], voxel[1], voxel[2]) == 0)
						continue;
				}

				vect3<double> pt(x, y, z);
				Voxel2World(disp_maps[0], pt);
				vect3<double> pt2 = nn.SearchNearestNeighbor(pt);
				vect3<double> disp = pt2 - pt;

				disp_maps[0](x, y, z) = disp[0];
				disp_maps[1](x, y, z) = disp[1];
				disp_maps[2](x, y, z) = disp[2];
			}
		}
	}
}


} } } // end namespace


#endif

