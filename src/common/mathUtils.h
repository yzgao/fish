//
//  mathUtils.h
//  Fish
//
//  Created by Yaozong Gao on 6/17/13.
//
//


#ifndef __mathUtils_h__
#define __mathUtils_h__

#include "stdafx.h"
#include <numeric>
#include "vnl/vnl_matrix.h"
#include "vnl/vnl_vector.h"
#include "vnl/algo/vnl_svd.h"
#include "common/vect.h"
#include "common/aux_func.h"
#include "vnl/algo/vnl_matrix_inverse.h"


namespace BRIC { namespace IDEA { namespace FISH {

class mathUtils
{
public:
    
	template <typename T>
	static double Determinant(const boost::numeric::ublas::matrix<T>& mat);
    
    template <typename T>
    static void PCA(vnl_matrix<T>& mat, vnl_matrix<T>& comps, std::vector<T>& vars, bool subtractMean = true);

	template <typename T>
	static void GetRotationMatrix_X(double radians, vnl_matrix<T>& rot);

	template <typename T>
	static void GetRotationMatrix_Y(double radians, vnl_matrix<T>& rot);

	template <typename T>
	static void GetRotationMatrix_Z(double radians, vnl_matrix<T>& rot);

	template <typename T>
	static double Correlation(T* a, T* b, int num);

	static void EvenDistribute(int total, std::vector<int>& buckets);

	// ax + by + cz - 1 = 0;
	static vect3<double> PlaneFit(const std::vector< vect3<double> >& pts);

	// estimate a rotation matrix from vector v1 to v2
	static vnl_matrix<double> RotationMatrixBetweenVectors(const vect3<double>& v1, const vect3<double>& v2);

};


template <typename T>
double mathUtils::Determinant(const boost::numeric::ublas::matrix<T>& mat)
{
	double det = 1.0;

	typedef boost::numeric::ublas::matrix<T> MatrixT;

	MatrixT matLU( mat );
	boost::numeric::ublas::permutation_matrix<std::size_t> pivots( matLU.size1() );

	int is_singular = boost::numeric::ublas::lu_factorize( matLU, pivots );

	if (!is_singular)
	{
		for (std::size_t i = 0; i < pivots.size(); ++i)
		{
			if (pivots(i) != i)
				det *= -1.0;

			det *= matLU(i,i);
		}
	}
	else
		det = 0.0;

	return det;
} 

    
template <typename T>
void mathUtils::PCA(vnl_matrix<T>& mat, vnl_matrix<T>& comps, std::vector<T>& vars, bool subtractMean)
{
    unsigned int numSamples = mat.columns();
    unsigned int numFeatures = mat.rows();
    
    if(subtractMean)
    {
        vnl_vector<T> mean;
        mean.set_size(numFeatures);
        mean.fill(0);
        
        for (unsigned int j = 0; j < numFeatures; ++j) {
            for (unsigned int i = 0; i < numSamples; ++i) {
                mean[j] += mat(j,i) / numSamples;
            }
        }
        
        for (unsigned int j = 0; j < numFeatures; ++j) {
            for (unsigned int i = 0; i < numSamples; ++i) {
                mat(j,i) -= mean[j];
            }
        }
    }
    
    if (numSamples < numFeatures) {
        // samples < features (HDLSS case)
        vnl_matrix<T> covMat;
        covMat = mat.transpose() * mat / static_cast<T>(numSamples - 1);
        
        vnl_svd<T> svd(covMat);
        vnl_diag_matrix<T> W = svd.W();
        
        vars.resize(numSamples);
        for (unsigned int i = 0; i < numSamples; ++i)
            vars[i] = W(i,i);
        
        comps = mat * svd.U();
		comps.normalize_columns();
        
    }else {
        // samples > features (Conventional case)
        vnl_matrix<T> covMat;
        covMat = mat * mat.transpose() / static_cast<T>(numSamples - 1);
        vnl_svd<T> svd(covMat);
        vnl_diag_matrix<T> W = svd.W();
        
        vars.resize(numFeatures);
        for (unsigned int i = 0; i < numFeatures; ++i)
            vars[i] = W(i,i);

        comps = svd.U();
    }

}


template <typename T>
void mathUtils::GetRotationMatrix_X(double a, vnl_matrix<T>& rot_x)
{
	rot_x.set_size(3, 3);
	rot_x(0, 0) = 1;		rot_x(0, 1) = 0;							rot_x(0, 2) = 0;
	rot_x(1, 0) = 0;		rot_x(1, 1) = static_cast<T>(std::cos(a));	rot_x(1, 2) = static_cast<T>(-std::sin(a));
	rot_x(2, 0) = 0;		rot_x(2, 1) = static_cast<T>(std::sin(a));	rot_x(2, 2) = static_cast<T>(std::cos(a));
}


template <typename T>
void mathUtils::GetRotationMatrix_Y(double b, vnl_matrix<T>& rot_y)
{
	rot_y.set_size(3, 3);
	rot_y(0, 0) = static_cast<T>(std::cos(b));	rot_y(0, 1) = 0;		rot_y(0, 2) = static_cast<T>(std::sin(b));
	rot_y(1, 0) = 0;							rot_y(1, 1) = 1;		rot_y(1, 2) = 0;
	rot_y(2, 0) = static_cast<T>(-std::sin(b));	rot_y(2, 1) = 0;		rot_y(2, 2) = static_cast<T>(std::cos(b));
}


template <typename T>
void mathUtils::GetRotationMatrix_Z(double c, vnl_matrix<T>& rot_z)
{
	rot_z.set_size(3, 3);
	rot_z(0, 0) = static_cast<T>(std::cos(c));	rot_z(0, 1) = static_cast<T>(-std::sin(c));	rot_z(0, 2) = 0;
	rot_z(1, 0) = static_cast<T>(std::sin(c));	rot_z(1, 1) = static_cast<T>(std::cos(c));	rot_z(1, 2) = 0;
	rot_z(2, 0) = 0;							rot_z(2, 1) = 0;							rot_z(2, 2) = 1;
}


template <typename T>
double mathUtils::Correlation(T* a, T* b, int num)
{
	// running mean
	double mean_a = 0, mean_b = 0;
	for (int i = 0; i < num; ++i)
	{
		mean_a += (static_cast<double>(a[i]) / num);
		mean_b += (static_cast<double>(b[i]) / num);
	}

	double cross_sum = 0, var_a = 0, var_b = 0;
	for (int i = 0; i < num; i++)
	{
		cross_sum += (a[i] - mean_a) * (b[i] - mean_b);
		var_a += (a[i] - mean_a) * (a[i] - mean_a);
		var_b += (b[i] - mean_b) * (b[i] - mean_b);
	}

	if (std::abs(var_a) < VECT_EPSILON || std::abs(var_b) < VECT_EPSILON)
	{
		return 0;
		//err_message("variation of either variable is zero in correlation calculation");
	}

	double ret = cross_sum / sqrt(var_a * var_b);

	return ret;
}


void mathUtils::EvenDistribute(int total, std::vector<int>& buckets)
{
	int num_buckets = static_cast<int>(buckets.size());
	assert_message(num_buckets > 0, "no buckets to allocate");

	if (total % num_buckets == 0)
	{
		for (size_t i = 0; i < buckets.size(); ++i)
			buckets[i] = total / num_buckets;
	}
	else
	{
		int first_allocate_num = total / num_buckets;
		for (size_t i = 0; i < buckets.size(); ++i)
			buckets[i] = first_allocate_num;

		int remain_num = total - first_allocate_num * num_buckets;
		assert_message(remain_num > 0 && remain_num < num_buckets, "unexpected remaining number in EvenDistribute");

		for (size_t i = 0; i < buckets.size(); ++i)
		{
			buckets[i] += 1;
			--remain_num;

			if (remain_num == 0)
				break;
		}
	}

	assert_message( std::accumulate(buckets.begin(), buckets.end(), static_cast<int>(0)) == total, "EvenDistrubute failed unexpectedly" );
}


vect3<double> mathUtils::PlaneFit(const std::vector< vect3<double> >& pts)
{
	double x2 = 0, y2 = 0, z2 = 0, xy = 0, xz = 0, yz = 0, x = 0, y = 0, z = 0;

	for (size_t i = 0; i < pts.size(); ++i)
	{
		vect3<double> p = pts[i];

		x2 += p[0] * p[0];
		y2 += p[1] * p[1];
		z2 += p[2] * p[2];

		xy += p[0] * p[1];
		xz += p[0] * p[2];
		yz += p[1] * p[2];

		x += p[0];
		y += p[1];
		z += p[2];
	}

	vnl_matrix<double> A(3, 3);
	vnl_matrix<double> b(3, 1);

	A(0, 0) = x2;	A(0, 1) = xy;	A(0, 2) = xz;
	A(1, 0) = xy;	A(1, 1) = y2;	A(1, 2) = yz;
	A(2, 0) = xz;	A(2, 1) = yz;	A(2, 2) = z2;

	b(0, 0) = x;
	b(1, 0) = y;
	b(2, 0) = z;

	vnl_matrix<double> solution = vnl_matrix_inverse<double>(A) * b;

	vect3<double> ret;
	ret[0] = solution(0, 0);
	ret[1] = solution(1, 0);
	ret[2] = solution(2, 0);

	return ret;
}


vnl_matrix<double> mathUtils::RotationMatrixBetweenVectors(const vect3<double>& v1, const vect3<double>& v2)
{
	vect3<double> v = vect3<double>::cross_product(v1, v2);
	double norm_v = v.l2norm();

	if (std::abs(norm_v) < 1e-8)
		return vnl_matrix<double>(3, 3).set_identity();

	double dot_prod = vect3<double>::dot_product(v1, v2);

	vnl_matrix<double> mat(3, 3);
	mat(0, 0) = 0;		mat(0, 1) = -v[2];	mat(0, 2) = v[1];
	mat(1, 0) = v[2];	mat(1, 1) = 0;		mat(1, 2) = -v[0];
	mat(2, 0) = -v[1];	mat(2, 1) = v[0];	mat(2, 2) = 0;

	vnl_matrix<double> rot_mat = vnl_matrix<double>(3, 3).set_identity() + mat + mat * (1 - dot_prod) / (norm_v * norm_v);

	return rot_mat;
}



} } }


#endif 