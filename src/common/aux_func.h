//
//  aux_func.h
//  FISH
//
//  Created by Yaozong Gao on 05/20/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __aux_func_h__
#define __aux_func_h__

#include "common/vect.h"


namespace BRIC { namespace IDEA { namespace FISH {

inline void err_message(const char* str) {
	assert(false);
	std::cerr << str << std::endl; 
	exit(-1);
}

inline void assert_message(bool flag, const char* str) {
	assert(flag);
	if (!flag) {
		std::cerr << str << std::endl;
		assert(false);
		exit(-1);
	}
}

template <typename T>
inline bool in_range(T val, T bound1, T bound2)
{
	if (bound1 < bound2)
	{
		if (val >= bound1 && val <= bound2)
			return true;
		else
			return false;
	}
	else{

		if (val >= bound2 && val <= bound1)
			return true;
		else
			return false;
	}
}

template <typename T>
inline T truncate(T val, T bound1, T bound2)
{
	if (bound1 < bound2)
	{
		return (val < bound1) ? bound1 : ( (val > bound2) ? bound2 : val );
	}
	else
	{
		return (val < bound2) ? bound2 : ( (val > bound1) ? bound1 : val );
	}
}


template <typename T>
void points_bounding_box(const std::vector< vect3<T> >& pts, vect3<T>& sp, vect3<T>& ep)
{
	sp[0] = sp[1] = sp[2] = std::numeric_limits<T>::max();
	ep[0] = ep[1] = ep[2] = -std::numeric_limits<T>::max();

	for (size_t i = 0; i < pts.size(); ++i)
	{
		const vect3<T>& pt = pts[i];
		for (int j = 0; j < 3; ++j)
		{
			if (pt[j] < sp[j])
				sp[j] = pt[j];

			if (pt[j] > ep[j])
				ep[j] = pt[j];
		}
	}
}


} } }

#endif