//
//  stringUtils.h
//  FISH
//
//  Created by Yaozong Gao on 4/17/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __stringUtils_h__
#define __stringUtils_h__

#include "stdafx.h"

#include <string>
#include <vector>
#include <fstream>
#include <sstream>

#include "common/vect.h"

namespace BRIC { namespace IDEA { namespace FISH {

    
/** @brief string utility
 *
 * string helper class
 */
class stringUtils
{
public:
    
    /** @brief return a slash character depending on the system */
    static std::string Slash() {
#ifdef _WIN32   // Windows
        return std::string("\\");
#else  // other OS
        return std::string("/");
#endif
    }

	/** @brief convert numeric to string */
	template <typename T>
	static std::string num2str(T val);

	/** @brief convert string to numeric */
	template <typename T>
	static T str2num(std::string str);
    
    /** @brief split file name into parent directory & filename */
    static std::vector<std::string> SplitFilename (const std::string& str);
    
    /** @brief split a string given delimiter */
    static void Split(const std::string &s, char delim, std::vector<std::string> &elems);
    
    /** @brief Split a string given delimiter */
	static std::vector<std::string> Split(const std::string &s, char delim);
    
    /** @brief read lines from file */
    static bool ReadLines(const char* filename, std::vector<std::string>& lines);
    
    /** @brief convert a double to hex string */
    static std::string DoubleToHexString(double x);
    
    /** @brief convert a vect3 from string */
    template <typename T>
    static vect3<T> ParseVect3(const std::string& str, char delim = ' ');
    
	/** @brief convert a string into a list of numbers */
	template <typename T>
	static void str2vec(std::string str, std::vector<T>& vec, char delim = ' ');

	/** @brief get extension */
	static std::string extension(std::string str);

	/** @brief get file parts */
	static void get_file_parts(std::string str, std::string& folder, std::string& filename, std::string& extension);

	/** @brief convert a list of numbers into one */
	template <typename T>
	static std::string vec2str(const std::vector<T>& nums, char delim);

	/** @brief convert a list of strings into one */
	static std::string token2str(const std::vector<std::string>& tokens, char delim);

	/** @brief get class name */
	template <typename T>
	static std::string GetClassName(const T&);
};



//
// Implementations
//
std::vector<std::string> stringUtils::SplitFilename (const std::string& str)
{
	std::vector<std::string> parts;
	size_t found = str.find_last_of("/\\");
	if (found == std::string::npos) {
		parts.push_back(str);
		parts.push_back("");
	}else {
		parts.push_back( str.substr(0,found) );
		parts.push_back( str.substr(found+1) );
	}

	return parts;
}

template <typename T>
std::string stringUtils::num2str(T val)
{
    std::ostringstream out;
	out << val;
	return out.str();
}

template <typename T>
T stringUtils::str2num(std::string str)
{
	T ret;
	std::istringstream in(str);
	in >> ret;
	return ret;
}


void stringUtils::Split(const std::string &s, char delim, std::vector<std::string> &elems)
{
	std::stringstream ss(s);
	std::string item;
	while(std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return;
}


std::vector<std::string> stringUtils::Split(const std::string &s, char delim)
{
	std::vector<std::string> elems;
	Split(s, delim, elems);
	return elems;
}


bool stringUtils::ReadLines(const char* filename, std::vector<std::string>& lines)
{
	std::ifstream infile;
	infile.open(filename, std::ifstream::in);

	if (!infile){
		std::cout << "fails to load " << filename << "!" << std::endl;
		return false;
	}

	std::string line;
	while (!infile.eof())
	{
		getline(infile, line);

		if (line.size() > 0) {
			if(line[line.size()-1] == '\r')
				lines.push_back(line.substr(0, line.size()-1));
			else
				lines.push_back(line);
		}
	}

	infile.close();
	return true;
}


std::string stringUtils::DoubleToHexString(double x) {

	const size_t bytesInDouble = sizeof(double);
	union {
		double value;
		unsigned char bytes[bytesInDouble];
	} u;

	u.value = x;

	char *buffer = new char[bytesInDouble * 2 + 1];
	unsigned char *input = u.bytes;
	char *output = buffer;

	for(unsigned int i = 0; i < bytesInDouble; ++i) {
		sprintf(output, "%02X", *input);
		++input;
		output += 2;
	}

	return std::string(buffer);
}


template <typename T>
vect3<T> stringUtils::ParseVect3(const std::string& str, char delim) 
{
	std::vector< std::string > elements;
	Split(str, delim, elements);
	if (elements.size() != 3) {
		std::cerr << "elements must be triple values" << std::endl;
		exit(-1);
	}

	vect3<T> ret;

	try {
		for(int i = 0; i < 3; ++i) {
			ret[i] = boost::lexical_cast<T>(elements[i]);
		}
	} catch ( boost::bad_lexical_cast& error ) {
		throw error;
	}

	return ret;
}

template <typename T>
void stringUtils::str2vec(std::string str, std::vector<T>& vec, char delim)
{
	std::vector<std::string> strs;
	Split(str, delim, strs);

	vec.resize(strs.size());
	for(unsigned int i = 0; i < vec.size(); ++i) 
		vec[i] = str2num<T>(strs[i]);
}

std::string stringUtils::extension(std::string str)
{
	size_t i = str.find_last_of('.');
	if (i == std::string::npos)
		return std::string("");

	return str.substr(i + 1);
}

void stringUtils::get_file_parts(std::string str, std::string& folder, std::string& filename, std::string& extension)
{
	size_t found = str.find_last_of("/\\");
	if (found == std::string::npos) {
		folder = str;
		filename = "";
		extension = "";
		return;
	}

	folder = str.substr(0, found);
	std::string filename_with_ext = str.substr(found + 1);

	size_t i = filename_with_ext.find_last_of('.');
	if (i == std::string::npos)
	{
		filename = filename_with_ext;
		extension = "";
	}
	else
	{
		filename = filename_with_ext.substr(0, i);
		extension = filename_with_ext.substr(i + 1);
	}
}

template <typename T>
std::string stringUtils::vec2str(const std::vector<T>& nums, char delim)
{
	std::string ret;

	for (size_t i = 0; i < nums.size(); ++i)
	{
		if (i == 0)
			ret += num2str(nums[i]);
		else
			ret += delim + num2str(nums[i]);
	}

	return ret;
}


std::string stringUtils::token2str(const std::vector<std::string>& tokens, char delim)
{
	std::string ret;
	for (size_t i = 0; i < tokens.size(); i++)
	{
		if (i == 0)
			ret += tokens[i];
		else
			ret += delim + tokens[i];
	}

	return ret;
}


template <typename T>
std::string stringUtils::GetClassName(const T&)
{
	return typeid(T).name();
}


} } }


#endif

