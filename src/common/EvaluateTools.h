//
//  EvaluateTools.h
//  FISH
//
//  Created by Yaozong Gao on 10/14/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __EvaluateTools_h__
#define __EvaluateTools_h__

#include "common/mxImageUtils.h"
#include "mesh/surface/SurfaceUtils.h"

namespace BRIC { namespace IDEA { namespace FISH {


class EvaluateTools
{
public:
	/** @brief compute Hausdroff distance */
	template <typename T>
	static double HausdroffDistance(const mxImage<T>& image1, T obj_val1, const mxImage<T>& image2, T obj_val2);

	/** @brief compute */
	template <typename T>
	static double GeneralizedHausdroffDistance(const mxImage<T>& image1, T obj_val1, const mxImage<T>& image2, T obj_val2, double percentage);

	/** @brief compute average surface distance */
	template <typename T>
	static double AverageSurfaceDistance(const mxImage<T>& image1, T objectVal1, const mxImage<T>& image2, T objectVal2);

	/** @brief compute dsc */
	template <typename T1, typename T2>
	static double DSC(const mxImage<T1>& image1, T1 obj_val1, const mxImage<T2>& image2, T2 obj_val2);

	/** @brief compute sensitivity */
	template <typename T1, typename T2>
	static double Sensitivity(const mxImage<T1>& test_image, T1 obj_val1, const mxImage<T2>& gt_image, T2 obj_val2);

	/** @brief compute PPV */
	template <typename T1, typename T2>
	static double PPV(const mxImage<T1>& test_image, T1 obj_val1, const mxImage<T2>& gt_image, T2 obj_val2);

	/** @brief find common slice range */
	template <typename T1, typename T2>
	static void FindCommonSliceRange(const mxImage<T1>& image1, const mxImage<T2>& image2, unsigned int& sp, unsigned int& ep);

	/** @brief mask out image except regions between slices */
	template <typename T1, typename T2>
	static void MaskSliceRegion(mxImage<T1>& image1, mxImage<T2>& image2, unsigned int sp, unsigned int ep);

	/** @brief compute rectum-dsc */
	template <typename T1, typename T2>
	static double RectumDSC(const mxImage<T1>& image1, T1 obj_val1, const mxImage<T2>& image2, T2 obj_val2);

	/** @brief compute rectum-asd */
	template <typename T1, typename T2>
	static double RectumASD(const mxImage<T1>& image1, T1 obj_val1, const mxImage<T2>& image2, T2 obj_val2);

	/** @brief compute rectum-sen */
	template <typename T1, typename T2>
	static double RectumSensitivity(const mxImage<T1>& image1, T1 obj_val1, const mxImage<T2>& image2, T2 obj_val2);

	/** @brief compute rectum-ppv */
	template <typename T1, typename T2>
	static double RectumPPV(const mxImage<T1>& image1, T1 obj_val1, const mxImage<T2>& image2, T2 obj_val2);
};


//////////////////////////////////////////////////////////////////////////

template <typename T>
double EvaluateTools::HausdroffDistance(const mxImage<T>& image1, T obj_val1, const mxImage<T>& image2, T obj_val2)
{
	Surface surf1, surf2;
	SurfaceUtils::IsoSurfaceFromImage(image1, surf1, obj_val1);
	SurfaceUtils::IsoSurfaceFromImage(image2, surf2, obj_val2);

	const std::vector<float>& pt1 = surf1.GetPoints();
	const std::vector<float>& pt2 = surf2.GetPoints();
	unsigned int pt_num1 = pt1.size() / 3;
	unsigned int pt_num2 = pt2.size() / 3;

	// 1 to 2
	double max_dist1 = 0;
	for (unsigned int i = 0; i < pt_num1; ++i)
	{
		vect3<float> v1(pt1[i * 3], pt1[i * 3 + 1], pt1[i * 3 + 2]);
		double minDist = std::numeric_limits<double>::max();
		for (unsigned int j = 0; j < pt_num2; ++j)
		{
			vect3<float> v2(pt2[j * 3], pt2[j * 3 + 1], pt2[j * 3 + 2]);
			double dist = (v2 - v1).norm();
			if (dist < minDist)
				minDist = dist;
		}
		max_dist1 = std::max( max_dist1, minDist );
	}

	// 2 to 1
	double max_dist2 = 0;
	for (unsigned int i = 0; i < pt_num2; ++i)
	{
		vect3<float> v2(pt2[i * 3], pt2[i * 3 + 1], pt2[i * 3 + 2]);
		double minDist = std::numeric_limits<double>::max();
		for (unsigned int j = 0; j < pt_num1; ++j)
		{
			vect3<float> v1(pt1[j * 3], pt1[j * 3 + 1], pt1[j * 3 + 2]);
			double dist = (v2 - v1).norm();
			if (dist < minDist)
				minDist = dist;
		}
		max_dist2 = std::max( max_dist2, minDist );
	}
	
	return std::max(max_dist1, max_dist2);
}

template <typename T>
double EvaluateTools::GeneralizedHausdroffDistance(const mxImage<T>& image1, T obj_val1, const mxImage<T>& image2, T obj_val2, double percentage) {

	Surface surf1, surf2;
	SurfaceUtils::IsoSurfaceFromImage(image1, surf1, obj_val1);
	SurfaceUtils::IsoSurfaceFromImage(image2, surf2, obj_val2);

	const std::vector<float>& pt1 = surf1.GetPoints();
	const std::vector<float>& pt2 = surf2.GetPoints();
	unsigned int pt_num1 = pt1.size() / 3;
	unsigned int pt_num2 = pt2.size() / 3;

	// 1 to 2
	std::vector<double> distances;
	distances.resize(pt_num1);

	for (unsigned int i = 0; i < pt_num1; ++i)
	{
		vect3<float> v1(pt1[i * 3], pt1[i * 3 + 1], pt1[i * 3 + 2]);
		double minDist = std::numeric_limits<double>::max();
		for (unsigned int j = 0; j < pt_num2; ++j)
		{
			vect3<float> v2(pt2[j * 3], pt2[j * 3 + 1], pt2[j * 3 + 2]);
			double dist = (v2 - v1).norm();
			if (dist < minDist)
				minDist = dist;
		}
		distances[i] = minDist;
	}
	std::sort(distances.begin(), distances.end());
	unsigned int idx = static_cast<unsigned int>(pt_num1 * percentage + 0.5);
	if (idx >= pt_num1) idx = pt_num1 - 1;
	double dist1 = distances[idx];

	// 2 to 1
	distances.resize(pt_num2);
	for (unsigned int i = 0; i < pt_num2; ++i)
	{
		vect3<float> v2(pt2[i * 3], pt2[i * 3 + 1], pt2[i * 3 + 2]);
		double minDist = std::numeric_limits<double>::max();
		for (unsigned int j = 0; j < pt_num1; ++j)
		{
			vect3<float> v1(pt1[j * 3], pt1[j * 3 + 1], pt1[j * 3 + 2]);
			double dist = (v2 - v1).norm();
			if (dist < minDist)
				minDist = dist;
		}
		distances[i] = minDist;
	}
	std::sort(distances.begin(), distances.end());
	idx = static_cast<unsigned int>(pt_num2 * percentage + 0.5);
	if (idx >= pt_num2) idx = pt_num2 - 1;
	double dist2 = distances[idx];

	return std::max(dist1, dist2);
}

template <typename T>
double EvaluateTools::AverageSurfaceDistance(const mxImage<T>& image1, T objectVal1, const mxImage<T>& image2, T objectVal2)
{
	Surface surf1, surf2;
	SurfaceUtils::IsoSurfaceFromImage(image1, surf1, objectVal1);
	SurfaceUtils::IsoSurfaceFromImage(image2, surf2, objectVal2);

	const std::vector<float>& pt1 = surf1.GetPoints();
	const std::vector<float>& pt2 = surf2.GetPoints();
	unsigned int pt_num1 = pt1.size() / 3;
	unsigned int pt_num2 = pt2.size() / 3;

	// 1 to 2
	double surfdist1 = 0;
	for (unsigned int i = 0; i < pt_num1; ++i)
	{
		vect3<float> v1(pt1[i*3], pt1[i*3+1], pt1[i*3+2]);
		double minDist = std::numeric_limits<double>::max();
		for (unsigned int j = 0; j < pt_num2; ++j)
		{
			vect3<float> v2(pt2[j*3], pt2[j*3+1], pt2[j*3+2]);
			double dist = (v2-v1).norm();
			if ( dist < minDist )
				minDist = dist;
		}
		surfdist1 += minDist;
	}
	surfdist1 /= pt_num1;

	// 2 to 1
	double surfdist2 = 0;
	for (unsigned int i = 0; i < pt_num2; ++i)
	{
		vect3<float> v2(pt2[i*3], pt2[i*3+1], pt2[i*3+2]);
		double minDist = std::numeric_limits<double>::max();
		for (unsigned int j = 0; j < pt_num1; ++j)
		{
			vect3<float> v1(pt1[j*3], pt1[j*3+1], pt1[j*3+2]);
			double dist = (v2-v1).norm();
			if ( dist < minDist )
				minDist = dist;
		}
		surfdist2 += minDist;
	}
	surfdist2 /= pt_num2;

	return (surfdist1 + surfdist2) / 2;
}

template <typename T1, typename T2>
double EvaluateTools::DSC(const mxImage<T1>& image1, T1 obj_val1, const mxImage<T2>& image2, T2 obj_val2)
{
	vect3<unsigned int> size1 = image1.GetImageSize();
	vect3<unsigned int> size2 = image2.GetImageSize();

	if (size1 != size2) {
		std::cout << "image sizes are not consistent" << std::endl;
		exit(-1);
	}

	size_t obj1 = 0, obj2 = 0, joint = 0;
	for (unsigned int z = 0; z < size1[2]; ++z)
	{
		for (unsigned int y = 0; y < size1[1]; ++y)
		{
			for (unsigned int x = 0; x < size1[0]; ++x)
			{
				if (image1(x, y, z) == obj_val1)
					++obj1;
				if (image2(x, y, z) == obj_val2)
					++obj2;
				if (image1(x, y, z) == obj_val1 && image2(x, y, z) == obj_val2)
					++joint;
			}
		}
	}

	double dsc = static_cast<double>(joint)* 2 / (static_cast<double>(obj1) + static_cast<double>(obj2));
	return dsc;
}

template <typename T1, typename T2>
double EvaluateTools::Sensitivity(const mxImage<T1>& test_image, T1 obj_val1, const mxImage<T2>& gt_image, T2 obj_val2)
{
	unsigned int true_positive = 0, false_negative = 0, false_positive = 0, true_negative = 0;

	vect3<unsigned int> size1 = test_image.GetImageSize();
	vect3<unsigned int> size2 = gt_image.GetImageSize();

	if (size1 != size2) {
		std::cout << "image sizes are not consistent" << std::endl;
		exit(-1);
	}

	for (unsigned int z = 0; z < size1[2]; ++z) {
		for (unsigned int y = 0; y < size1[1]; ++y) {
			for (unsigned int x = 0; x < size1[0]; ++x) {

				if (test_image(x, y, z) == obj_val1 && gt_image(x, y, z) == obj_val2)
					++true_positive;
				if (test_image(x, y, z) == obj_val1 && gt_image(x, y, z) != obj_val2)
					++false_positive;
				if (test_image(x, y, z) != obj_val1 && gt_image(x, y, z) == obj_val2)
					++false_negative;
				if (test_image(x, y, z) != obj_val1 && gt_image(x, y, z) != obj_val2)
					++true_negative;
			}
		}
	}

	float sensitivity = true_positive * 1.0f / (true_positive + false_negative);
	float specificity = true_negative * 1.0f / (true_negative + false_positive);
	float ppv = true_positive * 1.0f / (true_positive + false_positive);

	return sensitivity;
}

template <typename T1, typename T2>
double EvaluateTools::PPV(const mxImage<T1>& test_image, T1 obj_val1, const mxImage<T2>& gt_image, T2 obj_val2)
{
	unsigned int true_positive = 0, false_negative = 0, false_positive = 0, true_negative = 0;

	vect3<unsigned int> size1 = test_image.GetImageSize();
	vect3<unsigned int> size2 = gt_image.GetImageSize();

	if (size1 != size2) {
		std::cout << "image sizes are not consistent" << std::endl;
		exit(-1);
	}

	for (unsigned int z = 0; z < size1[2]; ++z) {
		for (unsigned int y = 0; y < size1[1]; ++y) {
			for (unsigned int x = 0; x < size1[0]; ++x) {

				if (test_image(x, y, z) == obj_val1 && gt_image(x, y, z) == obj_val2)
					++true_positive;
				if (test_image(x, y, z) == obj_val1 && gt_image(x, y, z) != obj_val2)
					++false_positive;
				if (test_image(x, y, z) != obj_val1 && gt_image(x, y, z) == obj_val2)
					++false_negative;
				if (test_image(x, y, z) != obj_val1 && gt_image(x, y, z) != obj_val2)
					++true_negative;
			}
		}
	}

	float sensitivity = true_positive * 1.0f / (true_positive + false_negative);
	float specificity = true_negative * 1.0f / (true_negative + false_positive);
	float ppv = true_positive * 1.0f / (true_positive + false_positive);

	return ppv;
}


template <typename T1, typename T2>
void EvaluateTools::FindCommonSliceRange(const mxImage<T1>& image1, const mxImage<T2>& image2, unsigned int& sp, unsigned int& ep)
{
	vect3<unsigned int> size1 = image1.GetImageSize();
	vect3<unsigned int> size2 = image2.GetImageSize();

	if (size1 != size2) {
		std::cout << "image sizes are not consistent" << std::endl;
		exit(-1);
	}

	unsigned int sp1, ep1, sp2, ep2;
	mxImageUtils::FindSliceRange(image1, sp1, ep1);
	mxImageUtils::FindSliceRange(image2, sp2, ep2);

	if (ep1 < sp1 || ep2 < sp2) {
		sp = 2; ep = 1;
	}

	if (sp1 > sp2)
		sp = sp1;
	else
		sp = sp2;

	if (ep1 > ep2)
		ep = ep2;
	else
		ep = ep1;
}


template <typename T1, typename T2>
void EvaluateTools::MaskSliceRegion(mxImage<T1>& image1, mxImage<T2>& image2, unsigned int sp, unsigned int ep)
{
	vect3<unsigned int> size = image1.GetImageSize();

	for (unsigned int z = 0; z < sp; ++z) {
		for (unsigned int y = 0; y < size[1]; ++y) {
			for (unsigned int x = 0; x < size[0]; ++x) {
				image1(x, y, z) = 0;
				image2(x, y, z) = 0;
			}
		}
	}

	for (unsigned int z = ep + 1; z < size[2]; ++z) {
		for (unsigned int y = 0; y < size[1]; ++y) {
			for (unsigned int x = 0; x < size[0]; ++x) {
				image1(x, y, z) = 0;
				image2(x, y, z) = 0;
			}
		}
	}
}


template <typename T1, typename T2>
double EvaluateTools::RectumDSC(const mxImage<T1>& image1, T1 obj_val1, const mxImage<T2>& image2, T2 obj_val2)
{
	unsigned int sp, ep;
	FindCommonSliceRange(image1, image2, sp, ep);

	if (ep < sp)
		return 0;

	mxImage<T1> mask_image1;
	mxImage<T2> mask_image2;
	mxImageUtils::Copy(image1, mask_image1);
	mxImageUtils::Copy(image2, mask_image2);

	MaskSliceRegion(mask_image1, mask_image2, sp, ep);

	return DSC(mask_image1, obj_val1, mask_image2, obj_val2);
}

template <typename T1, typename T2>
double EvaluateTools::RectumASD(const mxImage<T1>& image1, T1 obj_val1, const mxImage<T2>& image2, T2 obj_val2)
{
	unsigned int sp, ep;
	FindCommonSliceRange(image1, image2, sp, ep);

	mxImage<T1> mask_image1;
	mxImage<T2> mask_image2;
	mxImageUtils::Copy(image1, mask_image1);
	mxImageUtils::Copy(image2, mask_image2);

	MaskSliceRegion(mask_image1, mask_image2, sp, ep);

	return AverageSurfaceDistance(mask_image1, obj_val1, mask_image2, obj_val2);
}

template <typename T1, typename T2>
double EvaluateTools::RectumSensitivity(const mxImage<T1>& image1, T1 obj_val1, const mxImage<T2>& image2, T2 obj_val2)
{
	unsigned int sp, ep;
	FindCommonSliceRange(image1, image2, sp, ep);

	mxImage<T1> mask_image1;
	mxImage<T2> mask_image2;
	mxImageUtils::Copy(image1, mask_image1);
	mxImageUtils::Copy(image2, mask_image2);

	MaskSliceRegion(mask_image1, mask_image2, sp, ep);

	return Sensitivity(mask_image1, obj_val1, mask_image2, obj_val2);
}

template <typename T1, typename T2>
double EvaluateTools::RectumPPV(const mxImage<T1>& image1, T1 obj_val1, const mxImage<T2>& image2, T2 obj_val2)
{
	unsigned int sp, ep;
	FindCommonSliceRange(image1, image2, sp, ep);

	mxImage<T1> mask_image1;
	mxImage<T2> mask_image2;
	mxImageUtils::Copy(image1, mask_image1);
	mxImageUtils::Copy(image2, mask_image2);

	MaskSliceRegion(mask_image1, mask_image2, sp, ep);

	return PPV(mask_image1, obj_val1, mask_image2, obj_val2);
}


} } }


#endif