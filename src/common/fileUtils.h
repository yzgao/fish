//
//  fileUtils.h
//  FISH
//
//  Created by Yaozong Gao on 9/10/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __fileUtils_h__
#define __fileUtils_h__

#include "stdafx.h"

namespace BRIC { namespace IDEA { namespace FISH {

class fileUtils
{
public:
	static void get_all_files(std::string folder, std::string wildcard, std::vector<std::string>& files);
};


void fileUtils::get_all_files(std::string folder, std::string wildcard, std::vector<std::string>& files)
{
	boost::filesystem::path directory(folder);
	boost::filesystem::directory_iterator it(directory), end_it;

	typedef std::set<std::string> SetType;
	SetType sorted_set;
	const boost::regex filter(wildcard);

	while (it != end_it)
	{
		if (!boost::filesystem::is_regular_file(it->status()))
		{
			++it;
			continue;
		}

		std::string filename = it->path().filename().string();

		boost::smatch what;
		if (!boost::regex_match(filename, what, filter)) continue;

		sorted_set.insert(it->path().string());
		++it;
	}

	SetType::const_iterator set_it = sorted_set.begin();
	while (set_it != sorted_set.end())
	{
		files.push_back(*set_it);
		++set_it;
	}
}


} } }


#endif // !__fileUtils_h__
