//
//  SmartCast.cpp
//  Fish
//
//  Created by Yaozong Gao on 6/6/13.
//
//

#ifndef __SmartCast_h__
#define __SmartCast_h__


#include <limits>


namespace BRIC { namespace IDEA { namespace FISH {


template <typename OutputType>
class SmartCast
{
public:
	template <typename InputType>
	static OutputType Cast(InputType x) {
		return static_cast<OutputType>(x);
	}
};

template <>
class SmartCast<int>
{
public:
	template <typename InputType>
	static int Cast(InputType x) {
		return x >= 0 ? static_cast<int>(x + 0.5) : static_cast<int>(x - 0.5);
	}
};

template <>
class SmartCast<unsigned int>
{
public:
	template <typename InputType>
	static int Cast(InputType x) {
        if(x >= 0)
            return static_cast<unsigned int>(x + 0.5);
        else {
            std::cerr << "bad smart cast" << std::endl;
            assert(false);
            exit(-1);
        }
	}
};

template <>
class SmartCast<long>
{
public:
	template <typename InputType>
	static int Cast(InputType x) {
		return x >= 0 ? static_cast<long>(x + 0.5) : static_cast<long>(x - 0.5);
	}
};

template <>
class SmartCast<unsigned long>
{
public:
	template <typename InputType>
	static int Cast(InputType x) {
        if(x >= 0)
            return static_cast<unsigned long>(x + 0.5);
        else {
            std::cerr << "bad smart cast" << std::endl;
            assert(false);
            exit(-1);
        }
	}
};

template <>
class SmartCast<short>
{
public:
	template <typename InputType>
	static int Cast(InputType x) {
		return x >= 0 ? static_cast<short>(x + 0.5) : static_cast<short>(x - 0.5);
	}
};

template <>
class SmartCast<unsigned short>
{
public:
	template <typename InputType>
	static int Cast(InputType x) {
        if(x >= 0)
            return static_cast<unsigned short>(x + 0.5);
        else {
            std::cerr << "bad smart cast" << std::endl;
            assert(false);
            exit(-1);
        }
	}
};


} } }


#endif

