//
//  PCAShapeContext.h
//  FISH
//
//  Created by Yaozong Gao on 10/9/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __PCAShapeContext_h__
#define __PCAShapeContext_h__

#include "deform/interfaces.h"
#include "mesh/PCAShapeSpace.h"
#include "mesh/PDMTransform.h"

namespace BRIC { namespace IDEA { namespace FISH {

class PCAShapeContext: public IShapeContext
{
public:
	PCAShapeContext(const PCAShapeSpace& ss, double max_coeff = 3, float ratio = 1);
	virtual void RefineShape( const PDM& pdm, PDM& refinedPDM );
private:
	const PCAShapeSpace* m_ss;
	double m_maxcoeff;
	float m_ratio;
};

//////////////////////////////////////////////////////////////////////////

PCAShapeContext::PCAShapeContext(const PCAShapeSpace& ss, double max_coeff, float ratio)
{
	m_ss = &ss;
	m_maxcoeff = max_coeff;
	m_ratio = ratio;
}

void PCAShapeContext::RefineShape( const PDM& pdm, PDM& refinedPDM )
{
	// make a copy
	PDM df_pdm = pdm;

	// mapping to PCA shape space
	float e = 0;
	PDM meanPDM, alignedPDM;
	m_ss->GetPDM(&e, 1, meanPDM);

	PDMAffineTransform T;
	T(pdm, meanPDM, alignedPDM);

	m_ss->MapToSpace(alignedPDM, m_maxcoeff, alignedPDM);

	T.Inverse();
	T.Apply(alignedPDM, refinedPDM);

	// weighted linear combination
	if ( std::abs(m_ratio - 1) > VECT_EPSILON )
	{
		const std::vector<float>& df_pts = df_pdm.GetPoints();
		std::vector<float>& sh_pts = refinedPDM.GetPoints();
		size_t pt_num = sh_pts.size() / 3;

		for (size_t i = 0; i < pt_num; ++i) {
			vect3<float> df_pt(df_pts[i * 3], df_pts[i * 3 + 1], df_pts[i * 3 + 2]);
			vect3<float> sh_pt(sh_pts[i * 3], sh_pts[i * 3 + 1], sh_pts[i * 3 + 2]);
			vect3<float> pt = df_pt * (1 - m_ratio) + sh_pt * m_ratio;
			sh_pts[i * 3] = pt[0];
			sh_pts[i * 3 + 1] = pt[1];
			sh_pts[i * 3 + 2] = pt[2];
		}
	}
}

} } }

#endif