//
//  FreeShapeContext.h
//  FISH
//
//  Created by Yaozong Gao on 04/16/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __FreeShapeContext_h__
#define __FreeShapeContext_h__

#include "deform/interfaces.h"
#include "mesh/surface/SurfaceUtils.h"

namespace BRIC { namespace IDEA { namespace FISH {

class FreeShapeContext : public IShapeContext
{
public:
	FreeShapeContext(unsigned int iternum);
	virtual void RefineShape(const PDM& pdm, PDM& refinedPDM);
private:
	unsigned int m_iternum;
};

FreeShapeContext::FreeShapeContext(unsigned int iternum)
{
	m_iternum = iternum;
}

void FreeShapeContext::RefineShape(const PDM& pdm, PDM& refinedPDM)
{
	const Surface& surf = static_cast<const Surface&>(pdm);
	Surface& refined_surf = static_cast<Surface&>(refinedPDM);

	refined_surf = surf;
	if (m_iternum != 0)
		SurfaceUtils::SmoothSurface(refined_surf, m_iternum);

	SurfaceUtils::Remesh(refined_surf, 0.3f, 100);
}

} } }

#endif