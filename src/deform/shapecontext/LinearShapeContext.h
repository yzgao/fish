//
//  LinearShapeContext.h
//  FISH
//
//  Created by Yaozong Gao on 04/16/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __LinearShapeContext_h__
#define __LinearShapeContext_h__

#include "deform/interfaces.h"
#include "mesh/PDMTransform.h"

namespace BRIC { namespace IDEA { namespace FISH {

template <typename T>
class LinearShapeContext : public IShapeContext
{
public:
	LinearShapeContext(const PDM& shape);
	virtual void RefineShape(const PDM& pdm, PDM& refinedPDM);
private:
	PDM m_shape;
};

template <typename T>
LinearShapeContext<T>::LinearShapeContext(const PDM& shape)
{
	std::vector<float>& pts = m_shape.GetPoints();
	const std::vector<float>& arg_pts = shape.GetPoints();

	pts.resize( arg_pts.size() );
	::memcpy(&pts[0], &arg_pts[0], sizeof(float) * pts.size());
}

template <typename T>
void LinearShapeContext<T>::RefineShape(const PDM& pdm, PDM& refinedPDM)
{
	T transform;
	transform(m_shape, pdm, refinedPDM);
}


} } }

#endif