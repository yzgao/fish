//
//  interfaces.h
//  FISH
//
//  Created by Yaozong Gao on 10/9/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __deform_interfaces_h__
#define __deform_interfaces_h__

#include "common/vect.h"
#include "mesh/PDM.h"
#include "mesh/surface/Surface.h"

namespace BRIC { namespace IDEA { namespace FISH {

class IShapeContext 
{
public:
	virtual void RefineShape( const PDM& pdm, PDM& refinedPDM ) = 0;
};

class IDeformContext: public IShapeContext
{
public:
	virtual vect3<float> GetVertexOffset(const Surface& surf, unsigned int vertIdx, double searchRange) = 0;
	
	// debug interface
	virtual void SaveSurfaceToImage(const Surface& surf, const char* path) {}
};

} } }


#endif