//
//  SurfaceRegistration.h
//  Fish
//
//  Created by Yaozong Gao on 3/17/14.
//

#ifndef __SurfaceRegistration_h__
#define __SurfaceRegistration_h__

#include "mesh/surface/SurfaceUtils.h"
#include "mesh/PDMTransform.h"
#include "common/mxImageUtils.h"
#include "extern/NearestPointIndexer.h"
#include "deform/GVF.h"

#include <boost/numeric/ublas/matrix.hpp>

namespace BRIC { namespace IDEA { namespace FISH {

// parameters for surface registration
class SurfaceRegistrationParams
{
public:

	int dm_iternum;			// iteration number in the non-linear deformation (by default 4000)
	float dm_stepsize;		// step size in the non-linear deformation (by default 0.2)
	int dm_nbsize;			// neighbor size in neighborhood deformation (by default 4, the minimum should be >= 2)
	double dm_sigma;		// Gaussian sigma in neighborhood deformation (by default 0.6)
	double dm_smooth;		// the weight of internal smooth force (by default 1)
	double dm_aff;			// the weight of affine deformation (by default 0.99)

	SurfaceRegistrationParams() : dm_iternum(4000), dm_stepsize(0.2), dm_nbsize(4), dm_sigma(0.6), dm_smooth(1), dm_aff(0.99) {}

	// rectum & bladder: dm_nbsize - 4, dm_smooth - 1
	// prostate: dm_nbsize - 3, dm_smooth - 0
};

// register two surfaces based on the nearest distance
class SurfaceRegistration
{
public:

	/** @brief register two surfaces based on the nearest distance */
	template <typename T>
	static void Warp(Surface& srcSurf, const mxImage<T>& binaryImage, T isoval, const SurfaceRegistrationParams& params);
	static void Warp(Surface& srcSurf, const Surface& dstSurf, const SurfaceRegistrationParams& params);

private:

	/** @brief non-linear deformation */
	static void NonlinearDeform(Surface& srcSurf, const Surface& tgtSurf, float stepsize, int iternum, int nbsize, double sigma, 
		double smoothRatio, double affRatio, int numSmoothIter);

	/** @brief return a unit directional vector by using nearest distance */
	static void ComputeExternalForces(const Surface& srcSurf, const Surface& dstSurf, std::vector< vect3<float> >& ext_forces);
	static vect3<float> GetInternalOffset(Surface& surf, int idx);
};

//////////////////////////////////////////////////////////////////////////

template <typename T>
void SurfaceRegistration::Warp(Surface& srcSurf, const mxImage<T>& binaryImage, T isoval, const SurfaceRegistrationParams& params)
{
	Surface dstSurf;
	SurfaceUtils::IsoSurfaceFromImage(binaryImage, dstSurf, isoval);

	Warp(srcSurf, dstSurf, params);
}

void SurfaceRegistration::Warp(Surface& srcSurf, const Surface& dstSurf, const SurfaceRegistrationParams& params)
{
	srcSurf.UpdateNeighbors();

	// hierarchical non-linear deformation 
	int surfSmoothIteration = 3;

	NonlinearDeform(srcSurf, dstSurf, params.dm_stepsize, params.dm_iternum / 4, params.dm_nbsize, params.dm_sigma, params.dm_smooth, params.dm_aff, surfSmoothIteration);
	NonlinearDeform(srcSurf, dstSurf, params.dm_stepsize, params.dm_iternum / 4, params.dm_nbsize, params.dm_sigma, params.dm_smooth * 0.75, params.dm_aff, surfSmoothIteration);
	NonlinearDeform(srcSurf, dstSurf, params.dm_stepsize, params.dm_iternum / 4, params.dm_nbsize, params.dm_sigma, params.dm_smooth * 0.5, params.dm_aff, surfSmoothIteration);
	NonlinearDeform(srcSurf, dstSurf, params.dm_stepsize, params.dm_iternum / 4, params.dm_nbsize, params.dm_sigma, params.dm_smooth * 0.25, params.dm_aff, surfSmoothIteration);
}

void SurfaceRegistration::NonlinearDeform(Surface& srcSurf, const Surface& dstSurf, float stepsize, int iternum, int nbsize, double sigma, 
	double smoothRatio, double affRatio, int numSmoothIter)
{
	// align with the center
	vect3<float> center = dstSurf.Center();
	srcSurf.MoveTo(center);

	std::vector<float>& pts = srcSurf.GetPoints();
	std::vector< vect3<float> >& normals = srcSurf.GetNormals();
	int pt_num = static_cast<int>(pts.size() / 3);

	std::vector< std::vector<unsigned int> > nbvert_idxs, nbvert_levels;
	nbvert_idxs.resize(pt_num);
	nbvert_levels.resize(pt_num);

	// compute neighborhood for all vertices
	for (int j = 0; j < pt_num; ++j)
		SurfaceUtils::GetVertexNeighborhood(srcSurf, j, nbsize, nbvert_idxs[j], nbvert_levels[j]);

	// compute exponential table (for efficiency)
	std::vector<double> expTable;
	expTable.resize(nbsize + 1);
	if (nbsize == 0)
		expTable[0] = 1;
	else {
		for (size_t i = 0; i < expTable.size(); ++i) {
			double ratio = static_cast<double>(i) / static_cast<double>(nbsize);
			expTable[i] = exp(-ratio / sigma);
		}
	}

	Surface tmpSurf;
	tmpSurf.CopyFace(srcSurf);

	std::vector<float>& tmp_pts = tmpSurf.GetPoints();
	tmp_pts.resize(pts.size());

	PDMAffineTransform affine;

	// buffer for external forces
	std::vector< vect3<float> > ext_forces;
	ext_forces.resize( pt_num );

	// surface deformation
	for (int i = 0; i < iternum; ++i)
	{
		if ((i + 1) % 100 == 0)
			std::cout << (i+1) << std::endl;

		ComputeExternalForces(srcSurf, dstSurf, ext_forces);

		// clear to zero
		for (size_t k = 0; k < tmp_pts.size(); ++k)
			tmp_pts[k] = 0;

		srcSurf.UpdateNormals();

		for (int j = 0; j < pt_num; ++j)
		{
			vect3<float> extf = ext_forces[j];
			extf = normals[j] * vect3<float>::dot_product(extf, normals[j]) * stepsize;

			vect3<float> intf = GetInternalOffset(srcSurf, j);
			intf = intf - normals[j] * vect3<float>::dot_product(intf, normals[j]);
			intf = intf * stepsize * smoothRatio;

			vect3<float> combf = (extf + intf) / 2;

			for (size_t k = 0; k < nbvert_idxs[j].size(); ++k)
			{
				unsigned int idx = nbvert_idxs[j][k];
				unsigned int level = nbvert_levels[j][k];

				double w = expTable[level];
				tmp_pts[idx * 3] += combf[0] * w;
				tmp_pts[idx * 3 + 1] += combf[1] * w;
				tmp_pts[idx * 3 + 2] += combf[2] * w;
			}
		}

		// non-linear deformation 
		for (size_t j = 0; j < pts.size(); ++j)
			tmp_pts[j] = pts[j] + tmp_pts[j];

		// smooth a little bit for the non-linear deformed surface
		if (numSmoothIter > 1)
			SurfaceUtils::SmoothSurface(tmpSurf, numSmoothIter);

		// combine non-linear deformation with affine deformation 
		// (large weight on affine deformation to ensure the mesh quality)
		if (affRatio < VECT_EPSILON) {
			for (size_t j = 0; j < pts.size(); ++j)
				pts[j] = tmp_pts[j];
		}
		else {
			affine(srcSurf, tmpSurf, srcSurf);
			for (size_t j = 0; j < pts.size(); ++j)
				pts[j] = pts[j] + (tmp_pts[j] - pts[j]) * (1 - affRatio);
		}
	}
}

void SurfaceRegistration::ComputeExternalForces(const Surface& srcSurf, const Surface& dstSurf, std::vector< vect3<float> >& ext_forces)
{
	NearestPointIndexer<float> nn1;
	nn1.Initialize(srcSurf.GetPoints());

	NearestPointIndexer<float> nn2;
	nn2.Initialize(dstSurf.GetPoints());

	// clear to zero
	for (size_t i = 0; i < ext_forces.size(); ++i)
	{
		for (int k = 0; k < 3; ++k)
			ext_forces[i][k] = 0;
	}

	std::vector<int> counts;
	counts.resize( ext_forces.size() );
	for (size_t i = 0; i < counts.size(); ++i)
		counts[i] = 0;

	// iterate all the target vertices
	const std::vector<float>& dst_pts = dstSurf.GetPoints();
	int dst_pt_num = static_cast<int>(dst_pts.size() / 3);

	for (int i = 0; i < dst_pt_num; ++i)
	{
		vect3<float> dst_pt( dst_pts[i * 3], dst_pts[i * 3 + 1], dst_pts[i * 3 + 2] ); 
		size_t src_pt_idx = nn1.SearchNearestNeighborIndex(dst_pt);

		vect3<float> src_pt = nn1.GetPoint(src_pt_idx);

		vect3<float> force = dst_pt - src_pt;
		const double distance_threshold = 1000;

		if (force.norm() < distance_threshold)
		{
			ext_forces[src_pt_idx] += force;
			++ counts[src_pt_idx];
		}
	}

	// compute mean external force & fill in the empty force
	const std::vector<float>& src_pts = srcSurf.GetPoints();

	for (size_t i = 0; i < ext_forces.size(); ++i)
	{
		if (counts[i] != 0) {
			if (ext_forces[i].norm() > VECT_EPSILON)
				ext_forces[i] = ext_forces[i] / ext_forces[i].norm();
		} else {
			// if there is no external force, find nearest target vertex for guiding the deformation
			vect3<float> src_pt(src_pts[i * 3], src_pts[i * 3 + 1], src_pts[i * 3 + 2]);
			vect3<float> nn_pt = nn2.SearchNearestNeighbor(src_pt);
			vect3<float> force = nn_pt - src_pt;
			if (force.norm() > VECT_EPSILON)
				force = force / force.norm();
			ext_forces[i] = force;
		}
	}

}

vect3<float> SurfaceRegistration::GetInternalOffset(Surface& surf, int idx)
{
	const std::vector<float>& pts = surf.GetPoints();
	const std::vector< std::vector<int> >& nb_info = surf.GetNeighborhood();

	vect3<float> currentPt(pts[idx * 3], pts[idx * 3 + 1], pts[idx * 3 + 2]);

	vect3<float> nb_center(0, 0, 0);
	for (size_t k = 0; k < nb_info[idx].size(); ++k)
	{
		int nb_idx = nb_info[idx][k];

		vect3<float> nb_pt(pts[nb_idx * 3], pts[nb_idx * 3 + 1], pts[nb_idx * 3 + 2]);
		nb_center += nb_pt;
	}
	nb_center /= nb_info[idx].size();

	// internal force to guide the vertex towards neighborhood center
	vect3<float> intf = nb_center - currentPt;
	return intf;
}

} } }

#endif