//
//  SurfaceFit.h
//  Fish
//
//  Created by Yaozong Gao on 3/5/14.
//

#ifndef __SurfaceFit_h__
#define __SurfaceFit_h__

#include "mesh/surface/SurfaceUtils.h"
#include "mesh/PDMTransform.h"
#include "common/mxImageUtils.h"
#include "deform/GVF.h"

#include <boost/numeric/ublas/matrix.hpp>

namespace BRIC { namespace IDEA { namespace FISH {

// parameters for binary image fitting
class BinaryImageFitParams
{
public:

	int padsize;			// pad size after bounding box crop for efficient GVF computation (by default 20)
	double smooth_sigma;	// sigma for image smoothing, set negative values to disable (by default -1)
	double gvf_mu;			// smoothing in GVF, by default 0.1, too large mu will cause divergence (by default 0.1)
	int gvf_iternum;		// iteration number in the GVF (by default 100)

	int dm_iternum;			// iteration number in the non-linear deformation (by default 4000)
	float dm_stepsize;		// step size in the non-linear deformation (by default 0.2)
	int dm_nbsize;			// neighbor size in neighborhood deformation (by default 4, the minimum should be >= 2)
	double dm_sigma;		// Gaussian sigma in neighborhood deformation (by default 0.6)
	double dm_smooth;		// the weight of internal smooth force (by default 1)
	double dm_aff;			// the weight of affine deformation (by default 0.99)

	BinaryImageFitParams(): padsize(20), smooth_sigma(-1), gvf_mu(0.1), gvf_iternum(100), 
		dm_iternum(4000), dm_stepsize(0.2), dm_nbsize(4), dm_sigma(0.6), dm_smooth(1), dm_aff(0.99) {}

	// rectum & bladder: dm_nbsize - 4, dm_smooth - 1
	// prostate: dm_nbsize - 3, dm_smooth - 0
};

// fitting a surface on binary image
class SurfaceFit
{
public:
	/** @brief fit a surface onto the binary image */
	template <typename T>
	static void FitBinaryImage(Surface& surf, const mxImage<T>& binaryImage, const BinaryImageFitParams& params);

	/** @brief fit a surface by using GVF force */
	static void Deform(Surface& surf, const boost::array< mxImage<double>, 3 >& gvf, const BinaryImageFitParams& params);
	
private:
	/** @brief non-linear deformation guided by GVF */
	static void GVFDeformBase(Surface& surf, const boost::array< mxImage<double>, 3>& gvf, float stepsize, int iternum, int nbsize, double sigma,
		double smoothRatio, double affRatio, int numSmoothIter);

	/** @brief return a unit directional vector from GVF field */
	static vect3<float> GetExternalOffset(const boost::array< mxImage<double>, 3>& gvf, vect3<float> vertPos);
	static vect3<float> GetInternalOffset(Surface& surf, int idx);
};

//////////////////////////////////////////////////////////////////////////

template <typename T>
void SurfaceFit::FitBinaryImage(Surface& surf, const mxImage<T>& binaryImage, const BinaryImageFitParams& params)
{
	// compute the bounding box for binary image (for the purpose of efficiency)
	vect3<int> box_min, box_max;
	mxImageUtils::BoundingBox(binaryImage, box_min, box_max);

	// align with the mass center of binary image
	vect3<double> center = mxImageUtils::MassCenter(binaryImage);
	vect3<float> centerf;
	for (int i = 0; i < 3; ++i)
		centerf[i] = static_cast<float>(center[i]);
	mxImageUtils::Voxel2World(binaryImage, centerf);
	surf.MoveTo(centerf);

	// compute the tightest bounding box
	std::vector<float>& pts = surf.GetPoints();
	size_t pt_num = pts.size() / 3;
	for (size_t i = 0; i < pt_num; ++i) {
		vect3<float> world_pt(pts[i * 3], pts[i * 3 + 1], pts[i * 3 + 2]);
		vect3<int> voxel_pt;
		mxImageUtils::World2Voxel(binaryImage, world_pt, voxel_pt);
		
		for (int k = 0; k < 3; ++k) {
			if (voxel_pt[k] < box_min[k])
				box_min[k] = voxel_pt[k];
			if (voxel_pt[k] > box_max[k])
				box_max[k] = voxel_pt[k];
		}
	}

	for (int k = 0; k < 3; ++k) {
		box_min[k] -= params.padsize;
		box_max[k] += params.padsize;
	}

	// crop the image
	mxImage<T> cropImage;
	mxImageUtils::Crop2(binaryImage, box_min, box_max, cropImage, static_cast<T>(0));

	// compute gvf
	boost::array< mxImage<double>, 3 > gvf;
	GVF::CreateGVF_BinaryImage(cropImage, gvf, params.smooth_sigma, params.gvf_mu, params.gvf_iternum);

	// gvf based surface deformation
	Deform(surf, gvf, params);
}

void SurfaceFit::Deform(Surface& surf, const boost::array< mxImage<double>, 3 >& gvf, const BinaryImageFitParams& params)
{
	// update neighborhoods
	surf.UpdateNeighbors();

	// hierarchical non-linear deformation 
	int surfSmoothIteration = 3;
	GVFDeformBase(surf, gvf, params.dm_stepsize, params.dm_iternum / 4, params.dm_nbsize, params.dm_sigma, params.dm_smooth, params.dm_aff, surfSmoothIteration);
	GVFDeformBase(surf, gvf, params.dm_stepsize, params.dm_iternum / 4, params.dm_nbsize, params.dm_sigma, params.dm_smooth * 0.75, params.dm_aff, surfSmoothIteration);
	GVFDeformBase(surf, gvf, params.dm_stepsize, params.dm_iternum / 4, params.dm_nbsize, params.dm_sigma, params.dm_smooth * 0.5, params.dm_aff, surfSmoothIteration);
	GVFDeformBase(surf, gvf, params.dm_stepsize, params.dm_iternum / 4, params.dm_nbsize, params.dm_sigma, params.dm_smooth * 0.25, params.dm_aff, surfSmoothIteration);
}

void SurfaceFit::GVFDeformBase(Surface& surf, const boost::array< mxImage<double>, 3 >& gvf, float stepsize, int iternum, int nbsize, double sigma,
	double smoothRatio, double affRatio, int numSmoothIter)
{
	std::vector<float>& pts = surf.GetPoints();
	std::vector< vect3<float> >& normals = surf.GetNormals();
	int pt_num = static_cast<int>(pts.size() / 3);

	std::vector< std::vector<unsigned int> > nbvert_idxs, nbvert_levels;
	nbvert_idxs.resize(pt_num);
	nbvert_levels.resize(pt_num);

	// compute neighborhood for all vertices
	for (int j = 0; j < pt_num; ++j)
		SurfaceUtils::GetVertexNeighborhood(surf, j, nbsize, nbvert_idxs[j], nbvert_levels[j]);

	// compute exponential table (for efficiency)
	std::vector<double> expTable;
	expTable.resize(nbsize + 1);
	if (nbsize == 0)
		expTable[0] = 1;
	else {
		for (size_t i = 0; i < expTable.size(); ++i) {
			double ratio = static_cast<double>(i) / static_cast<double>(nbsize);
			expTable[i] = exp(-ratio / sigma);
		}
	}

	Surface tmpSurf;
	tmpSurf.CopyFace(surf);

	std::vector<float>& tmp_pts = tmpSurf.GetPoints();
	tmp_pts.resize(pts.size());

	PDMAffineTransform affine;

	// surface deformation
	for (int i = 0; i < iternum; ++i)
	{
		// clear to zero
		for (size_t k = 0; k < tmp_pts.size(); ++k)
			tmp_pts[k] = 0;

		surf.UpdateNormals();

		for (int j = 0; j < pt_num; ++j)
		{
			vect3<float> vertPos(pts[j * 3], pts[j * 3 + 1], pts[j * 3 + 2]);
			vect3<float> extf = GetExternalOffset(gvf, vertPos);
			extf = normals[j] * vect3<float>::dot_product(extf, normals[j]) * stepsize;

			vect3<float> intf = GetInternalOffset(surf, j);
			intf = intf - normals[j] * vect3<float>::dot_product(intf, normals[j]);
			intf = intf * stepsize * smoothRatio;

			vect3<float> combf = (extf + intf) / 2;

			for (size_t k = 0; k < nbvert_idxs[j].size(); ++k)
			{
				unsigned int idx = nbvert_idxs[j][k];
				unsigned int level = nbvert_levels[j][k];

				double w = expTable[level];
				tmp_pts[idx * 3] += combf[0] * w;
				tmp_pts[idx * 3 + 1] += combf[1] * w;
				tmp_pts[idx * 3 + 2] += combf[2] * w;
			}
		}

		// non-linear deformation 
		for (size_t j = 0; j < pts.size(); ++j)
			tmp_pts[j] = pts[j] + tmp_pts[j];

		// smooth a little bit for the non-linear deformed surface
		if (numSmoothIter > 1)
			SurfaceUtils::SmoothSurface(tmpSurf, numSmoothIter);

		// combine non-linear deformation with affine deformation 
		// (large weight on affine deformation to ensure the mesh quality)
		if (affRatio < VECT_EPSILON) {
			for (size_t j = 0; j < pts.size(); ++j)
				pts[j] = tmp_pts[j];
		} else {
			affine(surf, tmpSurf, surf);
			for (size_t j = 0; j < pts.size(); ++j)
				pts[j] = pts[j] + (tmp_pts[j] - pts[j]) * (1 - affRatio);
		}
	}
}

vect3<float> SurfaceFit::GetExternalOffset(const boost::array< mxImage<double>, 3>& gvf, vect3<float> vertPos)
{
	// get external force from gvf
	vect3<int> voxel_pt;
	mxImageUtils::World2Voxel(gvf[0], vertPos, voxel_pt);
	if (!gvf[0].PtInImage(voxel_pt[0], voxel_pt[1], voxel_pt[2])) {
		std::cerr << "vertices move outside the image" << std::endl;
		exit(-1);
	}

	vect3<double> ext;
	for (int k = 0; k < 3; ++k)
		ext[k] = gvf[k](voxel_pt[0], voxel_pt[1], voxel_pt[2]);

	vect3<double> spacing = gvf[0].GetSpacing();
	const double* axis = gvf[0].GetAxis();

	// transfer from voxel direction to world direction
	vect3<double> x_vec, y_vec, z_vec;
	x_vec[0] = axis[0] * ext[0]; x_vec[1] = axis[1] * ext[0]; x_vec[2] = axis[2] * ext[0];
	y_vec[0] = axis[3] * ext[1]; y_vec[1] = axis[4] * ext[1]; y_vec[2] = axis[5] * ext[1];
	z_vec[0] = axis[6] * ext[2]; z_vec[1] = axis[7] * ext[2]; z_vec[2] = axis[8] * ext[2];
	ext = x_vec * spacing[0] + y_vec * spacing[1] + z_vec * spacing[2];

	// normalized to unit vector
	double mag = ext.norm();
	if (std::abs(mag) > VECT_EPSILON)
		ext /= mag;

	vect3<float> extf(static_cast<float>(ext[0]), static_cast<float>(ext[1]), static_cast<float>(ext[2]));
	return extf;
}

vect3<float> SurfaceFit::GetInternalOffset(Surface& surf, int idx)
{
	const std::vector<float>& pts = surf.GetPoints();
	const std::vector< std::vector<int> >& nb_info = surf.GetNeighborhood();

	vect3<float> currentPt( pts[idx*3], pts[idx*3+1], pts[idx*3+2] );

	vect3<float> nb_center(0, 0, 0);
	for (size_t k = 0; k < nb_info[idx].size(); ++k)
	{
		int nb_idx = nb_info[idx][k];

		vect3<float> nb_pt( pts[nb_idx * 3], pts[nb_idx * 3 + 1], pts[nb_idx * 3 + 2] );
		nb_center += nb_pt;
	}
	nb_center /= nb_info[idx].size();

	// internal force to guide the vertex towards neighborhood center
	vect3<float> intf = nb_center - currentPt;
	return intf;
}

} } }

#endif