//
//  SimpleDM.h (Simple Deformable Model)
//  FISH
//
//  Created by Yaozong Gao on 04/03/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __SimpleDM_h__
#define __SimpleDM_h__

#include "deform/interfaces.h"

namespace BRIC { namespace IDEA { namespace FISH {

struct SimpleDM_params {
	unsigned int iter_num;
	float search_range;
	float step_size;
	float tangent_weight;
};

class SimpleDM
{
public:
	static void DeformSurface(IDeformContext* context, const SimpleDM_params& params, Surface& surf);
};

//////////////////////////////////////////////////////////////////////////

void SimpleDM::DeformSurface(IDeformContext* context, const SimpleDM_params& params, Surface& surf)
{
	surf.UpdateNeighbors();

	std::vector<float>& pts = surf.GetPoints();
	int pt_num = static_cast<int>(pts.size() / 3);

	for (unsigned int i = 0; i < params.iter_num; ++i)
	{
		surf.UpdateNormals();

		for (int j = 0; j < pt_num; ++j)
		{
			vect3<float> offset = context->GetVertexOffset(surf, j, params.search_range);

			vect3<float> internal_force = surf.GetNeighborCenter(j);
			for (int k = 0; k < 3; ++k)
				internal_force[k] = internal_force[k] - pts[j * 3 + k];

			for (int k = 0; k < 3; ++k)
				pts[j * 3 + k] += ( (offset[k] + internal_force[k] * params.tangent_weight) * params.step_size );
		}

		context->RefineShape(surf, surf);
	}
}

} } }

#endif