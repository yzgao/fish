//
//  BDModel.h
//  FISH
//
//  Created by Yaozong Gao on 02/13/15.
//  Copyright (c) 2015 yzgao@cs.unc.edu. All rights reserved.
//


#ifndef __BDModel_h__
#define __BDModel_h__


#include "common/stringUtils.h"
#include "deform/SimpleDM.h"
#include "deform/deformcontext/BinaryRegionDeformContext.h"
#include "deform/shapecontext/LinearShapeContext.h"
#include "deform/shapecontext/FreeShapeContext.h"
#include "deform/deformcontext/BinaryRegionDeformContext.h"
#include "mesh/PDMTransform.h"

namespace BRIC { namespace IDEA { namespace FISH {

// BDModel Input Parameters

// BDModel step deformation types
enum BDModel_deform_type
{
	BD_INIT = 0,
	BD_CENTER,
	BD_TRANSLATION,
	BD_RIGID,
	BD_SCALE,
	BD_DILATE,
	BD_AFFINE,
	BD_NONRIGID,
	BD_FREE,
	BD_AFFINE2
};

// BDModel steps
class BDModel_step
{
public:

	BDModel_deform_type type;		// deform type
	int iter;						// iteration number

	// free deformation on classification maps
	float search_range;				
	float stepsize;				
	float tangent_weight;
	int smooth_iter;
	int remesh_iter;

	// free and scale deformation
	double min_val;
	double max_val;
};


// BDModel configuration file
class BDModel_config
{
public:
	
	bool parse(const char* path);
	static bool write_default(const char* path);

public:

	std::vector<BDModel_step> m_steps;
};


// BDModel deformation
class BDModel
{
public:

	static void deform(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_config& config, Surface& surf);

private:

	static vect3<double> get_disp_vector(const mxImage<double>* disp_map, const vect3<float>& vert_world);

	static void translate_deform(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_step& step, Surface& surf);
	static void rigid_deform(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_step& step, Surface& surf);
	static void scale_deform(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_step& step, Surface& surf);
	static void dilate_deform(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_step& step, Surface& surf);
	static void affine_deform(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_step& step, Surface& surf);
	static void nonrigid_deform(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_step& step, Surface& surf);

	static void initialize(const mxImage<double>& prob_map, const mxImage<double>* disp_map, Surface& surf, float scale = 1.0f);
	static void move_to_center(const mxImage<double>& prob_map, const mxImage<double>* disp_map, Surface& surf);

	// free deform on label map
	static void free_deform(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_step& step, Surface& surf);
	static void affine_deform2(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_step& step, Surface& surf);
};



//////////////////////////////////////////////////////////////////////////
// implementation


bool BDModel_config::parse(const char* path)
{
	boost::property_tree::ptree pt;
	try
	{
		boost::property_tree::ini_parser::read_ini(path, pt);
	}
	catch (boost::property_tree::ini_parser_error& error)
	{
		std::cerr << error.what() << std::endl;
		return false;
	}

	try {
		int step_number = pt.get<int>("StepNumber");
		if (step_number > 0)
			m_steps.resize(step_number);

		for (int i = 0; i < step_number; ++i)
		{
			std::string section_name = std::string("Step") + stringUtils::num2str(i+1);
			std::string str_type = pt.get<std::string>(section_name + ".type");

			if (str_type == "initialize")
				m_steps[i].type = BD_INIT;
			else if (str_type == "center")
				m_steps[i].type = BD_CENTER;
			else if (str_type == "translation")
				m_steps[i].type = BD_TRANSLATION;
			else if (str_type == "rigid")
				m_steps[i].type = BD_RIGID;
			else if (str_type == "scale")
				m_steps[i].type = BD_SCALE;
			else if (str_type == "dilate")
				m_steps[i].type = BD_DILATE;
			else if (str_type == "affine")
				m_steps[i].type = BD_AFFINE;
			else if (str_type == "nonrigid")
				m_steps[i].type = BD_NONRIGID;
			else if (str_type == "free")
				m_steps[i].type = BD_FREE;
			else if (str_type == "affine2")
				m_steps[i].type = BD_AFFINE2;
			else
				err_message("unrecognized deformation type");


			boost::optional<int> v;
		
			switch (m_steps[i].type)
			{
			case BD_INIT:

				m_steps[i].stepsize = pt.get<float>(section_name + ".scale");
				break;

			case BD_CENTER:

				break;

			case BD_TRANSLATION:

				m_steps[i].stepsize = pt.get<float>(section_name + ".stepsize");
				m_steps[i].iter = pt.get<int>(section_name + ".iter");
				break;

			case BD_RIGID:

				m_steps[i].stepsize = pt.get<float>(section_name + ".stepsize");
				m_steps[i].iter = pt.get<int>(section_name + ".iter");
				break;

			case BD_SCALE:

				m_steps[i].stepsize = pt.get<float>(section_name + ".stepsize");
				m_steps[i].min_val = pt.get<float>(section_name + ".min_val");
				m_steps[i].max_val = pt.get<float>(section_name + ".max_val");
				break;

			case BD_DILATE:

				m_steps[i].iter = pt.get<float>(section_name + ".iter");
				break;

			case BD_AFFINE:

				m_steps[i].stepsize = pt.get<float>(section_name + ".stepsize");
				m_steps[i].iter = pt.get<int>(section_name + ".iter");
				break;

			case BD_NONRIGID:

				m_steps[i].stepsize = pt.get<float>(section_name + ".stepsize");
				m_steps[i].iter = pt.get<int>(section_name + ".iter");
				m_steps[i].tangent_weight = pt.get<float>(section_name + ".tangent_weight");
				m_steps[i].smooth_iter = pt.get<int>(section_name + ".smooth_iter");
				
				v = pt.get_optional<int>(section_name + ".remesh_iter");
				if (v)
					m_steps[i].remesh_iter = v.get();
				else
					m_steps[i].remesh_iter = 100;

				break;

			case BD_FREE:

				m_steps[i].stepsize = pt.get<float>(section_name + ".stepsize");
				m_steps[i].iter = pt.get<int>(section_name + ".iter");
				m_steps[i].search_range = pt.get<float>(section_name + ".search_range");
				m_steps[i].tangent_weight = pt.get<float>(section_name + ".tangent_weight");
				m_steps[i].smooth_iter = pt.get<int>(section_name + ".smooth_iter");
				m_steps[i].min_val = pt.get<double>(section_name + ".min_val");
				m_steps[i].max_val = pt.get<double>(section_name + ".max_val");
				break;

			case BD_AFFINE2:

				m_steps[i].stepsize = pt.get<float>(section_name + ".stepsize");
				m_steps[i].iter = pt.get<int>(section_name + ".iter");
				m_steps[i].search_range = pt.get<float>(section_name + ".search_range");
				m_steps[i].tangent_weight = pt.get<float>(section_name + ".tangent_weight");
				m_steps[i].min_val = pt.get<double>(section_name + ".min_val");
				m_steps[i].max_val = pt.get<double>(section_name + ".max_val");
				break;

			default:
				err_message("unrecognized deformation type");
			}

		}

	}
	catch (boost::property_tree::ptree_error& error)
	{
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}

bool BDModel_config::write_default(const char* path)
{
	FILE* fp = fopen(path, "w");

	fprintf(fp, "StepNumber=4\n");
	fprintf(fp, "\n");
	fprintf(fp, "[Step1]\n");
	fprintf(fp, "type=translation\n");
	fprintf(fp, "stepsize=1\n");
	fprintf(fp, "iter=10\n");
	fprintf(fp, "\n");
	fprintf(fp, "[Step2]\n");
	fprintf(fp, "type=rigid\n");
	fprintf(fp, "stepsize=0.5\n");
	fprintf(fp, "iter=10\n");
	fprintf(fp, "\n");
	fprintf(fp, "[Step3]\n");
	fprintf(fp, "type=scale\n");
	fprintf(fp, "min_val=0.5\n");
	fprintf(fp, "max_val=2\n");
	fprintf(fp, "stepsize=0.25\n");
	fprintf(fp, "\n");
	fprintf(fp, "[Step4]\n");
	fprintf(fp, "type=affine\n");
	fprintf(fp, "stepsize=0.5\n");
	fprintf(fp, "iter=10");
	fprintf(fp, "\n");
	fprintf(fp, "[Step5]\n");
	fprintf(fp, "type=free\n");
	fprintf(fp, "iter=10\n");
	fprintf(fp, "search_range=20\n");
	fprintf(fp, "stepsize=0.2\n");
	fprintf(fp, "tangent_weight=0.5\n");
	fprintf(fp, "smooth_iter=10\n");
	fprintf(fp, "min_val=0\n");
	fprintf(fp, "max_val=1\n");

	fclose(fp);

	return true;
}

void BDModel::deform(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_config& config, Surface& surf)
{
	for (int i = 0; i < 3; ++i)
		assert_message( prob_map.GetImageSize() == disp_map[i].GetImageSize(), "the same image size of prob_map and disp_map");

	for (size_t i = 0; i < config.m_steps.size(); ++i)
	{
		BDModel_deform_type type = config.m_steps[i].type;
		int iter = config.m_steps[i].iter;

		if (type == BD_INIT)
			initialize(prob_map, disp_map, surf, config.m_steps[i].stepsize);
		else if (type == BD_CENTER)
			move_to_center(prob_map, disp_map, surf);
		else if (type == BD_TRANSLATION)
			translate_deform(prob_map, disp_map, config.m_steps[i], surf);
		else if (type == BD_RIGID)
			rigid_deform(prob_map, disp_map, config.m_steps[i], surf);
		else if (type == BD_SCALE)
			scale_deform(prob_map, disp_map, config.m_steps[i], surf);
		else if (type == BD_DILATE)
			dilate_deform(prob_map, disp_map, config.m_steps[i], surf);
		else if (type == BD_AFFINE)
			affine_deform(prob_map, disp_map, config.m_steps[i], surf);
		else if (type == BD_NONRIGID)
			nonrigid_deform(prob_map, disp_map, config.m_steps[i], surf);
		else if (type == BD_FREE)
			free_deform(prob_map, disp_map, config.m_steps[i], surf);
		else if (type == BD_AFFINE2)
			affine_deform2(prob_map, disp_map, config.m_steps[i], surf);
		else
			err_message("unrecognized deformation type");
	}
}

vect3<double> BDModel::get_disp_vector(const mxImage<double>* disp_map, const vect3<float>& vert_world)
{
	vect3<double> offset(0, 0, 0);

	// get voxel position on displacement map
	vect3<double> voxel_pt;
	mxImageUtils::World2Voxel(disp_map[0], vert_world, voxel_pt);

	vect3<unsigned int> image_size = disp_map[0].GetImageSize();

	// check voxel inside the image
	if (voxel_pt[0] < 0 || voxel_pt[0] >= static_cast<float>(image_size[0]) || voxel_pt[1] < 0 || voxel_pt[1] >= static_cast<float>(image_size[1]) ||
		voxel_pt[2] < 0 || voxel_pt[2] >= static_cast<float>(image_size[2]))
	{
		return offset;
	}

	ImageLinearInterpolator<double, double> interp;
	offset[0] = interp.Get(disp_map[0], voxel_pt);
	offset[1] = interp.Get(disp_map[1], voxel_pt);
	offset[2] = interp.Get(disp_map[2], voxel_pt);

	return offset;
}

void BDModel::translate_deform(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_step& step, Surface& surf)
{
	vect3<unsigned int> image_size = disp_map[0].GetImageSize();

	for (int i = 0; i < step.iter; ++i)
	{
		unsigned int count = 0;

		const std::vector<float>& verts = surf.GetPoints();
		int vert_num = static_cast<int>( verts.size() / 3 );

		vect3<double> translation(0, 0, 0);
		for (int j = 0; j < vert_num; ++j)
		{
			vect3<float> vert( verts[j * 3], verts[j * 3 + 1], verts[j * 3 + 2] );
			vect3<double> disp = get_disp_vector(disp_map, vert);

			++count;
			for (int k = 0; k < 3; ++k)
				translation[k] += disp[k] * step.stepsize;
		}
		translation /= count;
		surf.Translate(translation.to<float>());
	}
}

void BDModel::rigid_deform(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_step& step, Surface& surf)
{
	for (int i = 0; i < step.iter; ++i)
	{
		// surface before deformation
		Surface tmp_surf = surf;

		std::vector<float>& verts = tmp_surf.GetPoints();
		int vert_num = static_cast<int>(verts.size() / 3);

		for (int j = 0; j < vert_num; ++j)
		{
			vect3<float> vert(verts[j * 3], verts[j * 3 + 1], verts[j * 3 + 2]);
			vect3<double> disp = get_disp_vector(disp_map, vert);

			for (int k = 0; k < 3; ++k)
				verts[j * 3 + k] += disp[k] * step.stepsize;
		}

		PDMRigidTransform transform;
		transform(surf, tmp_surf, surf);
	}
}

void BDModel::scale_deform(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_step& step, Surface& surf)
{
	std::vector<double> prob_sums;

	for (double scale_ratio = step.min_val; scale_ratio < (step.max_val + 1e-6); scale_ratio += step.stepsize)
	{
		vect3<float> scale( static_cast<float>(scale_ratio), static_cast<float>(scale_ratio), static_cast<float>(scale_ratio) );

		Surface tmp_surf = surf;
		tmp_surf.Scale(scale);

		mxImage<unsigned char> mask;
		unsigned char outside_val = 0, inside_val = 255;
		SurfaceUtils::CarveSurface(tmp_surf, prob_map, outside_val, inside_val, mask);

		vect3<unsigned int> image_size = mask.GetImageSize();
		double sum = 0;

		for (unsigned int z = 0; z < image_size[2]; ++z) {
			for (unsigned int y = 0; y < image_size[1]; ++y) {
				for (unsigned int x = 0; x < image_size[0]; ++x) {
					if (mask(x, y, z) > 0)
						sum += prob_map(x, y, z);
				}
			}
		}

		prob_sums.push_back(sum);
	}

	double prev_diff = 0;
	int scale_idx = -1;

	for (size_t i = 1; i < prob_sums.size(); ++i)
	{
		double current_diff = prob_sums[i] - prob_sums[i - 1];
		if (i == 1)
			prev_diff = current_diff;
		else
		{
			if (current_diff < prev_diff) {
				scale_idx = static_cast<int>(i);
				break;
			}
			prev_diff = current_diff;
		}
	}

	vect3<float> final_scale_ratio;
	if (scale_idx == -1)
		final_scale_ratio = vect3<float>(static_cast<float>(step.max_val), static_cast<float>(step.max_val), static_cast<float>(step.max_val));
	else {
		float tmp = static_cast<float>( step.min_val + step.stepsize * scale_idx );
		final_scale_ratio = vect3<float>(tmp, tmp, tmp);
	}

	surf.Scale(final_scale_ratio);
}

void BDModel::dilate_deform(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_step& step, Surface& surf)
{
	vect3<double> spacing = prob_map.GetSpacing();
	surf.UpdateNeighbors();

	double total = spacing[0] * step.iter;
	for (double v = 0; v <= total; v += 1)
	{
		surf.UpdateNormals();

		std::vector<float>& verts = surf.GetPoints();
		std::vector< vect3<float> >& normals = surf.GetNormals();
		int vert_num = static_cast<int>(verts.size() / 3);

		for (int j = 0; j < vert_num; ++j)
		{
			vect3<float> vert(verts[j * 3], verts[j * 3 + 1], verts[j * 3 + 2]);

			// internal force
			vect3<float> internal_force = surf.GetNeighborCenter(j);
			for (int k = 0; k < 3; ++k)
				internal_force[k] = internal_force[k] - vert[k];
			internal_force = internal_force - normals[j] * vect3<float>::dot_product(internal_force, normals[j]);

			// combine external and internal
			vert = vert + ((normals[j] + internal_force * step.tangent_weight));

			verts[j * 3] = vert[0];
			verts[j * 3 + 1] = vert[1];
			verts[j * 3 + 2] = vert[2];
		}

		// smooth surface
		if (step.smooth_iter > 0)
			SurfaceUtils::SmoothSurface(surf, step.smooth_iter);

		// re-mesh surface
		float remesh_stepsize = 0.3f;
		int remesh_iter = 100;
		SurfaceUtils::Remesh(surf, remesh_stepsize, remesh_iter);
	}

}

void BDModel::affine_deform(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_step& step, Surface& surf)
{
	for (int i = 0; i < step.iter; ++i)
	{
		Surface tmp_surf = surf;

		std::vector<float>& verts = tmp_surf.GetPoints();
		int vert_num = static_cast<int>(verts.size() / 3);

		for (int j = 0; j < vert_num; ++j)
		{
			vect3<float> vert(verts[j * 3], verts[j * 3 + 1], verts[j * 3 + 2]);
			vect3<double> disp = get_disp_vector(disp_map, vert);

			for (int k = 0; k < 3; ++k)
				verts[j * 3 + k] += disp[k] * step.stepsize;
		}

		PDMAffineTransform transform;
		transform(surf, tmp_surf, surf);
	}
}

void BDModel::nonrigid_deform(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_step& step, Surface& surf)
{
	surf.UpdateNeighbors();
	surf.UpdateFaceNeighbors();

	for (int i = 0; i < step.iter; ++i)
	{
		Surface tmp_surf = surf;

		// update normals
		tmp_surf.UpdateNormals();

		std::vector<float>& verts = tmp_surf.GetPoints();
		std::vector< vect3<float> >& normals = tmp_surf.GetNormals();
		int vert_num = static_cast<int>(verts.size() / 3);

		for (int j = 0; j < vert_num; ++j)
		{
			vect3<float> vert(verts[j * 3], verts[j * 3 + 1], verts[j * 3 + 2]);
			vect3<double> disp = get_disp_vector(disp_map, vert);

			// internal force
			vect3<float> internal_force = surf.GetNeighborCenter(j);
			for (int k = 0; k < 3; ++k)
				internal_force[k] = internal_force[k] - vert[k];
			internal_force = internal_force - normals[j] * vect3<float>::dot_product(internal_force, normals[j]);

			// combine external and internal force
			for (int k = 0; k < 3; ++k)
				verts[j * 3 + k] = verts[j * 3 + k] + ((disp[k] + internal_force[k] * step.tangent_weight) * step.stepsize);

			// check collision
			//if (!tmp_surf.VertexCollisonTest(j, dst_pt))
			//{
			//	verts[j * 3] = dst_pt[0];  verts[j * 3 + 1] = dst_pt[1];  verts[j * 3 + 2] = dst_pt[2];
			//}
		}

		surf = tmp_surf;

		// smooth surface
		if (step.smooth_iter > 0)
			SurfaceUtils::SmoothSurface(surf, step.smooth_iter);

		// re-mesh surface
		float remesh_stepsize = 0.3f;
		SurfaceUtils::Remesh(surf, remesh_stepsize, step.remesh_iter);
	}
}

void BDModel::free_deform(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_step& step, Surface& surf)
{
	FreeShapeContext shape_context(step.smooth_iter);
	BinaryRegionDeformContext<double> deform_context(prob_map, &shape_context);
		
	SimpleDM_params deform_params;
	deform_params.iter_num = step.iter;
	deform_params.search_range = step.search_range;
	deform_params.step_size = step.stepsize;
	deform_params.tangent_weight = step.tangent_weight;

	deform_context.SetBoundaryCriteria(step.min_val, step.max_val, -1);
	SimpleDM::DeformSurface(&deform_context, deform_params, surf);
}

void BDModel::affine_deform2(const mxImage<double>& prob_map, const mxImage<double>* disp_map, const BDModel_step& step, Surface& surf)
{
	LinearShapeContext<PDMAffineTransform> shape_context(surf);
	BinaryRegionDeformContext<double> deform_context(prob_map, &shape_context);

	SimpleDM_params deform_params;
	deform_params.iter_num = step.iter;
	deform_params.search_range = step.search_range;
	deform_params.step_size = step.stepsize;
	deform_params.tangent_weight = step.tangent_weight;

	deform_context.SetBoundaryCriteria(step.min_val, step.max_val, -1);
	SimpleDM::DeformSurface(&deform_context, deform_params, surf);
}

void BDModel::initialize(const mxImage<double>& prob_map, const mxImage<double>* disp_map, Surface& surf, float obj_scale)
{
	mxImage<unsigned char> mask_image;

	double threshold = 0.5;
	unsigned char inside_value = 255, outside_value = 0;
	mxImageUtils::Threshold(prob_map, mask_image, threshold, inside_value, outside_value);
	mxImageUtils::PickLargestComponent(mask_image);

	vect3<int> sp, ep;
	mxImageUtils::BoundingBox(mask_image, sp, ep);

	vect3<float> scale, center;
	vect3<double> spacing = mask_image.GetSpacing();

	for (int i = 0; i < 3; ++i)
	{
		scale[i] = static_cast<float>((ep[i] - sp[i] + 1) * spacing[i]);
		center[i] = (sp[i] + ep[i]) / 2;
	}
	mxImageUtils::Voxel2World(prob_map, center);

	surf.MoveTo(center);

	vect3<float> orig_scale = surf.Size();
	vect3<float> ratio;
	for (int i = 0; i < 3; ++i)
		ratio[i] = scale[i] / orig_scale[i] * obj_scale;

	surf.Scale(ratio);
}

void BDModel::move_to_center(const mxImage<double>& prob_map, const mxImage<double>* disp_map, Surface& surf)
{
	vect3<double> center = mxImageUtils::WorldCenter(prob_map);
	vect3<float> centerf( static_cast<float>(center[0]), static_cast<float>(center[1]), static_cast<float>(center[2]) );

	surf.MoveTo( centerf );
}

} } }


#endif