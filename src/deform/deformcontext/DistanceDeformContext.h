//
//  DistanceDeformContext.h
//  FISH
//
//  Created by Yaozong Gao on 10/10/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __DistanceDeformContext_h__
#define __DistanceDeformContext_h__

#include "deform/interfaces.h"
#include "common/mxImage.h"

namespace BRIC { namespace IDEA { namespace FISH {


class DistanceDeformContext: public IDeformContext
{
public:
	DistanceDeformContext(const mxImage<double>& distImage, IShapeContext* shapeContext, double distThreshold);
	virtual vect3<float> GetVertexOffset(const Surface& surf, unsigned int vertIdx, double searchRange);
	virtual void RefineShape( const PDM& pdm, PDM& refinedPDM );

	// debug function
	virtual void SaveSurfaceToImage(const Surface& surf, const char* path) {
		mxImage<unsigned char> image;
		SurfaceUtils::CarveSurface(surf,*m_distImage, static_cast<unsigned char>(0), static_cast<unsigned char>(255), image);
		ImageHelper::WriteImage<unsigned char, unsigned char>(image, path);
	}

private:
	const mxImage<double>* m_distImage;
	IShapeContext* m_shapeContext;
	double m_distThreshold;
};

//////////////////////////////////////////////////////////////////////////

DistanceDeformContext::DistanceDeformContext(const mxImage<double>& distImage, IShapeContext* shapeContext, double distThreshold)
{
	m_distImage = &distImage;
	m_shapeContext = shapeContext;
	m_distThreshold = distThreshold;
}

void DistanceDeformContext::RefineShape(const PDM& pdm, PDM& refinedPDM)
{
	m_shapeContext->RefineShape(pdm, refinedPDM);
}

vect3<float> DistanceDeformContext::GetVertexOffset(const Surface& surf, unsigned int vertIdx, double searchRange)
{
	const std::vector< vect3<float> >& normals = surf.GetNormals();
	const std::vector< float >& verts = surf.GetPoints();
	if (normals.size() == 0) { std::cerr << "compute normals before call deformable segmentation" << std::endl; exit(-1); }

	vect3<float> vertexNormal = normals[vertIdx];
	vect3<float> vert( verts[vertIdx*3], verts[vertIdx*3+1], verts[vertIdx*3+2] );

	double stepsize = m_distImage->GetSpacing()[0] / 2;									
	int steps = static_cast<int>(searchRange/stepsize+0.5);

	double minDist = std::numeric_limits<double>::max();
	int optimalStep = 0;
	for (int i = -steps; i <= steps; ++i)
	{
		vect3<float> point = vert + vertexNormal * i * stepsize;
		vect3<int> voxel;
		mxImageUtils::World2Voxel(*m_distImage, point, voxel);

		if (!m_distImage->PtInImage(voxel[0],voxel[1],voxel[2]))
			continue; 

		double dist = (*m_distImage)(voxel[0],voxel[1],voxel[2]);
		if (dist < m_distThreshold && dist < minDist) {
			minDist = dist;
			optimalStep = i;
		}
	}

	vect3<float> ret = vertexNormal * optimalStep;
	return ret;
}


} } }

#endif