//
//  ValleyDeformContext.h
//  FISH
//
//  Created by Yaozong Gao on 4/4/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __ValleyDeformContext_h__
#define __ValleyDeformContext_h__

#include "deform/interfaces.h"
#include "common/mxImage.h"

namespace BRIC { namespace IDEA { namespace FISH {

class ValleyDeformContext: public IDeformContext
{
public:
	ValleyDeformContext(const mxImage<double>& image, vect3<unsigned int> patchsize, IShapeContext* shapeContext);
	virtual vect3<float> GetVertexOffset(const Surface& surf, unsigned int vertIdx, double searchRange);
	virtual void RefineShape(const PDM& pdm, PDM& refinedPDM);

private:

	double GetMeanValue(const vect3<int>& voxel);

	const mxImage<double>* m_image;
	vect3<unsigned int> m_patchsize;
	IShapeContext* m_shapecontext;
};

//////////////////////////////////////////////////////////////////////////

ValleyDeformContext::ValleyDeformContext(const mxImage<double>& image, vect3<unsigned int> patchsize, IShapeContext* shapeContext) 
{
	m_image = &image; 
	m_patchsize = patchsize;
	m_shapecontext = shapeContext;
}

double ValleyDeformContext::GetMeanValue(const vect3<int>& voxel)
{
	vect3<unsigned int> imagesize = m_image->GetImageSize();

	vect3<int> sp, ep;
	for (int i = 0; i < 3; ++i)
	{
		sp[i] = voxel[i] - static_cast<int>(m_patchsize[i] / 2);
		ep[i] = sp[i] + static_cast<int>(m_patchsize[i]) - 1;

		if (sp[i] < 0)
			sp[i] = 0;
		if (ep[i] >= static_cast<int>(imagesize[i]))
			ep[i] = static_cast<int>(imagesize[i]-1);
	}

	double mean = 0;
	int count = 0;
	for (int z = sp[2]; z <= ep[2]; ++z) {
		for (int y = sp[1]; y <= ep[1]; ++y) {
			for (int x = sp[0]; x <= ep[0]; ++x) {
				mean += std::abs( (*m_image)(x,y,z) );
				++ count;
			}
		}
	}

	if (count == 0) {
		std::cerr << "empty count for mean computation?" << std::endl; exit(-1);
	}

	return mean / count;
}

vect3<float> ValleyDeformContext::GetVertexOffset(const Surface& surf, unsigned int vertIdx, double searchRange)
{
	const std::vector< vect3<float> >& normals = surf.GetNormals();
	const std::vector< float >& verts = surf.GetPoints();
	if (normals.size() == 0) { std::cerr << "compute normals before call deformable segmentation" << std::endl; exit(-1); }

	vect3<float> vertexNormal = normals[vertIdx];
	vect3<float> vert(verts[vertIdx * 3], verts[vertIdx * 3 + 1], verts[vertIdx * 3 + 2]);

	double stepsize = m_image->GetSpacing()[0] / 2;
	int steps = static_cast<int>(searchRange / stepsize + 0.5);

	double minDist = std::numeric_limits<double>::max();
	int optimalStep = 0;
	for (int i = -steps; i <= steps; ++i)
	{
		vect3<float> point = vert + vertexNormal * i * stepsize;
		vect3<int> voxel;
		mxImageUtils::World2Voxel(*m_image, point, voxel);

		if (!m_image->PtInImage(voxel[0], voxel[1], voxel[2]))
			continue;

		double dist = GetMeanValue(voxel);
		if (dist < minDist) {
			minDist = dist;
			optimalStep = i;
		}
	}

	vect3<float> ret = vertexNormal * optimalStep * stepsize;
	return ret;
}

void ValleyDeformContext::RefineShape(const PDM& pdm, PDM& refinedPDM) {
	m_shapecontext->RefineShape(pdm, refinedPDM);
}



} } }


#endif