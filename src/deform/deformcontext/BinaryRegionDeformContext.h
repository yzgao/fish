//
//  BinaryRegionDeformContext.h
//  FISH
//
//  Created by Yaozong Gao on 10/10/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __BinaryRegionDeformContext_h__
#define __BinaryRegionDeformContext_h__

#include "deform/interfaces.h"
#include "common/mxImageUtils.h"
#include "mesh/surface/SurfaceUtils.h"
#include "extern/ImageHelper.h"


namespace BRIC { namespace IDEA { namespace FISH {

template <typename T>
class BinaryRegionDeformContext: public IDeformContext
{
public:
	BinaryRegionDeformContext(const mxImage<T>& responseImage, IShapeContext* shapeContext);
	void SetBoundaryCriteria(T minResponse, T maxResponse, double minGrad);

	virtual vect3<float> GetVertexOffset(const Surface& surf, unsigned int vertIdx, double searchRange);
	virtual void RefineShape( const PDM& pdm, PDM& refinedPDM );

	// debug function
	virtual void SaveSurfaceToImage(const Surface& surf, const char* path) {
		mxImage<unsigned char> image;
		SurfaceUtils::CarveSurface(surf,*m_responseImage, static_cast<unsigned char>(0), static_cast<unsigned char>(255), image);
		ImageHelper::WriteImage<unsigned char, unsigned char>(image, path);
	}

private:
	const mxImage<T>* m_responseImage;
	IShapeContext* m_shapeContext;

	T m_minResponse;
	T m_maxResponse;
	double m_minGrad;
};

//////////////////////////////////////////////////////////////////////////

template <typename T>
BinaryRegionDeformContext<T>::BinaryRegionDeformContext(const mxImage<T>& responseImage, IShapeContext* shapeContext)
{
	m_responseImage = &responseImage;
	m_shapeContext = shapeContext;
}

template <typename T>
void BinaryRegionDeformContext<T>::SetBoundaryCriteria(T minResponse, T maxResponse, double minGrad)
{
	m_minResponse = minResponse;
	m_maxResponse = maxResponse;
	m_minGrad = minGrad;
}

template <typename T>
void BinaryRegionDeformContext<T>::RefineShape(const PDM& pdm, PDM& refinedPDM)
{
	m_shapeContext->RefineShape(pdm, refinedPDM);
}

template <typename T>
vect3<float> BinaryRegionDeformContext<T>::GetVertexOffset(const Surface& surf, unsigned int vertIdx, double searchRange)
{
	const std::vector< vect3<float> >& normals = surf.GetNormals();
	const std::vector< float >& verts = surf.GetPoints();
	if (normals.size() == 0) { std::cerr << "compute normals before call deformable segmentation" << std::endl; exit(-1); }

	vect3<float> vertexNormal = normals[vertIdx];
	vect3<float> vert( verts[vertIdx*3], verts[vertIdx*3+1], verts[vertIdx*3+2] );

//	vect3<int> tmpvoxel;
//	mxImageUtils::World2Voxel(*m_responseImage, vert, tmpvoxel);
//	T tmpval = (*m_responseImage)(tmpvoxel[0],tmpvoxel[1],tmpvoxel[2]);

	double stepsize = m_responseImage->GetSpacing()[0] / 4;									
	int steps = static_cast<int>(searchRange/stepsize+0.5);

	double maxGrad = -std::numeric_limits<double>::max();
	int optimalStep = 0;
//	T prev_val = 0;

	for (int i = -steps; i <= steps; ++i)
	{
		vect3<float> prev_point = vert + vertexNormal * (i - 1) * stepsize;
		vect3<float> curr_point = vert + vertexNormal * i * stepsize;
		vect3<float> next_point = vert + vertexNormal * (i + 1) * stepsize;

		vect3<int> prev_voxel, curr_voxel, next_voxel;
		mxImageUtils::World2Voxel(*m_responseImage, prev_point, prev_voxel);
		mxImageUtils::World2Voxel(*m_responseImage, curr_point, curr_voxel);
		mxImageUtils::World2Voxel(*m_responseImage, next_point, next_voxel);

		T prev_val = 0, curr_val = 0, next_val = 0;

		if (!m_responseImage->PtInImage(curr_voxel[0], curr_voxel[1], curr_voxel[2]))
			continue;
		curr_val = (*m_responseImage)(curr_voxel[0], curr_voxel[1], curr_voxel[2]);

		if (!m_responseImage->PtInImage(prev_voxel[0], prev_voxel[1], prev_voxel[2]))
			prev_val = 0;
		else
			prev_val = (*m_responseImage)(prev_voxel[0], prev_voxel[1], prev_voxel[2]);

		if (!m_responseImage->PtInImage(next_voxel[0], next_voxel[1], next_voxel[2]))
			next_val = 0;
		else
			next_val = (*m_responseImage)(next_voxel[0], next_voxel[1], next_voxel[2]);

		double grad = std::abs(next_val - prev_val);
		if ( (grad >= m_minGrad) && (curr_val <= m_maxResponse) && (curr_val >= m_minResponse) && (grad > maxGrad) )
		{
			maxGrad = grad;
			optimalStep = i;
		}

		//vect3<float> point = vert + vertexNormal * i * stepsize;
		//vect3<int> voxel;
		//mxImageUtils::World2Voxel(*m_responseImage, point, voxel);

		//if (!m_responseImage->PtInImage(voxel[0],voxel[1],voxel[2]))
		//	continue; 

		//T val = (*m_responseImage)(voxel[0],voxel[1],voxel[2]);
		//
		//if (i != -steps) {
		//	double grad = std::abs( static_cast<double>(val) - static_cast<double>(prev_val) );
		//	if ((grad >= m_minGrad) && (val <= m_maxResponse) && (val >= m_minResponse) && (grad > maxGrad)) {
		//		maxGrad = grad;
		//		optimalStep = i;
		//	}
		//}
		//prev_val = val;
	}

	vect3<float> ret = vertexNormal * optimalStep * stepsize;
	return ret;

}

} } }

#endif