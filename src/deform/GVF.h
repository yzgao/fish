//
//  GVF.h
//  Fish
//
//  Created by Yaozong Gao on 3/3/14.
//
//

#ifndef __GVF_h__
#define __GVF_h__

#include <boost/array.hpp>
#include "common/mxImageUtils.h"

namespace BRIC { namespace IDEA { namespace FISH {

class GVF {
public:

	typedef boost::array< mxImage<double>, 3 > VectorFieldType;

	// interface for binary image
	template <typename T>
	static void CreateGVF_BinaryImage(const mxImage<T>& binaryImage, VectorFieldType& vectorField, double sigma, double mu, unsigned int numIterations);

	// interface for edge map
	template <typename T>
	static void CreateGVF_EdgeMap(mxImage<T>& edgeMap, VectorFieldType& vectorField, double mu, unsigned int numIterations);

private:
	// enforce mirror boundary condition
	template <typename T>
	static void EnforceMirrorBoundary(mxImage<T>& image);
};

//////////////////////////////////////////////////////////////////////////

template <typename T>
void GVF::CreateGVF_BinaryImage(const mxImage<T>& binaryImage, VectorFieldType& vectorField, double sigma, double mu, unsigned int numIterations)
{
	// compute edge map
	vect3<unsigned int> imageSize = binaryImage.GetImageSize();

	mxImage<double> edgeMap;
	edgeMap.CopyImageInfo(binaryImage);
	edgeMap.SetImageSize(imageSize);

	if (sigma > 0) {
		// compute smooth image and edge map
		mxImage<double> smoothImage;
		smoothImage.CopyImageInfo(binaryImage);
		smoothImage.SetImageSize(imageSize);

		mxImageUtils::GaussianSmooth(binaryImage, smoothImage, sigma);
		mxImageUtils::GradientMap(smoothImage, edgeMap);

	} else {
		// compute edge map
		mxImageUtils::GradientMap(binaryImage, edgeMap);
	}

	CreateGVF_EdgeMap(edgeMap, vectorField, mu, numIterations);
}

template <typename T>
void GVF::CreateGVF_EdgeMap(mxImage<T>& edgeMap, VectorFieldType& vectorField, double mu, unsigned int numIterations)
{
	vect3<unsigned int> imageSize = edgeMap.GetImageSize();
	
	// normalization to [0,1]
	mxImageUtils::NormalizeWithMinMax(edgeMap);

	// enforce mirror boundary
	EnforceMirrorBoundary(edgeMap);

	// compute gradient field and serve as initialization
	mxImageUtils::GradientMaps(edgeMap, vectorField[0], vectorField[1], vectorField[2]);

	// b - gradient squared magnitude
	mxImage<double> b; 
	b.CopyImageInfo(edgeMap);
	b.SetImageSize(imageSize);

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z) {
		for (int y = 0; y < static_cast<int>(imageSize[1]); ++y) {
			for (int x = 0; x < static_cast<int>(imageSize[0]); ++x) {
				double vx = vectorField[0](x, y, z), vy = vectorField[1](x, y, z), vz = vectorField[2](x, y, z);
				b(x, y, z) = vx*vx + vy*vy + vz*vz;
			}
		}
	}

	// compute c[0]: b.*f_x; c[1]: b.*f_y; c[2]: b.*f_z
	VectorFieldType c; 
	c[0].CopyImageInfo(edgeMap); c[0].SetImageSize(imageSize);
	c[1].CopyImageInfo(edgeMap); c[1].SetImageSize(imageSize);
	c[2].CopyImageInfo(edgeMap); c[2].SetImageSize(imageSize);

	#pragma omp parallel for
	for (int z = 0; z < static_cast<int>(imageSize[2]); ++z) {
		for (int y = 0; y < static_cast<int>(imageSize[1]); ++y) {
			for (int x = 0; x < static_cast<int>(imageSize[0]); ++x) {
				for (int k = 0; k < 3; ++k) {
					c[k](x, y, z) = b(x, y, z) * vectorField[k](x, y, z);
				}
			}
		}
	}

	VectorFieldType delta, lmaps;
	delta[0].CopyImageInfo(vectorField[0]); delta[0].SetImageSize(imageSize);
	delta[1].CopyImageInfo(vectorField[1]); delta[1].SetImageSize(imageSize);
	delta[2].CopyImageInfo(vectorField[2]); delta[2].SetImageSize(imageSize);

	lmaps[0].CopyImageInfo(vectorField[0]); lmaps[0].SetImageSize(imageSize);
	lmaps[1].CopyImageInfo(vectorField[1]); lmaps[1].SetImageSize(imageSize);
	lmaps[2].CopyImageInfo(vectorField[2]); lmaps[2].SetImageSize(imageSize);

	std::cout << "iteration starts:" << std::endl;

	// diffusion process
	for (int i = 0; i < static_cast<int>(numIterations); ++i)
	{
		// enforce mirror boundary condition
		EnforceMirrorBoundary(vectorField[0]);
		EnforceMirrorBoundary(vectorField[1]);
		EnforceMirrorBoundary(vectorField[2]);

		// get second derivatives
		mxImageUtils::LaplacianMap(vectorField[0], lmaps[0]);
		mxImageUtils::LaplacianMap(vectorField[1], lmaps[1]);
		mxImageUtils::LaplacianMap(vectorField[2], lmaps[2]);

		// compute incremental vector field
		#pragma omp parallel for
		for (int z = 0; z < static_cast<int>(imageSize[2]); ++z) {
			for (int y = 0; y < static_cast<int>(imageSize[1]); ++y) {
				for (int x = 0; x < static_cast<int>(imageSize[0]); ++x) {
					for (int k = 0; k < 3; ++k) {
						delta[k](x, y, z) = mu * lmaps[k](x, y, z) - b(x, y, z) * vectorField[k](x, y, z) + c[k](x, y, z);
					}
				}
			}
		}

		// update vector field
		#pragma omp parallel for
		for (int z = 0; z < static_cast<int>(imageSize[2]); ++z) {
			for (int y = 0; y < static_cast<int>(imageSize[1]); ++y) {
				for (int x = 0; x < static_cast<int>(imageSize[0]); ++x) {
					for (int k = 0; k < 3; ++k) {
						vectorField[k](x,y,z) = vectorField[k](x,y,z) + delta[k](x, y, z);
					}
				}
			}
		}

		// print out information
		std::cout << i << " ";
		if ((i + 1) % 20 == 0)
			std::cout << std::endl;
	}

	std::cout << "iteration ends" << std::endl;
}

template <typename T>
void GVF::EnforceMirrorBoundary(mxImage<T>& image)
{
	vect3<unsigned int> size = image.GetImageSize();
	unsigned int M = size[0]-1, N = size[1]-1, O = size[2]-1;

	// corners
	image(0, 0, 0) = image(2, 2, 2);
	image(M, 0, 0) = image(M - 2, 2, 2);
	image(0, N, 0) = image(2, N - 2, 2);
	image(0, 0, O) = image(2, 2, O - 2);
	image(M, N, 0) = image(M - 2, N - 2, 2);
	image(M, 0, O) = image(M - 2, 2, O - 2);
	image(0, N, O) = image(2, N - 2, O - 2);
	image(M, N, O) = image(M - 2, N - 2, O - 2);

	// edges
	for (unsigned int z = 1; z < O; ++z) {
		image(0, 0, z) = image(2, 2, z);
		image(M, 0, z) = image(M - 2, 2, z);
		image(0, N, z) = image(2, N - 2, z);
		image(M, N, z) = image(M - 2, N - 2, z);
	}

	for (unsigned int y = 1; y < N; ++y) {
		image(0, y, 0) = image(2, y, 2);
		image(M, y, 0) = image(M - 2, y, 2);
		image(0, y, O) = image(2, y, O - 2);
		image(M, y, O) = image(M - 2, y, O - 2);
	}

	for (unsigned int x = 1; x < M; ++x) {
		image(x, 0, 0) = image(x, 2, 2);
		image(x, N, 0) = image(x, N - 2, 2);
		image(x, 0, O) = image(x, 2, O - 2);
		image(x, N, O) = image(x, N - 2, O - 2);
	}

	// faces
	for (unsigned int x = 1; x < M; ++x) {
		for (unsigned int y = 1; y < N; ++y) {
			image(x, y, 0) = image(x, y, 2);
			image(x, y, O) = image(x, y, O - 2);
		}
	}

	for (unsigned int x = 1; x < M; ++x) {
		for (unsigned int z = 1; z < O; ++z) {
			image(x, 0, z) = image(x, 2, z);
			image(x, N, z) = image(x, N-2, z);
		}
	}

	for (unsigned int y = 1; y < N; ++y) {
		for (unsigned int z = 1; z < O; ++z) {
			image(0, y, z) = image(2, y, z);
			image(M, y, z) = image(M - 2, y, z);
		}
	}
}


} } }

#endif
