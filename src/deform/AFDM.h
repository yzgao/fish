//
//  AFDM.h (Adaptive Focus Deformable Model)
//  FISH
//
//  Created by Yaozong Gao on 10/9/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __AFDM_h__
#define __AFDM_h__

#include "mesh/surface/SurfaceUtils.h"
#include "deform/interfaces.h"
#include "mesh/PDMTransform.h"
#include "mesh/PDMUtils.h"

namespace BRIC { namespace IDEA { namespace FISH {

// public deformable parameters
class AFDMParameters
{
public:
	// iteration number
	unsigned int m_iterNum;

	// controls the step size of deformation
	double m_minDeformRate;
	double m_maxDeformRate;

	// controls the number of driving vertex each iteration (ratio > 0 && ratio <= 1)
	double m_minVertRatio;
	double m_maxVertRatio;		// make this parameter > 1 to deform all vertices before the deformation ends.

	// controls the size of neighbor vertices to deform together
	int m_minNeighborSize;
	int m_maxNeighborSize;

	// controls how large influence of deforming the driving vertex
	double m_expSigma;

	// controls the weight between external force and smooth force
	double m_minSmoothWeight;		// between 0 and 1, the larger the smoother
	double m_maxSmoothWeight;		

	// controls the search range during deformation
	double m_maxSearchRange;
	double m_minSearchRange;

	// the way for shape refinement
	bool m_useAffine;
	bool m_useShapeSpace;

	// controls the frequency to do shape refinement
	int m_minShapeRefinementTime;
	int m_maxShapeRefinementTime;
	double m_fullShapeIterRatio;		// the ratio of iterations when refined shape dominates

	// controls the weight of affine transformation (set -1 to disable)
	double m_minShapeWeight;
	double m_maxShapeWeight;

	// Load parameters from disk
	bool Load(const char* path);
};


// Deformation parameters for one iteration
class AFDMParameters_
{
public:
	double m_deformRate;
	unsigned int m_drvVertNum;
	int m_neighborSize;
	double m_smoothWeight;
	double m_searchRange;
	bool m_useFullShape, m_useAffine, m_useShapeSpace;
	int m_shapeRefinementTime;
	double m_shapeWeight;
};


class AFDM
{
public:
	static void DeformSurface(IDeformContext* context, const AFDMParameters& params, Surface& surf);
private:
	static void ComputeNeighborhood(Surface& surf, int maxNeighorSize, std::vector< std::vector<unsigned int> >& nbVertIdxs, std::vector< std::vector<unsigned int> >& nbVertLevels);
	static void ComputeExpTable(double expSigma, boost::numeric::ublas::matrix<double>& expTable);
	static vect3<float> ComputeInternalOffset(const Surface& surf, unsigned int vertIdx);
	static void DeformSurface_(unsigned int iterIdx, IDeformContext* context, const AFDMParameters_& params, const std::vector< std::vector<unsigned int> >& nbVertIdxs,
		const std::vector< std::vector<unsigned int> >& nbVertLevels, const boost::numeric::ublas::matrix<double>& expTable, unsigned int& startVertIdx, Surface& surf);
};

//////////////////////////////////////////////////////////////////////////

bool AFDMParameters::Load(const char* path)
{
	using boost::property_tree::ptree;

	ptree pt;
	try{
		boost::property_tree::ini_parser::read_ini(path, pt);
	} catch ( boost::property_tree::ini_parser_error& error ) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	try {
		m_iterNum = pt.get<unsigned int>("Deformation.IterNum");
		m_minDeformRate = pt.get<double>("Deformation.MinDeformRate");
		m_maxDeformRate = pt.get<double>("Deformation.MaxDeformRate");
		m_minVertRatio = pt.get<double>("Deformation.MinVertRatio");
		m_maxVertRatio = pt.get<double>("Deformation.MaxVertRatio");
		m_minNeighborSize = pt.get<int>("Deformation.MinNeighborSize");
		m_maxNeighborSize = pt.get<int>("Deformation.MaxNeighborSize");
		m_expSigma = pt.get<double>("Deformation.ExpSigma");
		m_minSmoothWeight = pt.get<double>("Deformation.MinSmoothWeight");
		m_maxSmoothWeight = pt.get<double>("Deformation.MaxSmoothWeight");
		m_minSearchRange = pt.get<double>("Deformation.MinSearchRange");
		m_maxSearchRange = pt.get<double>("Deformation.MaxSearchRange");
		m_useAffine = pt.get<int>("Deformation.UseAffine") != 0;
		m_useShapeSpace = pt.get<int>("Deformation.UseShapeSpace") != 0;
		m_minShapeRefinementTime = pt.get<int>("Deformation.MinShapeRefinementTime");
		m_maxShapeRefinementTime = pt.get<int>("Deformation.MaxShapeRefinementTime");
		m_fullShapeIterRatio = pt.get<double>("Deformation.FullShapeIterRatio");
		m_minShapeWeight = pt.get<double>("Deformation.MinShapeWeight");
		m_maxShapeWeight = pt.get<double>("Deformation.MaxShapeWeight");

	} catch ( boost::property_tree::ptree_error& error ) {
		std::cerr << error.what() << std::endl;
		return false;
	}

	return true;
}


void AFDM::DeformSurface(IDeformContext* context, const AFDMParameters& params, Surface& surf)
{
	// compute the intermediate neighborhood
	surf.UpdateNeighbors();
	surf.UpdateNormals();

	// pre-compute neighborhood and exponential table for speedup
	std::vector< std::vector<unsigned int> > nbVertIdxs;
	std::vector< std::vector<unsigned int> > nbVertLevels;
	ComputeNeighborhood(surf, params.m_maxNeighborSize, nbVertIdxs, nbVertLevels);

	boost::numeric::ublas::matrix<double> expTable(params.m_maxNeighborSize+1, params.m_maxNeighborSize); // (level, maxNbSize)
	ComputeExpTable(params.m_expSigma, expTable);

	unsigned int startVertIdx = 0;
	unsigned int vert_num = surf.GetPoints().size() / 3;
	for (unsigned int i = 0; i < params.m_iterNum; ++i)
	{
		if (i % 10 == 0)
			std::cout << "iteration " << i << std::endl;

		AFDMParameters_ p;

		float iterRatio = static_cast<float>(i) / static_cast<float>(params.m_iterNum);
		// deformation rate
		p.m_deformRate = params.m_minDeformRate + iterRatio * (params.m_maxDeformRate-params.m_minDeformRate);

		// number of driving vertices
		double vertexRatio = params.m_minVertRatio + iterRatio * (params.m_maxVertRatio - params.m_minVertRatio);
		p.m_drvVertNum = static_cast<unsigned int>(vert_num * vertexRatio + 0.5);
		if( p.m_drvVertNum > vert_num ) p.m_drvVertNum = vert_num; 

		// neighborhood size
		p.m_neighborSize = static_cast<int>(params.m_maxNeighborSize - iterRatio*(params.m_maxNeighborSize-params.m_minNeighborSize) + 0.5);
		if (p.m_neighborSize < 0) p.m_neighborSize = 0;

		// smooth weight
		p.m_smoothWeight = params.m_maxSmoothWeight - iterRatio * (params.m_maxSmoothWeight - params.m_minSmoothWeight);
		p.m_smoothWeight = p.m_smoothWeight < 0 ? 0 : (p.m_smoothWeight > 1 ? 1: p.m_smoothWeight);

		// search range
		p.m_searchRange = params.m_maxSearchRange - iterRatio * (params.m_maxSearchRange - params.m_minSearchRange);

		// use full shape constraint, affine, shape space
		p.m_useFullShape = iterRatio <= params.m_fullShapeIterRatio ? true : false;
		p.m_useAffine = params.m_useAffine;
		p.m_useShapeSpace = params.m_useShapeSpace;

		// shape refinement interval and shape weights
		if (p.m_useFullShape) {
			p.m_shapeRefinementTime = 1;
			p.m_shapeWeight = 1;
		}else {
			float tmpRatio = (iterRatio - params.m_fullShapeIterRatio) / (1 - params.m_fullShapeIterRatio);
			p.m_shapeRefinementTime = params.m_minShapeRefinementTime + tmpRatio * (params.m_maxShapeRefinementTime - params.m_minShapeRefinementTime);
			p.m_shapeWeight = params.m_maxShapeWeight - tmpRatio * (params.m_maxShapeWeight - params.m_minShapeWeight);
		}

		DeformSurface_(i, context, p, nbVertIdxs, nbVertLevels, expTable, startVertIdx, surf);
		surf.UpdateNormals();
	}
}

void AFDM::ComputeNeighborhood(Surface& surf, int maxNeighorSize, std::vector< std::vector<unsigned int> >& nbVertIdxs, std::vector< std::vector<unsigned int> >& nbVertLevels)
{
	std::vector<float>& verts = surf.GetPoints();
	unsigned int vert_num = verts.size() / 3;

	// pre-compute the neighborhoods (not just one level)
	nbVertIdxs.resize(vert_num);
	nbVertLevels.resize(vert_num);

	//#pragma omp parallel for
	for (int i = 0; i < static_cast<int>(vert_num); ++i)
		SurfaceUtils::GetVertexNeighborhood(surf, i, maxNeighorSize, nbVertIdxs[i], nbVertLevels[i]);
}

void AFDM::ComputeExpTable(double expSigma, boost::numeric::ublas::matrix<double>& expTable)
{
	unsigned int rows = expTable.size1();
	unsigned int cols = expTable.size2();

	for (unsigned int i = 0; i < rows; ++i)
	{
		for (unsigned int j = 0; j < cols; ++j)
		{
			float factor = static_cast<float>(i) / static_cast<float>(j+1);
			expTable(i,j) = exp(-factor/expSigma);
		}
	}
}

vect3<float> AFDM::ComputeInternalOffset(const Surface& surf, unsigned int vertIdx)
{
	const std::vector<float>& verts = surf.GetPoints();
	const std::vector< std::vector<int> >& nbVerts = surf.GetNeighborhood();

	vect3<float> offset(0,0,0);
	for (unsigned int i = 0; i < nbVerts[vertIdx].size(); ++i) {
		unsigned int nbVertIdx = nbVerts[vertIdx][i];
		offset[0] = offset[0] + verts[nbVertIdx*3];
		offset[1] = offset[1] + verts[nbVertIdx*3+1];
		offset[2] = offset[2] + verts[nbVertIdx*3+2];
	}
	offset /= nbVerts[vertIdx].size();

	for (int i = 0; i < 3; ++i)
		offset[i] = offset[i] - verts[vertIdx*3+i];
	return offset;
}

void AFDM::DeformSurface_(unsigned int iterIdx, IDeformContext* context, const AFDMParameters_& params, const std::vector< std::vector<unsigned int> >& nbVertIdxs,
						   const std::vector< std::vector<unsigned int> >& nbVertLevels, const boost::numeric::ublas::matrix<double>& expTable, unsigned int& startVertIdx, Surface& surf)
{
	std::vector<float>& verts = surf.GetPoints();
	unsigned int vert_num = surf.GetPoints().size() / 3;

	// backup the verts before deformation if global affine is needed
	PDM prevPDM;
	if (params.m_useAffine) {
		std::vector<float>& prev_verts = prevPDM.GetPoints();
		prev_verts.resize(verts.size());
		std::copy(verts.begin(), verts.end(), prev_verts.begin());
	}

	// iterate over all driving vertices
	for (unsigned int i = 0; i < params.m_drvVertNum; ++i) {
		
		unsigned int vertIdx = (startVertIdx + i) % vert_num;
		vect3<float> externalOffset = context->GetVertexOffset(surf, vertIdx, params.m_searchRange);
		if ( std::abs(externalOffset[0])<=VECT_EPSILON && std::abs(externalOffset[1]<=VECT_EPSILON) && std::abs(externalOffset[2]<=VECT_EPSILON) )
			continue;

		unsigned int nbsize = params.m_neighborSize;
		unsigned int nb_num = nbVertLevels[vertIdx].size();
		if (nbsize > nbVertLevels[vertIdx][nb_num-1])
			nbsize = nbVertLevels[vertIdx][nb_num-1];

		// parallel (to do)
		for (unsigned int j = 0; j < nbVertIdxs[vertIdx].size(); ++j)
		{
			int nbLevel = nbVertLevels[vertIdx][j];
			int nbIdx = nbVertIdxs[vertIdx][j];

			if(nbLevel > static_cast<int>(nbsize)) break;

			float percent = 0;
			if (nbLevel == 0)
				percent = 1;
			else 
				percent = expTable(nbLevel, nbsize-1);
	
			vect3<float> vertExternalOffset = externalOffset * percent;
			vect3<float> vertInternalOffset = ComputeInternalOffset(surf, nbIdx);

			vect3<float> vertOffset = (vertExternalOffset*(1-params.m_smoothWeight) + vertInternalOffset*(params.m_smoothWeight)) * params.m_deformRate;
			verts[nbIdx*3] = verts[nbIdx*3] + vertOffset[0];
			verts[nbIdx*3+1] = verts[nbIdx*3+1] + vertOffset[1];
			verts[nbIdx*3+2] = verts[nbIdx*3+2] + vertOffset[2];
		}
	}
	startVertIdx = (startVertIdx + params.m_drvVertNum) % vert_num;

	//////////////////////////////////////////////////////////////////////////
	//SurfaceUtils::SaveSurface(surf, "/Users/yzgao/Desktop/tmp1.vtk");
	//context->SaveSurfaceToImage(surf, "/Users/yzgao/Desktop/tmp1.mha");
	//////////////////////////////////////////////////////////////////////////

	if (params.m_useFullShape || ((iterIdx+1) % params.m_shapeRefinementTime) == 0)
	{
		if (params.m_useAffine) {
			PDMAffineTransform T;  PDM refinedPDM;
			T(prevPDM, surf, refinedPDM);
			PDMUtils::LinearCombine(refinedPDM, params.m_shapeWeight, surf, 1-params.m_shapeWeight, surf);
		} else if (params.m_useShapeSpace) {
			PDM refinedPDM;
			context->RefineShape(surf, refinedPDM);
			PDMUtils::LinearCombine(refinedPDM, params.m_shapeWeight, surf, 1-params.m_shapeWeight, surf);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	//SurfaceUtils::SaveSurface(surf, "/Users/yzgao/Desktop/tmp2.vtk");
	//context->SaveSurfaceToImage(surf, "/Users/yzgao/Desktop/tmp2.mha");
	//////////////////////////////////////////////////////////////////////////
}


} } }

#endif