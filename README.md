Fish Library
================

Fish library for learning-based medical image analysis, including:

* Classification Forest and Multi-source Pixel-wise Image Labeling
* Anatomical Landmark Detection by Regression Forest
* Boundary Detection by Regression Forest
* Context-aware Landmark Detection by Regression Forest + Auto-context
* Multi-task Random Forest 
* Regression-based Deformable Models for Multi-organ Segmentation


### Dependency ###

Fish library is a cmake-based project. You need to install the latest ITK, VTK, and Boost in order to compile fish. 